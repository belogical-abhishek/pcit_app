package com.pcits.events.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.pcits.events.ForgetPasswordActivity;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;
import com.pcits.events.wizard.model.Page;
import com.pcits.events.wizard.ui.PageFragmentCallbacks;

public class ProfileFragmentPage4 extends Fragment implements
		PageFragmentCallbacks {

	private View mView;

	private static final String TAG = "ProfileFragmentPage4";

	private static final String ARG_KEY = "key";

	private TextViewRobotoCondensedRegular mLblResetPassword,
			mLblLastChangedPassword, mLblLastLoggedIn, mLblRegisteredTime;

	// private PageFragmentCallbacks mCallbacks;
	// private String mKey;
	// private CustomProfilePage2 mPage;

	public static ProfileFragmentPage4 create(String key) {
		Bundle args = new Bundle();
		args.putString(ARG_KEY, key);

		ProfileFragmentPage4 fragment = new ProfileFragmentPage4();
		fragment.setArguments(args);
		return fragment;
	}

	public ProfileFragmentPage4() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Bundle args = getArguments();
		// mKey = args.getString(ARG_KEY);
		// mPage = (CustomProfilePage2) mCallbacks.onGetPage(mKey);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.fragment_profile_page4, container,
				false);
		// ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage
		// .getTitle());

		initUI();
		return mView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (!(activity instanceof PageFragmentCallbacks)) {
			throw new ClassCastException(
					"Activity must implement PageFragmentCallbacks");
		}

		// mCallbacks = (PageFragmentCallbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		// mCallbacks = null;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);

		// In a future update to the support library, this should override
		// setUserVisibleHint
		// instead of setMenuVisibility.
		// if (mFlPhone != null && mFlEmail != null && mFlDob != null
		// && mFlPayInfo != null) {
		// InputMethodManager imm = (InputMethodManager) getActivity()
		// .getSystemService(Context.INPUT_METHOD_SERVICE);
		// if (!menuVisible) {
		// imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
		// }
		// }
	}

	public void saveState() {

	}

	public void restoreState() {

	}

	private void initUI() {
		mLblLastChangedPassword = (TextViewRobotoCondensedRegular) mView
				.findViewById(R.id.lbl_time_pass_changed);
		mLblLastLoggedIn = (TextViewRobotoCondensedRegular) mView
				.findViewById(R.id.lbl_last_logged_in);
		mLblResetPassword = (TextViewRobotoCondensedRegular) mView
				.findViewById(R.id.lbl_reset_pass);
		mLblRegisteredTime = (TextViewRobotoCondensedRegular) mView
				.findViewById(R.id.lbl_registered_time);

		if (GlobalValue.myUser != null) {
			if (!GlobalValue.myUser.getLastUpdatedPass().equals("")) {
				mLblLastChangedPassword.setText(" "
						+ DateTimeUtility.convertTimeStampToDate(
								GlobalValue.myUser.getLastUpdatedPass(),
								"MMM dd yyyy HH:mm:ss"));
			} else {
				mLblLastChangedPassword.setText(" "
						+ getResources().getString(
								R.string.you_have_not_changed));
			}

			mLblLastLoggedIn.setText(" "
					+ DateTimeUtility.convertTimeStampToDate(
							GlobalValue.myUser.getLastLoggedin(),
							"MMM dd yyyy HH:mm:ss"));
			mLblRegisteredTime.setText(" "
					+ DateTimeUtility.convertTimeStampToDate(
							GlobalValue.myUser.getRegiteredTime(),
							"MMM dd yyyy HH:mm:ss"));
		} else if (GlobalValue.myClient != null) {
			if (!GlobalValue.myClient.getLastUpdatedPass().equals("")) {
				mLblLastChangedPassword.setText(" "
						+ DateTimeUtility.convertTimeStampToDate(
								GlobalValue.myClient.getLastUpdatedPass(),
								"MMM dd yyyy HH:mm:ss"));
			} else {
				mLblLastChangedPassword.setText(" "
						+ getResources().getString(
								R.string.you_have_not_changed));
			}

			mLblLastLoggedIn.setText(" "
					+ DateTimeUtility.convertTimeStampToDate(
							GlobalValue.myClient.getLastLoggedin(),
							"MMM dd yyyy HH:mm:ss"));
			mLblRegisteredTime.setText(" "
					+ DateTimeUtility.convertTimeStampToDate(
							GlobalValue.myClient.getDate_register(),
							"MMM dd yyyy HH:mm:ss"));
		}

		// Should call this method by the end of declare UI.
		initControl();
	}

	private void initControl() {
		mLblResetPassword.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetPassword();
			}

		});
	}

	private void resetPassword() {
		Intent i = new Intent(getActivity(), ForgetPasswordActivity.class);
		startActivity(i);
	}

	@Override
	public Page onGetPage(String key) {
		// TODO Auto-generated method stub
		return null;
	}

}
