package com.pcits.events.config;

import java.util.ArrayList;

import com.pcits.events.obj.AttendedObj;
import com.pcits.events.obj.CategoryObj;
import com.pcits.events.obj.ClientInfo;
import com.pcits.events.obj.CreateEventObj;
import com.pcits.events.obj.DealObj;
import com.pcits.events.obj.MessageObj;
import com.pcits.events.obj.NotifyBoxObj;
import com.pcits.events.obj.PaidticketsObj;
import com.pcits.events.obj.TnCobj;
import com.pcits.events.obj.UserObj;

public class GlobalValue {

	public static String JMSDF_PREFERENCES = "JMSDF_PREFERENCES";
	public static SharedPref prefs;
	public static ClientInfo myClient;
	public static UserObj myUser;
	public static DealObj dealsObj;
	public static CreateEventObj createEventObj;
	public static MessageObj luckyDrawObj;
	public static MySharedPreferences mySpf;
	public static String SessionId;
	public static String android_id;
	public static String gcm_id;
	public static ArrayList<DealObj> arrDeals = new ArrayList<DealObj>();
	public static ArrayList<TnCobj> arrTnc;
	public static ArrayList<PaidticketsObj> arrTickets;
	public static ArrayList<MessageObj> arrMessage = new ArrayList<MessageObj>();
	public static ArrayList<CategoryObj> arrCategory;
	public static ArrayList<AttendedObj> arrCheckAttending;
	public static ArrayList<NotifyBoxObj> arrUpdateNewEvent;

	public static CategoryObj categoryObj;
	public static TnCobj tncObj;
	public static PaidticketsObj ticket;
	public static MessageObj msg;
	public static MessageObj luckyDraw;
	public static NotifyBoxObj notify;
	public static String mAttend;
	public static int check = 0;
	public static int checkLogin = 0;
	public static int test = 0;
	public static int MAX_EVENT = 50;

	public static boolean isNewEvent;

	public static double latitude;
	public static double longitude;

	// public static int bystartdateAsc;
	// public static int bystartdateDesc;
	// public static int bystartdateAddAsc;
	// public static int bystartdateAddDesc;
	// public static int checkIntentPush;

	// public static DealObj copiedDeal;
}
