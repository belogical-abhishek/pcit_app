/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pcits.events.wizard.model;

import java.util.ArrayList;

import android.support.v4.app.Fragment;

import com.pcits.common.utils.Logging;
import com.pcits.events.fragments.AddEventPage3Fragment;

/**
 * A page asking for a name and an email.
 */
public class CustomAddEventPage3 extends Page implements ICustomPage {
	public static final String ADDRESS_DATA_KEY = "address";
	public static final String CITY_DATA_KEY = "city";
	public static final String COUNTY_DATA_KEY = "county";
	public static final String COUNTRY_DATA_KEY = "country";
	public static final String POSTALCODE_DATA_KEY = "postalCode";
	public static final String LATITUDE_DATA_KEY = "latitude";
	public static final String LONGITUDE_DATA_KEY = "longitude";

	private AddEventPage3Fragment mFragment;

	public CustomAddEventPage3(ModelCallbacks callbacks, String title) {
		super(callbacks, title);
	}

	@Override
	public Fragment createFragment() {
		mFragment = AddEventPage3Fragment.create(getKey());
		return mFragment;
	}

	@Override
	public void getReviewItems(ArrayList<ReviewItem> dest) {
		// dest.add(new ReviewItem("Address", mData.getString(ADDRESS_DATA_KEY),
		// getKey(), -1));
		// dest.add(new ReviewItem("City", mData.getString(CITY_DATA_KEY),
		// getKey(), -1));
		// dest.add(new ReviewItem("County", mData.getString(COUNTY_DATA_KEY),
		// getKey(), -1));
		// dest.add(new ReviewItem("Country", mData.getString(COUNTRY_DATA_KEY),
		// getKey(), -1));
		// dest.add(new ReviewItem("Postal code", mData
		// .getString(POSTALCODE_DATA_KEY), getKey(), -1));
		// dest.add(new ReviewItem("Latitude",
		// mData.getString(LATITUDE_DATA_KEY),
		// getKey(), -1));
		// dest.add(new ReviewItem("Longitude", mData
		// .getString(LONGITUDE_DATA_KEY), getKey(), -1));
	}

	@Override
	public boolean isCompleted() {
		// boolean completed = false;
		// if (TextUtils.isEmpty(mData.getString(TITLE_DATA_KEY))
		// || TextUtils.isEmpty(mData.getString(COMPANY_DATA_KEY))) {
		// completed = false;
		// } else {
		// completed = true;
		// }
		// return completed;
		return true;
	}

	@Override
	public void saveState() {
		try {
			// TODO Auto-generated method stub
			mFragment.saveState();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	@Override
	public void restoreState() {
		try {
			// TODO Auto-generated method stub
			mFragment.restoreState();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

}
