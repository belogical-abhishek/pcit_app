package com.pcits.events.modelmanager;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by EnvisioDevs on 29-01-2018.
 */

public class AddToGroup {
    @SerializedName("clients")
    List<ClientToAdd> clientToAddList;

    public List<ClientToAdd> getClientToAddList() {
        return clientToAddList;
    }

    public void setClientToAddList(List<ClientToAdd> clientToAddList) {
        this.clientToAddList = clientToAddList;
    }
}
