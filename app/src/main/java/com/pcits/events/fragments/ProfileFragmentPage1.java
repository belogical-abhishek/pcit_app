package com.pcits.events.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.pcits.common.utils.Logging;
import com.pcits.events.ProfileActivity;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.utils.AccessStorage;
import com.pcits.events.widgets.CircleImageWithBorder;
import com.pcits.events.widgets.FloatLabeledEditText;
import com.pcits.events.wizard.model.CustomProfilePage1;
import com.pcits.events.wizard.model.Page;
import com.pcits.events.wizard.ui.PageFragmentCallbacks;

public class ProfileFragmentPage1 extends Fragment implements
		PageFragmentCallbacks {

	private View mView;

	private static final String TAG = "ProfileFragmentPage1";

	private static final String ARG_KEY = "key";

	private PageFragmentCallbacks mCallbacks;
	private String mKey;
	private CustomProfilePage1 mPage;

	private FloatLabeledEditText mFlUserName, mFlFirstName, mFlLastName,
			mFlGender, mFlEmail;

	private CircleImageWithBorder mImgAvatar;
	private static final int CHOOSE_AVATAR = 1001;
	private AQuery aq;

	public static ProfileFragmentPage1 create(String key) {
		Bundle args = new Bundle();
		args.putString(ARG_KEY, key);

		ProfileFragmentPage1 fragment = new ProfileFragmentPage1();
		fragment.setArguments(args);
		return fragment;
	}

	public ProfileFragmentPage1() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		mKey = args.getString(ARG_KEY);
		mPage = (CustomProfilePage1) mCallbacks.onGetPage(mKey);

		aq = new AQuery(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.fragment_profile_page1, container,
				false);
		try {
			initUI();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return mView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (!(activity instanceof PageFragmentCallbacks)) {
			throw new ClassCastException(
					"Activity must implement PageFragmentCallbacks");
		}

		mCallbacks = (PageFragmentCallbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);

		// In a future update to the support library, this should override
		// setUserVisibleHint
		// instead of setMenuVisibility.
		if (mFlUserName != null && mFlFirstName != null && mFlLastName != null
				&& mFlGender != null && mFlEmail != null) {
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			if (!menuVisible) {
				imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == CHOOSE_AVATAR && resultCode == Activity.RESULT_OK) {
			try {
				Uri uri = data.getData();
				String path = AccessStorage.getPath(getActivity(), uri);
				ProfileActivity.bmAvatar = AccessStorage
						.loadPrescaledBitmap(path);

				if (ProfileActivity.bmAvatar != null) {
					mImgAvatar.setImageBitmap(ProfileActivity.bmAvatar);
				}

				// lblMessage.setText(selectedImagePath);
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			}
		}
	}

	public void saveState() {
		try {
			if (ProfileActivity.tempClient != null) {
				ProfileActivity.tempClient.setUsername(mFlUserName
						.getTextString());
				ProfileActivity.tempClient.setFname(mFlFirstName
						.getTextString().trim());
				ProfileActivity.tempClient.setLname(mFlLastName.getTextString()
						.trim());
				ProfileActivity.tempClient.setGender(mFlGender.getTextString());
				ProfileActivity.tempClient.setEmail(mFlEmail.getTextString()
						.trim());
			} else if (ProfileActivity.tempAdmin != null) {
				ProfileActivity.tempAdmin.setUsername(mFlUserName
						.getTextString());
				ProfileActivity.tempAdmin.setFname(mFlFirstName.getTextString()
						.trim());
				ProfileActivity.tempAdmin.setLname(mFlLastName.getTextString()
						.trim());
				ProfileActivity.tempAdmin.setEmail(mFlEmail.getTextString()
						.trim());
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public void restoreState() {
		try {
			if (ProfileActivity.tempClient != null) {
				mFlUserName.setText(ProfileActivity.tempClient.getUsername());
				mFlFirstName.setText(ProfileActivity.tempClient.getFname());
				mFlLastName.setText(ProfileActivity.tempClient.getLname());
				mFlGender.setText(ProfileActivity.tempClient.getGender());
				mFlEmail.setText(ProfileActivity.tempClient.getEmail());

				if (ProfileActivity.bmAvatar == null) {
					aq.id(mImgAvatar).image(
							UserFunctions.URLAdmin
									+ GlobalValue.myClient.getImage(), false,
							true);
				}
			} else if (ProfileActivity.tempAdmin != null) {
				mFlUserName.setText(ProfileActivity.tempAdmin.getUsername());
				mFlFirstName.setText(ProfileActivity.tempAdmin.getFname());
				mFlLastName.setText(ProfileActivity.tempAdmin.getLname());
				mFlEmail.setText(ProfileActivity.tempAdmin.getEmail());

				if (ProfileActivity.bmAvatar == null) {
					aq.id(mImgAvatar).image(
							UserFunctions.URLAdmin
									+ GlobalValue.myUser.getImage(), false,
							true);
				}
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void initUI() {
		mFlFirstName = (FloatLabeledEditText) mView
				.findViewById(R.id.txt_firstName);
		mFlGender = (FloatLabeledEditText) mView.findViewById(R.id.txt_gender);
		mFlGender.getEditText().setKeyListener(null);
		mFlLastName = (FloatLabeledEditText) mView
				.findViewById(R.id.txt_lastName);
		mFlUserName = (FloatLabeledEditText) mView
				.findViewById(R.id.txt_userName);
		mFlUserName.getEditText().setKeyListener(null);
		mFlEmail = (FloatLabeledEditText) mView.findViewById(R.id.txt_email);

		if (ProfileActivity.tempAdmin != null) {
			mFlGender.setVisibility(View.GONE);
		}

		mImgAvatar = (CircleImageWithBorder) mView
				.findViewById(R.id.img_avatar);

		if (ProfileActivity.tempAdmin != null) {
			if (ProfileActivity.tempAdmin.getImage() != null) {
				aq.id(mImgAvatar).image(
						UserFunctions.URLAdmin
								+ ProfileActivity.tempAdmin.getImage(), false,
						true, 300, 0, new BitmapAjaxCallback() {
							@Override
							protected void callback(String url, ImageView iv,
									Bitmap bm, AjaxStatus status) {
								// TODO Auto-generated method stub
								super.callback(url, iv, bm, status);

								ProfileActivity.bmAvatar = bm;
							}
						});
			}
		} else if (ProfileActivity.tempClient != null) {
			if (ProfileActivity.tempClient.getImage() != null) {
				aq.id(mImgAvatar).image(
						UserFunctions.URLAdmin
								+ ProfileActivity.tempClient.getImage(), false,
						true, 300, 0, new BitmapAjaxCallback() {
							@Override
							protected void callback(String url, ImageView iv,
									Bitmap bm, AjaxStatus status) {
								// TODO Auto-generated method stub
								super.callback(url, iv, bm, status);

								ProfileActivity.bmAvatar = bm;
							}
						});
			}
		}

		restoreState();

		// Should call this method end of declaring UI.
		initControl();
	}

	private void initControl() {
		mImgAvatar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(
						Intent.createChooser(intent, "Complete action using"),
						CHOOSE_AVATAR);
			}
		});
	}

	@Override
	public Page onGetPage(String key) {
		// TODO Auto-generated method stub
		return null;
	}

}
