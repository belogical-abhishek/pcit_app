package com.pcits.events.modelmanager;

import com.google.gson.annotations.SerializedName;

/**
 * Created by EnvisioDevs on 24-11-2017.
 */

public class SplitClients {

    @SerializedName("email")
    String email;

    @SerializedName("amount")
    String amount;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
