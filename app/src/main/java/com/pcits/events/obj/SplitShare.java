package com.pcits.events.obj;

/**
 * Created by EnvisioDevs on 16-11-2017.
 */

public class SplitShare {

    ListUsersObj user;
    String name;
    String email;
    double share;
    double amount;
    boolean isUpdate;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ListUsersObj getUser() {
        return user;
    }

    public void setUser(ListUsersObj user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getShare() {
        return share;
    }

    public void setShare(double share) {
        this.share = share;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean isUpdate() {
        return isUpdate;
    }

    public void setUpdate(boolean update) {
        isUpdate = update;
    }
}
