package com.pcits.events.database.mapper;

import android.database.Cursor;

import com.pcits.events.obj.DealObj;

public class DealsMapper implements RowMapper<DealObj> {

	@Override
	public DealObj mapRow(Cursor row, int rowNum) {
		// TODO Auto-generated method stub
		DealObj dealsObj = new DealObj();
		dealsObj.setDeal_id(CursorParseUtility.getString(row, "deal_id"));
		dealsObj.setTitle(CursorParseUtility.getString(row, "title"));
		dealsObj.setCompany(CursorParseUtility.getString(row, "company"));
		dealsObj.setStart_date(CursorParseUtility.getString(row, "start_date"));
		dealsObj.setEnd_date(CursorParseUtility.getString(row, "end_date"));
		dealsObj.setStart_time(CursorParseUtility.getString(row, "start_time"));
		dealsObj.setEnd_time(CursorParseUtility.getString(row, "end_time"));
		dealsObj.setAfter_discount_value(CursorParseUtility.getDouble(row,
				"after_discount_value"));
		dealsObj.setImage(CursorParseUtility.getString(row, "image"));
		dealsObj.setStart_value(CursorParseUtility
				.getDouble(row, "start_value"));
		dealsObj.setAttend(CursorParseUtility.getInt(row, "attending"));
		dealsObj.setQuantity(CursorParseUtility.getInt(row, "quantity"));
		dealsObj.setStartTimeStamp(CursorParseUtility.getString(row, "startTimeStamp"));
		dealsObj.setEndTimeStamp(CursorParseUtility.getString(row, "endTimeStamp"));
		return dealsObj;
	}
}
