package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.pcits.events.R;
import com.pcits.events.obj.PurchaseObj;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedBold;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class ClientAdapter extends BaseAdapter {

	private ArrayList<PurchaseObj> arrTicket;
	private LayoutInflater mInflate;

	public ClientAdapter(Activity activity, ArrayList<PurchaseObj> arr) {
		this.arrTicket = arr;
		this.mInflate = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrTicket.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {
		// TODO Auto-generated method stub
		final HolderView holder;
		if (convertView == null) {
			holder = new HolderView();
			convertView = mInflate.inflate(R.layout.item_list_of_ticket, null);
			holder.lblFullName = (TextViewRobotoCondensedBold) convertView
					.findViewById(R.id.lblFullName);
			holder.lblTitle = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lblTitle);
			holder.lblTitle.setSelected(true);
			holder.lblCompany = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lblCompany);
			holder.lblGender = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lblGender);
			convertView.setTag(holder);
		} else {
			holder = (HolderView) convertView.getTag();
		}
		final PurchaseObj o = arrTicket.get(position);
		if (o != null) {
			holder.lblFullName.setText(o.getFullName());
			holder.lblTitle.setText(o.getTitle());
			holder.lblCompany.setText(o.getCompany());
			holder.lblGender.setText(o.getGender());
		}
		return convertView;
	}

	public class HolderView {
		TextViewRobotoCondensedRegular lblTitle, lblCompany, lblGender;
		TextViewRobotoCondensedBold lblFullName;
	}
}
