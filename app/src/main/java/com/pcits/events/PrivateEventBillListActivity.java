package com.pcits.events;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.pcits.common.utils.GsonUtility;
import com.pcits.events.adapters.AdapterBillList;
import com.pcits.events.adapters.AdapterSplitBill;
import com.pcits.events.listeners.OnSplitActionLisener;
import com.pcits.events.modelmanager.ListBillObj;
import com.pcits.events.network.EventsAPI;
import com.pcits.events.network.RestClient;
import com.pcits.events.obj.BillsObj;
import com.pcits.events.obj.ListEventObj;
import com.pcits.events.obj.ListUsersObj;
import com.pcits.events.obj.SplitShare;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PrivateEventBillListActivity extends Activity implements OnSplitActionLisener {
    ListView listAPEBLBills;
    private LinearLayout llMenu;
    Button btnAPEBLcreateNewBill;
    private String mDealId;
    private int mFunctionId;
    private Context mContext;
    private ProgressDialog mProgressDialog;
    private ArrayList<BillsObj> mSplitShareList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_private_event_bill_list);

            Intent intent = getIntent();
            mFunctionId = intent.getIntExtra("functionid", 0);
            mDealId = intent.getStringExtra("dealid");

            initUI();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void initUI()
    {
        try
        {
            llMenu=(LinearLayout)findViewById(R.id.llMenu);
            listAPEBLBills=(ListView)findViewById(R.id.listAPEBLBills);
            btnAPEBLcreateNewBill=(Button) findViewById(R.id.btnAPEBLcreateNewBill);
            mSplitShareList =new ArrayList<>();

            initAdapter();

            listAPEBLBills.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                {
                    BillsObj billsObj=mSplitShareList.get(position);

                    setStatusPrivateEvent(PrivateEventBillListActivity.this,"Event_Status",billsObj.getDeal_id(),billsObj.getId());
                    Intent intent = new Intent(PrivateEventBillListActivity.this, ActivitySplitBills.class);
                    intent.putExtra("functionid", id);
                    intent.putExtra("dealid", mDealId);

                    intent.putExtra("billno", billsObj.getId());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            });

            btnAPEBLcreateNewBill.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(PrivateEventBillListActivity.this, ActivitySplitBills.class);
                    intent.putExtra("functionid", mFunctionId);
                    intent.putExtra("dealid", mDealId);
                    intent.putExtra("billno", "0");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    /*Intent intent = new Intent(PrivateEventBillListActivity.this, ActivityAddBills.class);
                    intent.putExtra("DealId",mDealId);
                    startActivity(intent);
                    finish();*/
                }
            });

            llMenu.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent=new Intent(PrivateEventBillListActivity.this,ActivityHome.class);
                    startActivity(intent);
                    finish();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setListAdapter()
    {
        List<String> status;
        status=getStringArrayPref(this,"Event_Status",mSplitShareList);
       AdapterBillList adapter = new AdapterBillList(PrivateEventBillListActivity.this,mSplitShareList,status);
       listAPEBLBills.setAdapter(adapter);
    }

    private void initAdapter()
    {
        try
        {
            mContext=PrivateEventBillListActivity.this;
            mProgressDialog=new ProgressDialog(mContext);
            Retrofit retrofit = RestClient.retrofitService();
            EventsAPI eventsApiObj = retrofit.create(EventsAPI.class);
            Call<List<ListBillObj> > listBillObjCall = eventsApiObj.getBillForDeal(mDealId);
            listBillObjCall.enqueue(new Callback<List<ListBillObj>>()
            {
                @Override
                public void onResponse(Call<List<ListBillObj>> call, Response<List<ListBillObj>> response)
                {

                    if(response.body().size()>0)
                    {
                        mSplitShareList.clear();
                        for(int i=0;i<response.body().size();i++)
                        {
                            mSplitShareList.add(new BillsObj(response.body().get(i).getId(),
                                                response.body().get(i).getDeal_id(),response.body().get(i).getDescription(),
                                                    response.body().get(i).getImage(),response.body().get(i).getAmount(),
                                        response.body().get(i).getPaid_by(),response.body().get(i).getCreated_by()));
                        }
                        setListAdapter();

                        mProgressDialog.dismiss();

                    }
                    else {
                        mProgressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<ListBillObj>> call, Throwable t) {

                    t.printStackTrace();
                    mProgressDialog.dismiss();
                }


            });
        }catch (Exception e){
            e.printStackTrace();
            if(mProgressDialog.isShowing())
                mProgressDialog.dismiss();

        }

    }

    @Override
    public void OnSharePlus(int position) {

    }

    @Override
    public void OnShareMinus(int position) {

    }

    @Override
    public void OnEdited(int position, String updatedValue) {

    }

    @Override
    public void OnEditorModeOn() {

    }

    private void setStatusPrivateEvent(Context context, String key,String deal_id,String bill_id)
    {
        try
        {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            JSONArray a;
            try
            {
                String json = prefs.getString(key, null);
                 a = new JSONArray(json);
            }
            catch (Exception e)
            {
                 a = new JSONArray();
                e.printStackTrace();
            }

            BillsObj billsObj = null;
            JSONObject b=new JSONObject();

            try
            {
                b.put("event_id", deal_id);
                b.put("bill_id", bill_id);
            }
            catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            a.put(b);

            if (a!=null)
            {
                editor.putString(key, a.toString());
            }
            else
                {
                editor.putString(key, null);
            }
            editor.commit();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public List<String> getStringArrayPref(Context context, String key,List<BillsObj> list)
    {
        List<String> listBillStatus=new ArrayList<>(list.size());
        for (int i = 0; i < list.size(); i++)
        {
            listBillStatus.add("0");
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = prefs.getString(key, null);
        List<BillsObj> listBill=new ArrayList<>();
        if (json != null)
        {
            try
            {
                JSONArray a = null;
                try
                {
                    a = new JSONArray(json);
                    for (int i = 0; i < list.size(); i++)
                    {
                        BillsObj ss=list.get(i);
                        for(int j=0;j<a.length();j++)
                        {
                            JSONObject jsonObject=a.getJSONObject(j);
                            if(ss.getId().equalsIgnoreCase(jsonObject.getString("bill_id")) &&
                                    ss.getDeal_id().equalsIgnoreCase(jsonObject.getString("event_id")))
                            {
                                listBillStatus.set(i,ss.getId());
                            }
                            else
                            {
                                if(listBillStatus.get(i).equalsIgnoreCase("0"))
                                {
                                    listBillStatus.set(i,"0");
                                }
                            }

                        }
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return listBillStatus;
    }
}
