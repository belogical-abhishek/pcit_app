package com.pcits.events.modelmanager;

import com.google.gson.annotations.SerializedName;

/**
 * Created by EnvisioDevs on 29-01-2018.
 */

public class NewBill {
    /* "client_id": 2,
  "description": "Lorem Ipsum",
  "image": "image_path",
  "paid_by": "krupal@envisiodevs.com",
  "amount": 300*/

    @SerializedName("client_id")
    int clientId;
    @SerializedName("description")
    String description = "Lorem Ipsum";
    @SerializedName("image")
    String image="image_path";
    @SerializedName("paid_by")
    String paidBy;
    @SerializedName("amount")
    float amount;

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(String paidBy) {
        this.paidBy = paidBy;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
