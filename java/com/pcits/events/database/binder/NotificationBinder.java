package com.pcits.events.database.binder;

import android.database.sqlite.SQLiteStatement;

import com.pcits.events.obj.NotifyBoxObj;

public class NotificationBinder implements ParameterBinder {

	@Override
	public void bind(SQLiteStatement st, Object object) {
		// TODO Auto-generated method stub
		NotifyBoxObj mode = (NotifyBoxObj) object;
//		st.bindString(1, mode.getNotifyId());
		st.bindString(1, mode.getType_id() + "");
		st.bindString(2, mode.getSummary());
		st.bindString(3, mode.getDate());
		st.bindString(4, mode.getStatus_id());
		st.bindString(5, mode.getTitle());
	}
}
