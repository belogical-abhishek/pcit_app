package com.pcits.events.widgets.imagezoom;

import android.annotation.TargetApi;
import android.view.View;

@TargetApi(16)
public class SDK16 {

	public static void postOnAnimation(View view, Runnable r) {
		view.post(r);
		// view.postOnAnimation(r);
	}

}
