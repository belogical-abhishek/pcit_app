package com.pcits.events;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.pcits.events.adapters.ListEventsAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.fragments.FragmentListPrivateEvents;
import com.pcits.events.network.EventsAPI;
import com.pcits.events.network.RestClient;
import com.pcits.events.obj.ListEventObj;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RejectedPrivateEventActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private String mParam1;
    private String mParam2;
    private RecyclerView mRecyclerView;
    private ProgressDialog mProgressDialog;
    private FragmentListPrivateEvents.OnFragmentInteractionListener mListener;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected_private_event);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_events);
        mProgressDialog=new ProgressDialog(RejectedPrivateEventActivity.this);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        getEvents();
    }

    @Override
    public void onRefresh()
    {
        getEvents();
    }

    public void getEvents()
    {
//        Log.d(TAG, "getEvents: "+ GlobalValue.myClient.getUsername());
        mSwipeRefreshLayout.setRefreshing(true);
        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsApiObj = retrofit.create(EventsAPI.class);
        mProgressDialog.setMessage("Getting Events");
        mProgressDialog.show();

        try
        {
            //Log.d(TAG, "getEvents: ");

            if(GlobalValue.myClient!=null){
                Call<List<ListEventObj>> listeventCallobj = eventsApiObj.getEvents("events/"+Integer.valueOf(GlobalValue.myClient.getId()));
                listeventCallobj.enqueue(new Callback<List<ListEventObj>>() {
                    @Override
                    public void onResponse(Call<List<ListEventObj>> call, Response<List<ListEventObj>> response) {
                        mProgressDialog.dismiss();
                        if (response.isSuccessful())
                        {
                            List<ListEventObj> listevents = response.body();
                            List<ListEventObj> listeventsnew = new ArrayList<>();
                            for(int i=0;i<listevents.size();i++)
                            {
                                ListEventObj leo=listevents.get(i);
                                if(leo.getStatus().equalsIgnoreCase("-1"))
                                {
                                    listeventsnew.add(leo);
                                }
                            }
                            listevents.clear();
                            listevents=listeventsnew;
                            List<String> viewStatus=getStringArrayPref(getBaseContext(),"Event_List_Status",listevents);
                            boolean fromHome=true;
                            ListEventsAdapter eventsadapterobj = new ListEventsAdapter(fromHome,viewStatus,listevents,getBaseContext(),RejectedPrivateEventActivity.this,((GlobalValue.myClient!=null)?GlobalValue.myClient.getUsername():""));
                            eventsadapterobj.notifyDataSetChanged();
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
                            mRecyclerView.setLayoutManager(layoutManager);
                            mRecyclerView.setHasFixedSize(true);
                            mRecyclerView.setAdapter(eventsadapterobj);
                            mSwipeRefreshLayout.setRefreshing(false);
                            // setStatusPrivateEvent(listevents);
                            if (listevents==null || listevents.isEmpty()){
                                Toast.makeText(getBaseContext(),"No private events found",Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<ListEventObj>> call, Throwable throwable) {
                        mProgressDialog.dismiss();
                        throwable.printStackTrace();
                        mSwipeRefreshLayout.setRefreshing(false);

                    }
                });

            } else{
                mProgressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getBaseContext(),"Please login to find your private events",Toast.LENGTH_LONG).show();
            }
        }catch (Exception e) {
            e.printStackTrace();
            mProgressDialog.dismiss();
            mSwipeRefreshLayout.setRefreshing(false);

        }
    }

    public List<String> getStringArrayPref(Context context, String key, List<ListEventObj> list)
    {
        List<String> listEventStatus=new ArrayList<>(list.size());
        for (int i = 0; i < list.size(); i++)
        {
            listEventStatus.add("0");
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = prefs.getString(key, null);
        List<ListEventObj> listEvents=new ArrayList<>();
        if (json != null)
        {
            try
            {
                JSONArray a = null;
                try
                {
                    a = new JSONArray(json);
                    for (int i = 0; i < list.size(); i++)
                    {
                        ListEventObj ss=list.get(i);
                        for(int j=0;j<a.length();j++)
                        {
                            JSONObject jsonObject=a.getJSONObject(j);
                            if(ss.getDealId().equalsIgnoreCase(jsonObject.getString("event_id")))
                            {
                                listEventStatus.set(i,ss.getDealId());
                            }
                            else
                            {
                                if(listEventStatus.get(i).equalsIgnoreCase("0"))
                                {
                                    listEventStatus.set(i,"0");
                                }
                            }

                        }
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return listEventStatus;
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent=new Intent(RejectedPrivateEventActivity.this,ActivityHome.class);
        startActivity(intent);
        finish();
    }
}
