package com.pcits.events.modelmanager;

public interface ModeListener {

	public void onWSError();
	public void OnSuccess(String json);
}
