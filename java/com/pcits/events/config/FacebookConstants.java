package com.pcits.events.config;

public class FacebookConstants {
	public final static String PERMISSIONS = "read_stream,publish_stream,publish_checkins,offline_access,user_photos,email,user_birthday";
	public final static String FACEBOOK_APP_ID = "810449398985440";
	public final static int REQUEST_FACEBOOK_SSO = 1000;
}
