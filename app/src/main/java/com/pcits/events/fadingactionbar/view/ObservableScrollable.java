package com.pcits.events.fadingactionbar.view;

public interface ObservableScrollable {
	void setOnScrollChangedCallback(OnScrollChangedCallback callback);
}
