package com.pcits.events;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityAddBills extends AppCompatActivity {
    private EditText edtTitle, edtAmount, edtNumberOfPeople;
    private Button btnSplitBills;
    private String title, amount, noOfPeople;
    private Toolbar mToolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bills);

        initUI();
        btnSplitBills.setOnClickListener(onClickListener);
    }

    private void initUI() {

        edtTitle = (EditText) findViewById(R.id.edtbillsTitle);
        edtAmount = (EditText) findViewById(R.id.edtamount);
        edtNumberOfPeople = (EditText) findViewById(R.id.edtmembers);
        btnSplitBills = (Button) findViewById(R.id.btnSplitAmount);

        mToolBar = (Toolbar) findViewById(R.id.toolbar_AddBills);

        mToolBar.setTitle("");
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrow_left);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.black));
        }

    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            title = edtTitle.getText().toString();
            amount = edtAmount.getText().toString();
            noOfPeople = edtNumberOfPeople.getText().toString();

            validateData();

        }
    };

    public void sendData() {

        Intent intent = new Intent(ActivityAddBills.this, ActivitySplitAmount.class);
        intent.putExtra("numberOfPeople", noOfPeople);
        intent.putExtra("amount", amount);
        intent.putExtra("title", title);
        startActivity(intent);
        finish();
    }

    public void validateData() {
        if (title.equals("")) {
            Toast.makeText(this, "Please Enter Title", Toast.LENGTH_SHORT).show();
        } else if (amount.equals("")) {
            Toast.makeText(this, "Please Enter Amount", Toast.LENGTH_SHORT).show();

        } else if (noOfPeople.equals("")) {
            Toast.makeText(this, "Please Enter Number of People", Toast.LENGTH_SHORT).show();
        } else {
            sendData();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                Intent intent = new Intent(this, ActivitySplitBills.class);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
