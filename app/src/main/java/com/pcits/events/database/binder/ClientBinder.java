package com.pcits.events.database.binder;

import android.database.sqlite.SQLiteStatement;

import com.pcits.events.obj.ClientInfo;

public class ClientBinder implements ParameterBinder {

	@Override
	public void bind(SQLiteStatement st, Object object) {
		// TODO Auto-generated method stub
		ClientInfo clientObj = (ClientInfo) object;
		st.bindString(1, clientObj.getUsername());
		st.bindString(2, clientObj.getPassword());
		st.bindString(3, clientObj.getFname());
		st.bindString(4, clientObj.getLname());
		st.bindString(5, clientObj.getDob());
		st.bindString(6, clientObj.getGender());
		st.bindString(7, clientObj.getAddress());
		st.bindString(8, clientObj.getPhone());
		st.bindString(9, clientObj.getCity());
		st.bindString(10, clientObj.getCounty());
		st.bindString(11, clientObj.getPostcode());
		st.bindString(12, clientObj.getCountry());
		st.bindString(13, clientObj.getEmail());
		st.bindString(14, clientObj.getImage());
		st.bindString(15, clientObj.getDeviceid());
	}

}
