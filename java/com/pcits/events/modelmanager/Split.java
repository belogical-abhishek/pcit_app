package com.pcits.events.modelmanager;

import com.google.gson.annotations.SerializedName;

/**
 * Created by EnvisioDevs on 23-11-2017.
 */

public class Split {

    /*  "id": 3,
                "bill_id": 3,
                "amount": 100,
                "client_id": 3,
                "created_at": "2017-11-10 08:10:40",
                "updated_at": "2017-11-10 08:10:40"*/

    @SerializedName("id")
    int id;

    @SerializedName("amount")
    int amount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
