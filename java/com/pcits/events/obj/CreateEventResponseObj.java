package com.pcits.events.obj;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SAKSHI on 9/27/2017.
 */

public class CreateEventResponseObj {
    @SerializedName("deal_id")
    @Expose
    private String dealId;

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

}

