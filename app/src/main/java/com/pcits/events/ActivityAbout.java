/**
 * File        : ActivityAbout.java
 * App name    : M1SO Events
 * Version     : 1.1.4
 * Created     : 25/05/14

 * Created by Team M1SO on 25/01/14.
 * Copyright (c) 2014 M1SO. All rights reserved.
 */

package com.pcits.events;

import android.app.ActionBar;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.widget.TextView;

import com.pcits.events.fragments.FragmentAbout;

public class ActivityAbout extends FragmentActivity {

	// Create an instance of ActionBar
	private ActionBar actionbar;

	// Declare view objects
	private Fragment mFrag;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		// Change actionbar title
		int titleId = Resources.getSystem().getIdentifier("action_bar_title",
				"id", "android");
//		if (0 == titleId)
//			titleId = com.actionbarsherlock.R.id.abs__action_bar_title;

		// Change the title color to white
		TextView txtActionbarTitle = (TextView) findViewById(titleId);
		txtActionbarTitle.setTextColor(getResources().getColor(
				R.color.actionbar_title_color));

		// Connect view objects and xml ids
		// Get ActionBar and set back button on actionbar
		actionbar = getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);

		FragmentTransaction t = this.getSupportFragmentManager()
				.beginTransaction();
		mFrag = new FragmentAbout();
		t.replace(R.id.frame_content, mFrag);
		t.commit();

	}

	// Listener for option menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// Previous page or exit
			finish();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

}