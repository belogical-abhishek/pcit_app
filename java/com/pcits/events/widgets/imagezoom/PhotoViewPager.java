package com.pcits.events.widgets.imagezoom;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;

public class PhotoViewPager extends ViewPager {

	public PhotoViewPager(Context context) {
		super(context);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		try {
			return super.onInterceptTouchEvent(ev);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return false;
		}
	}

}
