package com.pcits.events.fragments;

import java.text.NumberFormat;
import java.util.Locale;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityHome;
import com.pcits.events.AdminActivity;
import com.pcits.events.EventAdminActivity;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.NormalAdminObj;
import com.pcits.events.obj.SuperAdminObj;
import com.pcits.events.widgets.KenBurnsView;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class DashBoardFragment extends Fragment {

	private static final String TAG = "DashboardFragment";

	private View v;
	// Declare NotBoringActionBar
	private LinearLayout llMenu, mLlAllAdmin;

	private Button mBtnAdmin, mBtnEvent;
	private TextViewRobotoCondensedRegular mLblAllEvent, mLblEventAdv,
			mLblEventFea, mLblAllClient, mLblMaleClient, mLblFemaleClient,
			mLblAllAdmin, mLblLt5GBP, mLbl5to10GBP, mLbl10to20GBP,
			mLbl20to40GBP, mLblGt40GBP, mLblEventNotExpired, mLblEventExpired,
			mLblAdminNotActivated, mLblClientNotDisclosed, mLblPurchasedMale,
			mLblPurchasedFemale, mLblPurchasedNotDisclosed, mLblEventFree,
			mLblLuckyDrawClaimed, mLblLuckyDrawUnclaimed, mLblLuckyDrawAwarded;
	private SuperAdminObj mSuperAdm;
	private NormalAdminObj mNormalAdm;
	private String mRole = "";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		v = inflater.inflate(R.layout.fragment_admin_dashboard, container,
				false);

		try {
			mRole = GlobalValue.myUser.getRole();

			Log.d(TAG, "User: " + GlobalValue.myUser.getRole());

			initUI();
			if (mRole.equals("0")) {
				initSuperAdminData();
			} else {
				initNormalAdminData();
			}
			initControl();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void initUI() {
		llMenu = (LinearLayout) v.findViewById(R.id.llMenu);
		mLlAllAdmin = (LinearLayout) v.findViewById(R.id.ll_all_admin);

		mBtnAdmin = (Button) v.findViewById(R.id.btn_admin);
		mBtnEvent = (Button) v.findViewById(R.id.btn_events);
		mLblAllClient = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_all_client);
		mLblAllEvent = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_all_event);
		mLblEventAdv = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_event_adv);
		mLblEventFea = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_event_fea);
		mLblLt5GBP = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_lt_5gbp);
		mLbl5to10GBP = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_5_to_10);
		mLbl10to20GBP = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_10_to_20);
		mLbl20to40GBP = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_20_to_40);
		mLblGt40GBP = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_gt_40);
		mLblFemaleClient = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_female_client);
		mLblMaleClient = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_male_client);
		mLblAllAdmin = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_all_admin);
		mLblEventNotExpired = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_event_notexpired);
		mLblEventExpired = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_event_expired);
		mLblAdminNotActivated = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_admin_not_Activated);
		mLblClientNotDisclosed = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_not_disclosed_client);
		mLblPurchasedFemale = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_purchased_female);
		mLblPurchasedMale = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_purchased_male);
		mLblPurchasedNotDisclosed = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_purchased_not_disclosed);
		mLblEventFree = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_event_free);
		mLblLuckyDrawAwarded = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_lucky_draw_awarded);
		mLblLuckyDrawClaimed = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_lucky_draw_claimed);
		mLblLuckyDrawUnclaimed = (TextViewRobotoCondensedRegular) v
				.findViewById(R.id.lbl_lucky_draw_unclaimed);

		// Hide Admin button if log in as normal admin.
		if (mRole.equals("0")) {
			mBtnAdmin.setVisibility(View.VISIBLE);
			mLlAllAdmin.setVisibility(View.VISIBLE);
		} else if (mRole.equals("1")) {
			mBtnAdmin.setVisibility(View.GONE);
			mLlAllAdmin.setVisibility(View.GONE);
		}
	}

	private void initSuperAdminData() {
		try {
			ModelManager.getSuperAdmin(getActivity(), true,
					new ModelManagerListener() {

						@Override
						public void onSuccess(Object object) {
							String json = (String) object;

							// Parser json
							mSuperAdm = ParserUtility
									.parserSuperAdminDashboard(json);

							// Assign data
							mLblAllEvent
									.setText(convertDoubleToString(mSuperAdm
											.getNumberOfEvent()));
							mLblEventAdv
									.setText(convertDoubleToString(mSuperAdm
											.getNumberOfEventAdv()));
							mLblEventFea
									.setText(convertDoubleToString(mSuperAdm
											.getNumberOfEventFea()));
							mLblLt5GBP.setText(convertDoubleToString(mSuperAdm
									.getLt5()));
							mLbl5to10GBP
									.setText(convertDoubleToString(mSuperAdm
											.getFrom5to10()));
							mLbl10to20GBP
									.setText(convertDoubleToString(mSuperAdm
											.getFrom10to20()));
							mLbl20to40GBP
									.setText(convertDoubleToString(mSuperAdm
											.getFrom20to40()));
							mLblGt40GBP.setText(convertDoubleToString(mSuperAdm
									.getGt40()));
							mLblAllClient
									.setText(convertDoubleToString(mSuperAdm
											.getNumberOfClient()));
							mLblFemaleClient
									.setText(convertDoubleToString(mSuperAdm
											.getNumberOfFemaleClient()));
							mLblMaleClient
									.setText(convertDoubleToString(mSuperAdm
											.getNumberOfMaleClient()));
							mLblAllAdmin
									.setText(convertDoubleToString(mSuperAdm
											.getNumberOfAdmin()));
							mLblEventNotExpired
									.setText(convertDoubleToString(mSuperAdm
											.getNumberEventNotExpired()));
							mLblEventExpired
									.setText(convertDoubleToString(mSuperAdm
											.getNumberEventExpired()));
							mLblAdminNotActivated
									.setText(convertDoubleToString(mSuperAdm
											.getNumberAdminNeedActivating()));
							mLblClientNotDisclosed
									.setText(convertDoubleToString(mSuperAdm
											.getGenderClientNotDisclosed()));
							mLblPurchasedFemale
									.setText(convertDoubleToString(mSuperAdm
											.getTotalPurchaseClientFemale()));
							mLblPurchasedMale
									.setText(convertDoubleToString(mSuperAdm
											.getTotalPurchaseClientMale()));
							mLblPurchasedNotDisclosed.setText(convertDoubleToString(mSuperAdm
									.getTotalPurchaseClientNotDisclosed()));
							mLblEventFree
									.setText(convertDoubleToString(mSuperAdm
											.getNumberEventFree()));
							mLblLuckyDrawAwarded
									.setText(convertDoubleToString(mSuperAdm
											.getNumberLuckyDrawAwarded()));
							mLblLuckyDrawClaimed
									.setText(convertDoubleToString(mSuperAdm
											.getNumberLuckyDrawClaimed()));
							mLblLuckyDrawUnclaimed
									.setText(convertDoubleToString(mSuperAdm
											.getNumberLuckyDrawUnclaimed()));
						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}
					});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void initNormalAdminData() {
		try {
			ModelManager.getNormalAdminDashboard(getActivity(),
					GlobalValue.myUser.getUsername(), true,
					new ModelManagerListener() {

						@Override
						public void onSuccess(Object object) {
							String json = (String) object;

							// Parser json
							mNormalAdm = ParserUtility
									.parserNormalAdminDashboard(json);

							// Assign data
							mLblAllEvent
									.setText(convertDoubleToString(mNormalAdm
											.getNumberOfEvent()));
							mLblEventAdv
									.setText(convertDoubleToString(mNormalAdm
											.getNumberOfEventAdv()));
							mLblEventFea
									.setText(convertDoubleToString(mNormalAdm
											.getNumberOfEventFea()));
							mLblLt5GBP.setText(convertDoubleToString(mNormalAdm
									.getLt5()));
							mLbl5to10GBP
									.setText(convertDoubleToString(mNormalAdm
											.getFrom5to10()));
							mLbl10to20GBP
									.setText(convertDoubleToString(mNormalAdm
											.getFrom10to20()));
							mLbl20to40GBP
									.setText(convertDoubleToString(mNormalAdm
											.getFrom20to40()));
							mLblGt40GBP
									.setText(convertDoubleToString(mNormalAdm
											.getGt40()));
							mLblAllClient
									.setText(convertDoubleToString(mNormalAdm
											.getNumberOfClient()));
							mLblFemaleClient
									.setText(convertDoubleToString(mNormalAdm
											.getNumberOfFemaleClient()));
							mLblMaleClient
									.setText(convertDoubleToString(mNormalAdm
											.getNumberOfMaleClient()));
							mLblEventNotExpired
									.setText(convertDoubleToString(mNormalAdm
											.getNumberEventNotExpired()));
							mLblEventExpired
									.setText(convertDoubleToString(mNormalAdm
											.getNumberEventExpired()));
							mLblClientNotDisclosed
									.setText(convertDoubleToString(mNormalAdm
											.getGenderClientNotDisclosed()));
							mLblPurchasedFemale
									.setText(convertDoubleToString(mNormalAdm
											.getTotalPurchaseClientFemale()));
							mLblPurchasedMale
									.setText(convertDoubleToString(mNormalAdm
											.getTotalPurchaseClientMale()));
							mLblPurchasedNotDisclosed.setText(convertDoubleToString(mNormalAdm
									.getTotalPurchaseClientNotDisclosed()));
							mLblEventFree
									.setText(convertDoubleToString(mNormalAdm
											.getNumberEventFree()));
							mLblLuckyDrawAwarded
									.setText(convertDoubleToString(mNormalAdm
											.getNumberLuckyDrawAwarded()));
							mLblLuckyDrawClaimed
									.setText(convertDoubleToString(mNormalAdm
											.getNumberLuckyDrawClaimed()));
							mLblLuckyDrawUnclaimed
									.setText(convertDoubleToString(mNormalAdm
											.getNumberLuckyDrawUnclaimed()));
						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}
					});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void initControl() {

		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
				Intent intent=new Intent(getActivity(),ActivityHome.class);
				startActivity(intent);
				getActivity().finish();
			}
		});

		mBtnAdmin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getActivity().startActivity(
						new Intent(getActivity(), AdminActivity.class));
			}
		});

		mBtnEvent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(), EventAdminActivity.class);
				if (mRole.equals("0")) {
					i.putExtra("role", "0");
				} else if (mRole.equals("1")) {
					i.putExtra("role", GlobalValue.myUser.getUsername());
				}
				getActivity().startActivity(i);
			}
		});
	}

	private String convertDoubleToString(double number) {
		String result = "";

		Locale loc = new Locale("en", "US");
		NumberFormat fmt = NumberFormat.getNumberInstance(loc);

		result = fmt.format(
				Long.parseLong(Double.valueOf(number).toString()
						.substring(0, (number + "").lastIndexOf("."))))

		.replace(",", ".");

		return result;
	}
}
