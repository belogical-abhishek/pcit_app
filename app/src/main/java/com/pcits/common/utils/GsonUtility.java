package com.pcits.common.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Pritam on 24/02/17.
 */

public class GsonUtility {

    /**
     * function to convert a json string to java object
     * @param jsonString
     * @param token
     * @param <T>
     * @return
     */


    public static <T> T convertJSONStringToObject(String jsonString, TypeToken token){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        return gson.fromJson(jsonString, token.getType());
    }

    public static String convertObjectToJSONString(Object src){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        return gson.toJson(src);
    }

    public static JSONObject getJSONObject(Object src) throws JSONException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        JSONObject obj = new JSONObject(gson.toJson(src));
        return obj ;
    }




}
