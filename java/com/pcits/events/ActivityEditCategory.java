package com.pcits.events;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.pcits.common.utils.Logging;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.CategoryObj;
import com.pcits.events.utils.AccessStorage;

public class ActivityEditCategory extends FragmentActivity implements
		OnClickListener {

	private EditText txtCategoryName;
	private Button btnSelectImg, btnSubmit;
	private String selectedImagePath;
	private Bitmap avatarBitmap;
	private CategoryObj category = null;
	private LinearLayout llMenu;
	private TextView lblMessage, lblTitleHeader;
	private ImageView imgCategory;
	private AQuery mAq;

	protected void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_add_category);
			mAq = new AQuery(this);
			initUI();
			initControl();

			// load ads
			getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	private void initUI() {
		lblTitleHeader = (TextView) findViewById(R.id.lblTitleHeader);
		lblTitleHeader.setText("Edit Category");
		txtCategoryName = (EditText) findViewById(R.id.txtCategoryName);
		btnSelectImg = (Button) findViewById(R.id.btnSelectImgCategory);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);
		imgCategory = (ImageView) findViewById(R.id.imgCategory);
		llMenu = (LinearLayout) findViewById(R.id.llMenu);
		lblMessage = (TextView) findViewById(R.id.messageText);

	}

	private void initControl() {
		try {
			txtCategoryName.setText(GlobalValue.categoryObj.getCategoryName());
			mAq.id(imgCategory).image(
					UserFunctions.URLAdmin
							+ GlobalValue.categoryObj.getCategoryImage(),
					false, true);
			btnSelectImg.setOnClickListener(this);
			btnSubmit.setOnClickListener(this);
			llMenu.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == RESULT_OK) {
			Toast.makeText(this, "choose image", Toast.LENGTH_SHORT).show();

			try {
				Uri uri = data.getData();
				selectedImagePath = AccessStorage.getPath(
						ActivityEditCategory.this, uri);
				avatarBitmap = AccessStorage
						.loadPrescaledBitmap(selectedImagePath);
				// avatarBitmap = CommonMethods.getResizedBitmap(avatarBitmap,
				// 200, 200);
				Drawable d = new BitmapDrawable(getResources(), avatarBitmap);
				imgCategory.setImageDrawable(d);
				lblMessage.setText(selectedImagePath);
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			}
		}
	}

	private void update() {
		final String categoryName = txtCategoryName.getText().toString();

		category = new CategoryObj();
		category.setCategoryId(GlobalValue.categoryObj.getCategoryId());
		category.setCategoryName(categoryName.trim());

		ModelManager.updateCategory(this, category, avatarBitmap, true,
				new ModelManagerListener() {
					@Override
					public void onSuccess(Object object) {
						// TODO Auto-generated method stub
						String json = (String) object;

						Log.e("aaaaa", "result :" + json);

						Toast.makeText(ActivityEditCategory.this,
								checkResult(json), Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onError() {
						// TODO Auto-generated method stub

					}
				});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	protected String checkResult(String strJson) {
		// TODO Auto-generated method stub
		JSONObject json = null;
		String message = "";
		try {
			json = new JSONObject(strJson);
			if (json.getString("status").equals("success")) {
				message = this.getString(R.string.message_success);
				finish();
			} else {
				message = json.getString("message");
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return message;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// select image
		if (v.getId() == R.id.btnSelectImgCategory) {
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(
					Intent.createChooser(intent, "Complete action using"), 1);
		}
		// submit
		if (v.getId() == R.id.btnSubmit) {
			// String message = validateForm();
			// if (!message.equals("success")) {
			// Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
			// }
			update();
		}
	}
}
