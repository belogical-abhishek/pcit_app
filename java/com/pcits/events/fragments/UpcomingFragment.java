package com.pcits.events.fragments;

import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONObject;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.pcits.common.utils.Logging;
import com.pcits.common.utils.RuntimePermissions;
import com.pcits.events.ActivityDetail;
import com.pcits.events.ActivityHome;
import com.pcits.events.AddEventActivity;
import com.pcits.events.R;
import com.pcits.events.adapters.HomeAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.libraries.JSONParser;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.libraries.ServiceNotification;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.obj.APIObj;
import com.pcits.events.obj.DealObj;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class UpcomingFragment extends Fragment implements
        OnClickListener, LocationListener, OnMapReadyCallback {

    private static final String TAG = "UpcomingFragment";
    private ArrayList<DealObj> mArrDeals;

    // Declare object of userFunctions and Utils class
    private UserFunctions userFunction;
    private Utils utils;

    // Create instance of list and ListAdapter
    private ListView list;
    private TextView lblNoResult;
    private HomeAdapter mla;

    private Button btnLoadMore;
    private LinearLayout lytRetry;

    // flag for current page
    private JSONObject json;
    private int mCurrentPage = 0;
    private int mPreviousPage;

    private int intLengthData;
    // Declare variable
    public static Activity self;
    private PullToRefreshListView mPullRefreshListView;

    private View v;

    private String client;
    private double lat;
    private double lnt;
    private LocationManager locationManager;
    private GoogleMap map;
    private MapView mapView;
    private String distance = "20";
    private LatLng mLatLng;

    private DealObj mDealObj;

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        map.clear();

        map.setIndoorEnabled(true);

        if (RuntimePermissions.hasLocationPermission(self)) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            map.setMyLocationEnabled(true);
        }

    }

    // Declare OnListSelected interface
    public interface OnDataListSelectedListener {
        public void onListSelected(String idSelected);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        self = getActivity();
        v = inflater.inflate(R.layout.fragment_upcoming, container, false);
        try {
            if (mArrDeals == null) {
                mArrDeals = new ArrayList<DealObj>();
            }
            // Clear old data
            mArrDeals.clear();

            // list = (ListView) v.findViewById(R.id.list);
            lblNoResult = (TextView) v.findViewById(R.id.lblNoResult);
            lytRetry = (LinearLayout) v.findViewById(R.id.llRetry);
            // btnRetry = (Button) v.findViewById(R.id.btnRetry);

            // Declare object of userFunctions class
            userFunction = new UserFunctions();
            utils = new Utils(getActivity());
            mapView = (MapView) v.findViewById(R.id.mapView);
            mapView.onCreate(savedInstanceState);
            // Create LoadMore button
            btnLoadMore = new Button(getActivity());
            btnLoadMore
                    .setBackgroundResource(R.drawable.apptheme_btn_default_holo_light);
            btnLoadMore.setText(getString(R.string.btn_load_more));
            btnLoadMore.setTextColor(getResources().getColor(R.color.text_btn));

            // Load data
            new loadFirstListView().execute();

            mPullRefreshListView = (PullToRefreshListView) v
                    .findViewById(R.id.lsvPullToRefresh);
            list = mPullRefreshListView.getRefreshableView();
            // Set a listener to be invoked when the list should be refreshed.
            if (NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
                mPullRefreshListView
                        .setOnRefreshListener(new OnRefreshListener<ListView>() {
                            @Override
                            public void onRefresh(
                                    PullToRefreshBase<ListView> refreshView) {

                                String label = DateUtils.formatDateTime(
                                        getActivity(),
                                        System.currentTimeMillis(),
                                        DateUtils.FORMAT_SHOW_TIME
                                                | DateUtils.FORMAT_SHOW_DATE
                                                | DateUtils.FORMAT_ABBREV_ALL);

                                // Update the LastUpdatedLabel
                                refreshView.getLoadingLayoutProxy()
                                        .setLastUpdatedLabel(label);
                                mArrDeals = new ArrayList<DealObj>();
                                new loadFirstListView().execute();

                            }
                        });
            } else {
                Toast.makeText(getActivity(),
                        getString(R.string.no_internet_connection),
                        Toast.LENGTH_SHORT).show();
            }

            // Listener to handle load more buttton when clicked
            btnLoadMore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    try {
                        // Starting a new async task
                        if (NetworkUtility.getInstance(getActivity())
                                .isNetworkAvailable()) {
                            json = null;
                            new loadMoreListView().execute();
                        } else {
                            Toast.makeText(getActivity(),
                                    getString(R.string.no_internet_connection),
                                    Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ex) {
                        throw new RuntimeException(ex);
                    }
                }
            });

            // Add an end-of-list listener
            mPullRefreshListView
                    .setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

                        @Override
                        public void onLastItemVisible() {
                            // Toast.makeText(getActivity(), "end page",
                            // Toast.LENGTH_SHORT).show();
                        }
                    });

            // Listener to get selected id when list item clicked
            list.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1,
                                        int position, long arg3) {
                    // TODO Auto-generated method stub

                    try {

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    GlobalValue.dealsObj = mArrDeals.get(position);
                    DatabaseUtility.insertDeals(getActivity(),
                            mArrDeals.get(position - 1));

                    Log.d("", "DATA " + "Successfully");

                    ModelManager.updateClickCount(getActivity(),
                            mArrDeals.get(position - 1).getDeal_id(), true,
                            new ModelManagerListener() {

                                @Override
                                public void onSuccess(Object object) {
                                    // TODO Auto-generated method stub
                                    String json = (String) object;
                                    boolean result = ParserUtility
                                            .updateAccount(json);
                                    if (result) {
                                        Log.d(TAG, "Successfully");
                                    }
                                }

                                @Override
                                public void onError() {
                                    // TODO Auto-generated method stub

                                }
                            });

                    // }
                    // Pass id to onListSelected method on HomeActivity
                    // mCallback.onListSelected(item.get(userFunction.key_deals_id));
                    Intent i = new Intent(getActivity(), ActivityDetail.class);
                    // i.putExtra(utils.EXTRA_DEAL_ID, mArrDeals.get(position -
                    // 1)
                    // .getDeal_id());
                    startActivity(i);
                    getActivity().overridePendingTransition(
                            R.anim.slide_in_left, R.anim.slide_out_left);

                    // Set the item as checked to be highlighted when in
                    // two-pane
                    // layout
                    list.setItemChecked(position, true);
                }
            });

            list.setOnItemLongClickListener(new OnItemLongClickListener() {

                @Override
                public boolean onItemLongClick(AdapterView<?> parent,
                                               View view, int position, long id) {
                    // TODO Auto-generated method stub
                    if (GlobalValue.myUser != null) {
                        try {
                            mDealObj = (DealObj) mArrDeals.get(position - 1)
                                    .clone();

                            showOperationDialog();
                        } catch (Exception ex) {
                            // TODO Auto-generated catch block
                            Logging.writeExceptionFromStackTrace(ex,
                                    ex.getMessage());
                        }
                    }
                    return true;
                }
            });

            // initNotBoringActionBar();
            setUpMap();

        } catch (Exception ex) {
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
        return v;
    }

    LatLng CENTER = null;

    private void setUpMap() {
        try {
            MapsInitializer.initialize(getActivity());

            switch (GooglePlayServicesUtil
                    .isGooglePlayServicesAvailable(getActivity())) {
                case ConnectionResult.SUCCESS:
                    // Gets to GoogleMap from the MapView and does initialization
                    // stuff
                    if (mapView != null) {

                        locationManager = ((LocationManager) getActivity()
                                .getSystemService(Context.LOCATION_SERVICE));

                        Boolean localBoolean = Boolean.valueOf(locationManager
                                .isProviderEnabled("network"));

                        if (localBoolean.booleanValue()) {

                            CENTER = new LatLng(lat, lnt);

                        } else {

                        }
                        mapView.getMapAsync(this);


                        Criteria crit = new Criteria();

                        String provider = locationManager.getBestProvider(crit,
                                true);

                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        Location loc = locationManager
                                .getLastKnownLocation(provider);

                        if (loc != null) {
                            onLocationChanged(loc);
                        }
                        locationManager.requestLocationUpdates(provider, 20000, 0,
                                this);

                        // Stop requesting location.
                        if (locationManager != null) {
                            locationManager.removeUpdates(this);
                        }

                    }
                    break;
                case ConnectionResult.SERVICE_MISSING:

                    break;
                case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:

                    break;
                default:

            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private void showOperationDialog() {
        final Dialog dialog = new Dialog(self);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_operation_event);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        TextViewRobotoCondensedRegular copy = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_copy);
        copy.setVisibility(View.GONE);
        TextViewRobotoCondensedRegular delete = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_delete);
        TextViewRobotoCondensedRegular revoke = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_revoke);
        TextViewRobotoCondensedRegular scan = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_scan);
        TextViewRobotoCondensedRegular edit = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_edit);

        // Set listener
        copy.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Open create event page with event copied
                AddEventActivity.isDeal = true;
                mDealObj.setDeal_id(null);
                GlobalValue.dealsObj = mDealObj;
                Intent i = new Intent(getActivity(), AddEventActivity.class);
                startActivity(i);

                // Dismiss the dialog
                dialog.dismiss();
            }
        });

        delete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(self, "Deleted", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        revoke.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(self, "Revoked", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        scan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(self, "Scanned", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        edit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Open edit event page
                // Intent i = new Intent(self, ActivityEditDeal.class);
                AddEventActivity.isDeal = true;
                GlobalValue.dealsObj = mDealObj;
                Intent i = new Intent(getActivity(), AddEventActivity.class);
                startActivity(i);
                // Dismiss dialog
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        inflater.inflate(R.menu.actions, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        mDealObj = null;
    }

    // Load first 10 videos
    private class loadFirstListView extends AsyncTask<Void, Void, Void> {

        protected Void doInBackground(Void... unused) {
            try {
                if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
                    getDataFromServer();
                } else {
                    // getDataFromDB();
                }

            } catch (Exception ex) {
                Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
            }
            return null;
        }

        protected void onPostExecute(Void unused) {
            try {
                // Condition if data length uder 10 button loadMore is remove
                list.removeFooterView(btnLoadMore);
                mPullRefreshListView.onRefreshComplete();
                if (intLengthData < UserFunctions.valueItemsPerPage) {
                    list.removeFooterView(btnLoadMore);
                } else {
                    list.addFooterView(btnLoadMore);
                }
                if (isAdded()) {
                    if (mArrDeals.size() != 0) {
                        // Check paramter notif
                        int paramNotif = utils
                                .loadPreferences(utils.UTILS_PARAM_NOTIF);

                        // Condition if app start in the first time notif will
                        // run
                        // in background
                        if (paramNotif != 1) {
                            utils.saveString(utils.UTILS_NOTIF, mArrDeals
                                    .get(0).getDeal_id());
                            utils.savePreferences(utils.UTILS_PARAM_NOTIF, 1);
                            startService();
                        }

                        // Adding load more button to lisview at bottom
                        lytRetry.setVisibility(View.GONE);
                        list.setVisibility(View.VISIBLE);
                        lblNoResult.setVisibility(View.GONE);
                        // Getting adapter
                        if (mla == null) {
                            mla = new HomeAdapter(getActivity(), mArrDeals);
                        }
                        mla.notifyDataSetChanged();
                        list.setAdapter(mla);
                        mPullRefreshListView.onRefreshComplete();

                    } else {
                        list.removeFooterView(btnLoadMore);
                        if (json != null) {
                            lblNoResult.setVisibility(View.VISIBLE);
                            lytRetry.setVisibility(View.GONE);

                        } else {
                            lblNoResult.setVisibility(View.GONE);
                            lytRetry.setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(),
                                    getString(R.string.no_connection),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                super.onPostExecute(unused);
                // Closing progress dialog
                // pDialog.dismiss();

                ActivityHome.rltProgress.setVisibility(View.GONE);
            } catch (Exception ex) {
                Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
            }
        }
    }

    // Load more videos
    private class loadMoreListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            ActivityHome.rltProgress.setVisibility(View.VISIBLE);
        }

        protected Void doInBackground(Void... unused) {
            try {
                // Store previous value of current page
                mPreviousPage = mCurrentPage;
                // Increment current page
                mCurrentPage += UserFunctions.valueItemsPerPage;
                if (NetworkUtility.getInstance(getActivity())
                        .isNetworkAvailable()) {
                    getDataFromServer();
                } else {
                    // getDataFromDB();
                }
            } catch (Exception ex) {
                Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
            }
            return (null);
        }

        protected void onPostExecute(Void unused) {
            // Condition if data length uder 10 button loadMore is remove
            list.removeFooterView(btnLoadMore);
            if (intLengthData < UserFunctions.valueItemsPerPage)
                list.removeFooterView(btnLoadMore);
            if (json != null) {
                // Get listview current position - used to maintain scroll
                // position
                int currentPosition = list.getFirstVisiblePosition();
                lytRetry.setVisibility(View.GONE);
                // Appending new data to menuItems ArrayList
                mla = new HomeAdapter(getActivity(), mArrDeals);
                mla.notifyDataSetChanged();
                // list.setAdapter(mla);
                // Setting new scroll position
                list.setSelectionFromTop(currentPosition + 1, 0);
                // mPullRefreshListView.onRefreshComplete();

            } else {
                if (mArrDeals != null) {
                    mCurrentPage = mPreviousPage;
                    lytRetry.setVisibility(View.GONE);
                } else {
                    lytRetry.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(),
                            getString(R.string.no_connection),
                            Toast.LENGTH_SHORT).show();
                }

            }
        }
    }

    // Method to get Data from Server
    public void getDataFromServer() {
        try {
            if (GlobalValue.myClient != null) {
                client = GlobalValue.myClient.getUsername();
            } else {
                client = "";
            }

            //for testing
            if(lat == 0) {
                lat = 18.5124172197085;
            }

            if(lnt == 0) {
                lnt = 73.80467511970849;
            }

            json = userFunction.upComing(Integer.parseInt(distance), lat, lnt,
                    mCurrentPage);
            if (json != null) {
                ArrayList<DealObj> arr = JSONParser.parserDeal(
                        UserFunctions.KEY_JSON_DATA, json.toString());
                mArrDeals.addAll(arr);
            }

            Log.e(TAG, "getDataFromServer: " + mArrDeals.size());

        } catch (NullPointerException ex) {
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void getDataFromDB() {
        try {
            String apiDeals = UserFunctions.URL_ALL_DEALS + mCurrentPage;
            APIObj allDealInfos = DatabaseUtility.getResuftFromApi(
                    getActivity(), apiDeals);
            Log.e(TAG, "Database: " + allDealInfos);
            String jsonAllDeal = "";
            if (allDealInfos != null) {
                jsonAllDeal = allDealInfos.getmResult();
            }
            ArrayList<DealObj> arr = JSONParser.parserDeal("data",
                    json.toString());
            mArrDeals.addAll(arr);
            intLengthData = mArrDeals.size();
            Log.e(TAG, "All deal: " + jsonAllDeal);

            APIObj currencyInfos = DatabaseUtility.getResuftFromApi(
                    getActivity(), UserFunctions.URL_CURRENCY);
            Log.e(TAG, "Database: " + currencyInfos);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        list.setAdapter(null);
        super.onDestroy();

    }

    public void startService() {

        Intent myIntent = new Intent(getActivity(), ServiceNotification.class);

        PendingIntent pendingIntent = PendingIntent.getService(getActivity(),
                0, myIntent, 0);

        AlarmManager alarmManager = (AlarmManager) getActivity()
                .getSystemService(getActivity().ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), 30 * 1000, pendingIntent);

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onLocationChanged(Location location) {
        // TODO Auto-generated method stub
        lat = location.getLatitude();
        lnt = location.getLongitude();

        mLatLng = new LatLng(lat, lnt);

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 16));

        // mMaps.addMarker(new MarkerOptions().position(mLatLng)).setTitle(
        // "Current Location");

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }

}
