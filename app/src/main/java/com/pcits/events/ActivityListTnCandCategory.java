package com.pcits.events;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.adapters.AdapterCategory;
import com.pcits.events.adapters.AdapterTnCByUser;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.widgets.KenBurnsView;

public class ActivityListTnCandCategory extends FragmentActivity {

	private Button btnAddDeal;

	// Create instance of list and ListAdapter
	private ListView list;
	private AdapterTnCByUser adapterTnCbyUser;
	private AdapterCategory adapterCategory;

	// create array variables to store data
	private String listCategory;
	private String listTnC;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;

	protected void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.layout_list_deals);
			// Get Bundle data
			Intent i = getIntent();
			listCategory = i.getStringExtra("listCategory");
			listTnC = i.getStringExtra("listTnC");

			initUI();
			initControl();
			// load ads
			getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			initNotBoringActionBar();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	private void initNotBoringActionBar() {
		mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}

	@Override
	protected void onResume() {
		try {
			// TODO Auto-generated method stub
			super.onResume();
			if (listCategory != null) {
				btnAddDeal.setText("Add New Category");
				btnAddDeal.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent i = new Intent(ActivityListTnCandCategory.this,
								ActivityAddCategory.class);
						startActivity(i);
					}
				});
				getListCategory();

			}
			if (listTnC != null) {
				btnAddDeal.setText("Add New TNC");
				btnAddDeal.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent i = new Intent(ActivityListTnCandCategory.this,
								ActivityAddTnC.class);
						startActivity(i);
					}
				});
				if (Integer.parseInt(GlobalValue.myUser.getRole()) == 0) {
					getListAllTnc();
				} else {
					getListTnc();
				}
			}
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	private void initUI() {
		try {
			llMenu = (LinearLayout) findViewById(R.id.llMenu);
			list = (ListView) findViewById(R.id.lsvDeals);
			btnAddDeal = (Button) findViewById(R.id.btnAddDeal);
			if (listCategory != null) {
				btnAddDeal.setText("Add New Category");
				btnAddDeal.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent i = new Intent(ActivityListTnCandCategory.this,
								ActivityAddCategory.class);
						startActivity(i);
					}
				});
				getListCategory();
				list.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub
						GlobalValue.categoryObj = GlobalValue.arrCategory
								.get(position);
						Intent i = new Intent(ActivityListTnCandCategory.this,
								ActivityEditCategory.class);
						startActivity(i);
					}
				});

			}
			if (listTnC != null) {
				btnAddDeal.setText("Add New TNC");
				btnAddDeal.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent i = new Intent(ActivityListTnCandCategory.this,
								ActivityAddTnC.class);
						startActivity(i);
					}
				});
				if (Integer.parseInt(GlobalValue.myUser.getRole()) == 0) {
					getListAllTnc();
				} else {
					getListTnc();
				}
				list.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub
						GlobalValue.tncObj = GlobalValue.arrTnc.get(position);
						Intent i = new Intent(ActivityListTnCandCategory.this,
								ActivityEditTnC.class);
						startActivity(i);
					}
				});
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void initControl() {
		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}

	// list tnc by user
	private void getListTnc() {
		try {
			ModelManager.getListTncByUser(ActivityListTnCandCategory.this,
					GlobalValue.myUser.getUsername(), true,
					new ModelManagerListener() {

						@Override
						public void onSuccess(Object object) {
							// TODO Auto-generated method stub
							String json = (String) object;
							GlobalValue.arrTnc = ParserUtility.getListTnc(json);
							if (GlobalValue.arrTnc.size() > 0) {
								adapterTnCbyUser = new AdapterTnCByUser(
										ActivityListTnCandCategory.this,
										GlobalValue.arrTnc);
								adapterTnCbyUser.notifyDataSetChanged();
								list.setAdapter(adapterTnCbyUser);
							} else {
								Toast.makeText(ActivityListTnCandCategory.this,
										"No Data", Toast.LENGTH_SHORT).show();
							}
						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}
					});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	// list all tnc
	private void getListAllTnc() {
		try {
			ModelManager.getListAllTnC(ActivityListTnCandCategory.this, true,
					new ModelManagerListener() {

						@Override
						public void onSuccess(Object object) {
							// TODO Auto-generated method stub
							String json = (String) object;
							GlobalValue.arrTnc = ParserUtility.getListTnc(json);
							if (GlobalValue.arrTnc.size() > 0) {
								adapterTnCbyUser = new AdapterTnCByUser(
										ActivityListTnCandCategory.this,
										GlobalValue.arrTnc);
								adapterTnCbyUser.notifyDataSetChanged();
								list.setAdapter(adapterTnCbyUser);
							} else {
								Toast.makeText(ActivityListTnCandCategory.this,
										"No Data", Toast.LENGTH_SHORT).show();
							}
						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}
					});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void getListCategory() {
		try {
			ModelManager.getListCategory(ActivityListTnCandCategory.this, true,
					new ModelManagerListener() {

						@Override
						public void onSuccess(Object object) {
							// TODO Auto-generated method stub
							String json = (String) object;
							GlobalValue.arrCategory = ParserUtility
									.getListCategory(json);
							if (GlobalValue.arrCategory.size() > 0) {
								adapterCategory = new AdapterCategory(
										ActivityListTnCandCategory.this,
										GlobalValue.arrCategory);
								adapterCategory.notifyDataSetChanged();
								list.setAdapter(adapterCategory);
							} else {
								Toast.makeText(ActivityListTnCandCategory.this,
										"No Data", Toast.LENGTH_SHORT).show();
							}
						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}
					});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
}
