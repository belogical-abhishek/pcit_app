package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.pcits.events.R;
import com.pcits.events.obj.TnCobj;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class TnCAdapter extends BaseAdapter {
	private Activity activity;
	// private ArrayList<HashMap<String, String>> data;
	private LayoutInflater inflater = null;
	private ArrayList<TnCobj> mArrTnC;

	public TnCAdapter(Activity a, ArrayList<TnCobj> arr) {
		activity = a;
		mArrTnC = arr;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	public int getCount() {
		// TODO Auto-generated method stub
		return mArrTnC.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_nice_tnc, null);
			holder = new ViewHolder();

			holder.lblTitle = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lblName);
			holder.horBar = convertView.findViewById(R.id.vw_hor_bar);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		TnCobj tnc = mArrTnC.get(position);
		if (tnc != null) {
			holder.lblTitle.setText(tnc.getTncDesc());

			// Hide last bar
			if (position == mArrTnC.size() - 1) {
				holder.horBar.setVisibility(View.GONE);
			} else {
				holder.horBar.setVisibility(View.VISIBLE);
			}
		}

		return convertView;
	}

	// Method to create instance of views
	static class ViewHolder {
		TextViewRobotoCondensedRegular lblTitle;
		View horBar;
	}
}
