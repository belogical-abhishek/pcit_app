package com.pcits.events;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.widgets.KenBurnsView;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class ActivityDetailLuckyDraw extends Activity {

	private ImageView qrImg;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;
	private TextView lblTitleHeader;
	private TextViewRobotoCondensedRegular lblDesc, lblPrize, lblTimestamp,
			lblCreateBy;
	private String url = "http://chart.apis.google.com/chart?cht=qr&chs=400x400&chld=M&choe=UTF-8&chl=";

	private AQuery mAq;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_detail_lucky_draw);
		initUI();
		qrImg = (ImageView) findViewById(R.id.imgBarcodeWinner);
		lblTitleHeader = (TextView) findViewById(R.id.lblTitleHeader);
		mAq = new AQuery(this);
		mAq.id(qrImg).image(
				url + " Id: " + GlobalValue.luckyDrawObj.getId() + "\n Type: "
						+ "winner" + "\n Event: "
						+ GlobalValue.luckyDrawObj.getTitle()
						+ "\n By company: "
						+ GlobalValue.luckyDrawObj.getCompany() + "\n User: "
						+ GlobalValue.luckyDrawObj.getClient_id() + "\n Code: "
						+ GlobalValue.luckyDrawObj.getUniquecode());

		llMenu = (LinearLayout) findViewById(R.id.llMenu);
		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		initNotBoringActionBar();

	}

	private void initNotBoringActionBar() {
		mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}

	private void initUI() {
		lblPrize = (TextViewRobotoCondensedRegular) findViewById(R.id.lblPrize);
		lblTimestamp = (TextViewRobotoCondensedRegular) findViewById(R.id.lblTimestamp);
		lblCreateBy = (TextViewRobotoCondensedRegular) findViewById(R.id.lblcreateBy);
	}

	private void initControl() {

	}
}
