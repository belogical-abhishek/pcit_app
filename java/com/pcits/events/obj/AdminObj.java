package com.pcits.events.obj;

public class AdminObj {

	private String userName, avatar, fullName, email, phone, country;
	private boolean isActive;

	public AdminObj() {
		// TODO Auto-generated constructor stub
	}

	public AdminObj(String userName, String avatar, String fullName,
			String email, String phone, String country, boolean isActive) {
		this.userName = userName;
		this.avatar = avatar;
		this.fullName = fullName;
		this.email = email;
		this.phone = phone;
		this.country = country;
		this.isActive = isActive;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
