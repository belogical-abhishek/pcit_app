package com.pcits.events;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.adapters.AdapterLuckyDraw;
import com.pcits.events.adapters.ScanTicketPage2Adapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.MessageObj;
import com.pcits.events.widgets.FloatLabeledEditText;
import com.pcits.events.widgets.KenBurnsView;

public class ListAttendOfEventActivity extends FragmentActivity {

	private String mDealId;
	private ListView lsvAttendOfEvent;
	private Button btnChooseWinner;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;
	private TextView lblTitleHeader, btnListWinner;
	private ScanTicketPage2Adapter adapterTicket;
	private int type;
	private FloatLabeledEditText txtNamePrize, txtValuePrize, txtCollect;
	private RadioGroup gpRadioWiner;
	private RadioButton radMoreWinner;
	private ArrayList<MessageObj> arrLucky;
	private AdapterLuckyDraw adapterLucky;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try{
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			setContentView(R.layout.layout_attendee_of_event);
	
			Intent i = getIntent();
			mDealId = i.getStringExtra("deal_id");
			// getCheckAttding();
			initUI();
			initControl();
			initNotBoringActionBar();
			getData();
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void initNotBoringActionBar() {
		mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}

	private void initUI() {
		lblTitleHeader = (TextView) findViewById(R.id.lblTitleHeader);
		lblTitleHeader.setText("Tickets");
		lsvAttendOfEvent = (ListView) findViewById(R.id.lsvAttendOfEvent);
		// lsvLuckyDraw = (ListView) findViewById(R.id.lsvLuckyDraw);
		llMenu = (LinearLayout) findViewById(R.id.llMenu);
		btnChooseWinner = (Button) findViewById(R.id.btnChooseWinner);
		txtNamePrize = (FloatLabeledEditText) findViewById(R.id.txtNamePrize);
		txtValuePrize = (FloatLabeledEditText) findViewById(R.id.txtValuePrize);
		txtCollect = (FloatLabeledEditText) findViewById(R.id.txtCollect);
		gpRadioWiner = (RadioGroup) findViewById(R.id.gpRadioWiner);
		radMoreWinner = (RadioButton) findViewById(R.id.radMoreWinner);
		btnListWinner = (TextView) findViewById(R.id.btnListWinner);
		// if (GlobalValue.arrMessage.size() > 0) {
		// gpRadioWiner.setVisibility(View.VISIBLE);
		// }
	}

	private void getCheckAttding() {
		try{
		ModelManager.getCheckLucky(this, mDealId, false,
				new ModelManagerListener() {

					@Override
					public void onSuccess(Object object) {
						// TODO Auto-generated method stub
						String json = (String) object;
						GlobalValue.arrMessage = ParserUtility
								.getListLuckyDrawOfEvent(json);
						if (GlobalValue.arrMessage.size() > 0) {
							gpRadioWiner.setVisibility(View.VISIBLE);
						}
					}

					@Override
					public void onError() {
						// TODO Auto-generated method stub

					}
				});
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	private void initControl() {
		try{
			if (radMoreWinner.isChecked()) {
				type = 1;
			} else if (!radMoreWinner.isChecked()) {
				type = 0;
			}
	
			llMenu.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});
			btnListWinner.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(ListAttendOfEventActivity.this,
							ActivityListLuckyDraw.class);
					i.putExtra("deal_id", mDealId);
					startActivity(i);
				}
			});
			// choose winner
			btnChooseWinner.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String mPrize = txtNamePrize.getText().toString();
					String mValue = txtValuePrize.getText().toString();
					String mCollect = txtCollect.getText().toString();
					String note = mPrize + ", " +  mCollect;
					ModelManager.pushWinner(ListAttendOfEventActivity.this,
							mDealId, note, mValue, type, true,
							new ModelManagerListener() {
	
								@Override
								public void onSuccess(Object object) {
									try{
										// TODO Auto-generated method stub
										String strJson = (String) object;
										Toast.makeText(ListAttendOfEventActivity.this,
												checkResult(strJson),
												Toast.LENGTH_SHORT).show();
		
										getData();
										// Intent i = new Intent(
										// ListAttendOfEventActivity.this,
										// ActivityListLuckyDraw.class);
										// i.putExtra("deal_id", mDealId);
										// startActivity(i);
									} catch (Exception ex) {
						                throw new RuntimeException(ex);
						            }
								}
	
								@Override
								public void onError() {
									// TODO Auto-generated method stub
	
								}
							});
				}
			});
			//
			// lsvAttendOfEvent.setOnItemClickListener(new OnItemClickListener() {
			//
			// @Override
			// public void onItemClick(AdapterView<?> parent, View view,
			// int position, long id) {
			// // TODO Auto-generated method stub`
			// GlobalValue.ticket = GlobalValue.arrTickets.get(position);
			// Intent i = new Intent(ListAttendOfEventActivity.this,
			// DetailClientTicketActivity.class);
			// startActivity(i);
			// }
			// });
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	private void getData() {
		try{
			// type = 2;
			// ModelManager.getListTickets(ListAttendOfEventActivity.this,
			// Integer.toString(type), mDealId, true,
			// new ModelManagerListener() {
			//
			// @Override
			// public void onSuccess(Object object) {
			// // TODO Auto-generated method stub
			// String json = (String) object;
			// GlobalValue.arrTickets = ParserUtility
			// .getListTickets(json);
			// if (GlobalValue.arrTickets.size() > 0) {
			// adapterTicket = new ScanTicketPage2Adapter(
			// ListAttendOfEventActivity.this,
			// GlobalValue.arrTickets);
			// adapterTicket.notifyDataSetChanged();
			// lsvAttendOfEvent.setAdapter(adapterTicket);
			// } else {
			// Toast.makeText(ListAttendOfEventActivity.this,
			// "No Data", Toast.LENGTH_SHORT).show();
			// }
			// }
			//
			// @Override
			// public void onError() {
			// // TODO Auto-generated method stub
			//
			// }
			// });
			ModelManager.getListLuckyDraw(this, mDealId, true,
					new ModelManagerListener() {
		
						@Override
						public void onSuccess(Object object) {
							try{
								// TODO Auto-generated method stub
								String json = (String) object;
								Log.e("ListAttendOfEvent", "Result LuckyDraw: " + json);
								arrLucky = ParserUtility.getListLuckyDrawOfEvent(json);
								if (arrLucky.size() > 0) {
									adapterLucky = new AdapterLuckyDraw(
											ListAttendOfEventActivity.this, arrLucky);
									adapterLucky.notifyDataSetChanged();
									lsvAttendOfEvent.setAdapter(adapterLucky);
									gpRadioWiner.setVisibility(View.VISIBLE);
								} else {
									Toast.makeText(ListAttendOfEventActivity.this,
											"No Data", Toast.LENGTH_SHORT).show();
								}
							} catch (Exception ex) {
				                throw new RuntimeException(ex);
				            }
						}
	
						@Override
						public void onError() {
							// TODO Auto-generated method stub
	
						}
					});
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	protected String checkResult(String strJson) {
		// TODO Auto-generated method stub
		JSONObject json = null;
		String message = "";
		try {
			json = new JSONObject(strJson);
			if (json.getString("status").equals("success")) {
				message = this.getString(R.string.message_success);
			} else {
				message = json.getString("message");
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return message;
	}
}
