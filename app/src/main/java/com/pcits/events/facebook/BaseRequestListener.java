package com.pcits.events.facebook;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import android.util.Log;

import com.pcits.events.facebook.AsyncFacebookRunner.RequestListener;

/**
 * Skeleton base class for RequestListeners, providing default error handling.
 * Applications should handle these error conditions.
 * 
 */
public abstract class BaseRequestListener implements RequestListener {

	public void onFacebookError(FacebookError e) {
		Log.e("Facebook", e.getMessage());
		throw new RuntimeException(e);
	}

	public void onFileNotFoundException(FileNotFoundException e) {
		Log.e("Facebook", e.getMessage());
		throw new RuntimeException(e);
	}

	public void onIOException(IOException e) {
		Log.e("Facebook", e.getMessage());
		throw new RuntimeException(e);
	}

	public void onMalformedURLException(MalformedURLException e) {
		Log.e("Facebook", e.getMessage());
		throw new RuntimeException(e);
	}

}
