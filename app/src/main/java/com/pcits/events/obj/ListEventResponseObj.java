package com.pcits.events.obj;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SAKSHI on 9/28/2017.
 */

public class ListEventResponseObj {


    @SerializedName("data")
    @Expose
    private List<ListEventObj> data = null;

    public List<ListEventObj> getData() {
        return data;
    }

    public void setData(List<ListEventObj> data) {
        this.data = data;
    }



}
