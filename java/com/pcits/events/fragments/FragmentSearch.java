package com.pcits.events.fragments;

import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.events.R;
import com.pcits.events.adapters.HomeAdapter;
import com.pcits.events.libraries.JSONParser;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.obj.DealObj;

public class FragmentSearch extends Fragment implements OnClickListener {

	// Create interface for MapsListFragment
	private OnListSelectedListener mCallback;

	// ArrayList<HashMap<String, String>> menuItems;
	private ArrayList<DealObj> mArrDeals;
	private ProgressDialog pDialog;

	// Declare object of userFunctions class
	private UserFunctions userFunction;

	// Create instance of list and ListAdapter
	private ListView list;
	private HomeAdapter sla;
	private LinearLayout lytRetry;

	private Button btnLoadMore;
	private ImageButton btnSearch;
	private EditText txtSearch;
	private TextView lblNoResult;

	// Flag for current page
	private JSONObject json;
	private int mCurrentPage = 0;
	private int mPreviousPage;

	private String mSearch;
	private int intLengthData;

	// Declare OnListSelected interface
	public interface OnListSelectedListener {
		public void onListSelected(String idSelected);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View v = inflater.inflate(R.layout.fragment_search, container, false);

		if (mArrDeals == null) {
			mArrDeals = new ArrayList<DealObj>();
		}

		list = (ListView) v.findViewById(R.id.list);
		lblNoResult = (TextView) v.findViewById(R.id.lblNoResult);
		txtSearch = (EditText) v.findViewById(R.id.txtSearch);
		btnSearch = (ImageButton) v.findViewById(R.id.btnSearch);
		lytRetry = (LinearLayout) v.findViewById(R.id.lytRetry);

		// Set text lblNoResult
		lblNoResult.setText(getString(R.string.lbl_result));

		// Declare object of userFunctions class
		userFunction = new UserFunctions();

		// Create LoadMore button
		btnLoadMore = new Button(getActivity());
		btnLoadMore
				.setBackgroundResource(R.drawable.apptheme_btn_default_holo_light);
		btnLoadMore.setText(getString(R.string.btn_load_more));
		btnLoadMore.setTextColor(getResources().getColor(R.color.text_btn));

		btnSearch.setOnClickListener(this);

		// Listener to handle load more buttton when clicked
		btnLoadMore.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Starting a new async task
				json = null;
				new loadMoreListView().execute();
			}
		});

		// Listener to get selected id when list item clicked
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				// Pass id to onListSelected method on HomeActivity
				mCallback.onListSelected(mArrDeals.get(position).getDeal_id());

				// Set the item as checked to be highlighted when in two-pane
				// layout
				list.setItemChecked(position, true);
			}
		});

		return v;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// The callback interface. If not, it throws an exception.
		try {
			mCallback = (OnListSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}
	}

	// Load first 10 videos
	private class loadFirstListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Showing progress dialog before sending http request
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please wait..");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected Void doInBackground(Void... unused) {
			// Call method getDataFromServer
			getDataFromServer();
			return (null);
		}

		protected void onPostExecute(Void unused) {
			// Condition if data length uder 10 button loadMore is remove
			if (intLengthData < userFunction.valueItemsPerPage) {
				list.removeFooterView(btnLoadMore);
			} else {
				list.addFooterView(btnLoadMore);
			}

			if (isAdded()) {
				if (mArrDeals.size() != 0) {

					// Adding load more button to lisview at bottom
					lytRetry.setVisibility(View.GONE);
					list.setVisibility(View.VISIBLE);
					// Getting adapter
					sla = new HomeAdapter(getActivity(), mArrDeals);
					list.setAdapter(sla);

				} else {
					list.removeFooterView(btnLoadMore);
					if (json != null) {
						lblNoResult.setVisibility(View.VISIBLE);
						lblNoResult.setText(getString(R.string.lbl_no_result));
						lytRetry.setVisibility(View.GONE);

					} else {
						lblNoResult.setVisibility(View.GONE);
						lytRetry.setVisibility(View.VISIBLE);
						Toast.makeText(getActivity(),
								getString(R.string.no_connection),
								Toast.LENGTH_SHORT).show();
					}
				}
			}
			// Closing progress dialog
			pDialog.dismiss();

		}
	}

	// Load more videos
	private class loadMoreListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Showing progress dialog before sending http request
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please wait..");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected Void doInBackground(Void... unused) {

			// Store previous value of current page
			mPreviousPage = mCurrentPage;
			// Increment current page
			mCurrentPage += userFunction.valueItemsPerPage;
			getDataFromServer();
			return (null);
		}

		protected void onPostExecute(Void unused) {
			// Condition if data length uder 10 button loadMore is remove
			if (intLengthData < userFunction.valueItemsPerPage)
				list.removeFooterView(btnLoadMore);

			if (json != null) {
				if (mArrDeals != null) {
					// Get listview current position - used to maintain scroll
					// position
					int currentPosition = list.getFirstVisiblePosition();

					lytRetry.setVisibility(View.GONE);
					// Appending new data to menuItems ArrayList
					sla = new HomeAdapter(getActivity(), mArrDeals);
					list.setAdapter(sla);
					// Setting new scroll position
					list.setSelectionFromTop(currentPosition + 1, 0);
				} else {
					list.removeFooterView(btnLoadMore);
				}
			} else {
				Log.d("json", "json not null");
				if (mArrDeals != null) {
					Log.d("menuItems", "menuItems not null");
					mCurrentPage = mPreviousPage;
					lytRetry.setVisibility(View.GONE);
				} else {
					Log.d("menuItems", "menuItems null");
					lytRetry.setVisibility(View.VISIBLE);
				}
				Toast.makeText(getActivity(),
						getString(R.string.no_connection), Toast.LENGTH_SHORT)
						.show();
			}
			// Closing progress dialog
			pDialog.dismiss();
		}
	}

	// Method get data from server
	public void getDataFromServer() {

		mSearch = mSearch.replace(" ", "%20");
		json = userFunction.searchByName(mSearch, mCurrentPage);
		mSearch = mSearch.replace("%20", " ");
		if (json != null) {
			// JSONArray dataDealsArray = json
			// .getJSONArray(userFunction.array_place_by_search);
			ArrayList<DealObj> deals = JSONParser.parserDeal(
					userFunction.array_place_by_search, json.toString());
			mArrDeals.addAll(deals);
			intLengthData = mArrDeals.size();
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnSearch:
			mSearch = txtSearch.getText().toString().trim();
			if (mSearch.length() > 0) {
				lblNoResult.setVisibility(View.GONE);
				list.setVisibility(View.VISIBLE);

				json = null;
				mArrDeals.clear();
				list.setAdapter(null);

				new loadFirstListView().execute();
			}
			break;

		case R.id.btnRetry:
			json = null;
			// Adding load more button to lisview at bottom
			new loadFirstListView().execute();
			break;
		default:
			break;
		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}

}
