package com.pcits.events;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.CalendarContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.BitmapAjaxCallback;
import com.facebook.common.util.UriUtil;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.reflect.TypeToken;
import com.pcits.common.utils.GsonUtility;
import com.pcits.events.adapters.ListEventsAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.fadingactionbar.FadingActionBarHelper;
import com.pcits.events.fragments.FragmentLogin;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.AcceptDeal;
import com.pcits.events.modelmanager.AddToGroup;
import com.pcits.events.modelmanager.ClientToAdd;
import com.pcits.events.network.EventsAPI;
import com.pcits.events.network.RestClient;
import com.pcits.events.obj.CreateEventObj;
import com.pcits.events.obj.DealObj;
import com.pcits.events.obj.ListEventObj;
import com.pcits.events.utils.AppConstants;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivityPrivateEventDetails extends AppCompatActivity implements View.OnClickListener {
    private TextView lblCompany, lblTitle, lblAddress, lblAfterDiscount,
            lblStartValue, lblDiscount, lblSave, lblAttending;
    private ListEventObj event;
    private LinearLayout mLlAdminButtons;
    private ProgressDialog mProgressDialog;


    private TextViewRobotoCondensedRegular lblDateStart, lblDateEnd,
            lblTimeStart, lblTimeEnd, lblDesc;
    private SimpleDraweeView imgThumbnail;
    private ImageView defaultView;
    private LinearLayout mLlBottom;

    private LinearLayout lytDetail, lytRetry;
    // private RelativeLayout layUser;
    private Button btnRetry, btnGet,btnRejectEvent, btnGoogleCal, btnWinner,btnAddUser;
    String title, description, startTime, endTime, startDate, endDate,strDate;

    private Display display;
    private int mImgHeight = 0;
    private String mGetDealId;
    private Float mDblLatitude;
    private Float mDblLongitude;
    private String mTitle;

    String extraString = "";
    String extraFromString = "";
    String extraDealIdString = "";
    String extraEmail = "";
    String extraName = "";


    private SimpleDateFormat sdf, sdf1;
    private Date da;
    private String TAG = "ActivityPrivateEventDetails";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
     //   setTheme(SampleList.THEME);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail_collapse);
        try {
          /*  FadingActionBarHelper helper = new FadingActionBarHelper()
                    .actionBarBackground(R.color.primary_color)
                    .headerLayout(R.layout.layout_header_detail)
                    .contentLayout(R.layout.activity_private_event_details)
                    .onHeaderClickListener(headerListener);

            setContentView(helper.createView(this));
            helper.initActionBar(this);
*/
            display = this.getWindowManager().getDefaultDisplay();
            mImgHeight = display.getWidth() * 9 / 16;

            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            strDate = sdf.format(c.getTime());

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setLogo(R.drawable.eventslogo);
            getSupportActionBar().setDisplayUseLogoEnabled(true);


            InitUI();


            try {
                if (savedInstanceState == null) {
                    Bundle extras = getIntent().getExtras();
                    if (extras != null) {
                        extraString = extras.getString("obj");
                        extraFromString = extras.getString("from");
                        extraDealIdString = extras.getString("dealid");
                        extraEmail = extras.getString("email");
                        extraName = extras.getString("name");
                    }
                } else {
                    extraString = (String) savedInstanceState.getSerializable("obj");
                    extraFromString = (String) savedInstanceState.getSerializable("from");
                    extraDealIdString = (String) savedInstanceState.getSerializable("dealid");
                    extraEmail = (String) savedInstanceState.getSerializable("email");
                    extraName = (String) savedInstanceState.getSerializable("name");
                }

                TypeToken<ListEventObj> token = new TypeToken<ListEventObj>() {};
              //  Log.d(TAG, "onCreate: from  "+extraFromString+" dealid "+extraDealIdString);


                if (extraString != "" && extraFromString.equalsIgnoreCase("private")) {

                    event = (ListEventObj) GsonUtility.convertJSONStringToObject(extraString, token);
                    setGlobalValue(event);
                    getData();

                }
                if (extraString != "" && extraFromString.equalsIgnoreCase("new")) {

                    //setGlobalValue(event);
                    getData();

                }
                //deep_link
                else if(extraString != "" && extraFromString.equalsIgnoreCase("deep_link"))
                {
                    btnGet.setText("Accept Invite");
                    btnRejectEvent.setVisibility(View.VISIBLE);
                    getEventFromServer(extraDealIdString);
                    addToGroup(extraName,extraEmail,extraDealIdString);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setGlobalValue(ListEventObj event) {

        GlobalValue.dealsObj = new DealObj();
        GlobalValue.dealsObj.setCategory_id(event.getCategoryId());
        GlobalValue.dealsObj.setTnc_id(event.getTncId());
        GlobalValue.dealsObj.setCompany(event.getCompany());
        GlobalValue.dealsObj.setTitle(event.getTitle());
        GlobalValue.dealsObj.setAddress(event.getCompany());
        GlobalValue.dealsObj.setAfter_discount_value(Double.parseDouble(""+event.getAfterDiscountValue()));
        if(event.getViewattendee()!=null)
            GlobalValue.dealsObj.setAttend(Integer.parseInt(event.getViewattendee()));
        else
            GlobalValue.dealsObj.setAttend(0);


        GlobalValue.dealsObj.setStart_value(Double.parseDouble(""+event.getStartValue()));
        GlobalValue.dealsObj.setStart_date(event.getStartDate());
        GlobalValue.dealsObj.setEnd_date(event.getEndDate());
        GlobalValue.dealsObj.setStart_time(event.getStartTimestamp());
        GlobalValue.dealsObj.setEnd_time(event.getEndTimestamp());
        GlobalValue.dealsObj.setDescription(event.getDescription());
        GlobalValue.dealsObj.setUsername(event.getUsername());
    }

    private class getDeal extends AsyncTask<String,ListEventObj,String>{
        EventsAPI eventsApiObj;
        ListEventObj deal = new ListEventObj();
        String dealId;

        public getDeal(String deal) {
            super();
            this.dealId = deal;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Retrofit retrofit = RestClient.retrofitService();
            eventsApiObj = retrofit.create(EventsAPI.class);
            mProgressDialog.setMessage("Getting Event details");
            mProgressDialog.show();

        }


        @Override
        protected String doInBackground(String... strings) {
            try {
                Response<List<ListEventObj>> dealResponse= eventsApiObj.getEventById("http://54.173.65.38/events-api/public/events/private?deal_id="+dealId).execute();
                if(dealResponse.isSuccessful()){
                    for(ListEventObj obj : dealResponse.body()) {
                        if(obj.getDealId().equals(dealId))
                        deal = obj;
                    }

                }else {
                    deal = null;
                }



            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }



        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(deal!=null){
                event = deal;
                setGlobalValue(deal);
                getData();
            }
            mProgressDialog.dismiss();
        }
    }



    private void getEventFromServer(String dealId){

        new getDeal(dealId).execute();
    }


    private void InitUI() {
        mProgressDialog =new ProgressDialog(ActivityPrivateEventDetails.this);
        lblAttending = (TextView) findViewById(R.id.lblAttend);
        lblCompany = (TextView) findViewById(R.id.lblCompany);
        lblTitle = (TextView) findViewById(R.id.lblTitle);
        lblTitle.setSelected(true);
        lblAddress = (TextView) findViewById(R.id.lblAddress);
        lblAfterDiscount = (TextView) findViewById(R.id.lblAfterDiscountValue);
        lblStartValue = (TextView) findViewById(R.id.lblStartValue);
        lblDiscount = (TextView) findViewById(R.id.lblDiscountValue);
        // lblSave = (TextView) findViewById(R.id.lblSaveValue);
        lblDateStart = (TextViewRobotoCondensedRegular) findViewById(R.id.lblStartDateValue);
        lblDateEnd = (TextViewRobotoCondensedRegular) findViewById(R.id.lblEndDateValue);
        lblTimeStart = (TextViewRobotoCondensedRegular) findViewById(R.id.lblStartTimeValue);
        lblTimeEnd = (TextViewRobotoCondensedRegular) findViewById(R.id.lblEndTimeValue);
        lytRetry = (LinearLayout) findViewById(R.id.lytRetry);
        lytDetail = (LinearLayout) findViewById(R.id.lytDetail);
        mLlBottom = (LinearLayout) findViewById(R.id.ll_bottom);
        btnRetry = (Button) findViewById(R.id.btnRetry);
        btnGet = (Button) findViewById(R.id.btnGet);
        btnRejectEvent=(Button)findViewById(R.id.btnRejectEvent);
        btnAddUser = (Button)findViewById(R.id.btnAddUser);

        btnGoogleCal = (Button) findViewById(R.id.btnGoogleCalendar);
        mLlAdminButtons = (LinearLayout) findViewById(R.id.ll_buttons);

        imgThumbnail = (SimpleDraweeView) findViewById(R.id.imgThumbnail);
        defaultView = (ImageView) findViewById(R.id.default_imgThumbnail);
        calcImageDimension();

        // adView = (AdView) this.findViewById(R.id.adView);
        lblDesc = (TextViewRobotoCondensedRegular) findViewById(R.id.lblDesc);

        btnRetry.setOnClickListener(this);

        btnGoogleCal.setOnClickListener(this);
        lblDesc.setOnClickListener(this);
        imgThumbnail.setOnClickListener(this);
        lblTitle.setOnClickListener(this);
    }

    public void getData() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String from = bundle.getString("from");
        if(from.equalsIgnoreCase("new")) {

            CreateEventObj newEvent = new CreateEventObj();
            TypeToken<CreateEventObj> token = new TypeToken<CreateEventObj>(){};
            newEvent = (CreateEventObj) GsonUtility.convertJSONStringToObject(bundle.getString("obj"),token);
            Log.d("image", "onClick: "+newEvent.getImage());
            Log.d("image", "onClick: "+bundle.getBoolean("isPreview", false));
            if (!bundle.getBoolean("isPreview", false)) {
                Log.d(TAG, "getData: in else ");

                //add here
                btnGet.setVisibility(View.GONE);
                btnAddUser.setVisibility(View.VISIBLE);

                btnAddUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addUsers();
                    }
                });
                mLlBottom.setVisibility(View.GONE);
                if (newEvent.getImage() != null && !newEvent.getImage().isEmpty() ) {


                    Uri uri = Uri.parse(newEvent.getImage());

                    imgThumbnail.setImageURI(uri);

                }else{
                    imgThumbnail.getHierarchy().setPlaceholderImage(R.drawable.ny);
                }
            }
            else {


                if (newEvent.getImage() != null && !newEvent.getImage().isEmpty()) {
                    File imgFile = new File(newEvent.getImage());

                    if (imgFile.exists()) {

                        Uri uri = Uri.fromFile(imgFile);
                        imgThumbnail.setImageURI(uri);
                    } else {
                        imgThumbnail.getHierarchy().setPlaceholderImage(R.drawable.ny);

                    }
                }

            }


            lytDetail.setVisibility(View.VISIBLE);
            btnGet.setVisibility(View.GONE);
            lytRetry.setVisibility(View.GONE);
            lblCompany.setText(newEvent.getCompany() + ",");
            lblTitle.setText(newEvent.getTitle());
            lblAddress.setText(newEvent.getAddress());
            mDblLatitude = newEvent.getLatitude();
            mDblLongitude = newEvent.getLongitude();

            if (newEvent.getAfterDiscountValue().equals("0")) {
                lblAfterDiscount.setText("Free");
            } else {
                lblAfterDiscount.setText(Utils.mCurrency + " "
                        + newEvent.getAfterDiscountValue());
            }
            lblAttending.setText("Attending: 0");
            lblStartValue.setText(Utils.mCurrency + " "
                    + newEvent.getStartValue());
            lblStartValue.setPaintFlags(lblStartValue.getPaintFlags()
                    | Paint.STRIKE_THRU_TEXT_FLAG);

            if(newEvent.getDiscount()==null || newEvent.getDiscount()==0) {

                    float discount = ((float)newEvent.getStartValue()-(float)newEvent.getAfterDiscountValue())/(float)newEvent.getStartValue();
                    Log.d("data", "getData: "+discount*100);
                    lblDiscount.setText("Discount "
                            +String.format("%.2f",  (discount*100) )+ "%");

            }
            else{
                lblDiscount.setText("Discount "
                        + newEvent.getDiscount() + "%");
            }

            // lblSave.setText(newEvent.getSave_value() + " "
            // + Utils.mCurrency);
            try {
                lblDateStart.setText(" " + DateTimeUtility.getDateFormat(newEvent.getStartDate(), "yyyy-MM-dd", "dd MMM yy"));
                lblDateEnd.setText(" " + DateTimeUtility.getDateFormat(newEvent.getEndDate(), "yyyy-MM-dd", "dd MMM yy"));

            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            lblTimeStart.setText(" "
                    + newEvent.getStartTimestamp());
            lblTimeEnd.setText("  " + newEvent.getEndTimestamp());
            // lblDesc.setMovementMethod(LinkMovementMethod.getInstance());

          //  lblDesc.setText(Html.fromHtml());
            setTextViewHTML(lblDesc,newEvent
                    .getDescription());
           // lblDesc.setMovementMethod(LinkMovementMethod.getInstance());
            // Load data from url




        }
else {


            if (GlobalValue.dealsObj != null) {
                // Hide bottom bar when previewing
                Intent i = getIntent();
                if (i.getBooleanExtra("isPreview", false)) {
                    mLlBottom.setVisibility(View.GONE);
                }

                // Display Data


                mDblLatitude = event.getLatitude();
                mDblLongitude = event.getLongitude();
                lytDetail.setVisibility(View.VISIBLE);
                lytRetry.setVisibility(View.GONE);
                lblCompany.setText(event.getCompany() + ",");
                lblTitle.setText(event.getTitle());
                lblAddress.setText(event.getAddress());

                if (event.getAfterDiscountValue().equals("0")) {
                    lblAfterDiscount.setText("Free");
                } else {
                    lblAfterDiscount.setText(Utils.mCurrency + " "
                            + event.getAfterDiscountValue());
                }
                lblAttending.setText("Attending: "
                        + event.getViewattendee());
                lblStartValue.setText(Utils.mCurrency + " "
                        + event.getStartValue());
                lblStartValue.setPaintFlags(lblStartValue.getPaintFlags()
                        | Paint.STRIKE_THRU_TEXT_FLAG);
                if(event.getDiscount()==null || event.getDiscount()==0) {
                    if(event.getAfterDiscountValue()!=0 && event.getSaveValue()!=0){
                        float discount = ((float)event.getStartValue()-(float)event.getAfterDiscountValue())/(float)event.getStartValue();
                        Log.d("data", "getData: "+discount*100);
                        lblDiscount.setText("Discount "
                                +String.format("%.2f",  (discount*100) )+ "%");
                    }
                }
                else{
                    lblDiscount.setText("Discount "
                            + event.getDiscount() + "%");
                }
                // lblSave.setText(event.getSave_value() + " "
                // + Utils.mCurrency);
                try {
                    lblDateStart.setText(" " + DateTimeUtility.getDateFormat(event.getStartDate(), "yyyy-MM-dd", "dd MMM yy"));
                    lblDateEnd.setText(" " + DateTimeUtility.getDateFormat(event.getEndDate(), "yyyy-MM-dd", "dd MMM yy"));

                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                lblTimeStart.setText(" "
                        + event.getStartTimestamp());
                lblTimeEnd.setText("  " + event.getEndTimestamp());
                // lblDesc.setMovementMethod(LinkMovementMethod.getInstance());
                setTextViewHTML(lblDesc,event.getDescription);
                // Load data from url

                // Set Image thumbnail from Server
                if (event.getImage() != null && !event.getImage().isEmpty() ) {


                    Uri uri = Uri.parse(event.getImage());

                    imgThumbnail.setImageURI(uri);

                }else{
                    imgThumbnail.getHierarchy().setPlaceholderImage(R.drawable.ny);
                }

                try {
                    DealObj dealObj = null;
                    if (DatabaseUtility.checkExistsDeals(ActivityPrivateEventDetails.this,
                            event.getDealId())) {
                        Log.e("", "DATA " + "data da co du lieu");
                    } else {
                        dealObj = new DealObj();
                        dealObj.setDeal_id(mGetDealId);
                        dealObj.setTitle(event.getTitle());
                        dealObj.setCompany(event.getCompany());
                        dealObj.setStart_date(event.getStartDate());
                        dealObj.setEnd_date(event.getEndDate());
                        dealObj.setStart_time(event.getStartTime());
                        dealObj.setEnd_time(event.getEndTime());
                        dealObj.setAfter_discount_value(Double.parseDouble("" + event.getAfterDiscountValue()));
                        dealObj.setStart_value(Double.parseDouble("" + event.getStartValue()));
                        if(event.getViewattendee()!=null)
                        dealObj.setAttend(Integer.parseInt(event.getViewattendee()));
                        else
                        dealObj.setAttend(0);

                        dealObj.setStartTimeStamp(event
                                .getStartTimestamp());
                        dealObj.setEndTimeStamp(event
                                .getEndTimestamp());
                        dealObj.setImage(event.getImage());

                        DatabaseUtility.insertDeals(ActivityPrivateEventDetails.this,
                                dealObj);
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                if (GlobalValue.myUser != null) {
                    if (Integer.parseInt(GlobalValue.myUser.getRole()) == 0) {
                        mLlAdminButtons.setVisibility(View.VISIBLE);
                        btnWinner.setVisibility(View.VISIBLE);
                        btnGet.setText(getResources().getString(R.string.edit));
                        btnGet.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
                                AddEventActivity.isDeal = true;
                                // AddEventActivity.dealObj =
                                // event;
                                Intent i = new Intent(ActivityPrivateEventDetails.this,
                                        AddEventActivity.class);
                                startActivity(i);
                                ActivityPrivateEventDetails.this.overridePendingTransition(
                                        R.anim.slide_in_left,
                                        R.anim.slide_out_left);

                            }
                        });
                    } else {
                        if (GlobalValue.myUser.getUsername().equalsIgnoreCase(
                                event.getUsername())) {
                            mLlAdminButtons.setVisibility(View.GONE);
                            btnWinner.setVisibility(View.VISIBLE);
                            btnGet.setText(getResources().getString(
                                    R.string.edit));
                            btnGet.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    AddEventActivity.isDeal = true;
                                    // AddEventActivity.dealObj =
                                    // event;
                                    Intent i = new Intent(ActivityPrivateEventDetails.this,
                                            AddEventActivity.class);
                                    startActivity(i);
                                    ActivityPrivateEventDetails.this
                                            .overridePendingTransition(
                                                    R.anim.slide_in_left,
                                                    R.anim.slide_out_left);
                                }
                            });

                        }
                    }
                } else {
                    mLlAdminButtons.setVisibility(View.GONE);
                    btnGet.setText(getResources().getString(R.string.get_it));
                    //btnRejectEvent.setVisibility(View.VISIBLE);
                    /*btnRejectEvent.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            rejectDeal();
                        }
                    });*/
                    btnGet.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Log.d(TAG, "onClick: user name "+GsonUtility.convertObjectToJSONString(GlobalValue.myClient));
                            if(GlobalValue.myClient==null){
                              //  Toast.makeText(ActivityPrivateEventDetails.this," You need to login first",Toast.LENGTH_LONG).show();
                                new AlertDialog.Builder(ActivityPrivateEventDetails.this)
                                        .setTitle(R.string.app_name)
                                        .setMessage(
                                                "You need login account 'Event Organiser' to create events..!")
                                        .setPositiveButton(android.R.string.ok,
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface arg0,
                                                                        int arg1) {
                                                        GlobalValue.myUser = null;
                                                        changeFragment(new FragmentLogin());
                                                    }
                                                })
                                        .setNegativeButton(android.R.string.cancel, null)
                                        .create().show();
                            }
                            else
                                {
                               if(extraFromString.equalsIgnoreCase("deep_link"))
                                acceptDeal();
                            }
                        }
                    });
                }

            } else {
                lytDetail.setVisibility(View.GONE);
                lytRetry.setVisibility(View.VISIBLE);
                Toast.makeText(ActivityPrivateEventDetails.this,
                        getString(R.string.no_connection), Toast.LENGTH_SHORT)
                        .show();

            }
        }

     //   mRltProgress.setVisibility(View.GONE);

    }
    private void changeFragment(Fragment targetFragment) {
        Bundle bundle = new Bundle();
        Utils utils = new Utils(ActivityPrivateEventDetails.this);
        bundle.putString(utils.EXTRA_ACTIVITY, utils.EXTRA_ACTIVITY__PRIVATE_DETAILS);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_content, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        targetFragment.setArguments(bundle);
    }


    private void addToGroup(String name,String extraEmail,String dealId) {
        ClientToAdd client = new ClientToAdd();
        client.setName(name);
        client.setEmail(extraEmail);

        List<ClientToAdd> list = new ArrayList<>();
        list.add(client);

        AddToGroup addToGroup = new AddToGroup();
        addToGroup.setClientToAddList(list);

        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsAPI = retrofit.create(EventsAPI.class);

        Call<ResponseBody> mAddToGroup = eventsAPI.addToGroup(AppConstants.BASEURL+"events/add-users/"+dealId,addToGroup);
        mAddToGroup.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "on add group Response: "+response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void addUsers() {
        Log.d("privateList", "addUsers: ");
        Intent intent = new Intent(ActivityPrivateEventDetails.this, ActivityListPrivateUsers.class);
        intent.putExtra("functionid", 1);
        intent.putExtra("dealid", extraDealIdString);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityPrivateEventDetails.this.startActivity(intent);
    }


    private void acceptDeal()
    {
        //event.getDealId()

        Log.d(TAG, "acceptDeal: "+GsonUtility.convertObjectToJSONString(event));
        AcceptDeal acceptDeal = new AcceptDeal();
       acceptDeal.setClientId(Integer.valueOf(GlobalValue.myClient.getId()));
       acceptDeal.setDealId(""+event.getDealId());

        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsAPI = retrofit.create(EventsAPI.class);

        Call<ResponseBody> mAcceptInvite = eventsAPI.acceptInvite(AppConstants.BASEURL+"/events/accept-invite",acceptDeal);
        mAcceptInvite.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "on accept Response: "+response.body());
                if(response.isSuccessful()) {
                    Toast.makeText(ActivityPrivateEventDetails.this, "Added to event", Toast.LENGTH_LONG).show();

                    finish();
                }
                else {
                    Toast.makeText(ActivityPrivateEventDetails.this, "Something went wrong", Toast.LENGTH_LONG).show();
                    finish();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(ActivityPrivateEventDetails.this, "Something went wrong", Toast.LENGTH_LONG).show();
                finish();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar_detail, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        try {
            if (v == btnGoogleCal) {

                setCalender();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setCalender() {
        try {

                    /*new code*/
            String startEvent = lblDateStart.getText().toString().trim();
            String endEvent = lblDateEnd.getText().toString().trim();
            String start = ""+startEvent+""+lblTimeStart.getText().toString();
            String end = ""+endEvent+""+lblTimeEnd.getText().toString();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yy HH:mm");
            long startMilliseconds=0, endMilliseconds=0;
            try {
                Date mstartDate = dateFormat.parse(start);
                Date mstopDate = dateFormat.parse(end);
                startMilliseconds = mstartDate.getTime();
                endMilliseconds = mstopDate.getTime();
                System.out.println("start Date in milli :: " + startMilliseconds);
                System.out.println("end Date in milli :: " + endMilliseconds);
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            Log.d("Google", "onClick: "+start);
            //  SimpleDateFormat sDateFormat = new SimpleDateFormat()
            Intent intent = new Intent(Intent.ACTION_EDIT);
            intent.setType("vnd.android.cursor.item/event");
            intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                    startMilliseconds);
            intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false);
            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                    endMilliseconds);
            intent.putExtra(CalendarContract.Events.TITLE, lblTitle
                    .getText().toString());
            intent.putExtra(CalendarContract.Events.DESCRIPTION, lblDesc.getText());
            intent.putExtra(CalendarContract.Events.EVENT_LOCATION,
                    lblAddress.getText().toString());
            startActivity(intent);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    private void showEventImage() {

    }

    View.OnClickListener headerListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            showEventImage();
        }
    };
    private void calcImageDimension() {
        imgThumbnail.getLayoutParams().height = mImgHeight;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i;
        switch (item.getItemId()) {
            case android.R.id.home:
                // Previous page or exit
                // i = new Intent(this, ActivityHome.class);
                // startActivity(i);
                GlobalValue.check = 0;
                finish();
                return true;
            case R.id.abDirection:
                // Call ActivityPlaceAroundYou
                // i = new Intent(this, ActivityDirection.class);
                // i.putExtra(utils.EXTRA_DEST_LAT, mDblLatitude);
                // i.putExtra(utils.EXTRA_DEST_LNG, mDblLongitude);
                // i.putExtra(utils.EXTRA_CATEGORY_MARKER, mIcMarker);
                // startActivity(i);






                String geoUri = "http://maps.google.com/maps?q=loc:" + mDblLatitude + "," + mDblLongitude + " (" + lblTitle.getText().toString() + ")";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
			/*final Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("geo:0,0?q="
							+ mDblLatitude + "," + mDblLongitude + "&z=16"));*/
                startActivity(intent);
                startActivity(intent);



                break;

            case R.id.abShare:
                // i = new Intent(this, ActivityShare.class);
                // i.putExtra(utils.EXTRA_DEAL_ID, mGetDealId);
                // i.putExtra(utils.EXTRA_DEAL_TITLE, mTitle);
                // i.putExtra(utils.EXTRA_DEAL_DESC, mDesc);
                // i.putExtra(utils.EXTRA_DEAL_IMG, mImgDeal);
                // startActivity(i);

                shareEvent();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
    private void shareEvent() {

        Intent iShare = new Intent(Intent.ACTION_SEND);
        iShare.setType("text/plain");
        iShare.putExtra(Intent.EXTRA_SUBJECT, title);
        iShare.putExtra(Intent.EXTRA_TEXT, "\n\nInstall application: "
                + "https://play.google.com/store/apps/details?id="
                + PacketUtility.getPacketName());
        startActivity(Intent.createChooser(iShare,
                getString(R.string.share_via)));
    }
    public void makeLinks(TextView textView, String[] links)
    {
        try
        {
            SpannableString spannableString = new SpannableString(textView.getText());
            for (int i = 0; i < links.length; i++)
            {
                final String link = links[i];

                int startIndexOfLink = textView.getText().toString().indexOf(link);
                int endingPosition = startIndexOfLink + link.length();

                ClickableSpan clickableSpan = new ClickableSpan()
                {
                    @Override
                    public void onClick(View textView)
                    {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                        startActivity(browserIntent);
                    }
                };
                spannableString.setSpan(clickableSpan, startIndexOfLink, endingPosition, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            textView.setHighlightColor(Color.TRANSPARENT); // prevent TextView change background when highlight
            textView.setMovementMethod(LinkMovementMethod.getInstance());
            textView.setText(spannableString, TextView.BufferType.SPANNABLE);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    protected void setTextViewHTML(TextView text, String html)
    {

        SpannableString spannableString = new SpannableString(html);
        Linkify.addLinks(spannableString, Linkify.ALL);
        URLSpan[] spans = spannableString.getSpans(0, spannableString.length() , URLSpan.class);
        String[] url=new String[spans.length];
        for(int i=0;i<spans.length;i++)
        {
            url[i]=""+spans[i].getURL();
        }
        text.setText(html);
        makeLinks(text, url);
    }
}
