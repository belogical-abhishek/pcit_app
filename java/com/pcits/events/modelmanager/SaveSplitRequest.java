package com.pcits.events.modelmanager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by EnvisioDevs on 24-11-2017.
 */

public class SaveSplitRequest {

    /*"clients":[{
		"email":"krupal@envisiodevs.com",
		"amount": 100},
		{
		"email":"ravina.buran@envisiodevs.com",
		"amount": 0},
		{
		"email":"manas.mehta@envisiodevs.com",
		"amount": 200}
		]*/

    @SerializedName("clients")
    ArrayList<SplitClients> splitlist;

    public ArrayList<SplitClients> getSplitlist() {
        return splitlist;
    }

    public void setSplitlist(ArrayList<SplitClients> splitlist) {
        this.splitlist = splitlist;
    }
}
