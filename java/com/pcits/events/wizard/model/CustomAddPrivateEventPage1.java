/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pcits.events.wizard.model;

import android.support.v4.app.Fragment;

import com.pcits.common.utils.Logging;
import com.pcits.events.fragments.AddEventPage1Fragment;
import com.pcits.events.fragments.AddPrivateEventPage1Fragment;

import java.util.ArrayList;

/**
 * A page asking for a name and an email.
 */
public class CustomAddPrivateEventPage1 extends Page implements ICustomPage {
	public static final String TITLE_DATA_KEY = "title";
	public static final String COMPANY_DATA_KEY = "company";
	public static final String CATEGORY_DATA_KEY = "category";
	public static final String IMAGE_DATA_KEY = "image";

	private AddPrivateEventPage1Fragment mFragment;

	public CustomAddPrivateEventPage1(ModelCallbacks callbacks, String title) {
		super(callbacks, title);
	}

	@Override
	public Fragment createFragment() {
		mFragment = AddPrivateEventPage1Fragment.create(getKey());
		return mFragment;
	}

	@Override
	public void getReviewItems(ArrayList<ReviewItem> dest) {
//		 dest.add(new ReviewItem("Title", mData.getString(TITLE_DATA_KEY),
//		 getKey(), -1));
//		 dest.add(new ReviewItem("Company", mData.getString(COMPANY_DATA_KEY),
//		 getKey(), -1));
//		 dest.add(new ReviewItem("Category",
//		 mData.getString(CATEGORY_DATA_KEY),
//		 getKey(), -1));
//		 dest.add(new ReviewItem("Image", mData.getString(IMAGE_DATA_KEY),
//		 getKey(), -1));
    }

    @Override
    public boolean isCompleted() {
        // boolean completed = false;
        // if (TextUtils.isEmpty(GlobalValue.dealsObj.getTitle())
        // || TextUtils.isEmpty(GlobalValue.dealsObj.getCategory_id())
        // || AddEventActivity.eventCroppedImage == null) {
        // completed = false;
        // } else {
        // completed = true;
        // }
        return true;
    }

    @Override
    public void saveState() {
        // TODO Auto-generated method stub
        try {
            mFragment.saveState();
        } catch (Exception ex) {
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
    }

    @Override
    public void restoreState() {
        try {
            // TODO Auto-generated method stub
            mFragment.restoreState();
        } catch (Exception ex) {
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
    }

}
