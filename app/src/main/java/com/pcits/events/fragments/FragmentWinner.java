package com.pcits.events.fragments;

import java.util.Collections;
import java.util.Comparator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityDetailTickets;
import com.pcits.events.R;
import com.pcits.events.adapters.MessageAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.MessageObj;
import com.pcits.events.stickylist.StickyListHeadersListView;

public class FragmentWinner extends Fragment implements
		OnClickListener, AdapterView.OnItemClickListener {

	private View v;
	// Declare sticky list header
	private StickyListHeadersListView mStickyList;
	private MessageAdapter mStickyAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		v = inflater.inflate(R.layout.fragment_winner, container, false);
		try{
			initUi();
			setData();
			initControl();
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return v;
	}


	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void initUi() {
		mStickyList = (StickyListHeadersListView) v
				.findViewById(R.id.lvMessage);
	}

	private void initControl() {

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		GlobalValue.msg = GlobalValue.arrMessage.get(position);
		Intent i = new Intent(getActivity(), ActivityDetailTickets.class);
		startActivity(i);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	private void setData() {
		try{
			if (GlobalValue.myClient != null) {

				ModelManager.getListMessage(getActivity(),
					GlobalValue.myClient.getUsername(), true,
					new ModelManagerListener() {

						@Override
						public void onSuccess(Object object) {
							// TODO Auto-generated method stub
							String json = (String) object;
							GlobalValue.arrMessage = ParserUtility
									.getListMessage(json);
							if (GlobalValue.arrMessage.size() > 0) {
								// Sort by status
								Collections.sort(GlobalValue.arrMessage,
										new Comparator<MessageObj>() {

											@Override
											public int compare(MessageObj lhs,
													MessageObj rhs) {
												// TODO Auto-generated method
												// stub
												Integer stt1 = Integer
														.valueOf(lhs
																.getStatus());
												Integer stt2 = Integer
														.valueOf(rhs
																.getStatus());
												return stt1.compareTo(stt2);
											}
										});

								mStickyAdapter = new MessageAdapter(
										getActivity(), GlobalValue.arrMessage);
								mStickyList.setAdapter(mStickyAdapter);
							} else {
								Toast.makeText(getActivity(), "No Data",
										Toast.LENGTH_SHORT).show();
							}
						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}
					});
			}
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}
}
