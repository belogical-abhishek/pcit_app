package com.pcits.events;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.api.services.gmail.GmailScopes;
import com.pcits.common.utils.GsonUtility;
import com.pcits.events.adapters.MySimple2Adapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.modelmanager.AddToGroup;
import com.pcits.events.modelmanager.ClientToAdd;
import com.pcits.events.network.EventsAPI;
import com.pcits.events.network.RestClient;
import com.pcits.events.obj.AddUsersobj;
import com.pcits.events.obj.ListUsersObj;
import com.pcits.events.utils.AppConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivityListPrivateUsers extends AppCompatActivity implements View.OnClickListener{
    private ListView mListViewUsers;
    private String mDealId;
    private int mFunctionId;
    private static final int REQUEST_INVITE = 10;

    private LinearLayout layoutEmail,llMenu;
    private AutoCompleteTextView emailAutoComplete;
    private TextView lblTitleHeader;private EditText emails;
    private Button emailButton;
    ArrayList<String> emlRecs = new ArrayList<String>();
    ArrayList<String> emlRecsSelected = new ArrayList<String>();

    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;

    private String[] emailids=new String[]{};

    private static final String BUTTON_TEXT = "Call Gmail API";
    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = { GmailScopes.GMAIL_LABELS };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_add_users);
        showContacts();
        mListViewUsers = (ListView) findViewById(R.id.list_users);
        layoutEmail = (LinearLayout)findViewById(R.id.email_layout);
        llMenu=(LinearLayout)findViewById(R.id.llMenu);
        emails = (EditText) findViewById(R.id.emailEdit);
        lblTitleHeader=(TextView)findViewById(R.id.lblTitleHeader);
        emailButton = (Button)findViewById(R.id.email_btn);
        emailButton.setOnClickListener(this);
        emailAutoComplete=(AutoCompleteTextView)findViewById(R.id.emailAutoComplete) ;

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, emlRecs);
        emailAutoComplete.setThreshold(1);//will start working from first character
        emailAutoComplete.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
        emailAutoComplete.setTextColor(Color.BLACK);

        Intent intent = getIntent();
        mFunctionId = intent.getIntExtra("functionid", 0);
        mDealId = intent.getStringExtra("dealid");

        Log.d("List", "onCreate: "+mFunctionId+" "+mDealId);
        if (mFunctionId == 1)
            //onInviteClicked();
            //get list of senders
            lblTitleHeader.setText("Invite User");
            layoutEmail.setVisibility(View.VISIBLE);
            emailButton.setOnClickListener(this);

         if (mFunctionId == 2) {
            /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Users Attending");
            getSupportActionBar().setLogo(R.drawable.eventslogo);
            getSupportActionBar().setDisplayUseLogoEnabled(true);*/
            lblTitleHeader.setText("Users Attending");
            // layoutEmail.setVisibility(View.GONE);
            listusers(true);
        }

        llMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        emailAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick (AdapterView<?> parent, View view, int position, long id)
            {
                if(emails.getText().toString().equalsIgnoreCase(""))
                {
                    //emails.setText(emlRecs.get(position));
                    emails.setVisibility(View.VISIBLE);
                    String selected = (String) parent.getItemAtPosition(position);
                    emails.setText(selected);
                    //int pos = Arrays.asList(regions).indexOf(selected);
                }
                else
                {
                    String selected = (String) parent.getItemAtPosition(position);
                    emails.setText(emails.getText()+","+selected);
                }

                String selected = (String) parent.getItemAtPosition(position);
                int pos = emlRecs.indexOf(selected);
                emailAutoComplete.setText("");
                emlRecsSelected.add(emlRecs.get(pos));
                emlRecs.remove(pos);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ActivityListPrivateUsers.this, android.R.layout.select_dialog_item, emlRecs);
                emailAutoComplete.setAdapter(adapter);
            }
        });
    }

    private void getMailIds()
    {
        try
        {
            HashSet<String> emlRecsHS = new HashSet<String>();
            Context context = this;
            ContentResolver cr = context.getContentResolver();
            String[] PROJECTION = new String[] { ContactsContract.RawContacts._ID,
                    ContactsContract.Contacts.DISPLAY_NAME,
                    ContactsContract.Contacts.PHOTO_ID,
                    ContactsContract.CommonDataKinds.Email.DATA,
                    ContactsContract.CommonDataKinds.Photo.CONTACT_ID };
            String order = "CASE WHEN "
                    + ContactsContract.Contacts.DISPLAY_NAME
                    + " NOT LIKE '%@%' THEN 1 ELSE 2 END, "
                    + ContactsContract.Contacts.DISPLAY_NAME
                    + ", "
                    + ContactsContract.CommonDataKinds.Email.DATA
                    + " COLLATE NOCASE";
            String filter = ContactsContract.CommonDataKinds.Email.DATA + " NOT LIKE ''";
            Cursor cur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, filter, null, order);
            if (cur.moveToFirst()) {
                do {
                    // names comes in hand sometimes
                    String name = cur.getString(1);
                    String emlAddr = cur.getString(3);

                    // keep unique only
                    if (emlRecsHS.add(emlAddr.toLowerCase())) {
                        emlRecs.add(emlAddr);
                    }
                } while (cur.moveToNext());
            }

            cur.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void showContacts()
    {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 1);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
            getMailIds();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
               finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void inviteFiend(String email){

        if(email.contains(","))
        emailids=email.split(",");
        else
            emailids=new String[1];
            emailids[0]=email;
        String deepLink = "https://"+getString(R.string.app_invite_id)+".app.goo.gl/?link="+getString(R.string.invitation_deep_link)+mDealId+"&apn=com.pcits.events[&amv=14][&afl=www.google.com]";
        deepLink="https://play.google.com/store/apps/details?id=com.pcits.events";
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  ,emailids );
        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.invitation_message));
        i.putExtra(Intent.EXTRA_TEXT   , deepLink);
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
            //onInviteClicked
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ActivityListPrivateUsers.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void onInviteClicked()
    {
        Log.d("List", "onInviteClicked: ");

        /*Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                .setMessage(getString(R.string.invitation_message))
                .setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)+"-"+mDealId))
                .setCallToActionText(getString(R.string.invitation_cta))
                .build();
        //intent.putExtra(Intent.EXTRA_EMAIL  , new String[]{"ravina.buran@envisiodevs.com"});
        intent.putExtra(Intent.EXTRA_EMAIL  , GlobalValue.myClient.getEmail());

        startActivityForResult(intent, REQUEST_INVITE);*/
        String[] reciepients = emailids;
        Intent email = new Intent(android.content.Intent.ACTION_SEND);
        email.putExtra(android.content.Intent.EXTRA_EMAIL,reciepients);
        email.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.invitation_cta));
        email.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.invitation_message)+"\n"+Uri.parse(getString(R.string.invitation_deep_link))+mDealId);
        email.setType("text/plain");
        email.setType("message/rfc822");
        Intent intent =  Intent.createChooser(email, "Choose Email Client");
        //context.start(Intent.createChooser(email, "Choose Email.."));
        startActivityForResult(intent, REQUEST_INVITE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("List", "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);
        //Log.d("List", "onActivityResult:  , data: "+data.toString());



        /*Bundle bundle = data.getExtras();
        if (bundle != null) {
            Set<String> keys = bundle.keySet();
            Iterator<String> it = keys.iterator();
            Log.e("List","Dumping Intent start");
            while (it.hasNext()) {
                String key = it.next();
                Log.e("List","[" + key + "=" + bundle.get(key)+"]");
            }
            Log.e("List","Dumping Intent end");
        }*/
        if(resultCode == RESULT_OK){
            for(int i = 0;i<emailids.length;i++)
            {
                addToGroup("",emailids[i],mDealId);

            }
            finish();
            onBackPressed();
        }
        if (requestCode == REQUEST_INVITE)
        {
            try
            {
                if (resultCode == RESULT_OK) {
                    //  listusers();
                    // Get the invitation IDs of all sent messages

                    String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);

                    StringBuilder sb = new StringBuilder();
                    sb.append("Sent ").append(Integer.toString(ids.length)).append(" invitations: ");
                    for (String id : ids) sb.append("[").append(id).append("]");
                    Log.d("Listprivate ids ", sb.toString());
                    //addUsers(ids);
                    for (String id : ids) {
                        Log.d("Listprivate", "onActivityResult: sent invitation " + id);
                    }
                } else {
                    // Sending failed or it was canceled, show failure message to the user
                    // ...
                    Log.d("List", "onActivityResult: ");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            finish();
            onBackPressed();
        }
    }

    private void addToGroup(String name,String extraEmail,String dealId) {
        ClientToAdd client = new ClientToAdd();
        client.setName(name);
        client.setEmail(extraEmail);

        List<ClientToAdd> list = new ArrayList<>();
        list.add(client);

        AddToGroup addToGroup = new AddToGroup();
        addToGroup.setClientToAddList(list);

        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsAPI = retrofit.create(EventsAPI.class);

        Call<ResponseBody> mAddToGroup = eventsAPI.addToGroup(AppConstants.BASEURL+"events/add-users/"+dealId,addToGroup);
        mAddToGroup.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("group", "on add group Response: "+response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.getStackTrace();
            }
        });
    }
    public void listusers(final boolean viewForAttending) {
        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsAPI = retrofit.create(EventsAPI.class);

        final Call<List<ListUsersObj>> listCallobj = eventsAPI.getUsers(mDealId);
        listCallobj.enqueue(new Callback<List<ListUsersObj>>() {
            @Override
            public void onResponse(Call<List<ListUsersObj>> call, Response<List<ListUsersObj>> response) {
                if (response.isSuccessful()) {
                    Log.d("ListPrivate", "onResponse: ");
                    List<ListUsersObj> listUsers=response.body();

                    if(viewForAttending) {
                        List<String> listemails = new ArrayList<String>();
                        Log.d("ListPrivate", "onResponse: " + GsonUtility.convertObjectToJSONString(listUsers));

                        for (int i = 0; i < listUsers.size(); i++)
                        {
                            //if (listUsers.get(i).getStatus() == 1)
                                listemails.add(listUsers.get(i).getEmail());
                        }
                        if (!listUsers.isEmpty())
                        {
                            ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(ActivityListPrivateUsers.this, android.R.layout.simple_list_item_1, listemails);
                            mListViewUsers.setAdapter(mArrayAdapter);

                            // Bind to our new adapter.

                        } else if (listUsers.isEmpty()) {
                            Toast.makeText(ActivityListPrivateUsers.this, "No Users Added or waiting for users to accept the invite", Toast.LENGTH_SHORT).show();
                        }
                    }else {

                        mListViewUsers.setAdapter(new MySimple2Adapter(ActivityListPrivateUsers.this,listUsers));

                    }
                }
            }

            @Override
            public void onFailure(Call<List<ListUsersObj>> call, Throwable t) {

            }
        });


    }

    private void addUsers(String[] ids) {
        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsAPI = retrofit.create(EventsAPI.class);

        List<String> emails = Arrays.asList(ids);

        AddUsersobj addUsersobj = new AddUsersobj();
        addUsersobj.setEmails(emails);

        Call<String> adduserscall = eventsAPI.addUser(addUsersobj, mDealId);
        adduserscall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {


                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });

    }

    @Override
    public void onClick(View view) {
        if(view==emailButton){
            Log.d("list", "onClick: ");
            if(!emails.getText().toString().isEmpty()){
                //inviteFiend(emails.getText().toString());
                String email=emails.getText().toString();
                if(email.contains(",")) {
                    emailids = email.split(",");
                }
                else
                    {
                        emailids=new String[1];
                        emailids[0]=email;
                }
                onInviteClicked();
            }
        }
    }
}
