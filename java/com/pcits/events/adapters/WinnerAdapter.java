package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androidquery.AQuery;
import com.pcits.events.R;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.obj.WinnerObj;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.widgets.CircleImageWithBorder;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedBold;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class WinnerAdapter extends BaseAdapter {

	private ArrayList<WinnerObj> arrWinner;
	private LayoutInflater mInflate;
	private AQuery mAq;

	public WinnerAdapter(Activity activity, ArrayList<WinnerObj> arr) {
		this.arrWinner = arr;
		this.mInflate = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mAq = new AQuery(activity);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrWinner.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {
		// TODO Auto-generated method stub
		final HolderView holder;
		if (convertView == null) {
			holder = new HolderView();
			convertView = mInflate.inflate(R.layout.item_winner, null);
			holder.lblFullName = (TextViewRobotoCondensedBold) convertView
					.findViewById(R.id.lbl_fullname);
			holder.lblAddress = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lbl_address);
			holder.lblAge = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lbl_age);
			holder.lblGender = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lbl_gender);
			holder.avatar = (CircleImageWithBorder) convertView
					.findViewById(R.id.avt_winner);
			holder.lblUniqueCode = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lbl_uniqueCode);

			convertView.setTag(holder);
		} else {
			holder = (HolderView) convertView.getTag();
		}

		final WinnerObj o = arrWinner.get(position);
		if (o != null) {
			if (!o.getAvatar().equals("")) {
				AQuery aq = mAq.recycle(convertView);
				aq.id(holder.avatar).image(
						UserFunctions.URLAdmin + o.getAvatar(), false, true);
			} else {
				holder.avatar.setImageResource(R.drawable.avatar2);
			}

			holder.lblFullName.setText(o.getFullName());
			holder.lblUniqueCode.setText(o.getUniqueCode());
			holder.lblGender.setText(o.getGender());
			holder.lblAddress.setText(o.getAddress());
			holder.lblAge.setText(DateTimeUtility.convertStringToDate(
					o.getAge(), "yyyy-MM-dd", "MMM dd yyyy"));
		}
		return convertView;
	}

	public class HolderView {
		CircleImageWithBorder avatar;
		TextViewRobotoCondensedRegular lblUniqueCode, lblAge, lblGender,
				lblAddress;
		TextViewRobotoCondensedBold lblFullName;
	}
}
