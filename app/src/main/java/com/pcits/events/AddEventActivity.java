/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pcits.events;

import java.io.File;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import android.net.Uri;
import android.app.AlertDialog;
import android.app.Dialog;
import java.io.InputStream;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnFailureListener;
import java.io.FileInputStream;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.pcits.common.utils.EventsPreferences;
import com.pcits.common.utils.GsonUtility;
import com.pcits.common.utils.Logging;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.fragments.PreviewEventFragment;
import com.pcits.events.libraries.JSONParser;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.DealObj;
import com.google.gson.reflect.TypeToken;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.pcits.events.obj.CreateEventObj;
import com.pcits.events.obj.CreateEventResponseObj;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedBold;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;
import com.pcits.events.wizard.model.AbstractWizardModel;
import com.pcits.events.wizard.model.ICustomPage;
import com.pcits.events.wizard.model.ModelCallbacks;
import com.pcits.events.wizard.model.Page;
import com.pcits.events.wizard.ui.PageFragmentCallbacks;
import com.pcits.events.wizard.ui.ReviewFragment;
import com.pcits.events.wizard.ui.StepPagerStrip;

public class AddEventActivity extends FragmentActivity implements
		PageFragmentCallbacks, ReviewFragment.Callbacks, ModelCallbacks {

	private static final String TAG = "AddEventActivity";
	// private String mGetDealId;
	private AddEventActivity self;

	private LinearLayout mLlHome;

	private ViewPager mPager;
	private MyPagerAdapter mPagerAdapter;

	private boolean mEditingAfterReview;

	private AbstractWizardModel mWizardModel = new EventWizardModel(this);

	private boolean mConsumePageSelectedEvent;

	private Button mNextButton;
	private Button mPrevButton;
	private TextViewRobotoCondensedRegular mTitle;

	private List<Page> mCurrentPageSequence;
	private StepPagerStrip mStepPagerStrip;

	private int mPrevScrollStep = 0;

	public static boolean isUpdated = false;
	public static boolean isCreated = false;
	public static DealObj dealObj;

	public static Bitmap eventCroppedImage;
	public static boolean isDeal = false;

	public static boolean isNewImageSet;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_event);
		self = this;
		if (GlobalValue.dealsObj == null) {
			GlobalValue.dealsObj = new DealObj();
		}

		if (savedInstanceState != null) {
			mWizardModel.load(savedInstanceState.getBundle("model"));
		}

		mWizardModel.registerListener(this);

		initUI();

		onPageTreeChanged();
		updateBottomBar();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		// Destroy public variables.
		new AlertDialog.Builder(this)
				.setTitle(R.string.app_name)
				.setMessage("Ignore changes?")
				.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								finish();
							}
						}).setNegativeButton(android.R.string.cancel, null)
				.create().show();
	}

	private void dialogSubmitSuccess()
	{
		/*new AlertDialog.Builder(this)
				.setTitle("Submitted successfully")
				.setMessage("Go back to main screen?")
				.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
                                finish();
							    startActivity(new Intent(AddEventActivity.this, ActivityHome.class));
							}
						}).setNegativeButton(android.R.string.cancel, null)
				.create().show();*/
		finish();
		startActivity(new Intent(AddEventActivity.this, ActivityHome.class));

	}

	private void dialogSubmitFailed() {
		new AlertDialog.Builder(this)
				.setTitle("Submit failed")
				.setMessage("Go back to main screen?")
				.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								finish();
							}
						}).setNegativeButton(android.R.string.cancel, null)
				.create().show();
	}

	private void initUI() {
		try {
			mLlHome = (LinearLayout) findViewById(R.id.llMenu);
			mNextButton = (Button) findViewById(R.id.next_button);
			mPrevButton = (Button) findViewById(R.id.prev_button);
			mTitle = (TextViewRobotoCondensedRegular) findViewById(R.id.lblTitleHeader);

            isNewImageSet = false;
			if (GlobalValue.dealsObj.getDeal_id() != null) {
				isDeal=true;
				mTitle.setText(getResources()
						.getString(R.string.page_edditdeal));
			} else {
				mTitle.setText(getResources().getString(R.string.page_adddeals));
			}

			mPager = (ViewPager) findViewById(R.id.pager);
			mStepPagerStrip = (StepPagerStrip) findViewById(R.id.strip);
			mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
			mPager.setAdapter(mPagerAdapter);

			// Should call this method end of declaring UI.
			initControl();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	private void initControl() {
		try {
			mStepPagerStrip
					.setOnPageSelectedListener(new StepPagerStrip.OnPageSelectedListener() {
						@Override
						public void onPageStripSelected(int position) {
							position = Math.min(mPagerAdapter.getCount() - 1,
									position);

							if (mPager.getCurrentItem() != position) {

								int prev = mPager.getCurrentItem();
								if (prev < mCurrentPageSequence.size()) {
									ICustomPage pagePrev = (ICustomPage) mCurrentPageSequence
											.get(prev);
									pagePrev.saveState();
								}

								mPager.setCurrentItem(position);

								int next = position;
								if (next < mCurrentPageSequence.size()) {
									ICustomPage pageNext = (ICustomPage) mCurrentPageSequence
											.get(next);
									pageNext.restoreState();
								}

							}

						}
					});

			mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
				@Override
				public void onPageSelected(int position) {
					try {
						int prev = mPrevScrollStep;
						if (prev < mCurrentPageSequence.size()) {
							ICustomPage pagePrev = (ICustomPage) mCurrentPageSequence
									.get(prev);
							pagePrev.saveState();
						}

						mStepPagerStrip.setCurrentPage(position);

						int next = position;
						if (next < mCurrentPageSequence.size()) {
							ICustomPage pageNext = (ICustomPage) mCurrentPageSequence
									.get(next);
							pageNext.restoreState();
						}

						mPrevScrollStep = position;

						if (mConsumePageSelectedEvent) {
							mConsumePageSelectedEvent = false;
							return;
						}

						mEditingAfterReview = false;
						updateBottomBar();
					} catch (Exception ex) {
						Logging.writeExceptionFromStackTrace(ex,
								ex.getMessage());
					}
				}
			});

			mNextButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						if (mPager.getCurrentItem() == mCurrentPageSequence
								.size()) {
							try {
								if (isValidated()) {
									showDialogConfirmCreation();
								}
							} catch (Exception ex) {
								Logging.writeExceptionFromStackTrace(ex,
										ex.getMessage());
							}
						} else {
							ICustomPage page = (ICustomPage) mCurrentPageSequence
									.get(mPager.getCurrentItem());
							page.saveState();

							if (mEditingAfterReview) {
								mPager.setCurrentItem(mPagerAdapter.getCount() - 1);
							} else {
								mPager.setCurrentItem(mPager.getCurrentItem() + 1);
							}
						}
					} catch (Exception ex) {
						Logging.writeExceptionFromStackTrace(ex,
								ex.getMessage());
					}
				}
			});

			mPrevButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					ICustomPage page = (ICustomPage) mCurrentPageSequence
							.get(mPager.getCurrentItem() - 1);
					page.restoreState();

					mPager.setCurrentItem(mPager.getCurrentItem() - 1);
				}
			});

			mLlHome.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					onBackPressed();
				}
			});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void showDialogConfirmCreation() {
		final Dialog dialog = new Dialog(self);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_confirmation);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		TextViewRobotoCondensedBold title = (TextViewRobotoCondensedBold) dialog
				.findViewById(R.id.dialog_title);
		TextViewRobotoCondensedRegular message = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.dialog_message);

		if (GlobalValue.dealsObj.getDeal_id() == null) {
			title.setText(getResources().getString(R.string.create_event));
			message.setText(self.getResources().getString(
					R.string.are_you_sure_you_want_to_create_event));
		} else {
			title.setText(getResources().getString(R.string.update_event));
			message.setText(self.getResources().getString(
					R.string.are_you_sure_you_want_to_update_event));
		}

		Button btnNegative = (Button) dialog.findViewById(R.id.btn_nagative);
		Button btnPositive = (Button) dialog.findViewById(R.id.btn_positive);

		btnNegative.setText(getResources().getString(R.string.no));
		btnPositive.setText(getResources().getString(R.string.yes));

		btnNegative.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		btnPositive.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Coding to create event here

				dialog.dismiss();

				// Create new event.
				createNewEvent();

			}
		});

		dialog.show();
	}

	private StorageReference storageReference;
	public void uploadPhoto()
    {

       /* dialog.setCancelable(false);
        dialog.show();*/

        TypeToken<com.pcits.events.obj.CreateEventObj> token = new TypeToken<CreateEventObj>(){};
        final CreateEventObj event = (CreateEventObj) GsonUtility.convertJSONStringToObject(EventsPreferences.getKeyPrivateEvent(AddEventActivity.this),token);
       /* Integer discount = ((event.getStartValue() - event.getAfterDiscountValue())
                / event.getStartValue() * 100 );

        event.setDiscount(discount);
        event.setSaveValue(event.getStartValue()-event.getAfterDiscountValue());*/
        Log.d(TAG, "uploadFile: ");
        final Uri[] fileUri = new Uri[1];






       // Log.e(TAG, "uploadPhoto: " + event.getImage());


        StorageReference uploadRef = storageReference.child("Files/event_" + System.currentTimeMillis() + ".jpg");
        InputStream stream = null;
        try {
                                        try
                                        {
                                            stream = new FileInputStream(new File(event.getImage()));
                                             UploadTask uploadTask = uploadRef.putStream(stream);

            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    @SuppressWarnings("VisibleForTests") Uri uri = taskSnapshot.getDownloadUrl();
                    fileUri[0] = uri;
                    //Log.e(TAG, "onSuccess: photo download link: " + uri.toString());
                    //Log.e(TAG, "onSuccess: username: " + GlobalValue.myClient.getUsername());
                    event.setImage("" + uri.toString());
					GlobalValue.dealsObj.setImage(event.getImage());
                    //event.setUsername(GlobalValue.myClient.getUsername());

                    createNewEvent();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    e.printStackTrace();
                    Toast.makeText(AddEventActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
                }
                catch(Exception e)
                {
                     Toast.makeText(AddEventActivity.this, "Please select image", Toast.LENGTH_LONG).show();
                     //progDialog.dismiss();
                }

        } catch (Exception e) {
            e.printStackTrace();
        }




        //if there is not any file

    }
	private boolean isValidated() {
		try {
			boolean isValidated = true;

			if (TextUtils.isEmpty(GlobalValue.dealsObj.getTitle())) {
				confirmValidation(0, "Title is empty", "Please enter title.");
				isValidated = false;
			} else if (TextUtils.isEmpty(GlobalValue.dealsObj.getCategory_id())) {
				confirmValidation(0, "Category is empty",
						"Please choose a category.");
				isValidated = false;
			} else if (AddEventActivity.eventCroppedImage == null) {
				confirmValidation(0, "Image is empty",
						"Please choose an image.");
				isValidated = false;
			} else if (GlobalValue.dealsObj.getStart_value() == null) {
				confirmValidation(1, "Start value is empty",
						"Please enter start value.");
				isValidated = false;
			} else if (GlobalValue.dealsObj.getAfter_discount_value() == null) {
				confirmValidation(1, "After discount is empty",
						"Please enter after discount.");
				isValidated = false;
			} else if (TextUtils.isEmpty(GlobalValue.dealsObj
					.getStart_time())){
				confirmValidation(1, "Start time is empty",
						"Please enter start time.");
				isValidated = false;
			} else if (TextUtils
					.isEmpty(GlobalValue.dealsObj.getEnd_time())) {
				confirmValidation(1, "End time is empty",
						"Please enter end time.");
				isValidated = false;
			} else if (TextUtils.isEmpty(GlobalValue.dealsObj.getAddress())) {
				confirmValidation(2, "Address is empty",
						"Please enter address.");
				isValidated = false;
			} else if (TextUtils.isEmpty(GlobalValue.dealsObj.getCity())) {
				confirmValidation(2, "City is empty", "Please enter city.");
				isValidated = false;
			} else if (TextUtils.isEmpty(GlobalValue.dealsObj.getCounty())) {
				confirmValidation(2, "County is empty", "Please enter county.");
				isValidated = false;
			} else if (TextUtils.isEmpty(GlobalValue.dealsObj.getCountry())) {
				confirmValidation(2, "Country is empty",
						"Please choose country.");
				isValidated = false;
			} else if (TextUtils.isEmpty(GlobalValue.dealsObj.getAddress())) {
				confirmValidation(2, "Postal code is empty",
						"Please enter postal code.");
				isValidated = false;
			} else if (TextUtils.isEmpty(GlobalValue.dealsObj.getDescription())) {
				confirmValidation(3, "Description is empty",
						"Please enter description.");
				isValidated = false;
			}

			return isValidated;
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void confirmValidation(final int idx, String strTitle,
			String strMessage) {
		final Dialog dialog = new Dialog(self);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_confirmation);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		TextViewRobotoCondensedBold title = (TextViewRobotoCondensedBold) dialog
				.findViewById(R.id.dialog_title);
		TextViewRobotoCondensedRegular message = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.dialog_message);

		title.setText(strTitle);
		message.setText(strMessage);

		Button btnNegative = (Button) dialog.findViewById(R.id.btn_nagative);
		Button btnPositive = (Button) dialog.findViewById(R.id.btn_positive);

		btnNegative.setText(self.getResources().getString(R.string.no));
		btnPositive.setText(self.getResources().getString(R.string.yes));

		btnNegative.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		btnPositive.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

				// Open validation page
				mPager.setCurrentItem(idx);
			}
		});

		dialog.show();
	}

	private void createNewEvent() {
		// Add more information
       // Toast.makeText(self, GlobalValue.dealsObj.getTitle(), Toast.LENGTH_SHORT).show();
		try
		{
			GlobalValue.dealsObj.setUsername(GlobalValue.myUser.getUsername());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			GlobalValue.dealsObj.setUsername(GlobalValue.myClient.getUsername());
		}
		/*try
		{
			GlobalValue.dealsObj.setUsername(GlobalValue.myClient.getUsername());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}*/
		GlobalValue.dealsObj.setFeatured(0);
		GlobalValue.dealsObj.setSave_value(GlobalValue.dealsObj
				.getStart_value());
		GlobalValue.dealsObj.setSave_value(GlobalValue.dealsObj
				.getStart_value()
				- GlobalValue.dealsObj.getAfter_discount_value());

		String discount = "";
		discount = ((GlobalValue.dealsObj.getStart_value() - GlobalValue.dealsObj
				.getAfter_discount_value())
				/ GlobalValue.dealsObj.getStart_value() * 100 + "");

		GlobalValue.dealsObj.setDiscount(Integer.parseInt(discount.trim()
				.substring(0, discount.trim().indexOf("."))));

		Log.d(TAG, "createNewEvent: "+ GsonUtility.convertObjectToJSONString(GlobalValue.dealsObj));

		// If deal id is null will create new one or update
		if (GlobalValue.dealsObj.getDeal_id() == null) {
			ModelManager.addDeals(this, GlobalValue.dealsObj,
					eventCroppedImage, true, new ModelManagerListener() {

						@Override
						public void onSuccess(Object object) {
							// TODO Auto-generated method stub
							String json = (String) object;
							Log.d(TAG, "[RESPONSE] - Create event:" + json);
							Toast.makeText(self, checkResult(json),
									Toast.LENGTH_SHORT).show();

							// Set is created true.
							isCreated = true;
							try {
								dealObj = JSONParser.parserDeal("data", json)
										.get(0);
							} catch (Exception ex) {
								Logging.writeExceptionFromStackTrace(ex,
										ex.getMessage());
							}
						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}

					});
		} else {
			ModelManager.updateDeal(this, GlobalValue.dealsObj,
					eventCroppedImage, true, new ModelManagerListener() {

						@Override
						public void onSuccess(Object object) {
							// TODO Auto-generated method stub
							String json = (String) object;
							Log.e(TAG, "[RESPONSE] - Update event:" + json);
							//Toast.makeText(self, checkResult(json),Toast.LENGTH_SHORT.show();

							// Set is updated true.
							isUpdated = true;
							try
							 {
								dealObj = JSONParser.parserDeal("data", json)
										.get(0);
								Toast.makeText(self, "Event updated",Toast.LENGTH_SHORT).show();
								Intent intent=new Intent(AddEventActivity.this,ActivityHome.class);
								startActivity(intent);
								finish();

							} catch (Exception ex)
							{
								Logging.writeExceptionFromStackTrace(ex,
										ex.getMessage());
								Toast.makeText(self, "Event updated",Toast.LENGTH_SHORT).show();
								Intent intent=new Intent(AddEventActivity.this,ActivityHome.class);
								startActivity(intent);
								finish();
							}
						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}

					});
		}
	}

	protected String checkResult(String strJson) {
		// TODO Auto-generated method stub
		JSONObject json = null;
		String message = "";
		try {
			json = new JSONObject(strJson);
			if (json.getString("status").equals("success")) {
				message = this.getString(R.string.message_success);
				// String deal_id = json.getString("data");
				// Intent i = new Intent(this, ActivityDealFeatured.class);
				// i.putExtra("dealId_new", deal_id);
				// startActivity(i);
				// finish();
				dialogSubmitSuccess();
			} else {
				message = json.getString("message");
				dialogSubmitFailed();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return message;
	}

	@Override
	public void onPageTreeChanged() {
		mCurrentPageSequence = mWizardModel.getCurrentPageSequence();
		recalculateCutOffPage();
		mStepPagerStrip.setPageCount(mCurrentPageSequence.size() + 1); // + 1 =
																		// review
																		// step
		mPagerAdapter.notifyDataSetChanged();updateBottomBar();
	}

	private void updateBottomBar() {
		int position = mPager.getCurrentItem();
		if (position == mCurrentPageSequence.size()) {
			mNextButton.setText(  R.string.submit);
			// mNextButton.setBackgroundResource(R.drawable.finish_background);
			// mNextButton.setTextAppearance(this,
			// R.style.TextAppearanceFinish);
			mNextButton.setBackgroundResource(R.drawable.bg_last_button);
		} else {
			mNextButton.setText(mEditingAfterReview ? R.string.review
					: R.string.next);
			// mNextButton
			// .setBackgroundResource(R.drawable.selectable_item_background);
			mNextButton.setBackgroundResource(R.drawable.bg_button);
			TypedValue v = new TypedValue();
			getTheme().resolveAttribute(android.R.attr.textAppearanceMedium, v,
					true);
			// mNextButton.setTextAppearance(this, v.resourceId);
			mNextButton.setEnabled(position != mPagerAdapter.getCutOffPage());
		}

		mPrevButton
				.setVisibility(position <= 0 ? View.INVISIBLE : View.VISIBLE);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mWizardModel.unregisterListener(this);

		// Destroy deal object
        if(GlobalValue.isNewEvent) {
            GlobalValue.dealsObj = null;
        }

		isDeal = false;
		eventCroppedImage = null;
		dealObj = null;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBundle("model", mWizardModel.save());
	}

	@Override
	public AbstractWizardModel onGetModel() {
		return mWizardModel;
	}

	@Override
	public void onEditScreenAfterReview(String key) {
		for (int i = mCurrentPageSequence.size() - 1; i >= 0; i--) {
			if (mCurrentPageSequence.get(i).getKey().equals(key)) {
				mConsumePageSelectedEvent = true;
				mEditingAfterReview = true;
				mPager.setCurrentItem(i);
				updateBottomBar();
				break;
			}
		}
	}

	@Override
	public void onPageDataChanged(Page page) {
		if (page.isRequired()) {
			if (recalculateCutOffPage()) {
				mPagerAdapter.notifyDataSetChanged();
				updateBottomBar();
			}
		}
	}

	@Override
	public Page onGetPage(String key) {
		return mWizardModel.findByKey(key);
	}

	private boolean recalculateCutOffPage() {
		// Cut off the pager adapter at first required page that isn't completed
		int cutOffPage = mCurrentPageSequence.size() + 1;
		for (int i = 0; i < mCurrentPageSequence.size(); i++) {
			Page page = mCurrentPageSequence.get(i);
			if (page.isRequired() && !page.isCompleted()) {
				cutOffPage = i;
				break;
			}
		}

		if (mPagerAdapter.getCutOffPage() != cutOffPage) {
			mPagerAdapter.setCutOffPage(cutOffPage);
			return true;
		}

		return false;
	}

	public class MyPagerAdapter extends FragmentStatePagerAdapter {
		private int mCutOffPage;
		private Fragment mPrimaryItem;

		public MyPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int i) {
			if (i >= mCurrentPageSequence.size()) {
				// return new ReviewFragment();
				return new PreviewEventFragment();
			}

			return mCurrentPageSequence.get(i).createFragment();
		}

		@Override
		public int getItemPosition(Object object) {
			// TODO: be smarter about this
			if (object == mPrimaryItem) {
				// Re-use the current fragment (its position never changes)
				return POSITION_UNCHANGED;
			}

			return POSITION_NONE;
		}

		@Override
		public void setPrimaryItem(ViewGroup container, int position,
				Object object) {
			super.setPrimaryItem(container, position, object);
			mPrimaryItem = (Fragment) object;
		}

		@Override
		public int getCount() {
			if (mCurrentPageSequence == null) {
				return 0;
			}
			return Math.min(mCutOffPage + 1, mCurrentPageSequence.size() + 1);
		}

		public void setCutOffPage(int cutOffPage) {
			if (cutOffPage < 0) {
				cutOffPage = Integer.MAX_VALUE;
			}
			mCutOffPage = cutOffPage;
		}

		public int getCutOffPage() {
			return mCutOffPage;
		}
	}
}
