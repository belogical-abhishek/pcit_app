/**
 * File        : ActivityAbout.java
 * App name    : M1SO Events
 * Version     : 1.1.4
 * Created     : 25/05/14

 * Created by Team M1SO on 25/01/14.
 * Copyright (c) 2014 M1SO. All rights reserved.
 */

package com.pcits.events;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.reflect.TypeToken;
import com.pcits.common.utils.EventsPreferences;
import com.pcits.common.utils.GsonUtility;
import com.pcits.common.utils.Logging;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.obj.CreateEventObj;
import com.pcits.events.widgets.ggplaces.PlacesAutoCompleteAdapter;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class MapPinActivity extends FragmentActivity implements
		LocationListener, OnMapReadyCallback {

	private final String TAG = MapPinActivity.class.getSimpleName();

	String filteredData="";
	private MapPinActivity self;
	private TextViewRobotoCondensedRegular mLblLat, mLblLng;
	private Button mBtnCancel, mBtnOk;
	private GoogleMap mMaps;
	private Marker mMarker;

	private AutoCompleteTextView mAtTxtPlace;

	private PlacesAutoCompleteAdapter mAdapter;

	HandlerThread mHandlerThread;
	Handler mThreadHandler;

	public MapPinActivity() {
		// Required empty public constructor
		if (mThreadHandler == null) {
			// Initialize and start the HandlerThread
			// which is basically a Thread with a Looper
			// attached (hence a MessageQueue)
			mHandlerThread = new HandlerThread(TAG,
					android.os.Process.THREAD_PRIORITY_BACKGROUND);
			mHandlerThread.start();

			// Initialize the Handler
			mThreadHandler = new Handler(mHandlerThread.getLooper()) {
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {
						ArrayList<String> results = mAdapter.resultList;

						if (results != null && results.size() > 0) {
							mAdapter.notifyDataSetChanged();
						} else {
							mAdapter.notifyDataSetInvalidated();
						}
					}
				}
			};
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.dialog_pin_map);
			self = this;

			initUI();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		// Get rid of our Place API Handlers
		if (mThreadHandler != null) {
			mThreadHandler.removeCallbacksAndMessages(null);
			mHandlerThread.quit();
		}
	}

	private void initUI() {
		mLblLat = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_lat_pin);
		mLblLng = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_lng_pin);
		mAtTxtPlace = (AutoCompleteTextView) findViewById(R.id.atTxtPlace);
		mAtTxtPlace.setAdapter(new PlacesAutoCompleteAdapter(self,
				R.layout.item_auto_place));

		mBtnCancel = (Button) findViewById(R.id.btn_cancel);
		mBtnOk = (Button) findViewById(R.id.btn_ok);

		try {
			// Get current location
			((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.maps)).getMapAsync(this);

			// Should call this method by the end of declaring UI.
			initControl();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
	}

	private void initControl() {
		try {
			mBtnCancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					MapPinActivity.this.onBackPressed();
				}
			});

			mBtnOk.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// Get lat and longitude
					if (!mLblLat.getText().toString().equals("")
							&& !mLblLng.getText().toString().equals("")) {

						if (GlobalValue.dealsObj != null) {
							GlobalValue.dealsObj.setLatitude(Double
									.parseDouble(mLblLat.getText().toString()));
							GlobalValue.dealsObj.setLongitude(Double
									.parseDouble(mLblLng.getText().toString()));
						} else if (GlobalValue.createEventObj != null) {
							GlobalValue.createEventObj.setLatitude(Float
									.parseFloat(mLblLat.getText().toString()));
							GlobalValue.createEventObj.setLongitude(Float
									.parseFloat(mLblLng.getText().toString()));
						}
						TypeToken<CreateEventObj> token = new TypeToken<CreateEventObj>() {
						};
						CreateEventObj eventObj = GsonUtility.convertJSONStringToObject(EventsPreferences.getKeyPrivateEvent(MapPinActivity.this), token);

						if (eventObj != null) {
							eventObj.setLatitude(Float
									.parseFloat(mLblLat.getText().toString()));
							eventObj.setLongitude(Float
									.parseFloat(mLblLng.getText().toString()));
						}
					}

					Intent i = new Intent();
					// Get address
					if (!mAtTxtPlace.getText().toString().trim().equals("")) {
						i.putExtra("address", filteredData);
					}
					setResult(RESULT_OK, i);
					MapPinActivity.this.onBackPressed();
				}
			});

			mAtTxtPlace
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
												View view, int position, long id) {
							// Get data associated with the specified position
							// in the list (AdapterView)
							String description = (String) parent
									.getItemAtPosition(position);

							// Move camera to new address.
							new UpdateMapsByAddress().execute(description);
						}
					});

			mAtTxtPlace.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start,
											  int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start,
										  int before, int count) {
					final String value = s.toString();

					// Remove all callbacks and messages
					mThreadHandler.removeCallbacksAndMessages(null);

					// Now add a new one
					mThreadHandler.postDelayed(new Runnable() {

						@Override
						public void run() {
							if (mAdapter == null) {
								mAdapter = new PlacesAutoCompleteAdapter(self,
										R.layout.item_auto_place);
							}
							// Background thread
							mAdapter.resultList = mAdapter.mPlaceAPI
									.autocomplete(value);

							// Footer
							if (mAdapter.resultList.size() > 0) {
								mAdapter.resultList.add("footer");
							}

							// Post to Main Thread
							mThreadHandler.sendEmptyMessage(1);
						}
					}, 500);
				}

				@Override
				public void afterTextChanged(Editable s) {
				}
			});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void onLocationChanged(Location loc) {

		LatLng latLng = new LatLng(loc.getLatitude(), loc.getLongitude());

		mMaps.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));

		if (mMarker != null) {
			// Only show 1 marker.
			mMarker.remove();
		}
		mMaps.clear();
		mMarker = mMaps.addMarker(new MarkerOptions().position(latLng));

		mLblLat.setText(loc.getLatitude() + "");
		mLblLng.setText(loc.getLongitude() + "");

		new getAddressFromLatLong().execute(latLng);

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		Log.d(TAG, "in onMapReady: ");
		mMaps = googleMap;
		if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return;
		}
		mMaps.setMyLocationEnabled(true);

		// Move to current location
		LocationManager locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		Criteria crit = new Criteria();

		List<String> providers = locManager.getProviders(true);
		Location bestLocation = null;
		for (String provider : providers) {
			Location l = locManager.getLastKnownLocation(provider);
			if (l == null) {
				continue;
			}
			if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
				// Found best last known location: %s", l);
				bestLocation = l;
			}
		}

		if (bestLocation != null) {
			onLocationChanged(bestLocation);

		}
//		 locManager.requestLocationUpdates(provider, 20000, 0, this);

		// Click on map to get latitude and longitude.
		mMaps.setOnMapClickListener(new OnMapClickListener() {

			@Override
			public void onMapClick(LatLng loc) {
				// Set marker
				if (mMarker != null) {
					// Only show 1 marker.
					mMarker.remove();
				}
				mMaps.clear();
				mMarker = mMaps.addMarker(new MarkerOptions().position(loc));

				// Set text
				mLblLat.setText(loc.latitude + "");
				mLblLng.setText(loc.longitude + "");


				new getAddressFromLatLong().execute(loc);
			}
		});

	}

	private class UpdateMapsByAddress extends AsyncTask<String, Void, LatLng> {

		String strAddress;

		@Override
		protected LatLng doInBackground(String... params) {
			try {
				// TODO Auto-generated method stub
				strAddress = params[0];
				if (strAddress.contains(" ")) {
					strAddress = strAddress.replace(" ", "%20");
				}
				String uri = "http://maps.google.com/maps/api/geocode/json?address="
						+ strAddress + "&sensor=false";
				HttpGet httpGet = new HttpGet(uri);
				HttpClient client = new DefaultHttpClient();
				HttpResponse response;
				StringBuilder stringBuilder = new StringBuilder();

				try {
					response = client.execute(httpGet);
					HttpEntity entity = response.getEntity();
					InputStream stream = entity.getContent();
					int b;
					while ((b = stream.read()) != -1) {
						stringBuilder.append((char) b);
					}
				} catch (Exception ex) {
					Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
					return new LatLng(0.0, 0.0);
				}

				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject = new JSONObject(stringBuilder.toString());

					double lng = ((JSONArray) jsonObject.get("results"))
							.getJSONObject(0).getJSONObject("geometry")
							.getJSONObject("location").getDouble("lng");

					double lat = ((JSONArray) jsonObject.get("results"))
							.getJSONObject(0).getJSONObject("geometry")
							.getJSONObject("location").getDouble("lat");

					return new LatLng(lat, lng);
				} catch (Exception ex) {
					Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
					return new LatLng(0.0, 0.0);
				}
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
				return new LatLng(0.0, 0.0);
			}
		}

		@Override
		protected void onPostExecute(LatLng result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			try {
				// Move camera
				LatLng latLng = new LatLng(result.latitude, result.longitude);
				mMaps.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));

				// Add marker
				mMaps.clear();
				mMaps.addMarker(new MarkerOptions().position(latLng)).setTitle(
						strAddress);

				// Set lat and long
				mLblLat.setText(result.latitude + "");
				mLblLng.setText(result.longitude + "");
			} catch (Exception ex) {
				throw new RuntimeException(ex);
			}
		}
	}


	public class getAddressFromLatLong extends AsyncTask<LatLng,Void, String>{

		@Override
		protected String doInBackground(LatLng... params) {
			LatLng loc[] = params;

			String filterAddress = "";
			Geocoder geoCoder = new Geocoder(getBaseContext(), Locale.getDefault());
			try {
				List<Address> addresses = geoCoder.getFromLocation(loc[0].latitude, loc[0].longitude, 1);
				Log.d(TAG, "doInBackground: "+GsonUtility.convertObjectToJSONString(addresses));


				if (addresses.size() > 0) {
					//if(addresses.get(0).getMaxAddressLineIndex()==0) {
						String arr[] =addresses.get(0).getAddressLine(0).split(",");
						filterAddress=addresses.get(0).getAddressLine(0);

					filteredData = "";

					filteredData += ""+arr[0]+", "+arr[1]+", "+arr[2];

					filteredData += ":" + addresses.get(0).getSubAdminArea();
					filteredData += ":" + addresses.get(0).getSubLocality();
					filteredData += ":" + addresses.get(0).getCountryName();
					filteredData += ":" + addresses.get(0).getPostalCode();
					Log.d(TAG, "onActivityResult"+filteredData);
					/*}
					else {
						for (int i = 0; i < addresses.get(0).getMaxAddressLineIndex(); i++) {
							String arr[] = addresses.get(0).getAddressLine(0).split(",");
							filterAddress += "" + arr[0] + ", " + arr[1] + ", " + arr[2];

							filterAddress += ":" + addresses.get(0).getSubAdminArea();
							filterAddress += ":" + addresses.get(0).getSubLocality();
							filterAddress += ":" + addresses.get(0).getCountryName();
							filterAddress += ":" + addresses.get(0).getPostalCode();
						}
					}*/
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			} catch (Exception e2) {
				// TODO: handle exception
				e2.printStackTrace();
			}

			return filterAddress;
		}

		@Override
		protected void onPostExecute(String result){

			Log.d(TAG, "onPostExecute: "+result);
			mAtTxtPlace.setText(result);
		}
	}
}