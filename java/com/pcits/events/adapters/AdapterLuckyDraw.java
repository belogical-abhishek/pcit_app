package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pcits.events.DetailClientTicketActivity;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.obj.MessageObj;
import com.pcits.events.widgets.MaterialRippleLayout;

public class AdapterLuckyDraw extends BaseAdapter {

	private Activity activity;
	private ArrayList<MessageObj> arrLuckyDraw;
	private LayoutInflater mInflate;

	public AdapterLuckyDraw(Activity activity,
			ArrayList<MessageObj> listLuckyDraw) {
		this.activity = activity;
		this.arrLuckyDraw = listLuckyDraw;
		this.mInflate = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrLuckyDraw.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		// TODO Auto-generated method stub
		final HolderView holder;
		if (convertView == null) {
			holder = new HolderView();
			convertView = mInflate.inflate(R.layout.item_lucky_draw, null);
			holder.lblUsername = (TextView) convertView
					.findViewById(R.id.lblUsername);
			holder.lblEmail = (TextView) convertView
					.findViewById(R.id.lblEmail);
			holder.lblDate = (TextView) convertView
					.findViewById(R.id.lblDateBuy);
			holder.itemLuckyDraw = (MaterialRippleLayout) convertView
					.findViewById(R.id.itemLuckyDraw);
			holder.lblAmount = (TextView) convertView.findViewById(R.id.lblAmount);
			holder.lblUniqueCode = (TextView) convertView.findViewById(R.id.lblUniqueCode);
			convertView.setTag(holder);
		} else {
			holder = (HolderView) convertView.getTag();
		}
		final MessageObj o = arrLuckyDraw.get(position);
		if (o != null) {
			Log.e("Adapter", o.getClient_id()+ "==" + o.getAmount() + "---" + o.getUniquecode());
			holder.lblUsername.setText("Name: " + o.getClient_id());
			holder.lblAmount.setText("Amount: " +o.getAmount());
			holder.lblUniqueCode.setText("UniqueCode: " + o.getUniquecode());
			holder.lblEmail.setText("Email: " + o.getEmail());
			holder.lblDate.setText("Date Created: " + o.getDate_created());
			holder.itemLuckyDraw.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					GlobalValue.luckyDraw = arrLuckyDraw.get(position);
					Intent i = new Intent(activity, DetailClientTicketActivity.class);
					parent.getContext().startActivity(i);
				}
			});

		}
		return convertView;
	}

	public class HolderView {
		TextView lblUsername, lblEmail, lblDate,
		 lblAmount, lblUniqueCode;
		MaterialRippleLayout itemLuckyDraw;
	}

}
