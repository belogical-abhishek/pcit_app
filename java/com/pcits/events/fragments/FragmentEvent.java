package com.pcits.events.fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.pcits.events.ActivityHome;
import com.pcits.events.AddEventActivity;
import com.pcits.events.R;
import com.pcits.events.adapters.HomeAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.JSONParser;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.obj.DealObj;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.floatingactionbutton.AddFloatingActionButton;

public class FragmentEvent extends Fragment {

	private AddFloatingActionButton btnAddDeal;

	// ArrayList<HashMap<String, String>> menuItems;
	private ProgressDialog pDialog;
	private ArrayList<DealObj> mArrDeals;

	// Declare object of userFunctions and Utils class
	private UserFunctions userFunction;
	private Utils utils;
	private ListView list;
	private HomeAdapter mla;

	private Button btnLoadMore;

	// flag for current page
	private JSONObject json, jsonCurrency;
	private int mCurrentPage = 0;
	private int mPreviousPage;

	// create array variables to store data
	public String[] mDealsId;
	public String[] mTitle;
	public String[] mCompany;
	public String[] mDateEnd;
	public String[] mDateStart;
	public String[] mTimeStart;
	public String[] mTimeEnd;
	public String[] mAfterDiscValue;
	public String[] mStartValue;
	public String[] mImg;
	public String[] mIcMarker;
	public String[] mAttend;

	private int intLengthData;

	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private String client;

	private View v;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		v = inflater.inflate(R.layout.layout_list_deals, container, false);

		if (mArrDeals == null) {
			mArrDeals = new ArrayList<DealObj>();
		}

		userFunction = new UserFunctions();
		utils = new Utils(getActivity());
		initUI();
		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void initUI() {
		llMenu = (LinearLayout) v.findViewById(R.id.llMenu);
		list = (ListView) v.findViewById(R.id.lsvDeals);
		btnAddDeal = (AddFloatingActionButton) v.findViewById(R.id.btnAddDeal);

		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
				Intent intent=new Intent(getActivity(),ActivityHome.class);
				getActivity().startActivity(intent);
				getActivity().finish();
			}
		});
		btnAddDeal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity(), AddEventActivity.class);
				startActivity(i);
			}
		});
		// menuItems = new ArrayList<HashMap<String, String>>();
		// Create LoadMore button
		btnLoadMore = new Button(getActivity());
		btnLoadMore
				.setBackgroundResource(R.drawable.apptheme_btn_default_holo_light);
		btnLoadMore.setText(getString(R.string.btn_load_more));
		btnLoadMore.setTextColor(getResources().getColor(R.color.text_btn));
		mArrDeals = new ArrayList<DealObj>();
		new loadFirstListView().execute();
		// Listener to handle load more button when clicked
		btnLoadMore.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Starting a new async task
				json = null;
				new loadMoreListView().execute();
			}
		});
		// Listener to get selected id when list item clicked
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				final int pos = position;
				// TODO Auto-generated method stub
				// HashMap<String, String> item = new HashMap<String, String>();
				// item = menuItems.get(position);

				// Pass id to onListSelected method on HomeActivity
				// mCallback.onListSelected(item.get(userFunction.key_deals_id));
				AddEventActivity.isDeal = true;
				GlobalValue.dealsObj = mArrDeals.get(position);
				Intent i = new Intent(getActivity(), AddEventActivity.class);
				// i.putExtra("id", mArrDeals.get(position).getDeal_id());

				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_left);

				// Set the item as checked to be highlighted when in
				// two-pane
				// layout
				list.setItemChecked(position, true);
			}
		});
	}

	// Load first 10 videos
	private class loadFirstListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Showing progress dialog before sending http request
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please wait..");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected Void doInBackground(Void... unused) {
			if (Integer.parseInt(GlobalValue.myUser.getRole()) == 0) {
				getDataFromServerAll();
			} else {
				getDataFromServer();
			}
			return (null);
		}

		protected void onPostExecute(Void unused) {
			// Condition if data length uder 10 button loadMore is remove
			if (intLengthData < UserFunctions.valueItemsPerPage) {
				list.removeFooterView(btnLoadMore);
			} else {
				if (Integer.parseInt(GlobalValue.myUser.getRole()) == 0) {
					list.addFooterView(btnLoadMore);
				}
			}
			if (mArrDeals.size() != 0) {
				// Check paramter notif
				int paramNotif = utils.loadPreferences(utils.UTILS_PARAM_NOTIF);

				// Condition if app start in the first time notif will run
				// in background
				if (paramNotif != 1) {
					utils.saveString(utils.UTILS_NOTIF, mDealsId[0]);
					utils.savePreferences(utils.UTILS_PARAM_NOTIF, 1);

				}

				// Adding load more button to lisview at bottom
				list.setVisibility(View.VISIBLE);
				// Getting adapter
				mla = new HomeAdapter(getActivity(), mArrDeals);
				list.setAdapter(mla);

			} else {
				list.removeFooterView(btnLoadMore);
				if (json != null) {

				} else {
					Toast.makeText(getActivity(),
							getString(R.string.no_connection),
							Toast.LENGTH_SHORT).show();
				}
			}

			// Closing progress dialog
			pDialog.dismiss();
		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (pDialog != null) {
			pDialog.dismiss();
			pDialog = null;
		}
	}

	// Load more videos
	private class loadMoreListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Showing progress dialog before sending http request
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please wait..");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected Void doInBackground(Void... unused) {

			// Store previous value of current page
			mPreviousPage = mCurrentPage;
			// Increment current page
			mCurrentPage += UserFunctions.valueItemsPerPage;
			if (Integer.parseInt(GlobalValue.myUser.getRole()) == 0) {
				getDataFromServerAll();
			} else {
				getDataFromServer();
			}
			return (null);
		}

		protected void onPostExecute(Void unused) {
			// Condition if data length under 10 button loadMore is remove
			if (intLengthData < UserFunctions.valueItemsPerPage)
				list.removeFooterView(btnLoadMore);
			if (json != null) {
				// Get listview current position - used to maintain scroll
				// position
				int currentPosition = list.getFirstVisiblePosition();
				// Appending new data to menuItems ArrayList
				mla = new HomeAdapter(getActivity(), mArrDeals);
				list.setAdapter(mla);
				// Setting new scroll position
				list.setSelectionFromTop(currentPosition + 1, 0);

			} else {
				if (mArrDeals != null) {
					mCurrentPage = mPreviousPage;
				} else {
					Toast.makeText(getActivity(),
							getString(R.string.no_connection),
							Toast.LENGTH_SHORT).show();
				}

			}
			// Closing progress dialog
			pDialog.dismiss();
		}
	}

	// Method to get Data from Server
	public void getDataFromServer() {

		try {
			// If mActivity equal activityCategory then use API dealByCategory
			// if (mActivity.equals(utils.EXTRA_ACTIVITY_CATEGORY)) {
			// json = userFunction.dealByCategory(mCategoryId,
			// mCurrentPage);
			// } else {
			String username = GlobalValue.myUser.getUsername();
			jsonCurrency = userFunction.currency(getActivity());
			json = userFunction.dealsByUser(username, mCurrentPage,
					getActivity());
			// }
			Log.e("url", "url: " + json);
			if (json != null) {
				ArrayList<DealObj> arr = JSONParser.parserDeal(
						userFunction.array_deals_by_user, json.toString());
				mArrDeals.addAll(arr);
				intLengthData = mArrDeals.size();
			}
			if (jsonCurrency != null) {
				JSONArray currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);
			}

		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	// Method to get Data from Server
	public void getDataFromServerAll() {

		try {
			if (GlobalValue.myClient != null) {
				client = GlobalValue.myClient.getUsername();
			} else {
				client = "";
			}
			jsonCurrency = userFunction.currency(getActivity());
			json = userFunction
					.latestDeals(client, mCurrentPage, getActivity());
			Log.e("url", "url: " + json);

			if (json != null) {
				JSONArray dataDealsArray;

				// If mActivity equal activityCategory then use array
				// dealByCategory
				ArrayList<DealObj> arr = JSONParser.parserDeal(
						userFunction.array_latest_deals, json.toString());
				mArrDeals.addAll(arr);
				intLengthData = mArrDeals.size();
			}
			if (jsonCurrency != null) {
				JSONArray currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);

			}

		} catch (JSONException ex) {
			throw new RuntimeException(ex);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
}
