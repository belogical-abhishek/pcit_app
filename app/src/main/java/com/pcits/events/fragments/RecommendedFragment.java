package com.pcits.events.fragments;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityDetail;
import com.pcits.events.ActivityHome;
import com.pcits.events.AddEventActivity;
import com.pcits.events.R;
import com.pcits.events.adapters.HomeAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.libraries.JSONParser;
import com.pcits.events.libraries.ServiceNotification;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.obj.APIObj;
import com.pcits.events.obj.DealObj;
import com.pcits.events.obj.ListEventObj;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class RecommendedFragment extends Fragment implements
		OnClickListener {

	private static final String TAG = "RecommendedFragment";
	private ArrayList<DealObj> mArrDeals;

	// Declare object of userFunctions and Utils class
	private UserFunctions userFunction;
	private Utils utils;

	// Create instance of list and ListAdapter
	private ListView list;
	private TextView lblNoResult;
	private HomeAdapter mla;

	private Button btnLoadMore;
	private LinearLayout lytRetry;

	// flag for current page
	private JSONObject json, jsonCurrency;
	private int mCurrentPage = 0;
	private int mPreviousPage;

	private int intLengthData;
	// Declare variable
	public static Activity self;
	private PullToRefreshListView mPullRefreshListView;

	private View v;

	public static LinearLayout llMenu;
	private String client;

	// Declare OnListSelected interface
	public interface OnDataListSelectedListener {
		public void onListSelected(String idSelected);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		self = getActivity();
		v = inflater.inflate(R.layout.fragment_home, container, false);
		try {
			if (mArrDeals == null) {
				mArrDeals = new ArrayList<DealObj>();
			}
			// Clear old data
			mArrDeals.clear();

			// list = (ListView) v.findViewById(R.id.list);
			lblNoResult = (TextView) v.findViewById(R.id.lblNoResult);
			lytRetry = (LinearLayout) v.findViewById(R.id.llRetry);
			// btnRetry = (Button) v.findViewById(R.id.btnRetry);
			llMenu = (LinearLayout) v.findViewById(R.id.llMenu);
			llMenu.setOnClickListener(this);

			// Declare object of userFunctions class
			userFunction = new UserFunctions();
			utils = new Utils(getActivity());

			// Create LoadMore button
			btnLoadMore = new Button(getActivity());
			btnLoadMore
					.setBackgroundResource(R.drawable.apptheme_btn_default_holo_light);
			btnLoadMore.setText(getString(R.string.btn_load_more));
			btnLoadMore.setTextColor(getResources().getColor(R.color.text_btn));

			// Load data
			new loadFirstListView().execute();

			Log.d(TAG, "onCreateView: " + mArrDeals.size());

			mPullRefreshListView = (PullToRefreshListView) v
					.findViewById(R.id.lsvPullToRefresh);
			list = mPullRefreshListView.getRefreshableView();
			// Set a listener to be invoked when the list should be refreshed.
			if (NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
				mPullRefreshListView
						.setOnRefreshListener(new OnRefreshListener<ListView>() {
							@Override
							public void onRefresh(
									PullToRefreshBase<ListView> refreshView) {

								String label = DateUtils.formatDateTime(
										getActivity(),
										System.currentTimeMillis(),
										DateUtils.FORMAT_SHOW_TIME
												| DateUtils.FORMAT_SHOW_DATE
												| DateUtils.FORMAT_ABBREV_ALL);

								// Update the LastUpdatedLabel
								refreshView.getLoadingLayoutProxy()
										.setLastUpdatedLabel(label);
								mArrDeals = new ArrayList<DealObj>();
								new loadFirstListView().execute();

							}
						});
			} else {
				Toast.makeText(getActivity(),
						getString(R.string.no_internet_connection),
						Toast.LENGTH_SHORT).show();
			}

			// Listener to handle load more buttton when clicked
			btnLoadMore.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// Starting a new async task
					if (NetworkUtility.getInstance(getActivity())
							.isNetworkAvailable()) {
						json = null;
						new loadMoreListView().execute();
					} else {
						Toast.makeText(getActivity(),
								getString(R.string.no_internet_connection),
								Toast.LENGTH_SHORT).show();
					}
				}
			});

			// Add an end-of-list listener
			mPullRefreshListView
					.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

						@Override
						public void onLastItemVisible() {
							// Toast.makeText(getActivity(), "end page",
							// Toast.LENGTH_SHORT).show();
						}
					});

			// Listener to get selected id when list item clicked
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					DatabaseUtility.insertDeals(getActivity(),
							mArrDeals.get(position - 1));

					Log.d("", "DATA " + "Successfully");

					// }
					// Pass id to onListSelected method on HomeActivity
					// mCallback.onListSelected(item.get(userFunction.key_deals_id));
					setStatusPrivateEvent(getActivity(),"Event_List_Status",mArrDeals.get(position - 1).getDeal_id());
					Intent i = new Intent(getActivity(), ActivityDetail.class);
					i.putExtra(utils.EXTRA_DEAL_ID, mArrDeals.get(position - 1)
							.getDeal_id());
					startActivity(i);
					getActivity().overridePendingTransition(
							R.anim.slide_in_left, R.anim.slide_out_left);

					// Set the item as checked to be highlighted when in
					// two-pane
					// layout
					list.setItemChecked(position, true);
				}
			});

			list.setOnItemLongClickListener(new OnItemLongClickListener() {

				@Override
				public boolean onItemLongClick(AdapterView<?> parent,
						View view, int position, long id) {
					// TODO Auto-generated method stub
					if (GlobalValue.myClient != null
							|| GlobalValue.myUser != null) {
						showOperationDialog(mArrDeals.get(position - 1));
					}
					return true;
				}
			});

			// initNotBoringActionBar();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return v;
	}

	private void showOperationDialog(final DealObj event) {
		final Dialog dialog = new Dialog(self);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_operation_event);
		dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
				WindowManager.LayoutParams.WRAP_CONTENT);

		// RippleView rplCopy = (RippleView) dialog.findViewById(R.id.rpl_copy);
		TextViewRobotoCondensedRegular copy = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_copy);
		TextViewRobotoCondensedRegular delete = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_delete);
		TextViewRobotoCondensedRegular revoke = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_revoke);
		TextViewRobotoCondensedRegular scan = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_scan);
		TextViewRobotoCondensedRegular edit = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_edit);

		// Show/hide operation
		if (GlobalValue.myUser == null) {
			if (!GlobalValue.myClient.getUsername().equals(event.getUsername())) {
				delete.setVisibility(View.GONE);
				revoke.setVisibility(View.GONE);
				scan.setVisibility(View.GONE);
				edit.setVisibility(View.GONE);
			}
		}

		// Set listener
		copy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(self, "Copied", Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			}
		});

		delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(self, "Deleted", Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			}
		});

		revoke.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(self, "Revoked", Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			}
		});

		scan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(self, "Scanned", Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			}
		});

		edit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Open edit event page
				Intent i = new Intent(self, AddEventActivity.class);
				i.putExtra("id", event.getDeal_id());
				startActivity(i);

				// Dismiss dialog
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.actions, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	// Load first 10 videos
	private class loadFirstListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			ActivityHome.rltProgress.setVisibility(View.VISIBLE);
		}

		protected Void doInBackground(Void... unused) {
			try {
				if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
					getDataFromServer();
				} else {
					getDataFromDB();
				}
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			}
			return null;
		}

		protected void onPostExecute(Void unused) {
			Log.d(TAG, "loadFirstListView: " + mArrDeals.size());
			// Condition if data length uder 10 button loadMore is remove
			if (intLengthData < UserFunctions.valueItemsPerPage) {
				list.removeFooterView(btnLoadMore);
			} else {
				list.addFooterView(btnLoadMore);
			}
			if (isAdded()) {
				if (mArrDeals.size() != 0) {
					// Check paramter notif
					int paramNotif = utils
							.loadPreferences(utils.UTILS_PARAM_NOTIF);

					// Condition if app start in the first time notif will run
					// in background
					if (paramNotif != 1) {
						utils.saveString(utils.UTILS_NOTIF, mArrDeals.get(0)
								.getDeal_id());
						utils.savePreferences(utils.UTILS_PARAM_NOTIF, 1);
						startService();
					}

					// Adding load more button to lisview at bottom
					lytRetry.setVisibility(View.GONE);
					list.setVisibility(View.VISIBLE);
					lblNoResult.setVisibility(View.GONE);
					// Getting adapter
					List<String> status=getStringArrayPref(getActivity(),"Event_List_Status",mArrDeals);
					mla = new HomeAdapter(getActivity(), mArrDeals,status);
					mla.notifyDataSetChanged();
					list.setAdapter(mla);
					mPullRefreshListView.onRefreshComplete();

				} else {
					list.removeFooterView(btnLoadMore);
					if (json != null) {
						lblNoResult.setVisibility(View.VISIBLE);
						lytRetry.setVisibility(View.GONE);

					} else {
						lblNoResult.setVisibility(View.GONE);
						lytRetry.setVisibility(View.VISIBLE);
						Toast.makeText(getActivity(),
								getString(R.string.no_connection),
								Toast.LENGTH_SHORT).show();
					}
				}
			}
			try {
				super.onPostExecute(unused);
				// Closing progress dialog

				// pDialog.dismiss();
				ActivityHome.rltProgress.setVisibility(View.GONE);
			} catch (NullPointerException ex) {
				throw new RuntimeException(ex);
			} catch (Exception ex) {
				throw new RuntimeException(ex);
			}
		}
	}

	// Load more videos
	private class loadMoreListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			ActivityHome.rltProgress.setVisibility(View.VISIBLE);
		}

		protected Void doInBackground(Void... unused) {

			// Store previous value of current page
			mPreviousPage = mCurrentPage;
			// Increment current page
			mCurrentPage += userFunction.valueItemsPerPage;
			if (NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
				getDataFromServer();
			} else {
				getDataFromDB();
			}
			return (null);
		}

		protected void onPostExecute(Void unused) {
			// Condition if data length uder 10 button loadMore is remove
			if (intLengthData < userFunction.valueItemsPerPage)
				list.removeFooterView(btnLoadMore);
			if (json != null) {
				// Get listview current position - used to maintain scroll
				// position
				int currentPosition = list.getFirstVisiblePosition();
				lytRetry.setVisibility(View.GONE);
				// Appending new data to menuItems ArrayList
				List<String> status=getStringArrayPref(getActivity(),"Event_List_Status",mArrDeals);
				mla = new HomeAdapter(getActivity(), mArrDeals,status);
				mla.notifyDataSetChanged();
				// list.setAdapter(mla);
				// Setting new scroll position
				list.setSelectionFromTop(currentPosition + 1, 0);
				// mPullRefreshListView.onRefreshComplete();

			} else {
				if (mArrDeals != null) {
					mCurrentPage = mPreviousPage;
					lytRetry.setVisibility(View.GONE);
				} else {
					lytRetry.setVisibility(View.VISIBLE);
					Toast.makeText(getActivity(),
							getString(R.string.no_connection),
							Toast.LENGTH_SHORT).show();
				}

			}
			ActivityHome.rltProgress.setVisibility(View.GONE);
		}
	}

	// Method to get Data from Server
	public void getDataFromServer() {

		try {
			if (GlobalValue.myClient != null) {
				client = GlobalValue.myClient.getUsername();
			} else {
				client = "";
			}
			jsonCurrency = userFunction.currency(getActivity());
			json = userFunction
					.latestDeals(client, mCurrentPage, getActivity());
			ArrayList<DealObj> arr = JSONParser.parserDeal("latestDeals",
					json.toString());
			mArrDeals.addAll(arr);
			intLengthData = mArrDeals.size();
			if (jsonCurrency != null) {
				JSONArray currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);

			}

			Log.d(TAG, "getDataFromServer: " + mArrDeals.size());

		} catch (JSONException ex) {
			throw new RuntimeException(ex);
		} catch (NullPointerException ex) {
			throw new RuntimeException(ex);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public void getDataFromDB() {
		try {
			String apiDeals = UserFunctions.URL_ALL_DEALS + mCurrentPage;
			APIObj allDealInfos = DatabaseUtility.getResuftFromApi(
					getActivity(), apiDeals);
			Log.d("FragmentHome", "Database: " + allDealInfos);
			String jsonAllDeal = "";
			if (allDealInfos != null) {
				jsonAllDeal = allDealInfos.getmResult();
			}
			ArrayList<DealObj> arr = JSONParser.parserDeal("latestDeals",
					json.toString());
			mArrDeals.addAll(arr);
			intLengthData = mArrDeals.size();
			Log.d("url", "All deal: " + jsonAllDeal);

			APIObj currencyInfos = DatabaseUtility.getResuftFromApi(
					getActivity(), UserFunctions.URL_CURRENCY);
			Log.d("FragmentHome", "Database: " + currencyInfos);
			String jsonCurr = "";
			if (currencyInfos != null) {
				jsonCurr = currencyInfos.getmResult();
			}
			jsonCurrency = new JSONObject(jsonCurr);
			Log.d("url", "Currency: " + jsonCurr);

			if (jsonCurrency != null) {
				JSONArray currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);
			}

		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (NullPointerException ex) {
			throw new RuntimeException(ex);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		list.setAdapter(null);
		super.onDestroy();

	}

	public void startService() {

		Intent myIntent = new Intent(getActivity(), ServiceNotification.class);

		PendingIntent pendingIntent = PendingIntent.getService(getActivity(),
				0, myIntent, 0);

		AlarmManager alarmManager = (AlarmManager) getActivity()
				.getSystemService(getActivity().ALARM_SERVICE);

		Calendar calendar = Calendar.getInstance();
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				calendar.getTimeInMillis(), 30 * 1000, pendingIntent);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == llMenu) {
			ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
		}
	}


	private void setStatusPrivateEvent(android.content.Context context, String key,String deal_id)
	{
		//Event_List_Status-key
		try
		{
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
			SharedPreferences.Editor editor = prefs.edit();
			JSONArray a;
			try
			{
				String json = prefs.getString(key, null);
				a = new JSONArray(json);
			}
			catch (Exception e)
			{
				a = new JSONArray();
				e.printStackTrace();
			}

			ListEventObj listEventObj = null;
			JSONObject b=new JSONObject();

			try
			{
				b.put("event_id", deal_id);

			}
			catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			a.put(b);

			if (a!=null)
			{
				editor.putString(key, a.toString());
			}
			else
			{
				editor.putString(key, null);
			}
			editor.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public List<String> getStringArrayPref(Context context, String key, List<DealObj> list)
	{
		List<String> listEventStatus=new ArrayList<>(list.size());
		for (int i = 0; i < list.size(); i++)
		{
			listEventStatus.add("0");
		}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		String json = prefs.getString(key, null);
		List<ListEventObj> listEvents=new ArrayList<>();
		if (json != null)
		{
			try
			{
				JSONArray a = null;
				try
				{
					a = new JSONArray(json);
					for (int i = 0; i < list.size(); i++)
					{
						DealObj ss=list.get(i);
						for(int j=0;j<a.length();j++)
						{
							JSONObject jsonObject=a.getJSONObject(j);
							if(ss.getDeal_id().equalsIgnoreCase(jsonObject.getString("event_id")))
							{
								listEventStatus.set(i,ss.getDeal_id());
							}
							else
							{
								if(listEventStatus.get(i).equalsIgnoreCase("0"))
								{
									listEventStatus.set(i,"0");
								}
							}

						}
					}
				}
				catch (JSONException e)
				{
					e.printStackTrace();
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return listEventStatus;
	}
}
