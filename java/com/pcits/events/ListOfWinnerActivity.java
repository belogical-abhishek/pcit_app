/**
 * File        : ActivityAbout.java
 * App name    : M1SO Events
 * Version     : 1.1.4
 * Created     : 25/05/14

 * Created by Team M1SO on 25/01/14.
 * Copyright (c) 2014 M1SO. All rights reserved.
 */

package com.pcits.events;

import java.util.ArrayList;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.pcits.common.utils.Logging;
import com.pcits.events.adapters.WinnerAdapter;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.WinnerObj;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.widgets.CircleImageWithBorder;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class ListOfWinnerActivity extends FragmentActivity {

	private static final String TAG = "ListOfTicketActivity";
	private ListOfWinnerActivity self;
	// Create an instance of ActionBar
	private android.app.ActionBar actionbar;

	private ListView mLvWinner;
	private ArrayList<WinnerObj> mArrWinner;
	private WinnerAdapter mWinnerAdapter;
	private Display display;

	private String mDealId = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		try{
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_list_of_winner);
	
			self = this;
			display = getWindowManager().getDefaultDisplay();
	
			Bundle b = getIntent().getExtras();
			if (b != null) {
				mDealId = b.getString("dealId");
				Log.e(TAG, "Deal id: " + mDealId);
			}
	
			// Connect view objects and xml ids
			// Get ActionBar and set back button on actionbar
			actionbar = getActionBar();
			actionbar.setDisplayHomeAsUpEnabled(true);
	
			initUI();
			getData();
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	// Listener for option menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// Previous page or exit
			finish();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void initUI() {
		mLvWinner = (ListView) findViewById(R.id.lv_winner);

		// Should call this method at end of
		initControl();
	}

	private void initControl() {
		mLvWinner.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				showDetailDialog(mArrWinner.get(position));
			}
		});
	}

	private void getData() {
		try{
			ModelManager.getListOfWinner(self, mDealId, true,
					new ModelManagerListener() {
	
						@Override
						public void onSuccess(Object object) {
							String json = (String) object;
							mArrWinner = ParserUtility.getListWinner(json);
							mWinnerAdapter = new WinnerAdapter(self, mArrWinner);
							mLvWinner.setAdapter(mWinnerAdapter);
						}
	
						@Override
						public void onError() {
						}
					});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void showDetailDialog(WinnerObj obj) {
		final Dialog dialog = new Dialog(self);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_winner_detail);
		dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);

		CircleImageWithBorder avatar = (CircleImageWithBorder) dialog
				.findViewById(R.id.avt_winner);
		TextViewRobotoCondensedRegular lblDialogTitle = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_dialog_title);
		TextViewRobotoCondensedRegular lblTitle = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_title);
		TextViewRobotoCondensedRegular lblCompany = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_company);
		TextViewRobotoCondensedRegular lblGCM = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_gcm);
		TextViewRobotoCondensedRegular lblUniqueCode = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_uniqueCode);
		TextViewRobotoCondensedRegular lblValue = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_value);
		TextViewRobotoCondensedRegular lblFullName = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_fullName);
		TextViewRobotoCondensedRegular lblGender = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_gender);
		TextViewRobotoCondensedRegular lblDob = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_dob);
		TextViewRobotoCondensedRegular lblAddress = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_address);
		TextViewRobotoCondensedRegular lblStatus = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_status);

		// Set data
		AQuery aq = new AQuery(self);
		if (!obj.getAvatar().equals("")) {
			aq.id(avatar).image(UserFunctions.URLAdmin + obj.getAvatar(),
					false, true);
		} else {
			avatar.setImageResource(R.drawable.avatar2);
		}

		lblDialogTitle.setText(obj.getFullName() + "'s information");
		lblTitle.setText(obj.getTitle());
		lblCompany.setText(obj.getCompany());
		lblGCM.setText(obj.getGcmId());
		lblUniqueCode.setText(obj.getUniqueCode());
		lblValue.setText(obj.getValue());
		lblFullName.setText(obj.getFullName());
		lblGender.setText(obj.getGender());
		lblDob.setText(DateTimeUtility.convertStringToDate(obj.getAge(),
				"yyyy-MM-dd", "MMM dd yyyy"));
		lblAddress.setText(obj.getAddress());
		if (obj.getStatus().equals("0")) {
			lblStatus.setText("Unclaimed");
		} else {
			lblStatus.setText("Claimed");
		}

		Button btnClose = (Button) dialog.findViewById(R.id.btn_close);

		btnClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}
}