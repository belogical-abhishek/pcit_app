package com.pcits.events;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.common.utils.GsonUtility;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.modelmanager.NewBill;
import com.pcits.events.modelmanager.SaveSplitRequest;
import com.pcits.events.modelmanager.SplitClients;
import com.pcits.events.network.EventsAPI;
import com.pcits.events.network.RestClient;
import com.pcits.events.obj.SplitShare;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivitySplitAmount extends AppCompatActivity implements TextView.OnEditorActionListener {

    private String numberOfPeople, amount, title;
    private TextView mTxtAmount;
    private EditText mEdtShare;
    private int mNoOfPeople;
    private String deal_id;
    private ProgressDialog mProgressDialog;
    private double mAmount;
    private List<EditText> mEdtList;
    private List<Boolean> isEdited;
    private double mDividedAmount;
    private Toolbar mToolBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_split_amount);

        getData();
        initUI();


        addview(numberOfPeople);


    }

    private void initUI() {

        mTxtAmount = (TextView) findViewById(R.id.txt_amount);
        mTxtAmount.setText(amount);

        mToolBar = (Toolbar) findViewById(R.id.toolbar_SplitAmount);

        mToolBar.setTitle("");
        setSupportActionBar(mToolBar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrow_left);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.black));
        }

    }

    private void getData() {

        Intent intent = getIntent();
        numberOfPeople = intent.getStringExtra("numberOfPeople");
        amount = intent.getStringExtra("amount");
        title = intent.getStringExtra("title");
        deal_id=intent.getStringExtra("dealId");

        mNoOfPeople = Integer.parseInt(numberOfPeople);
        mAmount = round(Double.parseDouble(amount),2);
    }

    private void addview(String numberOfPeople) {
       //create dynamic view according to number of people
        if (mEdtList != null) {
            mEdtList.clear();
        }

        int noOfPeople = Integer.parseInt(numberOfPeople);
        LinearLayout lm = (LinearLayout) findViewById(R.id.ll_share);


        mEdtList = new ArrayList<EditText>();
        isEdited = new ArrayList<>();
        double finalValue=0.0,totalFinalValue=0.0;
        for (int i = 0; i < noOfPeople; i++)
        {

            LinearLayout ll = new LinearLayout(ActivitySplitAmount.this);
            LinearLayout.LayoutParams ll_params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            ll.setWeightSum(2);
            ll.setLayoutParams(ll_params);
            ll.setOrientation(LinearLayout.HORIZONTAL);


            TextView txtname = new TextView(ActivitySplitAmount.this);
            LinearLayout.LayoutParams txt_params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            txt_params.gravity = Gravity.RIGHT | Gravity.TOP;
            txt_params.weight = 1;
            txt_params.setMargins(6, 6, 6, 6);
            int j = i;
            j++;
            txtname.setText("Share of member " + j + "");
            txtname.setLayoutParams(txt_params);
            ll.addView(txtname);

            mEdtShare = new EditText(ActivitySplitAmount.this);
            mEdtShare.setId(i);
            mDividedAmount = mAmount / noOfPeople;
            DecimalFormat df=new DecimalFormat("0.00");
            String formate = df.format(mDividedAmount);
            try
            {
                finalValue = (Double)df.parse(formate) ;
                totalFinalValue=totalFinalValue+finalValue;
                mEdtShare.setText(round(finalValue,2) + "");
            }
            catch (ParseException e)
            {
                e.printStackTrace();
            }
            LinearLayout.LayoutParams edt_params = new LinearLayout.LayoutParams(100, ViewGroup.LayoutParams.WRAP_CONTENT);
            edt_params.gravity = Gravity.START | Gravity.TOP;
            edt_params.weight = 1;
            mEdtShare.setSingleLine();
            mEdtShare.setInputType(InputType.TYPE_CLASS_NUMBER);
            edt_params.setMargins(6, 6, 6, 6);
            mEdtShare.setLayoutParams(edt_params);
            ll.addView(mEdtShare);
            mEdtShare.setTag(false);
            mEdtList.add(mEdtShare);
            isEdited.add(false);   //boolean to check if value in edit text is edited
            mEdtShare.setTag(false);
            mEdtShare.setOnEditorActionListener(this);

            if(i==noOfPeople-1)
            {
                double remain=mAmount-totalFinalValue;
                double val=finalValue+remain;
                mEdtShare.setText(String.valueOf(val));
            }
            lm.addView(ll);

        }

        Button btnDone = new Button(ActivitySplitAmount.this);
        LinearLayout.LayoutParams btn_params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        btn_params.weight = 1;
        btn_params.setMargins(6, 6, 6, 6);
        btnDone.setText("Done");
        btnDone.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        btnDone.setTextColor(getResources().getColor(R.color.white));
        btnDone.setLayoutParams(btn_params);
        lm.addView(btnDone);
        btnDone.setOnClickListener(onClickListener);

    }

    private void addBillToDeal()
    {
        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsAPI = retrofit.create(EventsAPI.class);
        mProgressDialog = new ProgressDialog(ActivitySplitAmount.this);
        mProgressDialog.setMessage("Saving bill ..");
        mProgressDialog.show();

        NewBill newBill = new NewBill();
        newBill.setAmount((float) mAmount);
        newBill.setPaidBy(GlobalValue.myClient.getEmail());
        newBill.setClientId(Integer.valueOf(GlobalValue.myClient.getId()));

        Call<Integer> addbill = eventsAPI.AddBill("http://54.173.65.38/events-api/public/bills/create/"+deal_id,newBill);
        addbill.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if(response.isSuccessful()){
                    mProgressDialog.dismiss();
                  //  saveSplitedBill(response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });

    }

   /* private SaveSplitRequest getBillRequestData()
    {
        SaveSplitRequest request = new SaveSplitRequest();
        ArrayList<SplitClients> clientList = new ArrayList<>();
       // Log.d(TAG, "getBillRequestData: "+ GsonUtility.convertObjectToJSONString(mSplitShareList));
        for(SplitShare s:mSplitShareList)
        {
            SplitClients client = new SplitClients();
            client.setEmail(""+s.getName());
            client.setAmount(""+s.getAmount());
            clientList.add(client);
        }
        //Log.d(TAG, "getBillRequestData: "+clientList);

        request.setSplitlist(clientList);
        return request;

    }*/
    /*private void saveSplitedBill(String billId){
        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsAPI = retrofit.create(EventsAPI.class);
        mProgressDialog = new ProgressDialog(ActivitySplitAmount.this);
        mProgressDialog.setMessage("Saving share..");
        mProgressDialog.show();
       // SaveSplitRequest saveRequest = getBillRequestData();
        SaveSplitRequest saveRequest=null;
        Call<Boolean> saveSplit = eventsAPI.saveBillSplit("http://54.173.65.38/events-api/public/bills/split/"+billId,saveRequest);
        saveSplit.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response)
            {
               // Log.d(TAG, "onResponse: "+response.toString());
                if(response.body().equals(true))
                {
                   // Toast.makeText(mContext, "Bill Saved", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                    Intent intent = new Intent(ActivitySplitBills.this, ActivityHome.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
                else
                    {
                  //  Toast.makeText(ActivitySplitBills.this, "Something went wrong, could not save bill", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

                t.printStackTrace();
                Toast.makeText(ActivitySplitBills.this, "Bill Saved", Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }*/


    private View.OnClickListener onClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
             //check if there is any amount remaining after splitting
            try
            {

                double currentAmount = 0, remianingAmount = 0;

                for (int i = 0; i < mNoOfPeople; i++)
                {
                    currentAmount = currentAmount + round(Double.parseDouble(mEdtList.get(i).getText().toString()),2);
                    mEdtList.get(i).setText(String.valueOf(currentAmount));
                }
                double amtAdmin=Double.parseDouble(mEdtList.get(0).getText().toString());
                remianingAmount =round(mAmount - currentAmount,2);
                amtAdmin=amtAdmin+remianingAmount;
                mEdtList.get(0).setText(String.valueOf(amtAdmin));

                /*if (round(currentAmount,2) > mAmount)
                {
                    Toast.makeText(ActivitySplitAmount.this, "Remaining Amount " + remianingAmount + "", Toast.LENGTH_SHORT).show();

                }
                else if (round(currentAmount,2) < mAmount)
                {
                    Toast.makeText(ActivitySplitAmount.this, "Remaining Amount " + remianingAmount + "", Toast.LENGTH_SHORT).show();
                }
                else if (round(currentAmount,2) == mAmount)
                {
                    Toast.makeText(ActivitySplitAmount.this, "Done", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ActivitySplitAmount.this, ActivitySplitBills.class);
                    intent.putExtra("title", title);
                    intent.putExtra("amount", amount);
                    startActivity(intent);
                    finish();
                }*/
                Toast.makeText(ActivitySplitAmount.this, "Done", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ActivitySplitAmount.this, ActivitySplitBills.class);
                intent.putExtra("title", title);
                intent.putExtra("amount", amount);
                startActivity(intent);
                finish();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    };

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

        try {

            if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                    actionId == EditorInfo.IME_ACTION_NEXT ||
                    actionId == EditorInfo.IME_ACTION_DONE ||
                    event.getAction() == KeyEvent.ACTION_DOWN &&
                            event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

                double edtAmount = Double.parseDouble(v.getText().toString());


                if (edtAmount > mAmount) {
                    Toast.makeText(this, "Please Enter amount less than total Amount", Toast.LENGTH_SHORT).show();
                } else {
                        if(!isEdited() || isEdited.get(v.getId())) {
                            isEdited.set(v.getId(), true);

                            calculateAmount(v.getId(), edtAmount);
                        }
                        else {
                            Toast.makeText(this, "Cannot Edit", Toast.LENGTH_SHORT).show();
                        }


                    // setEditTextValues(v.getId(), edtAmount);
                }
                return true; // consume.

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    private void calculateAmount(int id, double currentAmount) {

        try {
            double remainig = 0, sum = 0;
            int remainingPeople = 0;

            for (int i = 0; i < mEdtList.size(); i++) {

                if (mEdtList.get(i).getId() != id) {

                    sum = sum + Double.parseDouble(mEdtList.get(i).getText().toString());

                }
            }

            remainig = mAmount - (sum + currentAmount);
            for (int i = 0; i < isEdited.size(); i++) {
                if (!isEdited.get(i)) {
                    remainingPeople++;
                }
            }
            if (0 < remainig) {

                remainig = round(remainig / remainingPeople,2);
                for (int i = 0; i < mEdtList.size(); i++) {

                        if(mEdtList.get(i).getId()!=id  && !isEdited.get(i)) {
                            double currentEdtAmount = Double.parseDouble(mEdtList.get(i).getText().toString());
                            currentEdtAmount = currentEdtAmount + remainig;
                            mEdtList.get(i).setText(round(currentEdtAmount,2) + "");
                        }
                }


            } else if (0 > remainig) {

                remainig = round(remainig / remainingPeople,2);
                for (int i = 0; i < mEdtList.size(); i++) {

                        if(mEdtList.get(i).getId()!=id && !isEdited.get(i)) {

                            double currentEdtAmount = Double.parseDouble(mEdtList.get(i).getText().toString());
                            currentEdtAmount = currentEdtAmount + remainig;
                            if (0 < currentEdtAmount)

                                mEdtList.get(i).setText(round(currentEdtAmount,2) + "");
                            else if(0>currentEdtAmount)
                                Toast.makeText(this, "Sum is greater than total amount", Toast.LENGTH_SHORT).show();
                        }
                    }

            }
//            else if(remainig==0){
//                for (int i = 0; i < mEdtList.size(); i++) {
//                if(mEdtList.get(i).getId()!=id && !isEdited.get(i)) {
//                    mEdtList.get(i).setText("0");
//                }
//                }
//            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean isEdited(){

        int remainingPeople=0,editedPeople=0;
        for(int i=0;i<isEdited.size();i++){
            if(isEdited.get(i)){
                editedPeople++;
            }
            else {
                remainingPeople++;
            }
        }
        if(remainingPeople==1){
            return true;
        }
        else
        {
            return false;
        }
    }
    //roundoff double upto 2 decimals
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}