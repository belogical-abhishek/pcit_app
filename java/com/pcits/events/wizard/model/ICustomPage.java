package com.pcits.events.wizard.model;

public interface ICustomPage {

	public void saveState();

	public void restoreState();

}
