package com.pcits.events.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.pcits.common.utils.Logging;
import com.pcits.common.utils.RuntimePermissions;
import com.pcits.events.ActivityDetail;
import com.pcits.events.ActivityHome;
import com.pcits.events.R;
import com.pcits.events.adapters.AdapterCompany;
import com.pcits.events.adapters.AdapterFilterCategory;
import com.pcits.events.adapters.HomeAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.libraries.JSONParser;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.DealObj;
import com.pcits.events.obj.ListEventObj;
import com.pcits.events.utils.RangeSeekBar;
import com.pcits.events.utils.RangeSeekBar.OnRangeSeekBarChangeListener;
import com.pcits.events.utils.Utils;

public class FragmentFilter extends Fragment implements
        OnClickListener, OnFocusChangeListener, LocationListener, OnMapReadyCallback {

    private static final String TAG = "FragmentFilter";
    // Declare view objects
    private EditText txtMinPrice, txtMaxPrice, txtKM, txtMinDiscount,
            txtMaxDiscount;
    private AutoCompleteTextView txtCity, txtCounty, txtAddress, txtTitle;
    private SeekBar seekBarKM = null;
    private Button btnFind, btnBack, btnLoadMore, btnRetry;
    private LinearLayout layFilter, layResult, layLocation, layRadius,
            layCompany, layStartDate, layPrice, layRangseekBar, lytRetry,
            layDiscount, layCategory, layRangeSliderDiscount, layTitle;
    private CheckBox cbTitle, cbLocation, cbRadius, cbPrice, cbDiscount,
            cbCategory, cbCompany, cbStartDate;
    private TextView lblNoResult, lblCategory, lblStartDate, lblCompany;
    private ProgressDialog pDialog;
    private int mCurrentPage = 0;
    private int mPreviousPage;
    private View view;
    private RangeSeekBar<Integer> seekBarPrice, seekBarDiscount;
    // Create instance of list and ListAdapter
    private ArrayList<String> titleItems = new ArrayList<String>();
    private ArrayList<String> addressItems = new ArrayList<String>();
    private ArrayList<String> cityItems = new ArrayList<String>();
    private ArrayList<String> countyItems = new ArrayList<String>();
    private ArrayList<DealObj> mArrDeals;
    private ArrayAdapter<String> adapterAddress;
    private ArrayAdapter<String> adapterCity;
    private ArrayAdapter<String> adapterCounty;
    private ArrayAdapter<String> adapterTitle;
    // Declare object of userFunctions and Utils class
    private UserFunctions userFunction = new UserFunctions();
    private Utils utils;
    // create array variables to store data
    private String[] mAutoTitle;
    private String[] mAddress;
    private String[] mCity;
    private String[] mCounty;
    private String[] mCompany;

    private int intLengthData;
    public static String[] Categories;
    // category
    public static String[] sCategoryId;
    public static String[] sCategoryName;
    public static String[] sIcMarker;

    private JSONObject json;

    private ListView list;
    private HomeAdapter sla;
    private String title, min, max, company, address, city, county, category,
            startDate, mindiscount, maxdiscount, distance;
    private int type;

    private LatLng mLatLng;
    private double lat;
    private double lnt;
    private LocationManager locationManager;
    private GoogleMap map;
    private MapView mapView;
    // Create instance of list and ListAdapter
    private AdapterFilterCategory adapterFilterCategory;
    private AdapterCompany adapterCompany;
    private ArrayList<HashMap<String, String>> categoryItems = new ArrayList<HashMap<String, String>>();
    private ArrayList<HashMap<String, String>> companyItems = new ArrayList<HashMap<String, String>>();
    // date time
    private Calendar mCal;
    private String strDate;
    private SimpleDateFormat sdf_date;
    private LinearLayout llMenu;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.clear();
        map.setIndoorEnabled(true);
        if (RuntimePermissions.hasLocationPermission(getActivity())) {
            map.setMyLocationEnabled(true);
        }
        LocationManager locManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        Criteria crit = new Criteria();

        List<String> providers = locManager.getProviders(true);
        Location bestLocation = null;
        String bestProvider = "network";
        for (String provider : providers) {
            Location l = locManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
                bestProvider = provider;
            }
        }

        if (bestLocation != null) {
            onLocationChanged(bestLocation);
        }

        locationManager.requestLocationUpdates(bestProvider, 20000, 0,
                this);

        // Stop requesting location.
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }

    }

    // SupportMapFragment fragment;

    // Declare NotBoringActionBar
    // private KenBurnsView mHeaderPicture;

    // Declare OnListSelected interface
    public interface OnListSelectedListener {
        public void onListSelected(String idSelected);
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        view = inflater.inflate(R.layout.fragment_filter, container, false);
        try {
            if (mArrDeals == null) {
                mArrDeals = new ArrayList<DealObj>();
            }

            // fragment = ((SupportMapFragment)
            // getFragmentManager().findFragmentById(
            // R.id.maps));
            utils = new Utils(getActivity());
            mapView = (MapView) view.findViewById(R.id.mapView);
            mapView.onCreate(savedInstanceState);
            mCal = Calendar.getInstance();
            sdf_date = new SimpleDateFormat("yyyy-MM-dd");
            // Method to get Data from Server
            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        // Put in code over here that does the network related
                        // operations.
                        getDataListCompany();
                        getDataShowLocation();
                        getDataListCategory();
                    } catch (Exception ex) {
                        Logging.writeExceptionFromStackTrace(ex,
                                ex.getMessage());
                    }
                    return null;
                }

            };
            task.execute();
            initUI();
            initControl();
            // initNotBoringActionBar();
        } catch (Exception ex) {
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
        return view;
    }

    // private void initNotBoringActionBar() {
    // mHeaderPicture = (KenBurnsView) view.findViewById(R.id.header_picture);
    // mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);
    // }

    private void initUI() {
        // Connect view objects and xml ids
        lytRetry = (LinearLayout) view.findViewById(R.id.lytRetry);
        btnRetry = (Button) view.findViewById(R.id.btnRetry);
        lblNoResult = (TextView) view.findViewById(R.id.lblNoResult);
        list = (ListView) view.findViewById(R.id.lsvResultFilter);
        seekBarPrice = new RangeSeekBar<Integer>(0, 5000, getActivity());
        seekBarDiscount = new RangeSeekBar<Integer>(0, 5000, getActivity());
        seekBarKM = (SeekBar) view.findViewById(R.id.seekBar);

        txtKM = (EditText) view.findViewById(R.id.txtKM);
        txtMinPrice = (EditText) view.findViewById(R.id.txtMinprice);
        txtMaxPrice = (EditText) view.findViewById(R.id.txtMaxprice);
        txtCity = (AutoCompleteTextView) view.findViewById(R.id.txtCity);
        txtCounty = (AutoCompleteTextView) view.findViewById(R.id.txtCounty);
        txtAddress = (AutoCompleteTextView) view.findViewById(R.id.txtAddress);
        txtTitle = (AutoCompleteTextView) view.findViewById(R.id.txtTitle);

        txtMinPrice.setHint("No min");
        txtMaxPrice.setHint("No max");
        txtMaxDiscount = (EditText) view.findViewById(R.id.txtMaxDiscount);
        txtMinDiscount = (EditText) view.findViewById(R.id.txtMinDiscount);
        txtMaxDiscount.setText("0");
        txtMinDiscount.setText("0");

        lblCategory = (TextView) view.findViewById(R.id.lblCategory);
        lblStartDate = (TextView) view.findViewById(R.id.lblStartDate);
        lblCompany = (TextView) view.findViewById(R.id.lblFilterCompany);

        btnFind = (Button) view.findViewById(R.id.btnFind);
        layCategory = (LinearLayout) view.findViewById(R.id.layCategory);
        layDiscount = (LinearLayout) view.findViewById(R.id.layDiscount);
        layFilter = (LinearLayout) view.findViewById(R.id.layFilter);
        layResult = (LinearLayout) view.findViewById(R.id.layResult);
        layLocation = (LinearLayout) view.findViewById(R.id.layLocation);
        layRadius = (LinearLayout) view.findViewById(R.id.layRadius);
        layPrice = (LinearLayout) view.findViewById(R.id.layPrice);
        layRangseekBar = (LinearLayout) view.findViewById(R.id.layRangseekBar);
        layRangeSliderDiscount = (LinearLayout) view
                .findViewById(R.id.layRangSliderDiscount);
        layCompany = (LinearLayout) view.findViewById(R.id.layCompany);
        layStartDate = (LinearLayout) view.findViewById(R.id.layStartDate);
        layTitle = (LinearLayout) view.findViewById(R.id.layTitle);
        llMenu = (LinearLayout) view.findViewById(R.id.llMenu);
        llMenu.setOnClickListener(this);

        cbTitle = (CheckBox) view.findViewById(R.id.chekTitle);
        cbLocation = (CheckBox) view.findViewById(R.id.chekLocation);
        cbRadius = (CheckBox) view.findViewById(R.id.chekRadius);
        cbPrice = (CheckBox) view.findViewById(R.id.chekPrice);
        cbCategory = (CheckBox) view.findViewById(R.id.chekCategory);
        cbDiscount = (CheckBox) view.findViewById(R.id.chekDiscount);
        cbCompany = (CheckBox) view.findViewById(R.id.chekCompany);
        cbStartDate = (CheckBox) view.findViewById(R.id.chekStartDate);
        btnBack = (Button) view.findViewById(R.id.btnback);

        // Create LoadMore button
        // Set text lblNoResult
        lblNoResult.setText(getString(R.string.lbl_no_result));

        userFunction = new UserFunctions();

        btnLoadMore = new Button(getActivity());
        btnLoadMore
                .setBackgroundResource(R.drawable.apptheme_btn_default_holo_light);
        btnLoadMore.setText(getString(R.string.btn_load_more));
        btnLoadMore.setTextColor(getResources().getColor(R.color.text_btn));

        // Listener to handle load more buttton when clicked
        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                try {
                    // Starting a new async task
                    json = null;
                    new loadMoreListView().execute();
                } catch (Exception ex) {
                    Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
                }
            }
        });

        // Listener to get selected id when list item clicked
        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                // TODO Auto-generated method stub

                GlobalValue.dealsObj = mArrDeals.get(position);
                DatabaseUtility.insertDeals(getActivity(),
                        mArrDeals.get(position));

                ModelManager.updateClickCount(getActivity(),
                        mArrDeals.get(position).getDeal_id(), true,
                        new ModelManagerListener() {

                            @Override
                            public void onSuccess(Object object) {
                                try {
                                    // TODO Auto-generated method stub
                                    String json = (String) object;
                                    boolean result = ParserUtility
                                            .updateAccount(json);
                                    if (result) {
                                        Log.d(TAG, "Successfully");
                                    }
                                } catch (Exception ex) {
                                    throw new RuntimeException(ex);
                                }
                            }

                            @Override
                            public void onError() {
                                // TODO Auto-generated method stub

                            }
                        });

                // }
                // Pass id to onListSelected method on HomeActivity
                // mCallback.onListSelected(item.get(userFunction.key_deals_id));
                setStatusPrivateEvent(getActivity(),"Event_List_Status",mArrDeals.get(position).getDeal_id());
                Intent i = new Intent(getActivity(), ActivityDetail.class);
                // i.putExtra(utils.EXTRA_DEAL_ID, mArrDeals.get(position - 1)
                // .getDeal_id());
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.slide_in_left,
                        R.anim.slide_out_left);

                // Set the item as checked to be highlighted when in two-pane
                // layout
                list.setItemChecked(position, true);
            }
        });

        // auto complete title
        titleItems = new ArrayList<String>();
        adapterTitle = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, titleItems);
        txtTitle.setAdapter(adapterTitle);
        // auto complete address
        addressItems = new ArrayList<String>();
        adapterAddress = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, addressItems);
        txtAddress.setAdapter(adapterAddress);
        // auto complete city
        cityItems = new ArrayList<String>();
        adapterCity = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, cityItems);
        txtCity.setAdapter(adapterCity);
        // auto complete county
        countyItems = new ArrayList<String>();
        adapterCounty = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, countyItems);
        txtCounty.setAdapter(adapterCounty);
    }

    private void initControl() {
        try {
            // seeber price
            seekBarPrice
                    .setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() {

                        @Override
                        public void onRangeSeekBarValuesChanged(
                                RangeSeekBar<?> bar, Integer minValue,
                                Integer maxValue) {
                            // TODO Auto-generated method stub
                            txtMinPrice.setText(Integer.toString(minValue));
                            txtMinPrice.requestFocus();
                            txtMaxPrice.setText(Integer.toString(maxValue));
                            txtMinPrice.requestFocus();
                        }
                    });
            layRangseekBar.addView(seekBarPrice);
            // by discount
            seekBarDiscount
                    .setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() {

                        @Override
                        public void onRangeSeekBarValuesChanged(
                                RangeSeekBar<?> bar, Integer minValue,
                                Integer maxValue) {
                            // TODO Auto-generated method stub
                            txtMinDiscount.setText(Integer.toString(minValue));
                            txtMaxDiscount.setText(Integer.toString(maxValue));
                        }
                    });
            layRangeSliderDiscount.addView(seekBarDiscount);
            // seebar Radius
            seekBarKM.setProgress(5);
            txtKM.setText(seekBarKM.getProgress() + "");
            seekBarKM.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,
                                              boolean fromUser) {
                    // TODO Auto-generated method stub
                    txtKM.setText(Integer.toString(progress));
                }
            });
            // check title
            cbTitle.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {
                    // TODO Auto-generated method stub
                    if (isChecked) {
                        layTitle.setVisibility(View.VISIBLE);
                        txtTitle.setText("");
                    } else if (!isChecked) {
                        layTitle.setVisibility(View.GONE);
                        txtTitle.setText("");
                    }

                }
            });
            // check price
            cbPrice.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {
                    // TODO Auto-generated method stub
                    if (isChecked) {
                        layPrice.setVisibility(View.VISIBLE);
                    } else if (!isChecked) {
                        layPrice.setVisibility(View.GONE);
                    }

                }
            });
            // check category
            cbCategory
                    .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton buttonView,
                                                     boolean isChecked) {
                            // TODO Auto-generated method stub
                            if (isChecked) {
                                layCategory.setVisibility(View.VISIBLE);
                            } else if (!isChecked) {
                                layCategory.setVisibility(View.GONE);
                            }

                        }
                    });
            // check discount
            cbDiscount
                    .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton buttonView,
                                                     boolean isChecked) {
                            // TODO Auto-generated method stub
                            if (isChecked) {
                                layDiscount.setVisibility(View.VISIBLE);
                            } else if (!isChecked) {
                                layDiscount.setVisibility(View.GONE);
                            }
                        }
                    });
            // check location
            cbLocation
                    .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton buttonView,
                                                     boolean isChecked) {
                            // TODO Auto-generated method stub
                            if (isChecked) {
                                layLocation.setVisibility(View.VISIBLE);
                                txtAddress.setText("");
                                txtCity.setText("");
                                txtCounty.setText("");
                            } else if (!isChecked) {
                                layLocation.setVisibility(View.GONE);
                                txtAddress.setText("");
                                txtCity.setText("");
                                txtCounty.setText("");
                            }
                        }
                    });
            // check radius
            cbRadius.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {
                    // TODO Auto-generated method stub
                    if (isChecked) {
                        layRadius.setVisibility(View.VISIBLE);
                        setUpMap();
                    } else if (!isChecked) {
                        layRadius.setVisibility(View.GONE);

                    }
                }
            });
            // check Company
            cbCompany.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {
                    // TODO Auto-generated method stub
                    if (isChecked) {
                        layCompany.setVisibility(View.VISIBLE);
                    } else if (!isChecked) {
                        layCompany.setVisibility(View.GONE);

                    }
                }
            });
            // check Date Post
            cbStartDate
                    .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton buttonView,
                                                     boolean isChecked) {
                            // TODO Auto-generated method stub
                            if (isChecked) {
                                layStartDate.setVisibility(View.VISIBLE);
                            } else if (!isChecked) {
                                layStartDate.setVisibility(View.GONE);

                            }
                        }
                    });
            // select Date
            lblStartDate.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    OnDateSetListener callBack = new OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            mCal.set(year, monthOfYear, dayOfMonth);
                            Date currentDate = mCal.getTime();
                            strDate = sdf_date.format(currentDate);
                            lblStartDate.setText(strDate);
                        }
                    };

                    DatePickerDialog datePicker = new DatePickerDialog(
                            getActivity(), callBack, mCal.get(Calendar.YEAR),
                            mCal.get(Calendar.MONTH), mCal.get(Calendar.DATE));
                    datePicker.show();
                }
            });
            // setonclick
            lblCategory.setOnClickListener(this);
            lblCompany.setOnClickListener(this);
            btnFind.setOnClickListener(this);
            // button back
            btnBack.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    layFilter.setVisibility(View.VISIBLE);
                    layResult.setVisibility(View.GONE);
                }
            });

            txtTitle.setOnFocusChangeListener(this);
            txtAddress.setOnFocusChangeListener(this);
            txtCity.setOnFocusChangeListener(this);
            txtCounty.setOnFocusChangeListener(this);

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    // Method to get Data from Server
    public void getDataListCategory() {

        try {

            json = userFunction.categoryList();
            if (json != null) {
                JSONArray dataDealsArray = json
                        .getJSONArray(userFunction.array_category_list);

                intLengthData = dataDealsArray.length();
                sCategoryId = new String[intLengthData];
                sCategoryName = new String[intLengthData];
                sIcMarker = new String[intLengthData];

                // Store data to variable array
                for (int i = 0; i < intLengthData; i++) {
                    JSONObject dealObject = dataDealsArray.getJSONObject(i);
                    HashMap<String, String> mapCategory = new HashMap<String, String>();
                    sCategoryId[i] = dealObject
                            .getString(userFunction.key_category_id);
                    sCategoryName[i] = dealObject
                            .getString(userFunction.key_category_name);
                    sIcMarker[i] = dealObject
                            .getString(userFunction.key_category_marker);

                    mapCategory.put(userFunction.key_category_name,
                            sCategoryName[i]);
                    mapCategory.put(userFunction.key_category_id,
                            sCategoryId[i]);
                    categoryItems.add(mapCategory);
                }
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    // Method to get Data from Server
    public void getDataListCompany() {

        try {

            json = userFunction.companyList();
            if (json != null) {
                JSONArray dataDealsArray = json.getJSONArray("data");

                intLengthData = dataDealsArray.length();
                mCompany = new String[intLengthData];
                // Store data to variable array
                for (int i = 0; i < intLengthData; i++) {
                    JSONObject dealObject = dataDealsArray.getJSONObject(i);
                    HashMap<String, String> mapCompany = new HashMap<String, String>();
                    mCompany[i] = dealObject
                            .getString(userFunction.key_showlocation_company);

                    mapCompany.put(userFunction.key_showlocation_company,
                            mCompany[i]);
                    companyItems.add(mapCompany);
                }
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    // Method to get Data from Server
    private void getDataShowLocation() {

        try {
            // If mActivity equal activityCategory then use API dealByCategory
            json = userFunction.showByDeal();
            if (json != null) {
                JSONArray dataDealsArray = json.getJSONArray("data");
                intLengthData = dataDealsArray.length();
                mAutoTitle = new String[intLengthData];
                mAddress = new String[intLengthData];
                mCity = new String[intLengthData];
                mCounty = new String[intLengthData];

                for (int i = 0; i < intLengthData; i++) {
                    // Store data from server to variable
                    JSONObject dealsObject = dataDealsArray.getJSONObject(i);
                    ArrayList<String> address = new ArrayList<String>();
                    ArrayList<String> city = new ArrayList<String>();
                    ArrayList<String> county = new ArrayList<String>();
                    ArrayList<String> title = new ArrayList<String>();
                    mAutoTitle[i] = dealsObject
                            .getString(userFunction.key_showlocation_title);
                    mAddress[i] = dealsObject
                            .getString(userFunction.key_showlocation_address);
                    mCity[i] = dealsObject
                            .getString(userFunction.key_showlocation_city);
                    mCounty[i] = dealsObject
                            .getString(userFunction.key_showlocation_county);

                    title.add(mAutoTitle[i]);
                    address.add(mAddress[i]);
                    city.add(mCity[i]);
                    county.add(mCounty[i]);

                    // Adding HashList to ArrayList
                    titleItems.addAll(title);
                    addressItems.addAll(address);
                    cityItems.addAll(city);
                    countyItems.addAll(county);
                }
            }

        } catch (JSONException e) {
            throw new RuntimeException(e);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    // Load first 10 videos
    private class loadFirstListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            // Showing progress dialog before sending http request
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected Void doInBackground(Void... unused) {
            try {
                // Call method getDataFromServer
                getDataFilterServer();
            } catch (Exception ex) {
                Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
            }
            return (null);
        }

        protected void onPostExecute(Void unused) {
            // Condition if data length uder 10 button loadMore is remove
            if (intLengthData < userFunction.valueItemsPerPage) {
                list.removeFooterView(btnLoadMore);
            } else {
                list.addFooterView(btnLoadMore);
            }

            if (isAdded()) {
                if (mArrDeals.size() != 0) {

                    // Adding load more button to lisview at bottom
                    lytRetry.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);
                    // Getting adapter
                    List<String> status=getStringArrayPref(getActivity(),"Event_List_Status",mArrDeals);
                    sla = new HomeAdapter(getActivity(), mArrDeals,status);
                    list.setAdapter(sla);

                } else {
                    list.removeFooterView(btnLoadMore);
                    if (json != null) {
                        lblNoResult.setVisibility(View.VISIBLE);
                        lblNoResult.setText(getString(R.string.lbl_no_result));
                        lytRetry.setVisibility(View.GONE);

                    } else {
                        lblNoResult.setVisibility(View.GONE);
                        lytRetry.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(),
                                getString(R.string.no_connection),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
            // Closing progress dialog
            pDialog.dismiss();
            //
        }
    }

    // Load more videos
    private class loadMoreListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            // Showing progress dialog before sending http request
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected Void doInBackground(Void... unused) {
            try {
                // Store previous value of current page
                mPreviousPage = mCurrentPage;
                // Increment current page
                mCurrentPage += userFunction.valueItemsPerPage;
                getDataFilterServer();
            } catch (Exception ex) {
                Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
            }
            return (null);
        }

        protected void onPostExecute(Void unused) {
            // Condition if data length uder 10 button loadMore is remove
            if (intLengthData < userFunction.valueItemsPerPage)
                ;

            if (json != null) {
                if (mArrDeals != null) {
                    // Get listview current position - used to maintain scroll
                    // position
                    int currentPosition = list.getFirstVisiblePosition();

                    lytRetry.setVisibility(View.GONE);
                    // Appending new data to menuItems ArrayList
                    List<String> status=getStringArrayPref(getActivity(),"Event_List_Status",mArrDeals);
                    sla = new HomeAdapter(getActivity(), mArrDeals,status);
                    list.setAdapter(sla);
                    // Setting new scroll position
                    list.setSelectionFromTop(currentPosition + 1, 0);
                } else {
                    list.removeFooterView(btnLoadMore);
                }
            } else {
                Log.d("json", "json not null");
                if (mArrDeals != null) {
                    Log.d("menuItems", "menuItems not null");
                    mCurrentPage = mPreviousPage;
                    lytRetry.setVisibility(View.GONE);
                } else {
                    Log.d("menuItems", "menuItems null");
                    lytRetry.setVisibility(View.VISIBLE);
                }
                Toast.makeText(getActivity(),
                        getString(R.string.no_connection), Toast.LENGTH_SHORT)
                        .show();
            }
            // Closing progress dialog
            pDialog.dismiss();
        }
    }

    // Method get data from server
    public void getDataFilterServer() {
        try {
            address = address.replace(" ", "%20");
            title = title.replace(" ", "%20");
            company = company.replace(" ", "%20");
            city = city.replace(" ", "%20");
            county = county.replace(" ", "%20");
            json = userFunction.filter(type, min, max, company, startDate,
                    title, address, city, county, category,
                    Integer.parseInt(mindiscount),
                    Integer.parseInt(maxdiscount), lat, lnt,
                    Integer.parseInt(distance), mCurrentPage);
            county = county.replace("%20", " ");
            city = city.replace("%20", " ");
            company = company.replace("%20", " ");
            title = title.replace("%20", " ");
            address = address.replace("%20", " ");
            if (json != null) {
                ArrayList<DealObj> arr = JSONParser.parserDeal(
                        userFunction.KEY_JSON_DATA, json.toString());

                ArrayList<DealObj> unique = new ArrayList<>();

                for(DealObj dealObj : arr) {
                    boolean duplicateDeal = false;

                    for(int i=0;i<unique.size();i++) {
                        DealObj dealObjArr = unique.get(i);

                        if(dealObj.getDeal_id().equals(dealObjArr.getDeal_id())) {
                            duplicateDeal = true;
                            break;
                        }
                    }

                    if(!duplicateDeal) {
                        unique.add(dealObj);
                    }
                }

                arr = unique;
                unique = null;

                for(int i=0;i<arr.size();i++) {
                    double dlat = arr.get(i).getLatitude();
                    double dlon = arr.get(i).getLongitude();

                    double dist = getDistance(lat, lnt, dlat, dlon);

                    if(dist > Integer.parseInt(distance)) {
                        arr.remove(i);
                        i--;
                    }
                }

                mArrDeals.addAll(arr);

                intLengthData = mArrDeals.size();
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }

    private double getDistance(double lat1, double lng1, double lat2, double lng2) {

        double earthRadius = 3958.75; // in miles, change to 6371 for kilometer output

        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);

        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);

        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        double dist = earthRadius * c;

        return dist; // output distance, in MILES
    }

    @Override
    public void onClick(View v) {
        try {
            // TODO Auto-generated method stub
            if (v == llMenu) {
                //ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
                Intent intent=new Intent(getActivity(),ActivityHome.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            }
            title = txtTitle.getText().toString();
            // onClick button Find
            if (v.getId() == R.id.btnFind) {
               /* Toast.makeText(getActivity(), lat + " - " + lnt,
                        Toast.LENGTH_SHORT).show();*/

                if (cbTitle.isChecked() && cbLocation.isChecked()
                        && cbRadius.isChecked() && cbCategory.isChecked()
                        && cbDiscount.isChecked() && cbCompany.isChecked()
                        && cbStartDate.isChecked() && cbPrice.isChecked()) {
                    type = 19;
                }

                if (cbTitle.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbDiscount.isChecked()
                        && !cbCategory.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 1;
                }

                if (cbPrice.isChecked() && !cbTitle.isChecked()
                        && !cbCompany.isChecked() && !cbStartDate.isChecked()
                        && !cbCategory.isChecked() && !cbDiscount.isChecked()
                        && !cbLocation.isChecked() && !cbRadius.isChecked()) {
                    type = 2;
                }
                if (!cbTitle.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCategory.isChecked()
                        && !cbDiscount.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 0;
                }
                if (cbCompany.isChecked() && !cbStartDate.isChecked()
                        && !cbTitle.isChecked() && !cbCategory.isChecked()
                        && !cbLocation.isChecked() && !cbRadius.isChecked()
                        && !cbDiscount.isChecked() && !cbPrice.isChecked()) {
                    if (lblCompany.getText().toString().isEmpty()) {
                        Toast.makeText(getActivity(),
                                "Please select company", Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        type = 3;
                    }
                }
                if (cbLocation.isChecked() && !cbTitle.isChecked()
                        && !cbCompany.isChecked() && !cbStartDate.isChecked()
                        && !cbDiscount.isChecked() && !cbCategory.isChecked()
                        && !cbRadius.isChecked() && !cbPrice.isChecked()) {
                    type = 4;
                }
                if (cbRadius.isChecked() && !cbTitle.isChecked()
                        && !cbDiscount.isChecked() && !cbCategory.isChecked()
                        && !cbLocation.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 5;
                }
                if (cbRadius.isChecked() && cbTitle.isChecked()
                        && !cbDiscount.isChecked() && !cbCategory.isChecked()
                        && !cbLocation.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 5;
                }
                if (cbRadius.isChecked() && !cbTitle.isChecked()
                        && cbDiscount.isChecked() && !cbCategory.isChecked()
                        && !cbLocation.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 5;
                }
                if (cbRadius.isChecked() && !cbTitle.isChecked()
                        && !cbDiscount.isChecked() && cbCategory.isChecked()
                        && !cbLocation.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 5;
                }
                if (cbRadius.isChecked() && !cbTitle.isChecked()
                        && !cbDiscount.isChecked() && !cbCategory.isChecked()
                        && cbLocation.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 5;
                }
                if (cbRadius.isChecked() && !cbTitle.isChecked()
                        && !cbDiscount.isChecked() && !cbCategory.isChecked()
                        && !cbLocation.isChecked() && cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 5;
                }
                if (cbRadius.isChecked() && !cbTitle.isChecked()
                        && !cbDiscount.isChecked() && !cbCategory.isChecked()
                        && !cbLocation.isChecked() && !cbCompany.isChecked()
                        && cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 5;
                }
                if (cbRadius.isChecked() && !cbTitle.isChecked()
                        && !cbDiscount.isChecked() && !cbCategory.isChecked()
                        && !cbLocation.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && cbPrice.isChecked()) {
                    type = 5;
                }
                if (cbRadius.isChecked() && cbTitle.isChecked()
                        && cbDiscount.isChecked() && cbCategory.isChecked()
                        && cbLocation.isChecked() && cbCompany.isChecked()
                        && cbStartDate.isChecked() && cbPrice.isChecked()) {
                    type = 5;
                }
                if (cbCategory.isChecked() && !cbTitle.isChecked()
                        && !cbDiscount.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbPrice.isChecked()) {
                    type = 6;
                }
                if (cbDiscount.isChecked() && !cbTitle.isChecked()
                        && !cbCategory.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 7;
                }
                if (cbDiscount.isChecked() && !cbTitle.isChecked()
                        && cbCategory.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 8;
                }
                if (cbDiscount.isChecked() && !cbTitle.isChecked()
                        && !cbCategory.isChecked() && cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 9;
                }
                if (!cbDiscount.isChecked() && !cbTitle.isChecked()
                        && cbCategory.isChecked() && cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 10;
                }
                if (cbDiscount.isChecked() && !cbTitle.isChecked()
                        && cbCategory.isChecked() && cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 11;
                }
                if (!cbDiscount.isChecked() && cbTitle.isChecked()
                        && cbCategory.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 12;
                }
                if (!cbDiscount.isChecked() && cbTitle.isChecked()
                        && !cbCategory.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 13;
                }
                if (!cbDiscount.isChecked() && cbTitle.isChecked()
                        && !cbCategory.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && cbPrice.isChecked()) {
                    type = 14;
                }
                if (!cbDiscount.isChecked() && cbTitle.isChecked()
                        && !cbCategory.isChecked() && cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 16;
                }
                if (cbDiscount.isChecked() && cbTitle.isChecked()
                        && !cbCategory.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 17;
                }
                if (!cbDiscount.isChecked() && cbTitle.isChecked()
                        && !cbCategory.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 18;
                }
                if (!cbDiscount.isChecked() && !cbTitle.isChecked()
                        && cbCategory.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 20;
                }
                if (!cbDiscount.isChecked() && !cbTitle.isChecked()
                        && !cbCategory.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && cbCompany.isChecked()
                        && !cbStartDate.isChecked() && cbPrice.isChecked()) {
                    type = 21;
                }
                if (cbDiscount.isChecked() && !cbTitle.isChecked()
                        && !cbCategory.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 22;
                }
                if (!cbDiscount.isChecked() && !cbTitle.isChecked()
                        && !cbCategory.isChecked() && cbLocation.isChecked()
                        && !cbRadius.isChecked() && cbCompany.isChecked()
                        && !cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 23;
                }
                if (!cbDiscount.isChecked() && !cbTitle.isChecked()
                        && !cbCategory.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && cbCompany.isChecked()
                        && cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 24;
                }
                if (cbDiscount.isChecked() && cbTitle.isChecked()
                        && cbCategory.isChecked() && cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && cbStartDate.isChecked() && cbPrice.isChecked()) {
                    type = 25;
                }
                if (!cbDiscount.isChecked() && !cbTitle.isChecked()
                        && !cbCategory.isChecked() && cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 26;
                }
                if (!cbDiscount.isChecked() && !cbTitle.isChecked()
                        && !cbCategory.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && cbStartDate.isChecked() && cbPrice.isChecked()) {
                    type = 27;
                }
                if (cbDiscount.isChecked() && !cbTitle.isChecked()
                        && !cbCategory.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 28;
                }
                if (cbDiscount.isChecked() && cbTitle.isChecked()
                        && cbCategory.isChecked() && cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && !cbStartDate.isChecked() && cbPrice.isChecked()) {
                    type = 29;
                }
                if (!cbDiscount.isChecked() && !cbTitle.isChecked()
                        && !cbCategory.isChecked() && !cbLocation.isChecked()
                        && !cbRadius.isChecked() && !cbCompany.isChecked()
                        && cbStartDate.isChecked() && !cbPrice.isChecked()) {
                    type = 30;
                }

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                min = txtMinPrice.getText().toString().trim();
                max = txtMaxPrice.getText().toString().trim();
                mindiscount = txtMinDiscount.getText().toString();
                maxdiscount = txtMaxDiscount.getText().toString();
                address = txtAddress.getText().toString().trim();
                city = txtCity.getText().toString().trim();
                county = txtCounty.getText().toString().trim();
                distance = txtKM.getText().toString().trim();
                startDate = lblStartDate.getText().toString().trim();
                startDate = sdf.format(mCal.getTime());
                company = lblCompany.getText().toString();
                if (type == 0) {
                    // Toast.makeText(getActivity(),
                    // "You need input No min and No max", Toast.LENGTH_SHORT)
                    // .show();
                    layFilter.setVisibility(View.GONE);
                    layResult.setVisibility(View.VISIBLE);
                    lblNoResult.setVisibility(View.VISIBLE);
                } else {
                    layFilter.setVisibility(View.GONE);
                    layResult.setVisibility(View.VISIBLE);
                    lblNoResult.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);

                    json = null;
                    mArrDeals.clear();
                    list.setAdapter(null);
                    new loadFirstListView().execute();
                }
            }
            // select category
            if (v.getId() == R.id.lblCategory) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.getWindow();
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_filter_category);
                final ListView lsvCategory = (ListView) dialog
                        .findViewById(R.id.lsvFilterCategory);
                adapterFilterCategory = new AdapterFilterCategory(
                        getActivity(), categoryItems);
                lsvCategory.setAdapter(adapterFilterCategory);
                lsvCategory.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // TODO Auto-generated method stub
                        if(position<sCategoryName.length){
                            lblCategory.setText(sCategoryName[position]);
                            category = sCategoryId[position];
                            dialog.dismiss();
                        }
                    }
                });
                dialog.show();
            }
            // select Company
            if (v.getId() == R.id.lblFilterCompany) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.getWindow();
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_filter_category);
                final ListView lsvCategory = (ListView) dialog
                        .findViewById(R.id.lsvFilterCategory);
                adapterCompany = new AdapterCompany(getActivity(), companyItems);
                lsvCategory.setAdapter(adapterCompany);
                lsvCategory.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // TODO Auto-generated method stub
                        lblCompany.setText(mCompany[position]);
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        } catch (Exception ex) {
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
    }

    LatLng CENTER = null;

    private void setUpMap() {
        try {
            MapsInitializer.initialize(getActivity());

            switch (GooglePlayServicesUtil
                    .isGooglePlayServicesAvailable(getActivity())) {
                case ConnectionResult.SUCCESS:
                    // Gets to GoogleMap from the MapView and does initialization
                    // stuff
                    if (mapView != null) {

                        locationManager = ((LocationManager) getActivity()
                                .getSystemService(Context.LOCATION_SERVICE));

                        Boolean localBoolean = Boolean.valueOf(locationManager
                                .isProviderEnabled("network"));

                        if (localBoolean.booleanValue()) {

                            CENTER = new LatLng(lat, lnt);

                        } else {

                        }

                        mapView.getMapAsync(this);


                    }
                    break;
                case ConnectionResult.SERVICE_MISSING:

                    break;
                case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:

                    break;
                default:

            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        // TODO Auto-generated method stub
        lat = location.getLatitude();
        lnt = location.getLongitude();

        mLatLng = new LatLng(lat, lnt);

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 16));

        // mMaps.addMarker(new MarkerOptions().position(mLatLng)).setTitle(
        // "Current Location");

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }

    public void onResume() {
        super.onResume();
        setUpMap();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v == txtTitle || v == txtCity || v == txtCounty || v == txtAddress) {
            if (hasFocus) {
                getActivity().getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            } else {
                InputMethodManager imm = (InputMethodManager) getActivity()
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        }
    }

    ;

    private void setStatusPrivateEvent(android.content.Context context, String key,String deal_id)
    {
        //Event_List_Status-key
        try
        {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = prefs.edit();
            JSONArray a;
            try
            {
                String json = prefs.getString(key, null);
                a = new JSONArray(json);
            }
            catch (Exception e)
            {
                a = new JSONArray();
                e.printStackTrace();
            }

            ListEventObj listEventObj = null;
            JSONObject b=new JSONObject();

            try
            {
                b.put("event_id", deal_id);

            }
            catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            a.put(b);

            if (a!=null)
            {
                editor.putString(key, a.toString());
            }
            else
            {
                editor.putString(key, null);
            }
            editor.commit();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public List<String> getStringArrayPref(Context context, String key, List<DealObj> list)
    {
        List<String> listEventStatus=new ArrayList<>(list.size());
        for (int i = 0; i < list.size(); i++)
        {
            listEventStatus.add("0");
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = prefs.getString(key, null);
        List<ListEventObj> listEvents=new ArrayList<>();
        if (json != null)
        {
            try
            {
                JSONArray a = null;
                try
                {
                    a = new JSONArray(json);
                    for (int i = 0; i < list.size(); i++)
                    {
                        DealObj ss=list.get(i);
                        for(int j=0;j<a.length();j++)
                        {
                            JSONObject jsonObject=a.getJSONObject(j);
                            if(ss.getDeal_id().equalsIgnoreCase(jsonObject.getString("event_id")))
                            {
                                listEventStatus.set(i,ss.getDeal_id());
                            }
                            else
                            {
                                if(listEventStatus.get(i).equalsIgnoreCase("0"))
                                {
                                    listEventStatus.set(i,"0");
                                }
                            }

                        }
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return listEventStatus;
    }
}