package com.pcits.common.utils;

public final class DeviceInfo {
	
	public static String getOsVersion(){
		return System.getProperty("os.version")  + "(" + android.os.Build.VERSION.INCREMENTAL + ")";
	}
	
	public static String getBuildVersion(){
		return String.valueOf(android.os.Build.VERSION.SDK_INT);
	}
	
	public static String getDevice(){
		return android.os.Build.DEVICE;
	}
	
	public static String getDeviceModel(){
		return android.os.Build.MODEL;
	}
	
	public static String getDeviceProduct(){
		return android.os.Build.PRODUCT;
	}
	
	public static String getDeviceManufacture(){
		return android.os.Build.MANUFACTURER;
	}
	
	public static String getDeviceBuildID(){
		return android.os.Build.SERIAL;
	}
	
	public static String getAllInfo(){
		return  "Model     : " + getDeviceManufacture() + " - " + getDeviceModel() + " - " + getDeviceProduct() + "\n" +
				"OS. Ver   : " + getOsVersion() + "\n" +
				"SDK Ver   : " + getBuildVersion() + "\n" +
				"Device ID : " + getDeviceBuildID() + "\n" +
				"Device    : " + getDevice();
				
	}
}
