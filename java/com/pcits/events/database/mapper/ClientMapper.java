package com.pcits.events.database.mapper;

import android.database.Cursor;

import com.pcits.events.obj.ClientInfo;

public class ClientMapper implements RowMapper<ClientInfo> {

	@Override
	public ClientInfo mapRow(Cursor row, int rowNum) {
		// TODO Auto-generated method stub
		ClientInfo clientObj = new ClientInfo();
		clientObj.setUsername(CursorParseUtility.getString(row, "username"));
		clientObj.setPassword(CursorParseUtility.getString(row, "password"));
		clientObj.setFname(CursorParseUtility.getString(row, "fname"));
		clientObj.setLname(CursorParseUtility.getString(row, "lname"));
		clientObj.setDob(CursorParseUtility.getString(row, "dob"));
		clientObj.setAddress(CursorParseUtility.getString(row, "address"));
		clientObj.setPhone(CursorParseUtility.getString(row, "phone"));
		clientObj.setGender(CursorParseUtility.getString(row, "gender"));
		clientObj.setCity(CursorParseUtility.getString(row, "city"));
		clientObj.setCounty(CursorParseUtility.getString(row, "county"));
		clientObj.setPostcode(CursorParseUtility.getString(row, "postcode"));
		clientObj.setCountry(CursorParseUtility.getString(row, "country"));
		clientObj.setEmail(CursorParseUtility.getString(row, "email"));
		clientObj.setImage(CursorParseUtility.getString(row, "image"));
		clientObj.setDeviceid(CursorParseUtility.getString(row, "device_id"));
		return clientObj;
	}

}
