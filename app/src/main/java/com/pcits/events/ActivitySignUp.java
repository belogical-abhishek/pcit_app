package com.pcits.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.common.utils.GsonUtility;
import com.pcits.common.utils.Logging;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.ClientInfo;
import com.pcits.events.obj.UserObj;
import com.pcits.events.utils.AccessStorage;
import com.pcits.events.widgets.CircleImageWithBorder;
import com.pcits.events.widgets.FloatLabeledEditText;

public class ActivitySignUp extends FragmentActivity implements
		OnClickListener {

	// Declare object of Utils class
	protected Fragment mFrag;
	private RadioButton radUser, radClient;
	private FloatLabeledEditText txtEmail, txtUsername, txtFname,txtLname,txtPassword,
			txtComfirmPass;
	private TextView txtGender;
	private Button btnSignUp;
	private ClientInfo account = null;
	private UserObj user = null;
	private UserFunctions userFunction = new UserFunctions();
	private int intLengthData, type;
	public String[] mCountry;
	private JSONObject json;
	private String extraString = "";
	private String extraFromString = "";
	private String extraDealIdString = "";
	private String extraEmail = "";
	private String extraName = "";
	// create listApdater
	private ArrayList<HashMap<String, String>> countryItems = new ArrayList<HashMap<String, String>>();

	private final Pattern EMAIL_PATTERN = Pattern
			.compile("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+");
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private CircleImageWithBorder imgAvatar;
	// private TextView btnSelectImg;
	private String selectedImagePath;
	private Bitmap avatarBitmap;
	private boolean isFromDeepLink = false;


	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_sign_up);
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				// Put in code over here that does the network related
				getDataShowCountry();
				return null;
			}

		};
		task.execute();
		getExtras(savedInstanceState);
		initUI();
		initControl();
	}

	private void getExtras(Bundle savedInstanceState) {
		try {
			if (savedInstanceState == null) {
				Bundle extras = getIntent().getExtras();
				if (extras != null) {
					extraString = extras.getString("obj");
					extraFromString = extras.getString("from");
					extraDealIdString = extras.getString("dealid");
					extraEmail = extras.getString("email");
					extraName = extras.getString("name");
				}
			} else {
				extraString = (String) savedInstanceState.getSerializable("obj");
				extraFromString = (String) savedInstanceState.getSerializable("from");
				extraDealIdString = (String) savedInstanceState.getSerializable("dealid");
				extraEmail = (String) savedInstanceState.getSerializable("email");
				extraName = (String) savedInstanceState.getSerializable("name");
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	private void initUI() {
		imgAvatar = (CircleImageWithBorder) findViewById(R.id.imgAvatar);
		// btnSelectImg = (TextView) findViewById(R.id.btnSelectAvata);
		txtEmail = (FloatLabeledEditText) findViewById(R.id.txtEmail);
		txtGender = (TextView) findViewById(R.id.txtGender);
		txtUsername = (FloatLabeledEditText) findViewById(R.id.txtUser);
		txtFname = (FloatLabeledEditText) findViewById(R.id.txtFname);
		txtLname = (FloatLabeledEditText) findViewById(R.id.txtLname);
		txtPassword = (FloatLabeledEditText) findViewById(R.id.txtPass);
		txtComfirmPass = (FloatLabeledEditText) findViewById(R.id.txtConfirmPass);
		btnSignUp = (Button) findViewById(R.id.btnSignUp);
		radUser = (RadioButton) findViewById(R.id.radUser);
		radClient = (RadioButton) findViewById(R.id.radClient);
		llMenu = (LinearLayout) findViewById(R.id.llMenu);
		validateEmail();

	}

	private void initControl() {
		//
		txtGender.setOnClickListener(this);
		btnSignUp.setOnClickListener(this);
		llMenu.setOnClickListener(this);
		// btnSelectImg.setOnClickListener(this);
		imgAvatar.setOnClickListener(this);
		if(extraFromString != null && !extraFromString.isEmpty() && extraFromString.equalsIgnoreCase("deep_link"))
		{
			isFromDeepLink=true;

		}

	}

	private void validateEmail() {
		txtEmail.getEditText().addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				String email = txtEmail.getText().toString();
				if (validateEmail(email) == false) {
					txtEmail.setError(Html
							.fromHtml("<font color='red'>Invalid Email!</font>"));
				} else {
				}

			}
		});
	}

	private boolean validateEmail(String email) {
		return EMAIL_PATTERN.matcher(email).matches();
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// Previous page or exit
			// Intent i = new Intent(this, ActivityLogin.class);
			// startActivity(i);
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		// Intent i = new Intent(this, ActivityLogin.class);
		// startActivity(i);
		finish();
	}

	// Method to get Data from Server
	private void getDataShowCountry() {

		try {
			// If mActivity equal activityCategory then use API dealByCategory
			json = userFunction.showByCountry();
			if (json != null) {
				JSONArray dataDealsArray = json.getJSONArray("data");
				intLengthData = dataDealsArray.length();
				mCountry = new String[intLengthData];

				for (int i = 0; i < intLengthData; i++) {
					// Store data from server to variable
					JSONObject dealsObject = dataDealsArray.getJSONObject(i);
					HashMap<String, String> country = new HashMap<String, String>();
					mCountry[i] = dealsObject
							.getString(userFunction.key_showcountry_country);

					country.put(userFunction.key_showcountry_country,
							mCountry[i]);

					// Adding HashList to ArrayList
					countryItems.add(country);
				}
			}

		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	@SuppressLint("NewApi")
	private String validateForm() {
		// TODO Auto-generated method stub
		String message = "success";
		String email = txtEmail.getText().toString();
		String gender = txtGender.getText().toString();
		String username = txtUsername.getText().toString();
		String password = txtPassword.getText().toString();
		String confimPass = txtComfirmPass.getText().toString();
		if (radUser.isChecked()) {
			type = 1;
		}
		if (radClient.isChecked()) {
			type = 0;
		}
		if (!radClient.isChecked() && !radUser.isChecked()) {
			Toast.makeText(this, "Please select register.!", Toast.LENGTH_SHORT)
					.show();
			return message;
		}
		// if (dob.isEmpty()) {
		// Toast.makeText(this, "Please, input dob", Toast.LENGTH_SHORT)
		// .show();
		// return message;
		// }
		// if (gender.isEmpty()) {
		// Toast.makeText(this, "Please, input gender", Toast.LENGTH_SHORT)
		// .show();
		// return message;
		// }
		// if (phone.isEmpty()) {
		// Toast.makeText(this, "Please, input phone", Toast.LENGTH_SHORT)
		// .show();
		// return message;
		// }
		if (txtFname.getText().toString().isEmpty()) {
			Toast.makeText(this, "Please, select First name", Toast.LENGTH_SHORT)
					.show();
			return message;
		}
		if (txtLname.getText().toString().isEmpty()) {
			Toast.makeText(this, "Please, select last name", Toast.LENGTH_SHORT)
					.show();
			return message;
		}
		if (gender.isEmpty()) {
			Toast.makeText(this, "Please, select gender", Toast.LENGTH_SHORT)
					.show();
			return message;
		}
		if (email.isEmpty()) {
			Toast.makeText(this, "Please, input email", Toast.LENGTH_SHORT)
					.show();
			return message;
		}
		if (username.isEmpty()) {
			Toast.makeText(this, "Please, input username", Toast.LENGTH_SHORT)
					.show();
			return message;
		}
		if (password.isEmpty()) {
			Toast.makeText(this, "Please, input password", Toast.LENGTH_SHORT)
					.show();
			return message;
		}
		if (confimPass.isEmpty()) {
			Toast.makeText(this, "Please, input confirm password",
					Toast.LENGTH_SHORT).show();
			return message;
		} else {
			if (!confimPass.equals(password)) {
				Toast.makeText(this, "Repassword is wrong !",
						Toast.LENGTH_SHORT).show();
				return message;
			}
		}
		account = new ClientInfo();
		account.setDeviceid(GlobalValue.android_id);
		account.setEmail(email.trim());
		account.setGender(gender.trim());
		account.setUsername(username.trim());
		account.setFname(txtFname.getText().toString().trim());
		account.setLname(txtLname.getText().toString().trim());
		account.setUsername(username.trim());
		account.setPassword(password.trim());
		//
		user = new UserObj();
		user.setEmail(email.trim());
		user.setRole(Integer.toString(type));
		user.setUsername(username.trim());
		user.setFname(txtFname.getText().toString().trim());
		user.setLname(txtLname.getText().toString().trim());
		user.setPassword(password.trim());
		user.setStatus("" + 0);
		return message;

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == llMenu) {
			finish();
		}
		if (v.getId() == R.id.btnSignUp) {
			String message = validateForm();
			if (!message.equals("success")) {
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
			} else {
				if (account != null && type == 0) {
					register(account);
				}
				if (user != null && type == 1) {
					registerUser(user);
				}
			}
			Log.e("Type", "Typeeee: " + type);
		}
		if (v == imgAvatar) {
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(
					Intent.createChooser(intent, "Complete action using"), 1);
		}
		if (v == txtGender) {
			popupGender();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == RESULT_OK) {
			try {
				Uri uri = data.getData();
				selectedImagePath = AccessStorage.getPath(ActivitySignUp.this,
						uri);
				avatarBitmap = AccessStorage
						.loadPrescaledBitmap(selectedImagePath);
				// avatarBitmap = CommonMethods.getResizedBitmap(avatarBitmap,
				// 200, 200);
				Drawable d = new BitmapDrawable(getResources(), avatarBitmap);
				imgAvatar.setImageDrawable(d);
				// btnSelectImg.setText(selectedImagePath);
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			}
		}
	}

	private void register(ClientInfo acc) {
		// TODO Auto-generated method stub
		ClientInfo data = acc;
		Log.e("ACCOUNT  INFO", "ACCOUNT INFO : " + data);

		ModelManager.registerClient(this, data, avatarBitmap, true,
				new ModelManagerListener() {

					@Override
					public void onError() {
						// TODO Auto-generated method stub
						Log.d("Signup", "onError: register");

					}

					@Override
					public void onSuccess(Object object) {
						// TODO Auto-generated method stub
						String strJson = (String) object;
						Toast.makeText(ActivitySignUp.this,
								checkResult(strJson), Toast.LENGTH_SHORT)
								.show();
						// Intent i = new Intent(ActivitySignUp.this,
						// ActivityLogin.class);
						// startActivity(i);


						finish();
					}

				});
		// ModelManager.register(this, data, true, new ModelManagerListener() {
		//
		// @Override
		// public void onSuccess(Object object) {
		// // TODO Auto-generated method stub
		// String strJson = (String) object;
		// Toast.makeText(ActivitySignUp.this, checkResult(strJson),
		// Toast.LENGTH_SHORT).show();
		// // Intent i = new Intent(ActivitySignUp.this,
		// // ActivityLogin.class);
		// // startActivity(i);
		// }
		//
		// @Override
		// public void onError() {
		// // TODO Auto-generated method stub
		//
		// }
		// });
	}

	private void registerUser(UserObj us) {
		// TODO Auto-generated method stub
		UserObj data = us;
		Log.e("ACCOUNT INFO", "ACCOUNT INFO : " + GsonUtility.convertObjectToJSONString(data));

		ModelManager.registerAdmin(this, data, avatarBitmap, true,
				new ModelManagerListener() {

					@Override
					public void onError() {
						// TODO Auto-generated method stub
						Log.d("signup", "onError: registerUser ");

					}

					@Override
					public void onSuccess(Object object) {
						// TODO Auto-generated method stub
						String strJson = (String) object;
						Toast.makeText(ActivitySignUp.this,
								checkResult(strJson), Toast.LENGTH_SHORT)
								.show();
						// Intent i = new Intent(ActivitySignUp.this,
						// ActivityLogin.class);
						// startActivity(i);
						if(isFromDeepLink) {
							Intent intent = new Intent(ActivitySignUp.this ,ActivityLogin.class);
							intent.putExtra("dealid", extraDealIdString);
							intent.putExtra("from", extraFromString);
							intent.putExtra("email", "");
							intent.putExtra("name", ""+user.getUsername());
							startActivity(intent);
						}
						finish();
					}

				});
		// Old
		// ModelManager.registerUser(this, data, true, new
		// ModelManagerListener() {
		//
		// @Override
		// public void onSuccess(Object object) {
		// // TODO Auto-generated method stub
		// String strJson = (String) object;
		// Toast.makeText(ActivitySignUp.this, checkResult(strJson),
		// Toast.LENGTH_SHORT).show();
		// // Intent i = new Intent(ActivitySignUp.this,
		// // ActivityLogin.class);
		// // startActivity(i);
		// }
		//
		// @Override
		// public void onError() {
		// // TODO Auto-generated method stub
		//
		// }
		// });
	}

	protected String checkResult(String strJson) {
		// TODO Auto-generated method stub
		JSONObject json = null;
		String message = "";
		try {
			json = new JSONObject(strJson);
			if (json.getString("status").equals("success")) {
				message = this.getString(R.string.message_success);
				finish();
			} else {
				message = json.getString("message");
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return message;
	}

	@SuppressLint("NewApi")
	private void popupGender() {
		// TODO Auto-generated method stub
		PopupMenu popup = new PopupMenu(this, txtGender);

		popup.getMenuInflater().inflate(R.menu.gender, popup.getMenu());

		popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(android.view.MenuItem item) {
				// TODO Auto-generated method stub
				switch (item.getItemId()) {
				case R.id.male:
					txtGender.setText("Male");
					break;
				case R.id.female:
					txtGender.setText("Female");
					break;
				case R.id.undisclosed:
					txtGender.setText("Undisclosed");
					break;
				}
				return false;
			}
		});
		popup.show();
	}
}
