package com.pcits.events;

import yuku.ambilwarna.AmbilWarnaDialog;
import yuku.ambilwarna.AmbilWarnaDialog.OnAmbilWarnaListener;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.TnCobj;
import com.pcits.events.widgets.KenBurnsView;
import com.pcits.events.widgets.richtext.CustomEditText;
import com.pcits.events.widgets.richtext.CustomEditText.EventBack;

public class ActivityEditTnC extends FragmentActivity implements
		OnClickListener, OnAmbilWarnaListener {

	private EditText txtDesc/* , txtNotes */;
	private TextView lblTncId, lblUser;
	private Button btnSubmit;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;

	// Rich editor declare
	private CustomEditText txtNotes;
	private LinearLayout llEditor;
	private AmbilWarnaDialog colorPickerDialog;
	private ImageView imgChangeColor;

	private int selectionStart;
	private int selectionEnd;

	private EventBack eventBack = new EventBack() {

		@Override
		public void close() {
			llEditor.setVisibility(View.GONE);
		}

		@Override
		public void show() {
			llEditor.setVisibility(View.VISIBLE);
		}
	};
	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (txtNotes.isFocused()) {
				llEditor.setVisibility(View.VISIBLE);
			}
		}
	};

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_tnc);

		initUI();
		initControl();

		// int titleId = Resources.getSystem().getIdentifier("action_bar_title",
		// "id", "android");
		// if (0 == titleId)
		// titleId = com.actionbarsherlock.R.id.abs__action_bar_title;
		//
		// // Change the title color to white
		// TextView txtActionbarTitle = (TextView) findViewById(titleId);
		// txtActionbarTitle.setTextColor(getResources().getColor(
		// R.color.actionbar_title_color));
		//
		// // Get ActionBar and set back button on actionbar
		// actionbar = getSupportActionBar();
		// actionbar.setDisplayHomeAsUpEnabled(true);
		// load ads
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		initNotBoringActionBar();
	}

	private void initNotBoringActionBar() {
		mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}

	private void initUI() {
		txtDesc = (EditText) findViewById(R.id.txtDesc);
		lblTncId = (TextView) findViewById(R.id.lblTncId);
		lblUser = (TextView) findViewById(R.id.lblUser);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);
		llMenu = (LinearLayout) findViewById(R.id.llMenu);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);

		// Init rich editor
		initRichEditor();
	}

	private void initRichEditor() {
		txtNotes = (CustomEditText) findViewById(R.id.txtNote);
		colorPickerDialog = new AmbilWarnaDialog(this, Color.BLACK, this);
		ToggleButton boldToggle = (ToggleButton) findViewById(R.id.btnBold);
		ToggleButton italicsToggle = (ToggleButton) findViewById(R.id.btnItalics);
		ToggleButton underlinedToggle = (ToggleButton) findViewById(R.id.btnUnderline);
		imgChangeColor = (ImageView) findViewById(R.id.btnChangeTextColor);
		llEditor = (LinearLayout) findViewById(R.id.lnlAction);
		llEditor.setVisibility(View.VISIBLE);

		txtNotes.setSingleLine(false);
		txtNotes.setMinLines(10);
		txtNotes.setBoldToggleButton(boldToggle);
		txtNotes.setItalicsToggleButton(italicsToggle);
		txtNotes.setUnderlineToggleButton(underlinedToggle);
		txtNotes.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					llEditor.setVisibility(View.VISIBLE);
				} else {
					llEditor.setVisibility(View.GONE);
				}
			}
		});
		txtNotes.setEventBack(eventBack);
		txtNotes.setOnClickListener(clickListener);
		imgChangeColor.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				selectionStart = txtNotes.getSelectionStart();
				selectionEnd = txtNotes.getSelectionEnd();
				colorPickerDialog.show();

				// Close keyboard
				closeKeyboard();
			}
		});
	}

	private void initControl() {
		lblTncId.setText(GlobalValue.tncObj.getTnc_id());
		lblUser.setText(GlobalValue.tncObj.getUsername());
		txtDesc.setText(GlobalValue.tncObj.getTncDesc());
		txtNotes.setTextHTML(GlobalValue.tncObj.getNotes());
		txtNotes.setColor(
				new ForegroundColorSpan(getResources().getColor(R.color.black))
						.getForegroundColor(), selectionStart, selectionEnd);
		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		btnSubmit.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == btnSubmit) {
			editTnC();
		}
	}

	private void editTnC() {
		try{
			final String tncDesc = txtDesc.getText().toString();
			final String notes = txtNotes.getText().toString();
	
			if (tncDesc.isEmpty()) {
				Toast.makeText(this, "Please, input desc", Toast.LENGTH_SHORT)
						.show();
				txtDesc.requestFocus();
				return;
			}
			if (notes.isEmpty()) {
				Toast.makeText(this, "Please, input notes", Toast.LENGTH_SHORT)
						.show();
				txtNotes.requestFocus();
				return;
			}
	
			TnCobj tnc = new TnCobj();
			tnc.setTncDesc(tncDesc.trim());
			tnc.setNotes(notes.trim());
	
			// update
			ModelManager.editTnc(this, GlobalValue.tncObj.getTnc_id(), tnc, true,
				new ModelManagerListener() {

					@Override
					public void onSuccess(Object object) {
						try{
							// TODO Auto-generated method stub
							String json = (String) object;
							boolean result = ParserUtility.updateAccount(json);
							if (result) {
								GlobalValue.tncObj.setTncDesc(tncDesc);
								GlobalValue.tncObj.setNotes(notes);
								Toast.makeText(ActivityEditTnC.this,
										"Updated successfully!", Toast.LENGTH_SHORT)
										.show();
							}
						} catch (Exception ex) {
							throw new RuntimeException(ex);
						}
					}

					@Override
					public void onError() {
						// TODO Auto-generated method stub

					}
				});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void onCancel(AmbilWarnaDialog dialog) {
		showKeyboard();
	}

	@Override
	public void onOk(AmbilWarnaDialog dialog, int color) {
		txtNotes.setColor(color, selectionStart, selectionEnd);
		imgChangeColor.setBackgroundColor(color);

		showKeyboard();
	}

	private void closeKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if (txtNotes.hasFocus()) {
			imm.hideSoftInputFromWindow(txtNotes.getWindowToken(), 0);
		} else {
			imm.hideSoftInputFromWindow(txtNotes.getWindowToken(), 0);
		}
	}

	private void showKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if (txtNotes.hasFocus()) {
			imm.showSoftInput(txtNotes, InputMethodManager.SHOW_IMPLICIT);
		}
	}

}
