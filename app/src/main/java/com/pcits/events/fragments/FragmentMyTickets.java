package com.pcits.events.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.pcits.events.ActivityDetailTickets;
import com.pcits.events.R;

public class FragmentMyTickets extends Fragment {

	private View v;
	private LinearLayout llTickets;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		v = inflater.inflate(R.layout.item_tickets, container, false);
		llTickets = (LinearLayout) v.findViewById(R.id.llTickets);
		llTickets.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity(),
						ActivityDetailTickets.class);
				startActivity(i);
			}
		});

		return v;
	}
}
