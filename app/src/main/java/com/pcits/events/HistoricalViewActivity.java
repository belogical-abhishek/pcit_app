package com.pcits.events;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.pcits.events.adapters.HistoricalEventsPagerAdapter;

public class HistoricalViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historical_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.HVAtoolbar);
        setSupportActionBar(toolbar);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.HVAtab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Public Events"));
        tabLayout.addTab(tabLayout.newTab().setText("Private Events"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        //tabLayout.setTabTextColors(new Color(Color.parseColor("#ffffff")));

        final ViewPager viewPager = (ViewPager) findViewById(R.id.HVApager);
        final HistoricalEventsPagerAdapter adapter = new HistoricalEventsPagerAdapter(getFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(HistoricalViewActivity.this,ActivityHome.class);
        startActivity(intent);
        finish();
    }
}


