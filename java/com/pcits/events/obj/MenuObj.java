package com.pcits.events.obj;


public class MenuObj {

	public static String ID_HOME = "homeId";
	public static String ID_LOGIN = "loginId";
	public static String ID_FILTER = "filterId";
	public static String ID_SETTINGS = "settingId";
	public static String ID_HELP = "helpId";
	public static String ID_ABOUT = "aboutId";
	public static String ID_EXIT = "exitId";
	public static String ID_PROFILE = "profileId";
	public static String ID_MESSAGE = "messageId";
	public static String ID_MY_TICKETS = "myTicketsId";
	public static String ID_DASHBOARD = "dashboardId";
	public static String ID_EVENT = "eventId";
	public static String ID_Private_EVENT = "privateEventId";
	public static String ID_LIST_EVENT = "listEventId";
	public static String ID_CREAT_SURVEY = "createSurveyId";
	public static String ID_SCAN_TICKET = "scanTicketId";
	public static String ID_SCAN_WINNER = "scanWinnerId";
	public static String ID_PROMOTE_EVENT = "promoteEventId";
	public static String ID_CALENDAR = "calendarId";
	public static String ID_TNC = "tncId";
	public static String ID_CATEGORY = "categoryId";
	public static String ID_QUESTIONAIRE = "questionaireId";
	public static String ID_LOGOUT = "logoutId";
	public static String ID_Event_Private = "logoutId";

	private String id, title;
	private int icon;

	public MenuObj(String id, String title, int icon) {
		this.id = id;
		this.title = title;
		this.icon = icon;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

}
