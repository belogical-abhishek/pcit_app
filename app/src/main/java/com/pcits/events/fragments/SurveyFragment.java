package com.pcits.events.fragments;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pcits.common.utils.GsonUtility;
import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityHome;
import com.pcits.events.R;
import com.pcits.events.adapters.AdapterOptionsGroups;
import com.pcits.events.adapters.AdapterTypeQuestion;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.QuestionInfo;
import com.pcits.events.widgets.KenBurnsView;

public class SurveyFragment extends Fragment {

	// Declare object of userFunctions and Utils class
	private UserFunctions userFunction = new UserFunctions();
	private JSONObject json;
	private int intLengthData;
	// type question
	private ArrayList<HashMap<String, String>> typeQuestionItems;
	public static String[] sTypeId;
	public static String[] sTypeName;
	private String typeId;
	private AdapterTypeQuestion typeQuestionAdapter;
	// OptionsGroups
	private ArrayList<HashMap<String, String>> OptionsGroupsItems;
	public static String[] sOptionsGroupsId;
	public static String[] sOptionsGroupsName;
	private String OptionsGroupsId;
	private AdapterOptionsGroups OptionsGroupsAdapter;
	//
	private Button btnAddQuestion, btnAnswer;
	private LinearLayout llQuestion, llAnswer;
	private View view;
	private QuestionInfo question;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	//
	private TextView lblType, lblOptionsGroups;
	private EditText txtQuestion, txtquestionSub, txtanswer;
	private int check;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragment_question, container, false);
		try{
			initUI();
			initControl();
			typeQuestionItems = new ArrayList<HashMap<String, String>>();
			OptionsGroupsItems = new ArrayList<HashMap<String, String>>();
			AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
	
				@Override
				protected Void doInBackground(Void... params) {
					try{
						// Put in code over here that does the network related
						// operations.
						getDataListTypeQuestion();
						getDataListOptionsGroups();
					} catch (Exception ex) {
						Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		            }
					return null;
				}
	
			};
			task.execute();
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return view;
	}

	private void initUI() {
		btnAddQuestion = (Button) view.findViewById(R.id.btnAddQuestion);
		llQuestion = (LinearLayout) view.findViewById(R.id.llQuestion);
		llAnswer = (LinearLayout) view.findViewById(R.id.llAnswer);
		llMenu = (LinearLayout) view.findViewById(R.id.llMenu);
		lblType = (TextView) view.findViewById(R.id.lblType);
		txtQuestion = (EditText) view.findViewById(R.id.txtquestion);
		lblOptionsGroups = (TextView) view.findViewById(R.id.lblGroupName);
		txtquestionSub = (EditText) view.findViewById(R.id.txtquestionSub);
		txtanswer = (EditText) view.findViewById(R.id.txtanswer);
		btnAnswer = (Button) view.findViewById(R.id.btnAnswer);
	}

	private void initControl() {
		try{
			// btnAddmore.setOnClickListener(new OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// TODO Auto-generated method stub
			// EditText valueTV = new EditText(getActivity());
			// valueTV.setId(5);
			// valueTV.setBackgroundResource(R.drawable.bg_edittext);
			// valueTV.setPadding(8, 8, 8, 8);
			// valueTV.setLayoutParams(new LayoutParams(
			// LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			//
			// ((LinearLayout) llAns).addView(valueTV);
			// }
			// });
	
			llMenu.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
					Intent intent=new Intent(getActivity(),ActivityHome.class);
					getActivity().startActivity(intent);
					getActivity().finish();
				}
			});
	
			lblType.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					final Dialog dialog = new Dialog(getActivity());
					dialog.getWindow();
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.setContentView(R.layout.layout_filter_category);
					final ListView list = (ListView) dialog
							.findViewById(R.id.lsvFilterCategory);
					typeQuestionAdapter = new AdapterTypeQuestion(getActivity(),
							typeQuestionItems);
					list.setAdapter(typeQuestionAdapter);
					list.setOnItemClickListener(new OnItemClickListener() {
	
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							// TODO Auto-generated method stub
							lblType.setText(sTypeName[position]);
							typeId = sTypeId[position];
							dialog.dismiss();
						}
					});
					dialog.show();
				}
			});
			//
			lblOptionsGroups.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					final Dialog dialog = new Dialog(getActivity());
					dialog.getWindow();
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.setContentView(R.layout.layout_filter_category);
					final ListView list = (ListView) dialog
							.findViewById(R.id.lsvFilterCategory);
					OptionsGroupsAdapter = new AdapterOptionsGroups(getActivity(),
							OptionsGroupsItems);
					list.setAdapter(OptionsGroupsAdapter);
					list.setOnItemClickListener(new OnItemClickListener() {
	
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							// TODO Auto-generated method stub
							lblOptionsGroups.setText(sOptionsGroupsName[position]);
							OptionsGroupsId = sOptionsGroupsId[position];
							dialog.dismiss();
						}
					});
					dialog.show();
				}
			});
			//
			btnAddQuestion.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					check = 1;
					String message = validate();
					if (!message.equals("success")) {
						Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT)
								.show();
					} else {
						if (question != null) {
							addQuestion(question);
						}
					}
				}
			});
			//
			btnAnswer.setOnClickListener(new OnClickListener() {
				String answer = txtanswer.getText().toString();
	
				@Override
				public void onClick(View v) {
					check = 2;
					// TODO Auto-generated method stub
					ModelManager.createAnswer(getActivity(), OptionsGroupsId,
							answer, Integer.toString(check), true,
							new ModelManagerListener() {
	
								@Override
								public void onSuccess(Object object) {
									// TODO Auto-generated method stub
									String json = (String) object;
									Log.e("aaaaa", "result :" + json);
									Toast.makeText(getActivity(), "Successfully",
											Toast.LENGTH_SHORT).show();
									// Intent i = new Intent(ActivitySignUp.this,
									// ActivityLogin.class);
									// startActivity(i);
								}
	
								@Override
								public void onError() {
									// TODO Auto-generated method stub
	
								}
	
							});
				}
			});
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	// test
	private String validate() {
		String message = "failure";
		String name = txtQuestion.getText().toString();
		String text = txtquestionSub.getText().toString();
		// String ans = txtanswer.getText().toString();

		if (name.isEmpty()) {
			Toast.makeText(getActivity(), "Bug", Toast.LENGTH_SHORT).show();
			return message;
		}
		if (text.isEmpty()) {
			Toast.makeText(getActivity(), "Bug", Toast.LENGTH_SHORT).show();
			return message;
		}
		// if (ans.isEmpty()) {
		// Toast.makeText(getActivity(), "Bug", Toast.LENGTH_SHORT).show();
		// }
		else {
			 message = "success";
			question = new QuestionInfo();
			question.setUserId(GlobalValue.myUser.getUsername());
			question.setQuestion_name(name.trim());
			question.setQuestion_subtext(text.trim());
			question.setAnswer_required("1");
			question.setSurveyId("");
			question.setOption_group_id(OptionsGroupsId);
			question.setTypeId(typeId);
			// question.setAnswer(ans.trim());
			question.setAllow("");
		}
		return message;
	}

	// Method to get Data from Server
	public void getDataListTypeQuestion() {

		try {

			json = userFunction.typeQuestionList();
			if (json != null) {
				JSONArray dataDealsArray = json.getJSONArray("data");

				intLengthData = dataDealsArray.length();
				sTypeId = new String[intLengthData];
				sTypeName = new String[intLengthData];

				// Store data to variable array
				for (int i = 0; i < intLengthData; i++) {
					JSONObject dealObject = dataDealsArray.getJSONObject(i);
					HashMap<String, String> mapTypeQuestion = new HashMap<String, String>();
					sTypeId[i] = dealObject.getString(userFunction.key_type_id);
					sTypeName[i] = dealObject
							.getString(userFunction.key_type_name);

					mapTypeQuestion.put(userFunction.key_type_id, sTypeId[i]);
					mapTypeQuestion.put(userFunction.key_type_name,
							sTypeName[i]);
					typeQuestionItems.add(mapTypeQuestion);
				}
			}
		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public void getDataListOptionsGroups() {

		try {

			json = userFunction.showOptionsGroups();
			if (json != null) {
				JSONArray dataDealsArray = json.getJSONArray("data");

				intLengthData = dataDealsArray.length();
				sOptionsGroupsId = new String[intLengthData];
				sOptionsGroupsName = new String[intLengthData];

				// Store data to variable array
				for (int i = 0; i < intLengthData; i++) {
					JSONObject dealObject = dataDealsArray.getJSONObject(i);
					HashMap<String, String> mapOptionsGroups = new HashMap<String, String>();
					sOptionsGroupsId[i] = dealObject
							.getString(userFunction.key_OptionsGroups_id);
					sOptionsGroupsName[i] = dealObject
							.getString(userFunction.key_OptionsGroups_name);

					mapOptionsGroups.put(userFunction.key_OptionsGroups_id,
							sOptionsGroupsId[i]);
					mapOptionsGroups.put(userFunction.key_OptionsGroups_name,
							sOptionsGroupsName[i]);
					OptionsGroupsItems.add(mapOptionsGroups);
				}
			}
		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	private void addQuestion(QuestionInfo question) {
		try{
			// TODO Auto-generated method stub
			QuestionInfo data = addQuestionJson(question);
			Log.e("ACCOUNT INFO", "ACCOUNT INFO : " + GsonUtility.convertObjectToJSONString(data));
			ModelManager.createQuestion(getActivity(), data,
					Integer.toString(check), true, new ModelManagerListener() {
	
						@Override
						public void onSuccess(Object object) {
							// TODO Auto-generated method stub
							String json = (String) object;
							Log.e("aaaaa", "result :" + json);
							Toast.makeText(getActivity(), checkResult(json),
									Toast.LENGTH_SHORT).show();
							// Intent i = new Intent(ActivitySignUp.this,
							// ActivityLogin.class);
							// startActivity(i);
						}
	
						@Override
						public void onError() {
							// TODO Auto-generated method stub
	
						}
	
					});
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	private QuestionInfo addQuestionJson(QuestionInfo question) {
		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();
		try {
			json.put("userId", question.getUserId());
			json.put("question_name", question.getQuestion_name());
			json.put("question_subtext", question.getQuestion_subtext());
			json.put("surveyId", question.getSurveyId());
			json.put("answer_required", question.getAnswer_required());
			json.put("option_group_id", question.getOption_group_id());
			json.put("type", question.getTypeId());
			// json.put("answer", question.getAnswer());
			json.put("allow", question.getAllow());

		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
		return question;
	}

	protected String checkResult(String strJson) {
		// TODO Auto-generated method stub
		JSONObject json = null;
		String message = "";
		try {
			json = new JSONObject(strJson);
			if (json.getString("status").equals("success")) {
				message = this.getString(R.string.message_success);
				llQuestion.setVisibility(View.GONE);
				llAnswer.setVisibility(View.VISIBLE);
			} else {
				message = json.getString("message");
			}

		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }

		return message;
	}

}
