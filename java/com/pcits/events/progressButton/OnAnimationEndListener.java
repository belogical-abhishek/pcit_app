package com.pcits.events.progressButton;

interface OnAnimationEndListener {

    public void onAnimationEnd();
}
