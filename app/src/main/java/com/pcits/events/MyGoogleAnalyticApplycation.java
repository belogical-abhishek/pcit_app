package com.pcits.events;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;

//import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.analytics.tracking.android.GAServiceManager;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Logger.LogLevel;
import com.google.analytics.tracking.android.Tracker;
import com.google.firebase.FirebaseApp;

import io.fabric.sdk.android.Fabric;

public class MyGoogleAnalyticApplycation extends Application {
	private static GoogleAnalytics mGa;
	private static Tracker mTracker;
	private static final int GA_DISPATCH_PERIOD = 30;

	private String GOOGLE_ANALYTIC_KEY = "UA-54295529-1";

	// Prevent hits from being sent to reports, i.e. during testing.
	private static final boolean GA_IS_DRY_RUN = false;

	private static final LogLevel GA_LOG_VERBOSITY = LogLevel.INFO;

	private static final String TRACKING_PREF_KEY = "trackingPreference";

	@SuppressWarnings("deprecation")
	private void initializeGa() {

		mGa = GoogleAnalytics.getInstance(this);
		Fresco.initialize(this);
		MultiDex.install(this);
		mTracker = mGa.getTracker(GOOGLE_ANALYTIC_KEY);
		// Set dispatch period.
		GAServiceManager.getInstance().setLocalDispatchPeriod(
				GA_DISPATCH_PERIOD);

		// Set dryRun flag.
		mGa.setDryRun(GA_IS_DRY_RUN);

		// Set Logger verbosity.
		mGa.getLogger().setLogLevel(GA_LOG_VERBOSITY);

		// Set the opt out flag when user updates a tracking preference.
		SharedPreferences userPrefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		userPrefs
				.registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
					@Override
					public void onSharedPreferenceChanged(
							SharedPreferences sharedPreferences, String key) {
						if (key.equals(TRACKING_PREF_KEY)) {
							GoogleAnalytics
									.getInstance(getApplicationContext())
									.setAppOptOut(
											sharedPreferences.getBoolean(key,
													false));
						}
					}
				});
	}

	@Override
	public void onCreate() {
		super.onCreate();

		//Todo change before deployment
		Fabric.with(this, new Crashlytics.Builder().core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build()).build());
		//Fabric.with(this, new Crashlytics());
//		Crashlytics.start(this);
		initializeGa();

	}

	/*
	 * Returns the Google Analytics tracker.
	 */
	public static Tracker getGaTracker() {
		return mTracker;
	}

	/*
	 * Returns the Google Analytics instance.
	 */
	public static GoogleAnalytics getGaInstance() {
		return mGa;
	}
}