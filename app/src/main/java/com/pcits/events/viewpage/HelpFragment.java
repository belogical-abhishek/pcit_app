package com.pcits.events.viewpage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pcits.events.R;
@SuppressLint("ValidFragment")

public class HelpFragment extends Fragment{

	
	private int mCurrentPage;
	private SharedPreferences preferences;
	private SharedPreferences.Editor editor;
	private int intLevel;
	private String strbattery_details="";
	Context mainActivity;
	public HelpFragment() {



	}
	public HelpFragment(Context mainActivity) {

		this.mainActivity=mainActivity;
		// TODO Auto-generated constructor stub
		preferences = PreferenceManager.getDefaultSharedPreferences(this.mainActivity);

		editor = preferences.edit();
		strbattery_details = preferences.getString("battery_details", "");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/** Getting the arguments to the Bundle object */
		Bundle data = getArguments();

		/** Getting integer data of the key current_page from the bundle */
		mCurrentPage = data.getInt("current_page", 1);


	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {		

		View v = null;

		if(mCurrentPage==1)
		{
			Resources res = getResources();
			String text = String.format(res.getString(R.string.help1));
			v = inflater.inflate(R.layout.layout_help, container,false);
			TextView lblIntro1 = (TextView) v.findViewById(R.id.lblIntro1);
			lblIntro1.setText(Html.fromHtml(text));
		}
		if(mCurrentPage==2)
		{
			Resources res = getResources();
			String text = String.format(res.getString(R.string.help2));
			v = inflater.inflate(R.layout.layout_help2, container,false);
			TextView lblIntro2 = (TextView) v.findViewById(R.id.lblIntro2);
			lblIntro2.setText(Html.fromHtml(text));
		}
		return v;
	}
}
