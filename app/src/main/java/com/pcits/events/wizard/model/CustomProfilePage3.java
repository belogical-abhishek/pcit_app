/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pcits.events.wizard.model;

import java.util.ArrayList;

import android.support.v4.app.Fragment;

import com.pcits.events.ProfileActivity;
import com.pcits.events.fragments.ProfileFragmentPage3;

/**
 * A page asking for a name and an email.
 */
public class CustomProfilePage3 extends Page implements ICustomPage {
	// public static final String TITLE_DATA_KEY = "title";
	// public static final String COMPANY_DATA_KEY = "company";
	// public static final String CATEGORY_DATA_KEY = "category";
	// public static final String IMAGE_DATA_KEY = "image";

	private ProfileFragmentPage3 mFragment;

	public CustomProfilePage3(ModelCallbacks callbacks, String title) {
		super(callbacks, title);
	}

	@Override
	public Fragment createFragment() {
		mFragment = ProfileFragmentPage3.create(getKey());
		return mFragment;
	}

	@Override
	public void getReviewItems(ArrayList<ReviewItem> dest) {
		if (ProfileActivity.tempClient != null) {
			dest.add(new ReviewItem("Phone", ProfileActivity.tempClient
					.getPhone(), getKey(), -1));
			dest.add(new ReviewItem("Birthday", ProfileActivity.tempClient
					.getDob(), getKey(), -1));
		} else if (ProfileActivity.tempAdmin != null) {
			dest.add(new ReviewItem("Phone", ProfileActivity.tempAdmin
					.getPhone(), getKey(), -1));
			dest.add(new ReviewItem("Birthday", ProfileActivity.tempAdmin
					.getDob(), getKey(), -1));
			dest.add(new ReviewItem("Pay info", ProfileActivity.tempAdmin
					.getPayInfo(), getKey(), -1));
		}
	}

	@Override
	public boolean isCompleted() {
		// boolean completed = false;
		// if (TextUtils.isEmpty(mData.getString(TITLE_DATA_KEY))
		// || TextUtils.isEmpty(mData.getString(COMPANY_DATA_KEY))) {
		// completed = false;
		// } else {
		// completed = true;
		// }
		// return completed;
		return true;
	}

	@Override
	public void saveState() {
		// TODO Auto-generated method stub
		mFragment.saveState();
	}

	@Override
	public void restoreState() {
		// TODO Auto-generated method stub
		mFragment.restoreState();
	}

}
