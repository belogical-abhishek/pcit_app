/**
 * File        : UserFuntions.java
 * App name    : M1SO Events
 * Version     : 1.1.4
 * Created     : 25/05/14

 * Created by Team M1SO on 21/01/14.
 * Copyright (c) 2014 M1SO. All rights reserved.
 */

package com.pcits.events.libraries;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.obj.APIObj;

public class UserFunctions {

	private static final String TAG = "UserFunctions";

	private JSONParser jsonParser;

	// Web Service
	//private final static String Server = "http://pcits.com/";
	private final static String Server = "http://54.173.65.38/";

	// Folder
	//private final static String folderMain = "events/";
	private final static String folderMain = "events-local/";
	private final static String folderApi = "pcitsapi/";
	private final static String mApi = "api/";
	public final static String protocol = "http://";
	public final static String server_without_protocol = "54.173.65.38/";

	// Url
	private final static String URLApi = Server + folderMain + folderApi;
	//private final static String URLApi = "http://54.173.65.38/events-local/api/";
	private final static String FSapi = Server + folderMain + mApi;
	public static final String URLAdmin = Server + folderMain;

	// Service
	private static final String service_latest_deals = "latest_deals?";
	private static final String service_featured_deals = "featured_deals?";
	private static final String service_popular_deals = "popular_deals?";
	private static final String service_my_favorite = "my_favorite?";
	private final static String service_deals_by_user = "dealsByUser?";
	private final String service_notif = "notify_new_deal?";
	private final String service_category_list = "category_list?";
	private final static String service_tnc_list = "tnc_list?";
	private final static String service_tnc_list_ai = "tnc_list";
	private final String service_deal_by_category = "deal_by_category?";
	private final static String service_deal_detail = "deal_detail?";
	private final String service_deal_by_search = "deal_by_search_name?";
	private final String service_deal_around_you = "deal_around_you?";
	private final static String service_currency = "currency";
	public final String service_view_deal = "view-deal.php?";
	private final String service_desc = "description.php?";
	private final String service_search_event = "searchEvent.php?";
	private final String service_show_location = "showByDeal.php";
	private final String service_sort_date = "sortByDate.php?";
	private final String service_show_company = "showCompany.php?";
	private final String service_show_typeQuestion = "showTypeQuestion.php";
	private final String service_show_showOptionsGroups = "showOptionsGroups.php";
	private final static String service_create_account = "createAccount.php";
	private final static String service_login = "login.php";
	private final static String service_login_account = "loginAccount.php";
	private final static String service_login_user = "loginUser.php";
	private final static String service_update_client = "updateClient.php";
	private final static String service_update_admin = "updateAdmin.php";
	private final static String service_attended = "aceptedTC.php";
	private final static String service_check_attended = "checkAceptedTC.php";
	private final static String service_check_luckydraw = "checkLuckyDraw.php";
	private final static String service_change_password = "changePassword.php";
	private final static String service_create_user = "createUser.php";
	private final static String service_create_deals = "createDeal.php";
	private final static String service_edit_deals = "editDeal.php";
	private final static String service_create_category = "createCategory.php";
	private final static String service_create_tnc = "createTnC.php";
	private final static String service_register_device = "RegisterDevice.php";
	private final static String service_login_media = "loginFB.php";
	private final static String service_edit_tnc = "editTnC.php";
	private final static String service_edit_category = "editCategory.php";
	private final static String service_update_gcm = "updateGcm.php";
	private final static String service_show_tnc_by_user = "showTncByUser.php";
	private final static String service_show_category = "showCategory.php";
	private final String service_show_country = "showCountry.php";
	private final static String service_show_tnc = "showTnC.php";
	private final static String service_near_me = "nearMe.php?";
	private final static String service_paidtickets = "paidtickets.php";
	private final static String service_show_paidtickets = "showTickets.php";
	private final static String service_update_paidtickets = "updatePaidticket.php";
	private final static String service_ticket_by_event = "ticketByEvent.php";
	private final static String service_user_purchsae = "userPurchase.php";
	private final static String service_create_question = "createQuestion.php";
	private final static String service_calender_view = "calenderView?";
	private final static String service_push = "push.php";
	private final static String service_show_message = "showMessage.php";
	private final static String service_update_winner = "updateLuckyDraw.php";
	private final static String service_show_attended = "showAttended.php";
	private final static String service_show_luckydraw = "showLuckyDraw.php";
	private static final String service_get_list_of_ticket = "listTicketsPaid.php";
	private static final String service_get_list_notifybox = "listNotifyBox.php";
	private final static String service_update_notifybox = "updateNotifyBox.php";
	private final static String service_super_dashboard = "DashBoardSuper.php";
	private final static String service_normal_admin_dashboard = "NormalAdminDashBoard.php";
	private final static String service_get_list_of_admin = "showListAdmin.php";
	private final static String service_active_admin = "activeAdmin.php";
	private final static String service_event_dashboard = "EventDashBoard.php";
	private final static String service_update_admin_credits = "updateAdminCredits.php";
	private final static String service_updateFeaturedDate = "updateFeaturedDate.php";
	private final static String service_buy_credits = "payCredits.php";
	private static final String service_upcoming_deal = "Upcoming.php?";
	private static final String service_forget_password = "ForgetPassword.php";
	private static final String service_update_click_count = "updateClickEvent.php";

	// Param
	private final static String param_featured = "featured=";
	private final static String param_popular = "number=";
	private final static String param_username = "username=";
	private final static String param_client = "client=";
	public final static String param_start_index = "start_index=";
	public final static String param_items_per_page = "items_per_page=";
	private final String param_category_id = "category_id=";
	private final static String param_deal_id = "deal_id=";
	private final String param_user_lat = "user_lat=";
	private final String param_user_lng = "user_lng=";
	private final String param_keyword = "keyword=";
	private final String param_minprice = "minprice=";
	private final String param_maxprice = "maxprice=";
	private final String param_type = "type=";
	private final String param_address = "address=";
	private final String param_city = "city=";
	private final String param_county = "county=";
	private final String param_distance = "distance=";
	private final String param_lat = "lat=";
	private final String param_long = "long=";
	private final String param_mindiscount = "mindiscount=";
	private final String param_maxdiscount = "maxdiscount=";
	private final String param_category = "category=";
	private final String param_title = "title=";
	private final String param_start_date = "start_date=";
	private final String param_company = "company=";
	private final String param_sort_date_asc = "sort_date_asc=";
	private final String param_sort_date_desc = "sort_date_desc=";
	private final String param_sort_date_added_asc = "sort_dealid_asc=";
	// Account
	private final String param_userid = "clientname=";
	// Change Password
	private final String param_oldpassword = "oldpassword=";
	private final String param_newpassword = "newpassword=";

	// Key object name to get value
	public final String key_deals_id = "deal_id";
	public final String key_deals_tnc_id = "tnc_id";
	public final String key_deals_title = "title";
	public final String key_deals_starttimestamp = "start_timestamp";
	public final String key_deals_endtimestamp = "end_timestamp";
	public final String key_deals_date_start = "start_date";
	public final String key_deals_date_end = "end_date";
	public final String key_deals_time_start = "start_time";
	public final String key_deals_time_end = "end_time";
	public final String key_deals_after_disc_value = "after_discount_value";
	public final String key_deals_start_value = "start_value";
	public final String key_deals_image = "image";
	public final String key_deals_attended = "attended";
	public final String key_deals_quantity = "quantity";
	public final String key_deals_company = "company";
	public final String key_deals_address = "address";
	public final String key_deals_city = "city";
	public final String key_deals_county = "county";
	public final String key_deals_country = "country";
	public final String key_deals_postcode = "postcode";
	public final String key_deals_category = "category_name";
	public final String key_deals_category_id = "category_id";
	public final String key_deals_tnc_desc = "tnc_desc";
	public final String key_deals_lat = "latitude";
	public final String key_deals_lng = "longitude";
	public final String key_deals_url = "deal_url";
	public final String key_deals_disc = "discount";
	public final String key_deals_save = "save_value";
	public final String key_deals_desc = "description";
	public final String key_deals_tnc_notes = "tnc_notes";
	public final String key_deals_username = "username";
	public final String key_category_id = "category_id";
	public final String key_category_name = "category_name";
	public final String key_category_marker = "category_marker";
	public final String key_currency_code = "code";
	// type question
	public final String key_type_id = "id";
	public final String key_type_name = "input_type_name";
	// showOptionsGroups
	public final String key_OptionsGroups_id = "int";
	public final String key_OptionsGroups_name = "option_group_name";
	//
	public final String key_tnc_id = "tnc_id";
	public final String key_tnc_desc = "tnc_desc";
	public final String key_tnc_notes = "notes";
	public final String key_tnc_username = "username";
	//
	public final String key_showlocation_title = "title";
	public final String key_showlocation_address = "address";
	public final String key_showlocation_city = "city";
	public final String key_showlocation_county = "county";
	public final String key_showlocation_company = "company";
	public final String key_showcountry_country = "country";
	// detail account
	public final String key_client_id = "id";
	public final String key_client_fname = "fname";
	public final String key_client_lname = "lname";
	public final String key_client_email = "email";
	public final String key_client_dob = "dob";
	public final String key_client_gender = "gender";
	public final String key_client_address = "address";
	public final String key_client_city = "city";
	public final String key_client_county = "county";
	public final String key_client_postcode = "postcode";
	public final String key_client_country = "country";
	public final String key_client_phone = "phone";
	public final String key_client_username = "client_username";
	public final String key_client_password = "password";

	// Array
	public final String array_latest_deals = "latestDeals";
	public final String array_deals_by_user = "dealsByUser";
	public final String array_calender_view = "calenderView";
	public final String array_category_list = "categoryList";
	public final String array_tnc_list = "tncList";
	public final String array_deal_detail = "dealDetail";
	public final String array_place_by_search = "dealBySearchName";
	public final String array_around_you = "dealAroundYou";
	public final String array_deal_by_category = "dealByCategory";
	public final String array_currency = "currency";
	public final String array_show_location = "showLocation";
	public final String array_filter = "filter";

	// LoadUrl
	public final String varLoadURL = Server + folderMain + service_desc
			+ param_deal_id;
	private String webService;
	public final static int valueItemsPerPage = 10;
	public final static int number = 10;
	public final static int featured = 1;

	public final static String KEY_JSON_STATUS = "status";
	public final static String KEY_JSON_DATA = "data";
	public final static String JSON_STATUS_SUCCESS = "success";

	// url api
	public static String URL_LOGIN = FSapi + service_login;
	public static String URL_LOGIN_ClIENT = FSapi + service_login_account;
	public static String URL_LOGIN_USER = FSapi + service_login_user;
	public static String URL_REGISTER_CLIENT = FSapi + service_create_account;
	public static String URL_UPDATE_CLIENT = FSapi + service_update_client;
	public static String URL_ATTENDED = FSapi + service_attended;
	public static String URL_CHECK_ATTENDED = FSapi + service_check_attended;
	public static String URL_CHECK_LUCKY = FSapi + service_check_luckydraw;
	public static String URL_CHANGE_PASSWORD = FSapi + service_change_password;
	public static String URL_CREATE_ADMIN = FSapi + service_create_user;
	public static String URL_UPDATE_ADMIN = FSapi + service_update_admin;
	public static String URL_CREATE_DEAL = FSapi + service_create_deals;
	public static String URL_UPDATE_DEAL = FSapi + service_edit_deals;
	public static String URL_UPDATE_CATEGORY = FSapi + service_edit_category;
	public static String URL_CREATE_CATEGORY = FSapi + service_create_category;
	public static String URL_CREATE_TNC = FSapi + service_create_tnc;
	public static String URL_LOGIN_MEDIA = FSapi + service_login_media;
	public static String URL_REGISTER_DEVICE = FSapi + service_register_device;
	public static String URL_UPDATE_GCM = FSapi + service_update_gcm;
	public static String URL_UPDATE_TICKETS = FSapi
			+ service_update_paidtickets;
	public static String URL_UPDATE_NOTIFYBOX = FSapi
			+ service_update_notifybox;
	public static String URL_TICKETS_BY_EVENT = FSapi + service_ticket_by_event;
	public static String URL_SHOW_MESSAGE = FSapi + service_show_message;
	public static String URL_UPDATE_WINNER = FSapi + service_update_winner;

	public static final String URL_CURRENCY = URLApi + service_currency;
	// public static String URL_SHOW_TNC_BY_USER = FSapi
	// + service_show_tnc_by_user;
	public static String URL_SHOW_TNC_BY_USER = URLApi + service_tnc_list_ai;
	public static String URL_SHOW_CATEGORY = FSapi + service_show_category;
	public static String URL_EDIT_TNC = FSapi + service_edit_tnc;
	public static String URL_SHOW_ALL_TNC = FSapi + service_show_tnc;
	public static String URL_REGISTER_PAIDTICKETS = FSapi + service_paidtickets;
	public static String URL_SHOW_PAIDTICKETS = FSapi
			+ service_show_paidtickets;
	public static String URL_USER_PURCHASE = FSapi + service_user_purchsae;
	public static String URL_CREATE_QUESTION = FSapi + service_create_question;
	public static String URL_WINNER = FSapi + service_push;
	public static String URL_SHOW_ATTENDED = FSapi + service_show_attended;
	public static String URL_SHOW_LUCKY = FSapi + service_show_luckydraw;
	public static String URL_GET_LIST_OF_TICKET = FSapi
			+ service_get_list_of_ticket;
	public static String URL_GET_LIST_NOTIFYBOX = FSapi
			+ service_get_list_notifybox;

	public static String URL_GET_SUPER_DASHBOARD = FSapi
			+ service_super_dashboard;
	public static String URL_GET_NORMAL_ADMIN_DASHBOARD = FSapi
			+ service_normal_admin_dashboard;
	public static String URL_GET_LIST_OF_ADMIN = FSapi
			+ service_get_list_of_admin;
	public static String URL_ACTIVE_ADMIN = FSapi + service_active_admin;
	public static String URL_EVENT_DASHBOARD = FSapi + service_event_dashboard;
	public static String URL_UPDATE_ADMIN_CREDITS = FSapi
			+ service_update_admin_credits;
	public static String URL_UPDATE_FEATURED = FSapi
			+ service_updateFeaturedDate;
	public static String URL_BUY_CREDITS = FSapi + service_buy_credits;
	public static String URL_GET_UPCOMING_DEAL = FSapi + service_upcoming_deal;
	public static String URL_FORGET_PASSWORD = FSapi + service_forget_password;
	public static String URL_UPDATE_CLICK_COUNT = FSapi + service_update_click_count;

	//
	public static String user;
	public static String URL_ALL_DEALS = URLApi + service_latest_deals
			+ param_start_index;
	public static String URL_DEAL_DETAIL = URLApi + service_deal_detail
			+ param_deal_id;
	public static String URL_DEAL_FEATURED = URLApi + service_featured_deals
			+ param_featured + featured;
	public static String URL_DEAL_POPULAR = URLApi + service_popular_deals
			+ param_popular + number;

	public static String URL_DEAL_BY_USER = URLApi + service_deals_by_user
			+ param_username;

	// constructor
	public UserFunctions() {
		jsonParser = new JSONParser();
	}

	/**
	 * function make Login Request
	 * 
	 * @param email
	 * @param password
	 * */

	public JSONObject changePassword(String oldPass, String newPass,
			String userid) {
		try{
			webService = FSapi + service_change_password + param_oldpassword
					+ oldPass + "&" + param_newpassword + newPass + "&"
					+ param_userid + userid;
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject showTnC(String dealid) {
		try{
			webService = FSapi + service_show_tnc + param_deal_id + dealid;
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject tncList(String username, int type) {
		try{
			webService = URLApi + service_tnc_list + param_username + username
					+ "&" + param_type + type;
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject getNotif() {
		try{
			webService = URLApi + service_notif;
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject latestDeals(String client, int valueStartIndex,
			Context context) {
		try{
			webService = URLApi + service_latest_deals + param_client + client
					+ "&" + param_start_index + valueStartIndex + "&"
					+ param_items_per_page + valueItemsPerPage;
			Log.e(TAG, "latestDeals: "+webService);
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			
			// Insert/update into database
			
			if (!DatabaseUtility.checkExistsApi(context,
					UserFunctions.URL_ALL_DEALS + valueStartIndex)) {
				APIObj apiInfo = new APIObj();
				Log.e(TAG, "DATA" + " chua co du lieu");
				// add vao database
				apiInfo.setmApi(UserFunctions.URL_ALL_DEALS + valueStartIndex);
				if (DatabaseUtility.insertApi(context, apiInfo)) {
					Log.e(TAG, "INSERT thanh cong");
				} else {
					Log.e(TAG, "INSERT khong  thanh cong");
				}

			} else {
				Log.e("", "DATA" + "data da co du lieu");

			}
	
			if (DatabaseUtility.checkExistsApi(context,
					UserFunctions.URL_ALL_DEALS + valueStartIndex)) {
				if (DatabaseUtility.updateResuft_Api(context, json.toString(),
						UserFunctions.URL_ALL_DEALS + valueStartIndex)) {

				} else {
					Log.e(TAG, "update " + "update khong thanh cong");
				}
			}
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject featuredDeals(String client, Context context) {
		try{
			webService = URLApi + service_featured_deals + param_client + client
					+ "&" + param_featured + featured;
			JSONObject json = jsonParser.getJSONFromUrl(webService);
	
			// Insert/update into database
			if (!DatabaseUtility.checkExistsApi(context, URL_DEAL_FEATURED)) {
				APIObj apiInfo = new APIObj();
				Log.e(TAG, "DATA" + " chua co du lieu");
				// add vao database
				apiInfo.setmApi(URL_DEAL_FEATURED);
				if (DatabaseUtility.insertApi(context, apiInfo)) {
					Log.e(TAG, "INSERT thanh cong");
				} else {
					Log.e(TAG, "INSERT khong  thanh cong");
				}

			} else {
				Log.e("", "DATA" + "data da co du lieu");
			}
			if (DatabaseUtility.checkExistsApi(context, URL_DEAL_FEATURED)) {
				if (DatabaseUtility.updateResuft_Api(context, json.toString(),
						URL_DEAL_FEATURED)) {

				} else {
					Log.e(TAG, "update " + "update khong thanh cong");
				}
			}
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject popularDeals(String client, Context context) {
		try {
			webService = URLApi + service_popular_deals + param_client + client
					+ "&" + param_popular + number;
			JSONObject json = jsonParser.getJSONFromUrl(webService);
	
			// Insert/update into database

			if (!DatabaseUtility.checkExistsApi(context, URL_DEAL_POPULAR)) {
				APIObj apiInfo = new APIObj();
				Log.e(TAG, "DATA" + " chua co du lieu");
				// add vao database
				apiInfo.setmApi(URL_DEAL_POPULAR);
				if (DatabaseUtility.insertApi(context, apiInfo)) {
					Log.e(TAG, "INSERT thanh cong");
				} else {
					Log.e(TAG, "INSERT khong  thanh cong");
				}

			} else {
				Log.e("", "DATA" + "data da co du lieu");

			}

			if (DatabaseUtility.checkExistsApi(context, URL_DEAL_POPULAR)) {
				if (DatabaseUtility.updateResuft_Api(context, json.toString(),
						URL_DEAL_POPULAR)) {

				} else {
					Log.e(TAG, "update " + "update khong thanh cong");
				}
			}
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject myfavorite(String clientname, final Context context) {
		webService = URLApi + service_my_favorite + param_userid + clientname;
		JSONObject json = jsonParser.getJSONFromUrl(webService);
		return json;
	}

	public JSONObject dealsByUser(String username, int valueStartIndex,
			Context context) {
		try{
			webService = URLApi + service_deals_by_user + param_username + username
					+ "&" + param_start_index + valueStartIndex + "&"
					+ param_items_per_page + valueItemsPerPage;
			;
			JSONObject json = jsonParser.getJSONFromUrl(webService);

			// Insert/update into database
			user = username;
		
			if (!DatabaseUtility.checkExistsApi(context,
					UserFunctions.URL_DEAL_BY_USER + user)) {
				Log.e(TAG, "URL" + UserFunctions.URL_DEAL_BY_USER + user);
				APIObj apiInfo = new APIObj();
				Log.e(TAG, "DATA" + " chua co du lieu");
				// add vao database
				apiInfo.setmApi(UserFunctions.URL_DEAL_BY_USER + user);
				if (DatabaseUtility.insertApi(context, apiInfo)) {
					Log.e(TAG, "INSERT thanh cong");
				} else {
					Log.e(TAG, "INSERT khong  thanh cong");
				}

			} else {
				Log.e("", "DATA" + "data da co du lieu");

			}
					if (DatabaseUtility.checkExistsApi(context,
					UserFunctions.URL_DEAL_BY_USER + user)) {
				if (DatabaseUtility.updateResuft_Api(context, json.toString(),
						UserFunctions.URL_DEAL_BY_USER + user)) {

				} else {
					Log.e(TAG, "update " + "update khong thanh cong");
				}
			}
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject calenderView(String username, Context context) {
		try{
			webService = URLApi + service_calender_view + param_username + username;
			JSONObject json = jsonParser.getJSONFromUrl(webService);
	
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}


	public JSONObject aroundYou(double userLat, double userLng) {
		try{
			webService = URLApi + service_deal_around_you + param_user_lat
					+ userLat + "&" + param_user_lng + userLng;
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}


	public JSONObject dealDetail(String dealId, Context context) {
		try{
			webService = URLApi + service_deal_detail + param_deal_id + dealId;
			Log.e(TAG, "Url get detail: " + webService);
			JSONObject json = jsonParser.getJSONFromUrl(webService);
	
			// Insert/update into database
		
			if (!DatabaseUtility.checkExistsApi(context,
					UserFunctions.URL_DEAL_DETAIL + dealId)) {
				APIObj apiInfo = new APIObj();
				Log.e(TAG, "DATA" + " chua co du lieu");
				// add vao database
				apiInfo.setmApi(UserFunctions.URL_DEAL_DETAIL + dealId);
				if (DatabaseUtility.insertApi(context, apiInfo)) {
					Log.e(TAG, "INSERT thanh cong");
				} else {
					Log.e(TAG, "INSERT khong  thanh cong");
				}

			} else {
				Log.e(TAG, "data da co du lieu");
			}

			if (DatabaseUtility.checkExistsApi(context,
					UserFunctions.URL_DEAL_DETAIL + dealId)) {
				if (DatabaseUtility.updateResuft_Api(context, json.toString(),
						UserFunctions.URL_DEAL_DETAIL + dealId)) {

				} else {
					Log.e(TAG, "update " + "update khong thanh cong");
				}
			}
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject typeQuestionList() {
		webService = FSapi + service_show_typeQuestion;
		JSONObject json = jsonParser.getJSONFromUrl(webService);
		return json;
	}

	public JSONObject showOptionsGroups() {
		try{
			webService = FSapi + service_show_showOptionsGroups;
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject categoryList() {
		try{
			webService = URLApi + service_category_list;
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject companyList() {
		try{
			webService = FSapi + service_show_company;
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject showByCountry() {
		try{
			webService = FSapi + service_show_country;
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject showByDeal() {
		try{
			webService = FSapi + service_show_location;
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject dealByCategory(String valueCategoryId, int valueStartIndex) {
		try{
			webService = URLApi + service_deal_by_category + param_category_id
					+ valueCategoryId + "&" + param_start_index + valueStartIndex
					+ "&" + param_items_per_page + valueItemsPerPage;
	
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}
	// upcoming
		public JSONObject upComing(int distance, double lat, double longitude,int valueStartIndex) {
			try{
				webService = FSapi + service_upcoming_deal + param_distance + distance + "&"
						+ param_lat + lat + "&" + param_long + longitude +"&" + param_start_index + valueStartIndex
						+ "&" + param_items_per_page + valueItemsPerPage;
				//Log.e("UserFunctions", webService);
				JSONObject json = jsonParser.getJSONFromUrl(webService);
				return json;
			} catch (Exception ex) {
                throw new RuntimeException(ex);
            }
		}

	// near me
	public JSONObject nearMe(int distance, double lat, double longitude) {
		webService = FSapi + service_near_me + param_distance + distance + "&"
				+ param_lat + lat + "&" + param_long + longitude;
		Log.d(TAG, "nearMe: "+webService);
		JSONObject json = jsonParser.getJSONFromUrl(webService);
		return json;
	}

	public JSONObject filter(int type, String minprice, String maxprice,
			String company, String startDate, String title, String address,
			String city, String county, String category, int mindiscount,
			int maxdiscount, double lat, double longitude, int distance,
			int valueStartIndex) {
		try{
			webService = FSapi + service_search_event + param_type + type + "&"
					+ param_minprice + minprice + "&" + param_maxprice + maxprice
					+ "&" + param_company + company + "&" + param_title + title
					+ "&" + param_start_date + startDate + "&" + param_address
					+ address + "&" + param_city + city + "&" + param_county
					+ county + "&" + param_category + category + "&"
					+ param_mindiscount + mindiscount + "&" + param_maxdiscount
					+ maxdiscount + "&" + param_lat + lat + "&" + param_long
					+ longitude + "&" + param_distance + distance + "&"
					+ param_start_index + valueStartIndex + "&"
					+ param_items_per_page + valueItemsPerPage;
	
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject sortDateAsc(int dateAsc, int valueStartIndex) {
		try{
			webService = FSapi + service_sort_date + param_sort_date_asc + dateAsc
					+ "&" + param_start_index + valueStartIndex + "&"
					+ param_items_per_page + valueItemsPerPage;
	
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject sortDateDesc(int dateDesc, int valueStartIndex) {
		webService = FSapi + service_sort_date + param_sort_date_desc
				+ dateDesc + "&" + param_start_index + valueStartIndex + "&"
				+ param_items_per_page + valueItemsPerPage;

		JSONObject json = jsonParser.getJSONFromUrl(webService);
		return json;
	}

	public JSONObject sortDateAddAsc(int dateAddAsc, int valueStartIndex) {
		try{
			webService = FSapi + service_sort_date + param_sort_date_added_asc
					+ dateAddAsc + "&" + param_start_index + valueStartIndex + "&"
					+ param_items_per_page + valueItemsPerPage;
	
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject searchByName(String valueKeyName, int valueStartIndex) {
		try{
			webService = URLApi + service_deal_by_search + param_keyword
					+ valueKeyName + "&" + param_start_index + valueStartIndex
					+ "&" + param_items_per_page + valueItemsPerPage;
	
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public JSONObject currency(Context context) {
		webService = URLApi + service_currency;
		JSONObject json = jsonParser.getJSONFromUrl(webService);

		// Insert/update into database
		try {
			if (!DatabaseUtility.checkExistsApi(context, URL_CURRENCY)) {
				APIObj apiInfo = new APIObj();
				Log.e(TAG, "DATA" + " chua co du lieu");
				// add vao database
				apiInfo.setmApi(URL_CURRENCY);
				if (DatabaseUtility.insertApi(context, apiInfo)) {
					Log.e(TAG, "INSERT thanh cong");
				} else {
					Log.e(TAG, "INSERT khong  thanh cong");
				}
			} else {
				Log.e("", "DATA" + "data da co du lieu");
			}
		
			if (DatabaseUtility.checkExistsApi(context, URL_CURRENCY)) {
				if (DatabaseUtility.updateResuft_Api(context, json.toString(),
						URL_CURRENCY)) {
				} else {
					Log.e(TAG, "update " + "update khong thanh cong");
				}
			}
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }

		return json;
	}

	public JSONObject loadURL(String dealId) {
		try{
			webService = Server + folderMain + service_desc + param_deal_id
					+ dealId;
			JSONObject json = jsonParser.getJSONFromUrl(webService);
			return json;
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}
}