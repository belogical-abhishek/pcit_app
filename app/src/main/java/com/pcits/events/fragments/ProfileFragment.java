package com.pcits.events.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.facebook.login.LoginManager;
import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityChangePassword;
import com.pcits.events.ActivityHome;
import com.pcits.events.R;
import com.pcits.events.adapters.AdapterCountry;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.ClientInfo;
import com.pcits.events.obj.UserObj;
import com.pcits.events.utils.AccessStorage;
import com.pcits.events.widgets.KenBurnsView;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class ProfileFragment extends Fragment implements
		OnClickListener {

	// Declare view objects
	private EditText txtUsername, txtPassword, txtFirstName, txtLastName,
			txtaddress, txtCity, txtCounty, txtPostcode, txtPhone, txtEmail,
			txtFullName, txtPay;

	private ImageView imgAvatar;
	private TextView txtDob, txtGender, txtCountry, btnSelectImg;
	private TableRow trFullName, trAddress, trCity, trCounty, trCountry,
			trPostcode, trPhone, trPayInfo, trGender;
	private ImageButton btnEdit, btnSave, btnNotsave, btnLogout;
	private Calendar mCal;
	private String strDate;
	private SimpleDateFormat sdf_date;

	private final Pattern EMAIL_PATTERN = Pattern
			.compile("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+");
	// private String valid_email;

	private UserFunctions userFunction = new UserFunctions();
	private int intLengthData;
	public String[] mCountry;
	private JSONObject json;
	// create listApdater
	private ArrayList<HashMap<String, String>> countryItems = new ArrayList<HashMap<String, String>>();
	private AdapterCountry adapterCountry;

	private LinearLayout llMenu, llChangePass;
	private KenBurnsView mHeaderPicture;

	private LinearLayout mLlFirstPage, mLlSecondPage;
	private Button mBtnNext, mBtnPrev;
	private AQuery aq;
	private Bitmap avatarBitmap;
	private String selectedImagePath;
	private boolean memCache = false;
	private boolean fileCache = true;

	private View view;
	// preview profile
	private TextViewRobotoCondensedRegular lbl_fname, lbl_lname, lbl_username,
			lbl_email, lbl_address, lbl_city, lbl_post_code, lbl_county,
			lbl_country, lbl_pay_info,lable_payinfo;
	private Button btn_negative, btn_positive;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.activity_profile, container, false);
		try{
			AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
	
				@Override
				protected Void doInBackground(Void... params) {
					try{
						// Put in code over here that does the network related
						getDataShowCountry();
					} catch (Exception ex) {
						Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		            }
					return null;
				}
	
			};
			task.execute();
			initUI();
			initControl();
			initData();
			mCal = Calendar.getInstance();
			sdf_date = new SimpleDateFormat("yyyy-MM-dd");
			initNotBoringActionBar();
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return view;
	}

	private void initNotBoringActionBar() {
		// mHeader = findViewById(R.id.header);
		// mHeaderLogo = (ImageView) findViewById(R.id.header_logo);
		mHeaderPicture = (KenBurnsView) view.findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void initUI() {
		llChangePass = (LinearLayout) view.findViewById(R.id.llChangePass);
		llMenu = (LinearLayout) view.findViewById(R.id.llMenu);
		txtFirstName = (EditText) view.findViewById(R.id.txtFirstName);
		txtLastName = (EditText) view.findViewById(R.id.txtLastName);
		txtFullName = (EditText) view.findViewById(R.id.txtFullName);
		txtUsername = (EditText) view.findViewById(R.id.txtUser);
		txtPassword = (EditText) view.findViewById(R.id.txtPass);
		txtaddress = (EditText) view.findViewById(R.id.txtAddress);
		txtDob = (TextView) view.findViewById(R.id.txtDob);
		txtCity = (EditText) view.findViewById(R.id.txtCity);
		txtCounty = (EditText) view.findViewById(R.id.txtCounty);
		txtCountry = (TextView) view.findViewById(R.id.txtCountry);
		txtPostcode = (EditText) view.findViewById(R.id.txtPostcode);
		txtPhone = (EditText) view.findViewById(R.id.txtPhone);
		txtEmail = (EditText) view.findViewById(R.id.txtEmail);
		txtGender = (TextView) view.findViewById(R.id.txtGender);
		txtPay = (EditText) view.findViewById(R.id.txtPay);
		imgAvatar = (ImageView) view.findViewById(R.id.imgAvatar);
		btnSelectImg = (TextView) view.findViewById(R.id.btnSelectAvata);
		//
		btnEdit = (ImageButton) view.findViewById(R.id.btnEdit);
		btnSave = (ImageButton) view.findViewById(R.id.btnSave);
		btnNotsave = (ImageButton) view.findViewById(R.id.btnNotSave);
		btnLogout = (ImageButton) view.findViewById(R.id.btnLogout);
		//
		trAddress = (TableRow) view.findViewById(R.id.trAddress);
		trCity = (TableRow) view.findViewById(R.id.trCity);
		trCountry = (TableRow) view.findViewById(R.id.trCountry);
		trCounty = (TableRow) view.findViewById(R.id.trCounty);
		trFullName = (TableRow) view.findViewById(R.id.trFullName);
		trPhone = (TableRow) view.findViewById(R.id.trPhone);
		trPostcode = (TableRow) view.findViewById(R.id.trPostCode);
		trPayInfo = (TableRow) view.findViewById(R.id.trPayInfo);
		trGender = (TableRow) view.findViewById(R.id.trGender);

		mLlFirstPage = (LinearLayout) view.findViewById(R.id.ll_first_page);
		mLlSecondPage = (LinearLayout) view.findViewById(R.id.ll_second_page);
		mBtnNext = (Button) view.findViewById(R.id.btn_next);
		mBtnPrev = (Button) view.findViewById(R.id.btn_prev);

		validateEmail();
	}

	private void initControl() {
		txtDob.setOnClickListener(this);
		btnEdit.setOnClickListener(this);
		btnNotsave.setOnClickListener(this);
		btnSave.setOnClickListener(this);
		btnLogout.setOnClickListener(this);
		txtGender.setOnClickListener(this);
		txtCountry.setOnClickListener(this);
		llMenu.setOnClickListener(this);
		llChangePass.setOnClickListener(this);
		mBtnNext.setOnClickListener(this);
		mBtnPrev.setOnClickListener(this);
		btnSelectImg.setOnClickListener(this);
	}

	private void validateEmail() {
		txtEmail.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				String email = txtEmail.getText().toString();
				 if (validateEmail(email) == false) {
					 txtEmail.setError(Html.fromHtml("<font color='red'>Invalid Email!</font>"));
					 //valid_email = null;
				 } else {
				 	//valid_email = email;
				 }
			}
		});
	}

	private boolean validateEmail(String email) {
		return EMAIL_PATTERN.matcher(email).matches();
	}

	private void initData() {
		aq = new AQuery(getActivity());

		if (GlobalValue.myClient != null) {
			if (Integer.parseInt(GlobalValue.myClient.getType()) == 0) {
				llChangePass.setVisibility(View.VISIBLE);
				aq.id(imgAvatar)
						.image(userFunction.URLAdmin
								+ GlobalValue.myClient.getImage(), memCache,
								fileCache);
			} else {
				llChangePass.setVisibility(View.GONE);
				btnEdit.setVisibility(View.GONE);
				aq.id(imgAvatar).image(GlobalValue.myClient.getImage(),
						memCache, fileCache);
			}

			trPayInfo.setVisibility(View.GONE);
			txtFirstName.setText(GlobalValue.myClient.getFname());
			txtLastName.setText(GlobalValue.myClient.getLname());
			trFullName.setVisibility(View.VISIBLE);
			txtFullName.setText(GlobalValue.myClient.getFullname());
			txtaddress.setText(GlobalValue.myClient.getAddress());
			txtCity.setText(GlobalValue.myClient.getCity());
			txtDob.setText(GlobalValue.myClient.getDob());
			txtEmail.setText(GlobalValue.myClient.getEmail());
			txtCountry.setText(GlobalValue.myClient.getCountry());
			txtCounty.setText(GlobalValue.myClient.getCounty());
			txtGender.setText(GlobalValue.myClient.getGender());
			txtPhone.setText(GlobalValue.myClient.getPhone());
			txtPostcode.setText(GlobalValue.myClient.getPostcode());
			txtUsername.setText(GlobalValue.myClient.getUsername());
		}
		if (GlobalValue.myUser != null) {
			llChangePass.setVisibility(View.GONE);
			txtFirstName.setText(GlobalValue.myUser.getFname());
			txtLastName.setText(GlobalValue.myUser.getLname());
			txtaddress.setText(GlobalValue.myUser.getAddress());
			txtCity.setText(GlobalValue.myUser.getCity());
			txtDob.setText(GlobalValue.myUser.getDob());
			txtEmail.setText(GlobalValue.myUser.getEmail());
			txtPay.setText(GlobalValue.myUser.getPayInfo());
			txtCountry.setText(GlobalValue.myUser.getCountry());
			txtCounty.setText(GlobalValue.myUser.getCounty());
			txtGender.setText("");
			trGender.setVisibility(View.GONE);
			txtPhone.setText(GlobalValue.myUser.getPhone());
			txtPostcode.setText(GlobalValue.myUser.getPostcode());
			txtUsername.setText(GlobalValue.myUser.getUsername());
			aq.id(imgAvatar).image(
					userFunction.URLAdmin + GlobalValue.myUser.getImage(),
					memCache, fileCache);
		}
	}

	// Method to get Data from Server
	private void getDataShowCountry() {

		try {
			// If mActivity equal activityCategory then use API dealByCategory
			json = userFunction.showByCountry();
			if (json != null) {
				JSONArray dataDealsArray = json.getJSONArray("data");
				intLengthData = dataDealsArray.length();
				mCountry = new String[intLengthData];

				for (int i = 0; i < intLengthData; i++) {
					// Store data from server to variable
					JSONObject dealsObject = dataDealsArray.getJSONObject(i);
					HashMap<String, String> country = new HashMap<String, String>();
					mCountry[i] = dealsObject
							.getString(userFunction.key_showcountry_country);

					country.put(userFunction.key_showcountry_country,
							mCountry[i]);

					// Adding HashList to ArrayList
					countryItems.add(country);
				}
			}

		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == llChangePass) {
			Intent i = new Intent(getActivity(), ActivityChangePassword.class);
			startActivity(i);
		} else if (v == llMenu) {
			// ActivityHome.resideMenu.openMenu();
			//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
			Intent intent=new Intent(getActivity(),ActivityHome.class);
			getActivity().startActivity(intent);
			getActivity().finish();

		} else if (v.getId() == R.id.txtDob) {
			OnDateSetListener callBack = new OnDateSetListener() {

				@Override
				public void onDateSet(DatePicker view, int year,
						int monthOfYear, int dayOfMonth) {
					mCal.set(year, monthOfYear, dayOfMonth);
					Date currentDate = mCal.getTime();
					strDate = sdf_date.format(currentDate);
					txtDob.setText(strDate);
				}
			};

			DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
					callBack, mCal.get(Calendar.YEAR),
					mCal.get(Calendar.MONTH), mCal.get(Calendar.DATE));
			datePicker.show();
		} else if (v.getId() == R.id.txtGender) {
			popupMenu();
		} else if (v.getId() == R.id.btnEdit) {
			btnEdit.setVisibility(View.GONE);
			btnNotsave.setVisibility(View.VISIBLE);
			btnSave.setVisibility(View.VISIBLE);
			btnSelectImg.setVisibility(View.VISIBLE);
			//
			txtaddress.setEnabled(true);
			txtFirstName.setEnabled(true);
			txtLastName.setEnabled(true);
			txtCity.setEnabled(true);
			txtCountry.setEnabled(true);
			txtCounty.setEnabled(true);
			txtEmail.setEnabled(true);
			txtPay.setEnabled(true);
			txtDob.setEnabled(true);
			txtPhone.setEnabled(true);
			txtPostcode.setEnabled(true);
			txtGender.setEnabled(true);
		} else if (v.getId() == R.id.btnSave) {
			dialogPreViewProfile();

		} else if (v.getId() == R.id.btnNotSave) {
			btnEdit.setVisibility(View.VISIBLE);
			btnNotsave.setVisibility(View.GONE);
			btnSave.setVisibility(View.GONE);
			btnSelectImg.setVisibility(View.GONE);
			//
			txtaddress.setEnabled(false);
			txtFirstName.setEnabled(false);
			txtLastName.setEnabled(false);
			txtCity.setEnabled(false);
			txtCountry.setEnabled(false);
			txtCounty.setEnabled(false);
			txtEmail.setEnabled(false);
			txtDob.setEnabled(false);
			txtPhone.setEnabled(false);
			txtPostcode.setEnabled(false);
			txtGender.setEnabled(false);
			initData();
		} else if (v.getId() == R.id.btnLogout) {
			logOut();
		} else if (v.getId() == R.id.txtCountry) {
			final Dialog dialog = new Dialog(getActivity());
			dialog.getWindow();
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layout_filter_category);
			final ListView lsvCategory = (ListView) dialog
					.findViewById(R.id.lsvFilterCategory);
			adapterCountry = new AdapterCountry(getActivity(), countryItems);
			lsvCategory.setAdapter(adapterCountry);
			lsvCategory.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					txtCountry.setText(mCountry[position]);
					dialog.dismiss();
				}
			});
			dialog.show();
		} else if (v == mBtnNext) {
			mLlFirstPage.setVisibility(View.GONE);
			mLlSecondPage.setVisibility(View.VISIBLE);
		} else if (v == mBtnPrev) {
			mLlSecondPage.setVisibility(View.GONE);
			mLlFirstPage.setVisibility(View.VISIBLE);
		} else if (v == btnSelectImg) {
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(
					Intent.createChooser(intent, "Complete action using"), 1);
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == getActivity().RESULT_OK) {
			Toast.makeText(getActivity(), "choose image", Toast.LENGTH_SHORT)
					.show();

			try {
				Uri uri = data.getData();
				selectedImagePath = AccessStorage.getPath(getActivity(), uri);
				avatarBitmap = AccessStorage
						.loadPrescaledBitmap(selectedImagePath);
				// avatarBitmap = CommonMethods.getResizedBitmap(avatarBitmap,
				// 200, 200);
				Drawable d = new BitmapDrawable(getResources(), avatarBitmap);
				imgAvatar.setImageDrawable(d);
			} catch(Exception ex){
	            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
	        }
		}
	}

	// preview profile
	private void dialogPreViewProfile() {
		final String firstName = txtFirstName.getText().toString();
		final String lastName = txtLastName.getText().toString();
		final String address = txtaddress.getText().toString();
		final String city = txtCity.getText().toString();
		final String county = txtCounty.getText().toString();
		final String country = txtCountry.getText().toString();
		final String dob = txtDob.getText().toString();
		final String phone = txtPhone.getText().toString();
		final String postcode = txtPostcode.getText().toString();
		final String gender = txtGender.getText().toString();
		final String email = txtEmail.getText().toString();
		final String payinfo = txtPay.getText().toString();

		final Dialog dialog = new Dialog(getActivity());
		dialog.getWindow();
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_preview_profile);
		lbl_fname = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_fname);
		lbl_fname.setText(firstName);
		lbl_lname = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_lname);
		lbl_lname.setText(lastName);
		lbl_username = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_username);
		if (GlobalValue.myClient != null) {
			lbl_username.setText(GlobalValue.myClient.getUsername());
		} else if (GlobalValue.myUser != null) {
			lbl_username.setText(GlobalValue.myUser.getUsername());
		}
		lbl_email = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_email);
		lbl_email.setText(email);
		lbl_address = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_address);
		lbl_address.setText(address);
		lbl_city = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_city);
		lbl_city.setText(city);
		lbl_post_code = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_post_code);
		lbl_post_code.setText(postcode);
		lbl_county = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_county);
		lbl_county.setText(county);
		lbl_country = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_country);
		lbl_country.setText(country);
		lable_payinfo = (TextViewRobotoCondensedRegular) dialog.findViewById(R.id.lable_payinfo);
		lbl_pay_info = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_pay_info);
		if (GlobalValue.myClient != null) {
			lable_payinfo.setText(getResources().getString(R.string.gender));
			lbl_pay_info.setText(gender);
		} else if (GlobalValue.myUser != null) {
			lable_payinfo.setText(getResources().getString(R.string.pay_info));
			lbl_pay_info.setText(payinfo);
		}
		btn_negative = (Button) dialog.findViewById(R.id.btn_negative);
		btn_positive = (Button) dialog.findViewById(R.id.btn_positive);
		
		btn_positive.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (GlobalValue.myClient != null) {
					updateAccount();
				} else if (GlobalValue.myUser != null) {
					updateUser();
				}
			}
		});
		btn_negative.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	// Log out confirm
	private void logOut() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Log out");
		builder.setMessage("Are you sure?");
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				logoutProcess();
			}

			// Log out process.
			private void logoutProcess() {

				try {
					LoginManager.getInstance().logOut();
				}catch (Exception ex){

				}
				// Reset account.
				GlobalValue.myClient = null;
				// GlobalValue.myFbUser = null;
				GlobalValue.myUser = null;
				// find the active session which can only be facebook in my app
//				Session session = Session.getActiveSession();
//				// run the closeAndClearTokenInformation which does the
//				// cache related to the Session.
//				session.closeAndClearTokenInformation();
				// Log out.
				Intent i = new Intent(getActivity(), ActivityHome.class);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
			}
		});
		builder.create().show();
	}

	@SuppressLint("NewApi")
	private void popupMenu() {
		// TODO Auto-generated method stub
		PopupMenu popup = new PopupMenu(getActivity(), txtGender);

		popup.getMenuInflater().inflate(R.menu.gender, popup.getMenu());

		popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(android.view.MenuItem item) {
				// TODO Auto-generated method stub
				switch (item.getItemId()) {
				case R.id.male:
					txtGender.setText("Male");
					break;
				case R.id.female:
					txtGender.setText("Female");
					break;
				case R.id.undisclosed:
					txtGender.setText("Undisclosed");
					break;
				}
				return false;
			}
		});
		popup.show();
	}

	private void updateAccount() {
		final String firstName = txtFirstName.getText().toString();
		final String lastName = txtLastName.getText().toString();
		final String address = txtaddress.getText().toString();
		final String city = txtCity.getText().toString();
		final String county = txtCounty.getText().toString();
		final String country = txtCountry.getText().toString();
		final String dob = txtDob.getText().toString();
		final String phone = txtPhone.getText().toString();
		final String postcode = txtPostcode.getText().toString();
		final String gender = txtGender.getText().toString();
		final String email = txtEmail.getText().toString();

		// Validate input.
		if (firstName.isEmpty()) {
			Toast.makeText(getActivity(), "Please, input first name",
					Toast.LENGTH_SHORT).show();
			txtFirstName.requestFocus();
			return;
		}
		if (lastName.isEmpty()) {
			Toast.makeText(getActivity(), "Please, input last name",
					Toast.LENGTH_SHORT).show();
			txtLastName.requestFocus();
			return;
		}
		if (email.isEmpty()) {
			Toast.makeText(getActivity(), "Please, input email",
					Toast.LENGTH_SHORT).show();
			txtEmail.requestFocus();
			return;
		}

		// Create account object.
		ClientInfo account = new ClientInfo();
		account.setUsername(GlobalValue.myClient.getUsername());
		account.setFname(firstName.trim());
		account.setLname(lastName.trim());
		account.setDob(dob.trim());
		account.setGender(gender.trim());
		account.setAddress(address.trim());
		account.setCity(city.trim());
		account.setCounty(county.trim());
		account.setCountry(country.trim());
		account.setPostcode(postcode.trim());
		account.setPhone(phone.trim());
		account.setEmail(email.trim());

		ModelManagerListener modelManagerListener = new ModelManagerListener() {
            @Override
            public void onError() {
                Toast.makeText(getActivity(), "Failed.", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(Object object) {
                String json = (String) object;

                boolean result = ParserUtility.updateAccount(json);

                if (result) {
                    Toast.makeText(getActivity(), "Success.", Toast.LENGTH_LONG).show();
                }
            }
        };

		// Update process.
		ModelManager.updateClient(getActivity(), account, avatarBitmap, true, modelManagerListener);

		btnEdit.setVisibility(View.VISIBLE);
		btnNotsave.setVisibility(View.GONE);
		btnSave.setVisibility(View.GONE);
		btnSelectImg.setVisibility(View.GONE);
		//
		txtaddress.setEnabled(false);
		txtFirstName.setEnabled(false);
		txtLastName.setEnabled(false);
		txtCity.setEnabled(false);
		txtCountry.setEnabled(false);
		txtCounty.setEnabled(false);
		txtEmail.setEnabled(false);
		txtDob.setEnabled(false);
		txtPhone.setEnabled(false);
		txtPostcode.setEnabled(false);
		txtGender.setEnabled(false);
	}

	private void updateUser() {
		final String firstName = txtFirstName.getText().toString();
		final String lastName = txtLastName.getText().toString();
		final String address = txtaddress.getText().toString();
		final String city = txtCity.getText().toString();
		final String county = txtCounty.getText().toString();
		final String country = txtCountry.getText().toString();
		final String dob = txtDob.getText().toString();
		final String phone = txtPhone.getText().toString();
		final String postcode = txtPostcode.getText().toString();
		// final String gender = txtGender.getText().toString();
		final String email = txtEmail.getText().toString();
		final String payinfo = txtPay.getText().toString();

		// Validate input.
		if (firstName.isEmpty()) {
			Toast.makeText(getActivity(), "Please, input first name",
					Toast.LENGTH_SHORT).show();
			txtFirstName.requestFocus();
			return;
		}
		if (lastName.isEmpty()) {
			Toast.makeText(getActivity(), "Please, input last name",
					Toast.LENGTH_SHORT).show();
			txtLastName.requestFocus();
			return;
		}
		if (email.isEmpty()) {
			Toast.makeText(getActivity(), "Please, input email",
					Toast.LENGTH_SHORT).show();
			txtEmail.requestFocus();
			return;
		}
		if (payinfo.isEmpty()) {
			Toast.makeText(getActivity(), "Please, input pay info",
					Toast.LENGTH_SHORT).show();
			txtEmail.requestFocus();
			return;
		}

		// Create account object.
		UserObj user = new UserObj();
		user.setUsername(GlobalValue.myUser.getUsername());
		user.setFname(firstName.trim());
		user.setLname(lastName.trim());
		user.setDob(dob.trim());
		user.setPayInfo(payinfo.trim());
		user.setAddress(address.trim());
		user.setCity(city.trim());
		user.setCounty(county.trim());
		user.setCountry(country.trim());
		user.setPostcode(postcode.trim());
		user.setPhone(phone.trim());
		user.setEmail(email.trim());

		// Update process.
		ModelManager.updateAdmin(getActivity(), user, avatarBitmap, true,
				new ModelManagerListener() {

					@Override
					public void onSuccess(Object object) {
						String json = (String) object;
						boolean result = ParserUtility.updateAccount(json);
						// GlobalValue.myUser = new UserObj();
						// GlobalValue.myUser = ParserUtility.parserUser(json);
						if (result) {
							GlobalValue.myUser.setFname(firstName);
							GlobalValue.myUser.setLname(lastName);
							GlobalValue.myUser.setAddress(address);
							GlobalValue.myUser.setDob(dob);
							GlobalValue.myUser.setCity(city);
							GlobalValue.myUser.setEmail(email);
							GlobalValue.myUser.setPayInfo(payinfo);
							GlobalValue.myUser.setCountry(country);
							GlobalValue.myUser.setCounty(county);
							GlobalValue.myUser.setPhone(phone);
							GlobalValue.myUser.setPostcode(postcode);
						}
					}

					@Override
					public void onError() {
					}
				});

		btnEdit.setVisibility(View.VISIBLE);
		btnNotsave.setVisibility(View.GONE);
		btnSave.setVisibility(View.GONE);
		btnSelectImg.setVisibility(View.GONE);
		//
		txtaddress.setEnabled(false);
		txtFirstName.setEnabled(false);
		txtLastName.setEnabled(false);
		txtCity.setEnabled(false);
		txtCountry.setEnabled(false);
		txtCounty.setEnabled(false);
		txtEmail.setEnabled(false);
		txtDob.setEnabled(false);
		txtPhone.setEnabled(false);
		txtPostcode.setEnabled(false);
		txtGender.setEnabled(false);
		txtPay.setEnabled(false);
	}
}
