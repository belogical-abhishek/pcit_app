package com.pcits.events.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.pcits.events.ActivityHelp;
import com.pcits.events.ActivityHome;
import com.pcits.events.R;
import com.pcits.events.widgets.KenBurnsView;

public class FragmentAbout extends Fragment implements OnClickListener {

	private View view;
	Button btnRetry, btnShare, btnRate;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		view = inflater.inflate(R.layout.activity_about, container, false);
		btnShare = (Button) view.findViewById(R.id.btnShare);
		btnRate = (Button) view.findViewById(R.id.btnRate);
		llMenu = (LinearLayout) view.findViewById(R.id.llMenu);
		llMenu.setOnClickListener(this);

		btnShare.setOnClickListener(this);
		btnRate.setOnClickListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		if (v == llMenu)
		{
			//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
			Intent intent=new Intent(getActivity(),ActivityHome.class);
			getActivity().startActivity(intent);
			getActivity().finish();
		}

		switch (v.getId()) {

		case R.id.btnShare:
			// Share this app to other application
			Intent iShare = new Intent(Intent.ACTION_SEND);
			iShare.setType("text/plain");
			iShare.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
			iShare.putExtra(Intent.EXTRA_TEXT, getString(R.string.gplay_url));
			startActivity(Intent.createChooser(iShare,
					getString(R.string.share_via)));
			break;

		case R.id.btnRate:
			// Rate this app in Play Store
			Intent iRate = new Intent(Intent.ACTION_VIEW);
			iRate.setData(Uri.parse(getString(R.string.gplay_url)));
			startActivity(iRate);
			break;

		default:
			break;
		}
		// TODO Auto-generated method stub

	}
}
