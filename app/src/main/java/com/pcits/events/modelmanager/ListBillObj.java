package com.pcits.events.modelmanager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by EnvisioDevs on 23-11-2017.
 */

public class ListBillObj {

    /*"id": 3,
        "deal_id": "D000000120",
        "description": "Lorem Ipsum",
        "image": "image_path",
        "amount": 300,
        "paid_by": 3,
        "created_by": 2,
        "created_at": "2017-11-10 08:10:22",
        "updated_at": "2017-11-10 08:10:22",
        "split"*/

    @SerializedName("id")
    String id;

    @SerializedName("deal_id")
    String deal_id;

    @SerializedName("description")
    String description;

    @SerializedName("image")
    String image;

    @SerializedName("amount")
    String amount;

    @SerializedName("paid_by")
    String paid_by;

    @SerializedName("created_by")
    String created_by;

    @SerializedName("split")
    ArrayList<Split> split;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeal_id() {
        return deal_id;
    }

    public void setDeal_id(String deal_id) {
        this.deal_id = deal_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setPaid_by(String paid_by) {
        this.paid_by = paid_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getDescription() {

        return description;
    }

    public String getImage() {
        return image;
    }

    public String getPaid_by() {
        return paid_by;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public ArrayList<Split> getSplit() {
        return split;
    }

    public void setSplit(ArrayList<Split> split) {
        this.split = split;
    }
}
