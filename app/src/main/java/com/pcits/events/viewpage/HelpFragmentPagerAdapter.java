package com.pcits.events.viewpage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pcits.events.ActivityHome;
import com.pcits.events.R;

import java.util.List;

public class HelpFragmentPagerAdapter extends PagerAdapter
{
	private Context context;
	private List<String> stringList;
	private LayoutInflater layoutInflater;

	public HelpFragmentPagerAdapter(Context context, List<String> list)
	{
		this.context = context;
		this.stringList = list;
		this.layoutInflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount()
	{
		return stringList.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object)
	{
		return view == ((View)object);
	}


	@Override
	public Object instantiateItem(ViewGroup container, int position)
	{
		View view = layoutInflater.inflate(R.layout.row_layout_viewpager_help, container, false);
		String strObject = stringList.get(position);

		TextView helpinfo = (TextView)view.findViewById(R.id.help_page_info);

		helpinfo.setText(Html.fromHtml(strObject));
		container.addView(view);
		return view;
	}
}