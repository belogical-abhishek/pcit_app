package com.pcits.events.fragments;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.pcits.common.utils.GsonUtility;
import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityDetail;
import com.pcits.events.ActivityHome;
import com.pcits.events.ActivityListPrivateUsers;
import com.pcits.events.AddEventActivity;
import com.pcits.events.ExpandableToolBarActivity;
import com.pcits.events.R;
import com.pcits.events.adapters.HomeAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.libraries.JSONParser;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.libraries.ServiceNotification;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.obj.APIObj;
import com.pcits.events.obj.DealObj;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class FragmentHome extends Fragment implements OnClickListener {

	private static final String TAG = "FragmentHome";
	private ArrayList<DealObj> mArrDeals;

	// Declare object of userFunctions and Utils class
	private UserFunctions userFunction;
	private Utils utils;

	// Create instance of list and ListAdapter
	private ListView list;
	private TextView lblNoResult;
	private HomeAdapter mla;

	private Button btnLoadMore;
	private LinearLayout lytRetry;

	// flag for current page
	private JSONObject json, jsonCurrency;
	private int mCurrentPage = 0;
	private int mPreviousPage;

	private int intLengthData;
	// Declare variable
	public static Activity self;
	private PullToRefreshListView mPullRefreshListView;

	private View v;

	public static LinearLayout llMenu;
	private String client;

	private DealObj mDealObj;

	private int mClickedPos;

	// Declare OnListSelected interface
	public interface OnDataListSelectedListener {
		public void onListSelected(String idSelected);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		self = getActivity();
		v = inflater.inflate(R.layout.fragment_home, container, false);
		try {
			if (mArrDeals == null) {
				mArrDeals = new ArrayList<DealObj>();
			}
			// Clear old data
			mArrDeals.clear();

			// list = (ListView) v.findViewById(R.id.list);
			lblNoResult = (TextView) v.findViewById(R.id.lblNoResult);
			lytRetry = (LinearLayout) v.findViewById(R.id.llRetry);
			// btnRetry = (Button) v.findViewById(R.id.btnRetry);
			llMenu = (LinearLayout) v.findViewById(R.id.llMenu);
			llMenu.setOnClickListener(this);

			// Declare object of userFunctions class
			userFunction = new UserFunctions();
			utils = new Utils(getActivity());

			// Create LoadMore button
			btnLoadMore = new Button(getActivity());
			btnLoadMore
					.setBackgroundResource(R.drawable.apptheme_btn_default_holo_light);
			btnLoadMore.setText(getString(R.string.btn_load_more));
			btnLoadMore.setTextColor(getResources().getColor(R.color.text_btn));

			// Load data
			mPullRefreshListView = (PullToRefreshListView) v
					.findViewById(R.id.lsvPullToRefresh);
			list = mPullRefreshListView.getRefreshableView();
			new loadFirstListView().execute();

			// Set a listener to be invoked when the list should be refreshed.
			if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
				mPullRefreshListView
						.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
							@Override
							public void onRefresh(
									PullToRefreshBase<ListView> refreshView) {

								String label = DateUtils.formatDateTime(
										getActivity(),
										System.currentTimeMillis(),
										DateUtils.FORMAT_SHOW_TIME
												| DateUtils.FORMAT_SHOW_DATE
												| DateUtils.FORMAT_ABBREV_ALL);

								// Update the LastUpdatedLabel
								refreshView.getLoadingLayoutProxy()
										.setLastUpdatedLabel(label);
								new loadFirstListView().execute();

							}
						});
			} else {
				Toast.makeText(getActivity(),
						getString(R.string.no_internet_connection),
						Toast.LENGTH_SHORT).show();
			}

			// Listener to handle load more buttton when clicked
			btnLoadMore.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// Starting a new async task
					if (NetworkUtility.getInstance(getActivity())
							.isNetworkAvailable()) {
						json = null;
						new loadMoreListView().execute();
					} else {
						Toast.makeText(getActivity(),
								getString(R.string.no_internet_connection),
								Toast.LENGTH_SHORT).show();
					}
				}
			});

			// Add an end-of-list listener

			// Listener to get selected id when list item clicked
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {

					if((position-1)<mArrDeals.size()){
					// TODO Auto-generated method stub
					GlobalValue.dealsObj = mArrDeals.get(position - 1);
					DatabaseUtility.insertDeals(getActivity(),
							mArrDeals.get(position - 1));

					ModelManager.updateClickCount(getActivity(),
							mArrDeals.get(position - 1).getDeal_id(), true,
							new ModelManagerListener() {

								@Override
								public void onSuccess(Object object) {
									try {
										// TODO Auto-generated method stub
										String json = (String) object;
										boolean result = ParserUtility
												.updateAccount(json);
										if (result) {
											Log.d(TAG, "Successfully");
										}
									} catch (Exception ex) {
										throw new RuntimeException(ex);
									}
								}

								@Override
								public void onError() {
									// TODO Auto-generated method stub

								}
							});

					// }
					// Pass id to onListSelected method on HomeActivity
					// mCallback.onListSelected(item.get(userFunction.key_deals_id));
			//			addUsers(mArrDeals.get(position - 1).getDeal_id());
					Intent i = new Intent(getActivity(), ActivityDetail.class);
					//Intent i = new Intent(getActivity(), ExpandableToolBarActivity.class);
					// i.putExtra(utils.EXTRA_DEAL_ID, mArrDeals.get(position -
					// 1)
					// .getDeal_id());
					startActivity(i);
					getActivity().overridePendingTransition(
							R.anim.slide_in_left, R.anim.slide_out_left);

					// Set the item as checked to be highlighted when in
					// two-pane
					// layout
					list.setItemChecked(position, true);
				}
				}
			});

			list.setOnItemLongClickListener(new OnItemLongClickListener() {

				@Override
				public boolean onItemLongClick(AdapterView<?> parent,
						View view, int position, long id) {
					// TODO Auto-generated method stub
					if (GlobalValue.myClient != null
							|| GlobalValue.myUser != null) {
						try {
							mDealObj = (DealObj) mArrDeals.get(position - 1)
									.clone();
						} catch (Exception ex) {
							// TODO Auto-generated catch block
							Logging.writeExceptionFromStackTrace(ex,
									ex.getMessage());
						}
						showOperationDialog();

						// Get clicked position for update event which just
						// updated.
						mClickedPos = position - 1;
					}
					return true;
				}
			});

			// initNotBoringActionBar();

			return v;
		} catch (Exception ex) {
			if (Logging.debugFile.exists()) {
				ex.printStackTrace();
			}
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			return v;
		}
	}


	private void addUsers(String id) {
		Log.d("privateList", "addUsers: ");
		Intent intent = new Intent(getContext(), ActivityListPrivateUsers.class);
		intent.putExtra("functionid", 1);
		intent.putExtra("dealid", id);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getContext().startActivity(intent);
	}

	private void showOperationDialog() {
		final Dialog dialog = new Dialog(self);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_operation_event);
		dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
				WindowManager.LayoutParams.WRAP_CONTENT);

		TextViewRobotoCondensedRegular copy = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_copy);
		TextViewRobotoCondensedRegular delete = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_delete);
		TextViewRobotoCondensedRegular revoke = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_revoke);
		TextViewRobotoCondensedRegular scan = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_scan);
		TextViewRobotoCondensedRegular edit = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_edit);

		// Show/hide operation
		if (GlobalValue.myUser == null) {
			if (!GlobalValue.myClient.getUsername().equals(
					mDealObj.getUsername())) {
				delete.setVisibility(View.GONE);
				revoke.setVisibility(View.GONE);
				scan.setVisibility(View.GONE);
				edit.setVisibility(View.GONE);
			}
		}

		// Set listener
		copy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Open create event page with event copied
				AddEventActivity.isDeal = true;
				mDealObj.setDeal_id(null);
                try {
                    mDealObj.setStart_date(""+DateTimeUtility.getDateFormat(mDealObj.getStart_date(),"dd MMM yy","yyyy-MM-dd"));
                    mDealObj.setEnd_date(""+DateTimeUtility.getDateFormat(mDealObj.getEnd_date(),"dd MMM yy","yyyy-MM-dd"));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
				GlobalValue.dealsObj = mDealObj;
				Intent i = new Intent(getActivity(), AddEventActivity.class);
				startActivity(i);

				// Dismiss the dialog
				dialog.dismiss();
			}
		});

		delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(self, "Deleted", Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			}
		});

		revoke.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(self, "Revoked", Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			}
		});

		scan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(self, "Scanned", Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			}
		});

		edit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Open edit event page
				// Intent i = new Intent(self, ActivityEditDeal.class);
				AddEventActivity.isDeal = true;
				try {
					mDealObj.setStart_date(""+DateTimeUtility.getDateFormat(mDealObj.getStart_date(),"dd MMM yy","yyyy-MM-dd"));
					mDealObj.setEnd_date(""+DateTimeUtility.getDateFormat(mDealObj.getEnd_date(),"dd MMM yy","yyyy-MM-dd"));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				GlobalValue.dealsObj = mDealObj;


				Log.d(TAG, "onClick: "+ GsonUtility.convertObjectToJSONString(GlobalValue.dealsObj));
				Intent i = new Intent(getActivity(), AddEventActivity.class);
				startActivity(i);
				// Dismiss dialog
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		mDealObj = null;

		// Refresh auto when update or create new event.
		if (mla != null) {
			if (AddEventActivity.isUpdated) {
				if (AddEventActivity.dealObj != null) {
					// Replace event which just updated.
					mArrDeals.add(mClickedPos, AddEventActivity.dealObj);
					AddEventActivity.dealObj = null;

					// Refresh view
					mla.notifyDataSetChanged();
				}

				AddEventActivity.isUpdated = false;
			} else if (AddEventActivity.isCreated) {
				if (AddEventActivity.dealObj != null) {
					// Create temp array.
					ArrayList<DealObj> arrTemp = new ArrayList<DealObj>();
					arrTemp.addAll(mArrDeals);

					// Clear old data
					mArrDeals.clear();

					// Add new event which just created to first item.
					mArrDeals.add(AddEventActivity.dealObj);

					// Append the old data
					mArrDeals.addAll(arrTemp);

					// Clear temp data
					arrTemp.clear();

					// Refresh view
					mla.notifyDataSetChanged();

					// Scroll on top
					list.smoothScrollToPositionFromTop(0, 0);

					AddEventActivity.dealObj = null;
				}

				AddEventActivity.isCreated = false;
			}
		}
	}

	// Load first 10 videos
	private class loadFirstListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Clear old data
			mArrDeals.clear();
			mCurrentPage = 0;

			ActivityHome.rltProgress.setVisibility(View.VISIBLE);
		}

		protected Void doInBackground(Void... unused) {
			try {
				if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
					getDataFromServer();
				} else {
					getDataFromDB();
				}
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			}
			return null;
		}

		protected void onPostExecute(Void unused) {
			// Condition if data length uder 10 button loadMore is remove
			list.removeFooterView(btnLoadMore);
			if (intLengthData < UserFunctions.valueItemsPerPage) {
				list.removeFooterView(btnLoadMore);
			} else {
				list.addFooterView(btnLoadMore);
			}
			if (isAdded()) {
				if (mArrDeals.size() != 0) {
					// Check parameter notify
					int paramNotif = utils
							.loadPreferences(utils.UTILS_PARAM_NOTIF);

					// Condition if app start in the first time notif will run
					// in background
					if (paramNotif != 1) {
						utils.saveString(utils.UTILS_NOTIF, mArrDeals.get(0)
								.getDeal_id());
						utils.savePreferences(utils.UTILS_PARAM_NOTIF, 1);
						startService();
					}

					// Adding load more button to lisview at bottom
					lytRetry.setVisibility(View.GONE);
					list.setVisibility(View.VISIBLE);
					lblNoResult.setVisibility(View.GONE);
					// Getting adapter
					mla = new HomeAdapter(getActivity(), mArrDeals);
					mla.notifyDataSetChanged();
					list.setAdapter(mla);
					mPullRefreshListView.onRefreshComplete();
					if (mla == null) {
						mla = new HomeAdapter(getActivity(), mArrDeals);
						list.setAdapter(mla);
					} else {
						mla.notifyDataSetChanged();
					}

				} else {
					list.removeFooterView(btnLoadMore);
					if (json != null) {
						lblNoResult.setVisibility(View.VISIBLE);
						lytRetry.setVisibility(View.GONE);

					} else {
						lblNoResult.setVisibility(View.GONE);
						lytRetry.setVisibility(View.VISIBLE);
						Toast.makeText(getActivity(),
								getString(R.string.no_connection),
								Toast.LENGTH_SHORT).show();
					}
				}
			}
			try {
				super.onPostExecute(unused);
				mPullRefreshListView.onRefreshComplete();
				// Closing progress dialog
				ActivityHome.rltProgress.setVisibility(View.GONE);
			} catch (NullPointerException ex) {
				if (Logging.debugFile.exists()) {
					ex.printStackTrace();
				}
				throw new RuntimeException(ex);
			} catch (Exception ex) {
				if (Logging.debugFile.exists()) {
					ex.printStackTrace();
				}
				throw new RuntimeException(ex);
			}
		}
	}

	// Load more videos
	private class loadMoreListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			ActivityHome.rltProgress.setVisibility(View.VISIBLE);
		}

		protected Void doInBackground(Void... unused) {
			try {
				// Store previous value of current page
				mPreviousPage = mCurrentPage;
				// Increment current page
				mCurrentPage += UserFunctions.valueItemsPerPage;
				if (NetworkUtility.getInstance(getActivity())
						.isNetworkAvailable()) {
					getDataFromServer();
				} else {
					getDataFromDB();
				}
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			}
			return (null);
		}

		protected void onPostExecute(Void unused) {
			// Condition if data length under 10 button loadMore is remove
			if ((intLengthData % UserFunctions.valueItemsPerPage) != 0
					|| intLengthData >= GlobalValue.MAX_EVENT) {
				list.removeFooterView(btnLoadMore);
			}
			if (json != null) {
				// Get listview current position - used to maintain scroll
				// position
				int currentPosition = list.getFirstVisiblePosition();
				lytRetry.setVisibility(View.GONE);
				// Appending new data to menuItems ArrayList
				mla.notifyDataSetChanged();
				// Setting new scroll position
				list.setSelectionFromTop(currentPosition + 1, 0);

			} else {
				if (mArrDeals != null) {
					mCurrentPage = mPreviousPage;
					lytRetry.setVisibility(View.GONE);
				} else {
					lytRetry.setVisibility(View.VISIBLE);
					Toast.makeText(getActivity(),
							getString(R.string.no_connection),
							Toast.LENGTH_SHORT).show();
				}

			}
			ActivityHome.rltProgress.setVisibility(View.GONE);
		}
	}

	// Method to get Data from Server
	public void getDataFromServer() {

		try {
			if (GlobalValue.myClient != null) {
				client = GlobalValue.myClient.getUsername();
			} else {
				client = "";
			}
			// jsonCurrency = userFunction.currency(getActivity());
			json = userFunction
					.latestDeals(client, mCurrentPage, getActivity());

			ArrayList<DealObj> arr = JSONParser.parserDeal("latestDeals",
					json.toString());
			mArrDeals.addAll(arr);
			intLengthData = mArrDeals.size();
			// if (jsonCurrency != null) {
			// JSONArray currencyArray = jsonCurrency
			// .getJSONArray(userFunction.array_currency);
			// JSONObject currencyObject = currencyArray.getJSONObject(0);
			// Utils.mCurrency = " "
			// + currencyObject
			// .getString(userFunction.key_currency_code);
			//
			// }

		} catch (NullPointerException ex) {
			if (Logging.debugFile.exists()) {
				ex.printStackTrace();
			}
			throw new RuntimeException(ex);
		} catch (Exception ex) {
			if (Logging.debugFile.exists()) {
				ex.printStackTrace();
			}
			throw new RuntimeException(ex);
		}
	}

	public void getDataFromDB() {
		try {
			String apiDeals = UserFunctions.URL_ALL_DEALS + mCurrentPage;
			APIObj allDealInfos = DatabaseUtility.getResuftFromApi(
					getActivity(), apiDeals);
			String jsonAllDeal = "";
			if (allDealInfos != null) {
				jsonAllDeal = allDealInfos.getmResult();
			}
			ArrayList<DealObj> arr = JSONParser.parserDeal("latestDeals",
					jsonAllDeal.toString());
			mArrDeals.addAll(arr);
			intLengthData = mArrDeals.size();

			// APIObj currencyInfos = DatabaseUtility.getResuftFromApi(
			// getActivity(), UserFunctions.URL_CURRENCY);
			// String jsonCurr = "";
			// if (currencyInfos != null) {
			// jsonCurr = currencyInfos.getmResult();
			// }
			// jsonCurrency = new JSONObject(jsonCurr);
			//
			// if (jsonCurrency != null) {
			// JSONArray currencyArray = jsonCurrency
			// .getJSONArray(userFunction.array_currency);
			// JSONObject currencyObject = currencyArray.getJSONObject(0);
			// Utils.mCurrency = " "
			// + currencyObject
			// .getString(userFunction.key_currency_code);
			// }

		} catch (NullPointerException ex) {
			if (Logging.debugFile.exists()) {
				ex.printStackTrace();
			}
			throw new RuntimeException(ex);
		} catch (Exception ex) {
			if (Logging.debugFile.exists()) {
				ex.printStackTrace();
			}
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		// list.setAdapter(null);
		super.onDestroy();

	}

	public void startService() {

		Intent myIntent = new Intent(getActivity(), ServiceNotification.class);

		PendingIntent pendingIntent = PendingIntent.getService(getActivity(),
				0, myIntent, 0);

		AlarmManager alarmManager = (AlarmManager) getActivity()
				.getSystemService(Context.ALARM_SERVICE);

		Calendar calendar = Calendar.getInstance();
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				calendar.getTimeInMillis(), 30 * 1000, pendingIntent);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == llMenu) {
			//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
			Intent intent=new Intent(getActivity(),ActivityHome.class);
			getActivity().startActivity(intent);
			getActivity().finish();
		}
	}

}