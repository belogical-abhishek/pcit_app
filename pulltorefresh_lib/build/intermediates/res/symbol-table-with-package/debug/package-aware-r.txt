com.handmark.pulltorefresh.library
int anim slide_in_from_bottom 0x7f010001
int anim slide_in_from_top 0x7f010002
int anim slide_out_to_bottom 0x7f010003
int anim slide_out_to_top 0x7f010004
int array City_list 0x7f030001
int array price_list 0x7f030002
int attr ptrAdapterViewBackground 0x7f040001
int attr ptrAnimationStyle 0x7f040002
int attr ptrDrawable 0x7f040003
int attr ptrDrawableBottom 0x7f040004
int attr ptrDrawableEnd 0x7f040005
int attr ptrDrawableStart 0x7f040006
int attr ptrDrawableTop 0x7f040007
int attr ptrHeaderBackground 0x7f040008
int attr ptrHeaderSubTextColor 0x7f040009
int attr ptrHeaderTextAppearance 0x7f04000a
int attr ptrHeaderTextColor 0x7f04000b
int attr ptrListViewExtrasEnabled 0x7f04000c
int attr ptrMode 0x7f04000d
int attr ptrOverScroll 0x7f04000e
int attr ptrRefreshableViewBackground 0x7f04000f
int attr ptrRotateDrawableWhilePulling 0x7f040010
int attr ptrScrollingWhileRefreshingEnabled 0x7f040011
int attr ptrShowIndicator 0x7f040012
int attr ptrSubHeaderTextAppearance 0x7f040013
int dimen header_footer_left_right_padding 0x7f080001
int dimen header_footer_top_bottom_padding 0x7f080002
int dimen indicator_corner_radius 0x7f080003
int dimen indicator_internal_padding 0x7f080004
int dimen indicator_right_padding 0x7f080005
int drawable default_ptr_flip 0x7f090001
int drawable default_ptr_rotate 0x7f090002
int drawable indicator_arrow 0x7f090003
int drawable indicator_bg_bottom 0x7f090004
int drawable indicator_bg_top 0x7f090005
int id fl_inner 0x7f0c0001
int id gridview 0x7f0c0002
int id pull_to_refresh_image 0x7f0c0003
int id pull_to_refresh_progress 0x7f0c0004
int id pull_to_refresh_sub_text 0x7f0c0005
int id pull_to_refresh_text 0x7f0c0006
int id scrollview 0x7f0c0007
int id webview 0x7f0c0008
int layout pull_to_refresh_header_horizontal 0x7f0f0001
int layout pull_to_refresh_header_vertical 0x7f0f0002
int string connection_error_code 0x7f150001
int string connection_error_disabled 0x7f150002
int string connection_error_internal 0x7f150003
int string connection_error_invalid 0x7f150004
int string connection_error_invalid_account 0x7f150005
int string connection_error_license_check_failed 0x7f150006
int string connection_error_message 0x7f150007
int string connection_error_misconfigured 0x7f150008
int string connection_error_missing 0x7f150009
int string connection_error_needs_resolution 0x7f15000a
int string connection_error_network 0x7f15000b
int string connection_error_outdated 0x7f15000c
int string connection_error_sign_in_required 0x7f15000d
int string connection_error_unknown 0x7f15000e
int string define_circularprogressbutton 0x7f15000f
int string library_circularprogressbutton_author 0x7f150010
int string library_circularprogressbutton_authorWebsite 0x7f150011
int string library_circularprogressbutton_isOpenSource 0x7f150012
int string library_circularprogressbutton_libraryDescription 0x7f150013
int string library_circularprogressbutton_libraryName 0x7f150014
int string library_circularprogressbutton_libraryVersion 0x7f150015
int string library_circularprogressbutton_libraryWebsite 0x7f150016
int string library_circularprogressbutton_licenseId 0x7f150017
int string library_circularprogressbutton_repositoryLink 0x7f150018
int string menu_browser 0x7f150019
int string menu_next 0x7f15001a
int string menu_previous 0x7f15001b
int string menu_refresh 0x7f15001c
int string menu_stop 0x7f15001d
int string pull_to_refresh_from_bottom_pull_label 0x7f15001e
int string pull_to_refresh_from_bottom_refreshing_label 0x7f15001f
int string pull_to_refresh_from_bottom_release_label 0x7f150020
int string pull_to_refresh_pull_label 0x7f150021
int string pull_to_refresh_refreshing_label 0x7f150022
int string pull_to_refresh_release_label 0x7f150023
int[] styleable PullToRefresh { 0x7f040001, 0x7f040002, 0x7f040003, 0x7f040004, 0x7f040005, 0x7f040006, 0x7f040007, 0x7f040008, 0x7f040009, 0x7f04000a, 0x7f04000b, 0x7f04000c, 0x7f04000d, 0x7f04000e, 0x7f04000f, 0x7f040010, 0x7f040011, 0x7f040012, 0x7f040013 }
int styleable PullToRefresh_ptrAdapterViewBackground 0
int styleable PullToRefresh_ptrAnimationStyle 1
int styleable PullToRefresh_ptrDrawable 2
int styleable PullToRefresh_ptrDrawableBottom 3
int styleable PullToRefresh_ptrDrawableEnd 4
int styleable PullToRefresh_ptrDrawableStart 5
int styleable PullToRefresh_ptrDrawableTop 6
int styleable PullToRefresh_ptrHeaderBackground 7
int styleable PullToRefresh_ptrHeaderSubTextColor 8
int styleable PullToRefresh_ptrHeaderTextAppearance 9
int styleable PullToRefresh_ptrHeaderTextColor 10
int styleable PullToRefresh_ptrListViewExtrasEnabled 11
int styleable PullToRefresh_ptrMode 12
int styleable PullToRefresh_ptrOverScroll 13
int styleable PullToRefresh_ptrRefreshableViewBackground 14
int styleable PullToRefresh_ptrRotateDrawableWhilePulling 15
int styleable PullToRefresh_ptrScrollingWhileRefreshingEnabled 16
int styleable PullToRefresh_ptrShowIndicator 17
int styleable PullToRefresh_ptrSubHeaderTextAppearance 18
