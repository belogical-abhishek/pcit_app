/*
 * Name: $RCSfile: DatabaseUtility.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Oct 31, 2011 2:55:54 PM $
 *
 * Copyright (C) 2011 COMPANY_NAME, Inc. All rights reserved.
 */

package com.pcits.events.database;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.pcits.events.database.binder.AdminBinder;
import com.pcits.events.database.binder.ApiBinder;
import com.pcits.events.database.binder.ClientBinder;
import com.pcits.events.database.binder.DealsBinder;
import com.pcits.events.database.binder.NotificationBinder;
import com.pcits.events.database.binder.PaidticketBinder;
import com.pcits.events.database.mapper.AdminMapper;
import com.pcits.events.database.mapper.ApiMapper;
import com.pcits.events.database.mapper.ClientMapper;
import com.pcits.events.database.mapper.DealsMapper;
import com.pcits.events.database.mapper.NotificationMapper;
import com.pcits.events.database.mapper.PaidticketMapper;
import com.pcits.events.obj.APIObj;
import com.pcits.events.obj.ClientInfo;
import com.pcits.events.obj.DealObj;
import com.pcits.events.obj.NotifyBoxObj;
import com.pcits.events.obj.PaidticketsObj;
import com.pcits.events.obj.UserObj;

public final class DatabaseUtility {

	private static final String TAG = "DatabaseUtility";

	public static String STRING_SQL_INSERT_INTO_API = "INSERT OR IGNORE INTO Api("
			+ DBKeyConfig.KEY_API_API
			+ ","
			+ DBKeyConfig.KEY_API_RESULT
			+ ")VALUES(?,?)";
	// insert history deal
	private static String STRING_SQL_INSERT_INTO_DEALS = "INSERT OR IGNORE INTO "
			+ DBKeyConfig.TABLE_DEALS + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	// insert paidticket
	private static String STRING_SQL_INSERT_INTO_PAIDTICKET = "INSERT OR IGNORE INTO "
			+ DBKeyConfig.TABLE_PAIDTICKET + " VALUES(?,?,?,?,?,?,?,?,?,?)";
	// insert admin
	private static String STRING_SQL_INSERT_INTO_ADMIN = "INSERT OR IGNORE INTO "
			+ DBKeyConfig.TABLE_ADMIN
			+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	// insert client
	private static String STRING_SQL_INSERT_INTO_CLIENT = "INSERT OR IGNORE INTO "
			+ DBKeyConfig.TABLE_CLIENT
			+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	// Insert into tbl_notification table
	public static String STRING_SQL_INSERT_INTO_NOTIFICATION = "INSERT OR IGNORE INTO "
			+ DBKeyConfig.TABLE_NOTIFICATION
			+ "("
			+ DBKeyConfig.KEY_NOTIFICATION_TYPE_ID
			+ ","
			+ DBKeyConfig.KEY_NOTIFICATION_SUMMARY
			+ ","
			+ DBKeyConfig.KEY_NOTIFICATION_DATE
			+ ","
			+ DBKeyConfig.KEY_NOTIFICATION_STATUS_ID
			+ ","
			+ DBKeyConfig.KEY_NOTIFICATION_TITLE + ")VALUES(?,?,?,?,?)";
	public static String STRING_SQL_DELETE_FROM_NOTIFICATION = "Delete from "
			+ DBKeyConfig.TABLE_NOTIFICATION + " where "
			+ DBKeyConfig.KEY_NOTIFICATION_NOTIFY_ID + " like '";

	// ************************* TABLE API ************************************

	// add
	public static boolean insertApi(Context context, APIObj apiInfo) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.insert(STRING_SQL_INSERT_INTO_API, apiInfo,
				new ApiBinder());
	}

	// add admin
	public static boolean insertAdmin(Context context, UserObj adminObj) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.insert(STRING_SQL_INSERT_INTO_ADMIN, adminObj,
				new AdminBinder());
	}

	// login admin by username
	public static UserObj getAdminByUser(Context context, String username,
			String pass) {
		PrepareStatement statement = new PrepareStatement(context);
		List<UserObj> arr = statement.select(DBKeyConfig.TABLE_ADMIN, "*",
				"username=" + "'" + username + "'" + " and " + "password="
						+ "'" + pass + "'", new AdminMapper());
		return arr.size() > 0 ? arr.get(0) : null;
	}

	// add client
	public static boolean insertClient(Context context, ClientInfo clientObj) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.insert(STRING_SQL_INSERT_INTO_CLIENT, clientObj,
				new ClientBinder());
	}

	// login client by username
	public static ClientInfo getClient(Context context, String username,
			String pass) {
		PrepareStatement statement = new PrepareStatement(context);
		List<ClientInfo> arr = statement.select(DBKeyConfig.TABLE_CLIENT, "*",
				"username=" + "'" + username + "'" + " and " + "password="
						+ "'" + pass + "'", new ClientMapper());
		return arr.size() > 0 ? arr.get(0) : null;
	}

	// add paidtickets
	public static boolean insertPaidticket(Context context,
			PaidticketsObj paidObj) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.insert(STRING_SQL_INSERT_INTO_PAIDTICKET, paidObj,
				new PaidticketBinder());
	}

	// list paidtickets
	public static ArrayList<PaidticketsObj> getListPaidtickets(Context context) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.select(DBKeyConfig.TABLE_PAIDTICKET, "*", "",
				new PaidticketMapper());
	}

	// add deals history
	public static boolean insertDeals(Context context, DealObj dealObj) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.insert(STRING_SQL_INSERT_INTO_DEALS, dealObj,
				new DealsBinder());
	}

	// check exist deals
	public static boolean checkExistsDeals(Context context, String dealId) {
		PrepareStatement statement = new PrepareStatement(context);
		ArrayList<DealObj> dealInfo = new ArrayList<DealObj>();
		dealInfo = statement.select(DBKeyConfig.TABLE_DEALS, "*", "deal_id ="
				+ "'" + dealId + "'", new DealsMapper());
		if (dealInfo.size() > 0)
			return true;
		else {
			return false;
		}
	}

	// list history
	public static ArrayList<DealObj> getListDeals(Context context) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.select(DBKeyConfig.TABLE_DEALS + " limit 10", "*", "",
				new DealsMapper());
	}

	// clear history
	public static boolean clearHistory(Context context) {
		try{
			PrepareStatement statement = new PrepareStatement(context);
			return statement.query("Delete from " + DBKeyConfig.TABLE_DEALS, null);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	// search
	public static boolean checkExistsApi(Context context, String API) {
		PrepareStatement statement = new PrepareStatement(context);
		ArrayList<APIObj> apiInfos = new ArrayList<APIObj>();
		apiInfos = statement.select(DBKeyConfig.TABLE_API, "*",
				DBKeyConfig.KEY_API_API + " like '" + API + "'",
				new ApiMapper());
		if (apiInfos.size() > 0)
			return true;
		else {
			return false;
		}
	}

	public static APIObj getResuftFromApi(Context context, String API) {
		PrepareStatement statement = new PrepareStatement(context);
		ArrayList<APIObj> apiInfos = statement.select(DBKeyConfig.TABLE_API,
				"*", DBKeyConfig.KEY_API_API + " like '" + API + "'",
				new ApiMapper());
		Log.e("SIZE", "size " + apiInfos.size());
		if (apiInfos.size() > 0) {

			return apiInfos.get(0);

		} else {
			return null;
		}
	}

	public static boolean updateResuft_Api(Context context, String result,
			String api) {
		try{
			char c = '\'';
			for (int i = 0; i < result.length(); i++) {
				if ((result.charAt(i) + "").equals(c + "")) {
					result = result.replace(c + "", "N\u00b4");
				}
			}
	
			PrepareStatement statement = new PrepareStatement(context);
			return statement.update(DBKeyConfig.TABLE_API, "result='" + result
					+ "'", DBKeyConfig.KEY_API_API + "='" + api + "'");
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	/* tbl_notification queries */
	public static boolean insertNotification(Context context,
			NotifyBoxObj notify) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.insert(STRING_SQL_INSERT_INTO_NOTIFICATION, notify,
				new NotificationBinder());
	}

	public static ArrayList<NotifyBoxObj> selectNotification(Context context) {
		PrepareStatement statement = new PrepareStatement(context);
		ArrayList<NotifyBoxObj> arrNotify = statement.select(
				DBKeyConfig.TABLE_NOTIFICATION, "*", "0=0",
				new NotificationMapper());
		Log.e(TAG, "Notification size " + arrNotify.size());
		if (arrNotify.size() > 0) {
			return arrNotify;
		} else {
			return new ArrayList<NotifyBoxObj>();
		}
	}

	public static boolean deleteFromNotification(Context context,
			NotifyBoxObj obj) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.delete(
				STRING_SQL_DELETE_FROM_NOTIFICATION + obj.getNotifyId() + "'",
				obj, null);
	}
}
