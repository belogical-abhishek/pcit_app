package com.pcits.events.libraries;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.pcits.events.obj.DealObj;


public class JSONParser {

	public JSONObject getJSONFromUrl(String url) {

		JSONObject jObj = null;

		try {
			HttpClient client = new DefaultHttpClient();
			HttpConnectionParams
					.setConnectionTimeout(client.getParams(), 15000);
			HttpConnectionParams.setSoTimeout(client.getParams(), 15000);
			// Perform a GET request to Server for a JSON list of all the latest
			// place
			HttpUriRequest request = new HttpGet(url);
			// Get the response that Server sends back
			HttpResponse response = client.execute(request);
			// Convert this response into an inputstream for the parser to use
			InputStream atomInputStream = response.getEntity().getContent();

			BufferedReader in = new BufferedReader(new InputStreamReader(
					atomInputStream));

			String line;
			String str = "";
			while ((line = in.readLine()) != null) {
				str += line;
			}


			jObj = new JSONObject(str);

		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}

		// return JSON String
		return jObj;

	}

	public static ArrayList<DealObj> parserDeal(String keyData, String json) {
		ArrayList<DealObj> arrDeal = new ArrayList<DealObj>();
		try {
			JSONObject jsonObj = new JSONObject(json);
			JSONArray arrObj = jsonObj.getJSONArray(keyData);
			for (int i = 0; i < arrObj.length(); i++) {
				JSONObject obj = arrObj.getJSONObject(i);

				int attended = 0, quantity = 0, isHot = 0, viewed = 0;

                try {
                    viewed =  obj.getInt("viewed");
                } catch(Exception e) {
                    e.printStackTrace();
                }

                try {
                    quantity =  obj.getInt("quantity");
                } catch(Exception e) {
                    e.printStackTrace();
                }

                try {
                    isHot =  obj.getInt("isHot");
                } catch(Exception e) {
                    e.printStackTrace();
                }

				try {
					attended =  obj.getInt("attended");
				} catch(Exception e) {
					e.printStackTrace();
				}


				DealObj deal = new DealObj(obj.getString("deal_id"),
						obj.getString("tnc_id"), obj.getString("tnc_desc"),
						obj.getString("title"), obj.getString("company"),
						obj.getString("start_timestamp"),
						obj.getString("end_timestamp"),
						obj.getString("start_date"), obj.getString("end_date"),
						obj.getString("start_time"), obj.getString("end_time"),
						obj.getDouble("after_discount_value"),
						obj.getDouble("start_value"),
						obj.getString("category_id"),
						obj.getString("category_marker"),
						obj.getString("category_name"), obj.getString("image"),
						obj.getString("tnc_notes"), attended,
						quantity, obj.getString("username"),
						obj.getString("address"), obj.getString("city"),
						obj.getString("county"), obj.getString("postcode"),
						obj.getString("country"), obj.getDouble("latitude"),
						obj.getDouble("longitude"), obj.getString("deal_url"),
						obj.getInt("discount"), obj.getDouble("save_value"),
						obj.getString("description"), isHot, viewed);

				arrDeal.add(deal);
			}
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException(e);

		} catch (NumberFormatException ex) {
			ex.printStackTrace();

			throw new RuntimeException(ex);
		} catch (NullPointerException ex) {
			ex.printStackTrace();

			throw new RuntimeException(ex);
		} catch (Exception ex) {
			ex.printStackTrace();

			throw new RuntimeException(ex);
		}
		return arrDeal;
	}
}
