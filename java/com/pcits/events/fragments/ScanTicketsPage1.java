package com.pcits.events.fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityHome;
import com.pcits.events.R;
import com.pcits.events.adapters.HomeAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.libraries.JSONParser;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.obj.APIObj;
import com.pcits.events.obj.DealObj;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.KenBurnsView;

public class ScanTicketsPage1 extends Fragment {

	boolean loadingMore = false;

	private ArrayList<DealObj> mArrDeals;
	private RelativeLayout mRltProg;
	private ListView list;
	private HomeAdapter mla;

	private Button btnLoadMore;
	// Declare object of userFunctions and Utils class
	private UserFunctions userFunction;
	private Utils utils;
	// flag for current page
	private JSONObject json, jsonCurrency;
	private int mCurrentPage = 0;
	private int mPreviousPage;

	// create array variables to store data

	private int intLengthData;

	private TextView lblTitleHeader;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private String client;

	private View v;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		v = inflater.inflate(R.layout.fragment_scan_page1, container, false);
		try{
			if (mArrDeals == null) {
				mArrDeals = new ArrayList<DealObj>();
			}
	
			userFunction = new UserFunctions();
			utils = new Utils(getActivity());
			initUI();
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void initUI() {
		try{
			mRltProg = (RelativeLayout) v.findViewById(R.id.rlt_progress);
			mRltProg.setOnClickListener(null);
			lblTitleHeader = (TextView) v.findViewById(R.id.lblTitleHeader);
			lblTitleHeader.setText("Events");
			llMenu = (LinearLayout) v.findViewById(R.id.llMenu);
			list = (ListView) v.findViewById(R.id.lsvDeals);
	
			llMenu.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// ActivityHome.resideMenu.openMenu();
					//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
					Intent intent=new Intent(getActivity(),ActivityHome.class);
					startActivity(intent);
					getActivity().finish();

				}
			});
			// menuItems = new ArrayList<HashMap<String, String>>();
			// Create LoadMore button
			btnLoadMore = new Button(getActivity());
			btnLoadMore
					.setBackgroundResource(R.drawable.apptheme_btn_default_holo_light);
			btnLoadMore.setText(getString(R.string.btn_load_more));
			btnLoadMore.setTextColor(getResources().getColor(R.color.text_btn));
			mArrDeals = new ArrayList<DealObj>();
			new loadFirstListView().execute();
			// Listener to handle load more buttton when clicked
			btnLoadMore.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					// Starting a new async task
					try{
						json = null;
						new loadMoreListView().execute();
					} catch (Exception ex) {
		                throw new RuntimeException(ex);
		            }
				}
			});
			// Listener to get selected id when list item clicked
			list.setOnItemClickListener(new OnItemClickListener() {
	
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					// HashMap<String, String> item = new HashMap<String, String>();
					// item = menuItems.get(position);
	
					// Pass id to onListSelected method on HomeActivity
					// mCallback.onListSelected(item.get(userFunction.key_deals_id));
					Intent i = new Intent(getActivity(), ScanTicketsPage2.class);
					i.putExtra("id", mArrDeals.get(position).getDeal_id());
					startActivity(i);
					getActivity().overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_left);
	
					// Set the item as checked to be highlighted when in
					// two-pane
					// layout
					list.setItemChecked(position, true);
				}
			});
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	// Load first 10 videos
	private class loadFirstListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Showing progress dialog before sending http request
			mRltProg.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... unused) {
			try{
				if (Integer.parseInt(GlobalValue.myUser.getRole()) == 0) {
					if (NetworkUtility.getInstance(getActivity())
							.isNetworkAvailable()) {
						getDataFromServerAll();
					} else {
						getDataFromDBAll();
					}
				} else {
	
					if (NetworkUtility.getInstance(getActivity())
							.isNetworkAvailable()) {
						getDataFromServer();
					} else {
						getDataFromDBByUser();
					}
				}
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
            }
			return (null);
		}

		@Override
		protected void onPostExecute(Void unused) {
			// Condition if data length uder 10 button loadMore is remove
			if (intLengthData < userFunction.valueItemsPerPage) {
				list.removeFooterView(btnLoadMore);
			} else {
				if (Integer.parseInt(GlobalValue.myUser.getRole()) == 0) {
					list.addFooterView(btnLoadMore);
				}
			}
			if (mArrDeals.size() != 0) {
				// Check paramter notif
				int paramNotif = utils.loadPreferences(utils.UTILS_PARAM_NOTIF);

				// Condition if app start in the first time notif will run
				// in background
				if (paramNotif != 1) {
					utils.saveString(utils.UTILS_NOTIF, mArrDeals.get(0)
							.getDeal_id());
					utils.savePreferences(utils.UTILS_PARAM_NOTIF, 1);

				}

				// Adding load more button to lisview at bottom
				list.setVisibility(View.VISIBLE);
				// Getting adapter
				mla = new HomeAdapter(getActivity(), mArrDeals);
				list.setAdapter(mla);

			} else {
				list.removeFooterView(btnLoadMore);
				if (json != null) {

				} else {
					Toast.makeText(getActivity(),
							getString(R.string.no_connection),
							Toast.LENGTH_SHORT).show();
				}
			}

			// Closing progress dialog
			mRltProg.setVisibility(View.GONE);
		}
	}

	// Load more videos
	private class loadMoreListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Showing progress dialog before sending http request
			mRltProg.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... unused) {
			try{
				// Store previous value of current page
				mPreviousPage = mCurrentPage;
				// Increment current page
				mCurrentPage += userFunction.valueItemsPerPage;
				if (Integer.parseInt(GlobalValue.myUser.getRole()) == 0) {
					if (NetworkUtility.getInstance(getActivity())
							.isNetworkAvailable()) {
						getDataFromServerAll();
					} else {
						getDataFromDBAll();
					}
				} else {
					getDataFromServer();
				}
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
            }
			return (null);
		}

		@Override
		protected void onPostExecute(Void unused) {
			try{
				// Condition if data length uder 10 button loadMore is remove
				if (intLengthData < userFunction.valueItemsPerPage)
					list.removeFooterView(btnLoadMore);
				if (json != null) {
					// Get listview current position - used to maintain scroll
					// position
					int currentPosition = list.getFirstVisiblePosition();
					// Appending new data to menuItems ArrayList
					mla = new HomeAdapter(getActivity(), mArrDeals);
					list.setAdapter(mla);
					// Setting new scroll position
					list.setSelectionFromTop(currentPosition + 1, 0);
	
				} else {
					if (mArrDeals != null) {
						mCurrentPage = mPreviousPage;
					} else {
						Toast.makeText(getActivity(),
								getString(R.string.no_connection),
								Toast.LENGTH_SHORT).show();
					}
	
				}
				// Closing progress dialog
				mRltProg.setVisibility(View.GONE);
			} catch (Exception ex) {
                throw new RuntimeException(ex);
            }
		}
	}

	// Method to get Data from Server
	public void getDataFromServer() {

		try {
			// If mActivity equal activityCategory then use API dealByCategory
			// if (mActivity.equals(utils.EXTRA_ACTIVITY_CATEGORY)) {
			// json = userFunction.dealByCategory(mCategoryId,
			// mCurrentPage);
			// } else {
			String username = GlobalValue.myUser.getUsername();
			jsonCurrency = userFunction.currency(getActivity());
			json = userFunction.dealsByUser(username,mCurrentPage, getActivity());
			// }
			Log.e("url", "url: " + json);
			if (json != null) {
				ArrayList<DealObj> arr = JSONParser.parserDeal(
						userFunction.array_deals_by_user, json.toString());
				mArrDeals.addAll(arr);
				intLengthData = mArrDeals.size();
				// If mActivity equal activityCategory then use array
				// dealByCategory
				// if (mActivity.equals(utils.EXTRA_ACTIVITY_CATEGORY)) {
				// dataDealsArray = json
				// .getJSONArray(userFunction.array_deal_by_category);
				// } else {
				// JSONArray dataDealsArray = json.getJSONArray("dealsByUser");
				// // }
				// intLengthData = dataDealsArray.length();
				// mDealsId = new String[intLengthData];
				// mTitle = new String[intLengthData];
				// mCompany = new String[intLengthData];
				// mDateEnd = new String[intLengthData];
				// mDateStart = new String[intLengthData];
				// mTimeStart = new String[intLengthData];
				// mTimeEnd = new String[intLengthData];
				// mAfterDiscValue = new String[intLengthData];
				// mStartValue = new String[intLengthData];
				// mImg = new String[intLengthData];
				// mIcMarker = new String[intLengthData];
				// mAttend = new String[intLengthData];
				//
				// for (int i = 0; i < intLengthData; i++) {
				// // Store data from server to variable
				// JSONObject dealsObject = dataDealsArray.getJSONObject(i);
				// HashMap<String, String> map = new HashMap<String, String>();
				// mDealsId[i] = dealsObject
				// .getString(userFunction.key_deals_id);
				// mTitle[i] = dealsObject
				// .getString(userFunction.key_deals_title);
				// mCompany[i] = dealsObject
				// .getString(userFunction.key_deals_company);
				// mDateEnd[i] = dealsObject
				// .getString(userFunction.key_deals_date_end);
				// mDateStart[i] = dealsObject
				// .getString(userFunction.key_deals_date_start);
				// mTimeStart[i] = dealsObject
				// .getString(userFunction.key_deals_time_start);
				// mTimeEnd[i] = dealsObject
				// .getString(userFunction.key_deals_time_end);
				// mAfterDiscValue[i] = dealsObject
				// .getString(userFunction.key_deals_after_disc_value);
				// mStartValue[i] = dealsObject
				// .getString(userFunction.key_deals_start_value);
				// mImg[i] = dealsObject
				// .getString(userFunction.key_deals_image);
				// mIcMarker[i] = dealsObject
				// .getString(userFunction.key_category_marker);
				// mAttend[i] = dealsObject
				// .getString(userFunction.key_deals_attended);
				//
				// map.put(userFunction.key_deals_id, mDealsId[i]); // id not
				// // using
				// // any
				// // where
				// map.put(userFunction.key_deals_title, mTitle[i]);
				// map.put(userFunction.key_deals_company, mCompany[i]);
				// map.put(userFunction.key_deals_date_end, mDateEnd[i]);
				// map.put(userFunction.key_deals_date_start, mDateStart[i]);
				// map.put(userFunction.key_deals_time_start, mTimeStart[i]);
				// map.put(userFunction.key_deals_time_end, mTimeEnd[i]);
				// map.put(userFunction.key_deals_after_disc_value,
				// mAfterDiscValue[i]);
				// map.put(userFunction.key_deals_start_value, mStartValue[i]);
				// map.put(userFunction.key_deals_image, mImg[i]);
				// map.put(userFunction.key_category_marker, mIcMarker[i]);
				// map.put(userFunction.key_deals_attended, mAttend[i]);
				//
				// // Adding HashList to ArrayList
				// menuItems.add(map);
				// }
			}
			if (jsonCurrency != null) {
				JSONArray currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);
			}

		} catch (JSONException ex) {
			throw new RuntimeException(ex);
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public void getDataFromDBByUser() {
		try {
			// If mActivity equal activityCategory then use API dealByCategory
			// if (mActivity.equals(utils.EXTRA_ACTIVITY_CATEGORY)) {
			// json = userFunction.dealByCategory(mCategoryId, mCurrentPage);
			// } else {
			String username = GlobalValue.myUser.getUsername();
			String apiDeals = UserFunctions.URL_DEAL_BY_USER + username;
			APIObj allDealInfos = DatabaseUtility.getResuftFromApi(
					getActivity(), apiDeals);
			Log.e("FragmentHome", "Database: " + allDealInfos);
			String jsonAllDeal = "";
			if (allDealInfos != null) {
				jsonAllDeal = allDealInfos.getmResult();
			}
			json = new JSONObject(jsonAllDeal);
			Log.e("url", "All deal: " + jsonAllDeal);

			APIObj currencyInfos = DatabaseUtility.getResuftFromApi(
					getActivity(), UserFunctions.URL_CURRENCY);
			Log.e("FragmentHome", "Database: " + currencyInfos);
			String jsonCurr = "";
			if (currencyInfos != null) {
				jsonCurr = currencyInfos.getmResult();
			}
			jsonCurrency = new JSONObject(jsonCurr);
			Log.e("url", "Currency: " + jsonCurr);

			// }

			if (json != null) {
				ArrayList<DealObj> arr = JSONParser.parserDeal(
						userFunction.array_deals_by_user, json.toString());
				mArrDeals.addAll(arr);
				intLengthData = mArrDeals.size();
				// JSONArray dataDealsArray;
				//
				// // If mActivity equal activityCategory then use array
				// // dealByCategory
				// // if (mActivity.equals(utils.EXTRA_ACTIVITY_CATEGORY)) {
				// // dataDealsArray = json
				// // .getJSONArray(userFunction.array_deal_by_category);
				// // } else {
				// dataDealsArray = json
				// .getJSONArray(userFunction.array_latest_deals);
				// // }
				// intLengthData = dataDealsArray.length();
				// mDealsId = new String[intLengthData];
				// mTitle = new String[intLengthData];
				// mCompany = new String[intLengthData];
				// mDateEnd = new String[intLengthData];
				// mDateStart = new String[intLengthData];
				// mTimeStart = new String[intLengthData];
				// mTimeEnd = new String[intLengthData];
				// mAfterDiscValue = new String[intLengthData];
				// mStartValue = new String[intLengthData];
				// mImg = new String[intLengthData];
				// mIcMarker = new String[intLengthData];
				// mAttend = new String[intLengthData];
				//
				// for (int i = 0; i < intLengthData; i++) {
				// // Store data from server to variable
				// JSONObject dealsObject = dataDealsArray.getJSONObject(i);
				// HashMap<String, String> map = new HashMap<String, String>();
				// mDealsId[i] = dealsObject
				// .getString(userFunction.key_deals_id);
				// mTitle[i] = dealsObject
				// .getString(userFunction.key_deals_title);
				// mCompany[i] = dealsObject
				// .getString(userFunction.key_deals_company);
				// mDateEnd[i] = dealsObject
				// .getString(userFunction.key_deals_date_end);
				// mDateStart[i] = dealsObject
				// .getString(userFunction.key_deals_date_start);
				// mTimeStart[i] = dealsObject
				// .getString(userFunction.key_deals_time_start);
				// mTimeEnd[i] = dealsObject
				// .getString(userFunction.key_deals_time_end);
				// mAfterDiscValue[i] = dealsObject
				// .getString(userFunction.key_deals_after_disc_value);
				// mStartValue[i] = dealsObject
				// .getString(userFunction.key_deals_start_value);
				// mImg[i] = dealsObject
				// .getString(userFunction.key_deals_image);
				// mIcMarker[i] = dealsObject
				// .getString(userFunction.key_category_marker);
				// mAttend[i] = dealsObject
				// .getString(userFunction.key_deals_attended);
				//
				// map.put(userFunction.key_deals_id, mDealsId[i]); // id not
				// // using
				// // any
				// // where
				// map.put(userFunction.key_deals_title, mTitle[i]);
				// map.put(userFunction.key_deals_company, mCompany[i]);
				// map.put(userFunction.key_deals_date_end, mDateEnd[i]);
				// map.put(userFunction.key_deals_date_start, mDateStart[i]);
				// map.put(userFunction.key_deals_time_start, mTimeStart[i]);
				// map.put(userFunction.key_deals_time_end, mTimeEnd[i]);
				// map.put(userFunction.key_deals_after_disc_value,
				// mAfterDiscValue[i]);
				// map.put(userFunction.key_deals_start_value, mStartValue[i]);
				// map.put(userFunction.key_deals_image, mImg[i]);
				// map.put(userFunction.key_category_marker, mIcMarker[i]);
				// map.put(userFunction.key_deals_attended, mAttend[i]);
				//
				// // Adding HashList to ArrayList
				// menuItems.add(map);
				// }
			}
			if (jsonCurrency != null) {
				JSONArray currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);
			}

		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	// Method to get Data from Server
	public void getDataFromServerAll() {

		try {
			// If mActivity equal activityCategory then use API dealByCategory
			// if (mActivity.equals(utils.EXTRA_ACTIVITY_CATEGORY)) {
			// json = userFunction.dealByCategory(mCategoryId, mCurrentPage);
			// } else {
			if (GlobalValue.myClient != null) {
				client = GlobalValue.myClient.getUsername();
			} else {
				client = "";
			}
			jsonCurrency = userFunction.currency(getActivity());
			json = userFunction
					.latestDeals(client, mCurrentPage, getActivity());
			// }
			Log.e("url", "url: " + json);

			if (json != null) {
				ArrayList<DealObj> arr = JSONParser.parserDeal(
						userFunction.array_latest_deals, json.toString());
				mArrDeals.addAll(arr);
				intLengthData = mArrDeals.size();
				// JSONArray dataDealsArray;
				//
				// // If mActivity equal activityCategory then use array
				// // dealByCategory
				// dataDealsArray = json
				// .getJSONArray(userFunction.array_latest_deals);
				//
				// intLengthData = dataDealsArray.length();
				// mDealsId = new String[intLengthData];
				// mTitle = new String[intLengthData];
				// mCompany = new String[intLengthData];
				// mDateEnd = new String[intLengthData];
				// mDateStart = new String[intLengthData];
				// mTimeStart = new String[intLengthData];
				// mTimeEnd = new String[intLengthData];
				// mAfterDiscValue = new String[intLengthData];
				// mStartValue = new String[intLengthData];
				// mImg = new String[intLengthData];
				// mIcMarker = new String[intLengthData];
				// mAttend = new String[intLengthData];
				//
				// for (int i = 0; i < intLengthData; i++) {
				// // Store data from server to variable
				// JSONObject dealsObject = dataDealsArray.getJSONObject(i);
				// HashMap<String, String> map = new HashMap<String, String>();
				// mDealsId[i] = dealsObject
				// .getString(userFunction.key_deals_id);
				// mTitle[i] = dealsObject
				// .getString(userFunction.key_deals_title);
				// mCompany[i] = dealsObject
				// .getString(userFunction.key_deals_company);
				// mDateEnd[i] = dealsObject
				// .getString(userFunction.key_deals_date_end);
				// mDateStart[i] = dealsObject
				// .getString(userFunction.key_deals_date_start);
				// mTimeStart[i] = dealsObject
				// .getString(userFunction.key_deals_time_start);
				// mTimeEnd[i] = dealsObject
				// .getString(userFunction.key_deals_time_end);
				// mAfterDiscValue[i] = dealsObject
				// .getString(userFunction.key_deals_after_disc_value);
				// mStartValue[i] = dealsObject
				// .getString(userFunction.key_deals_start_value);
				// mImg[i] = dealsObject
				// .getString(userFunction.key_deals_image);
				// mIcMarker[i] = dealsObject
				// .getString(userFunction.key_category_marker);
				// mAttend[i] = dealsObject
				// .getString(userFunction.key_deals_attended);
				//
				// map.put(userFunction.key_deals_id, mDealsId[i]); // id not
				// // using
				// // any
				// // where
				// map.put(userFunction.key_deals_title, mTitle[i]);
				// map.put(userFunction.key_deals_company, mCompany[i]);
				// map.put(userFunction.key_deals_date_end, mDateEnd[i]);
				// map.put(userFunction.key_deals_date_start, mDateStart[i]);
				// map.put(userFunction.key_deals_time_start, mTimeStart[i]);
				// map.put(userFunction.key_deals_time_end, mTimeEnd[i]);
				// map.put(userFunction.key_deals_after_disc_value,
				// mAfterDiscValue[i]);
				// map.put(userFunction.key_deals_start_value, mStartValue[i]);
				// map.put(userFunction.key_deals_image, mImg[i]);
				// map.put(userFunction.key_category_marker, mIcMarker[i]);
				// map.put(userFunction.key_deals_attended, mAttend[i]);
				//
				// // Adding HashList to ArrayList
				// menuItems.add(map);
				// }
			}
			if (jsonCurrency != null) {
				JSONArray currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);

			}

		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public void getDataFromDBAll() {
		try {
			// If mActivity equal activityCategory then use API dealByCategory
			// if (mActivity.equals(utils.EXTRA_ACTIVITY_CATEGORY)) {
			// json = userFunction.dealByCategory(mCategoryId, mCurrentPage);
			// } else {
			String apiDeals = UserFunctions.URL_ALL_DEALS + mCurrentPage;
			APIObj allDealInfos = DatabaseUtility.getResuftFromApi(
					getActivity(), apiDeals);
			Log.e("FragmentHome", "Database: " + allDealInfos);
			String jsonAllDeal = "";
			if (allDealInfos != null) {
				jsonAllDeal = allDealInfos.getmResult();
			}
			json = new JSONObject(jsonAllDeal);
			Log.e("url", "All deal: " + jsonAllDeal);

			APIObj currencyInfos = DatabaseUtility.getResuftFromApi(
					getActivity(), UserFunctions.URL_CURRENCY);
			Log.e("FragmentHome", "Database: " + currencyInfos);
			String jsonCurr = "";
			if (currencyInfos != null) {
				jsonCurr = currencyInfos.getmResult();
			}
			jsonCurrency = new JSONObject(jsonCurr);
			Log.e("url", "Currency: " + jsonCurr);

			// }

			if (json != null) {
				ArrayList<DealObj> arr = JSONParser.parserDeal(
						userFunction.array_latest_deals, json.toString());
				mArrDeals.addAll(arr);
				intLengthData = mArrDeals.size();
				// JSONArray dataDealsArray;
				//
				// // If mActivity equal activityCategory then use array
				// // dealByCategory
				// // if (mActivity.equals(utils.EXTRA_ACTIVITY_CATEGORY)) {
				// // dataDealsArray = json
				// // .getJSONArray(userFunction.array_deal_by_category);
				// // } else {
				// dataDealsArray = json
				// .getJSONArray(userFunction.array_latest_deals);
				// // }
				// intLengthData = dataDealsArray.length();
				// mDealsId = new String[intLengthData];
				// mTitle = new String[intLengthData];
				// mCompany = new String[intLengthData];
				// mDateEnd = new String[intLengthData];
				// mDateStart = new String[intLengthData];
				// mTimeStart = new String[intLengthData];
				// mTimeEnd = new String[intLengthData];
				// mAfterDiscValue = new String[intLengthData];
				// mStartValue = new String[intLengthData];
				// mImg = new String[intLengthData];
				// mIcMarker = new String[intLengthData];
				// mAttend = new String[intLengthData];
				//
				// for (int i = 0; i < intLengthData; i++) {
				// // Store data from server to variable
				// JSONObject dealsObject = dataDealsArray.getJSONObject(i);
				// HashMap<String, String> map = new HashMap<String, String>();
				// mDealsId[i] = dealsObject
				// .getString(userFunction.key_deals_id);
				// mTitle[i] = dealsObject
				// .getString(userFunction.key_deals_title);
				// mCompany[i] = dealsObject
				// .getString(userFunction.key_deals_company);
				// mDateEnd[i] = dealsObject
				// .getString(userFunction.key_deals_date_end);
				// mDateStart[i] = dealsObject
				// .getString(userFunction.key_deals_date_start);
				// mTimeStart[i] = dealsObject
				// .getString(userFunction.key_deals_time_start);
				// mTimeEnd[i] = dealsObject
				// .getString(userFunction.key_deals_time_end);
				// mAfterDiscValue[i] = dealsObject
				// .getString(userFunction.key_deals_after_disc_value);
				// mStartValue[i] = dealsObject
				// .getString(userFunction.key_deals_start_value);
				// mImg[i] = dealsObject
				// .getString(userFunction.key_deals_image);
				// mIcMarker[i] = dealsObject
				// .getString(userFunction.key_category_marker);
				// mAttend[i] = dealsObject
				// .getString(userFunction.key_deals_attended);
				//
				// map.put(userFunction.key_deals_id, mDealsId[i]); // id not
				// // using
				// // any
				// // where
				// map.put(userFunction.key_deals_title, mTitle[i]);
				// map.put(userFunction.key_deals_company, mCompany[i]);
				// map.put(userFunction.key_deals_date_end, mDateEnd[i]);
				// map.put(userFunction.key_deals_date_start, mDateStart[i]);
				// map.put(userFunction.key_deals_time_start, mTimeStart[i]);
				// map.put(userFunction.key_deals_time_end, mTimeEnd[i]);
				// map.put(userFunction.key_deals_after_disc_value,
				// mAfterDiscValue[i]);
				// map.put(userFunction.key_deals_start_value, mStartValue[i]);
				// map.put(userFunction.key_deals_image, mImg[i]);
				// map.put(userFunction.key_category_marker, mIcMarker[i]);
				// map.put(userFunction.key_deals_attended, mAttend[i]);
				//
				// // Adding HashList to ArrayList
				// menuItems.add(map);
				// }
			}
			if (jsonCurrency != null) {
				JSONArray currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);
			}

		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	private void clickOnHomeMenu(Fragment mFrgHome)
	{

		if (mFrgHome == null) {
			mFrgHome = new FragmentHome();
		}
		try
		{
			changeFragment(mFrgHome);
		} catch (IllegalStateException ex) {
		}
	}

	private void changeFragment(Fragment targetFragment) {
		getActivity().getSupportFragmentManager().beginTransaction()
				.replace(R.id.frame_content, targetFragment, "fragment")
				.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
				.commit();
		//targetFragment.setArguments(getActivity().bundle);
	}
}
