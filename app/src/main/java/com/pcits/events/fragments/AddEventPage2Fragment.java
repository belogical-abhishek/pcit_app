/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pcits.events.fragments;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.pcits.common.utils.GsonUtility;
import com.pcits.common.utils.Logging;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.widgets.FloatLabeledEditText;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;
import com.pcits.events.wizard.model.CustomAddEventPage2;
import com.pcits.events.wizard.ui.PageFragmentCallbacks;

public class AddEventPage2Fragment extends Fragment {

	private static final String TAG = "AddEventPage2Fragment";

	private static final String ARG_KEY = "key";

	private PageFragmentCallbacks mCallbacks;
	private String mKey;
	private CustomAddEventPage2 mPage;

	private EditText mFlBeforeDiscount, mFlAfterDiscount;
	private TextViewRobotoCondensedRegular mLblStartDate, mLblStartTime,
			mLblEndDate, mLblEndTime;

	private static Date mStartDate, mEndDate;

	private View mRootView;
	private Bundle mBundle;

	public static AddEventPage2Fragment create(String key) {
		Bundle args = new Bundle();
		args.putString(ARG_KEY, key);

		AddEventPage2Fragment fragment = new AddEventPage2Fragment();
		fragment.setArguments(args);
		return fragment;
	}

	public AddEventPage2Fragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mBundle = savedInstanceState;

		Bundle args = getArguments();
		mKey = args.getString(ARG_KEY);
		mPage = (CustomAddEventPage2) mCallbacks.onGetPage(mKey);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragment_add_event_page2,
				container, false);
		// ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage
		// .getTitle());

		initUI();

		return mRootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (!(activity instanceof PageFragmentCallbacks)) {
			throw new ClassCastException(
					"Activity must implement PageFragmentCallbacks");
		}

		mCallbacks = (PageFragmentCallbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);

		// In a future update to the support library, this should override
		// setUserVisibleHint
		// instead of setMenuVisibility.
		if (mFlBeforeDiscount != null && mFlAfterDiscount != null
				&& mLblStartDate != null && mLblStartTime != null
				&& mLblEndDate != null && mLblEndTime != null) {
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			if (!menuVisible) {
				imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
			}
		}
	}

	public void saveState() {
		try {
			GlobalValue.dealsObj.setStart_date(mLblStartDate.getText()
					.toString().trim());
			GlobalValue.dealsObj.setStart_time(mLblStartTime.getText()
					.toString().trim());
			GlobalValue.dealsObj.setEnd_date(mLblEndDate.getText().toString()
					.trim());
			GlobalValue.dealsObj.setEnd_time(mLblEndTime.getText().toString()
					.trim());
			GlobalValue.dealsObj
					.setStart_value(Double.parseDouble(mFlBeforeDiscount
							.getText().toString().trim()));
			GlobalValue.dealsObj.setAfter_discount_value(Double
					.parseDouble(mFlAfterDiscount.getText().toString().trim()));
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public void restoreState() {
		try {
		    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

			if (GlobalValue.dealsObj.getStart_value() != null) {
				String startValue = GlobalValue.dealsObj.getStart_value() + "";
				mFlBeforeDiscount.setText(startValue.substring(0,
						startValue.indexOf(".")));
			}
			if (GlobalValue.dealsObj.getAfter_discount_value() != null) {
				String afterValue = GlobalValue.dealsObj
						.getAfter_discount_value() + "";
				mFlAfterDiscount.setText(afterValue.substring(0,
						afterValue.indexOf(".")));
			}

			try {
                Date date = new Date(GlobalValue.dealsObj.getStart_date());
                mLblStartDate.setText(DateTimeUtility.formatDate(date,
                        "yyyy-MM-dd"));
            } catch(Exception e) {
                mLblStartDate.setText(GlobalValue.dealsObj.getStart_date());
            }

			//Log.d(TAG, "new restoreState: "+ GsonUtility.convertObjectToJSONString(GlobalValue.dealsObj));
//			mLblStartDate.setText(DateTimeUtility.formatDate(new Date(GlobalValue.dealsObj.getStart_date()),
//                    "yyyy-MM-dd"));
			String[] startTime ;
			try
			{
				 startTime = GlobalValue.dealsObj.getStart_time().split(":");
			}
			catch (Exception e)
			{
				 startTime = GlobalValue.dealsObj.getStartTimeStamp().split(":");
			}

			mLblStartTime.setText(startTime[0]+":"+startTime[1]);

            try {
                Date date = new Date(GlobalValue.dealsObj.getStart_date());
                mLblStartDate.setText(DateTimeUtility.formatDate(date,
                        "yyyy-MM-dd"));
            } catch(Exception e) {
                mLblStartDate.setText(GlobalValue.dealsObj.getStart_date());
            }

            try {
				mLblEndDate.setText(
						DateTimeUtility.formatDate(new Date(GlobalValue.dealsObj.getEnd_date()),
								"yyyy-MM-dd"));
			} catch(Exception ex) {
				mLblEndDate.setText(GlobalValue.dealsObj.getEnd_date());
			}
			String[] endTime;
			try
			{
				 endTime = GlobalValue.dealsObj.getEnd_time().split(":");
			}
			catch (Exception e)
			{
				e.printStackTrace();
				endTime = GlobalValue.dealsObj.getEndTimeStamp().split(":");
			}


			mLblEndTime.setText(endTime[0]+":"+endTime[1]);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void initUI() {
		mFlBeforeDiscount = (EditText) mRootView
				.findViewById(R.id.txtStartValue);

		mFlBeforeDiscount.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				mFlAfterDiscount.setError(null);
				mFlBeforeDiscount.setError(null);

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {

				if(!mFlBeforeDiscount.getText().toString().isEmpty() && !mFlAfterDiscount.getText().toString().isEmpty() ){

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {

							if(Float.parseFloat(mFlAfterDiscount.getText().toString())>Float.parseFloat(mFlBeforeDiscount.getText().toString())) {
								Toast.makeText(getContext(), "After discount value cannot be greater then actual value", Toast.LENGTH_LONG).show();
								mFlBeforeDiscount.setError("Invalid");
							}
						}
					},2000);
				}

			}
		});
		mFlAfterDiscount = (EditText) mRootView
				.findViewById(R.id.txtAfterDiscValue);

		mFlAfterDiscount.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				mFlAfterDiscount.setError(null);
				mFlBeforeDiscount.setError(null);
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if(!mFlBeforeDiscount.getText().toString().isEmpty() && !mFlAfterDiscount.getText().toString().isEmpty() ){
					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							if (Float.parseFloat(mFlAfterDiscount.getText().toString()) > Float.parseFloat(mFlBeforeDiscount.getText().toString())) {
								Toast.makeText(getContext(), "After discount value cannot be greater then actual value", Toast.LENGTH_LONG).show();
								mFlAfterDiscount.setError("Invalid");
							}
						}
					}, 2000);
				}



			}
		});


		mLblStartDate = (TextViewRobotoCondensedRegular) mRootView
				.findViewById(R.id.lblStartDate);

		mLblStartTime = (TextViewRobotoCondensedRegular) mRootView
				.findViewById(R.id.lblStartTime);

		mLblEndDate = (TextViewRobotoCondensedRegular) mRootView
				.findViewById(R.id.lblEndDate);

		mLblEndTime = (TextViewRobotoCondensedRegular) mRootView
				.findViewById(R.id.lblEndTime);

		// Should call this method end of declaring UI.
		initControl();
	}

	private void initControl() {
		mLblStartDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showStartDatePicker();
			}
		});

		mLblEndDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showEndDatePicker();
			}
		});

		mLblStartTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showStartTimePicker();
			}
		});

		mLblEndTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showEndTimePicker();
			}
		});
	}

	private void showEndTimePicker() {
		final Calendar cal = Calendar.getInstance();

		TimePickerDialog.OnTimeSetListener callBack = new TimePickerDialog.OnTimeSetListener() {

			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
				if (mStartDate != null) {
					cal.set(mStartDate.getYear() + 1900, mStartDate.getMonth(),
							mStartDate.getDate(), hourOfDay, minute);
				} else {
					cal.set(Calendar.YEAR, Calendar.MONTH, Calendar.DATE,
							hourOfDay, minute);
				}

				String hour = "";
				String min = "";

				hour = (hourOfDay < 10) ? "0" + hourOfDay : hourOfDay + "";
				min = (minute < 10) ? "0" + minute : minute + "";

				mLblEndTime.setText(hour + ":" + min);

				// Set data to deal object
				GlobalValue.dealsObj.setEnd_time(mLblEndTime.getText()
						.toString());
			}
		};
		DurationTimePickDialog timePicker = new DurationTimePickDialog(
				getActivity(), callBack, cal.get(Calendar.HOUR_OF_DAY) + 2,
				cal.get(Calendar.MINUTE), true, 15);
		timePicker.show();
	}

	private void showStartTimePicker() {
		final Calendar cal = Calendar.getInstance();

		TimePickerDialog.OnTimeSetListener callBack = new TimePickerDialog.OnTimeSetListener() {

			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
				if (mStartDate != null) {
					cal.set(mStartDate.getYear() + 1900, mStartDate.getMonth(),
							mStartDate.getDate(), hourOfDay, minute);
				} else {
					cal.set(Calendar.YEAR, Calendar.MONTH, Calendar.DATE,
							hourOfDay, minute);
				}

				String hour = "";
				String min = "";

				hour = (hourOfDay < 10) ? "0" + hourOfDay : hourOfDay + "";
				min = (minute < 10) ? "0" + minute : minute + "";

				mLblStartTime.setText(hour + ":" + min);

				// Set data to deal object
				GlobalValue.dealsObj.setStart_time(mLblStartTime.getText()
						.toString());
			}
		};

		DurationTimePickDialog timePicker = new DurationTimePickDialog(
				getActivity(), callBack, cal.get(Calendar.HOUR_OF_DAY),
				cal.get(Calendar.MINUTE), true, 15);
		timePicker.show();
	}

	private void showEndDatePicker() {
		final Calendar cal = Calendar.getInstance();

		OnDateSetListener callBack = new OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				// Start date by current date + 1
				cal.set(year, monthOfYear, dayOfMonth);
				mEndDate = cal.getTime();

				mLblEndDate.setText(DateTimeUtility.formatDate(mEndDate,
						"yyyy-MM-dd"));

				// Set data to deal object
				GlobalValue.dealsObj.setEnd_date((mLblEndDate.getText()
						.toString()));
			}
		};

		DatePickerDialog datePicker = null;
		if (mStartDate != null) {
			datePicker = new DatePickerDialog(getActivity(), callBack,
					mStartDate.getYear() + 1900, mStartDate.getMonth(),
					mStartDate.getDate());
		} else {
			datePicker = new DatePickerDialog(getActivity(), callBack,
					cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
					cal.get(Calendar.DATE) + 1);
		}
		datePicker.show();
	}

	private void showStartDatePicker() {
		final Calendar cal = Calendar.getInstance();

		OnDateSetListener callBack = new OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				// Start date by current date + 1
				cal.set(year, monthOfYear, dayOfMonth);
				mStartDate = cal.getTime();

				mLblStartDate.setText(DateTimeUtility.formatDate(mStartDate,
						"yyyy-MM-dd"));

				// Set data to deal object
				GlobalValue.dealsObj.setStart_date((mLblStartDate.getText()
						.toString()));
			}
		};

		DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
				callBack, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
				cal.get(Calendar.DATE) + 1);
		datePicker.show();
	}

	private class DurationTimePickDialog extends TimePickerDialog {
		final OnTimeSetListener mCallback;
		TimePicker mTimePicker;
		final int increment;

		public DurationTimePickDialog(Context context,
				OnTimeSetListener callBack, int hourOfDay, int minute,
				boolean is24HourView, int increment) {
			super(context, callBack, hourOfDay, minute / increment,
					is24HourView);
			this.mCallback = callBack;
			this.increment = increment;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {
			if (mCallback != null && mTimePicker != null) {
				mTimePicker.clearFocus();
				mCallback.onTimeSet(mTimePicker, mTimePicker.getCurrentHour(),
						mTimePicker.getCurrentMinute() * increment);
			}
		}

		@Override
		protected void onStop() {
			// override and do nothing
		}

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			try {
				Class<?> rClass = Class.forName("com.android.internal.R$id");
				Field timePicker = rClass.getField("timePicker");
				this.mTimePicker = (TimePicker) findViewById(timePicker
						.getInt(null));
				Field m = rClass.getField("minute");

				NumberPicker mMinuteSpinner = (NumberPicker) mTimePicker
						.findViewById(m.getInt(null));
				mMinuteSpinner.setMinValue(0);
				mMinuteSpinner.setMaxValue((60 / increment) - 1);
				List<String> displayedValues = new ArrayList<String>();
				for (int i = 0; i < 60; i += increment) {
					displayedValues.add(String.format("%02d", i));
				}
				mMinuteSpinner.setDisplayedValues(displayedValues
						.toArray(new String[0]));
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			}
		}
	}
}
