package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pcits.events.ActivityDetailTickets;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.obj.PaidticketsObj;
import com.pcits.events.widgets.MaterialRippleLayout;

public class TicketsAdapter extends BaseAdapter {

	private Activity activity;
	private ArrayList<PaidticketsObj> arrTicket;
	private LayoutInflater mInflate;

	public TicketsAdapter(Activity a, ArrayList<PaidticketsObj> listTicket) {
		this.activity = a;
		this.arrTicket = listTicket;
		this.mInflate = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrTicket.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		// TODO Auto-generated method stub
		final HolderView holder;
		if (convertView == null) {
			holder = new HolderView();
			convertView = mInflate.inflate(R.layout.item_ticket, null);
			holder.lblTitle = (TextView) convertView
					.findViewById(R.id.lblTitleEvent);
			holder.lblCompany = (TextView) convertView
					.findViewById(R.id.lblCompnay);
			holder.lblDate = (TextView) convertView
					.findViewById(R.id.lblDateBuy);
			holder.lblLive = (TextView) convertView.findViewById(R.id.lblLive);
			holder.itemTicket = (MaterialRippleLayout) convertView
					.findViewById(R.id.itemTicket);
			convertView.setTag(holder);
		} else {
			holder = (HolderView) convertView.getTag();
		}
		final PaidticketsObj o = arrTicket.get(position);
		if (o != null) {
			holder.lblTitle.setText(o.getTitle());
			holder.lblCompany.setText("By: " + o.getCompany());
			holder.lblDate.setText("Date buy: " + o.getUsed_date());
			if (o.getStatus() == 0) {
				holder.lblLive.setBackgroundColor(Color.GREEN);
				holder.lblLive.setText("Live");
			} else {
				holder.lblLive.setBackgroundColor(Color.GRAY);
				holder.lblLive.setText("Used");
			}

			holder.itemTicket.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					GlobalValue.ticket = arrTicket.get(position);
					Intent i = new Intent(activity, ActivityDetailTickets.class);
					parent.getContext().startActivity(i);
				}
			});

		}
		return convertView;
	}

	public class HolderView {
		TextView lblTitle, lblCompany, lblDate, lblLive;
		MaterialRippleLayout itemTicket;
	}

}
