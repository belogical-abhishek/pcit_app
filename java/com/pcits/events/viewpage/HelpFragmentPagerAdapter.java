package com.pcits.events.viewpage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.pcits.events.ActivityHome;
public class HelpFragmentPagerAdapter extends FragmentPagerAdapter{
	
	final int PAGE_COUNT = 2;
	Activity mainActivity;
	/** Constructor of the class 
	 * @param mainActivity */
	public HelpFragmentPagerAdapter(FragmentManager fm, ActivityHome mainActivity) {
		super(fm);
		
		this.mainActivity=mainActivity;
		
	}

	public HelpFragmentPagerAdapter(FragmentManager fm, Activity help) {
		// TODO Auto-generated constructor stub
		super(fm);
		this.mainActivity=help;
	}


	/** This method will be invoked when a page is requested to create */
	@Override
	public Fragment getItem(int arg0) {

		HelpFragment helpFragment ;
		
		helpFragment = new HelpFragment(this.mainActivity);
		Bundle data = new Bundle();
		data.putInt("current_page", arg0+1);
		helpFragment.setArguments(data);



		return helpFragment;
	}

	/** Returns the number of pages */
	@Override
	public int getCount() {		
		return PAGE_COUNT;
	}

}
