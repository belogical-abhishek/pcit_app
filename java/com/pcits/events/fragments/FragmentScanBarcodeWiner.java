package com.pcits.events.fragments;

import net.sourceforge.zbar.Symbol;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityHome;
import com.pcits.events.R;
import com.pcits.events.integration.android.ZBarConstants;
import com.pcits.events.integration.android.ZBarScannerActivity;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.progressButton.CircularProgressButton;

public class FragmentScanBarcodeWiner extends Fragment {

	private Button scanBtn;
	private TextView contentTxt;
	private static final int ZBAR_SCANNER_REQUEST = 0;
	private static final int ZBAR_QR_SCANNER_REQUEST = 1;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private String contents;
	private CircularProgressButton btnUpdate;
	private View v;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		v = inflater.inflate(R.layout.fragment_scan_ticket, container, false);
		try {
			initUi();
			initControl();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return v;

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		btnUpdate.setProgress(0);
	}

	private void initUi() {
		try {
			// lblTitleHeader.setText("Scan Ticket");
			llMenu = (LinearLayout) v.findViewById(R.id.llMenu);
			scanBtn = (Button) v.findViewById(R.id.scan_button);
			contentTxt = (TextView) v.findViewById(R.id.scan_content);
			btnUpdate = (CircularProgressButton) v
					.findViewById(R.id.circularButton1);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void initControl() {
		try {
			llMenu.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// ActivityHome.resideMenu.openMenu();
					//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
					Intent intent=new Intent(getActivity(),ActivityHome.class);
					startActivity(intent);
					getActivity().finish();
				}
			});
			//
			scanBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (isCameraAvailable()) {
						Intent intent = new Intent(getActivity(),
								ZBarScannerActivity.class);
						intent.putExtra(ZBarConstants.SCAN_MODES,
								new int[] { Symbol.QRCODE });
						startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
					} else {
						Toast.makeText(getActivity(),
								"Rear Facing Camera Unavailable",
								Toast.LENGTH_SHORT).show();
					}
				}
			});
			//

			btnUpdate.setIndeterminateProgressMode(true);
			btnUpdate.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						String[] result = contents.split(" ");
						String Id = result[1];
						String type = result[3];
						Log.d("scanwinner", "id: " + Id + "type: " + type);

						ModelManager.updateWinner(getActivity(), Id,
								Integer.toString(1), true,
								new ModelManagerListener() {

									@Override
									public void onSuccess(Object object) {
										try {
											String strJson = (String) object;
											Toast.makeText(getActivity(),
													checkResult(strJson),
													Toast.LENGTH_SHORT).show();
											btnUpdate.setProgress(100);
										} catch (Exception ex) {
											throw new RuntimeException(ex);
										}
									}

									@Override
									public void onError() {
										// TODO Auto-generated method stub
										btnUpdate.setProgress(0);
									}
								});
						// Toast.makeText(getActivity(), "bug",
						// Toast.LENGTH_SHORT)
						// .show();
						// if (btnUpdate.getProgress() == 0) {
						// btnUpdate.setProgress(50);
						// } else if (btnUpdate.getProgress() == 100) {
						// btnUpdate.setProgress(0);
						// } else {
						// btnUpdate.setProgress(100);
						//
						// }
					} catch (Exception ex) {
						throw new RuntimeException(ex);
					}
				}
			});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	protected String checkResult(String strJson) {
		// TODO Auto-generated method stub
		JSONObject json = null;
		String message = "";
		try {
			json = new JSONObject(strJson);
			if (json.getString("status").equals("success")) {
				message = this.getString(R.string.message_success);
			} else {
				message = json.getString("message");
			}

		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}

		return message;
	}

	public boolean isCameraAvailable() {
		PackageManager pm = getActivity().getPackageManager();
		return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// retrieve scan result
		switch (requestCode) {
		case ZBAR_SCANNER_REQUEST:
		case ZBAR_QR_SCANNER_REQUEST:
			if (resultCode == getActivity().RESULT_OK) {
				contents = data.getStringExtra("SCAN_RESULT");
				contentTxt.setText(contents);
				btnUpdate.setVisibility(View.VISIBLE);
			} else if (resultCode == getActivity().RESULT_CANCELED
					&& data != null) {
				String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
				if (!TextUtils.isEmpty(error)) {
					Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT)
							.show();
				}
			}
			break;
		}
	}
}
