package com.pcits.events;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.pcits.events.viewpage.CirclePageIndicator;
import com.pcits.events.viewpage.HelpFragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ActivityHelp extends FragmentActivity {

	private ViewPager viewPager;
	private HelpFragmentPagerAdapter mAdapter;
	private LinearLayout llMenu;
	private Handler handler;
	private final int delay = 2000;
	private int page = 0;

	/*Runnable runnable = new Runnable()
	{
		public void run()
		{
			if (mAdapter.getCount() == page)
			{
				page = 0;
			}
			else {
				page++;
			}
			viewPager.setCurrentItem(page, true);
			handler.postDelayed(this, delay);
		}
	};*/

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help);
		setTitle("Help");
		final RadioGroup radioGroup = (RadioGroup)findViewById(R.id.radiogroupHelp);
		llMenu=(LinearLayout)findViewById(R.id.llMenu);
		handler = new Handler();

		viewPager = (ViewPager)findViewById(R.id.help_view_pager);
		mAdapter = new HelpFragmentPagerAdapter(ActivityHelp.this, getTestData());

		//SlideshowPagerAdapter adapter = new SlideshowPagerAdapter(getSupportFragmentManager());
		viewPager.setAdapter(mAdapter);
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
		{
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
			{
			}

			@Override
			public void onPageSelected(int position)
			{
				page = position;
				switch (position){
					case 0:
						radioGroup.check(R.id.radioButtonHelp);
						break;
					case 1:
						radioGroup.check(R.id.radioButton2Help);
						break;

				}
			}
			@Override
			public void onPageScrollStateChanged(int state)
			{
			}
		});

		llMenu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(ActivityHelp.this,ActivityHome.class);
				startActivity(intent);
				finish();
			}
		});
	}
	public List<String> getTestData()
	{
		List<String> mTestData = new ArrayList<String>();
		mTestData.add(new String(getApplicationContext().getString(R.string.help1)));
		mTestData.add(new String(getApplicationContext().getString(R.string.help2)));
		return mTestData;
	}
/*
	@Override
	protected void onResume()
	{
		super.onResume();
		handler.postDelayed(runnable, delay);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		handler.removeCallbacks(runnable);
	}*/
}