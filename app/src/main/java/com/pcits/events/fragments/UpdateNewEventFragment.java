package com.pcits.events.fragments;

import java.util.Collections;
import java.util.Comparator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.R;
import com.pcits.events.adapters.NotifyUpdateNewEventAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.obj.NotifyBoxObj;
import com.pcits.events.stickylist.StickyListHeadersListView;

public class UpdateNewEventFragment extends Fragment {

	private View v;
	// Declare sticky list header
	private StickyListHeadersListView mStickyList;
	private NotifyUpdateNewEventAdapter mStickyAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		v = inflater.inflate(R.layout.layout_notify_update_new_event,
				container, false);
		initUI();
		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		try {
			super.onResume();
			setData();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	private void initUI() {
		mStickyList = (StickyListHeadersListView) v
				.findViewById(R.id.lvUpdateNewEvent);
	}

	// private void setData() {
	// if (GlobalValue.myClient != null) {
	//
	// ModelManager.getListNotifyBox(getActivity(), true,
	// new ModelManagerListener() {
	//
	// @Override
	// public void onSuccess(Object object) {
	// // TODO Auto-generated method stub
	// String json = (String) object;
	// GlobalValue.arrUpdateNewEvent = ParserUtility
	// .getListNotifyBox(json);
	// if (GlobalValue.arrUpdateNewEvent.size() > 0) {
	// // Sort by status
	// Collections.sort(GlobalValue.arrUpdateNewEvent,
	// new Comparator<NotifyBoxObj>() {
	//
	// @Override
	// public int compare(
	// NotifyBoxObj lhs,
	// NotifyBoxObj rhs) {
	// // TODO Auto-generated method
	// // stub
	// Integer stt1 = Integer
	// .valueOf(lhs
	// .getType_id());
	// Integer stt2 = Integer
	// .valueOf(rhs
	// .getType_id());
	// return stt1.compareTo(stt2);
	// }
	// });
	//
	// mStickyAdapter = new NotifyUpdateNewEventAdapter(
	// getActivity(),
	// GlobalValue.arrUpdateNewEvent);
	// mStickyAdapter.notifyDataSetChanged();
	// mStickyList.setAdapter(mStickyAdapter);
	// } else {
	// mStickyList.removeAllViews();
	// Toast.makeText(getActivity(), "No Data",
	// Toast.LENGTH_SHORT).show();
	// }
	// }
	//
	// @Override
	// public void onError() {
	// // TODO Auto-generated method stub
	//
	// }
	// });
	// }
	// }

	private void setData() {
		if (GlobalValue.myClient != null) {
			GlobalValue.arrUpdateNewEvent = DatabaseUtility
					.selectNotification(getActivity());
			if (GlobalValue.arrUpdateNewEvent.size() > 0) {
				// Sort by status
				Collections.sort(GlobalValue.arrUpdateNewEvent,
						new Comparator<NotifyBoxObj>() {

							@Override
							public int compare(NotifyBoxObj lhs,
									NotifyBoxObj rhs) {
								// TODO Auto-generated method
								// stub
								try {
									Integer stt1 = Integer.valueOf(lhs
											.getType_id());
									Integer stt2 = Integer.valueOf(rhs
											.getType_id());
									Integer date1 = Integer.valueOf(lhs
											.getDate());
									Integer date2 = Integer.valueOf(rhs
											.getDate());

									if (stt1.compareTo(stt2) != 0) {
										return stt1.compareTo(stt2);
									}

									return date2.compareTo(date1);
								} catch (NumberFormatException ex) {
									throw new RuntimeException(ex);
								} catch (Exception ex) {
									throw new RuntimeException(ex);
								}
							}
						});

				mStickyAdapter = new NotifyUpdateNewEventAdapter(getActivity(),
						GlobalValue.arrUpdateNewEvent);
				mStickyAdapter.notifyDataSetChanged();
				mStickyList.setAdapter(mStickyAdapter);
			} else {
				mStickyList.removeAllViews();
				Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}
}
