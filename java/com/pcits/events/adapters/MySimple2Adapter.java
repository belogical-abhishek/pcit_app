package com.pcits.events.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import com.pcits.events.obj.ListUsersObj;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EnvisioDevs on 07-03-2018.
 */

public class MySimple2Adapter extends BaseAdapter {
    List<ListUsersObj> mUsers = new ArrayList<>();
    Context context;

    public MySimple2Adapter(Context context, List<ListUsersObj> users) {
        mUsers = users;
        this.context = context;
    }
    @Override
    public int getCount() {
        return mUsers.size();
    }

    @Override
    public ListUsersObj getItem(int i) {
        return mUsers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        TwoLineListItem twoLineListItem;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            twoLineListItem = (TwoLineListItem) inflater.inflate(
                    android.R.layout.simple_list_item_2, null);
        } else {
            twoLineListItem = (TwoLineListItem) convertView;
        }

        TextView text1 = twoLineListItem.getText1();
        TextView text2 = twoLineListItem.getText2();

        text1.setText(mUsers.get(position).getEmail());
        if(mUsers.get(position).getStatus()==0)
            text2.setText("Pending");

        else if(mUsers.get(position).getStatus()==1)
            text2.setText("Attending");


        return twoLineListItem;
    }
}
