/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pcits.events.wizard.model;

import java.util.ArrayList;

import android.support.v4.app.Fragment;

import com.pcits.events.ProfileActivity;
import com.pcits.events.fragments.ProfileFragmentPage1;

/**
 * A page asking for a name and an email.
 */
public class CustomProfilePage1 extends Page implements ICustomPage {
	// public static final String USERNAME_DATA_KEY = "userName";
	// public static final String FNAME_DATA_KEY = "fName";
	// public static final String LNAME_DATA_KEY = "lName";
	// public static final String GENDER_DATA_KEY = "gender";
	// public static final String DOB_DATA_KEY = "dob";
	// public static final String PAY_INFO_DATA_KEY = "payInfo";

	private ProfileFragmentPage1 mFragment;

	public CustomProfilePage1(ModelCallbacks callbacks, String title) {
		super(callbacks, title);
	}

	@Override
	public Fragment createFragment() {
		mFragment = ProfileFragmentPage1.create(getKey());
		return mFragment;
	}

	@Override
	public void getReviewItems(ArrayList<ReviewItem> dest) {
		if (ProfileActivity.tempClient != null) {
			dest.add(new ReviewItem("User name", ProfileActivity.tempClient
					.getUsername(), getKey(), -1));
			dest.add(new ReviewItem("Full name", ProfileActivity.tempClient
					.getFname() + " " + ProfileActivity.tempClient.getLname(),
					getKey(), -1));
			dest.add(new ReviewItem("Gender", ProfileActivity.tempClient
					.getGender(), getKey(), -1));
			dest.add(new ReviewItem("Email", ProfileActivity.tempClient
					.getEmail(), getKey(), -1));
		} else if (ProfileActivity.tempAdmin != null) {
			dest.add(new ReviewItem("User name", ProfileActivity.tempAdmin
					.getUsername(), getKey(), -1));
			dest.add(new ReviewItem("Full name", ProfileActivity.tempAdmin
					.getFname() + " " + ProfileActivity.tempAdmin.getLname(),
					getKey(), -1));
			dest.add(new ReviewItem("Email", ProfileActivity.tempAdmin
					.getEmail(), getKey(), -1));
		}
	}

	@Override
	public boolean isCompleted() {
		// boolean completed = false;
		// if (TextUtils.isEmpty(mData.getString(TITLE_DATA_KEY))
		// || TextUtils.isEmpty(mData.getString(COMPANY_DATA_KEY))) {
		// completed = false;
		// } else {
		// completed = true;
		// }
		// return completed;
		return true;
	}

	@Override
	public void saveState() {
		// TODO Auto-generated method stub
		mFragment.saveState();
	}

	@Override
	public void restoreState() {
		// TODO Auto-generated method stub
		mFragment.restoreState();
	}

}
