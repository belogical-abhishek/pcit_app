package com.pcits.events.fragments;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;

import com.pcits.events.ProfileActivity;
import com.pcits.events.R;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.widgets.FloatLabeledEditText;
import com.pcits.events.wizard.model.Page;
import com.pcits.events.wizard.ui.PageFragmentCallbacks;

public class ProfileFragmentPage3 extends Fragment implements
		PageFragmentCallbacks {

	private View mView;

	private static final String TAG = "ProfileFragmentPage2";

	private static final String ARG_KEY = "key";

	private FloatLabeledEditText mFlPhone, mFlDob, mFlPayInfo;

	// private PageFragmentCallbacks mCallbacks;
	// private String mKey;
	// private CustomProfilePage2 mPage;

	private boolean isCancel = false;

	private String mDob = "", mDob2 = "";

	public static ProfileFragmentPage3 create(String key) {
		Bundle args = new Bundle();
		args.putString(ARG_KEY, key);

		ProfileFragmentPage3 fragment = new ProfileFragmentPage3();
		fragment.setArguments(args);
		return fragment;
	}

	public ProfileFragmentPage3() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Bundle args = getArguments();
		// mKey = args.getString(ARG_KEY);
		// mPage = (CustomProfilePage2) mCallbacks.onGetPage(mKey);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.fragment_profile_page3, container,
				false);
		// ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage
		// .getTitle());

		initUI();
		return mView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (!(activity instanceof PageFragmentCallbacks)) {
			throw new ClassCastException(
					"Activity must implement PageFragmentCallbacks");
		}

		// mCallbacks = (PageFragmentCallbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		// mCallbacks = null;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);

		// In a future update to the support library, this should override
		// setUserVisibleHint
		// instead of setMenuVisibility.
		if (mFlPhone != null && mFlDob != null && mFlPayInfo != null) {
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			if (!menuVisible) {
				imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
			}
		}
	}

	public void saveState() {
		try {
			if (ProfileActivity.tempClient != null) {
				ProfileActivity.tempClient.setPhone(mFlPhone.getTextString());
				ProfileActivity.tempClient.setDob(mFlDob.getTextString());

				Log.i("PCIT_ABHISHEK", "Phone : " + ProfileActivity.tempClient.getPhone());
				Log.i("PCIT_ABHISHEK", "DOB : " + ProfileActivity.tempClient.getDob());
			} else if (ProfileActivity.tempAdmin != null) {
				ProfileActivity.tempAdmin.setPhone(mFlPhone.getTextString());
				ProfileActivity.tempAdmin.setDob(mFlDob.getTextString());
				ProfileActivity.tempAdmin.setPayInfo(mFlPayInfo.getTextString()
						.trim());
			}
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public void restoreState() {
		try {
			if (ProfileActivity.tempClient != null) {
				mFlPhone.setText(ProfileActivity.tempClient.getPhone());
				mFlDob.setText(ProfileActivity.tempClient.getDob());
			} else if (ProfileActivity.tempAdmin != null) {
				mFlPhone.setText(ProfileActivity.tempAdmin.getPhone());
				mFlDob.setText(ProfileActivity.tempAdmin.getDob());
				mFlPayInfo.setText(ProfileActivity.tempAdmin.getPayInfo());
			}
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	private void initUI() {
		mFlPhone = (FloatLabeledEditText) mView.findViewById(R.id.txt_phone);
		mFlDob = (FloatLabeledEditText) mView.findViewById(R.id.txt_dob);
		mFlDob.getEditText().setKeyListener(null);
		mFlPayInfo = (FloatLabeledEditText) mView
				.findViewById(R.id.txt_payInfo);

		if (ProfileActivity.tempAdmin != null) {
			mDob = ProfileActivity.tempAdmin.getDob();
		} else if (ProfileActivity.tempClient != null) {
			mDob = ProfileActivity.tempClient.getDob();

			mFlPayInfo.setVisibility(View.GONE);
		}

		// Should call this method by the end of declare UI.
		initControl();
	}

	private void initControl() {
		mFlDob.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showDatePicker();
			}
		});

		mFlDob.getEditText().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showDatePicker();
			}
		});
	}

	@Override
	public Page onGetPage(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	private void showDatePicker() {
		final Calendar cal = Calendar.getInstance();

		OnDateSetListener callBack = new OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				// Calendar cal2 = Calendar.getInstance();
				// Start date by current date + 1
				cal.set(year, monthOfYear, dayOfMonth);

				// mFlDob.setText(DateTimeUtility.formatDate(cal.getTime(),
				// "yyyy-MM-dd"));

				// Set data to deal object
				// if (ProfileActivity.tempAdmin != null) {
				// ProfileActivity.tempAdmin.setDob((mFlDob.getText()
				// .toString()));
				// } else if (ProfileActivity.tempClient != null) {
				// ProfileActivity.tempClient.setDob((mFlDob.getText()
				// .toString()));
				// }

				mDob2 = DateTimeUtility.formatDate(cal.getTime(), "yyyy-MM-dd");
			}

		};

		DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
				callBack, cal.get(Calendar.YEAR) - 20, cal.get(Calendar.MONTH),
				cal.get(Calendar.DATE));

		datePicker.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				isCancel = true;
			}
		});

		datePicker.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {
				if (isCancel) {
					mFlDob.setText(mDob);
					isCancel = false;
				} else {
					mFlDob.setText(mDob2);

					if (ProfileActivity.tempAdmin != null) {
						ProfileActivity.tempAdmin.setDob(mDob2);
					} else if (ProfileActivity.tempClient != null) {
						ProfileActivity.tempClient.setDob(mDob2);
					}

					mDob = mDob2;
				}
			}
		});

		datePicker.show();
	}
}
