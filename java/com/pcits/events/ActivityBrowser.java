/**
 * File        : ActivityBrowser.java
 * App name    : M1SO Events
 * Version     : 1.1.4
 * Created     : 25/05/14

 * Created by Team M1SO on 21/01/14.
 * Copyright (c) 2014 M1SO. All rights reserved.
 */

package com.pcits.events;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.events.utils.Utils;

public class ActivityBrowser extends FragmentActivity {

	// Create an instance of ActionBar
	private android.app.ActionBar actionbar;

	private WebView web;
	private ProgressBar prgPageLoading;
	private String mGetDealTitle;
	private String url;

	private Utils utils;

	private MenuItem miPrev, miNext;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.actionbar_browser, menu);
		miPrev = (MenuItem) menu.findItem(R.id.abPrevious);
		miNext = (MenuItem) menu.findItem(R.id.abNext);

		return true;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_PROGRESS);
		setContentView(R.layout.activity_browser);

		// Declare object of Utils and userFunction class
		utils = new Utils(this);

		// Get intent Data from ActivityDetail
		Intent i = getIntent();

		url = i.getStringExtra(utils.EXTRA_DEAL_URL);
		mGetDealTitle = i.getStringExtra(utils.EXTRA_DEAL_TITLE);

		// Change actionbar title
		int titleId = Resources.getSystem().getIdentifier("action_bar_title",
				"id", "android");
//		if (0 == titleId)
//			titleId = com.actionbarsherlock.R.id.abs__action_bar_title;

		// Change the title color
		TextView txtActionbarTitle = (TextView) findViewById(titleId);
		txtActionbarTitle.setTextColor(getResources().getColor(
				R.color.actionbar_title_color));

		// Get ActionBar and set back button on actionbar
		actionbar = getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setTitle(Html.fromHtml(mGetDealTitle));

		// Connecct view object and xml ids
		web = (WebView) findViewById(R.id.web);
		prgPageLoading = (ProgressBar) findViewById(R.id.prgPageLoading);

		web.setHorizontalScrollBarEnabled(true);
		web.getSettings().setAllowFileAccess(true);
		web.getSettings().setJavaScriptEnabled(true);
		setProgressBarVisibility(true);

		web.getSettings().setBuiltInZoomControls(true);
		web.getSettings().setUseWideViewPort(true);
		web.setInitialScale(1);

		web.loadUrl(url);

		final Activity act = this;
		web.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView webview, int progress) {

				act.setProgress(progress * 100);
				prgPageLoading.setProgress(progress);

			}

		});

		web.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {

				super.onPageStarted(web, url, favicon);
				prgPageLoading.setVisibility(View.VISIBLE);

			}

			@Override
			public void onPageFinished(WebView view, String url) {

				super.onPageFinished(web, url);

				prgPageLoading.setProgress(0);
				prgPageLoading.setVisibility(View.GONE);

				if (web.canGoBack()) {
					miPrev.setEnabled(true);
					miPrev.setIcon(R.drawable.ic_action_previous_item);
				} else {
					miPrev.setEnabled(false);
					miPrev.setIcon(R.drawable.ic_action_previous_item_disabled);
				}

				if (web.canGoForward()) {
					miNext.setEnabled(true);
					miNext.setIcon(R.drawable.ic_action_next_item);
				} else {
					miNext.setEnabled(false);
					miNext.setIcon(R.drawable.ic_action_next_item_disabled);
				}
			}

			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				Toast.makeText(act, description, Toast.LENGTH_SHORT).show();
			}

		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finish();
		super.onBackPressed();
	}

	// Listener when item selected Menu in action bar
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case android.R.id.home:
			// Previous page or exit
			finish();
			return true;

		case R.id.abPrevious:
			if (web.canGoBack()) {
				web.goBack();
			}
			break;
		case R.id.abNext:
			if (web.canGoForward()) {
				web.goForward();
			}
			break;
		case R.id.abRefresh:
			web.reload();
			break;
		case R.id.abStop:
			web.stopLoading();
			break;
		case R.id.abBrowser:
			Intent iBrowser = new Intent(Intent.ACTION_VIEW);
			iBrowser.setData(Uri.parse(url));
			startActivity(iBrowser);
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

}