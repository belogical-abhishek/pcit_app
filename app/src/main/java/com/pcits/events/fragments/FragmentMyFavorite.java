package com.pcits.events.fragments;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityDetail;
import com.pcits.events.R;
import com.pcits.events.adapters.HomeAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.config.SharedPref;
import com.pcits.events.libraries.JSONParser;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.obj.DealObj;
import com.pcits.events.obj.ListEventObj;
import com.pcits.events.utils.Utils;

public class FragmentMyFavorite extends Fragment {

	// ArrayList<HashMap<String, String>> menuItems;
	private ArrayList<DealObj> mArrDeals;
	private ProgressDialog pDialog;

	// Declare object of userFunctions and Utils class
	private UserFunctions userFunction;
	private Utils utils;

	// Create instance of list and ListAdapter
	private ListView list;
	private TextView lblNoResult;
	private HomeAdapter mla;

	private Button btnLoadMore;
	private LinearLayout lytRetry;

	// flag for current page
	private JSONObject json, jsonCurrency;

	// Declare variable
	public static Activity self;
	public ArrayList<DealObj> arrDeals;
	private PullToRefreshListView mPullRefreshListView;

	private View v;

	// Declare NotBoringActionBar
	public static LinearLayout llMenu;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		v = inflater.inflate(R.layout.fragment_home, container, false);
		try{
			if (mArrDeals == null) {
				mArrDeals = new ArrayList<DealObj>();
			}
	
			// list = (ListView) v.findViewById(R.id.list);
			lblNoResult = (TextView) v.findViewById(R.id.lblNoResult);
			lytRetry = (LinearLayout) v.findViewById(R.id.llRetry);
			userFunction = new UserFunctions();
			utils = new Utils(getActivity());
	
			// menuItems = new ArrayList<HashMap<String, String>>();
	
			mPullRefreshListView = (PullToRefreshListView) v
					.findViewById(R.id.lsvPullToRefresh);
			list = mPullRefreshListView.getRefreshableView();
			// btnRetry = (Button) v.findViewById(R.id.btnRetry);
	
			// Declare object of userFunctions class
	
			// Set a listener to be invoked when the list should be refreshed.
			if (NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
				mPullRefreshListView
						.setOnRefreshListener(new OnRefreshListener<ListView>() {
							@Override
							public void onRefresh(
									PullToRefreshBase<ListView> refreshView) {
	
								String label = DateUtils.formatDateTime(
										getActivity(), System.currentTimeMillis(),
										DateUtils.FORMAT_SHOW_TIME
												| DateUtils.FORMAT_SHOW_DATE
												| DateUtils.FORMAT_ABBREV_ALL);
	
								// Update the LastUpdatedLabel
								refreshView.getLoadingLayoutProxy()
										.setLastUpdatedLabel(label);
								// menuItems = new ArrayList<HashMap<String,
								// String>>();
								new loadFirstListView().execute();
	
							}
						});
			} else {
				Toast.makeText(getActivity(),
						getString(R.string.no_internet_connection),
						Toast.LENGTH_SHORT).show();
			}
	
			// Add an end-of-list listener
			mPullRefreshListView
					.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {
	
						@Override
						public void onLastItemVisible() {
							// Toast.makeText(getActivity(), "end page",
							// Toast.LENGTH_SHORT).show();
						}
					});
	
			// Listener to get selected id when list item clicked
			list.setOnItemClickListener(new OnItemClickListener() {
	
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					// HashMap<String, String> item = new HashMap<String, String>();
					// item = menuItems.get(position - 1);
	
					// Pass id to onListSelected method on HomeActivity
					// mCallback.onListSelected(item.get(userFunction.key_deals_id));
					setStatusPrivateEvent(getActivity(),"Event_List_Status",mArrDeals.get(position - 1)
							.getDeal_id());
					Intent i = new Intent(getActivity(), ActivityDetail.class);
					i.putExtra(utils.EXTRA_DEAL_ID, mArrDeals.get(position - 1)
							.getDeal_id());
					startActivity(i);
					getActivity().overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_left);
	
					// Set the item as checked to be highlighted when in two-pane
					// layout
					list.setItemChecked(position, true);
				}
			});
			return v;
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			return v;
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (GlobalValue.myClient != null) {
			mPullRefreshListView.setVisibility(View.VISIBLE);
		} else {
			mPullRefreshListView.setVisibility(View.GONE);
			lblNoResult.setVisibility(View.VISIBLE);
			lblNoResult.setText("You need login..!");
		}
	}

	// Load first 10 videos
	private class loadFirstListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Showing progress dialog before sending http request
			if (!GlobalValue.prefs
					.getBooleanValue(SharedPref.APPLICATION_INSTALLED)) {
				pDialog = new ProgressDialog(getActivity());
				pDialog.setMessage("Please wait..");
				pDialog.setIndeterminate(true);
				pDialog.setCancelable(false);
				pDialog.show();
			}
		}

		protected Void doInBackground(Void... unused) {
			// try {
			// Thread.sleep(3000);
			// } catch (InterruptedException e) {
			// }
			try{
				if (NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
					getDataFromServer();
				} 
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
            }
			return null;
		}

		protected void onPostExecute(Void unused) {
			// Condition if data length uder 10 button loadMore is remove
			if (isAdded()) {
				if (mArrDeals.size() != 0) {
					// Check paramter notif
					lytRetry.setVisibility(View.GONE);
					list.setVisibility(View.VISIBLE);
					lblNoResult.setVisibility(View.GONE);
					// Getting adapter
					List<String> status=getStringArrayPref(getActivity(),"Event_List_Status",mArrDeals);
					mla = new HomeAdapter(getActivity(), mArrDeals,status);
					mla.notifyDataSetChanged();
					list.setAdapter(mla);
					mPullRefreshListView.onRefreshComplete();

				} else {
					list.removeFooterView(btnLoadMore);
					if (json != null) {
						lblNoResult.setVisibility(View.VISIBLE);
						lytRetry.setVisibility(View.GONE);

					} else {
						lblNoResult.setVisibility(View.GONE);
						lytRetry.setVisibility(View.VISIBLE);
						Toast.makeText(getActivity(),
								getString(R.string.no_connection),
								Toast.LENGTH_SHORT).show();
					}
				}
			}
			try {
				super.onPostExecute(unused);
				// Closing progress dialog
				
				pDialog.dismiss();
			} catch (NullPointerException ex) {
				throw new RuntimeException(ex);
			} catch (Exception ex) {
                throw new RuntimeException(ex);
            }
		}
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		if (pDialog != null) {
			pDialog.dismiss();
			pDialog = null;
		}
	}

	public void getDataFromServer() {

		try {
			jsonCurrency = userFunction.currency(getActivity());
			json = userFunction.myfavorite(GlobalValue.myClient.getUsername(),
					getActivity());
			// }
			Log.e("url", "url: " + json);

			if (json != null) {
				ArrayList<DealObj> arr = JSONParser.parserDeal(
						userFunction.array_latest_deals, json.toString());
				mArrDeals.addAll(arr);
			}
			if (jsonCurrency != null) {
				JSONArray currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);

			}

		} catch (JSONException ex) {
			throw new RuntimeException(ex);
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	private void setStatusPrivateEvent(android.content.Context context, String key,String deal_id)
	{
		//Event_List_Status-key
		try
		{
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
			SharedPreferences.Editor editor = prefs.edit();
			JSONArray a;
			try
			{
				String json = prefs.getString(key, null);
				a = new JSONArray(json);
			}
			catch (Exception e)
			{
				a = new JSONArray();
				e.printStackTrace();
			}

			ListEventObj listEventObj = null;
			JSONObject b=new JSONObject();

			try
			{
				b.put("event_id", deal_id);

			}
			catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			a.put(b);

			if (a!=null)
			{
				editor.putString(key, a.toString());
			}
			else
			{
				editor.putString(key, null);
			}
			editor.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public List<String> getStringArrayPref(Context context, String key, List<DealObj> list)
	{
		List<String> listEventStatus=new ArrayList<>(list.size());
		for (int i = 0; i < list.size(); i++)
		{
			listEventStatus.add("0");
		}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		String json = prefs.getString(key, null);
		List<ListEventObj> listEvents=new ArrayList<>();
		if (json != null)
		{
			try
			{
				JSONArray a = null;
				try
				{
					a = new JSONArray(json);
					for (int i = 0; i < list.size(); i++)
					{
						DealObj ss=list.get(i);
						for(int j=0;j<a.length();j++)
						{
							JSONObject jsonObject=a.getJSONObject(j);
							if(ss.getDeal_id().equalsIgnoreCase(jsonObject.getString("event_id")))
							{
								listEventStatus.set(i,ss.getDeal_id());
							}
							else
							{
								if(listEventStatus.get(i).equalsIgnoreCase("0"))
								{
									listEventStatus.set(i,"0");
								}
							}

						}
					}
				}
				catch (JSONException e)
				{
					e.printStackTrace();
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return listEventStatus;
	}
}
