package com.pcits.events.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import com.pcits.events.ActivityHome;
import com.pcits.events.R;

public class QuestionaireFragment extends Fragment {

	private TextView lblType;
	private View view;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragment_question, container, false);
		initUI();
		initControl();
		return view;
	}

	private void initUI() {
		llMenu = (LinearLayout) view.findViewById(R.id.llMenu);
		lblType = (TextView) view.findViewById(R.id.lblType);
	}

	private void initControl() {
		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
				Intent intent=new Intent(getActivity(),ActivityHome.class);
				getActivity().startActivity(intent);
				getActivity().finish();
			}
		});

		lblType.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				popupType();
			}
		});
	}

	@SuppressLint("NewApi")
	private void popupType() {
		// TODO Auto-generated method stub
		PopupMenu popup = new PopupMenu(getActivity(), lblType);

		popup.getMenuInflater().inflate(R.menu.typequestion, popup.getMenu());

		popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(android.view.MenuItem item) {
				// TODO Auto-generated method stub
				switch (item.getItemId()) {
				case R.id.radio:
					lblType.setText("Radio Button");
					break;
				case R.id.checkbox:
					lblType.setText("Check Box");
					break;
				case R.id.textbox:
					lblType.setText("Text Box");
					break;
				}
				return false;
			}
		});
		popup.show();
	}
}
