package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.pcits.common.utils.Logging;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.obj.MenuObj;
import com.pcits.events.widgets.CircleImageWithBorder;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class MenuDrawerAdapter extends BaseAdapter {

	private static final String TAG = "MenuDrawerAdapter";

	private LayoutInflater mInflater;
	private ArrayList<MenuObj> mArrMenu;
	private int mPosition;
	private boolean mIsCollapse = true;

	private Activity mAct;
	private AQuery mAq;

	public MenuDrawerAdapter(Activity activity, ArrayList<MenuObj> arrMenu) {
		this.mArrMenu = arrMenu;
		mInflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mAct = activity;
		mAq = new AQuery(activity);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		try {
			return mArrMenu.size();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			return 0;
		}
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mArrMenu.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		try {
			Holder holder = null;
			if (convertView == null) {
				holder = new Holder();
				convertView = mInflater.inflate(R.layout.item_menu, null);
				holder.llItem = (LinearLayout) convertView
						.findViewById(R.id.ll_menu_item);
				holder.menuIcon = (ImageView) convertView
						.findViewById(R.id.img_menu_icon);
				holder.lblTitle = (TextViewRobotoCondensedRegular) convertView
						.findViewById(R.id.lbl_menu_title);
				holder.lblTitle.setSelected(true);
				holder.rltSmallHeader = (RelativeLayout) convertView
						.findViewById(R.id.rlt_small_header);
				holder.avatar = (CircleImageWithBorder) convertView
						.findViewById(R.id.circle_avatar);

				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}

			MenuObj menu = mArrMenu.get(position);
			if (menu != null) {
				holder.lblTitle.setText(menu.getTitle());

				// Set small header
				if (menu.getId().equalsIgnoreCase(MenuObj.ID_HOME)) {
					holder.rltSmallHeader.setVisibility(View.VISIBLE);

					AQuery aq = mAq.recycle(convertView);
					// Set avatar
					if (GlobalValue.myClient != null) {
						if (Integer.parseInt(GlobalValue.myClient.getType()) == 0) {
							aq.id(holder.avatar).image(
									UserFunctions.URLAdmin
											+ GlobalValue.myClient.getImage(),
									false, true);
						} else {
							aq.id(holder.avatar).image(
									GlobalValue.myClient.getImage(), false,
									true);
						}
					} else if (GlobalValue.myUser != null) {
						aq.id(holder.avatar).image(
								UserFunctions.URLAdmin
										+ GlobalValue.myUser.getImage(), false,
								true);
					}

				} else {
					holder.rltSmallHeader.setVisibility(View.GONE);
				}

				if (mPosition == position) {
					holder.llItem.setBackgroundColor(mAct.getResources()
							.getColor(R.color.bg_menu_selected));
				} else {
					holder.llItem.setBackgroundColor(mAct.getResources()
							.getColor(android.R.color.transparent));
				}

				if (menu.getIcon() != 0) {
					holder.llItem.setVisibility(View.VISIBLE);
					holder.menuIcon.setVisibility(View.VISIBLE);
					holder.menuIcon.setImageResource(menu.getIcon());
				} else {
					holder.menuIcon.setVisibility(View.INVISIBLE);

					if (mIsCollapse) {
						holder.llItem.setVisibility(View.GONE);
					} else {
						holder.llItem.setVisibility(View.VISIBLE);
					}
				}
			}

			return convertView;
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	class Holder {
		ImageView menuIcon;
		TextViewRobotoCondensedRegular lblTitle;
		LinearLayout llItem;
		RelativeLayout rltSmallHeader;
		CircleImageWithBorder avatar;
	}

	public void setmPosition(int mPosition) {
		this.mPosition = mPosition;
	}

	public void setmIsCollapse(boolean mIsCollapse) {
		this.mIsCollapse = mIsCollapse;
	}
}
