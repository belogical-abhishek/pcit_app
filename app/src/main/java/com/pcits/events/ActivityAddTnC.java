package com.pcits.events;

import org.json.JSONException;
import org.json.JSONObject;

import yuku.ambilwarna.AmbilWarnaDialog;
import yuku.ambilwarna.AmbilWarnaDialog.OnAmbilWarnaListener;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.pcits.common.utils.Logging;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.fragments.AddEventPage4Fragment;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.TnCobj;
import com.pcits.events.widgets.FloatLabeledEditText;
import com.pcits.events.widgets.KenBurnsView;
import com.pcits.events.widgets.richtext.CustomEditText;
import com.pcits.events.widgets.richtext.CustomEditText.EventBack;

public class ActivityAddTnC extends FragmentActivity implements
		OnClickListener, OnAmbilWarnaListener {

	private FloatLabeledEditText txtDesc, txtUser, txtId/* ,txtNotes */;
	// private TextView lblUser;
	private Button btnSubmit;
	private TnCobj tnc = null;
	// Declare NotBoringActionBar
	private LinearLayout llMenu, mLlId;
	private KenBurnsView mHeaderPicture;

	private Bundle mBundle;

	// Rich editor declare
	private CustomEditText txtNotes;
	private LinearLayout llEditor;
	private AmbilWarnaDialog colorPickerDialog;
	private ImageView imgChangeColor;

	private int selectionStart;
	private int selectionEnd;

	private EventBack eventBack = new EventBack() {

		@Override
		public void close() {
			llEditor.setVisibility(View.GONE);
		}

		@Override
		public void show() {
			llEditor.setVisibility(View.VISIBLE);
		}
	};
	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (txtNotes.isFocused()) {
				llEditor.setVisibility(View.VISIBLE);
			}
		}
	};

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_tnc);
		initUI();
		initControl();
		// initData();
		// mCal = Calendar.getInstance();
		// sdf_date = new SimpleDateFormat("yyyy-MM-dd");
		// Declare object of Utils class

		// connect view objects and xml ids

		// Change actionbar title
		// int titleId = Resources.getSystem().getIdentifier("action_bar_title",
		// "id", "android");
		// if (0 == titleId)
		// titleId = com.actionbarsherlock.R.id.abs__action_bar_title;
		//
		// // Change the title color to white
		// TextView txtActionbarTitle = (TextView) findViewById(titleId);
		// txtActionbarTitle.setTextColor(getResources().getColor(
		// R.color.actionbar_title_color));
		//
		// // Get ActionBar and set back button on actionbar
		// actionbar = getSupportActionBar();
		// actionbar.setDisplayHomeAsUpEnabled(true);
		// load ads
		// getWindow().setSoftInputMode(
		// WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		initNotBoringActionBar();
	}

	private void initNotBoringActionBar() {
		mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}

	private void initUI() {
		mLlId = (LinearLayout) findViewById(R.id.ll_tncId);
		txtId = (FloatLabeledEditText) findViewById(R.id.txtId);
		txtId.getEditText().setKeyListener(null);
		txtUser = (FloatLabeledEditText) findViewById(R.id.txtUser);
		txtUser.getEditText().setKeyListener(null);
		if (GlobalValue.myUser != null) {
			txtUser.setText(GlobalValue.myUser.getUsername());
		}
		txtDesc = (FloatLabeledEditText) findViewById(R.id.txtDesc);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);
		llMenu = (LinearLayout) findViewById(R.id.llMenu);
		llMenu.setOnClickListener(this);

		// Init rich editor
		initRichEditor();

		// Init data for updating
		mBundle = getIntent().getExtras();
		if (mBundle != null && mBundle.getString("key")!=null) {
			if (mBundle.getString("key").equalsIgnoreCase("edit")
					&& AddEventPage4Fragment.tncObj != null) {
				mLlId.setVisibility(View.VISIBLE);
				txtId.setText(AddEventPage4Fragment.tncObj.getTnc_id());
				txtUser.setText(GlobalValue.myUser.getUsername());
				txtDesc.setText(AddEventPage4Fragment.tncObj.getTncDesc());
				txtNotes.setTextHTML(AddEventPage4Fragment.tncObj.getNotes());
			}
		}
	}

	private void initRichEditor() {
		txtNotes = (CustomEditText) findViewById(R.id.txtNote);
		colorPickerDialog = new AmbilWarnaDialog(this, Color.BLACK, this);
		ToggleButton boldToggle = (ToggleButton) findViewById(R.id.btnBold);
		ToggleButton italicsToggle = (ToggleButton) findViewById(R.id.btnItalics);
		ToggleButton underlinedToggle = (ToggleButton) findViewById(R.id.btnUnderline);
		imgChangeColor = (ImageView) findViewById(R.id.btnChangeTextColor);
		llEditor = (LinearLayout) findViewById(R.id.lnlAction);
		llEditor.setVisibility(View.VISIBLE);

		txtNotes.setSingleLine(false);
		txtNotes.setMinLines(10);
		txtNotes.setBoldToggleButton(boldToggle);
		txtNotes.setItalicsToggleButton(italicsToggle);
		txtNotes.setUnderlineToggleButton(underlinedToggle);
		txtNotes.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					llEditor.setVisibility(View.VISIBLE);
				} else {
					llEditor.setVisibility(View.GONE);
				}
			}
		});
		txtNotes.setEventBack(eventBack);
		txtNotes.setOnClickListener(clickListener);
		imgChangeColor.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				selectionStart = txtNotes.getSelectionStart();
				selectionEnd = txtNotes.getSelectionEnd();
				colorPickerDialog.show();

				// Close keyboard
				closeKeyboard();
			}
		});
	}

	private void initControl() {
		btnSubmit.setOnClickListener(this);
		llMenu.setOnClickListener(this);
	}

	private String validateForm() {
		// TODO Auto-generated method stub
		String message = "success";
		String desc = txtDesc.getText().toString();
		String notes = txtNotes.getTextHTML();
		String username = txtUser.getText().toString();
		if (desc.isEmpty()) {
			Toast.makeText(this, "Please, input desc", Toast.LENGTH_SHORT)
					.show();
			return message;
		}
		if (notes.isEmpty()) {
			Toast.makeText(this, "Please, input notes", Toast.LENGTH_SHORT)
					.show();
			return message;
		}
		tnc = new TnCobj();
		tnc.setNotes(notes.trim());
		tnc.setTncDesc(desc);
		tnc.setUsername(username.trim());
		return message;
	}

	@Override
	public void onClick(View v) {
		try{
			// TODO Auto-generated method stub
			if (v == llMenu) {
				finish();
			}
			if (v.getId() == R.id.btnSubmit)
			{
				String message = validateForm();
				Log.d("TANDC", "onClick: "+message);
				if (!message.equals("success"))
				{
					Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				}
				else
					{
						if (mBundle != null && mBundle.getString("key")!=null)
						{
							if (mBundle.getString("key").equalsIgnoreCase("edit"))
							{
								editTnC();
							}
						}
						else
							{
								if (tnc != null)
								{
									addTnC(tnc);
								}
							}
	
					}
			}
		}
		catch(Exception ex)
		{
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	private void addTnC(TnCobj tnc) {
		try{
			Log.d("TANDC", "addTnC: ");
			// TODO Auto-generated method stub
			TnCobj data = tnc;
			Log.d("ACCOUNT INFO", "ACCOUNT INFO: " + data.getTncDesc());
			ModelManager.createTnC(this, data, true, new ModelManagerListener() {
	
				@Override
				public void onSuccess(Object object) {
					// TODO Auto-generated method stub
					String strJson = (String) object;
					Toast.makeText(ActivityAddTnC.this, checkResult(strJson),
							Toast.LENGTH_SHORT).show();
					// Intent i = new Intent(ActivitySignUp.this,
					// ActivityLogin.class);
					// startActivity(i);
				}
	
				@Override
				public void onError() {
					// TODO Auto-generated method stub
	
				}
			});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void editTnC() {
		try{
			final String tncDesc = txtDesc.getText().toString();
			final String notes = txtNotes.getText().toString();
	
			if (tncDesc.isEmpty()) {
				Toast.makeText(this, "Please, input desc", Toast.LENGTH_SHORT)
						.show();
				txtDesc.requestFocus();
				return;
			}
			if (notes.isEmpty()) {
				Toast.makeText(this, "Please, input notes", Toast.LENGTH_SHORT)
						.show();
				txtNotes.requestFocus();
				return;
			}
	
			TnCobj tnc = new TnCobj();
			tnc.setTncDesc(tncDesc.trim());
			tnc.setNotes(notes.trim());
	
			// update
			ModelManager.editTnc(this, txtId.getTextString(), tnc, true,
					new ModelManagerListener() {
	
						@Override
						public void onSuccess(Object object) {
							try{
								// TODO Auto-generated method stub
								String json = (String) object;
								boolean result = ParserUtility.updateAccount(json);
								if (result) {
									GlobalValue.dealsObj.setTnc_desc(tncDesc);
									Toast.makeText(ActivityAddTnC.this,
											"Updated successfully!", Toast.LENGTH_SHORT)
											.show();
								}
							} catch (Exception ex) {
								throw new RuntimeException(ex);
							}
						}
	
						@Override
						public void onError() {
							// TODO Auto-generated method stub
	
						}
					});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	protected String checkResult(String strJson) {
		// TODO Auto-generated method stub
		JSONObject json = null;
		String message = "";
		try {
			json = new JSONObject(strJson);
			if (json.getString("status").equals("success")) {
				message = this.getString(R.string.message_success);
				finish();
			} else {
				message = json.getString("message");
			}

		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}

		return message;
	}

	@Override
	public void onCancel(AmbilWarnaDialog dialog) {
		showKeyboard();
	}

	@Override
	public void onOk(AmbilWarnaDialog dialog, int color) {
		txtNotes.setColor(color, selectionStart, selectionEnd);
		imgChangeColor.setBackgroundColor(color);

		showKeyboard();
	}

	private void closeKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if (txtNotes.hasFocus()) {
			imm.hideSoftInputFromWindow(txtNotes.getWindowToken(), 0);
		} else {
			imm.hideSoftInputFromWindow(txtNotes.getWindowToken(), 0);
		}
	}

	private void showKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if (txtNotes.hasFocus()) {
			imm.showSoftInput(txtNotes, InputMethodManager.SHOW_IMPLICIT);
		}
	}
}
