package com.pcits.events.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pcits.events.R;
import com.pcits.events.libraries.UserFunctions;

public class AdapterCompany extends BaseAdapter {
	private Activity activity;
	private ArrayList<HashMap<String, String>> data;
	private LayoutInflater inflater = null;

	// Declare object of userFunctions class
	private UserFunctions userFunction;

	public AdapterCompany(Activity a, ArrayList<HashMap<String, String>> d) {
		activity = a;
		data = d;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		// Declare object of UserFuntion class
		userFunction = new UserFunctions();
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_category, null);
			holder = new ViewHolder();

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// Connect views object and views id on xml
		holder.lblTitle = (TextView) convertView.findViewById(R.id.lblName);
		HashMap<String, String> item = new HashMap<String, String>();
		item = data.get(position);

		// Set data to textview and imageview
		holder.lblTitle
				.setText(item.get(userFunction.key_showlocation_company));
		return convertView;
	}

	// Method to create instance of views
	static class ViewHolder {
		ImageView imgThumbnail, icMarker;
		TextView lblTitle, lblDateEnd, lblAfterDiscValue, lblStartValue;
	}
}
