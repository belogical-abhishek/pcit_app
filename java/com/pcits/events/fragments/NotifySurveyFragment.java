package com.pcits.events.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pcits.events.R;

public class NotifySurveyFragment extends Fragment {
	
	private View v;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 super.onCreateView(inflater, container, savedInstanceState);
		 v = inflater
					.inflate(R.layout.layout_notify_survey, container, false);
		 return v;
	}

}
