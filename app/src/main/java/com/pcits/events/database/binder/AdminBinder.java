package com.pcits.events.database.binder;

import android.database.sqlite.SQLiteStatement;

import com.pcits.events.obj.UserObj;

public class AdminBinder implements ParameterBinder {

	@Override
	public void bind(SQLiteStatement st, Object object) {
		// TODO Auto-generated method stub
		UserObj adminObj = (UserObj) object;
		st.bindString(1, adminObj.getUsername());
		st.bindString(2, adminObj.getPassword());
		st.bindString(3, adminObj.getFname());
		st.bindString(4, adminObj.getLname());
		st.bindString(5, adminObj.getDob());
		st.bindString(6, adminObj.getAddress());
		st.bindString(7, adminObj.getPhone());
		st.bindString(8, adminObj.getCity());
		st.bindString(9, adminObj.getCounty());
		st.bindString(10, adminObj.getPostcode());
		st.bindString(11, adminObj.getCountry());
		st.bindString(12, adminObj.getEmail());
		st.bindString(13, adminObj.getRole());
		st.bindString(14, adminObj.getPayInfo());
		st.bindString(15, adminObj.getStatus());
		st.bindString(16, adminObj.getNotes());
		st.bindString(17, adminObj.getImage());
	}

}
