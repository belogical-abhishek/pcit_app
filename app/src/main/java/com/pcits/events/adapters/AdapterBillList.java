package com.pcits.events.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pcits.events.R;
import com.pcits.events.obj.BillsObj;

import java.util.List;

public class AdapterBillList extends ArrayAdapter<BillsObj> {

    Context mContext;
    List<BillsObj> mList;
    List<String> status;
    String TAG="AdapterBillList";

    public AdapterBillList( Context context, List<BillsObj> list,List<String> status)
    {
        super(context, 0, list);
        mContext = context;
        mList = list;
        this.status=status;
        Log.d(TAG, "AdapterSplitBill: "+list.size());
    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public int getPosition(@Nullable BillsObj item)
    {
        return super.getPosition(item);
    }

    @SuppressLint("ResourceAsColor")
    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflator = LayoutInflater.from(mContext);
        View view  = inflator.inflate(R.layout.adapter_bill_list, null);
        SplitViewHolder viewHolder = new SplitViewHolder();
        viewHolder.tvBill=(TextView)view.findViewById(R.id.tvBill);
        viewHolder.bill_no      = (TextView) view.findViewById(R.id.bill_no);
        viewHolder.bill_amount = (TextView) view.findViewById(R.id.bill_amount);
        viewHolder.linear_ABL=(LinearLayout)view.findViewById(R.id.linear_ABL);

        BillsObj billsObj=mList.get(position);
        viewHolder.tvBill.setText(String.valueOf(position+1));
        viewHolder.bill_no.setText(billsObj.getDescription());
        viewHolder.bill_amount.setText(billsObj.getAmount());

        if(!status.get(position).equalsIgnoreCase("0"))
        {
            viewHolder.linear_ABL.setBackgroundColor(R.color.text_disc);
        }
        return view;
    }

    static class SplitViewHolder
    {
        // protected ImageView plusImage,minusImage;
        protected TextView tvBill,bill_no,bill_amount;//amountShare;
        private LinearLayout linear_ABL;
    }

}

