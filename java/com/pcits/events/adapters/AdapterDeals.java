package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.pcits.events.R;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.obj.DealObj;

public class AdapterDeals extends BaseAdapter {

	private ArrayList<DealObj> arrDeals;
	private LayoutInflater mInflate;
	private Activity mAct;
	private AQuery aq;

	public AdapterDeals(Activity activity, ArrayList<DealObj> listdeals) {
		this.arrDeals = listdeals;
		this.mAct = activity;
		this.mInflate = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrDeals.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {
		// TODO Auto-generated method stub
		final HolderView holder;
		if (convertView == null) {
			holder = new HolderView();
			convertView = mInflate.inflate(R.layout.adapter_home, null);
			holder.lblTitle = (TextView) convertView
					.findViewById(R.id.lblTitle);
			holder.lblCompany = (TextView) convertView
					.findViewById(R.id.lblCompany);
			holder.lblStartValue = (TextView) convertView
					.findViewById(R.id.lblStartValue);
			holder.lblAfterDiscValue = (TextView) convertView
					.findViewById(R.id.lblAfterDiscountValue);
			holder.lblDateEnd = (TextView) convertView
					.findViewById(R.id.lblEndDate);
			holder.lblDateStart = (TextView) convertView
					.findViewById(R.id.lblStartDate);
			holder.lblTimeStart = (TextView) convertView
					.findViewById(R.id.lblStartTime);
			holder.lblTimeEnd = (TextView) convertView
					.findViewById(R.id.lblEndTime);
			holder.lblAttend = (TextView) convertView
					.findViewById(R.id.lblattend);

			holder.imgThumbnail = (ImageView) convertView
					.findViewById(R.id.imgThumbnail);
			holder.icMarker = (ImageView) convertView
					.findViewById(R.id.icMarker);
			convertView.setTag(holder);
		} else {
			holder = (HolderView) convertView.getTag();
		}
		final DealObj o = arrDeals.get(position);
		if (o != null) {
			aq = new AQuery(mAct);
			boolean memCache = false;
			boolean fileCache = true;
			aq.id(holder.imgThumbnail).image(
					UserFunctions.URLAdmin + o.getImage(), memCache, fileCache);
			aq.id(holder.icMarker).image(
					UserFunctions.URLAdmin + o.getCategory_id(), memCache,
					fileCache);
			holder.lblTitle.setText(o.getTitle());
			holder.lblCompany.setText(o.getCompany());
			holder.lblStartValue.setText(Double.toString(o.getSave_value()));
			holder.lblAfterDiscValue.setText(Double.toString(o
					.getAfter_discount_value()));
			holder.lblDateEnd.setText("  " + o.getEnd_date());
			holder.lblDateStart.setText("  " + o.getStart_date());
			holder.lblTimeStart.setText(o.getStart_time());
			holder.lblTimeEnd.setText(o.getEnd_time());
		}
		return convertView;
	}

	public class HolderView {
		ImageView imgThumbnail, icMarker;
		TextView lblTitle, lblCompany, lblDateEnd, lblDateStart,
				lblAfterDiscValue, lblStartValue, lblTimeStart, lblTimeEnd,
				lblAttend;
	}

}
