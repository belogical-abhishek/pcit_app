/**
 * File        : ActivityHome.java
 * App name    : M1SO Events
 * Version     : 1.1.4
 * Created     : 25/05/14
 * <p>
 * Created by Team M1SO on 21/01/14.
 * Copyright (c) 2014 M1SO. All rights reserved.
 */

package com.pcits.events;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import android.*;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.util.AQUtility;

import com.facebook.login.LoginManager;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.drive.Drive;
import com.pcits.common.utils.EventsPreferences;
import com.pcits.common.utils.GsonUtility;
import com.pcits.common.utils.Logging;
import com.pcits.common.utils.RuntimePermissions;
import com.pcits.events.adapters.MenuDrawerAdapter;
import com.pcits.events.adapters.TabsAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.config.MySharedPreferences;
import com.pcits.events.config.ads.Ads;
import com.pcits.events.fragments.DashBoardFragment;
import com.pcits.events.fragments.FragmentAbout;
import com.pcits.events.fragments.FragmentCalenderView;
import com.pcits.events.fragments.FragmentCategory;
import com.pcits.events.fragments.FragmentFeatured;
import com.pcits.events.fragments.FragmentFilter;
import com.pcits.events.fragments.FragmentHelp;
import com.pcits.events.fragments.FragmentHome;
import com.pcits.events.fragments.FragmentListPrivateEvents;
import com.pcits.events.fragments.FragmentLogin;
import com.pcits.events.fragments.FragmentNearMe;
import com.pcits.events.fragments.FragmentPopular;
import com.pcits.events.fragments.FragmentScanBarcodeWiner;
import com.pcits.events.fragments.FragmentTnC;
import com.pcits.events.fragments.MessageFragment;
import com.pcits.events.fragments.QuestionaireFragment;
import com.pcits.events.fragments.ScanTicketsPage1;
import com.pcits.events.fragments.SurveyFragment;
import com.pcits.events.fragments.TicketsFragment;
import com.pcits.events.fragments.UpcomingFragment;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.obj.MenuObj;
import com.pcits.events.services.GPSTracker;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.CircleImageWithBorder;
import com.pcits.events.widgets.PagerSlidingTabStrip;
import com.pcits.events.widgets.RippleView;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedBold;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.pcits.events.utils.AppConstants.RC_LOCATION_PERM;

public class ActivityHome extends FragmentActivity implements OnClickListener,ConnectionCallbacks, OnConnectionFailedListener, NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "ActivityHome";
    private Dialog dialog;
    private FrameLayout frameLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawer;
    private ActionBar mActionBar;
    // Declare object of AdView class
    public static AdView adView;

    // Set argument
    final static String ARG_ID = "id";
    // Declare object of Context and Utils class
    private Context ctx;
    private Utils utils;
    private Bundle bundle;
    private GoogleApiClient mGoogleApiClient;
    // public static LinearLayout lytRetry;

    // Declare NotBoringActionBar
    private RippleView btnLocation, btnSort, btnAddEvents;
    // private KenBurnsView mHeaderPicture;

    private LinearLayout llMenu;
    // private FrameLayout header;
    private RelativeLayout rlHeader;
    // tab view
//    private PagerSlidingTabStrip tabs;
    private TabLayout tabs;
    private ViewPager viewPager;
    // private MyPagerAdapter pagerAdapter;
    private TabsAdapter adapter;

    public static RelativeLayout rltProgress;

    // Declare drawer menu
    private ListView mLvMenu;
    public static DrawerLayout mDrwLayout;
    private MenuDrawerAdapter mMenuAdapter;
    private int currentItem = 0;
    // private ActionBarDrawerToggle mActDrwToggle;
    private ActivityHome self;
    public static boolean accMenuIsShow = false;
    public static int currentMenu = 0;
    public static String currentMenuID = MenuObj.ID_HOME;

    private FragmentHome mFrgHome;
    private FragmentLogin mFrgLogin;
    private FragmentFilter mFrgFilter;
    private FragmentHelp mFrgHelp;
    private FragmentAbout mFrgAbout;
    private TicketsFragment mFrgTickets;
    private DashBoardFragment mFrgDashboard;
    private SurveyFragment mFrgSurvey;
    private ScanTicketsPage1 mFrgScanTickets;
    private FragmentTnC mFrgTnC;
    private FragmentCategory mFrgCategory;
    private QuestionaireFragment mFrgQuestion;
    private FragmentCalenderView mFrgCalender;
    private MessageFragment mFrgMessage;
    private FragmentScanBarcodeWiner mFrgBarcodeWinner;

    public static boolean hasPush = false;
    public static boolean hasNewMessage = false;
    public static String DEAL_ID_TO_PUSH = "";

    private CircleImageWithBorder mImgLogo;
    private AQuery mAq;



    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        //setTheme(R.style.DetailTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        Log.d(TAG, "onCreate: user: "+ GsonUtility.convertObjectToJSONString(GlobalValue.myUser));
        Log.d(TAG, "onCreate: user: "+GsonUtility.convertObjectToJSONString(GlobalValue.myClient));
        ctx = this;
        self = this;
        mAq = new AQuery(self);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }

        try
        {
            Location location = getLastKnownLocation();
            GlobalValue.longitude = location.getLongitude();
            GlobalValue.latitude = location.getLatitude();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        Toast.makeText(this, "Lon : " + GlobalValue.longitude + " Lat : " + GlobalValue.latitude, Toast.LENGTH_LONG).show();

        // Declare object of Utils class;
        utils = new Utils(this);
        initUI();
        // pushNotification();
    }


    private void initUI()
    {
        rlHeader = (RelativeLayout) findViewById(R.id.rl_header);
        frameLayout = (FrameLayout)findViewById(R.id.headerHome);
        tabs = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.pager);
        adapter = new TabsAdapter(getSupportFragmentManager(), this);

        adapter.addFragment(new FragmentHome(),getResources().getString(R.string.AllEVENTS));
        adapter.addFragment(new FragmentNearMe(),getResources().getString(R.string.NEARME));
        adapter.addFragment(new UpcomingFragment(),getResources().getString(R.string.upcoming_caps));
        adapter.addFragment(new FragmentPopular(),getResources().getString(R.string.POPULAR));
        adapter.addFragment(new FragmentListPrivateEvents(),getResources().getString(R.string.PrivateEvents));
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
        final int pageMargin = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                        .getDisplayMetrics());
        viewPager.setPageMargin(pageMargin);
        tabs.setupWithViewPager(viewPager);
        if(MySharedPreferences.getInstance(ActivityHome.this).getStringValue("tab").isEmpty())
            viewPager.setCurrentItem(0);
        else
            viewPager.setCurrentItem(Integer.parseInt(MySharedPreferences.getInstance(ActivityHome.this).getStringValue("tab")));
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.d(TAG, "onR TabSelected: "+tab.getPosition());
                MySharedPreferences.getInstance(ActivityHome.this).putStringValue("tab",""+tab.getPosition());
                Log.d(TAG, "onR Resume: "+MySharedPreferences.getInstance(ActivityHome.this).getStringValue("tab"));


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


      /*  tabs.setViewPager(viewPager);
        tabs.setTextSize(25);
        tabs.setTextColorResource(R.color.white);*/

        // tabs.notifyDataSetChanged();

        rltProgress = (RelativeLayout) findViewById(R.id.rlt_progress);
        rltProgress.setOnClickListener(null);

        mDrwLayout = (DrawerLayout) findViewById(R.id.drawer_league);

        mLvMenu = (ListView) findViewById(R.id.lv_menu);


        // Connect view objects and xml ids
        adView = (AdView) this.findViewById(R.id.adView);
        btnLocation = (RippleView) findViewById(R.id.btnLocation);
        btnSort = (RippleView) findViewById(R.id.btnSort);
        btnAddEvents = (RippleView) findViewById(R.id.btnAddEvents);
        llMenu = (LinearLayout) findViewById(R.id.llMenuHome);
        mImgLogo = (CircleImageWithBorder) findViewById(R.id.img_logo);

        llMenu.setOnClickListener(this);

        btnLocation.setOnClickListener(this);
        btnSort.setOnClickListener(this);
        btnAddEvents.setOnClickListener(this);
        mImgLogo.setOnClickListener(this);

        if (GlobalValue.myClient != null) {
            btnAddEvents.setVisibility(View.GONE);
        } else if (GlobalValue.myUser != null) {
            btnAddEvents.setVisibility(View.GONE);
        } else {
            btnAddEvents.setVisibility(View.GONE);
        }

        // Check parameter overlays
        int paramOverlay = utils.loadPreferences(utils.UTILS_OVERLAY);

        // Condition if app start in the first time overlay will show
        if (paramOverlay != 1) {
            showOverLay();
        }

        bundle = new Bundle();
        bundle.putString(utils.EXTRA_ACTIVITY, utils.EXTRA_ACTIVITY_HOME);

        try {
            // load ads
            //   Ads.loadAds(adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        }catch (Exception e){
            e.printStackTrace();
        }
        //
        MyGoogleAnalyticApplycation.getGaTracker().set(Fields.SCREEN_NAME,
                "Home Screen");

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.pcits.events", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d(TAG,
                        "KeyHash:"
                                + Base64.encodeToString(md.digest(),
                                Base64.DEFAULT));
            }
        } catch (NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }

    private Location getLastKnownLocation()
    {
        LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            @SuppressLint("MissingPermission")
            Location l = locationManager.getLastKnownLocation(provider);

            if (l == null) {
                continue;
            }

            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if(requestCode == 101)
        {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Location location = getLastKnownLocation();
                GlobalValue.longitude = location.getLongitude();
                GlobalValue.latitude = location.getLatitude();

                Toast.makeText(this, "Lon : " + GlobalValue.longitude + " Lat : " + GlobalValue.latitude, Toast.LENGTH_LONG).show();
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void changeFragment(Fragment targetFragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_content, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        targetFragment.setArguments(bundle);
    }

    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    @Override
    protected void onStart()
    {
        // TODO Auto-generated method stub
        super.onStart();
        MyGoogleAnalyticApplycation.getGaTracker().send(
                MapBuilder.createAppView().build());
    }

    @Override
    protected void onResume()
    {
        try
        {
            Log.d(TAG, "onR Resume: "+currentItem);
            Log.d(TAG, "onR Resume: "+MySharedPreferences.getInstance(ActivityHome.this).getStringValue("tab"));
            if(MySharedPreferences.getInstance(ActivityHome.this).getStringValue("tab").isEmpty())
                viewPager.setCurrentItem(0);
            else
                viewPager.setCurrentItem(Integer.parseInt(MySharedPreferences.getInstance(ActivityHome.this).getStringValue("tab")));
            super.onResume();


            frameLayout.setBackgroundColor(ContextCompat.getColor(ActivityHome.this,R.color.colorPrimary));
          //  adView.setBackgroundColor(ContextCompat.getColor(ActivityHome.this,R.color.colorPrimary));
          //  initUI();
            // Set logo by avatar
            if (GlobalValue.myClient != null) {
                mAq.id(mImgLogo).image(
                        UserFunctions.URLAdmin
                                + GlobalValue.myClient.getImage(), false, true);
            } else if (GlobalValue.myUser != null) {
                mAq.id(mImgLogo).image(
                        UserFunctions.URLAdmin + GlobalValue.myUser.getImage(),
                        false, true);
            } else {
                mImgLogo.setImageResource(R.drawable.eventslogo);
            }

            // Load/reload main menu
            if (GlobalValue.myUser != null) {
                if (GlobalValue.myUser.getRole().equals("0")) {
                    createMenuDrawer(menuSuperAdmin());
                } else if (GlobalValue.myUser.getRole().equals("1")) {
                    createMenuDrawer(menuAdmin());
                }
            } else if (GlobalValue.myClient != null) {
                createMenuDrawer(menuUser());
            } else {
                createMenuDrawer(menuNotLoggedIn());
            }

            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addApi(Drive.API).addScope(Drive.SCOPE_FILE)
                        .addScope(Drive.SCOPE_APPFOLDER)
                        // required for App Folder sample
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this).build();
            }
            mGoogleApiClient.connect();

            // Must call after initial main menu.
            pushNotification();
        } catch (Exception ex) {
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
    }


    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

        if (isTaskRoot()) {
            AQUtility.cleanCacheAsync(ctx);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == llMenu) {
            mDrwLayout.openDrawer(Gravity.LEFT);
            return;
        } else if (view == mImgLogo) {
            if (GlobalValue.myClient != null) {
                if (hasNewMessage) {
                    currentMenuID = MenuObj.ID_MESSAGE;

                    // Get current position
                    currentMenu = 3;
                    mMenuAdapter.setmPosition(currentMenu);
                    clickOnMessage();

                    hasNewMessage = false;
                } else {
                    // currentMenuID = MenuObj.ID_PROFILE;

                    // Get current position
                    // currentMenu = 2;
                    // mMenuAdapter.setmPosition(currentMenu);
                    clickOnProfileMenu();
                }
            } else {
                mDrwLayout.openDrawer(Gravity.LEFT);
            }
            return;
        }

        switch (view.getId()) {
            case R.id.overlayLayout:
                utils.savePreferences(utils.UTILS_OVERLAY, 1);
                dialog.dismiss();
                break;
            case R.id.btnLocation:
                // Call ActivityPlaceAroundYou
                Intent iCart = new Intent(this, ActivityAroundYou.class);
                startActivity(iCart);

                break;
            case R.id.btnSort:
                // popupMenu();
                break;
            case R.id.btnAddEvents:
                if (GlobalValue.myUser != null) {
                    if (Integer.parseInt(GlobalValue.myUser.getStatus()) == 1) {
                    } else {
                        Toast.makeText(this,
                                "Account yet get activation, Please waiting...",
                                Toast.LENGTH_SHORT).show();
                    }
                }
                else
                    {
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.app_name)
                            .setMessage(
                                    "You need login account 'Event Organiser' to create events..!")
                            .setPositiveButton(android.R.string.ok,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface arg0,
                                                            int arg1) {
                                            GlobalValue.myUser = null;
                                            changeFragment(new FragmentLogin());
                                        }
                                    })
                            .setNegativeButton(android.R.string.cancel, null)
                            .create().show();
                }
                break;
            default:
                break;
        }

    }



    @Override
    public void onBackPressed() {
        if (currentMenuID.equalsIgnoreCase(MenuObj.ID_HOME)) {
            clickOnExitMenu();
        } else {
            currentMenu = 0;
            mMenuAdapter.setmPosition(currentMenu);
            currentMenuID = MenuObj.ID_HOME;
            clickOnHomeMenu();

            mMenuAdapter.notifyDataSetChanged();
        }
    }

    // Method is used to show overlay when apps starting in the first time.
    private void showOverLay() {

        dialog = new Dialog(ActivityHome.this,
                android.R.style.Theme_Translucent_NoTitleBar);

        dialog.setContentView(R.layout.overlay_view);

        LinearLayout layout = (LinearLayout) dialog
                .findViewById(R.id.overlayLayout);
        layout.setOnClickListener(this);
        dialog.show();

    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.d(TAG, "onR PostResume: "+currentItem);
        Log.d(TAG, "onR Resume: "+MySharedPreferences.getInstance(ActivityHome.this).getStringValue("tab"));
        if(MySharedPreferences.getInstance(ActivityHome.this).getStringValue("tab").isEmpty())
            viewPager.setCurrentItem(0);
        else
            viewPager.setCurrentItem(Integer.parseInt(MySharedPreferences.getInstance(ActivityHome.this).getStringValue("tab")));
    }

    @Override
    protected void onPause() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onPause();

    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onConnected(Bundle arg0) {
        // TODO Auto-generated method stub
        Log.i(TAG, "GoogleApiClient connected");
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub
        Log.i(TAG, "GoogleApiClient connection suspended");
    }

    private void createMenuDrawer(final ArrayList<MenuObj> arrMenu) {
        try {
            // Set menu
            mMenuAdapter = new MenuDrawerAdapter(this, arrMenu);
            // Set highlight
            mMenuAdapter.setmPosition(currentMenu);
            // Check whether acc menu is opening or closed.
            mMenuAdapter.setmIsCollapse(!accMenuIsShow);

            mLvMenu.setAdapter(mMenuAdapter);
            mLvMenu.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // Open page
                    if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_HOME)) {
                        // Get menu id
                        currentMenuID = MenuObj.ID_HOME;

                        // Get current position
                        currentMenu = position;
                        mMenuAdapter.setmPosition(position);
                        clickOnHomeMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_LOGIN)) {
                        // Get menu id
                        currentMenuID = MenuObj.ID_LOGIN;

                        // Get current position
                        currentMenu = position;
                        mMenuAdapter.setmPosition(position);
                        clickOnLoginMenu();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_FILTER)) {
                        // Get menu id
                        currentMenuID = MenuObj.ID_FILTER;

                        // Get current position
                        currentMenu = position;
                        mMenuAdapter.setmPosition(position);
                        clickOnFilterMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_SETTINGS)) {
                        clickOnSettingsMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_HELP)) {
                        // Get menu id
                        currentMenuID = MenuObj.ID_HELP;

                        // Get current position
                        currentMenu = position;
                        mMenuAdapter.setmPosition(position);
                        clickOnHelpMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_ABOUT)) {
                        // Get menu id
                        currentMenuID = MenuObj.ID_ABOUT;

                        // Get current position
                        currentMenu = position;
                        mMenuAdapter.setmPosition(position);
                        clickOnAboutMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_EXIT)) {
                        clickOnExitMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_LOGOUT)) {
                        clickOnLogoutMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_PROFILE)) {
                        // Get menu id
                        // currentMenuID = MenuObj.ID_PROFILE;

                        // Get current position
                        // currentMenu = position;
                        // mMenuAdapter.setmPosition(position);
                        clickOnProfileMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    }
                    else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_MESSAGE)) {

                        currentMenuID = MenuObj.ID_MESSAGE;

                        // Get current position
                        currentMenu = position;
                        mMenuAdapter.setmPosition(position);
                        clickOnMessage();
                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_MY_TICKETS)) {
                        // Get menu id
                        currentMenuID = MenuObj.ID_MY_TICKETS;

                        // Get current position
                        currentMenu = position;
                        mMenuAdapter.setmPosition(position);
                        clickOnMyTicketsMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_DASHBOARD)) {
                        // Get menu id
                        currentMenuID = MenuObj.ID_DASHBOARD;

                        // Get current position
                        currentMenu = position;
                        mMenuAdapter.setmPosition(position);
                        clickOnDashboardMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_EVENT)) {
                        // Get menu id
                        // currentMenuID = MenuObj.ID_EVENT;

                        // Get current position
                        // currentMenu = position;
                        // mMenuAdapter.setmPosition(position);
                        clickOnEventMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();

                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_Private_EVENT)) {
                        // Get menu id
                         currentMenuID = MenuObj.ID_Private_EVENT;

                        // Get current position
                         currentMenu = position;
                         mMenuAdapter.setmPosition(position);
                        clickOnPrivateEventMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    }
//                    else if (arrMenu.get(position).getId()
//                            .equalsIgnoreCase(MenuObj.ID_LIST_EVENT)) {
//                        // Get menu id
//                        currentMenuID = MenuObj.ID_LIST_EVENT;
//
//                        // Get current position
//                        currentMenu = position;
//                        mMenuAdapter.setmPosition(position);
//                        clickOnListPrivateEvent();
//
//                        // Close menu
//                        mDrwLayout.closeDrawers();
//                    }
                    else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_CREAT_SURVEY)) {
                        // Get menu id
                        currentMenuID = MenuObj.ID_CREAT_SURVEY;

                        // Get current position
                        currentMenu = position;
                        // Coding to create survey here
                        mMenuAdapter.setmPosition(position);
                        clickOnSurvey();
                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_SCAN_TICKET)) {
                        // Get menu id
                        currentMenuID = MenuObj.ID_SCAN_TICKET;

                        // Get current position
                        currentMenu = position;
                        mMenuAdapter.setmPosition(position);
                        clickOnScanTicketsMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_SCAN_WINNER)) {
                        // Get menu id
                        currentMenuID = MenuObj.ID_SCAN_WINNER;

                        // Get current position
                        currentMenu = position;
                        mMenuAdapter.setmPosition(position);
                        clickOnScanWinnerMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_PROMOTE_EVENT)) {
                        // Coding to promote event here

                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_CALENDAR)) {
                        // Get menu id
                        currentMenuID = MenuObj.ID_CALENDAR;

                        // Coding to view calendar here
                        currentMenu = position;
                        mMenuAdapter.setmPosition(position);
                        clickOnCalender();
                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_TNC)) {
                        // Get menu id
                        currentMenuID = MenuObj.ID_TNC;

                        // Get current position
                        currentMenu = position;
                        mMenuAdapter.setmPosition(position);
                        clickOnTnCMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_CATEGORY)) {
                        // Get menu id
                        currentMenuID = MenuObj.ID_CATEGORY;

                        // Get current position
                        currentMenu = position;
                        mMenuAdapter.setmPosition(position);
                        clickOnCategoryMenu();

                        // Close menu
                        mDrwLayout.closeDrawers();
                    } else if (arrMenu.get(position).getId()
                            .equalsIgnoreCase(MenuObj.ID_QUESTIONAIRE)) {
                        // Get menu id
                        currentMenuID = MenuObj.ID_QUESTIONAIRE;

                        // Get current position
                        currentMenu = position;
                        // Coding to open questionair page here
                        mMenuAdapter.setmPosition(position);
                        clickOnQuestionMenu();
                        // Close menu
                        mDrwLayout.closeDrawers();
                    }

                    // Highlight menu item.
                    mMenuAdapter.notifyDataSetChanged();

                }
            });
        } catch (Exception ex) {
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
    }

    private void clickOnScanTicketsMenu() {
        tabs.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        rlHeader.setVisibility(View.GONE);
        if (mFrgScanTickets == null) {
            mFrgScanTickets = new ScanTicketsPage1();
        }
        try {
            changeFragment(mFrgScanTickets);
        } catch (IllegalStateException ex) {
        }
    }

    // Declare menu
    private ArrayList<MenuObj> menuNotLoggedIn() {
        ArrayList<MenuObj> arrMenu = new ArrayList<MenuObj>();
        MenuObj menu = new MenuObj(MenuObj.ID_HOME, getResources().getString(
                R.string.menu_home), R.drawable.ic_latest);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_LOGIN, getResources().getString(
                R.string.menu_login), R.drawable.ic_action_user);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_FILTER, getResources().getString(
                R.string.menu_filter), R.drawable.ic_filter);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_SETTINGS, getResources().getString(
                R.string.menu_settings), R.drawable.ic_settings);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_HELP, getResources().getString(
                R.string.menu_help), R.drawable.ic_action_help);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_ABOUT, getResources().getString(
                R.string.menu_about), R.drawable.ic_about);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_EXIT, getResources().getString(
                R.string.menu_exit), R.drawable.ic_action_cancel);
        arrMenu.add(menu);
        return arrMenu;
    }

    private ArrayList<MenuObj> menuUser() {
        ArrayList<MenuObj> arrMenu = new ArrayList<MenuObj>();
        MenuObj menu = new MenuObj(MenuObj.ID_HOME, getResources().getString(
                R.string.menu_home), R.drawable.ic_latest);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_LOGIN,
                GlobalValue.myClient.getUsername(), R.drawable.ic_action_user);
        arrMenu.add(menu);
        menu=new MenuObj(MenuObj.ID_Private_EVENT,getResources().getString(
                R.string.menu__private_event),0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_PROFILE, getResources().getString(
                R.string.menu_profile), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_MESSAGE, getResources().getString(
                R.string.menu_message), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_MY_TICKETS, getResources().getString(
                R.string.menu_my_tickets), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_FILTER, getResources().getString(
                R.string.menu_filter), R.drawable.ic_filter);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_SETTINGS, getResources().getString(
                R.string.menu_settings), R.drawable.ic_settings);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_HELP, getResources().getString(
                R.string.menu_help), R.drawable.ic_action_help);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_ABOUT, getResources().getString(
                R.string.menu_about), R.drawable.ic_about);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_LOGOUT, getResources().getString(
                R.string.menu_logout), R.drawable.ic_logout);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_EXIT, getResources().getString(
                R.string.menu_exit), R.drawable.ic_action_cancel);
        arrMenu.add(menu);
        return arrMenu;
    }

    private ArrayList<MenuObj> menuSuperAdmin() {
        ArrayList<MenuObj> arrMenu = new ArrayList<MenuObj>();
        MenuObj menu = new MenuObj(MenuObj.ID_HOME, getResources().getString(
                R.string.menu_home), R.drawable.ic_latest);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_LOGIN, GlobalValue.myUser.getUsername(),
                R.drawable.ic_action_user);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_PROFILE, getResources().getString(
                R.string.menu_profile), 0);
        arrMenu.add(menu);

        menu = new MenuObj(MenuObj.ID_DASHBOARD, getResources().getString(
                R.string.menu_dashboard), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_EVENT, getResources().getString(
                R.string.menu_event), 0);
        arrMenu.add(menu);

        menu = new MenuObj(MenuObj.ID_CREAT_SURVEY, getResources().getString(
                R.string.menu_create_survey), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_SCAN_TICKET, getResources().getString(
                R.string.menu_scan_ticket), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_SCAN_WINNER, getResources().getString(
                R.string.menu_scan_winner), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_PROMOTE_EVENT, getResources().getString(
                R.string.menu_promote_event), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_CALENDAR, getResources().getString(
                R.string.menu_calendar_view), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_TNC, getResources().getString(
                R.string.menu_tnc), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_CATEGORY, getResources().getString(
                R.string.menu_category), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_QUESTIONAIRE, getResources().getString(
                R.string.menu_questionaire), 0);
        arrMenu.add(menu);

        menu = new MenuObj(MenuObj.ID_FILTER, getResources().getString(
                R.string.menu_filter), R.drawable.ic_filter);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_SETTINGS, getResources().getString(
                R.string.menu_settings), R.drawable.ic_settings);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_HELP, getResources().getString(
                R.string.menu_help), R.drawable.ic_action_help);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_ABOUT, getResources().getString(
                R.string.menu_about), R.drawable.ic_about);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_LOGOUT, getResources().getString(
                R.string.menu_logout), R.drawable.ic_logout);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_EXIT, getResources().getString(
                R.string.menu_exit), R.drawable.ic_action_cancel);
        arrMenu.add(menu);
        return arrMenu;
    }

    private ArrayList<MenuObj> menuAdmin() {
        ArrayList<MenuObj> arrMenu = new ArrayList<MenuObj>();
        MenuObj menu = new MenuObj(MenuObj.ID_HOME, getResources().getString(
                R.string.menu_home), R.drawable.ic_latest);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_LOGIN, GlobalValue.myUser.getUsername(),
                R.drawable.ic_action_user);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_PROFILE, getResources().getString(
                R.string.menu_profile), 0);
        arrMenu.add(menu);

        menu = new MenuObj(MenuObj.ID_DASHBOARD, getResources().getString(
                R.string.menu_dashboard), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_EVENT, getResources().getString(
                R.string.menu_event), 0);
        arrMenu.add(menu);

        menu = new MenuObj(MenuObj.ID_CREAT_SURVEY, getResources().getString(
                R.string.menu_create_survey), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_SCAN_TICKET, getResources().getString(
                R.string.menu_scan_ticket), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_SCAN_WINNER, getResources().getString(
                R.string.menu_scan_winner), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_PROMOTE_EVENT, getResources().getString(
                R.string.menu_promote_event), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_CALENDAR, getResources().getString(
                R.string.menu_calendar_view), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_TNC, getResources().getString(
                R.string.menu_tnc), 0);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_QUESTIONAIRE, getResources().getString(
                R.string.menu_questionaire), 0);
        arrMenu.add(menu);

        menu = new MenuObj(MenuObj.ID_FILTER, getResources().getString(
                R.string.menu_filter), R.drawable.ic_filter);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_SETTINGS, getResources().getString(
                R.string.menu_settings), R.drawable.ic_settings);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_HELP, getResources().getString(
                R.string.menu_help), R.drawable.ic_action_help);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_ABOUT, getResources().getString(
                R.string.menu_about), R.drawable.ic_about);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_LOGOUT, getResources().getString(
                R.string.menu_logout), R.drawable.ic_logout);
        arrMenu.add(menu);
        menu = new MenuObj(MenuObj.ID_EXIT, getResources().getString(
                R.string.menu_exit), R.drawable.ic_action_cancel);
        arrMenu.add(menu);
        return arrMenu;
    }

    // Menu click listeners
    private void clickOnHomeMenu() {
        tabs.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.VISIBLE);
        rlHeader.setVisibility(View.VISIBLE);
        if (mFrgHome == null) {
            mFrgHome = new FragmentHome();
        }
        try {
            changeFragment(mFrgHome);
        } catch (IllegalStateException ex) {
        }
    }

    private void clickOnLoginMenu() {
        if (GlobalValue.myUser != null) {
            collapseAccMenu();
        } else if (GlobalValue.myClient != null) {
            collapseAccMenu();
        } else {
            // Close main menu
            mDrwLayout.closeDrawers();

            tabs.setVisibility(View.GONE);
            viewPager.setVisibility(View.GONE);
            rlHeader.setVisibility(View.GONE);
            if (mFrgLogin == null) {
                mFrgLogin = new FragmentLogin();
            }
            try {
                changeFragment(mFrgLogin);
            } catch (IllegalStateException ex) {
            }
        }
    }

    private void collapseAccMenu() {
        if (accMenuIsShow) {
            mMenuAdapter.setmIsCollapse(true);
            accMenuIsShow = false;
        } else {
            mMenuAdapter.setmIsCollapse(false);
            accMenuIsShow = true;
        }
    }

    private void clickOnFilterMenu() {
        tabs.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        rlHeader.setVisibility(View.GONE);
        if (mFrgFilter == null) {
            mFrgFilter = new FragmentFilter();
        }
        try {
            changeFragment(mFrgFilter);
        } catch (IllegalStateException ex) {
        }
    }

    private void clickOnSettingsMenu() {
        Intent i = new Intent(this, ActivitySetting.class);
        startActivity(i);
    }

    private void clickOnHelpMenu() {
        tabs.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        rlHeader.setVisibility(View.GONE);
        if (mFrgHelp == null) {
            mFrgHelp = new FragmentHelp();
        }
        try {
            changeFragment(mFrgHelp);
        } catch (IllegalStateException ex) {
        }
    }

    private void clickOnAboutMenu() {
        tabs.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        rlHeader.setVisibility(View.GONE);
        if (mFrgAbout == null) {
            mFrgAbout = new FragmentAbout();
        }
        try {
            changeFragment(mFrgAbout);
        } catch (IllegalStateException ex) {
        }
    }

    private void clickOnProfileMenu() {
        // Open profile activity.
        startActivity(new Intent(self, ProfileActivity.class));
    }

    private void clickOnQuestionMenu() {
        tabs.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        rlHeader.setVisibility(View.GONE);
        /*if (mFrgQuestion == null) {
            mFrgQuestion = new QuestionaireFragment();
        }*/
        SurveyFragment s = new SurveyFragment();
        try {
            changeFragment(s);
        } catch (IllegalStateException ex) {
        }
    }

    private void clickOnCalender() {
        tabs.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        rlHeader.setVisibility(View.GONE);
        if (mFrgCalender == null) {
            mFrgCalender = new FragmentCalenderView();
        }
        try {
            changeFragment(mFrgCalender);
        } catch (IllegalStateException ex) {
        }
    }

    private void clickOnMessage() {
        tabs.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        rlHeader.setVisibility(View.GONE);
        if (mFrgMessage == null) {
            mFrgMessage = new MessageFragment();
        }
        try {
            changeFragment(mFrgMessage);
        } catch (IllegalStateException ex) {
        }
    }

    private void clickOnExitMenu() {
        final Dialog dialog = new Dialog(self);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirmation);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextViewRobotoCondensedBold title = (TextViewRobotoCondensedBold) dialog
                .findViewById(R.id.dialog_title);
        TextViewRobotoCondensedRegular message = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.dialog_message);

        title.setText(self.getResources().getString(R.string.exit_event_app));
        message.setText(self.getResources().getString(
                R.string.do_you_want_to_exit));

        Button btnNagative = (Button) dialog.findViewById(R.id.btn_nagative);
        Button btnPositive = (Button) dialog.findViewById(R.id.btn_positive);

        btnNagative.setText(self.getResources().getString(R.string.no));
        btnPositive.setText(self.getResources().getString(R.string.yes));

        btnNagative.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnPositive.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
//					Session session = Session.getActiveSession();
//					// run the closeAndClearTokenInformation
//					// which does the following
//					// cache related to the Session.
//					session.closeAndClearTokenInformation();
                } catch (NullPointerException ex) {
                    ex.toString();
                }

                // Dismiss the dialog for first.
                dialog.dismiss();

                // Kill the app.
                finish();
                // Intent i = new Intent(Intent.ACTION_MAIN);
                // i.addCategory(Intent.CATEGORY_HOME);
                // startActivity(i);
            }
        });

        dialog.show();

    }

    private void clickOnLogoutMenu() {
        final Dialog dialog = new Dialog(self);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirmation);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextViewRobotoCondensedBold title = (TextViewRobotoCondensedBold) dialog
                .findViewById(R.id.dialog_title);
        TextViewRobotoCondensedRegular message = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.dialog_message);

        title.setText(self.getResources().getString(R.string.menu_logout));
        message.setText(self.getResources().getString(
                R.string.do_you_want_to_log_out));

        Button btnNagative = (Button) dialog.findViewById(R.id.btn_nagative);
        Button btnPositive = (Button) dialog.findViewById(R.id.btn_positive);

        btnNagative.setText(self.getResources().getString(R.string.no));
        btnPositive.setText(self.getResources().getString(R.string.yes));

        btnNagative.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnPositive.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                try {
                    LoginManager.getInstance().logOut();
                }catch (Exception ex){

                }
                // Reset account.
                GlobalValue.myClient = null;
                GlobalValue.myUser = null;
                MySharedPreferences sharedPreferences = MySharedPreferences.getInstance(ActivityHome.this);
                sharedPreferences.putStringValue(MySharedPreferences.CLIENT_USER_KEY,"");
                sharedPreferences.putStringValue(MySharedPreferences.ADMIN_USER_KEY,"");
                // find the active session which can only be
                // facebook in my app
//				Session session = Session.getActiveSession();
//				// run the closeAndClearTokenInformation which does
//				// the
//				// cache related to the Session.
//				session.closeAndClearTokenInformation();
                // Dismiss the dialog
                dialog.dismiss();
                // Log out.
                finish();
                Intent i = new Intent(ActivityHome.this, ActivityHome.class);
                startActivity(i);
            }
        });

        dialog.show();

    }

    private void clickOnScanWinnerMenu() {
        tabs.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        rlHeader.setVisibility(View.GONE);
        if (mFrgBarcodeWinner == null) {
            mFrgBarcodeWinner = new FragmentScanBarcodeWiner();
        }
        try {
            changeFragment(mFrgBarcodeWinner);
        } catch (IllegalStateException ex) {
        }

    }

    private void clickOnMyTicketsMenu() {
        tabs.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        rlHeader.setVisibility(View.GONE);
        if (mFrgTickets == null) {
            mFrgTickets = new TicketsFragment();
        }
        try {
            changeFragment(mFrgTickets);
        } catch (IllegalStateException ex) {
        }
    }

    private void clickOnDashboardMenu() {
        tabs.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        rlHeader.setVisibility(View.GONE);
        if (mFrgDashboard == null) {
            mFrgDashboard = new DashBoardFragment();
        }
        try {
            changeFragment(mFrgDashboard);
        } catch (IllegalStateException ex) {
        }
    }

    private void clickOnEventMenu() {
        // tabs.setVisibility(View.GONE);
        // viewPager.setVisibility(View.GONE);
        // rlHeader.setVisibility(View.GONE);
        // if (mFrgEvent == null) {
        // mFrgEvent = new FragmentEvent();
        // }
        // try {F
        // changeFragment(mFrgEvent);
        // } catch (IllegalStateException ex) {
        // }

        Intent i = new Intent(self, AddEventActivity.class);
        startActivity(i);
    }
    private void clickOnPrivateEventMenu() {
        // tabs.setVisibility(View.GONE);
        // viewPager.setVisibility(View.GONE);
        // rlHeader.setVisibility(View.GONE);
        // if (mFrgEvent == null) {
        // mFrgEvent = new FragmentEvent();
        // }
        // try {
        // changeFragment(mFrgEvent);
        // } catch (IllegalStateException ex) {
        // }
        EventsPreferences.clearEventKey(ActivityHome.this);
        Intent i = new Intent(self, AddPrivateEventActivity.class);
        startActivity(i);
    }
    private void clickOnListPrivateEvent() {
        // tabs.setVisibility(View.GONE);
        // viewPager.setVisibility(View.GONE);
        // rlHeader.setVisibility(View.GONE);
        // if (mFrgEvent == null) {
        // mFrgEvent = new FragmentEvent();
        // }
        // try {
        // changeFragment(mFrgEvent);
        // } catch (IllegalStateException ex) {
        // }

        Intent i = new Intent(self, ActivityListEvents.class);
        startActivity(i);
    }
    private void clickOnSurvey() {
        tabs.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        rlHeader.setVisibility(View.GONE);
        if (mFrgSurvey == null) {
            mFrgSurvey = new SurveyFragment();
        }
        try {
            changeFragment(mFrgSurvey);
        } catch (IllegalStateException ex) {
        }
    }

    private void clickOnTnCMenu() {
        tabs.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        rlHeader.setVisibility(View.GONE);
        if (mFrgTnC == null) {
            mFrgTnC = new FragmentTnC();
        }
        try {
            changeFragment(mFrgTnC);
        } catch (IllegalStateException ex) {
        }
    }

    private void clickOnCategoryMenu() {
        tabs.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        rlHeader.setVisibility(View.GONE);
        if (mFrgCategory == null) {
            mFrgCategory = new FragmentCategory();
        }
        try {
            changeFragment(mFrgCategory);
        } catch (IllegalStateException ex) {
        }
    }

    public void sortEvents(String type) {
        // boolean isSorted = false;
        // BaseAdapter adapter = null;
        // ArrayList<DealObj> arr = new ArrayList<DealObj>();
        //
        // if (viewPager.getCurrentItem() == 0) {
        // isSorted = true;
        // adapter = FragmentFeatured.mla;
        // arr = FragmentFeatured.mArrDeals;
        // } else if (viewPager.getCurrentItem() == 1) {
        // isSorted = true;
        // adapter = FragmentHome.mla;
        // arr = FragmentHome.mArrDeals;
        // } else if (viewPager.getCurrentItem() == 2) {
        // isSorted = true;
        // adapter = FragmentPopular.mla;
        // arr = FragmentPopular.mArrDeals;
        // }
        //
        // if (isSorted) {
        // if (type.equals(ActivityHome.SORT_BY_DATE_ASC)) {
        // Collections.sort(arr, new Comparator<DealObj>() {
        //
        // @Override
        // public int compare(DealObj lhs, DealObj rhs) {
        // // TODO Auto-generated method stub
        // Integer date1 = Integer.parseInt(lhs
        // .getStartTimeStamp());
        // Integer date2 = Integer.parseInt(rhs
        // .getStartTimeStamp());
        // return date1.compareTo(date2);
        // }
        //
        // });
        // } else if (type.equals(ActivityHome.SORT_BY_DATE_DESC)) {
        // Collections.sort(arr, new Comparator<DealObj>() {
        //
        // @Override
        // public int compare(DealObj lhs, DealObj rhs) {
        // // TODO Auto-generated method stub
        // Integer date1 = Integer.parseInt(lhs
        // .getStartTimeStamp());
        // Integer date2 = Integer.parseInt(rhs
        // .getStartTimeStamp());
        // return date2.compareTo(date1);
        // }
        //
        // });
        // } else if (type.equals(ActivityHome.SORT_BY_CREATETED_ASC)) {
        // Collections.sort(arr, new Comparator<DealObj>() {
        //
        // @Override
        // public int compare(DealObj lhs, DealObj rhs) {
        // // TODO Auto-generated method stub
        // Integer date1 = Integer.parseInt(lhs.getDeal_id()
        // .substring(1));
        // Integer date2 = Integer.parseInt(rhs.getDeal_id()
        // .substring(1));
        // return date1.compareTo(date2);
        // }
        //
        // });
        // } else if (type.equals(ActivityHome.SORT_BY_CREATETED_DESC)) {
        // Collections.sort(arr, new Comparator<DealObj>() {
        //
        // @Override
        // public int compare(DealObj lhs, DealObj rhs) {
        // // TODO Auto-generated method stub
        // Integer date1 = Integer.parseInt(lhs.getDeal_id()
        // .substring(1));
        // Integer date2 = Integer.parseInt(rhs.getDeal_id()
        // .substring(1));
        // return date2.compareTo(date1);
        // }
        //
        // });
        // }
        // adapter.notifyDataSetChanged();
        // } else {
        // Toast.makeText(self, "Sort function not supported on this page",
        // Toast.LENGTH_SHORT).show();
        // }
    }

    private void pushNotification() {
        if (hasPush) {
            // String pushCode = b.getString("code");
            Log.e(TAG, "Push when the app is on: " + DEAL_ID_TO_PUSH);
            if (DEAL_ID_TO_PUSH.equals("winner")) {
                if (GlobalValue.myClient != null) {
                    currentMenuID = MenuObj.ID_MESSAGE;

                    // Get current position(Set by 3 because Message menu is 3
                    // index - Need to change if menu order is changed)
                    currentMenu = 3;
                    mMenuAdapter.setmPosition(3);

                    clickOnMessage();
                } else {
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.app_name)
                            .setMessage("You need login..!")
                            .setPositiveButton(android.R.string.ok,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface arg0, int arg1) {
                                            GlobalValue.myUser = null;
                                            GlobalValue.checkLogin = 1;

                                            FragmentLogin.hasPush = true;
                                            clickOnLoginMenu();
                                        }
                                    })
                            .setNegativeButton(android.R.string.cancel, null)
                            .create().show();
                }
            } else {
                if (!DEAL_ID_TO_PUSH.isEmpty()) {
                //    Intent i = new Intent(this, ActivityDetail.class);
                    Intent i = new Intent(this, ExpandableToolBarActivity.class);
                    i.putExtra(utils.EXTRA_DEAL_ID, DEAL_ID_TO_PUSH);
                    startActivity(i);
                } else if (DEAL_ID_TO_PUSH.isEmpty()) {
                    Log.e(TAG, "Null");
                }
            }

            hasPush = false;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: "+requestCode+" "+resultCode+" ");

        if (requestCode == 1 && resultCode == RESULT_OK) {
            mGoogleApiClient.connect();
        } else if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
//			String yes = getString(R.string.yes);
//			String no = getString(R.string.no);

            // Do something after user returned from app settings screen, like showing a Toast.
//			Toast.makeText(
//					this,
//					getString(R.string.returned_from_app_settings_to_activity,
//							hasLocationPermission(this) ? yes : no,
////							hasLocationPermission(this) ? yes : no,
////							hasSmsPermission() ? yes : no),
//					Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}