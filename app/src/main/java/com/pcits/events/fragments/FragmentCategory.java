package com.pcits.events.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityAddCategory;
import com.pcits.events.ActivityEditCategory;
import com.pcits.events.ActivityHome;
import com.pcits.events.R;
import com.pcits.events.adapters.AdapterCategory;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.widgets.floatingactionbutton.AddFloatingActionButton;

public class FragmentCategory extends Fragment {

	private ListView list;
	private AdapterCategory adapterCategory;
	private AddFloatingActionButton btnAddDeal;
	private TextView lblTitleHeader;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;

	private View v;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		v = inflater.inflate(R.layout.layout_list_deals, container, false);
		try {
			initUI();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return v;
	}

	private void initUI() {
		try {
			lblTitleHeader = (TextView) v.findViewById(R.id.lblTitleHeader);
			lblTitleHeader.setText("Events");
			llMenu = (LinearLayout) v.findViewById(R.id.llMenu);
			list = (ListView) v.findViewById(R.id.lsvDeals);
			btnAddDeal = (AddFloatingActionButton) v
					.findViewById(R.id.btnAddDeal);
			llMenu.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// ActivityHome.resideMenu.openMenu();
					//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
					Intent intent=new Intent(getActivity(),ActivityHome.class);
					getActivity().startActivity(intent);
					getActivity().finish();
				}
			});
			// btnAddDeal.setTitle("Add New Category");
			btnAddDeal.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(getActivity(),
							ActivityAddCategory.class);
					startActivity(i);
				}
			});
			getListCategory();
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					GlobalValue.categoryObj = GlobalValue.arrCategory
							.get(position);
					Intent i = new Intent(getActivity(),
							ActivityEditCategory.class);
					startActivity(i);
				}
			});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void getListCategory() {
		try {
			ModelManager.getListCategory(getActivity(), true,
					new ModelManagerListener() {

						@Override
						public void onSuccess(Object object) {
							// TODO Auto-generated method stub
							String json = (String) object;
							GlobalValue.arrCategory = ParserUtility
									.getListCategory(json);
							if (GlobalValue.arrCategory.size() > 0) {
								adapterCategory = new AdapterCategory(
										getActivity(), GlobalValue.arrCategory);
								adapterCategory.notifyDataSetChanged();
								list.setAdapter(adapterCategory);
							} else {
								Toast.makeText(getActivity(), "No Data",
										Toast.LENGTH_SHORT).show();
							}
						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}
					});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
}
