package com.pcits.events;

import java.util.regex.Pattern;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.pcits.events.widgets.FloatLabeledEditText;
import com.pcits.events.widgets.KenBurnsView;

public class ForgetPasswordActivity extends FragmentActivity {

	private FloatLabeledEditText txtYourEmail;
	private Button btnSubmit;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;

	private final Pattern EMAIL_PATTERN = Pattern
			.compile("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+");
	private WebView web;
	private ProgressBar prgPageLoading;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_browser);

		// Connecct view object and xml ids
		web = (WebView) findViewById(R.id.web);
		prgPageLoading = (ProgressBar) findViewById(R.id.prgPageLoading);

		web.setHorizontalScrollBarEnabled(true);
		web.getSettings().setAllowFileAccess(true);
		web.getSettings().setJavaScriptEnabled(true);
		setProgressBarVisibility(true);

		web.getSettings().setBuiltInZoomControls(true);
		web.getSettings().setUseWideViewPort(true);
		web.setInitialScale(1);

		web.loadUrl("http://pcits.com/events/forget-pass.php");

		final Activity act = this;
		web.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView webview, int progress) {

				act.setProgress(progress * 100);
				prgPageLoading.setProgress(progress);

			}

		});
		// initUI();
		// initControl();
		// initNotBoringActionBar();
	}

	// private void initNotBoringActionBar() {
	// mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
	// mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);
	//
	// }

	// private void initUI() {
	// txtYourEmail = (FloatLabeledEditText) findViewById(R.id.txtYourEmail);
	// btnSubmit = (Button) findViewById(R.id.btnSubmit);
	// llMenu = (LinearLayout) findViewById(R.id.llMenu);
	// validateEmail();
	// }

	// private void validateEmail() {
	// txtYourEmail.getEditText().addTextChangedListener(new TextWatcher() {
	//
	// @Override
	// public void onTextChanged(CharSequence s, int start, int before,
	// int count) {
	//
	// }
	//
	// @Override
	// public void beforeTextChanged(CharSequence s, int start, int count,
	// int after) {
	//
	// }
	//
	// @Override
	// public void afterTextChanged(Editable s) {
	// String email = txtYourEmail.getText().toString();
	// if (validateEmail(email) == false) {
	// txtYourEmail.setError(Html
	// .fromHtml("<font color='red'>Invalid Email!</font>"));
	// } else {
	// }
	//
	// }
	// });
	// }
	//
	// private boolean validateEmail(String email) {
	// return EMAIL_PATTERN.matcher(email).matches();
	// }
	//
	// private void initControl() {
	// llMenu.setOnClickListener(new OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	// finish();
	// }
	// });
	// // submit
	// btnSubmit.setOnClickListener(new OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	// forgetpass();
	// }
	// });
	// }
	//
	// private void forgetpass() {
	// String email = txtYourEmail.getText().toString();
	// if (email.isEmpty()) {
	// Toast.makeText(this, "Please, input email", Toast.LENGTH_SHORT)
	// .show();
	// txtYourEmail.requestFocus();
	// return;
	// }
	// ModelManager.forgetpassword(this, email, true,
	// new ModelManagerListener() {
	//
	// @Override
	// public void onSuccess(Object object) {
	// // TODO Auto-generated method stub
	// String json = (String) object;
	//
	// Log.e("aaaaa", "result :" + json);
	//
	// Toast.makeText(ForgetPasswordActivity.this,
	// checkResult(json), Toast.LENGTH_SHORT).show();
	// }
	//
	// @Override
	// public void onError() {
	// // TODO Auto-generated method stub
	//
	// }
	// });
	// }
	//
	// protected String checkResult(String strJson) {
	// // TODO Auto-generated method stub
	// JSONObject json = null;
	// String message = "";
	// try {
	// json = new JSONObject(strJson);
	// if (json.getString("status").equals("success")) {
	// message = this.getString(R.string.message_success);
	// new AlertDialog.Builder(this)
	// .setTitle(R.string.app_name)
	// .setMessage("Please check mail.!")
	// .setPositiveButton(android.R.string.ok,
	// new DialogInterface.OnClickListener() {
	// @Override
	// public void onClick(
	// DialogInterface arg0, int arg1) {
	// finish();
	// }
	// })
	// .create().show();
	// } else {
	// message = json.getString("message");
	// }
	//
	// } catch (JSONException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// return message;
	// }

}
