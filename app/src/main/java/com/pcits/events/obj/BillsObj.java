package com.pcits.events.obj;

/**
 * Created by SAKSHI on 10/24/2017.
 */

public class BillsObj
{
    public String id,deal_id,description,image;
    public String amount,paid_by,created_by;

    public BillsObj(String id, String deal_id, String description, String image, String amount, String paid_by, String created_by) {
        this.id = id;
        this.deal_id = deal_id;
        this.description = description;
        this.image = image;
        this.amount = amount;
        this.paid_by = paid_by;
        this.created_by = created_by;
    }

    public String getId() {
        return this.id;
    }

    public String getDeal_id() {
        return this.deal_id;
    }

    public String getDescription() {
        return this.description;
    }

    public String getImage() {
        return this.image;
    }

    public String getAmount() {
        return this.amount;
    }

    public String getPaid_by() {
        return this.paid_by;
    }

    public String getCreated_by() {
        return this.created_by;
    }
}
