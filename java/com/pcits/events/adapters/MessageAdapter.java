package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.pcits.events.ActivityDetailTickets;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.obj.MessageObj;
import com.pcits.events.stickylist.StickyListHeadersAdapter;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.widgets.MaterialRippleLayout;

public class MessageAdapter extends BaseAdapter implements
		StickyListHeadersAdapter, SectionIndexer {

	private Activity activity;
	private ArrayList<MessageObj> arrMessage;
	private LayoutInflater mInflate;
	private int[] mSectionIndices;
	private Integer[] mSectionLetters;

	public MessageAdapter(Activity a, ArrayList<MessageObj> listMessage) {
		this.activity = a;
		this.arrMessage = listMessage;
		this.mInflate = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		// mHeaders = a.getResources().getStringArray(R.array.tickets_header);
		mSectionIndices = getSectionIndices();
		mSectionLetters = getSectionLetters();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrMessage.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		// TODO Auto-generated method stub
		final HolderView holder;
		if (convertView == null) {
			holder = new HolderView();
			convertView = mInflate.inflate(R.layout.item_tab_winner, null);
			holder.lblTitle = (TextView) convertView
					.findViewById(R.id.lblTitleEvent);
			holder.lblCompany = (TextView) convertView
					.findViewById(R.id.lblCompnay);
			holder.lblDateCreated = (TextView) convertView
					.findViewById(R.id.lblDateCreated);
			holder.lblDateClaimed = (TextView) convertView
					.findViewById(R.id.lblDateClaimed);
			holder.itemTicket = (MaterialRippleLayout) convertView
					.findViewById(R.id.itemTicket);
			holder.imgStatus = (ImageView) convertView
					.findViewById(R.id.img_status);

			convertView.setTag(holder);
		} else {
			holder = (HolderView) convertView.getTag();
		}
		final MessageObj o = arrMessage.get(position);
		if (o != null) {
			holder.lblTitle.setText(o.getTitle());
			holder.lblCompany.setText("By: " + o.getCompany());
			holder.lblDateCreated.setText("Date Created: "
					+ DateTimeUtility.convertTimeStampToDate(
							o.getDate_created(), "dd MMM yy - HH:mm"));

			if (o.getStatus() == 0) {
				// holder.lblLive.setBackgroundColor(Color.GREEN);
				// holder.lblLive.setText("Live");
				holder.imgStatus.setImageResource(R.drawable.ic_tick);
			} else {
				// holder.lblLive.setBackgroundColor(Color.GRAY);
				// holder.lblLive.setText("Used");
				holder.imgStatus.setImageResource(R.drawable.ic_expired);
			}

			holder.itemTicket.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					GlobalValue.msg = arrMessage.get(position);
					Intent i = new Intent(activity, ActivityDetailTickets.class);
					parent.getContext().startActivity(i);
				}
			});

		}
		return convertView;
	}

	public class HolderView {
		TextView lblTitle, lblCompany, lblDateCreated, lblDateClaimed;
		MaterialRippleLayout itemTicket;
		ImageView imgStatus;
	}

	class HeaderViewHolder {
		TextView lblHeader;
	}

	@Override
	public Object[] getSections() {
		// TODO Auto-generated method stub
		return mSectionLetters;
	}

	@Override
	public int getPositionForSection(int section) {
		// TODO Auto-generated method stub
		if (mSectionIndices.length == 0) {
			return 0;
		}

		if (section >= mSectionIndices.length) {
			section = mSectionIndices.length - 1;
		} else if (section < 0) {
			section = 0;
		}
		return mSectionIndices[section];
	}

	@Override
	public int getSectionForPosition(int position) {
		// TODO Auto-generated method stub
		for (int i = 0; i < mSectionIndices.length; i++) {
			if (position < mSectionIndices[i]) {
				return i - 1;
			}
		}
		return mSectionIndices.length - 1;
	}

	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		HeaderViewHolder holder;

		if (convertView == null) {
			holder = new HeaderViewHolder();
			convertView = mInflate.inflate(R.layout.item_header_ticket, parent,
					false);
			holder.lblHeader = (TextView) convertView
					.findViewById(R.id.lblHeader);
			convertView.setTag(holder);
		} else {
			holder = (HeaderViewHolder) convertView.getTag();
		}

		// set header text as first char in name
		String headerChar = arrMessage.get(position).getStatus() + "";
		if (headerChar.equals("0")) {
			holder.lblHeader.setText(activity.getResources().getString(
					R.string.live));
		} else {
			holder.lblHeader.setText(activity.getResources().getString(
					R.string.used));
		}

		return convertView;
	}

	@Override
	public long getHeaderId(int position) {
		// TODO Auto-generated method stub
		return arrMessage.get(position).getStatus();
	}

	private int[] getSectionIndices() {
		ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
		int lastFirstChar = arrMessage.get(0).getStatus();
		sectionIndices.add(0);
		for (int i = 1; i < arrMessage.size(); i++) {
			if (arrMessage.get(i).getStatus() != lastFirstChar) {
				lastFirstChar = arrMessage.get(i).getStatus();
				sectionIndices.add(i);
			}
		}
		int[] sections = new int[sectionIndices.size()];
		for (int i = 0; i < sectionIndices.size(); i++) {
			sections[i] = sectionIndices.get(i);
		}
		return sections;
	}

	private Integer[] getSectionLetters() {
		Integer[] letters = new Integer[mSectionIndices.length];
		for (int i = 0; i < mSectionIndices.length; i++) {
			letters[i] = arrMessage.get(mSectionIndices[i]).getStatus();
		}
		return letters;
	}
}
