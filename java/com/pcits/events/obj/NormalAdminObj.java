package com.pcits.events.obj;

public class NormalAdminObj {

	private double numberOfEvent, numberOfEventAdv, numberOfEventFea,
			numberOfClient, numberOfMaleClient, numberOfFemaleClient, lt5,
			from5to10, from10to20, from20to40, gt40, numberEventNotExpired,
			numberEventExpired, genderClientNotDisclosed,
			totalPurchaseClientMale, totalPurchaseClientFemale,
			totalPurchaseClientNotDisclosed, numberEventFree,
			numberLuckyDrawClaimed, numberLuckyDrawAwarded,
			numberLuckyDrawUnclaimed;

	public NormalAdminObj() {
		// TODO Auto-generated constructor stub
	}

	public NormalAdminObj(double event, double eventAdv, double eventFea,
			double client, double maleClient, double femaleClient, double lt5,
			double from5to10, double from10to20, double from20to40,
			double gt40, double numberEventNotExpired,
			double numberEventExpired, double genderClientNotDisclosed,
			double totalPurchaseClientMale, double totalPurchaseClientFemale,
			double totalPurchaseClientNotDisclosed, double numberEventFree,
			double numberLuckyDrawClaimed, double numberLuckyDrawAwarded,
			double numberLuckyDrawUnclaimed) {
		this.numberOfEvent = event;
		this.numberOfEventAdv = eventAdv;
		this.numberOfEventFea = eventFea;
		this.numberOfClient = client;
		this.numberOfMaleClient = maleClient;
		this.numberOfFemaleClient = femaleClient;
		this.lt5 = lt5;
		this.from5to10 = from5to10;
		this.from10to20 = from10to20;
		this.from20to40 = from20to40;
		this.gt40 = gt40;
		this.numberEventNotExpired = numberEventNotExpired;
		this.numberEventExpired = numberEventExpired;
		this.genderClientNotDisclosed = genderClientNotDisclosed;
		this.totalPurchaseClientMale = totalPurchaseClientMale;
		this.totalPurchaseClientFemale = totalPurchaseClientFemale;
		this.totalPurchaseClientNotDisclosed = totalPurchaseClientNotDisclosed;
		this.numberEventFree = numberEventFree;
		this.numberLuckyDrawClaimed = numberLuckyDrawClaimed;
		this.numberLuckyDrawUnclaimed = numberLuckyDrawUnclaimed;
		this.numberLuckyDrawAwarded = numberLuckyDrawAwarded;
	}

	public double getNumberEventNotExpired() {
		return numberEventNotExpired;
	}

	public void setNumberEventNotExpired(double numberEventNotExpired) {
		this.numberEventNotExpired = numberEventNotExpired;
	}

	public double getNumberEventExpired() {
		return numberEventExpired;
	}

	public void setNumberEventExpired(double numberEventExpired) {
		this.numberEventExpired = numberEventExpired;
	}

	public double getGenderClientNotDisclosed() {
		return genderClientNotDisclosed;
	}

	public void setGenderClientNotDisclosed(double genderClientNotDisclosed) {
		this.genderClientNotDisclosed = genderClientNotDisclosed;
	}

	public double getTotalPurchaseClientMale() {
		return totalPurchaseClientMale;
	}

	public void setTotalPurchaseClientMale(double totalPurchaseClientMale) {
		this.totalPurchaseClientMale = totalPurchaseClientMale;
	}

	public double getTotalPurchaseClientFemale() {
		return totalPurchaseClientFemale;
	}

	public void setTotalPurchaseClientFemale(double totalPurchaseClientFemale) {
		this.totalPurchaseClientFemale = totalPurchaseClientFemale;
	}

	public double getTotalPurchaseClientNotDisclosed() {
		return totalPurchaseClientNotDisclosed;
	}

	public void setTotalPurchaseClientNotDisclosed(
			double totalPurchaseClientNotDisclosed) {
		this.totalPurchaseClientNotDisclosed = totalPurchaseClientNotDisclosed;
	}

	public double getNumberEventFree() {
		return numberEventFree;
	}

	public void setNumberEventFree(double numberEventFree) {
		this.numberEventFree = numberEventFree;
	}

	public double getNumberLuckyDrawClaimed() {
		return numberLuckyDrawClaimed;
	}

	public void setNumberLuckyDrawClaimed(double numberLuckyDrawClaimed) {
		this.numberLuckyDrawClaimed = numberLuckyDrawClaimed;
	}

	public double getNumberLuckyDrawAwarded() {
		return numberLuckyDrawAwarded;
	}

	public void setNumberLuckyDrawAwarded(double numberLuckyDrawAwarded) {
		this.numberLuckyDrawAwarded = numberLuckyDrawAwarded;
	}

	public double getNumberLuckyDrawUnclaimed() {
		return numberLuckyDrawUnclaimed;
	}

	public void setNumberLuckyDrawUnclaimed(double numberLuckyDrawUnclaimed) {
		this.numberLuckyDrawUnclaimed = numberLuckyDrawUnclaimed;
	}

	public double getLt5() {
		return lt5;
	}

	public void setLt5(double lt5) {
		this.lt5 = lt5;
	}

	public double getFrom5to10() {
		return from5to10;
	}

	public void setFrom5to10(double from5to10) {
		this.from5to10 = from5to10;
	}

	public double getFrom10to20() {
		return from10to20;
	}

	public void setFrom10to20(double from10to20) {
		this.from10to20 = from10to20;
	}

	public double getFrom20to40() {
		return from20to40;
	}

	public void setFrom20to40(double from20to40) {
		this.from20to40 = from20to40;
	}

	public double getGt40() {
		return gt40;
	}

	public void setGt40(double gt40) {
		this.gt40 = gt40;
	}

	public double getNumberOfEvent() {
		return numberOfEvent;
	}

	public void setNumberOfEvent(double numberOfEvent) {
		this.numberOfEvent = numberOfEvent;
	}

	public double getNumberOfEventAdv() {
		return numberOfEventAdv;
	}

	public void setNumberOfEventAdv(double numberOfEventAdv) {
		this.numberOfEventAdv = numberOfEventAdv;
	}

	public double getNumberOfEventFea() {
		return numberOfEventFea;
	}

	public void setNumberOfEventFea(double numberOfEventFea) {
		this.numberOfEventFea = numberOfEventFea;
	}

	public double getNumberOfClient() {
		return numberOfClient;
	}

	public void setNumberOfClient(double numberOfClient) {
		this.numberOfClient = numberOfClient;
	}

	public double getNumberOfMaleClient() {
		return numberOfMaleClient;
	}

	public void setNumberOfMaleClient(double numberOfMaleClient) {
		this.numberOfMaleClient = numberOfMaleClient;
	}

	public double getNumberOfFemaleClient() {
		return numberOfFemaleClient;
	}

	public void setNumberOfFemaleClient(double numberOfFemaleClient) {
		this.numberOfFemaleClient = numberOfFemaleClient;
	}

}
