package com.pcits.events.database.mapper;

import android.database.Cursor;

import com.pcits.events.database.DBKeyConfig;
import com.pcits.events.obj.NotifyBoxObj;

public class NotificationMapper implements RowMapper<NotifyBoxObj> {

	@Override
	public NotifyBoxObj mapRow(Cursor row, int rowNum) {
		// TODO Auto-generated method stub
		NotifyBoxObj mode = new NotifyBoxObj();
		mode.setNotifyId(CursorParseUtility.getString(row,
				DBKeyConfig.KEY_NOTIFICATION_NOTIFY_ID));
		mode.setType_id(CursorParseUtility.getInt(row,
				DBKeyConfig.KEY_NOTIFICATION_TYPE_ID));
		mode.setSummary(CursorParseUtility.getString(row,
				DBKeyConfig.KEY_NOTIFICATION_SUMMARY));
		mode.setDate(CursorParseUtility.getString(row,
				DBKeyConfig.KEY_NOTIFICATION_DATE));
		mode.setStatus_id(CursorParseUtility.getString(row,
				DBKeyConfig.KEY_NOTIFICATION_STATUS_ID));
		mode.setTitle(CursorParseUtility.getString(row,
				DBKeyConfig.KEY_NOTIFICATION_TITLE));

		return mode;
	}

}
