package com.pcits.events;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.pcits.common.utils.GsonUtility;
import com.pcits.events.adapters.ListEventsAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.network.EventsAPI;
import com.pcits.events.network.RestClient;
import com.pcits.events.obj.ListEventObj;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivityListEvents extends Activity {
    private RecyclerView mRecyclerView;
    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_events);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_events);
        mProgressDialog = new ProgressDialog(this);
        Log.d("ActivityListEvents", "private activity: "+GlobalValue.myUser.getUsername());
        Log.d("ActivityListEvents", "private activity: "+GsonUtility.convertObjectToJSONString(GlobalValue.myUser));
        Log.d("ActivityListEvents", "getEvents: "+ GsonUtility.convertObjectToJSONString(GlobalValue.myClient));

        getEvents();
    }

    public void getEvents() {
        Log.d("ActivityListEvents", "getEvents: "+GlobalValue.myClient.getUsername());
        Log.d("ActivityListEvents", "getEvents: "+ GsonUtility.convertObjectToJSONString(GlobalValue.myUser));
        Log.d("ActivityListEvents", "getEvents: "+ GsonUtility.convertObjectToJSONString(GlobalValue.myClient));
        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsApiObj = retrofit.create(EventsAPI.class);
        mProgressDialog.setMessage("Getting Events");
        mProgressDialog.show();

        try {
            Call<List<ListEventObj>> listeventCallobj = eventsApiObj.getEvents("events/"+Integer.valueOf(GlobalValue.myClient.getId()));
            listeventCallobj.enqueue(new Callback<List<ListEventObj>>() {
                @Override
                public void onResponse(Call<List<ListEventObj>> call, Response<List<ListEventObj>> response) {
                    mProgressDialog.dismiss();
                    if (response.isSuccessful()) {
                        List<ListEventObj> listevents = response.body();
                        ListEventsAdapter eventsadapterobj = new ListEventsAdapter(listevents, ActivityListEvents.this,(Activity) ActivityListEvents.this,GlobalValue.myClient.getUsername());
                        eventsadapterobj.notifyDataSetChanged();
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ActivityListEvents.this);
                        mRecyclerView.setLayoutManager(layoutManager);
                        mRecyclerView.setHasFixedSize(true);
                        mRecyclerView.setAdapter(eventsadapterobj);
                        if (listevents==null || listevents.isEmpty()){
                            Toast.makeText(ActivityListEvents.this,"No private events found",Toast.LENGTH_LONG).show();
                        }

                    }
                }

                @Override
                public void onFailure(Call<List<ListEventObj>> call, Throwable throwable) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
