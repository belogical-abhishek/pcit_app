/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pcits.events.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.google.gson.reflect.TypeToken;
import com.pcits.common.utils.EventsPreferences;
import com.pcits.common.utils.GsonUtility;
import com.pcits.common.utils.ImageHelper;
import com.pcits.common.utils.Logging;
import com.pcits.events.AddEventActivity;
import com.pcits.events.AddPrivateEventActivity;
import com.pcits.events.R;
import com.pcits.events.adapters.CategoryAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.cropper.CropImageView;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.obj.CategoryObj;
import com.pcits.events.obj.CreateEventObj;
import com.pcits.events.utils.AccessStorage;
import com.pcits.events.widgets.FloatLabeledEditText;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;
import com.pcits.events.wizard.model.CustomAddEventPage1;
import com.pcits.events.wizard.model.CustomAddPrivateEventPage1;
import com.pcits.events.wizard.ui.PageFragmentCallbacks;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import static android.support.v4.provider.FontsContractCompat.FontRequestCallback.RESULT_OK;
import static com.pcits.events.libraries.UserFunctions.URLAdmin;

public class AddPrivateEventPage1Fragment extends Fragment {

    private static final String TAG = "AddEventPage1Fragment";

    private static final String ARG_KEY = "key";
    private String tempImmage;

    private PageFragmentCallbacks mCallbacks;
    private String mKey;
    private CustomAddPrivateEventPage1 mPage;
    private EditText mFlTitle, mFlCompany;
    private TextViewRobotoCondensedRegular mLblCategory;
    private View mRootView;

    // Crop image
    private Button mBtnSelectImg;
    private String selectedImagePath;

    private Uri selectedUri;
    private Bitmap eventImage,selectedImage;
    private Dialog mCropDialog;
    private TextView btnOk;
    private ImageView cropImg;
    private ImageView mImgPreview;
    private static final int CROP_IMAGE = 1000;
    private int mEventImageW = 0, mEventImageH = 0;

    private CategoryAdapter adapterCategory;
    private ArrayList<CategoryObj> mArrCate;

    public static AddPrivateEventPage1Fragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        AddPrivateEventPage1Fragment fragment = new AddPrivateEventPage1Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    public AddPrivateEventPage1Fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: user: "+GsonUtility.convertObjectToJSONString(GlobalValue.myUser));
        Log.d(TAG, "onCreate: user: "+GsonUtility.convertObjectToJSONString(GlobalValue.myClient));
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (CustomAddPrivateEventPage1) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_add_event_page1,
                container, false);
        // ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage
        // .getTitle());
        try {

            initUI();
        } catch (Exception ex) {
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
        return mRootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException(
                    "Activity must implement PageFragmentCallbacks");
        }

        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);

        // In a future update to the support library, this should override
        // setUserVisibleHint
        // instead of setMenuVisibility.
        if (mFlTitle != null && mFlCompany != null && mLblCategory != null) {
            InputMethodManager imm = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (!menuVisible) {
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d(TAG, "onActivityResult: "+requestCode);
        if (requestCode == CROP_IMAGE && resultCode == Activity.RESULT_OK) {
            try {
                Uri uri = null;

                try {
                    uri = data.getData();
                } catch(Exception ex) {
                    ex.printStackTrace();
                }

                //selectedUri = data.getData();
                //selectedImagePath = AccessStorage.getPath(getActivity(), selectedUri);
                selectedImagePath = uri.getPath();
                selectedUri = uri;

               /* CropImage.activity(selectedUri)
                        .start(getContext(),this);*/
                File imgFile = new File(AccessStorage.getPath(getContext(), selectedUri));
                if (imgFile.exists())
                {
                    if (MaxSizeImage(imgFile.getPath()))
                    {
                        CropImage.activity(selectedUri).setAspectRatio(16,9)
                                .start(getContext(), this);

               /* UCrop.of(selectedUri, outputFileUri)
                        .withAspectRatio(16, 9)
                        .withMaxResultSize(200, 200)
                        .start((Activity)getContext());*/
                        eventImage = AccessStorage
                                .loadPrescaledBitmap(selectedImagePath);

                        if (mCropDialog != null) {
                            mCropDialog.dismiss();
                        }

                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = true;

                        // Returns null, sizes are in the options variable
                        BitmapFactory.decodeFile(selectedImagePath, options);
                        mEventImageW = options.outWidth;
                        mEventImageH = options.outHeight;
/*
                if (mCropDialog != null) {
                    mCropDialog.dismiss();
                }
                if (mEventImageW >= 1080 && mEventImageH >= 608) {
                    showCropImageDialog();
                } else {
                    showCropImageDialog();

                    Toast.makeText(
                            getActivity(),
                            "Your image is small. Please upload 1080x608 minimum.",
                            Toast.LENGTH_LONG).show();
                }*/
               /* if (requestCode == UCrop.REQUEST_CROP) {
                    final Uri resultUri = UCrop.getOutput(data);
                    Log.d(TAG, "onActivityResult: "+resultUri);
                    File imgFile = new  File(AccessStorage.getPath(getContext(),resultUri));

                    if(imgFile.exists()){

                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        cropImg.setImageBitmap(myBitmap);
                        selectedImagePath = AccessStorage.getPath(getContext(),resultUri);

                    }

                    btnOk.setVisibility(View.VISIBLE);
                } else if (resultCode == UCrop.RESULT_ERROR) {
                    final Throwable cropError = UCrop.getError(data);
                    cropError.printStackTrace();
                }*/
                    }
                    else
                    {
                        Toast.makeText(getActivity(),
                                getActivity().getApplicationContext().getString(R.string.image_size_limit),
                                Toast.LENGTH_SHORT).show();
                    }
                }



            } catch (Exception ex) {
                Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            Log.d(TAG, "onActivityResult: resultCode "+resultCode);
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                Uri resultUri = result.getUri();
                Log.d(TAG, "onActivityResult: " + resultUri);
                File imgFile = new File(AccessStorage.getPath(getContext(), resultUri));

                if (imgFile.exists())
                {
                    if(MaxSizeImage(imgFile.getPath()))
                    {
                        Bitmap myBitmap = null;

                        myBitmap = BitmapFactory.decodeFile(resultUri.getPath());

                        if(myBitmap == null) {
                            myBitmap = BitmapFactory.decodeFile(resultUri.getPath());
                        }

                        //Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        cropImg.setImageBitmap(myBitmap);
                        cropImg.setVisibility(View.VISIBLE);
                        //selectedImagePath = AccessStorage.getPath(getContext(), resultUri);
                        selectedImagePath = resultUri.getPath();
                        mImgPreview.setImageBitmap(myBitmap);

                        TypeToken<CreateEventObj> token = new TypeToken<CreateEventObj>(){};
                        CreateEventObj eventObj = GsonUtility.convertJSONStringToObject(EventsPreferences.getKeyPrivateEvent(getContext()),token);

                        if(eventObj==null)
                            eventObj=new CreateEventObj();
                        eventObj.setImage(selectedImagePath);
                        EventsPreferences.setKeyPrivateEvent(getContext(),GsonUtility.convertObjectToJSONString(eventObj));
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.detach(this).attach(this).commit();
                    }
                    else
                    {
                        Toast.makeText(getActivity(),
                                getActivity().getApplicationContext().getString(R.string.image_size_limit),
                                Toast.LENGTH_SHORT).show();
                    }
                }
                btnOk.setVisibility(View.VISIBLE);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                error.printStackTrace();
            }
        }
    }

    public void saveState() {
        try {
//			GlobalValue.dealsObj.setTitle(mFlTitle.getText().toString());
//			GlobalValue.dealsObj.setCompany(mFlCompany.getText().toString());
            GlobalValue.createEventObj.setCategoryId(mLblCategory.getText()
                    .toString());
            GlobalValue.createEventObj.setTitle(mFlTitle.getText().toString());
            GlobalValue.createEventObj.setCompany(mFlCompany.getText().toString());

            AddPrivateEventActivity.eventCroppedImage = selectedImage;


            CreateEventObj eventObj;
            if(EventsPreferences.getKeyPrivateEvent(getContext()).isEmpty())
                eventObj = new CreateEventObj();
            else{
                TypeToken<CreateEventObj> token = new TypeToken<CreateEventObj>(){};
                eventObj = GsonUtility.convertJSONStringToObject(EventsPreferences.getKeyPrivateEvent(getContext()),token);
            }



            eventObj.setTitle(mFlTitle.getText().toString());
            eventObj.setCompany(mFlCompany.getText().toString());
            eventObj.setCategoryId(mLblCategory.getText().toString());
            eventObj.setImage(selectedImagePath);

            EventsPreferences.setKeyPrivateEvent(getContext(), GsonUtility.convertObjectToJSONString(eventObj));
            //save to pref
            Log.d(TAG, "saveState: "+GsonUtility.convertObjectToJSONString(EventsPreferences.getKeyPrivateEvent(getContext())));


            // GlobalValue.dealsObj.setImage(tempImmage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void restoreState() {
        try {
            TypeToken<CreateEventObj> token = new TypeToken<CreateEventObj>(){};
            CreateEventObj eventObj = GsonUtility.convertJSONStringToObject(EventsPreferences.getKeyPrivateEvent(getContext()),token);
            Log.d(TAG, "restoreState: "+GsonUtility.convertObjectToJSONString(GlobalValue.dealsObj));

            mFlTitle.setText(eventObj.getTitle());
            mFlCompany.setText(eventObj.getCompany());
            mLblCategory.setText(eventObj.getCategoryId());

            if (eventObj.getImage() != null) {
                File imgFile = new File(eventObj.getImage());

                if(imgFile.exists()){

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());


                    mImgPreview.setImageBitmap(myBitmap);
                    selectedImage = myBitmap;
                    selectedImagePath=eventObj.getImage();
                    selectedUri= Uri.fromFile(new File(selectedImagePath));

                }

            } else {
                mImgPreview.setImageDrawable(ContextCompat.getDrawable(getContext(),R.drawable.ny));
//                aq.id(mImgPreview).image(
//                        UserFunctions.URLAdmin
//                                + GlobalValue.dealsObj.getImage(), false, true,
//                        300, 0, new BitmapAjaxCallback() {
//                            @Override
//                            protected void callback(String url, ImageView iv,
//                                                    Bitmap bm, AjaxStatus status) {
//                                // TODO Auto-generated method stub
//                                super.callback(url, iv, bm, status);
//
//                                AddEventActivity.eventCroppedImage = bm;
//                            }
//                        });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    /*public void restoreState() {
        try {
            Log.d(TAG, "restoreState: "+GsonUtility.convertObjectToJSONString(GlobalValue.dealsObj));
            mFlTitle.setText(GlobalValue.dealsObj.getTitle());
            mFlCompany.setText(GlobalValue.dealsObj.getCompany());
            mLblCategory.setText(GlobalValue.dealsObj.getCategory_name());

			*//*if (AddEventActivity.eventCroppedImage != null) {
				mImgPreview.setImageBitmap(AddEventActivity.eventCroppedImage);
			} else *//*

            if(GlobalValue.dealsObj.getImage() != null) {
                selectedImagePath = GlobalValue.dealsObj.getImage();
                if(AddEventActivity.isNewImageSet) {
                    String[] tokens = selectedImagePath.split(":");

                    File file;

                    if(tokens.length > 1) {
                        file = new File(tokens[1]);
                    } else {
                        file = new File(tokens[0]);
                    }

                    selectedUri = Uri.fromFile(file);
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedUri);
                    mImgPreview.setImageBitmap(bitmap);
                } else {
                    String dealImagePath = GlobalValue.dealsObj.getImage();
                    String absolutePath;

                    if(dealImagePath.contains("http")) {
                        absolutePath = dealImagePath;
                    } else {
                        absolutePath = URLAdmin + GlobalValue.dealsObj.getImage();
                    }

                    try {
                        new AsyncTask<String, Void, Bitmap>() {
                            @Override
                            protected void onPostExecute(Bitmap bitmap) {
                                super.onPostExecute(bitmap);
                                mImgPreview.setImageBitmap(bitmap);
                            }

                            @Override
                            protected Bitmap doInBackground(String... strings) {
                                URL url = null;
                                try {
                                    url = new URL(strings[0]);
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }

                                try {
                                    return BitmapFactory.decodeStream(url.openConnection().getInputStream());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                return null;
                            }
                        }.execute(absolutePath);

                        selectedImagePath = absolutePath;
                        File file = new File(absolutePath);
                        selectedUri = Uri.fromFile(file);
                    } catch(Exception ex) {
                        ex.printStackTrace();
                    }
                }
            } else {
                AQuery aq = new AQuery(getActivity());
                aq.id(mImgPreview).image(
                        URLAdmin
                                + GlobalValue.dealsObj.getImage(), false, true,
                        300, 0, new BitmapAjaxCallback() {
                            @Override
                            protected void callback(String url, ImageView iv,
                                                    Bitmap bm, AjaxStatus status) {
                                // TODO Auto-generated method stub
                                super.callback(url, iv, bm, status);

                                AddEventActivity.eventCroppedImage = bm;
                            }
                        });
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }*/

    public boolean isValidated() {
        try {
            if (AddEventActivity.eventCroppedImage == null
                    || mFlTitle.getText().toString().trim().length() == 0
                    || mLblCategory
                    .getText()
                    .toString()
                    .equalsIgnoreCase(
                            getActivity().getResources().getString(
                                    R.string.select_category))) {
                return false;
            } else {
                return true;
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private void initUI() {
        try {
            mFlTitle = (EditText) mRootView
                    .findViewById(R.id.txtTitle);

            mFlCompany = (EditText) mRootView
                    .findViewById(R.id.txtCompany);

            mLblCategory = (TextViewRobotoCondensedRegular) mRootView
                    .findViewById(R.id.lblCategory);

            mBtnSelectImg = (Button) mRootView.findViewById(R.id.btnSelectImg);

            mImgPreview = (ImageView) mRootView
                    .findViewById(R.id.img_cropped_preview);

			if (AddEventActivity.isDeal)
			{
				restoreState();
			}
           // restoreState();

            // Should call this method end of declaring UI.
            initControl();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void initControl() {
        try {
            mBtnSelectImg.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // selectImage();
                    selectImageDialog();
                   // showCropImageDialog();
                }
            });

            mLblCategory.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.d(TAG, "onClick:mLblCategory clicked");
                    getCategories();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void showCategoryDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_nice_category);
        final ListView lsvCategory = (ListView) dialog
                .findViewById(R.id.lsvFilterCategory);
        adapterCategory = new CategoryAdapter(getActivity(), mArrCate);
        lsvCategory.setAdapter(adapterCategory);
        lsvCategory.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                mLblCategory.setText(mArrCate.get(position).getCategoryName());

                // Set category id
                TypeToken<CreateEventObj> token = new TypeToken<CreateEventObj>(){};
                CreateEventObj eventObj = GsonUtility.convertJSONStringToObject(EventsPreferences.getKeyPrivateEvent(getContext()),token);

                if(eventObj==null)
                    eventObj=new CreateEventObj();
                eventObj.setCategoryId(mArrCate.get(position).getCategoryName());
                EventsPreferences.setKeyPrivateEvent(getContext(),GsonUtility.convertObjectToJSONString(eventObj));
                GlobalValue.createEventObj.setCategoryId(mArrCate.get(position)
                        .getCategoryId());

                AddPrivateEventActivity.selectedCategory = mArrCate.get(position).getCategoryName();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void getCategories() {
        try {
            if (mArrCate != null && mArrCate.size() > 1) {
                // Show dialog
                showCategoryDialog();
            } else {
                if (NetworkUtility.getInstance(getActivity())
                        .isNetworkAvailable()) {
                    ModelManager.getListCategory(getActivity(), true,
                            new ModelManagerListener() {

                                @Override
                                public void onSuccess(Object object) {
                                    String json = (String) object;
                                    Log.d(TAG, "onSuccess catagory options: "+json);
                                    mArrCate = ParserUtility
                                            .getListCategory(json);

                                    // Show dialog
                                    showCategoryDialog();
                                }

                                @Override
                                public void onError() {
                                    // TODO Auto-generated method stub

                                }
                            });
                } else {
                    Toast.makeText(getActivity(), "No network connection",
                            Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void selectImage() {
        ImageHelper imageHelper = new ImageHelper(getActivity()
                .getApplicationContext());
        Intent photoPickerIntent = imageHelper.getAndCropImageFromGallery();
        // tempImmage = imageHelper.getTempImagePath;
        startActivityForResult(photoPickerIntent, 1);
    }




    //new crop dialog
    private void selectImageDialog() {
        mCropDialog = new Dialog(getActivity());
        mCropDialog.getWindow().setBackgroundDrawableResource(R.drawable.black_background);
        mCropDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        mCropDialog.setContentView(R.layout.dialog_select_image);

        mCropDialog.getWindow().setLayout(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT);
        //mCropDialog.setTitle(getResources().getString(R.string.select_image));


         cropImg = (ImageView) mCropDialog
                .findViewById(R.id.imageView);
        Button btEdit = (Button) mCropDialog.findViewById(R.id.button_edit);
        Button btnBrowse = (Button) mCropDialog.findViewById(R.id.button_browse);
        btnOk = (TextView) mCropDialog.findViewById(R.id.btn_ok);

        /*final ImageView imgCropped = (ImageView) mCropDialog
                .findViewById(R.id.img_cropped);*/

        try {
            if (selectedImagePath != null) {
                if(!selectedImagePath.isEmpty()){
                    File f= new File(selectedImagePath);
                    if(f.exists()){
                        cropImg.setImageBitmap(BitmapFactory.decodeFile(selectedImagePath));
                    }
                }
            }

            btnBrowse.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,
                            "Complete action using"), CROP_IMAGE);
                }
            });

            final Activity activity = (Activity) getContext();
            btEdit.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(selectedImagePath!=null)
                        if(!selectedImagePath.isEmpty())
                        {
                            CropImage.activity(selectedUri).setAspectRatio(16,9)
                                    .start(activity, AddPrivateEventPage1Fragment.this);
                        }
                }
            });

            btnOk.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCropDialog.dismiss();
                    File file = new File(selectedImagePath);

                    if(file.exists()) {
                        mImgPreview.setImageBitmap(BitmapFactory.decodeFile(selectedImagePath));
                    }

                    mLblCategory.setText(AddPrivateEventActivity.selectedCategory);
                }
            });

            mCropDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }









    // Crop image
    private void showCropImageDialog() {
        mCropDialog = new Dialog(getActivity());
        mCropDialog.getWindow().setBackgroundDrawableResource(R.drawable.black_background);
        mCropDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        mCropDialog.setContentView(R.layout.dialog_crop_image);

        mCropDialog.getWindow().setLayout(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT);
        //mCropDialog.setTitle(getResources().getString(R.string.select_image));


        final CropImageView cropImg = (CropImageView) mCropDialog
                .findViewById(R.id.crop_img);
        cropImg.setFixedAspectRatio(true);
        cropImg.setAspectRatio(16, 9);
        Button btnCrop = (Button) mCropDialog.findViewById(R.id.btn_crop);
        Button btnBrowse = (Button) mCropDialog.findViewById(R.id.btn_browse);
        Button btnRotate = (Button) mCropDialog.findViewById(R.id.btn_rotate);
         btnOk = (Button) mCropDialog.findViewById(R.id.btn_ok);
        mCropDialog.findViewById(R.id.lbl_recommended_image).setSelected(true);
        /*final ImageView imgCropped = (ImageView) mCropDialog
                .findViewById(R.id.img_cropped);*/

        try {
            if (eventImage != null) {
                cropImg.setImageBitmap(eventImage);
            }

            btnBrowse.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,
                            "Complete action using"), CROP_IMAGE);
                }
            });

            btnRotate.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    cropImg.rotateImage(90);
                }
            });

            btnCrop.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // Set width/height
                    try {
                        Bitmap bitmapOrg = BitmapFactory.decodeResource(
                                getResources(), R.drawable.butterfly);

                        Bitmap bitmap = bitmapOrg;
                        Uri tempUri = getImageUri(getContext(), bitmap);
                        File finalFile = new File(getRealPathFromURI(tempUri));
                        if(MaxSizeImage(finalFile.getPath()))
                        {
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

                            AddEventActivity.eventCroppedImage = cropImg
                                    .getCroppedImage();

                            double newW = 0;
                            if (mEventImageW == 0) {
                                Drawable d = getResources().getDrawable(
                                        R.drawable.butterfly);
                                int w = d.getIntrinsicWidth();

                                double x = Double.parseDouble(w + "")
                                        / Double.parseDouble(AddEventActivity.eventCroppedImage
                                        .getWidth() + "");

                                newW = Double.parseDouble(w + "") / x;
                            } else {
                                double x = Double.parseDouble(eventImage.getWidth()
                                        + "")
                                        / Double.parseDouble(AddEventActivity.eventCroppedImage
                                        .getWidth() + "");

                                newW = Double.parseDouble(mEventImageW + "") / x;
                            }

                            if (Math.round(newW) < 1080) {
                                Toast.makeText(
                                        getActivity(),
                                        "Your cropper is "
                                                + Math.round(newW)
                                                + "x"
                                                + Math.round(newW * 9 / 16)
                                                + ". Please upload 1080x608 minimum.",
                                        Toast.LENGTH_LONG).show();
                            } else {
                                if (AddEventActivity.eventCroppedImage.getWidth() < 1080) {
                                    AddEventActivity.eventCroppedImage = getResizedBitmap(
                                            AddEventActivity.eventCroppedImage,
                                            Integer.parseInt(Math.round(newW) + ""),
                                            Integer.parseInt(Math
                                                    .round(newW * 9 / 16) + ""));
                                }

                                // Set width/height
                                int width = cropImg.getLayoutParams().width;
                                /* imgCropped.getLayoutParams().height = (width * 9 / 16);*/
                                cropImg
                                        .setImageBitmap(AddEventActivity.eventCroppedImage);

                                // Set preview image
                                mImgPreview
                                        .setImageBitmap(AddEventActivity.eventCroppedImage);

                                // Show ok button
                                btnOk.setVisibility(View.VISIBLE);
                            }
                        }
                        else
                        {
                            Toast.makeText(getActivity(),
                                    getActivity().getApplicationContext().getString(R.string.image_size_limit),
                                    Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ex) {
                        Toast.makeText(getActivity(),
                                "Error, the image is not valid dimension.",
                                Toast.LENGTH_SHORT).show();
                        // throw new RuntimeException(ex);
                        Logging.writeExceptionFromStackTrace(ex,
                                ex.getMessage());
                    }
                }
            });

            btnOk.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v)
                {
                    mCropDialog.dismiss();
                }
            });

            mCropDialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }

    public boolean MaxSizeImage(String imagePath)
    {
        boolean temp = false;
        File file = new File(imagePath);
        long length = file.length();

        if (length < 4000000) // 1.5 mb 1500000/4000000
            temp = true;

        return temp;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
}
