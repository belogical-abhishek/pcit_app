package com.pcits.events;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.pcits.common.utils.Logging;
import com.pcits.events.adapters.HomeAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.libraries.JSONParser;
import com.pcits.events.libraries.ServiceNotification;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.obj.APIObj;
import com.pcits.events.obj.DealObj;
import com.pcits.events.obj.ListEventObj;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.KenBurnsView;

public class EventAdminActivity extends Activity implements
		OnClickListener {

	private static final String TAG = "EventAdminActivity";
	private ArrayList<DealObj> mArrDeals;

	// Declare object of userFunctions and Utils class
	private UserFunctions userFunction;
	private Utils utils;

	// Create instance of list and ListAdapter
	private ListView list;
	private TextView lblNoResult;
	private HomeAdapter mla;

	private Button btnLoadMore;
	private LinearLayout lytRetry;

	// flag for current page
	private JSONObject json, jsonCurrency;
	private int mCurrentPage = 0;
	private int mPreviousPage;

	private int intLengthData;
	// Declare variable
	private EventAdminActivity self;
	private PullToRefreshListView mPullRefreshListView;

	private LinearLayout llMenu;
	private String client;

	// Declare header
	private KenBurnsView mHeaderPicture;

	private String mRole = "";

	private RelativeLayout mRlProg;

	// Declare OnListSelected interface
	public interface OnDataListSelectedListener {
		public void onListSelected(String idSelected);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		try{
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_event_admin);
			self = this;
	
			Bundle b = getIntent().getExtras();
			if (b != null) {
				mRole = b.getString("role");
			}
	
			if (mArrDeals == null) {
				mArrDeals = new ArrayList<DealObj>();
			}
			// Clear old data
			mArrDeals.clear();
	
			// list = (ListView) v.findViewById(R.id.list);
			lblNoResult = (TextView) findViewById(R.id.lblNoResult);
			lytRetry = (LinearLayout) findViewById(R.id.llRetry);
			// btnRetry = (Button) v.findViewById(R.id.btnRetry);
			llMenu = (LinearLayout) findViewById(R.id.llMenu);
			llMenu.setOnClickListener(this);
	
			// Declare object of userFunctions class
			userFunction = new UserFunctions();
			utils = new Utils(self);
	
			// Create LoadMore button
			btnLoadMore = new Button(self);
			btnLoadMore
					.setBackgroundResource(R.drawable.apptheme_btn_default_holo_light);
			btnLoadMore.setText(getString(R.string.btn_load_more));
			btnLoadMore.setTextColor(getResources().getColor(R.color.text_btn));
	
			// Header
			mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
			mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);
	
			mRlProg = (RelativeLayout) findViewById(R.id.rlt_progress);
			mRlProg.setOnClickListener(null);
	
			// Load data
			new loadFirstListView().execute();
	
			mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.lsvPullToRefresh);
			list = mPullRefreshListView.getRefreshableView();
			// Set a listener to be invoked when the list should be refreshed.
			if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
				mPullRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
							@Override
							public void onRefresh(
									PullToRefreshBase<ListView> refreshView) {
	
								String label = DateUtils.formatDateTime(self,
										System.currentTimeMillis(),
										DateUtils.FORMAT_SHOW_TIME
												| DateUtils.FORMAT_SHOW_DATE
												| DateUtils.FORMAT_ABBREV_ALL);
	
								// Update the LastUpdatedLabel
								refreshView.getLoadingLayoutProxy()
										.setLastUpdatedLabel(label);
								mArrDeals = new ArrayList<DealObj>();
								new loadFirstListView().execute();
	
							}
						});
			} else {
				Toast.makeText(self, getString(R.string.no_internet_connection),
						Toast.LENGTH_SHORT).show();
			}
	
			// Listener to handle load more buttton when clicked
			btnLoadMore.setOnClickListener(new View.OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					try{
						// Starting a new async task
						if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
							json = null;
							new loadMoreListView().execute();
						} else {
							Toast.makeText(self,
									getString(R.string.no_internet_connection),
									Toast.LENGTH_SHORT).show();
						}
					} catch (Exception ex) {
		                throw new RuntimeException(ex);
		            }
				}
			});
	
			// Add an end-of-list listener
			mPullRefreshListView
					.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {
	
						@Override
						public void onLastItemVisible() {
							// Toast.makeText(self, "end page",
							// Toast.LENGTH_SHORT).show();
						}
					});
	
			// Listener to get selected id when list item clicked
			list.setOnItemClickListener(new OnItemClickListener() {
	
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3)
				{
					setStatusPrivateEvent(EventAdminActivity.this,"Event_List_Status",mArrDeals.get(position - 1).getDeal_id());
					Intent i = new Intent(self, EventDashboardActivity.class);
					i.putExtra("dealId", mArrDeals.get(position - 1).getDeal_id());
					startActivity(i);
				}
			});
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	// Load first 10 videos
	private class loadFirstListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			mRlProg.setVisibility(View.VISIBLE);
		}

		protected Void doInBackground(Void... unused) {
			try{
				if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
					getDataFromServer();
				} else {
					getDataFromDB();
				}

			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
            }
			return null;
		}

		protected void onPostExecute(Void unused) {
			Log.e(TAG, "loadFirstListView: " + mArrDeals.size());
			// Condition if data length uder 10 button loadMore is remove
			if (intLengthData < UserFunctions.valueItemsPerPage) {
				list.removeFooterView(btnLoadMore);
			} else {
				list.addFooterView(btnLoadMore);
			}
			if (mArrDeals.size() != 0) {
				// Check paramter notif
				int paramNotif = utils.loadPreferences(utils.UTILS_PARAM_NOTIF);

				// Condition if app start in the first time notif will run
				// in background
				if (paramNotif != 1) {
					utils.saveString(utils.UTILS_NOTIF, mArrDeals.get(0)
							.getDeal_id());
					utils.savePreferences(utils.UTILS_PARAM_NOTIF, 1);
					startService();
				}

				// Adding load more button to lisview at bottom
				lytRetry.setVisibility(View.GONE);
				list.setVisibility(View.VISIBLE);
				lblNoResult.setVisibility(View.GONE);
				// Getting adapter
				List<String> status=getStringArrayPref(EventAdminActivity.this,"Event_List_Status",mArrDeals);
				mla = new HomeAdapter(self, mArrDeals,status);
				mla.notifyDataSetChanged();
				list.setAdapter(mla);
				mPullRefreshListView.onRefreshComplete();

			} else {
				list.removeFooterView(btnLoadMore);
				if (json != null) {
					lblNoResult.setVisibility(View.VISIBLE);
					lytRetry.setVisibility(View.GONE);
				} else {
					lblNoResult.setVisibility(View.GONE);
					lytRetry.setVisibility(View.VISIBLE);
					Toast.makeText(self, getString(R.string.no_connection),
							Toast.LENGTH_SHORT).show();
				}
			}
			super.onPostExecute(unused);
			// Closing progress dialog
			try {
				mRlProg.setVisibility(View.GONE);
			} catch (NullPointerException ex) {
				ex.toString();
			}
		}
	}

	// Load more videos
	private class loadMoreListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			mRlProg.setVisibility(View.VISIBLE);
		}

		protected Void doInBackground(Void... unused) {
			try{
				// Store previous value of current page
				mPreviousPage = mCurrentPage;
				// Increment current page
				mCurrentPage += userFunction.valueItemsPerPage;
				if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
					getDataFromServer();
				} else {
					getDataFromDB();
				}
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
            }
			return (null);
		}

		protected void onPostExecute(Void unused) {
			// Condition if data length uder 10 button loadMore is remove
			if (intLengthData < userFunction.valueItemsPerPage)
				list.removeFooterView(btnLoadMore);
			if (json != null) {
				// Get listview current position - used to maintain scroll
				// position
				int currentPosition = list.getFirstVisiblePosition();
				lytRetry.setVisibility(View.GONE);
				// Appending new data to menuItems ArrayList
				List<String> status=getStringArrayPref(EventAdminActivity.this,"Event_List_Status",mArrDeals);
				mla = new HomeAdapter(self, mArrDeals,status);
				mla.notifyDataSetChanged();
				// list.setAdapter(mla);
				// Setting new scroll position
				list.setSelectionFromTop(currentPosition + 1, 0);
				// mPullRefreshListView.onRefreshComplete();

			} else {
				if (mArrDeals != null) {
					mCurrentPage = mPreviousPage;
					lytRetry.setVisibility(View.GONE);
				} else {
					lytRetry.setVisibility(View.VISIBLE);
					Toast.makeText(self, getString(R.string.no_connection),
							Toast.LENGTH_SHORT).show();
				}

			}

			// Close progress bar
			mRlProg.setVisibility(View.GONE);
		}
	}

	// Method to get Data from Server
	public void getDataFromServer() {

		try {
			if (GlobalValue.myClient != null) {
				client = GlobalValue.myClient.getUsername();
			} else {
				client = "";
			}
			jsonCurrency = userFunction.currency(self);
			ArrayList<DealObj> arr = null;
			if (mRole.equals("0")) {
				json = userFunction.latestDeals(client, mCurrentPage, self);
				arr = JSONParser.parserDeal("latestDeals", json.toString());
			} else {
				json = userFunction.dealsByUser(mRole, mCurrentPage, self);
				arr = JSONParser.parserDeal(userFunction.array_deals_by_user,
						json.toString());
			}

			mArrDeals.addAll(arr);
			intLengthData = mArrDeals.size();
			if (jsonCurrency != null) {
				JSONArray currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);

			}

			Log.e(TAG, "getDataFromServer: " + mArrDeals.size());

		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public void getDataFromDB() {
		try {
			String apiDeals = UserFunctions.URL_ALL_DEALS + mCurrentPage;
			APIObj allDealInfos = DatabaseUtility.getResuftFromApi(self,
					apiDeals);
			Log.e("FragmentHome", "Database: " + allDealInfos);
			String jsonAllDeal = "";
			if (allDealInfos != null) {
				jsonAllDeal = allDealInfos.getmResult();
			}
			ArrayList<DealObj> arr = null;
			if (mRole.equals("0")) {
				json = userFunction.latestDeals(client, mCurrentPage, self);
				arr = JSONParser.parserDeal("latestDeals", json.toString());
			} else {
				json = userFunction.dealsByUser(mRole, mCurrentPage, self);
				arr = JSONParser.parserDeal(userFunction.array_deals_by_user,
						json.toString());
			}
			mArrDeals.addAll(arr);
			intLengthData = mArrDeals.size();
			Log.e("url", "All deal: " + jsonAllDeal);

			APIObj currencyInfos = DatabaseUtility.getResuftFromApi(self,
					UserFunctions.URL_CURRENCY);
			Log.e("FragmentHome", "Database: " + currencyInfos);
			String jsonCurr = "";
			if (currencyInfos != null) {
				jsonCurr = currencyInfos.getmResult();
			}
			jsonCurrency = new JSONObject(jsonCurr);
			Log.e("url", "Currency: " + jsonCurr);

			if (jsonCurrency != null) {
				JSONArray currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);
			}

		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (NullPointerException ex) {
			throw new RuntimeException(ex);
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		list.setAdapter(null);
		super.onDestroy();

	}

	public void startService() {

		Intent myIntent = new Intent(self, ServiceNotification.class);

		PendingIntent pendingIntent = PendingIntent.getService(self, 0,
				myIntent, 0);

		AlarmManager alarmManager = (AlarmManager) self
				.getSystemService(self.ALARM_SERVICE);

		Calendar calendar = Calendar.getInstance();
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				calendar.getTimeInMillis(), 30 * 1000, pendingIntent);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == llMenu) {
			finish();
		}
	}


	private void setStatusPrivateEvent(android.content.Context context, String key,String deal_id)
	{
		//Event_List_Status-key
		try
		{
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(EventAdminActivity.this);
			SharedPreferences.Editor editor = prefs.edit();
			JSONArray a;
			try
			{
				String json = prefs.getString(key, null);
				a = new JSONArray(json);
			}
			catch (Exception e)
			{
				a = new JSONArray();
				e.printStackTrace();
			}

			ListEventObj listEventObj = null;
			JSONObject b=new JSONObject();

			try
			{
				b.put("event_id", deal_id);

			}
			catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			a.put(b);

			if (a!=null)
			{
				editor.putString(key, a.toString());
			}
			else
			{
				editor.putString(key, null);
			}
			editor.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public List<String> getStringArrayPref(Context context, String key, List<DealObj> list)
	{
		List<String> listEventStatus=new ArrayList<>(list.size());
		for (int i = 0; i < list.size(); i++)
		{
			listEventStatus.add("0");
		}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		String json = prefs.getString(key, null);
		List<ListEventObj> listEvents=new ArrayList<>();
		if (json != null)
		{
			try
			{
				JSONArray a = null;
				try
				{
					a = new JSONArray(json);
					for (int i = 0; i < list.size(); i++)
					{
						DealObj ss=list.get(i);
						for(int j=0;j<a.length();j++)
						{
							JSONObject jsonObject=a.getJSONObject(j);
							if(ss.getDeal_id().equalsIgnoreCase(jsonObject.getString("event_id")))
							{
								listEventStatus.set(i,ss.getDeal_id());
							}
							else
							{
								if(listEventStatus.get(i).equalsIgnoreCase("0"))
								{
									listEventStatus.set(i,"0");
								}
							}

						}
					}
				}
				catch (JSONException e)
				{
					e.printStackTrace();
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return listEventStatus;
	}
}
