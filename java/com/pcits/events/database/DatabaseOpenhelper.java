package com.pcits.events.database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.pcits.common.utils.Logging;
import com.pcits.events.config.DatabaseConfig;
import com.pcits.events.utils.SmartLog;

public class DatabaseOpenhelper extends SQLiteOpenHelper {

	private static final String TAG = "DatabaseOpenhelper";
	private Context context = null;
	private DatabaseConfig databaseConfig = null;

	public DatabaseOpenhelper(Context context, DatabaseConfig config) {
		super(context, config.getDB_NAME(), null, config.getDB_VERSION());
		try{
			this.context = context;
			this.databaseConfig = config;
	
			if (!isDatabaseExist()) {
	
				// Create blank file
				getReadableDatabase();
				close();
				copyDatabase();
			}
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try{
			// Create Api table.
			String strCreateApiTbl = "create table if not exists "
					+ DBKeyConfig.TABLE_API + "(" + DBKeyConfig.KEY_API_ID + ","
					+ DBKeyConfig.KEY_API_API + "," + DBKeyConfig.KEY_API_RESULT
					+ ")";
			db.execSQL(strCreateApiTbl);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (newVersion > oldVersion) {
			try {
				copyDatabase();
			} catch(Exception ex){
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			}
		}
	}

	/**
	 * Copy database to application directory on SD card
	 * 
	 * @throws IOException
	 * 
	 * @throws IOException
	 */
	private void copyDatabase() throws IOException {
		try{
			SmartLog.log(TAG, "Copy database into application directory");
			SmartLog.log(TAG, databaseConfig.getDatabasefullpath());
			InputStream is = context.getAssets().open(databaseConfig.getDB_NAME());
			OutputStream os = new FileOutputStream(
					databaseConfig.getDatabasefullpath());
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
			os.flush();
			os.close();
			is.close();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Check database is exist
	 * 
	 * @return
	 */
	private boolean isDatabaseExist() {
		SQLiteDatabase checkDB = null;
		try {

			File file = new File(databaseConfig.getDatabasefullpath());
			if (file.exists() && !file.isDirectory()) {
				checkDB = SQLiteDatabase.openDatabase(
						databaseConfig.getDatabasefullpath(), null,
						SQLiteDatabase.NO_LOCALIZED_COLLATORS
								| SQLiteDatabase.OPEN_READONLY);
				SmartLog.log(
						TAG,
						"Database is exist! "
								+ databaseConfig.getDatabasefullpath()
								+ " ======================");
			}

		} catch(Exception ex){
			Logging.values = databaseConfig.getDatabasefullpath();
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		finally{
			if (checkDB != null) {
				checkDB.close();
			}
		}
		return (checkDB != null) ? true : false;
	}

}
