/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pcits.events;

import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.ClientInfo;
import com.pcits.events.obj.UserObj;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedBold;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;
import com.pcits.events.wizard.model.AbstractWizardModel;
import com.pcits.events.wizard.model.ICustomPage;
import com.pcits.events.wizard.model.ModelCallbacks;
import com.pcits.events.wizard.model.Page;
import com.pcits.events.wizard.ui.PageFragmentCallbacks;
import com.pcits.events.wizard.ui.ReviewFragment;
import com.pcits.events.wizard.ui.StepPagerStrip;

public class ProfileActivity extends FragmentActivity implements
		PageFragmentCallbacks, ReviewFragment.Callbacks, ModelCallbacks {

	private static final String TAG = "ProfileActivity";
	private ProfileActivity self;

	private LinearLayout mLlHome, llChangePass;

	private ViewPager mPager;
	private MyPagerAdapter mPagerAdapter;

	private boolean mEditingAfterReview;

	private AbstractWizardModel mWizardModel = new ProfileWizardModel(this);

	private boolean mConsumePageSelectedEvent;

	private Button mNextButton;
	private Button mPrevButton;

	private List<Page> mCurrentPageSequence;
	private StepPagerStrip mStepPagerStrip;

	private int mPrevScrollStep = 0;

	public static ClientInfo tempClient;
	public static UserObj tempAdmin;

	public static Bitmap bmAvatar;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile_2);
		self = this;
		try {
			if (GlobalValue.myUser != null) {
				try {
					tempAdmin = (UserObj) GlobalValue.myUser.clone();
				} catch (Exception ex) {
					Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
				}
			} else {
				try {
					tempClient = (ClientInfo) GlobalValue.myClient.clone();
				} catch (Exception ex) {
					Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
				}
			}

			if (savedInstanceState != null) {
				mWizardModel.load(savedInstanceState.getBundle("model"));
			}

			mWizardModel.registerListener(this);

			initUI();

			onPageTreeChanged();
			updateBottomBar();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	private void initUI() {
		try {
			llChangePass = (LinearLayout) findViewById(R.id.llChangePass);
			if (GlobalValue.myClient != null) {
				if (Integer.parseInt(GlobalValue.myClient.getType()) == 1) {
					llChangePass.setVisibility(View.GONE);
				} else {
					llChangePass.setVisibility(View.VISIBLE);
				}
			}
			mLlHome = (LinearLayout) findViewById(R.id.llMenu);
			mNextButton = (Button) findViewById(R.id.next_button);
			mPrevButton = (Button) findViewById(R.id.prev_button);

			mPager = (ViewPager) findViewById(R.id.pager);
			mStepPagerStrip = (StepPagerStrip) findViewById(R.id.strip);
			mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
			mPager.setAdapter(mPagerAdapter);

			// Should call this method end of declaring UI.
			initControl();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void initControl() {
		try {
			mStepPagerStrip
					.setOnPageSelectedListener(new StepPagerStrip.OnPageSelectedListener() {
						@Override
						public void onPageStripSelected(int position) {
							try {
								position = Math.min(
										mPagerAdapter.getCount() - 1, position);

								if (mPager.getCurrentItem() != position) {

									int prev = mPager.getCurrentItem();
									if (prev < mCurrentPageSequence.size()) {
										ICustomPage pagePrev = (ICustomPage) mCurrentPageSequence
												.get(prev);
										pagePrev.saveState();
									}

									mPager.setCurrentItem(position);

									int next = position;
									if (next < mCurrentPageSequence.size()) {
										ICustomPage pageNext = (ICustomPage) mCurrentPageSequence
												.get(next);
										pageNext.restoreState();
									}

								}
							} catch (Exception ex) {
								throw new RuntimeException(ex);
							}

						}
					});

			mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
				@Override
				public void onPageSelected(int position) {
					int prev = mPrevScrollStep;
					int next = position;
					try {
						if (prev < mCurrentPageSequence.size()) {
							ICustomPage pagePrev = (ICustomPage) mCurrentPageSequence
									.get(prev);
							pagePrev.saveState();
						}

						mStepPagerStrip.setCurrentPage(position);

						if (next < mCurrentPageSequence.size()) {
							ICustomPage pageNext = (ICustomPage) mCurrentPageSequence
									.get(next);
							pageNext.restoreState();
						}

						mPrevScrollStep = position;

						Log.d("position change", "prev: " + prev + "; next:"
								+ next);

						if (mConsumePageSelectedEvent) {
							mConsumePageSelectedEvent = false;
							return;
						}

						mEditingAfterReview = false;
						updateBottomBar();
					} catch (Exception ex) {
						Logging.values = "posittion from : "
								+ Integer.toString(prev) + " to "
								+ Integer.toString(next);
						throw new RuntimeException(ex);
					}
				}
			});

			mNextButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						if (mPager.getCurrentItem() == mCurrentPageSequence
								.size()) {
							showDialogConfirmCreation();
						} else {
							ICustomPage page = (ICustomPage) mCurrentPageSequence
									.get(mPager.getCurrentItem());
							page.saveState();

							if (mEditingAfterReview) {
								mPager.setCurrentItem(mPagerAdapter.getCount() - 1);
							} else {
								mPager.setCurrentItem(mPager.getCurrentItem() + 1);
							}
						}
					} catch (Exception ex) {
						throw new RuntimeException(ex);
					}
				}
			});

			mPrevButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					ICustomPage page = (ICustomPage) mCurrentPageSequence
							.get(mPager.getCurrentItem() - 1);
					page.restoreState();

					mPager.setCurrentItem(mPager.getCurrentItem() - 1);
				}
			});

			mLlHome.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					onBackPressed();
				}
			});

			llChangePass.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(ProfileActivity.this,
							ActivityChangePassword.class);
					startActivity(i);
				}
			});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void showDialogConfirmCreation() {
		final Dialog dialog = new Dialog(self);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_confirmation);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		TextViewRobotoCondensedBold title = (TextViewRobotoCondensedBold) dialog
				.findViewById(R.id.dialog_title);
		TextViewRobotoCondensedRegular message = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.dialog_message);

		title.setText(getResources().getString(R.string.update_profile));
		message.setText(getResources().getString(
				R.string.do_you_want_to_update_your_profile));

		Button btnNegative = (Button) dialog.findViewById(R.id.btn_nagative);
		Button btnPositive = (Button) dialog.findViewById(R.id.btn_positive);

		btnNegative.setText(getResources().getString(R.string.no));
		btnPositive.setText(getResources().getString(R.string.yes));

		btnNegative.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		btnPositive.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Coding to create event here

				dialog.dismiss();

				if (GlobalValue.myClient != null) {
					updateClient();
				} else if (GlobalValue.myUser != null) {
					updateAdmin();
				}
			}
		});

		dialog.show();
	}

	@Override
	public void onPageTreeChanged() {
		try {
			mCurrentPageSequence = mWizardModel.getCurrentPageSequence();
			recalculateCutOffPage();
			mStepPagerStrip.setPageCount(mCurrentPageSequence.size() + 1); // +
																			// 1
																			// =
																			// review
																			// step
			mPagerAdapter.notifyDataSetChanged();
			updateBottomBar();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	private void updateBottomBar() {
		int position = mPager.getCurrentItem();
		if (position == mCurrentPageSequence.size()) {
			mNextButton.setText(R.string.submit);
			// mNextButton.setBackgroundResource(R.drawable.finish_background);
			// mNextButton.setTextAppearance(this,
			// R.style.TextAppearanceFinish);
			mNextButton.setBackgroundResource(R.drawable.bg_last_button);
		} else {
			mNextButton.setText(mEditingAfterReview ? R.string.review
					: R.string.next);
			// mNextButton
			// .setBackgroundResource(R.drawable.selectable_item_background);
			mNextButton.setBackgroundResource(R.drawable.bg_button);
			TypedValue v = new TypedValue();
			getTheme().resolveAttribute(android.R.attr.textAppearanceMedium, v,
					true);
			// mNextButton.setTextAppearance(this, v.resourceId);
			mNextButton.setEnabled(position != mPagerAdapter.getCutOffPage());
		}

		mPrevButton
				.setVisibility(position <= 0 ? View.INVISIBLE : View.VISIBLE);
	}

	@Override
	protected void onDestroy() {
		try {
			super.onDestroy();
			mWizardModel.unregisterListener(this);

			tempAdmin = null;
			tempClient = null;
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		try {
			super.onSaveInstanceState(outState);
			outState.putBundle("model", mWizardModel.save());
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	@Override
	public AbstractWizardModel onGetModel() {
		return mWizardModel;
	}

	@Override
	public void onEditScreenAfterReview(String key) {
		for (int i = mCurrentPageSequence.size() - 1; i >= 0; i--) {
			if (mCurrentPageSequence.get(i).getKey().equals(key)) {
				mConsumePageSelectedEvent = true;
				mEditingAfterReview = true;
				mPager.setCurrentItem(i);
				updateBottomBar();
				break;
			}
		}
	}

	@Override
	public void onPageDataChanged(Page page) {
		if (page.isRequired()) {
			if (recalculateCutOffPage()) {
				mPagerAdapter.notifyDataSetChanged();
				updateBottomBar();
			}
		}
	}

	@Override
	public Page onGetPage(String key) {
		try {
			return mWizardModel.findByKey(key);
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			return null;
		}
	}

	private boolean recalculateCutOffPage() {
		// Cut off the pager adapter at first required page that isn't completed
		int cutOffPage = mCurrentPageSequence.size() + 1;
		for (int i = 0; i < mCurrentPageSequence.size(); i++) {
			Page page = mCurrentPageSequence.get(i);
			if (page.isRequired() && !page.isCompleted()) {
				cutOffPage = i;
				break;
			}
		}

		if (mPagerAdapter.getCutOffPage() != cutOffPage) {
			mPagerAdapter.setCutOffPage(cutOffPage);
			return true;
		}

		return false;
	}

	public class MyPagerAdapter extends FragmentStatePagerAdapter {
		private int mCutOffPage;
		private Fragment mPrimaryItem;

		public MyPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int i) {
			if (i >= mCurrentPageSequence.size()) {
				return new ReviewFragment();
			}

			return mCurrentPageSequence.get(i).createFragment();
		}

		@Override
		public int getItemPosition(Object object) {
			// TODO: be smarter about this
			if (object == mPrimaryItem) {
				// Re-use the current fragment (its position never changes)
				return POSITION_UNCHANGED;
			}

			return POSITION_NONE;
		}

		@Override
		public void setPrimaryItem(ViewGroup container, int position,
				Object object) {
			super.setPrimaryItem(container, position, object);
			mPrimaryItem = (Fragment) object;
		}

		@Override
		public int getCount() {
			if (mCurrentPageSequence == null) {
				return 0;
			}
			return Math.min(mCutOffPage + 1, mCurrentPageSequence.size() + 1);
		}

		public void setCutOffPage(int cutOffPage) {
			if (cutOffPage < 0) {
				cutOffPage = Integer.MAX_VALUE;
			}
			mCutOffPage = cutOffPage;
		}

		public int getCutOffPage() {
			return mCutOffPage;
		}
	}

	private void updateClient() {
		// Update process.
		ModelManager.updateClient(self, tempClient, bmAvatar, true,
				new ModelManagerListener() {

					@Override
					public void onSuccess(Object object) {
						String json = (String) object;
						try {
							boolean result = ParserUtility.updateAccount(json);
							if (result) {
								GlobalValue.myClient = ParserUtility
										.parserAccount(json);

								try {
									tempClient = (ClientInfo) GlobalValue.myClient
											.clone();
								} catch (Exception ex) {
									Logging.writeExceptionFromStackTrace(ex,
											ex.getMessage());
								}
								Toast.makeText(self, "Updated successfully!",
										Toast.LENGTH_SHORT).show();
							}
						} catch (Exception ex) {
							Logging.writeExceptionFromStackTrace(ex,
									ex.getMessage());
						}
					}

					@Override
					public void onError() {
					}
				});
	}

	private void updateAdmin() {
		// Update process.
		ModelManager.updateAdmin(self, tempAdmin, bmAvatar, true,
				new ModelManagerListener() {

					@Override
					public void onSuccess(Object object) {
						String json = (String) object;
						try {
							boolean result = ParserUtility.updateAccount(json);

							if (result) {
								GlobalValue.myUser = ParserUtility
										.parserUser(json);
								try {
									tempAdmin = (UserObj) GlobalValue.myUser
											.clone();
								} catch (Exception ex) {
									Logging.writeExceptionFromStackTrace(ex,
											ex.getMessage());
								}
								Toast.makeText(self, "Updated successfully!",
										Toast.LENGTH_SHORT).show();
							}
						} catch (Exception ex) {
							Logging.writeExceptionFromStackTrace(ex,
									ex.getMessage());
						}
					}

					@Override
					public void onError() {
					}
				});
	}
}
