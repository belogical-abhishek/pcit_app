package com.pcits.events;

/**
 * Created by EnvisioDevs on 30-11-2017.
 */


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.pcits.common.utils.Logging;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.obj.APIObj;
import com.pcits.events.obj.AttendedObj;
import com.pcits.events.obj.DealObj;
import com.pcits.events.obj.GcmObj;
import com.pcits.events.obj.PurchaseObj;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedBold;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class ExpandableToolBarActivity extends AppCompatActivity  implements View.OnClickListener {

    private ArrayList<String> stringArrayList;
    private String TAG= "ExpandableToolBarActivity";
    private ListView recyclerView;
    // Declare object of userFunction, JSONObject, and Utils class
    private UserFunctions userFunction;
    private JSONObject json;
    private Utils utils;
    // format date
    private SimpleDateFormat sdf, sdf1;
    private Date da;
    // Declare variables to store data
    private String mGetDealId;
    private String mTnCid;
    private String mTitle;
    private String mUrl;
    private String mImgDeal;
    private Double mDblLatitude;
    private Double mDblLongitude;
    private String mUsername;

    final String mimeType = "text/html";
    final String encoding = "UTF-8";
   // private RecyclerAdapter adapter;


    private TextView lblCompany, lblTitle, lblAddress, lblAfterDiscount,
            lblStartValue, lblDiscount, lblSave, lblAttending;
    private TextViewRobotoCondensedRegular lblDateStart, lblDateEnd,
            lblTimeStart, lblTimeEnd, lblDesc;
    private ImageView imgThumbnail;
    private LinearLayout lytDetail, lytRetry;
    // private RelativeLayout layUser;
    private Button btnRetry, btnGet, btnGoogleCal, btnWinner;
    private AttendedObj attend = null;
    private GcmObj gcmInfo = null;
    private PurchaseObj purchase = null;
    int paymentMethod = 0;

    private Dialog dialog, numberPeople;
    private int sl, quantity;
    private String username, strDate;

    private RelativeLayout mRltProgress;
    private LinearLayout mLlAdminButtons;
    private Button mBtnScan, mBtnDashboard;
    // Get width/height
    private Display display;
    private int mImgHeight = 0;

    private AQuery aq;
    private LinearLayout mLlBottom;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar_detail, menu);
        return true;
    }

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;

    // note that these credentials will differ between live & sandbox
    // environments.
    private static final String CONFIG_CLIENT_ID = "ASyaxRDC-Wy2QOCymQ7HpAAe-iD70tJQu9nXKyMfLySYr4OQd2cEtNZU8lyE";

    private static final int REQUEST_CODE_PAYMENT = 1;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("Example Merchant")
            .merchantPrivacyPolicyUri(
                    Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(
                    Uri.parse("https://www.example.com/legal"));


    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail_collapse);

        //helper.initActionBar(this);

        display = this.getWindowManager().getDefaultDisplay();
        mImgHeight = display.getWidth() * 9 / 16;



        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        strDate = sdf.format(c.getTime());
        // Declare object of userFunction and utils class.
        userFunction = new UserFunctions();
        utils = new Utils(this);

//        getActionBar().setDisplayHomeAsUpEnabled(true);
        InitUI();

        String extraString = "";
        String extraFromString = "";




        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        if (GlobalValue.dealsObj == null) {
            Intent i = getIntent();
            mGetDealId = i.getStringExtra(utils.EXTRA_DEAL_ID);
        } else if (GlobalValue.dealsObj != null) {
            mGetDealId = GlobalValue.dealsObj.getDeal_id();
        }



        // Change actionbar title
        int titleId = Resources.getSystem().getIdentifier("action_bar_title",
                "id", "android");

        if (GlobalValue.dealsObj != null) {
            getData();
        } else {
            new getDataAsync().execute();
        }




    }

    private void getCheckAttding() {
        try {
            ModelManager.getCheckAttending(this,
                    GlobalValue.myClient.getUsername(), mGetDealId, false,
                    new ModelManagerListener() {

                        @Override
                        public void onSuccess(Object object) {
                            // TODO Auto-generated method stub
                            String json = (String) object;
                            GlobalValue.arrCheckAttending = ParserUtility
                                    .getListAttending(json);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub

                        }
                    });
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    protected void onResume() {
        try {
            // TODO Auto-generated method stub
            super.onResume();

            // Call asynctask class
            // new getDataAsync().execute();
            if (GlobalValue.dealsObj != null) {
                getData();
            } else {
                new getDataAsync().execute();
            }

            if (GlobalValue.myClient != null) {
                getCheckAttding();
            }
        } catch (Exception ex) {
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);

        calcImageDimension();
    }

    private void InitUI() {
        lblAttending = (TextView) findViewById(R.id.lblAttend);
        lblCompany = (TextView) findViewById(R.id.lblCompany);
        lblTitle = (TextView) findViewById(R.id.lblTitle);
        lblTitle.setSelected(true);
        lblAddress = (TextView) findViewById(R.id.lblAddress);
        lblAfterDiscount = (TextView) findViewById(R.id.lblAfterDiscountValue);
        lblStartValue = (TextView) findViewById(R.id.lblStartValue);
        lblDiscount = (TextView) findViewById(R.id.lblDiscountValue);
        // lblSave = (TextView) findViewById(R.id.lblSaveValue);
        lblDateStart = (TextViewRobotoCondensedRegular) findViewById(R.id.lblStartDateValue);
        lblDateEnd = (TextViewRobotoCondensedRegular) findViewById(R.id.lblEndDateValue);
        lblTimeStart = (TextViewRobotoCondensedRegular) findViewById(R.id.lblStartTimeValue);
        lblTimeEnd = (TextViewRobotoCondensedRegular) findViewById(R.id.lblEndTimeValue);
        lytRetry = (LinearLayout) findViewById(R.id.lytRetry);
        lytDetail = (LinearLayout) findViewById(R.id.lytDetail);
        btnRetry = (Button) findViewById(R.id.btnRetry);
        btnGet = (Button) findViewById(R.id.btnGet);
        btnGoogleCal = (Button) findViewById(R.id.btnGoogleCalendar);
        imgThumbnail = (ImageView) findViewById(R.id.imgThumbnail);
        calcImageDimension();

        // adView = (AdView) this.findViewById(R.id.adView);
        lblDesc = (TextViewRobotoCondensedRegular) findViewById(R.id.lblDesc);

        mRltProgress = (RelativeLayout) findViewById(R.id.rlt_progress);
        mRltProgress.setOnClickListener(null);
        mLlAdminButtons = (LinearLayout) findViewById(R.id.ll_buttons);
        mBtnDashboard = (Button) findViewById(R.id.btnDashboard);
        mBtnDashboard.setSelected(true);
        mBtnScan = (Button) findViewById(R.id.btnScan);
        btnWinner = (Button) findViewById(R.id.btnWinner);
        mLlBottom = (LinearLayout) findViewById(R.id.ll_bottom);

        btnRetry.setOnClickListener(this);
        btnGet.setOnClickListener(this);
        mBtnDashboard.setOnClickListener(this);
        mBtnScan.setOnClickListener(this);
        btnGoogleCal.setOnClickListener(this);
        btnWinner.setOnClickListener(this);
        lblDesc.setOnClickListener(this);
        imgThumbnail.setOnClickListener(this);
        lblTitle.setOnClickListener(this);
    }

    private void calcImageDimension() {
        imgThumbnail.getLayoutParams().height = mImgHeight;
    }

    // AsyncTask to Get Data from Server and put it on View Object
    public class getDataAsync extends AsyncTask<Void, Void, Void> {

        // ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            // Show progress dialog when fetching data from database
            // progress = ProgressDialog.show(ActivityDetail.this, "",
            // getString(R.string.loading_data), true);

            mRltProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            // Method to get Data from Server
            if (NetworkUtility.getInstance(ExpandableToolBarActivity.this)
                    .isNetworkAvailable()) {
                try{
                    getDataFromServer();
                }catch (Exception e){
                    e.printStackTrace();
                    getDataFromDB();
                }
            } else {
                getDataFromDB();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            // progress.dismiss();
            getData();
        }

    }

    // Method to get Data from Server
    public void getDataFromServer() {
        try {
            json = userFunction.dealDetail(mGetDealId, this);
            if (json != null) {
                // JSONArray dataDealsArray = json
                // .getJSONArray(userFunction.array_deal_detail);

                // JSONObject dealsObject = dataDealsArray.getJSONObject(0);
                GlobalValue.dealsObj = new DealObj();
                GlobalValue.dealsObj = ParserUtility.detailDeals(
                        userFunction.array_deal_detail, json.toString());

            }

        } catch (NullPointerException e) {
            if (Logging.debugFile.exists()) {
                e.printStackTrace();
            }
            throw new RuntimeException(e);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    // Method to get Data from database
    private void getDataFromDB() {
        try {
            String apiDealDetail = UserFunctions.URL_DEAL_DETAIL + mGetDealId;
            APIObj dealDetail = DatabaseUtility.getResuftFromApi(this,
                    apiDealDetail);
            String jsonDealDetail = "";
            if (dealDetail != null) {
                jsonDealDetail = dealDetail.getmResult();
            }

            json = new JSONObject(jsonDealDetail);
            if (json != null) {
                GlobalValue.dealsObj = new DealObj();
                GlobalValue.dealsObj = ParserUtility.detailDeals(
                        userFunction.array_deal_detail, json.toString());
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void getData() {
        try {
            if (GlobalValue.dealsObj != null) {
                // Hide bottom bar when previewing
                Intent i = getIntent();
                if (i.getBooleanExtra("isPreview", false)) {
                    mLlBottom.setVisibility(View.GONE);
                }

                // Display Data
                aq = new AQuery(ExpandableToolBarActivity.this);

                lytDetail.setVisibility(View.VISIBLE);
                lytRetry.setVisibility(View.GONE);
                lblCompany.setText(GlobalValue.dealsObj.getCompany() + ",");
                lblTitle.setText(GlobalValue.dealsObj.getTitle());
                lblAddress.setText(GlobalValue.dealsObj.getAddress());

                if (GlobalValue.dealsObj.getAfter_discount_value().equals("0")) {
                    lblAfterDiscount.setText("Free");
                } else {
                    lblAfterDiscount.setText(Utils.mCurrency + " "
                            + GlobalValue.dealsObj.getAfter_discount_value());
                }
                lblAttending.setText("Attending: "
                        + GlobalValue.dealsObj.getAttend());
                lblStartValue.setText(Utils.mCurrency + " "
                        + GlobalValue.dealsObj.getStart_value());
                lblStartValue.setPaintFlags(lblStartValue.getPaintFlags()
                        | Paint.STRIKE_THRU_TEXT_FLAG);

                if(GlobalValue.dealsObj.getDiscount()!=0)
                    lblDiscount.setText("Discount "
                            + GlobalValue.dealsObj.getDiscount() + "%");
                else {
                    Double percent = (GlobalValue.dealsObj.getAfter_discount_value()/GlobalValue.dealsObj.getStart_value())*100;
                    lblDiscount.setText("Dicount "+String.format("%.2f",percent)+ "%");
                }

                // lblSave.setText(GlobalValue.dealsObj.getSave_value() + " "
                // + Utils.mCurrency);
                lblDateStart.setText(" " + GlobalValue.dealsObj.getStart_date());
                lblDateEnd.setText(" " + GlobalValue.dealsObj.getEnd_date());
                lblTimeStart.setText("  "
                        + GlobalValue.dealsObj.getStart_time());
                lblTimeEnd.setText("  " + GlobalValue.dealsObj.getEnd_time());
                // lblDesc.setMovementMethod(LinkMovementMethod.getInstance());
                setTextViewHTML(lblDesc,GlobalValue.dealsObj.getDescription());
                username = GlobalValue.dealsObj.getUsername();
                // Load data from url

                // Set Image thumbnail from Server
                if (GlobalValue.dealsObj.getImage() != null) {
                    aq.id(imgThumbnail).image(
                            UserFunctions.URLAdmin
                                    + GlobalValue.dealsObj.getImage(), false,
                            true, 300, 0, new BitmapAjaxCallback() {
                                @Override
                                protected void callback(String url,
                                                        ImageView iv, Bitmap bm,
                                                        AjaxStatus status) {
                                    if (bm != null) {
                                        // holder.imgThumbnail.setImageBitmap(bm);
                                        BitmapDrawable bmd = new BitmapDrawable(
                                                bm);
                                        imgThumbnail.setBackgroundDrawable(bmd);
                                    }
                                }
                            });
                } else {
                    imgThumbnail
                            .setImageBitmap(AddEventActivity.eventCroppedImage);
                }

                try {
                    DealObj dealObj = null;
                    if (DatabaseUtility.checkExistsDeals(ExpandableToolBarActivity.this,
                            mGetDealId)) {
                        Log.e("", "DATA " + "data da co du lieu");
                    } else {
                        dealObj = new DealObj();
                        dealObj.setDeal_id(mGetDealId);
                        dealObj.setTitle(GlobalValue.dealsObj.getTitle());
                        dealObj.setCompany(GlobalValue.dealsObj.getCompany());
                        dealObj.setStart_date(GlobalValue.dealsObj
                                .getStart_date());
                        dealObj.setEnd_date(GlobalValue.dealsObj.getEnd_date());
                        dealObj.setStart_time(GlobalValue.dealsObj
                                .getStart_time());
                        dealObj.setEnd_time(GlobalValue.dealsObj.getEnd_time());
                        dealObj.setAfter_discount_value(GlobalValue.dealsObj
                                .getAfter_discount_value());
                        dealObj.setStart_value(GlobalValue.dealsObj
                                .getStart_value());
                        dealObj.setAttend(GlobalValue.dealsObj.getAttend());
                        dealObj.setQuantity(GlobalValue.dealsObj.getQuantity());
                        dealObj.setStartTimeStamp(GlobalValue.dealsObj
                                .getStartTimeStamp());
                        dealObj.setEndTimeStamp(GlobalValue.dealsObj
                                .getEndTimeStamp());
                        dealObj.setImage(GlobalValue.dealsObj.getImage());

                        DatabaseUtility.insertDeals(ExpandableToolBarActivity.this,
                                dealObj);
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                 //   Log.e(TAG, e.getMessage());
                    e.printStackTrace();
                }

                if (GlobalValue.myUser != null) {
                    if (Integer.parseInt(GlobalValue.myUser.getRole()) == 0) {
                        mLlAdminButtons.setVisibility(View.VISIBLE);
                        btnWinner.setVisibility(View.VISIBLE);
                        btnGet.setText(getResources().getString(R.string.edit));
                        btnGet.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
                                AddEventActivity.isDeal = true;
                                // AddEventActivity.dealObj =
                                // GlobalValue.dealsObj;
                                Intent i = new Intent(ExpandableToolBarActivity.this,
                                        AddEventActivity.class);
                                startActivity(i);
                                ExpandableToolBarActivity.this.overridePendingTransition(
                                        R.anim.slide_in_left,
                                        R.anim.slide_out_left);

                            }
                        });
                    } else {
                        if (GlobalValue.myUser.getUsername().equalsIgnoreCase(
                                username)) {
                            mLlAdminButtons.setVisibility(View.GONE);
                            btnWinner.setVisibility(View.VISIBLE);
                            btnGet.setText(getResources().getString(
                                    R.string.edit));
                            btnGet.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    AddEventActivity.isDeal = true;
                                    // AddEventActivity.dealObj =
                                    // GlobalValue.dealsObj;
                                    Intent i = new Intent(ExpandableToolBarActivity.this,
                                            AddEventActivity.class);
                                    startActivity(i);
                                    ExpandableToolBarActivity.this
                                            .overridePendingTransition(
                                                    R.anim.slide_in_left,
                                                    R.anim.slide_out_left);
                                }
                            });

                        }
                    }
                } else {
                    mLlAdminButtons.setVisibility(View.GONE);
                    btnGet.setText(getResources().getString(R.string.get_it));
                }

            } else {
                lytDetail.setVisibility(View.GONE);
                lytRetry.setVisibility(View.VISIBLE);
                Toast.makeText(ExpandableToolBarActivity.this,
                        getString(R.string.no_connection), Toast.LENGTH_SHORT)
                        .show();

            }

            mRltProgress.setVisibility(View.GONE);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    // Listener for option menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        Intent i;
        switch (item.getItemId()) {
            case android.R.id.home:
                // Previous page or exit
                // i = new Intent(this, ActivityHome.class);
                // startActivity(i);
                GlobalValue.check = 0;
                finish();
                return true;
            case R.id.abDirection:
                // Call ActivityPlaceAroundYou
                // i = new Intent(this, ActivityDirection.class);
                // i.putExtra(utils.EXTRA_DEST_LAT, mDblLatitude);
                // i.putExtra(utils.EXTRA_DEST_LNG, mDblLongitude);
                // i.putExtra(utils.EXTRA_CATEGORY_MARKER, mIcMarker);
                // startActivity(i);
                if (GlobalValue.dealsObj != null) {
                    mDblLatitude = GlobalValue.dealsObj.getLatitude();
                    mDblLongitude = GlobalValue.dealsObj.getLongitude();
                }
                String geoUri = "http://maps.google.com/maps?q=loc:" + mDblLatitude + "," + mDblLongitude + " (" + lblTitle.getText().toString() + ")";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
			/*final Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("geo:0,0?q="
							+ mDblLatitude + "," + mDblLongitude + "&z=16"));*/
                startActivity(intent);
                break;

            case R.id.abShare:
                // i = new Intent(this, ActivityShare.class);
                // i.putExtra(utils.EXTRA_DEAL_ID, mGetDealId);
                // i.putExtra(utils.EXTRA_DEAL_TITLE, mTitle);
                // i.putExtra(utils.EXTRA_DEAL_DESC, mDesc);
                // i.putExtra(utils.EXTRA_DEAL_IMG, mImgDeal);
                // startActivity(i);

                shareEvent();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        // Intent i = new Intent(this, ActivityHome.class);
        // startActivity(i);
        GlobalValue.check = 0;
        finish();
    }

    private String validateForm() {
        String message = "success";
        if (GlobalValue.myClient.getUsername() != null) {
            Toast.makeText(this, "Please login", Toast.LENGTH_SHORT).show();
            return message;
        }

        return message;
    }

    // Listener for on click
    @SuppressLint("InlinedApi")
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnGet) {
            // Show dialog Terms & Conditons
            // Comment to allow more purchase.
            // if (GlobalValue.arrCheckAttending.size() > 0) {
            // Toast.makeText(ActivityDetail.this, "Did you attending..!",
            // Toast.LENGTH_SHORT).show();
            // } else {
            if (NetworkUtility.getInstance(ExpandableToolBarActivity.this)
                    .isNetworkAvailable()) {
                if (GlobalValue.myClient == null) {
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.app_name)
                            .setMessage("You need login..!")
                            .setPositiveButton(android.R.string.ok,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface arg0, int arg1) {
                                            GlobalValue.myUser = null;
                                            GlobalValue.check = 1;
                                            Intent i = new Intent(
                                                    ExpandableToolBarActivity.this,
                                                    ActivityLogin.class);
                                            startActivity(i);
                                        }
                                    })
                            .setNegativeButton(android.R.string.cancel, null)
                            .create().show();
                }
                if (GlobalValue.myClient != null) {
                    if (GlobalValue.arrCheckAttending.size() > 0) {
                        final Dialog dialog = new Dialog(this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.dialog_confirmation);
                        dialog.getWindow().setBackgroundDrawable(
                                new ColorDrawable(
                                        android.graphics.Color.TRANSPARENT));

                        TextViewRobotoCondensedBold title = (TextViewRobotoCondensedBold) dialog
                                .findViewById(R.id.dialog_title);
                        TextViewRobotoCondensedRegular message = (TextViewRobotoCondensedRegular) dialog
                                .findViewById(R.id.dialog_message);

                        title.setText(getResources().getString(
                                R.string.purchase_event));
                        message.setText(getResources().getString(
                                R.string.you_have_purchased_already));

                        Button btnNagative = (Button) dialog
                                .findViewById(R.id.btn_nagative);
                        Button btnPositive = (Button) dialog
                                .findViewById(R.id.btn_positive);

                        btnNagative.setText(getResources().getString(
                                R.string.no));
                        btnPositive.setText(getResources().getString(
                                R.string.yes));

                        btnNagative.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        btnPositive.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                showTnCDialog();
                                dialog.dismiss();
                            }
                        });

                        dialog.show();

                    } else {
                        showTnCDialog();
                    }
                }
            } else {
                Toast.makeText(this,
                        getString(R.string.no_internet_connection),
                        Toast.LENGTH_SHORT).show();
            }
            // }
        }
        // Google Calendar
        if (v == btnGoogleCal) {

            try {
                String startEvent = lblDateStart.getText().toString().trim();
                String endEvent = lblDateEnd.getText().toString().trim();
                String start = ""+startEvent+""+lblTimeStart.getText().toString();
                String end = ""+endEvent+""+lblTimeEnd.getText().toString();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yy HH:mm");
                long startMilliseconds=0, endMilliseconds=0;
                try {
                    Date mstartDate = dateFormat.parse(start);
                    Date mstopDate = dateFormat.parse(end);
                    startMilliseconds = mstartDate.getTime();
                    endMilliseconds = mstopDate.getTime();
                    System.out.println("start Date in milli :: " + startMilliseconds);
                    System.out.println("end Date in milli :: " + endMilliseconds);
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                        startMilliseconds);
                intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false);
                intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                        endMilliseconds);
                intent.putExtra(CalendarContract.Events.TITLE, lblTitle
                        .getText().toString());
                intent.putExtra(CalendarContract.Events.DESCRIPTION, lblDesc.getText().toString());
                intent.putExtra(CalendarContract.Events.EVENT_LOCATION,
                        lblAddress.getText().toString());
                startActivity(intent);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (v.getId() == R.id.btnRetry) {
            // Retry to get Data
            // new getDataAsync().execute();
        }

        // Admin buttons listener
        if (v == mBtnScan) {
            // Coding to go to scan here
            Toast.makeText(this, "Scanning", Toast.LENGTH_SHORT).show();
        }
        if (v == mBtnDashboard) {
            // Go to dashboard page
            Intent i = new Intent(this, DashboardActivity.class);
            startActivity(i);
        }
        if (v == btnWinner) {
            Winner();
        }
    }

    private void showEventImage() {
        Intent i = new Intent(this, ShowEventImageActivity.class);
        try {
            i.putExtra("urlImg",
                    UserFunctions.URLAdmin + GlobalValue.dealsObj.getImage());
        } catch (NullPointerException ex) {
            i.putExtra("urlImg", UserFunctions.URLAdmin + mImgDeal);
        }
        startActivity(i);
    }

    private void showTnCDialog() {
        dialog = new Dialog(this);
        dialog.setTitle("Terms & Conditons");
        dialog.setContentView(R.layout.layout_terms_conditions);
        final TextView lblTermAndCondition = (TextView) dialog
                .findViewById(R.id.lblTermAndCondition);
        //Log.d(TAG, "Note: " + GlobalValue.dealsObj.getTnc_notes());
        lblTermAndCondition.setText(Html.fromHtml(GlobalValue.dealsObj
                .getTnc_notes()));
        final CheckBox cbAccept = (CheckBox) dialog.findViewById(R.id.cbAccept);
        final Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        final Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        // final Button btnClientUser = (Button) dialog
        // .findViewById(R.id.btnClientName);
        // btnClientUser.setSelected(true);
        // btnClientUser.setText("Welcome: " +
        // GlobalValue.myClient.getUsername());
        // Register device
        if (GlobalValue.dealsObj != null) {
            mTnCid = GlobalValue.dealsObj.getTnc_id();
            mUsername = GlobalValue.dealsObj.getUsername();
        }
        gcmInfo = new GcmObj();
        gcmInfo.setClientname(GlobalValue.myClient.getUsername());
        gcmInfo.setIme(GlobalValue.android_id);
        gcmInfo.setStatus(1);
        // attending
        attend = new AttendedObj();
        attend.setClientname(GlobalValue.myClient.getUsername());
        attend.setDeviceId(GlobalValue.android_id);
        attend.setDealId(mGetDealId);
        attend.setTncId(mTnCid);
        attend.setUsername(mUsername);
        attend.setStatusId("");
        attend.setAttendedId("");

        cbAccept.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (cbAccept.isChecked()) {
                    btnAccept.setEnabled(true);
                    btnAccept.setBackgroundResource(R.drawable.button_selector);
                    btnAccept.setTextColor(Color.WHITE);
                } else if (!cbAccept.isChecked()) {
                    btnAccept.setEnabled(false);
                    btnAccept
                            .setBackgroundResource(R.drawable.button_enabled_false);
                    btnAccept.setTextColor(Color.GRAY);
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        btnAccept.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // Show payment dialog.
                numberPeople = new Dialog(ExpandableToolBarActivity.this);
                numberPeople.setContentView(R.layout.layout_number_people);
                numberPeople.setTitle("Attending...");
                Button btnMinus = (Button) numberPeople
                        .findViewById(R.id.btnMinus);
                Button btnPlus = (Button) numberPeople
                        .findViewById(R.id.btnPlus);
                final EditText txtNumber = (EditText) numberPeople
                        .findViewById(R.id.txtQuantityPeople);
                Button btnOk = (Button) numberPeople.findViewById(R.id.btnOk);
                Button btnCancel = (Button) numberPeople
                        .findViewById(R.id.btnCancel);

                btnMinus.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        String quantity = txtNumber.getText().toString();
                        if (quantity == null || quantity.equals("")) {
                            txtNumber.setText("1");
                        } else {

                            if (Integer.parseInt(quantity) > 1) {
                                sl = Integer.parseInt(quantity) - 1;
                                txtNumber.setText(sl + "");
                            } else {
                                txtNumber.setText("1");
                            }
                        }

                        // Bring pointer to end
                        txtNumber.setSelection(txtNumber.getText().toString()
                                .length());
                    }
                });
                btnPlus.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        String quantity = txtNumber.getText().toString();
                        if (quantity == null || quantity.equals("")) {
                            txtNumber.setText("1");
                        } else {
                            sl = Integer.parseInt(quantity) + 1;
                            if (sl > 999) {
                                Toast.makeText(ExpandableToolBarActivity.this,
                                        "Maximum is 999", Toast.LENGTH_SHORT)
                                        .show();
                            } else {
                                txtNumber.setText(sl + "");
                            }
                        }

                        // Bring pointer to end
                        txtNumber.setSelection(txtNumber.getText().toString()
                                .length());
                    }
                });
                btnOk.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        String number = txtNumber.getText().toString();
                        if (number.isEmpty()) {
                            Toast.makeText(ExpandableToolBarActivity.this,
                                    "You need input quantity",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            quantity = Integer.parseInt(number);
                            dialogPaypal();
                        }
                    }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        numberPeople.dismiss();
                    }
                });

                numberPeople.show();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void Winner() {
        Intent i = new Intent(this, ListAttendOfEventActivity.class);
        i.putExtra("deal_id", mGetDealId);
        startActivity(i);
    }

    private void dialogPaypal() {
        final Dialog dialog1 = new Dialog(ExpandableToolBarActivity.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.dialog_choose_payment);
        Button btnOk = (Button) dialog1.findViewById(R.id.btn_ok);
        Button btnCancel = (Button) dialog1.findViewById(R.id.btn_cancel);

        RadioGroup gr = (RadioGroup) dialog1.findViewById(R.id.gr_rad_payment);
        final int CASH_ON_DELIVERY = 0;
        final int PAYPAL = 1;
        final int BANKING_TRANSFER = 2;

        gr.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                View radPayment = group.findViewById(checkedId);
                paymentMethod = group.indexOfChild(radPayment);
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (paymentMethod == CASH_ON_DELIVERY) {
                    // paymentCashOnDelivery();
                    Toast.makeText(ExpandableToolBarActivity.this, "Not available !",
                            Toast.LENGTH_SHORT).show();
                } else if (paymentMethod == PAYPAL) {
                    double price = GlobalValue.dealsObj
                            .getAfter_discount_value() * quantity;
                    requestPaypal(
                            GlobalValue.dealsObj.getAfter_discount_value()
                                    * quantity, "Content", "GBP");

                    Toast.makeText(ExpandableToolBarActivity.this,
                            price + Utils.mCurrency, Toast.LENGTH_SHORT).show();

                } else if (paymentMethod == BANKING_TRANSFER) {
                    // paymentBankingTransfer();
                    Toast.makeText(ExpandableToolBarActivity.this, "not available !",
                            Toast.LENGTH_SHORT).show();
                }

                dialog1.dismiss();
                numberPeople.dismiss();
                if (GlobalValue.myClient != null) {
                    dialog.dismiss();
                }

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });

        dialog1.show();
    }

    private void register(AttendedObj att) {
        // TODO Auto-generated method stub
        AttendedObj data = att;
       // Log.e(TAG, "ACCOUNT INFO : " + data);
        ModelManager.attended(this, data, true, new ModelManagerListener() {

            @Override
            public void onSuccess(Object object) {
                // TODO Auto-generated method stub

                String strJson = (String) object;
                Toast.makeText(ExpandableToolBarActivity.this, checkResult(strJson),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError() {
                // TODO Auto-generated method stub

            }
        });
    }

    protected String checkResult(String strJson) {
        // TODO Auto-generated method stub
        JSONObject json = null;
        String message = "";
        try {
            json = new JSONObject(strJson);
            if (json.getString("status").equals("success")) {
                message = this.getString(R.string.message_success);
            } else {
                message = json.getString("message");
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return message;
    }

    private void updateGcm(GcmObj gcm) {
        // TODO Auto-generated method stub
        ModelManager.updateGcm(this, GlobalValue.android_id, gcm, true,
                new ModelManagerListener() {

                    @Override
                    public void onSuccess(Object object) {
                        // TODO Auto-generated method stub
                        String strJson = (String) object;
                        Toast.makeText(ExpandableToolBarActivity.this,
                                checkResult(strJson), Toast.LENGTH_SHORT)
                                .show();
                    }

                    @Override
                    public void onError() {
                        // TODO Auto-generated method stub

                    }
                });
    }

    protected String checkResultAddGcm(String strJson) {
        // TODO Auto-generated method stub
        JSONObject json = null;
        String message = "";
        try {
            json = new JSONObject(strJson);
            if (json.getString("status").equals("success")) {
            } else {
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return message;
    }

    private void userPurchase(PurchaseObj purchase) {
        // TODO Auto-generated method stub
        PurchaseObj data = purchase;
       // Log.e(TAG, "ACCOUNT INFO : " + data);
        ModelManager.userPurchase(this, data, true, new ModelManagerListener() {

            @Override
            public void onSuccess(Object object) {
                // TODO Auto-generated method stub

                String strJson = (String) object;
                Toast.makeText(ExpandableToolBarActivity.this, checkResult(strJson),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError() {
                // TODO Auto-generated method stub

            }
        });
    }

    private void shareEvent() {
        if (GlobalValue.dealsObj != null) {
            mTitle = GlobalValue.dealsObj.getTitle();
        }
        Intent iShare = new Intent(Intent.ACTION_SEND);
        iShare.setType("text/plain");
        iShare.putExtra(Intent.EXTRA_SUBJECT, mTitle);
        iShare.putExtra(Intent.EXTRA_TEXT, "\nDetail: " + userFunction.URLAdmin
                + userFunction.service_view_deal + userFunction.key_deals_id
                + "=" + mGetDealId + "\n\nInstall application: "
                + "https://play.google.com/store/apps/details?id="
                + PacketUtility.getPacketName());
        startActivity(Intent.createChooser(iShare,
                getString(R.string.share_via)));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Comment to allow more purchase
        // if (GlobalValue.arrCheckAttending.size() > 0) {
        // new AlertDialog.Builder(this)
        // .setTitle(R.string.app_name)
        // .setMessage("Did you attending..!")
        // // .setPositiveButton(android.R.string.ok,
        // // new DialogInterface.OnClickListener() {
        // // @Override
        // // public void onClick(DialogInterface arg0,
        // // int arg1) {
        // // GlobalValue.myUser = null;
        // // GlobalValue.check = 1;
        // // Intent i = new Intent(
        // // ActivityDetail.this,
        // // ActivityLogin.class);
        // // startActivity(i);
        // // // finish();
        // // // FragmentHome.self.finish();
        // // }
        // // })
        // .setNegativeButton(android.R.string.cancel, null).create()
        // .show();
        // } else {
        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data
                    .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {

                    String PAYMENT_PAY_ID = confirm.toJSONObject()
                            .getJSONObject("response").getString("id");
                    String PAYMENT_AMOUNT = confirm.getPayment().toJSONObject()
                            .getString("amount");
                    Toast toast = Toast.makeText(ExpandableToolBarActivity.this,
                            "Payment successfully !", Toast.LENGTH_LONG);
                    toast.setGravity(
                            Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();

                    // Register tickets
                    if (GlobalValue.dealsObj != null) {
                        mUsername = GlobalValue.dealsObj.getUsername();
                        mUrl = GlobalValue.dealsObj.getDeal_url();
                        mTitle = GlobalValue.dealsObj.getTitle();
                    }
                    purchase = new PurchaseObj();
                    if (GlobalValue.myClient != null) {
                        purchase.setUsername(GlobalValue.myClient.getUsername());
                    }
                    purchase.setUserType("Website");
                    purchase.setDeal_id(mGetDealId);
                    purchase.setPaymentType("Paypal");
                    purchase.setQuantity(quantity);
                    purchase.setAmount(PAYMENT_AMOUNT);
                    purchase.setCurrency(Utils.mCurrency);
                    purchase.setTxn_id(PAYMENT_PAY_ID);
                    purchase.setCreateDate(strDate);
                    purchase.setUsed_date(strDate);
                    purchase.setStatus(0);
                    purchase.setNotes("");
                    purchase.setAdmin_id(mUsername);
                    purchase.setClient_email(GlobalValue.myClient.getEmail());
                    //
                    register(attend);
                    // Only update gcm and register ticket when user buys
                    // ticket by normal user
                    // account(Not facebook account)
                    if (GlobalValue.myClient != null) {
                        updateGcm(gcmInfo);
                        // registerTickets(tickets);
                        userPurchase(purchase);
                    }

                    // DatabaseUtility.insertPaidticket(ActivityDetail.this,
                    // tickets);
                    //
                    Intent i;
                    // Open ActivityBrowser
                    i = new Intent(getApplicationContext(),
                            ActivityBrowser.class);
                    i.putExtra(utils.EXTRA_DEAL_URL, mUrl);
                    i.putExtra(utils.EXTRA_DEAL_TITLE, mTitle);
                    startActivity(i);

                } catch (JSONException e) {

                    e.printStackTrace();

                    //Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
           // Log.i(TAG, "The user canceled.");
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
           // Log.i(TAG, "An invalid payment was submitted. Please see the docs.");
        }
        // }
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        double value = GlobalValue.dealsObj.getAfter_discount_value()
                * quantity;
        return new PayPalPayment(new BigDecimal(value), "Content", "GBP",
                paymentIntent);
    }

    private void requestPaypal(double value, String content, String currency) {
        String PAYPAL_CLIENT_APP_ID = "AUO3MhDAn7i4un0oWMRng4-J_C2G0qsqRHpaG7O8osoe0J61Mjb29XQnpvaJ";
        String PAYPAL_RECEIVE_EMAIL_ID = "fruity.nam-facilitator@gmail.com";

        // PayPalPayment thingToBuy =
        // getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(value),
                currency, content, PayPalPayment.PAYMENT_INTENT_SALE);

		/*
		 * See getStuffToBuy(..) for examples of some available payment options.
		 */

        Intent intent = new Intent(ExpandableToolBarActivity.this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        startActivityForResult(intent, REQUEST_CODE_PAYMENT);

    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    View.OnClickListener headerListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            showEventImage();
        }
    };

    public void makeLinks(TextView textView, String[] links)
    {
        try
        {
            SpannableString spannableString = new SpannableString(textView.getText());
            for (int i = 0; i < links.length; i++)
            {
                final String link = links[i];

                int startIndexOfLink = textView.getText().toString().indexOf(link);
                int endingPosition = startIndexOfLink + link.length();

                ClickableSpan clickableSpan = new ClickableSpan()
                {
                    @Override
                    public void onClick(View textView)
                    {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                        startActivity(browserIntent);
                    }
                };
                spannableString.setSpan(clickableSpan, startIndexOfLink, endingPosition, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            textView.setHighlightColor(Color.TRANSPARENT); // prevent TextView change background when highlight
            textView.setMovementMethod(LinkMovementMethod.getInstance());
            textView.setText(spannableString, TextView.BufferType.SPANNABLE);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    protected void setTextViewHTML(TextView text, String html)
    {

        SpannableString spannableString = new SpannableString(html);
        Linkify.addLinks(spannableString, Linkify.ALL);
        URLSpan[] spans = spannableString.getSpans(0, spannableString.length() , URLSpan.class);
        String[] url=new String[spans.length];
        for(int i=0;i<spans.length;i++)
        {
            url[i]=""+spans[i].getURL();
        }
        text.setText(html);
        makeLinks(text, url);
    }
}