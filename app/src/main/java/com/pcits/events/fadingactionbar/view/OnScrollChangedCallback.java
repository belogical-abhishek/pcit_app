package com.pcits.events.fadingactionbar.view;

public interface OnScrollChangedCallback {
	void onScroll(int l, int t);
}
