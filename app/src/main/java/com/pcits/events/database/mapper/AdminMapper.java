package com.pcits.events.database.mapper;

import android.database.Cursor;

import com.pcits.events.obj.UserObj;

public class AdminMapper implements RowMapper<UserObj> {

	@Override
	public UserObj mapRow(Cursor row, int rowNum) {
		// TODO Auto-generated method stub
		UserObj adminObj = new UserObj();
		adminObj.setUsername(CursorParseUtility.getString(row, "username"));
		adminObj.setPassword(CursorParseUtility.getString(row, "password"));
		adminObj.setFname(CursorParseUtility.getString(row, "fname"));
		adminObj.setLname(CursorParseUtility.getString(row, "lname"));
		adminObj.setDob(CursorParseUtility.getString(row, "dob"));
		adminObj.setAddress(CursorParseUtility.getString(row, "address"));
		adminObj.setPhone(CursorParseUtility.getString(row, "phone"));
		adminObj.setCity(CursorParseUtility.getString(row, "city"));
		adminObj.setCounty(CursorParseUtility.getString(row, "county"));
		adminObj.setPostcode(CursorParseUtility.getString(row, "postcode"));
		adminObj.setCountry(CursorParseUtility.getString(row, "country"));
		adminObj.setEmail(CursorParseUtility.getString(row, "email"));
		adminObj.setRole(CursorParseUtility.getString(row, "role"));
		adminObj.setPayInfo(CursorParseUtility.getString(row, "payInfo"));
		adminObj.setStatus(CursorParseUtility.getString(row, "status"));
		adminObj.setNotes(CursorParseUtility.getString(row, "notes"));
		adminObj.setImage(CursorParseUtility.getString(row, "image"));
		return adminObj;
	}

}
