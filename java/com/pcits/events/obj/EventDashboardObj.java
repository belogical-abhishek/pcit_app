package com.pcits.events.obj;

public class EventDashboardObj {

	private String title;
	private double totalSpent, advSpent, feaSpent;
	private double totalRegisteredMember, registeredMale, registeredFemale,
			registeredNotDisclosed;
	private double totalPurchased, purchasedMale, purchasedFemale,
			purchasedNotDisclosed;
	private double totalClientPurchased;
	private double luckyAwarded, luckyWon, luckyClaimed, luckyUnclaimed,
			luckyValue;
	private double surveyReceived, surveySent;

	public EventDashboardObj() {
	}

	public EventDashboardObj(String title, double totalSpent, double advSpent,
			double feaSpent, double totalRegisteredMember,
			double registeredMale, double registeredFemale,
			double registeredNotDisclosed, double totalPurchased,
			double purchasedMale, double purchasedFemale,
			double purchasedNotDisclosed, double totalClientPurchased,
			double luckyAwarded, double luckyWon, double luckyClaimed,
			double luckyUnclaimed, double luckyValue, double surveyReceived,
			double surveySent) {
		this.title = title;
		this.totalSpent = totalSpent;
		this.advSpent = advSpent;
		this.feaSpent = feaSpent;
		this.totalRegisteredMember = totalRegisteredMember;
		this.registeredMale = registeredMale;
		this.registeredFemale = registeredFemale;
		this.registeredNotDisclosed = registeredNotDisclosed;
		this.totalPurchased = totalPurchased;
		this.purchasedMale = purchasedMale;
		this.purchasedFemale = purchasedFemale;
		this.purchasedNotDisclosed = purchasedNotDisclosed;
		this.totalClientPurchased = totalClientPurchased;
		this.luckyAwarded = luckyAwarded;
		this.luckyClaimed = luckyClaimed;
		this.luckyUnclaimed = luckyUnclaimed;
		this.luckyValue = luckyValue;
		this.luckyWon = luckyWon;
		this.surveyReceived = surveyReceived;
		this.surveySent = surveySent;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getTotalSpent() {
		return totalSpent;
	}

	public void setTotalSpent(double totalSpent) {
		this.totalSpent = totalSpent;
	}

	public double getAdvSpent() {
		return advSpent;
	}

	public void setAdvSpent(double advSpent) {
		this.advSpent = advSpent;
	}

	public double getFeaSpent() {
		return feaSpent;
	}

	public void setFeaSpent(double feaSpent) {
		this.feaSpent = feaSpent;
	}

	public double getTotalRegisteredMember() {
		return totalRegisteredMember;
	}

	public void setTotalRegisteredMember(double totalRegisteredMember) {
		this.totalRegisteredMember = totalRegisteredMember;
	}

	public double getRegisteredMale() {
		return registeredMale;
	}

	public void setRegisteredMale(double registeredMale) {
		this.registeredMale = registeredMale;
	}

	public double getRegisteredFemale() {
		return registeredFemale;
	}

	public void setRegisteredFemale(double registeredFemale) {
		this.registeredFemale = registeredFemale;
	}

	public double getRegisteredNotDisclosed() {
		return registeredNotDisclosed;
	}

	public void setRegisteredNotDisclosed(double registeredNotDisclosed) {
		this.registeredNotDisclosed = registeredNotDisclosed;
	}

	public double getTotalPurchased() {
		return totalPurchased;
	}

	public void setTotalPurchased(double totalPurchased) {
		this.totalPurchased = totalPurchased;
	}

	public double getPurchasedMale() {
		return purchasedMale;
	}

	public void setPurchasedMale(double purchasedMale) {
		this.purchasedMale = purchasedMale;
	}

	public double getPurchasedFemale() {
		return purchasedFemale;
	}

	public void setPurchasedFemale(double purchasedFemale) {
		this.purchasedFemale = purchasedFemale;
	}

	public double getPurchasedNotDisclosed() {
		return purchasedNotDisclosed;
	}

	public void setPurchasedNotDisclosed(double purchasedNotDisclosed) {
		this.purchasedNotDisclosed = purchasedNotDisclosed;
	}

	public double getTotalClientPurchased() {
		return totalClientPurchased;
	}

	public void setTotalClientPurchased(double totalClientPurchased) {
		this.totalClientPurchased = totalClientPurchased;
	}

	public double getLuckyAwarded() {
		return luckyAwarded;
	}

	public void setLuckyAwarded(double luckyAwarded) {
		this.luckyAwarded = luckyAwarded;
	}

	public double getLuckyWon() {
		return luckyWon;
	}

	public void setLuckyWon(double luckyWon) {
		this.luckyWon = luckyWon;
	}

	public double getLuckyClaimed() {
		return luckyClaimed;
	}

	public void setLuckyClaimed(double luckyClaimed) {
		this.luckyClaimed = luckyClaimed;
	}

	public double getLuckyUnclaimed() {
		return luckyUnclaimed;
	}

	public void setLuckyUnclaimed(double luckyUnclaimed) {
		this.luckyUnclaimed = luckyUnclaimed;
	}

	public double getLuckyValue() {
		return luckyValue;
	}

	public void setLuckyValue(double luckyValue) {
		this.luckyValue = luckyValue;
	}

	public double getSurveyReceived() {
		return surveyReceived;
	}

	public void setSurveyReceived(double surveyReceived) {
		this.surveyReceived = surveyReceived;
	}

	public double getSurveySent() {
		return surveySent;
	}

	public void setSurveySent(double surveySent) {
		this.surveySent = surveySent;
	}
}
