/**
 * File        : ActivityAbout.java
 * App name    : M1SO Events
 * Version     : 1.1.4
 * Created     : 25/05/14

 * Created by Team M1SO on 25/01/14.
 * Copyright (c) 2014 M1SO. All rights reserved.
 */

package com.pcits.events;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;

import com.androidquery.AQuery;
import com.pcits.events.widgets.imagezoom.PhotoView;

public class ShowEventImageActivity extends FragmentActivity {

	private PhotoView mImgThumbnail;
	private String mUrlImg = "";
	private Display mDisplay;
	private View mVwTrans;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_evet_image);

		mDisplay = getWindowManager().getDefaultDisplay();

		initUI();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);

		calcImageDimension();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	private void initUI() {
		mVwTrans = findViewById(R.id.vw_trans);
		mImgThumbnail = (PhotoView) findViewById(R.id.img_thumbnail);
		calcImageDimension();

		Bundle b = getIntent().getExtras();
		mUrlImg = b.getString("urlImg");
		AQuery aq = new AQuery(this);
		aq.id(mImgThumbnail).image(mUrlImg, false, true);

		// Should call this method by the end of declaring UI.
		initControl();
	}

	private void initControl() {
		mVwTrans.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	private void calcImageDimension() {
		mImgThumbnail.getLayoutParams().height = mDisplay.getWidth() * 9 / 16;
	}
}