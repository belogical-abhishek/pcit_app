package com.pcits.events;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.pcits.events.viewpage.CirclePageIndicator;
import com.pcits.events.viewpage.HelpFragmentPagerAdapter;

public class ActivityHelp extends FragmentActivity {

	private ViewPager pager;
	private CirclePageIndicator indicator;
	private float density;
	private LinearLayout llbt_details,llMenu;
	private Animation slideRight;
	private FragmentManager fm;
	private HelpFragmentPagerAdapter pagerAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.help);

		// llbt_details=(LinearLayout)findViewById(R.id.llbt_details);
		slideRight = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);

		llbt_details.startAnimation(slideRight);

		/** Getting a reference to the ViewPager defined the layout file */
		// pager = (ViewPager) findViewById(R.id.pager);
		// indicator = (CirclePageIndicator)findViewById(R.id.indicator);
		density = getResources().getDisplayMetrics().density;

		/** Getting fragment manager */
		fm = getSupportFragmentManager();
		LinearLayout llMenu=(LinearLayout)findViewById(R.id.llMenu);

		/** Instantiating FragmentPagerAdapter */

		pagerAdapter = new HelpFragmentPagerAdapter(fm, ActivityHelp.this);
		/** Setting the pagerAdapter to the pager object */

		pager.setAdapter(pagerAdapter);

		indicator.setViewPager(pager);

		// indicator.setBackgroundColor(0xFFCCCCCC);
		indicator.setRadius(10 * density);
		indicator.setPageColor(0x880000FF);
		// indicator.setFillColor(0xFF073847);
		indicator.setFillColor(0xFF62c7e5);

		indicator.setStrokeColor(0xFF000000);
		indicator.setStrokeWidth(2 * density);
	}

}
