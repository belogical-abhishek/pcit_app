package com.pcits.events.fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityDetail;
import com.pcits.events.ActivityHome;
import com.pcits.events.AddEventActivity;
import com.pcits.events.R;
import com.pcits.events.adapters.HomeAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.libraries.JSONParser;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.obj.APIObj;
import com.pcits.events.obj.DealObj;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class FragmentPopular extends Fragment {

	private static final String TAG = "FragmentPopular";

	public static ArrayList<DealObj> mArrDeals;

	// Declare object of userFunctions and Utils class
	private UserFunctions userFunction;

	// Create instance of list and ListAdapter
	private ListView list;
	private TextView lblNoResult;
	public static HomeAdapter mla;

	private Button btnLoadMore;
	private LinearLayout lytRetry;

	// flag for current page
	private JSONObject json, jsonCurrency;
	private String client;

	// Declare variable
	private Activity self;
	private PullToRefreshListView mPullRefreshListView;

	private View v;

	public static LinearLayout llMenu;

	private DealObj mDealObj;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		self = getActivity();
		v = inflater.inflate(R.layout.fragment_home, container, false);

		try {
			if (mArrDeals == null) {
				mArrDeals = new ArrayList<DealObj>();
			}
			// Clear old data
			mArrDeals.clear();

			lblNoResult = (TextView) v.findViewById(R.id.lblNoResult);
			lytRetry = (LinearLayout) v.findViewById(R.id.llRetry);

			// Declare object of userFunctions class
			userFunction = new UserFunctions();

			new loadFirstListView().execute();
			mPullRefreshListView = (PullToRefreshListView) v
					.findViewById(R.id.lsvPullToRefresh);
			list = mPullRefreshListView.getRefreshableView();
			// Set a listener to be invoked when the list should be refreshed.
			if (NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
				mPullRefreshListView
						.setOnRefreshListener(new OnRefreshListener<ListView>() {
							@Override
							public void onRefresh(
									PullToRefreshBase<ListView> refreshView) {

								String label = DateUtils.formatDateTime(
										getActivity(),
										System.currentTimeMillis(),
										DateUtils.FORMAT_SHOW_TIME
												| DateUtils.FORMAT_SHOW_DATE
												| DateUtils.FORMAT_ABBREV_ALL);

								// Update the LastUpdatedLabel
								refreshView.getLoadingLayoutProxy()
										.setLastUpdatedLabel(label);
								mArrDeals = new ArrayList<DealObj>();
								new loadFirstListView().execute();

							}
						});
			} else {
				Toast.makeText(getActivity(),
						getString(R.string.no_internet_connection),
						Toast.LENGTH_SHORT).show();
			}

			// Add an end-of-list listener
			mPullRefreshListView
					.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

						@Override
						public void onLastItemVisible() {
							// Toast.makeText(getActivity(), "end page",
							// Toast.LENGTH_SHORT).show();
						}
					});

			// Listener to get selected id when list item clicked
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					GlobalValue.dealsObj = mArrDeals.get(position - 1);
					DatabaseUtility.insertDeals(getActivity(),
							mArrDeals.get(position - 1));

					Log.d("", "DATA " + "Successfully");
					ModelManager.updateClickCount(getActivity(),
							mArrDeals.get(position - 1).getDeal_id(), true,
							new ModelManagerListener() {

								@Override
								public void onSuccess(Object object) {
									// TODO Auto-generated method stub
									String json = (String) object;
									boolean result = ParserUtility
											.updateAccount(json);
									if (result) {
										Log.d(TAG, "Successfully");
									}
								}

								@Override
								public void onError() {
									// TODO Auto-generated method stub

								}
							});

					// }
					// Pass id to onListSelected method on HomeActivity
					// mCallback.onListSelected(item.get(userFunction.key_deals_id));
					Intent i = new Intent(getActivity(), ActivityDetail.class);
					// i.putExtra(utils.EXTRA_DEAL_ID, mArrDeals.get(position -
					// 1)
					// .getDeal_id());
					startActivity(i);
					getActivity().overridePendingTransition(
							R.anim.slide_in_left, R.anim.slide_out_left);

					// Set the item as checked to be highlighted when in
					// two-pane
					// layout
					list.setItemChecked(position, true);
				}
			});
			list.setOnItemLongClickListener(new OnItemLongClickListener() {

				@Override
				public boolean onItemLongClick(AdapterView<?> parent,
						View view, int position, long id) {
					// TODO Auto-generated method stub
					if (GlobalValue.myUser != null) {
						try {
							mDealObj = (DealObj) mArrDeals.get(position - 1)
									.clone();
							showOperationDialog();
						} catch (Exception ex) {
							// TODO Auto-generated catch block
							Logging.writeExceptionFromStackTrace(ex,
									ex.getMessage());
						}
					}
					return true;
				}
			});
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		mDealObj = null;
	}

	// Load first 10 videos
	private class loadFirstListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Showing progress dialog before sending http request
			ActivityHome.rltProgress.setVisibility(View.VISIBLE);
		}

		protected Void doInBackground(Void... unused) {
			try {
				if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
					getDataFromServer();
				} else {
					// Load data from database
					getDataFromDB();
				}
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			}
			return null;
		}

		protected void onPostExecute(Void unused) {
			// Condition if data length under 10 button loadMore is remove
			try {
				if (isAdded()) {
					if (mArrDeals.size() != 0) {
						// Check parameter notify
						lytRetry.setVisibility(View.GONE);
						list.setVisibility(View.VISIBLE);
						lblNoResult.setVisibility(View.GONE);
						// Getting adapter
						mla = new HomeAdapter(getActivity(), mArrDeals);
						mla.notifyDataSetChanged();
						list.setAdapter(mla);
					} else {
						list.removeFooterView(btnLoadMore);
						if (json != null) {
							lblNoResult.setVisibility(View.VISIBLE);
							lytRetry.setVisibility(View.GONE);

						} else {
							lblNoResult.setVisibility(View.GONE);
							lytRetry.setVisibility(View.VISIBLE);
							Toast.makeText(getActivity(),
									getString(R.string.no_connection),
									Toast.LENGTH_SHORT).show();
						}
					}
				}
				super.onPostExecute(unused);
				mPullRefreshListView.onRefreshComplete();
				// Closing progress dialog
				ActivityHome.rltProgress.setVisibility(View.GONE);
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			}
		}
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		// if (pDialog != null) {
		// pDialog.dismiss();
		// pDialog = null;
		// }
	}

	public void getDataFromServer() {
		if (GlobalValue.myClient != null) {
			client = GlobalValue.myClient.getUsername();
		} else {
			client = "";
		}
		try {
			jsonCurrency = userFunction.currency(getActivity());
			json = userFunction.popularDeals(client, getActivity());
			// }
			Log.e("url", "url: " + json);

			if (json != null) {
				ArrayList<DealObj> arr = JSONParser.parserDeal(
						userFunction.array_latest_deals, json.toString());
				mArrDeals.addAll(arr);
			}
			if (jsonCurrency != null) {
				JSONArray currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);

			}

		} catch (JSONException ex) {
			throw new RuntimeException(ex);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public void getDataFromDB() {

		try {
			APIObj currencyInfos = DatabaseUtility.getResuftFromApi(
					getActivity(), UserFunctions.URL_CURRENCY);
			Log.e("FragmentFeatured", "Database: " + currencyInfos);
			String jsonCurr = "";
			if (currencyInfos != null) {
				jsonCurr = currencyInfos.getmResult();
			}
			jsonCurrency = new JSONObject(jsonCurr);

			APIObj popularDeals = DatabaseUtility.getResuftFromApi(
					getActivity(), UserFunctions.URL_DEAL_POPULAR);
			Log.e("FragmentFeatured", "Database: " + popularDeals);
			String jsonAllDeal = "";
			if (popularDeals != null) {
				jsonAllDeal = popularDeals.getmResult();
			}
			json = new JSONObject(jsonAllDeal);

			if (json != null) {
				ArrayList<DealObj> arr = JSONParser.parserDeal(
						userFunction.array_latest_deals, json.toString());
				mArrDeals.addAll(arr);
			}
			if (jsonCurrency != null) {
				JSONArray currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);

			}

		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		list.setAdapter(null);
		super.onDestroy();

	}

	private void showOperationDialog() {
		final Dialog dialog = new Dialog(self);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_operation_event);
		dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
				WindowManager.LayoutParams.WRAP_CONTENT);

		TextViewRobotoCondensedRegular copy = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_copy);
		copy.setVisibility(View.GONE);
		TextViewRobotoCondensedRegular delete = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_delete);
		TextViewRobotoCondensedRegular revoke = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_revoke);
		TextViewRobotoCondensedRegular scan = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_scan);
		TextViewRobotoCondensedRegular edit = (TextViewRobotoCondensedRegular) dialog
				.findViewById(R.id.lbl_edit);

		// Set listener
		copy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Open create event page with event copied
				AddEventActivity.isDeal = true;
				mDealObj.setDeal_id(null);
				GlobalValue.dealsObj = mDealObj;
				Intent i = new Intent(getActivity(), AddEventActivity.class);
				startActivity(i);

				// Dismiss the dialog
				dialog.dismiss();
			}
		});

		delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(self, "Deleted", Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			}
		});

		revoke.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(self, "Revoked", Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			}
		});

		scan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(self, "Scanned", Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			}
		});

		edit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Open edit event page
				// Intent i = new Intent(self, ActivityEditDeal.class);
				AddEventActivity.isDeal = true;
				GlobalValue.dealsObj = mDealObj;
				Intent i = new Intent(getActivity(), AddEventActivity.class);
				startActivity(i);
				// Dismiss dialog
				dialog.dismiss();
			}
		});

		dialog.show();
	}

}
