package com.pcits.events;

import net.sourceforge.zbar.Symbol;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.events.integration.android.ZBarConstants;
import com.pcits.events.integration.android.ZBarScannerActivity;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.progressButton.CircularProgressButton;
import com.pcits.events.widgets.KenBurnsView;

public class ScanTicketActivity extends Activity {

	private Button scanBtn;
	private TextView contentTxt;
	private static final int ZBAR_SCANNER_REQUEST = 0;
	private static final int ZBAR_QR_SCANNER_REQUEST = 1;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;
	private TextView lblTitleHeader;
	private String contents;
	private CircularProgressButton btnUpdate;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_scan_ticket);
		initUI();
		// getData();
		initNotBoringActionBar();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		btnUpdate.setProgress(0);
	}

	private void initNotBoringActionBar() {
	//	mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
//		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}

	private void initUI() {
		lblTitleHeader = (TextView) findViewById(R.id.lblTitleHeader);
		lblTitleHeader.setText("Scan Ticket");
		llMenu = (LinearLayout) findViewById(R.id.llMenu);
		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(ScanTicketActivity.this,ActivityHome.class);
				startActivity(intent);
				finish();
			}
		});
		scanBtn = (Button) findViewById(R.id.scan_button);
		contentTxt = (TextView) findViewById(R.id.scan_content);

		scanBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isCameraAvailable()) {
					Intent intent = new Intent(ScanTicketActivity.this,
							ZBarScannerActivity.class);
					intent.putExtra(ZBarConstants.SCAN_MODES,
							new int[] { Symbol.QRCODE });
					startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
				} else {
					Toast.makeText(ScanTicketActivity.this,
							"Camera is unavailable",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		btnUpdate = (CircularProgressButton) findViewById(R.id.circularButton1);
		btnUpdate.setIndeterminateProgressMode(true);
		btnUpdate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String[] result = contents.split(" ");
				String Id = result[1];
				String type = result[3];

				ModelManager.updateTickets(ScanTicketActivity.this, Id,
						Integer.toString(1), true, new ModelManagerListener() {

							@Override
							public void onSuccess(Object object) {
								// TODO Auto-generated method stub
								String strJson = (String) object;
								Toast.makeText(ScanTicketActivity.this,
										checkResult(strJson),
										Toast.LENGTH_SHORT).show();
								btnUpdate.setProgress(100);
							}

							@Override
							public void onError() {
								// TODO Auto-generated method stub
								btnUpdate.setProgress(0);
							}
						});
				// if (btnUpdate.getProgress() == 0) {
				// btnUpdate.setProgress(50);
				// } else if (btnUpdate.getProgress() == 100) {
				// btnUpdate.setProgress(0);
				// } else {
				// btnUpdate.setProgress(100);
				//
				// }
			}
		});

	}

	protected String checkResult(String strJson) {
		// TODO Auto-generated method stub
		JSONObject json = null;
		String message = "";
		try {
			json = new JSONObject(strJson);
			if (json.getString("status").equals("success")) {
				message = this.getString(R.string.message_success);
			} else {
				message = json.getString("message");
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return message;
	}

	public boolean isCameraAvailable() {
		PackageManager pm = ScanTicketActivity.this.getPackageManager();
		return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// retrieve scan result
		switch (requestCode) {
		case ZBAR_SCANNER_REQUEST:
		case ZBAR_QR_SCANNER_REQUEST:
			if (resultCode == ScanTicketActivity.this.RESULT_OK) {
				contents = data.getStringExtra("SCAN_RESULT");
				contentTxt.setText(contents);
				btnUpdate.setVisibility(View.VISIBLE);
			} else if (resultCode == ScanTicketActivity.this.RESULT_CANCELED
					&& data != null) {
				String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
				if (!TextUtils.isEmpty(error)) {
					Toast.makeText(ScanTicketActivity.this, error,
							Toast.LENGTH_SHORT).show();
				}
			}
			break;
		}
	}
}
