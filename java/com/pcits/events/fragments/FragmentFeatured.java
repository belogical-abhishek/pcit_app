package com.pcits.events.fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityDetail;
import com.pcits.events.ActivityHome;
import com.pcits.events.R;
import com.pcits.events.adapters.HomeAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.libraries.JSONParser;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.obj.APIObj;
import com.pcits.events.obj.DealObj;
import com.pcits.events.utils.Utils;

public class FragmentFeatured extends Fragment {

	public static ArrayList<DealObj> mArrDeals;

	// Declare object of userFunctions and Utils class
	private UserFunctions userFunction;
	private Utils utils;

	// Create instance of list and ListAdapter
	private ListView list;
	private TextView lblNoResult;
	public static HomeAdapter mla;

	private LinearLayout lytRetry;

	// flag for current page
	private JSONObject json, jsonCurrency;
	private String client;

	// Declare variable
	String mCategoryId, mCategoryName, mActivity;
	// public ArrayList<DealObj> arrDeals;
	private PullToRefreshListView mPullRefreshListView;

	private View v;

	public static LinearLayout llMenu;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		v = inflater.inflate(R.layout.fragment_home, container, false);

		if (mArrDeals == null) {
			mArrDeals = new ArrayList<DealObj>();
		}
		// Clear old data
		mArrDeals.clear();

		// list = (ListView) v.findViewById(R.id.list);
		lblNoResult = (TextView) v.findViewById(R.id.lblNoResult);
		lytRetry = (LinearLayout) v.findViewById(R.id.llRetry);
		// btnRetry = (Button) v.findViewById(R.id.btnRetry);

		// Declare object of userFunctions class
		userFunction = new UserFunctions();
		utils = new Utils(getActivity());

		new loadFirstListView().execute();
		mPullRefreshListView = (PullToRefreshListView) v
				.findViewById(R.id.lsvPullToRefresh);
		list = mPullRefreshListView.getRefreshableView();
		// Set a listener to be invoked when the list should be refreshed.
		if (NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
			mPullRefreshListView
					.setOnRefreshListener(new OnRefreshListener<ListView>() {
						@Override
						public void onRefresh(
								PullToRefreshBase<ListView> refreshView) {

							String label = DateUtils.formatDateTime(
									getActivity(), System.currentTimeMillis(),
									DateUtils.FORMAT_SHOW_TIME
											| DateUtils.FORMAT_SHOW_DATE
											| DateUtils.FORMAT_ABBREV_ALL);

							// Update the LastUpdatedLabel
							refreshView.getLoadingLayoutProxy()
									.setLastUpdatedLabel(label);
							// menuItems = new ArrayList<HashMap<String,
							// String>>();
							mArrDeals = new ArrayList<DealObj>();
							new loadFirstListView().execute();
							// new loadingPullRefresh().execute();

						}
					});
		} else {
			Toast.makeText(getActivity(),
					getString(R.string.no_internet_connection),
					Toast.LENGTH_SHORT).show();
		}

		// Add an end-of-list listener
		mPullRefreshListView
				.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

					@Override
					public void onLastItemVisible() {
						// Toast.makeText(getActivity(), "end page",
						// Toast.LENGTH_SHORT).show();
					}
				});

		// Listener to get selected id when list item clicked
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				// DealObj dealObj = null;
				//
				// HashMap<String, String> item = new HashMap<String, String>();
				// item = menuItems.get(position - 1);
				if (DatabaseUtility.checkExistsDeals(getActivity(), mArrDeals
						.get(position - 1).getDeal_id())) {
					Log.e("", "DATA " + "data da co du lieu");
				} else {
					// dealObj = new DealObj();
					// dealObj.setDeal_id(item.get(userFunction.key_deals_id));
					// dealObj.setTitle(item.get(userFunction.key_deals_title));
					// dealObj.setCompany(item.get(userFunction.key_deals_company));
					// dealObj.setStart_date(item
					// .get(userFunction.key_deals_date_start));
					// dealObj.setEnd_date(item
					// .get(userFunction.key_deals_date_end));
					// dealObj.setStart_time(item
					// .get(userFunction.key_deals_time_start));
					// dealObj.setEnd_time(item
					// .get(userFunction.key_deals_time_end));
					// dealObj.setAfter_discount_value(Double.parseDouble(item
					// .get(userFunction.key_deals_after_disc_value)));
					// dealObj.setStart_value(Double.parseDouble(item
					// .get(userFunction.key_deals_start_value)));
					// dealObj.setAttend(Integer.parseInt(item
					// .get(userFunction.key_deals_attended)));
					//
					// dealObj.setImage(userFunction.URLAdmin
					// + item.get(userFunction.key_deals_image));

					DatabaseUtility.insertDeals(getActivity(),
							mArrDeals.get(position - 1));

					Log.e("", "DATA " + "Successfully");
				}
				// Pass id to onListSelected method on HomeActivity
				// mCallback.onListSelected(item.get(userFunction.key_deals_id));
				Intent i = new Intent(getActivity(), ActivityDetail.class);
				i.putExtra(utils.EXTRA_DEAL_ID, mArrDeals.get(position - 1)
						.getDeal_id());
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_left);

				// Set the item as checked to be highlighted when in two-pane
				// layout
				list.setItemChecked(position, true);
			}
		});

		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	// Load first 10 videos
	private class loadFirstListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Showing progress dialog before sending http request
			// pDialog = new ProgressDialogWithMessage(getActivity());
			// pDialog.setMessage("Please wait...");
			// pDialog.setIndeterminate(true);
			// pDialog.setCancelable(false);
			// pDialog.show();

			ActivityHome.rltProgress.setVisibility(View.VISIBLE);
		}

		protected Void doInBackground(Void... unused) {
			try{
				if (NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
					getDataFromServer();
				} else {
					// Load data from database
					getDataFromDB();
				}
			} catch(Exception ex){
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			}
			return null;
		}

		protected void onPostExecute(Void unused) {
			// Condition if data length uder 10 button loadMore is remove
			if (isAdded()) {
				if (mArrDeals.size() != 0) {
					// Check paramter notif
					lytRetry.setVisibility(View.GONE);
					list.setVisibility(View.VISIBLE);
					lblNoResult.setVisibility(View.GONE);
					// Getting adapter
					mla = new HomeAdapter(getActivity(), mArrDeals);
					mla.notifyDataSetChanged();
					list.setAdapter(mla);
					mPullRefreshListView.onRefreshComplete();

				} else {
					// list.removeFooterView(btnLoadMore);
					if (json != null) {
						lblNoResult.setVisibility(View.VISIBLE);
						lytRetry.setVisibility(View.GONE);

					} else {
						lblNoResult.setVisibility(View.GONE);
						lytRetry.setVisibility(View.VISIBLE);
						Toast.makeText(getActivity(),
								getString(R.string.no_connection),
								Toast.LENGTH_SHORT).show();
					}
				}
			}
			super.onPostExecute(unused);
			// Closing progress dialog
			// pDialog.dismiss();
			ActivityHome.rltProgress.setVisibility(View.GONE);
		}
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		// if (pDialog != null) {
		// pDialog.dismiss();
		// pDialog = null;
		// }
	}

	public void getDataFromServer() {
		if (GlobalValue.myClient != null) {
			client = GlobalValue.myClient.getUsername();
		} else {
			client = "";
		}
		jsonCurrency = userFunction.currency(getActivity());
		json = userFunction.featuredDeals(client, getActivity());
		Log.e("url", "url: " + json);

		if (json != null) {
			ArrayList<DealObj> arr = JSONParser.parserDeal(
					userFunction.array_latest_deals, json.toString());
			mArrDeals.addAll(arr);
		}
		if (jsonCurrency != null) {
			JSONArray currencyArray;
			try {
				currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);
			} catch (JSONException e) {
				throw new RuntimeException(e);
			} catch (Exception ex) {
	            throw new RuntimeException(ex);
	        }
		}

	}

	public void getDataFromDB() {
		try {
			APIObj currencyInfos = DatabaseUtility.getResuftFromApi(
					getActivity(), UserFunctions.URL_CURRENCY);
			Log.e("FragmentFeatured", "Database: " + currencyInfos);
			String jsonCurr = "";
			if (currencyInfos != null) {
				jsonCurr = currencyInfos.getmResult();
			}
			jsonCurrency = new JSONObject(jsonCurr);

			APIObj featuredDeals = DatabaseUtility.getResuftFromApi(
					getActivity(), UserFunctions.URL_DEAL_FEATURED);
			Log.e("FragmentFeatured", "Database: " + featuredDeals);
			String jsonAllDeal = "";
			if (featuredDeals != null) {
				jsonAllDeal = featuredDeals.getmResult();
			}
			json = new JSONObject(jsonAllDeal);
			// }
			Log.e("url", "url: " + json);

			if (json != null) {
				ArrayList<DealObj> arr = JSONParser.parserDeal(
						userFunction.array_latest_deals, json.toString());
				mArrDeals.addAll(arr);
			}
			if (jsonCurrency != null) {
				JSONArray currencyArray = jsonCurrency
						.getJSONArray(userFunction.array_currency);
				JSONObject currencyObject = currencyArray.getJSONObject(0);
				Utils.mCurrency = " "
						+ currencyObject
								.getString(userFunction.key_currency_code);

			}

		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}
}
