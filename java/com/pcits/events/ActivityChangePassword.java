package com.pcits.events;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.widgets.FloatLabeledEditText;

public class ActivityChangePassword extends FragmentActivity implements
		OnClickListener {

	// Declare object of Utils class
	private FloatLabeledEditText txtCurrentPass, txtNewPass, txtConfirmNewPass;
	private Button btnChange;

	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private String username;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_change_password);
		initUI();
		initControl();
	}

	private void initUI() {
		llMenu = (LinearLayout) findViewById(R.id.llMenu);

		txtCurrentPass = (FloatLabeledEditText) findViewById(R.id.txtCurrentPass);
		txtNewPass = (FloatLabeledEditText) findViewById(R.id.txtNewPass);
		txtConfirmNewPass = (FloatLabeledEditText) findViewById(R.id.txtConfirmNewPass);
		btnChange = (Button) findViewById(R.id.btnChange);
	}

	private void initControl() {
		btnChange.setOnClickListener(this);
		llMenu.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == llMenu) {
			finish();
		}
		if (v.getId() == R.id.btnChange) {
			changePass();
		}
	}

	private void changePass() {
		String currentPass = txtCurrentPass.getText().toString();
		final String newPass = txtNewPass.getText().toString();
		String confirmPass = txtConfirmNewPass.getText().toString();

		// Validate input.
		if (currentPass.isEmpty()) {
			Toast.makeText(this, "Please, input current password",
					Toast.LENGTH_SHORT).show();
			txtCurrentPass.requestFocus();
			return;
		}
		if (newPass.isEmpty()) {
			Toast.makeText(this, "Please, input current password",
					Toast.LENGTH_SHORT).show();
			txtNewPass.requestFocus();
			return;
		}
		if (confirmPass.isEmpty()) {
			Toast.makeText(this, "Please, input confirm password",
					Toast.LENGTH_SHORT).show();
			return;
		} else {
			if (!confirmPass.equals(newPass)) {
				Toast.makeText(this, "Repassword is wrong !",
						Toast.LENGTH_SHORT).show();
				return;
			}
		}
		if (GlobalValue.myClient != null) {
			username = GlobalValue.myClient.getUsername();
		}
		if (GlobalValue.myUser != null) {
			username = GlobalValue.myUser.getUsername();
		}

		ModelManager.changePassword(ActivityChangePassword.this, username,
				currentPass, newPass, true, new ModelManagerListener() {

					@Override
					public void onSuccess(Object object) {
						// TODO Auto-generated method stub
						String strJson = (String) object;
						Toast.makeText(ActivityChangePassword.this,
								checkResult(strJson), Toast.LENGTH_SHORT)
								.show();
						logOut();
					}

					@Override
					public void onError() {
						// TODO Auto-generated method stub

					}
				});
	}

	protected String checkResult(String strJson) {
		// TODO Auto-generated method stub
		JSONObject json = null;
		String message = "";
		try {
			json = new JSONObject(strJson);
			if (json.getString("status").equals("success")) {
				message = this.getString(R.string.message_success);
			} else {
				message = json.getString("message");
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return message;
	}

	// Log out confirm
	private void logOut() {
		try {
			LoginManager.getInstance().logOut();
		}catch (Exception ex){

		}
		// Reset account.
		GlobalValue.myClient = null;
		GlobalValue.myUser = null;
		// Log out.
		Intent i = new Intent(ActivityChangePassword.this, ActivityLogin.class);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
		finish();
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// Previous page or exit
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
