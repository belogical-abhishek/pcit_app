package com.pcits.events;

import java.math.BigDecimal;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.KenBurnsView;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedBold;

public class BuyCreditActivity extends Activity {

	private static final String TAG = "BuyCreditActivity";
	// Declare NotBoringActionBar
	private LinearLayout llMenu, llBuyCredits;
	private KenBurnsView mHeaderPicture;
	private TextView lblTitleHeader;
	private EditText txtCredits;
	private Button btnPay;
	private String credit, price;
	private TextViewRobotoCondensedBold lblMonney;
	private double savevalue;

	private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;

	// note that these credentials will differ between live & sandbox
	// environments.
	private static final String CONFIG_CLIENT_ID = "ASyaxRDC-Wy2QOCymQ7HpAAe-iD70tJQu9nXKyMfLySYr4OQd2cEtNZU8lyE";

	private static final int REQUEST_CODE_PAYMENT = 1;
	private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
	private static final int REQUEST_CODE_PROFILE_SHARING = 3;

	private static PayPalConfiguration config = new PayPalConfiguration()
			.environment(CONFIG_ENVIRONMENT)
			.clientId(CONFIG_CLIENT_ID)
			// The following are only used in PayPalFuturePaymentActivity.
			.merchantName("Example Merchant")
			.merchantPrivacyPolicyUri(
					Uri.parse("https://www.example.com/privacy"))
			.merchantUserAgreementUri(
					Uri.parse("https://www.example.com/legal"));

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_buy_credits);
		Intent intent = new Intent(this, PayPalService.class);
		intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
		startService(intent);
		initUI();
		initControl();
		initNotBoringActionBar();
	}

	private void initNotBoringActionBar() {
		mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);
	}

	private void initUI() {
		lblTitleHeader = (TextView) findViewById(R.id.lblTitleHeader);
		llMenu = (LinearLayout) findViewById(R.id.llMenu);
		txtCredits = (EditText) findViewById(R.id.txtCredits);
		btnPay = (Button) findViewById(R.id.btnPay);
		lblMonney = (TextViewRobotoCondensedBold) findViewById(R.id.lblTotalMonney);
		llBuyCredits = (LinearLayout) findViewById(R.id.llBuyCredits);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		String valuesCredits = txtCredits.getText().toString();
		if (valuesCredits.equalsIgnoreCase("")
				&& valuesCredits.equalsIgnoreCase("")) {
			lblMonney.setText("");
		} else {
			savevalue = (Double.parseDouble(valuesCredits) / 10);
			lblMonney.setText(Double.toString(savevalue) + " GBP");
		}
		return super.onTouchEvent(event);

	}

	private void initControl() {
		//
		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		//
		llBuyCredits.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				String valuesCredits = txtCredits.getText().toString();
				if (valuesCredits.equalsIgnoreCase("")
						&& valuesCredits.equalsIgnoreCase("")) {
					lblMonney.setText("");
				} else {
					savevalue = (Double.parseDouble(valuesCredits) / 10);
					lblMonney.setText(Double.toString(savevalue) + " GBP");
				}
				return false;
			}
		});
		//
		btnPay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String valuesCredits = txtCredits.getText().toString();
				if (valuesCredits.equalsIgnoreCase("")
						&& valuesCredits.equalsIgnoreCase("")) {
					lblMonney.setText("");
				} else {
					savevalue = (Double.parseDouble(valuesCredits) / 10);
					lblMonney.setText(Double.toString(savevalue) + " GBP");
				}
				String message = validateForm();
				if (!message.equals("success")) {
					Toast.makeText(BuyCreditActivity.this, message,
							Toast.LENGTH_SHORT).show();
				} else {
					if (credit != null) {
						Log.e("BuyCreditActivity", "credit: " + credit);
						// buyCredits(GlobalValue.myUser.getUsername(),
						// Integer.parseInt(credit));
						requestPaypal(savevalue, "Content",
								"GBP");

						Toast.makeText(BuyCreditActivity.this,
								savevalue + Utils.mCurrency,
								Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			PaymentConfirmation confirm = data
					.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
			if (confirm != null) {
				try {
					Log.i(TAG, confirm.toJSONObject().toString(4));
					Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
					String PAYMENT_PAY_ID = confirm.toJSONObject()
							.getJSONObject("response").getString("id");
					String PAYMENT_AMOUNT = confirm.getPayment().toJSONObject()
							.getString("amount");
					Log.e("ActivityDetail", "id :" + PAYMENT_PAY_ID);
					Log.e("ActivityDetail", "amount :" + PAYMENT_AMOUNT);
					Toast toast = Toast.makeText(BuyCreditActivity.this,
							"Payment successfully !", Toast.LENGTH_LONG);
					toast.setGravity(
							Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
					toast.show();
//					if (GlobalValue.myUser != null) {
						buyCredits("admin4",
								Integer.parseInt(credit));
//					}
					//
					// Intent i;
					// // Open ActivityBrowser
					// i = new Intent(getApplicationContext(),
					// ActivityBrowser.class);
					// i.putExtra(utils.EXTRA_DEAL_URL, mUrl);
					// i.putExtra(utils.EXTRA_DEAL_TITLE, mTitle);
					// startActivity(i);

				} catch (JSONException e) {
					Log.e(TAG, "an extremely unlikely failure occurred: ", e);
				}
			}
		} else if (resultCode == Activity.RESULT_CANCELED) {
			Log.i(TAG, "The user canceled.");
		} else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
			Log.i(TAG, "An invalid payment was submitted. Please see the docs.");
		}
		// }
	}

	private void requestPaypal(double value, String content, String currency) {
		// PayPalPayment thingToBuy =
		// getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
		PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(value),
				currency, content, PayPalPayment.PAYMENT_INTENT_SALE);

		/*
		 * See getStuffToBuy(..) for examples of some available payment options.
		 */

		Intent intent = new Intent(BuyCreditActivity.this,
				PaymentActivity.class);

		// send the same configuration for restart resiliency
		intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

		intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

		startActivityForResult(intent, REQUEST_CODE_PAYMENT);

	}

	@Override
	public void onDestroy() {
		// Stop service when done
		stopService(new Intent(this, PayPalService.class));
		super.onDestroy();
	}

	private String validateForm() {
		// TODO Auto-generated method stub
		String message = "success";
		credit = txtCredits.getText().toString();
		if (credit.isEmpty()) {
			Toast.makeText(this, "Please, input credit", Toast.LENGTH_SHORT)
					.show();
			return message;
		}
		if (credit.equalsIgnoreCase("0")) {
			Toast.makeText(this, "Credit > 0", Toast.LENGTH_SHORT).show();
			return message;
		}
		return message;
	}

	private void buyCredits(String username, int credits) {
		// TODO Auto-generated method stub
		Log.e("ACCOUNT INFO", "ACCOUNT INFO: " + username + " == " + credits);
		ModelManager.buyCredits(this, username, credits, true,
				new ModelManagerListener() {

					@Override
					public void onSuccess(Object object) {
						// TODO Auto-generated method stub
						String strJson = (String) object;
						Toast.makeText(BuyCreditActivity.this,
								checkResult(strJson), Toast.LENGTH_SHORT)
								.show();
						// Intent i = new Intent(ActivitySignUp.this,
						// ActivityLogin.class);
						// startActivity(i);
					}

					@Override
					public void onError() {
						// TODO Auto-generated method stub

					}
				});
	}

	protected String checkResult(String strJson) {
		// TODO Auto-generated method stub
		JSONObject json = null;
		String message = "";
		try {
			json = new JSONObject(strJson);
			if (json.getString("status").equals("success")) {
				message = this.getString(R.string.message_success);
				finish();
			} else {
				message = json.getString("message");
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return message;
	}
}
