package com.pcits.events.fragments;

import java.util.Collections;
import java.util.Comparator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityDetailTickets;
import com.pcits.events.ActivityHome;
import com.pcits.events.R;
import com.pcits.events.adapters.StickyTicketsAdapter;
import com.pcits.events.adapters.TicketsAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.PaidticketsObj;
import com.pcits.events.stickylist.StickyListHeadersListView;
import com.pcits.events.widgets.KenBurnsView;

public class TicketsFragment extends Fragment implements
		OnClickListener, AdapterView.OnItemClickListener {

	private View v;
	private TicketsAdapter adapterTicket;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;
	private TextView lblTitleHeader;
	private int type;

	// Declare sticky list header
	private StickyListHeadersListView mStickyList;
	private StickyTicketsAdapter mStickyAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		v = inflater.inflate(R.layout.fragment_menu_tickets, container, false);
		try{
			initUI();
			// getData();
			setData();
			initNotBoringActionBar();
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return v;
	}

	private void initNotBoringActionBar() {
		mHeaderPicture = (KenBurnsView) v.findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}

	private void initUI() {
		mStickyList = (StickyListHeadersListView) v.findViewById(R.id.lvTicket);
		lblTitleHeader = (TextView) v.findViewById(R.id.lblTitleHeader);
		lblTitleHeader.setText("My Ticket");
		llMenu = (LinearLayout) v.findViewById(R.id.llMenu);
		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// ActivityHome.resideMenu.openMenu();
				//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
				Intent intent=new Intent(getActivity(),ActivityHome.class);
				startActivity(intent);
				getActivity().finish();
			}
		});

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		GlobalValue.ticket = GlobalValue.arrTickets.get(position);
		Intent i = new Intent(getActivity(), ActivityDetailTickets.class);
		startActivity(i);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// getData();
	}

	private void getData() {
		try{
			if (GlobalValue.myClient != null) {
				type = 1;
	
				ModelManager.getListTickets(getActivity(), Integer.toString(type),
						GlobalValue.myClient.getUsername(), true,
						new ModelManagerListener() {
	
							@Override
							public void onSuccess(Object object) {
								// TODO Auto-generated method stub
								String json = (String) object;
								GlobalValue.arrTickets = ParserUtility
										.getListTickets(json);
								if (GlobalValue.arrTickets.size() > 0) {
									adapterTicket = new TicketsAdapter(
											getActivity(), GlobalValue.arrTickets);
									adapterTicket.notifyDataSetChanged();
								} else {
									Toast.makeText(getActivity(), "No Data",
											Toast.LENGTH_SHORT).show();
								}
							}
	
							@Override
							public void onError() {
								// TODO Auto-generated method stub
	
							}
						});
			}
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	private void getDataDB() {

	}

	@Override
	public void onClick(View v) {
	}

	private void setData() {
		try{
			if (GlobalValue.myClient != null) {
				type = 1;
	
				ModelManager.getListTickets(getActivity(), Integer.toString(type),
						GlobalValue.myClient.getUsername(), true,
						new ModelManagerListener() {
	
							@Override
							public void onSuccess(Object object) {
								try{
									// TODO Auto-generated method stub
									String json = (String) object;
									GlobalValue.arrTickets = ParserUtility
											.getListTickets(json);
									Log.e("Tickets", "Json : " + json);
									Log.e("Tickets", "size : " + GlobalValue.arrTickets.size());
									if (GlobalValue.arrTickets.size() > 0) {
										// Sort by status
										Collections.sort(GlobalValue.arrTickets,
												new Comparator<PaidticketsObj>() {
		
													@Override
													public int compare(
															PaidticketsObj lhs,
															PaidticketsObj rhs) {
														// TODO Auto-generated method
														// stub
														Integer stt1 = Integer
																.valueOf(lhs
																		.getStatus());
														Integer stt2 = Integer
																.valueOf(rhs
																		.getStatus());
														return stt1.compareTo(stt2);
													}
												});
		
										mStickyAdapter = new StickyTicketsAdapter(
												getActivity(), GlobalValue.arrTickets);
										mStickyList.setAdapter(mStickyAdapter);
									} else {
										Toast.makeText(getActivity(), "No Data",
												Toast.LENGTH_SHORT).show();
									}
								} catch (Exception ex) {
					                throw new RuntimeException(ex);
					            }
							}
	
							@Override
							public void onError() {
								// TODO Auto-generated method stub
	
							}
						});
			}
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}
}
