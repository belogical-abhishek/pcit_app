/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pcits.events.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.pcits.common.utils.Logging;
import com.pcits.events.MapPinActivity;
import com.pcits.events.R;
import com.pcits.events.adapters.CountryAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.widgets.FloatLabeledEditText;
import com.pcits.events.widgets.floatingactionbutton.FloatingActionButton;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;
import com.pcits.events.wizard.model.CustomAddEventPage3;
import com.pcits.events.wizard.ui.PageFragmentCallbacks;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

public class AddEventPage3Fragment extends Fragment {

	private static final String TAG = "AddEventPage3Fragment";

	private static final String ARG_KEY = "key";

	private PageFragmentCallbacks mCallbacks;
	private String mKey;
	private CustomAddEventPage3 mPage;

	private EditText mFlAddress, mFlCity, mFlCounty, mFlPostalCode,
			mFlLat, mFlLng;
	private TextViewRobotoCondensedRegular mLblCountry;

	private ArrayList<String> mArrCountry;

	private View mRootView;
	private Bundle mBundle;

	private FloatingActionButton mFabMap;
	private final int REQUEST_ADDRESS = 1000;

	public static AddEventPage3Fragment create(String key) {
		Bundle args = new Bundle();
		args.putString(ARG_KEY, key);

		AddEventPage3Fragment fragment = new AddEventPage3Fragment();
		fragment.setArguments(args);
		return fragment;
	}

	public AddEventPage3Fragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mBundle = savedInstanceState;

		Bundle args = getArguments();
		mKey = args.getString(ARG_KEY);
		mPage = (CustomAddEventPage3) mCallbacks.onGetPage(mKey);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragment_add_event_page3,
				container, false);
		// ((TextView) mRootView.findViewById(android.R.id.title)).setText(mPage
		// .getTitle());
		try {
			initUI();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return mRootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (!(activity instanceof PageFragmentCallbacks)) {
			throw new ClassCastException(
					"Activity must implement PageFragmentCallbacks");
		}

		mCallbacks = (PageFragmentCallbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);

		// In a future update to the support library, this should override
		// setUserVisibleHint
		// instead of setMenuVisibility.
		if (mFlAddress != null && mFlCity != null && mLblCountry != null
				|| mFlPostalCode != null && mFlLat != null && mFlLng != null) {
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			if (!menuVisible) {
				imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (requestCode == REQUEST_ADDRESS) {
			if (resultCode == Activity.RESULT_OK) {
				if(GlobalValue.createEventObj != null){
					mFlLat.setText(String.valueOf(GlobalValue.createEventObj.getLatitude()));
					mFlLng.setText(String.valueOf(GlobalValue.createEventObj.getLongitude()));
				}
				String address = data.getStringExtra("address");
				Log.d(TAG, "onActivityResult: "+address);
				if(address!=null) {
					String[] arr = address.split(":");
					int size = arr.length;
					if (size == 1) {
						mFlAddress.setText(arr[0]);
						mFlCity.setText("");
						mFlCounty.setText("");
						mLblCountry.setText(getResources().getString(
								R.string.country));
					} else if (size == 2) {
						mFlAddress.setText(arr[0]);
						mLblCountry.setText(arr[1]);
						mFlCity.setText("");
						mFlCounty.setText("");
					} else if (size == 3) {
						mFlAddress.setText(arr[0]);
						mFlCounty.setText(arr[1]);
						mLblCountry.setText(arr[2]);
						mFlCity.setText("");
					}  else {
						mFlAddress.setText(arr[0]);
						mFlCity.setText(arr[1]);
						mFlCounty.setText(arr[2]);
						mLblCountry.setText(arr[3]);
						mFlPostalCode.setText(arr[4]);
					}
				}else{
					Toast.makeText(getContext(),"Address invalid",Toast.LENGTH_LONG).show();
				}

			} else {
				Log.e(TAG, "Fail");
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void saveState() {
		try {
			GlobalValue.dealsObj.setAddress(mFlAddress.getText().toString().trim());
			GlobalValue.dealsObj.setCity(mFlCity.getText().toString().trim());
			GlobalValue.dealsObj.setCounty(mFlCounty.getText().toString().trim());
			GlobalValue.dealsObj.setCountry(mLblCountry.getText().toString()
					.trim());
			GlobalValue.dealsObj.setPostcode(mFlPostalCode.getText().toString()
					.trim());
			GlobalValue.dealsObj.setLatitude(Double.parseDouble(mFlLat
					.getText().toString().trim()));
			GlobalValue.dealsObj.setLongitude(Double.parseDouble(mFlLng
					.getText().toString().trim()));
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public void restoreState() {
		try {
			mFlAddress.setText(GlobalValue.dealsObj.getAddress());
			mFlCity.setText(GlobalValue.dealsObj.getCity());
			mFlCounty.setText(GlobalValue.dealsObj.getCounty());
			mLblCountry.setText(GlobalValue.dealsObj.getCountry());
			mFlPostalCode.setText(GlobalValue.dealsObj.getPostcode());

			if (GlobalValue.dealsObj.getLatitude() != null) {
				mFlLat.setText(GlobalValue.dealsObj.getLatitude() + "");
			}
			if (GlobalValue.dealsObj.getLongitude() != null) {
				mFlLng.setText(GlobalValue.dealsObj.getLongitude() + "");
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void initUI() {
		try {
			mFlAddress = (EditText) mRootView
					.findViewById(R.id.txtAddress);

			mFlCity = (EditText) mRootView
					.findViewById(R.id.txtCity);

			mFlCounty = (EditText) mRootView
					.findViewById(R.id.txtCounty);

			mFlPostalCode = (EditText) mRootView
					.findViewById(R.id.txtPostCode);

			mFlLat = (EditText) mRootView
					.findViewById(R.id.txtLatitude);
			mFlLat.setKeyListener(null);

			mFlLng = (EditText) mRootView
					.findViewById(R.id.txtLongtitude);
			mFlLng.setKeyListener(null);

			mLblCountry = (TextViewRobotoCondensedRegular) mRootView
					.findViewById(R.id.lblCountry);

			mLblCountry.setText(Locale.getDefault().getCountry());

			mFabMap = (FloatingActionButton) mRootView
					.findViewById(R.id.fab_map);

			// Should call this method in the end of declaring UI.
			initControl();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void initControl() {
		try {
			mLblCountry.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (NetworkUtility.getInstance(getActivity())
							.isNetworkAvailable()) {
						// Disable this field to avoid show more than 1 dialog.
						mLblCountry.setEnabled(false);

						showCountryDialog();
					} else {
						Toast.makeText(getActivity(), "No network connection",
								Toast.LENGTH_SHORT).show();
					}
				}
			});

			mFabMap.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					showMapToPin();
				}
			});

			mFlAddress.setOnFocusChangeListener(new OnFocusChangeListener() {

				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (!hasFocus) {
						getLocationWhenFocusChanged();
					}
				}
			});

			mFlCity.setOnFocusChangeListener(new OnFocusChangeListener() {

				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (!hasFocus) {
						getLocationWhenFocusChanged();
					}
				}
			});

			mFlCounty.setOnFocusChangeListener(new OnFocusChangeListener() {

				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (!hasFocus) {
						getLocationWhenFocusChanged();
					}
				}
			});

			mLblCountry.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					getLocationWhenFocusChanged();
				}
			});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void getLocationWhenFocusChanged() {
		try {
			String address = mFlAddress.getText().toString().trim();
			String city = mFlCity.getText().toString().trim();
			String county = mFlCounty.getText().toString().trim();
			String country = mLblCountry.getText().toString();
			if (country.equalsIgnoreCase("country")) {
				country = "";
			}

			String strAddress = address + " " + city + " " + county + " "
					+ country;

			if (!strAddress.trim().equals("")) {
				new GetLatLngFromAddress().execute(strAddress.trim());
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void showMapToPin() {
		Intent i = new Intent(getActivity(), MapPinActivity.class);
		startActivityForResult(i, REQUEST_ADDRESS);
	}

	private void showCountryDialog() {
		try {
			final UserFunctions userFunction = new UserFunctions();

			// Get countries for first click.
			if (mArrCountry == null || mArrCountry.size() <= 0) {
				mArrCountry = new ArrayList<String>();

				new AsyncTask<Void, Void, Void>() {

					@Override
					protected Void doInBackground(Void... params) {
						// TODO Auto-generated method stub
						try {
							// If mActivity equal activityCategory then use API
							// dealByCategory
							JSONObject json = userFunction.showByCountry();
							if (json != null) {
								JSONArray dataDealsArray = json
										.getJSONArray("data");

								for (int i = 0; i < dataDealsArray.length(); i++) {
									JSONObject obj = dataDealsArray
											.getJSONObject(i);
									mArrCountry
											.add(obj.getString(userFunction.key_showcountry_country));
								}
							}

						} catch (JSONException ex) {
							Logging.writeExceptionFromStackTrace(ex,
									ex.getMessage());
						} catch (Exception ex) {
							Logging.writeExceptionFromStackTrace(ex,
									ex.getMessage());
						}
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						// TODO Auto-generated method stub
						super.onPostExecute(result);

						final Dialog dialog = new Dialog(getActivity());
						dialog.getWindow();
						dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						dialog.setContentView(R.layout.layout_nice_country);
						final ListView lsvCategory = (ListView) dialog
								.findViewById(R.id.lsvFilterCategory);
						CountryAdapter adapter = new CountryAdapter(
								getActivity(), mArrCountry);
						lsvCategory.setAdapter(adapter);
						lsvCategory
								.setOnItemClickListener(new OnItemClickListener() {

									@Override
									public void onItemClick(
											AdapterView<?> parent, View view,
											int position, long id) {
										// TODO Auto-generated method stub
										mLblCountry.setText(mArrCountry
												.get(position));
										dialog.dismiss();

										// Set country
										GlobalValue.dealsObj
												.setCountry(mArrCountry
														.get(position));
									}
								});
						dialog.show();

						// Enable country field.
						mLblCountry.setEnabled(true);
					}
				}.execute();
			} else {
				final Dialog dialog = new Dialog(getActivity());
				dialog.getWindow();
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.layout_filter_category);
				final ListView lsvCategory = (ListView) dialog
						.findViewById(R.id.lsvFilterCategory);
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(
						getActivity(), android.R.layout.simple_list_item_1,
						mArrCountry);
				lsvCategory.setAdapter(adapter);
				lsvCategory.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub
						mLblCountry.setText(mArrCountry.get(position));
						dialog.dismiss();
					}
				});
				dialog.show();

				// Enable country field.
				mLblCountry.setEnabled(true);
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private class GetLatLngFromAddress extends AsyncTask<String, Void, LatLng> {

		@Override
		protected LatLng doInBackground(String... params) {
			try {
				String strAddress = params[0];
				if (strAddress.contains(" ")) {
					strAddress = strAddress.replace(" ", "%20");
				}
				String uri = "http://maps.google.com/maps/api/geocode/json?address="
						+ strAddress + "&sensor=false";
//				HttpGet httpGet = new HttpGet(uri);
//				HttpClient client = new DefaultHttpClient();
//				org.apache.http.HttpResponse response;

				StringBuilder stringBuilder = new StringBuilder();

				try {
					URL url = new URL(uri);

					InputStream stream = url.openStream();
					int b;
					while ((b = stream.read()) != -1) {
						stringBuilder.append((char) b);
					}
				} catch (Exception ex) {
					Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
					return new LatLng(0.0, 0.0);
				}

				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject = new JSONObject(stringBuilder.toString());

					double lng = ((JSONArray) jsonObject.get("results"))
							.getJSONObject(0).getJSONObject("geometry")
							.getJSONObject("location").getDouble("lng");

					double lat = ((JSONArray) jsonObject.get("results"))
							.getJSONObject(0).getJSONObject("geometry")
							.getJSONObject("location").getDouble("lat");

					return new LatLng(lat, lng);
				} catch (Exception ex) {
					Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
					return new LatLng(0.0, 0.0);
				}
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
				return new LatLng(0.0, 0.0);
			}
		}

		@Override
		protected void onPostExecute(LatLng result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			try {
				GlobalValue.dealsObj.setLatitude(result.latitude);
				GlobalValue.dealsObj.setLongitude(result.longitude);

				mFlLat.setText(GlobalValue.dealsObj.getLatitude() + "");
				mFlLng.setText(GlobalValue.dealsObj.getLongitude() + "");
			} catch (Exception ex) {
				throw new RuntimeException(ex);
			}
		}
	}
}
