package com.pcits.events.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.google.gson.reflect.TypeToken;
import com.pcits.common.utils.EventsPreferences;
import com.pcits.common.utils.GsonUtility;
import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityDetail;
import com.pcits.events.ActivityPrivateEventDetails;
import com.pcits.events.AddEventActivity;
import com.pcits.events.AddPrivateEventActivity;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.obj.CreateEventObj;
import com.pcits.events.utils.AccessStorage;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedBold;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;
import com.pcits.events.wizard.model.ICustomPage;

import java.io.File;

public class PreviewPrivateEventFragment extends Fragment implements ICustomPage {

    private Display display;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_preview_event,
                container, false);
        try {
            display = getActivity().getWindowManager().getDefaultDisplay();

            initPreview(rootView);
        } catch (Exception ex) {
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
        return rootView;
    }

    private void initPreview(View rootView) {
        RelativeLayout rltEvent = (RelativeLayout) rootView
                .findViewById(R.id.lytImage);
        ImageView imgThumbnail = (ImageView) rootView
                .findViewById(R.id.imgThumbnail);
        imgThumbnail.getLayoutParams().width = display.getWidth();
        imgThumbnail.getLayoutParams().height = display.getWidth() * 9 / 16;

        TextViewRobotoCondensedBold lblTitle = (TextViewRobotoCondensedBold) rootView
                .findViewById(R.id.lblTitle);
        TextViewRobotoCondensedRegular lblCompany = (TextViewRobotoCondensedRegular) rootView
                .findViewById(R.id.lblCompany);
        TextViewRobotoCondensedRegular lblStartValue = (TextViewRobotoCondensedRegular) rootView
                .findViewById(R.id.lblStartValue);
        TextViewRobotoCondensedRegular lblAfterDiscount = (TextViewRobotoCondensedRegular) rootView
                .findViewById(R.id.lblAfterDiscountValue);
        TextViewRobotoCondensedRegular lblDate = (TextViewRobotoCondensedRegular) rootView
                .findViewById(R.id.lblDate);
        TextViewRobotoCondensedRegular lblTime = (TextViewRobotoCondensedRegular) rootView
                .findViewById(R.id.lblTime);
        TextViewRobotoCondensedRegular lblAttending = (TextViewRobotoCondensedRegular) rootView
                .findViewById(R.id.lblAttending);

        Log.d("preview", "initPreview: ");
        try {

            TypeToken<CreateEventObj> token = new TypeToken<CreateEventObj>(){};
            final CreateEventObj eventObj = GsonUtility.convertJSONStringToObject(EventsPreferences.getKeyPrivateEvent(getContext()),token);

            Log.d("Preview", "initPreview: "+GsonUtility.convertObjectToJSONString(eventObj));
            if (eventObj.getTitle() != null) {
                lblTitle.setText(eventObj.getTitle());
                lblTitle.setSelected(true);
            }
            if (eventObj.getCompany() != null) {
                lblCompany.setText("By: " + eventObj.getCompany());
            }
            if (eventObj.getStartDate() != null
                    && eventObj.getEndDate() != null) {
                lblDate.setText(DateTimeUtility.getDateFormat(
                        eventObj.getStartDate(), "yyyy-MM-dd", "dd MMM yy")
                        + " - "
                        + DateTimeUtility.getDateFormat(
                        eventObj.getEndDate(), "yyyy-MM-dd", "dd MMM yy"));
            }
            if (eventObj.getStartTimestamp() != null
                    && eventObj.getEndTimestamp() != null) {
                lblTime.setText(eventObj.getStartTimestamp()
                        + " - "
                        +
                        eventObj.getEndTimestamp());
            } /*else {
                if (eventObj.getStartDate() != null
                        && eventObj.getEndDate() != null) {
                    lblDate.setText(DateTimeUtility.convertStringToDate(
                            eventObj.getStartDate(), "yyyy-MM-dd",
                            "dd MMM yy")
                            + " - "
                            + DateTimeUtility.convertStringToDate(
                            eventObj.getEndDate(),
                            "yyyy-MM-dd", "dd MMM yy"));
                }
                if (eventObj.getStartTime() != null
                        && eventObj.getEndTime() != null) {
                    lblTime.setText(eventObj.getStartTime()
                            + " - " + eventObj.getEndTime());
                }
            }*/
            if (eventObj.getStartValue() != null) {
                String startValue = Utils.mCurrency + " "
                        + eventObj.getStartValue()+".0";

               /* lblStartValue.setText(startValue.substring(0,
                        startValue.indexOf(".")));*/
                lblStartValue.setText(startValue);
                lblStartValue.setPaintFlags(lblStartValue.getPaintFlags()
                        | Paint.STRIKE_THRU_TEXT_FLAG);
            }
            if (eventObj.getAfterDiscountValue() != null) {
                if (eventObj.getAfterDiscountValue() == 0) {
                    lblAfterDiscount.setText("Free");
                } else {
                    String afterValue = Utils.mCurrency + " "
                            + eventObj.getAfterDiscountValue()+".0";
                   /* lblAfterDiscount.setText(afterValue.substring(0,
                            afterValue.indexOf(".")));*/
                    lblAfterDiscount.setText(afterValue);
                }
            }

            if(eventObj.getViewattendee()!=null) {
                lblAttending.setText(eventObj.getViewattendee() + "");
            }
            else
                lblAttending.setText("0");


            if (eventObj.getImage() != null) {



                imgThumbnail.setImageBitmap(BitmapFactory.decodeFile(eventObj.getImage()));
            }
            else imgThumbnail.setImageDrawable(ContextCompat.getDrawable(getContext(),R.drawable.butterfly));

            imgThumbnail.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if(isValidated()) {
                        Intent intent = new Intent(getActivity(), ActivityPrivateEventDetails.class);
                        Bundle bundle = new Bundle();
                        Log.d("image", "onClick: " + eventObj.getImage());
                        bundle.putString("obj", EventsPreferences.getKeyPrivateEvent(getContext()));
                        bundle.putString("from", "new");
                        bundle.putString("image", eventObj.getImage());
                        bundle.putBoolean("isPreview", true);
                        bundle.putString("title", eventObj.getTitle());
                        bundle.putString("startdate", eventObj.getStartDate().toString());
                        bundle.putString("endate", eventObj.getEndDate().toString());
                        bundle.putString("startTime", eventObj.getStartTime());
                        bundle.putString("endTime", eventObj.getEndTime());
                        bundle.putString("description", eventObj.getDescription());
                        intent.putExtras(bundle);
                        getActivity().startActivity(intent);
                    }
                }
            });
            
            if(AddPrivateEventActivity.eventCroppedImage!=null)
                imgThumbnail.setImageBitmap(AddPrivateEventActivity.eventCroppedImage);
           /* if (eventObj.getImage()!= null) {

                Bitmap bitmap = AccessStorage
                        .loadPrescaledBitmap(eventObj.getImage());
                imgThumbnail.setImageBitmap(bitmap);
            }*/
//			} else if (GlobalValue.dealsObj.getImage() != null) {
//				AQuery aq = new AQuery(getActivity());
//				aq.id(imgThumbnail).image(
//						UserFunctions.URLAdmin
//								+ GlobalValue.dealsObj.getImage(), false, true);
//			}

            // Go to detail page
           /* imgThumbnail.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), ActivityPrivateEventDetails.class);
                    i.putExtra("isPreview", true);
                    getActivity().startActivity(i);
                }
            });*/
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private boolean isValidated() {
        try {
            boolean isValidated = true;
            TypeToken<CreateEventObj> token = new TypeToken<CreateEventObj>(){};
            final CreateEventObj eventObj = GsonUtility.convertJSONStringToObject(EventsPreferences.getKeyPrivateEvent(getContext()),token);



            if (TextUtils.isEmpty(eventObj.getTitle())) {
                Toast.makeText(getContext(),"Title cannot be empty",Toast.LENGTH_LONG).show();
                isValidated = false;
            }
            else if (TextUtils.isEmpty(eventObj.getCategoryId())) {
                Toast.makeText(getContext(),"Category cannot be empty",Toast.LENGTH_LONG).show();

                isValidated = false;
            }

            else if (eventObj.getStartValue() == null) {
                Toast.makeText(getContext(),"Start value cannot be empty",Toast.LENGTH_LONG).show();


                isValidated = false;
            } else if (eventObj.getAfterDiscountValue() == null) {
                Toast.makeText(getContext(),"After discount value cannot be empty",Toast.LENGTH_LONG).show();


                isValidated = false;
            } else if (TextUtils.isEmpty(eventObj
                    .getStartTimestamp())) {
                Toast.makeText(getContext(),"Start time cannot be empty",Toast.LENGTH_LONG).show();


                isValidated = false;
            } else if (TextUtils
                    .isEmpty(eventObj.getEndTimestamp())) {
                Toast.makeText(getContext(),"End time cannot be empty",Toast.LENGTH_LONG).show();


                isValidated = false;
            } else if (TextUtils.isEmpty(eventObj.getAddress())) {
                Toast.makeText(getContext(),"Address cannot be empty",Toast.LENGTH_LONG).show();


                isValidated = false;
            } else if (TextUtils.isEmpty(eventObj.getCity())) {
                Toast.makeText(getContext(),"City cannot be empty",Toast.LENGTH_LONG).show();

                isValidated = false;
            } else if (TextUtils.isEmpty(eventObj.getCounty())) {
                Toast.makeText(getContext(),"County cannot be empty",Toast.LENGTH_LONG).show();

                isValidated = false;
            } else if (TextUtils.isEmpty(eventObj.getCountry())) {
                Toast.makeText(getContext(),"Country cannot be empty",Toast.LENGTH_LONG).show();


                isValidated = false;
            } else if (TextUtils.isEmpty(eventObj.getAddress())) {
                Toast.makeText(getContext(),"Postal code cannot be empty",Toast.LENGTH_LONG).show();

                isValidated = false;
            } else if (TextUtils.isEmpty(eventObj.getDescription())) {
                Toast.makeText(getContext(),"Description cannot be empty",Toast.LENGTH_LONG).show();


                isValidated = false;
            }

            return isValidated;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
    @Override
    public void saveState() {
        // TODO Auto-generated method stub

    }

    @Override
    public void restoreState() {
        // TODO Auto-generated method stub

    }

}
