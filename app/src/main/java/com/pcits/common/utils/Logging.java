package com.pcits.common.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class Logging {
	// public static final String ERROR_FILE = "sdcard/" + R.string.app_name;
	public static final String ERROR_FILE = "sdcard/pcits.txt";
	public static final File debugFile = new File("sdcard/debug");
	public static SimpleDateFormat mDateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	public static SimpleDateFormat mShorDateFormat = new SimpleDateFormat(
			"yyyy-MM-dd");
	public static String values = "";

	public static void appendLog(String text, String filename) {
		File logFile = new File(filename);
		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
				appendLog(DeviceInfo.getAllInfo(), filename);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		try {
			// BufferedWriter for performance, true to set append to file flag
			BufferedWriter buf = new BufferedWriter(new FileWriter(logFile,
					true));
			buf.append(text);
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String getTime() {
		return mDateFormat.format(new Date());
	}

	public static String getDate() {
		return mShorDateFormat.format(new Date());
	}

	public static void writeExceptionFromStackTrace(Exception ex, String msg) {
		StackTraceElement[] stack = ex.getStackTrace();
		String exception = "";
		for (StackTraceElement s : stack) {
			exception = exception + s.toString() + "\n\t\t";
		}
		if (!values.isEmpty()) {
			appendLog(">>> " + getTime() + ":" + msg + "\n" + values + "\n"
					+ exception, ERROR_FILE);
			values = "";
		} else
			appendLog(">>> " + getTime() + ":" + msg + "\n" + exception,
					ERROR_FILE);

		if (debugFile.exists()) {
			ex.printStackTrace();
		}
	}

}
