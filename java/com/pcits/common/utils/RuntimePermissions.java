package com.pcits.common.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;

import com.pcits.events.R;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static com.pcits.events.utils.AppConstants.RC_LOCATION_PERM;
import static com.pcits.events.utils.AppConstants.RC_SOTRAGE_PERM;

/**
 * Created by Shrikant on 10-09-2017.
 */

public class RuntimePermissions extends Activity{

    public static  boolean hasCameraPermission(Context mContext) {
        return EasyPermissions.hasPermissions(mContext, Manifest.permission.CAMERA);
    }

    public static  boolean hasLocationPermission(Context mContext) {
        return EasyPermissions.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    public static  boolean hasAllPermission(Context mContext) {
        return EasyPermissions.hasPermissions(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR,
                Manifest.permission.CAMERA,
//				Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_CONTACTS,
                Manifest.permission.GET_ACCOUNTS,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.WRITE_CONTACTS,
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE);
    }
    public static  boolean hasStoragePermission(Context mContext) {
        return EasyPermissions.hasPermissions(mContext,new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE});
    }

    public static  boolean hasContactPermission(Context mContext) {
        return EasyPermissions.hasPermissions(mContext, new String[] {Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS,Manifest.permission.GET_ACCOUNTS});
    }

    public static  boolean hasCalenderPermission(Context mContext) {
        return EasyPermissions.hasPermissions(mContext,new String[] {Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR});
    }

    @AfterPermissionGranted(RC_SOTRAGE_PERM)
    public void storagePermissionTask(Context mContext, PermissionInterface permissionInterface) {

        if (!RuntimePermissions.hasStoragePermission(mContext)) {
            EasyPermissions.requestPermissions(
                    this,
                    mContext.getString(R.string.permission_msg),
                    RC_SOTRAGE_PERM,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE
            );
        }else{
            permissionInterface.onPermissionGranted();
        }
    }
}
