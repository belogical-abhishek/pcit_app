/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pcits.events.wizard.model;

import android.support.v4.app.Fragment;

import com.pcits.common.utils.Logging;
import com.pcits.events.fragments.AddEventPage4Fragment;
import com.pcits.events.fragments.AddPrivateEventPage4Fragment;

import java.util.ArrayList;

/**
 * A page asking for a name and an email.
 */
public class CustomAddPrivateEventPage4 extends Page implements ICustomPage {
    public static final String DEAL_URL_DATA_KEY = "dealUrl";
    public static final String TNC_DATA_KEY = "tnc";
    public static final String DESCRIPTION_DATA_KEY = "description";

    private AddPrivateEventPage4Fragment mFragment;

    public CustomAddPrivateEventPage4(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        mFragment = AddPrivateEventPage4Fragment.create(getKey());
        return mFragment;
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        // dest.add(new ReviewItem("Deal Url",
        // mData.getString(DEAL_URL_DATA_KEY),
        // getKey(), -1));
        // dest.add(new ReviewItem("Terms & Conditions", mData
        // .getString(TNC_DATA_KEY), getKey(), -1));
        // dest.add(new ReviewItem("Description", mData
        // .getString(DESCRIPTION_DATA_KEY), getKey(), -1));
    }

    @Override
    public boolean isCompleted() {
        // boolean completed = false;
        // if (TextUtils.isEmpty(mData.getString(TNC_DATA_KEY))
        // || TextUtils.isEmpty(mData.getString(DESCRIPTION_DATA_KEY))) {
        // completed = false;
        // } else {
        // completed = true;
        // }
        // return completed;
        return true;
    }

    @Override
    public void saveState() {
        // TODO Auto-generated method stub
        try {
            mFragment.saveState();
        } catch (Exception ex) {
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
    }

    @Override
    public void restoreState() {
        // TODO Auto-generated method stub
        try {
            mFragment.restoreState();
        } catch (Exception ex) {
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
    }
}
