package com.pcits.events.network;

import com.pcits.events.modelmanager.AcceptDeal;
import com.pcits.events.modelmanager.AddToGroup;
import com.pcits.events.modelmanager.ListBillObj;
import com.pcits.events.modelmanager.NewBill;
import com.pcits.events.modelmanager.SaveSplitRequest;
import com.pcits.events.obj.AddUsersobj;
import com.pcits.events.obj.CreateEventObj;
import com.pcits.events.obj.CreateEventResponseObj;
import com.pcits.events.obj.ListEventObj;
import com.pcits.events.obj.ListTnCObj;
import com.pcits.events.obj.ListUsersObj;
import com.pcits.events.obj.TnCobj;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by SAKSHI on 9/27/2017.
 */

public interface EventsAPI {

    @POST("events/create")
    Call<CreateEventResponseObj> createEvent(@Body CreateEventObj createEvent);

   // @GET("events/private")
    @GET()
    Call<List<ListEventObj>> getEvents(@Url String id);

    @POST()
    Call<ResponseBody> acceptInvite (@Url String url,@Body AcceptDeal acceptDeal);

    @POST()
    Call<ResponseBody> addToGroup(@Url String url,@Body AddToGroup addToGroup);

    @GET()
    Call<List<ListEventObj>> getEventById(@Url String dealId);

    @POST("events/add-users/{deal_id}")
    Call<String> addUser(@Body AddUsersobj addUsersobj,@Path("deal_id")String deal_id);

    @GET("events/users/{deal_id}")
    Call<List<ListUsersObj>> getUsers(@Path("deal_id") String deal_id);

    @GET("events/bills/{deal_id}")
    Call<List<ListBillObj>> getBillForDeal(@Path("deal_id") String deal_id);

    @POST()
    Call<Boolean> saveBillSplit(@Url String url, @Body SaveSplitRequest s);

    @POST
    Call<Integer>AddBill(@Url String url, @Body NewBill s);

    @GET()
    Call<ListTnCObj> getTnCList(@Url String url);



}
