/**
 * File        : ActivitySplash.java
 * App name    : M1SO Events
 * Version     : 1.1.4
 * Created     : 25/05/14
 * <p>
 * Created by Team M1SO on 21/01/14.
 * Copyright (c) 2014 M1SO. All rights reserved.
 */

package com.pcits.events;

import android.*;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Patterns;

//import com.crashlytics.android.Crashlytics;
//import com.crashlytics.android.Crashlytics;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.gson.Gson;
import com.pcits.common.utils.RuntimePermissions;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.config.MySharedPreferences;
import com.pcits.events.config.SharedPref;
import com.pcits.events.fragments.FragmentHome;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.obj.ClientInfo;
import com.pcits.events.obj.UserObj;
import com.pcits.events.utils.StringUtility;
import com.pcits.events.widgets.kenburnsview.KenBurnsView;
import com.splunk.mint.Mint;

import java.util.List;
import java.util.regex.Pattern;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.pcits.events.utils.AppConstants.RC_LOCATION_PERM;

public class ActivitySplash extends Activity implements EasyPermissions.PermissionCallbacks {

    private KenBurnsView mSplashPicture;
    private Handler handler;
    Intent splashIntent;
    boolean isFromDeepLink = false;

    /**
     * Called when the activity is first created.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mint.initAndStartSession(ActivitySplash.this, "45da184c");
        setContentView(R.layout.activity_splash);
        initUI();


        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();
        Log.d("deeplink", "onCreate: -----");
        Log.d("deeplink", "onCreate:action "+action);
        Log.d("deeplink", "onCreate:data "+data);

        getDeal(data);

       // initDeepLink();



        if (NetworkUtility.getInstance(this).isNetworkAvailable()) {
            GlobalValue.gcm_id = new GcmManager(this).getRegid();
            GlobalValue.android_id = Secure.getString(
                    this.getContentResolver(), Secure.ANDROID_ID);
            Log.d("Android", "Android ID : " + GlobalValue.android_id);

            // Register device
            if (!StringUtility.isEmpty(GlobalValue.gcm_id)) {
                ModelManager.registerDevice(this, GlobalValue.android_id,
                        GlobalValue.gcm_id);
            }
        }

        if (GlobalValue.mySpf == null) {
            GlobalValue.prefs = new SharedPref(this);
        }

        Gson gson = new Gson();
        MySharedPreferences sharedPreferences = MySharedPreferences.getInstance(this);
        String clientObj = sharedPreferences.getStringValue(MySharedPreferences.CLIENT_USER_KEY);

        if (clientObj != null && !clientObj.isEmpty()) {
            GlobalValue.myClient = gson.fromJson(clientObj, ClientInfo.class);
        }

        String adminObj = sharedPreferences.getStringValue(MySharedPreferences.ADMIN_USER_KEY);

        if (adminObj != null && !adminObj.isEmpty()) {
            GlobalValue.myUser = gson.fromJson(adminObj, UserObj.class);
        }

        this.handler = new Handler();
        permissionTask();
        // ATTENTION: This was auto-generated to handle app links.

    }

    private void getDeal(Uri deepLink) {
        if(deepLink!=null) {
            String[] parts = deepLink.toString().split("/");
            splashIntent = new Intent(ActivitySplash.this, ActivityPrivateEventDetails.class);
            splashIntent.putExtra("dealid", parts[parts.length-1]);
            splashIntent.putExtra("from", "deep_link");
            boolean gotId = false;
            if(GlobalValue.myClient!=null) {
                splashIntent.putExtra("email", "" + GlobalValue.myClient.getEmail());
                splashIntent.putExtra("name", "" + GlobalValue.myClient.getUsername());
            }
            else{
                //navigate to registration page
                splashIntent = new Intent(ActivitySplash.this, ActivitySignUp.class);
                splashIntent.putExtra("dealid", parts[1]);
                splashIntent.putExtra("from", "deep_link");
                splashIntent.putExtra("email", "");
                splashIntent.putExtra("name", "" );
            }


            isFromDeepLink = true;
        }

    }

    private void initDeepLink() {

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                        }

                        Log.d("Splash", "onSuccess: "+deepLink);
                        if(deepLink!=null) {
                            String[] parts = deepLink.toString().split("-");
                            splashIntent = new Intent(ActivitySplash.this, ActivityPrivateEventDetails.class);
                            splashIntent.putExtra("dealid", parts[1]);
                            splashIntent.putExtra("from", "deep_link");
                            boolean gotId = false;
                            if(GlobalValue.myClient!=null) {
                                splashIntent.putExtra("email", "" + GlobalValue.myClient.getEmail());
                                splashIntent.putExtra("name", "" + GlobalValue.myClient.getUsername());
                            }
                            else{
                                //navigate to registration page
                                splashIntent = new Intent(ActivitySplash.this, ActivitySignUp.class);
                                splashIntent.putExtra("dealid", parts[1]);
                                splashIntent.putExtra("from", "deep_link");
                                splashIntent.putExtra("email", "");
                                splashIntent.putExtra("name", "" );
                            }


                            isFromDeepLink = true;
                        }

                        // Handle the deep link. For example, open the linked
                        // content, or apply promotional credit to the user's
                        // account.
                        // ...

                        // ...
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("Splash", "getDynamicLink:onFailure", e);
                    }
                });
    }

    @AfterPermissionGranted(RC_LOCATION_PERM)
    private void permissionTask() {

        if (!RuntimePermissions.hasAllPermission(this)) {
            EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.permission_msg),
                    RC_LOCATION_PERM,
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.WRITE_CALENDAR, android.Manifest.permission.READ_CALENDAR,
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.GET_ACCOUNTS,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.READ_PHONE_STATE);
        } else {
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if(!isFromDeepLink)
                     splashIntent = new Intent(ActivitySplash.this, ActivityHome.class);
                    isFromDeepLink = false;
                    startActivity(splashIntent);
                    // overridePendingTransition(R.anim.slide_in_left,
                    // R.anim.slide_out_left);
                    finish();
                }

            }, 1500);

        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        mSplashPicture.resume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        mSplashPicture.pause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    private void initUI() {
        try {
            mSplashPicture = (KenBurnsView) findViewById(R.id.imgSplash);
        } catch (NullPointerException ex) {
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }
}
