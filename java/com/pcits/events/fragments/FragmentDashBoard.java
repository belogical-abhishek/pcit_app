package com.pcits.events.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityHome;
import com.pcits.events.ListOfTicketActivity;
import com.pcits.events.R;
import com.pcits.events.adapters.TicketsAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.widgets.KenBurnsView;

public class FragmentDashBoard extends Fragment {

	private View v;
	private Button btnTicket;
	private ListView lsvDashBoard;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;
	private TicketsAdapter adapterTicket;

	private Button mBtnClient;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		v = inflater.inflate(R.layout.fragment_dashboard, container, false);
		try{
			initUI();
			initControl();
			initNotBoringActionBar();
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return v;
	}

	private void initNotBoringActionBar() {
		mHeaderPicture = (KenBurnsView) v.findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void initUI() {
		btnTicket = (Button) v.findViewById(R.id.btnClietAttending);
		lsvDashBoard = (ListView) v.findViewById(R.id.lsvPaidEvent);
		llMenu = (LinearLayout) v.findViewById(R.id.llMenu);
		mBtnClient = (Button) v.findViewById(R.id.btnListOfClient);
	}

	private void initControl() {
		try{
			btnTicket.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					lsvDashBoard.setVisibility(View.VISIBLE);
					btnTicket.setVisibility(View.GONE);
					getData();
				}
			});
	
			llMenu.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
					Intent intent=new Intent(getActivity(),ActivityHome.class);
					startActivity(intent);
					getActivity().finish();
				}
			});
	
			mBtnClient.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					getActivity().startActivity(
							new Intent(getActivity(), ListOfTicketActivity.class));
				}
			});
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	private void getData() {
		try{
			if (GlobalValue.myUser != null) {
				int type = 0;
	
				ModelManager.getListTickets(getActivity(), Integer.toString(type),
						GlobalValue.myUser.getUsername(), true,
						new ModelManagerListener() {
	
							@Override
							public void onSuccess(Object object) {
								// TODO Auto-generated method stub
								String json = (String) object;
								GlobalValue.arrTickets = ParserUtility
										.getListTickets(json);
								if (GlobalValue.arrTickets.size() > 0) {
									adapterTicket = new TicketsAdapter(
											getActivity(), GlobalValue.arrTickets);
									adapterTicket.notifyDataSetChanged();
									lsvDashBoard.setAdapter(adapterTicket);
								} else {
									Toast.makeText(getActivity(), "No Data",
											Toast.LENGTH_SHORT).show();
								}
							}
	
							@Override
							public void onError() {
								// TODO Auto-generated method stub
	
							}
						});
			}
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}
}
