package com.pcits.events;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.pcits.common.utils.GsonUtility;
import com.pcits.events.adapters.ListEventsAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.network.EventsAPI;
import com.pcits.events.network.RestClient;
import com.pcits.events.obj.ListEventObj;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivityListEvents extends Activity {
    private RecyclerView mRecyclerView;
    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_events);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_events);
        mProgressDialog = new ProgressDialog(this);
        Log.d("ActivityListEvents", "private activity: "+GlobalValue.myUser.getUsername());
        Log.d("ActivityListEvents", "private activity: "+GsonUtility.convertObjectToJSONString(GlobalValue.myUser));
        Log.d("ActivityListEvents", "getEvents: "+ GsonUtility.convertObjectToJSONString(GlobalValue.myClient));

        getEvents();
    }

    public void getEvents() {
        Log.d("ActivityListEvents", "getEvents: "+GlobalValue.myClient.getUsername());
        Log.d("ActivityListEvents", "getEvents: "+ GsonUtility.convertObjectToJSONString(GlobalValue.myUser));
        Log.d("ActivityListEvents", "getEvents: "+ GsonUtility.convertObjectToJSONString(GlobalValue.myClient));
        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsApiObj = retrofit.create(EventsAPI.class);
        mProgressDialog.setMessage("Getting Events");
        mProgressDialog.show();

        try {
            Call<List<ListEventObj>> listeventCallobj = eventsApiObj.getEvents("events/"+Integer.valueOf(GlobalValue.myClient.getId()));
            listeventCallobj.enqueue(new Callback<List<ListEventObj>>() {
                @Override
                public void onResponse(Call<List<ListEventObj>> call, Response<List<ListEventObj>> response) {
                    mProgressDialog.dismiss();
                    if (response.isSuccessful()) {
                        List<ListEventObj> listevents = response.body();
                        List<ListEventObj> listeventsnew = new ArrayList<>();
                        for(int i=0;i<listevents.size();i++)
                        {
                            ListEventObj leo=listevents.get(i);
                            if(!leo.getStatus().equalsIgnoreCase("-1"))
                            {
                                listeventsnew.add(leo);
                            }
                        }
                        listevents.clear();
                        listevents=listeventsnew;
                        List<String> viewStatus=getStringArrayPref(ActivityListEvents.this,"Event_List_Status",listevents);
                        boolean fromHome=true;
                        ListEventsAdapter eventsadapterobj = new ListEventsAdapter(fromHome,viewStatus,listevents, ActivityListEvents.this,(Activity) ActivityListEvents.this,GlobalValue.myClient.getUsername());
                        eventsadapterobj.notifyDataSetChanged();
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ActivityListEvents.this);
                        mRecyclerView.setLayoutManager(layoutManager);
                        mRecyclerView.setHasFixedSize(true);
                        mRecyclerView.setAdapter(eventsadapterobj);
                        if (listevents==null || listevents.isEmpty()){
                            Toast.makeText(ActivityListEvents.this,"No private events found",Toast.LENGTH_LONG).show();
                        }

                    }
                }

                @Override
                public void onFailure(Call<List<ListEventObj>> call, Throwable throwable)
                {
                        throwable.getStackTrace();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<String> getStringArrayPref(Context context, String key, List<ListEventObj> list)
    {
        List<String> listEventStatus=new ArrayList<>(list.size());
        for (int i = 0; i < list.size(); i++)
        {
            listEventStatus.add("0");
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = prefs.getString(key, null);
        List<ListEventObj> listEvents=new ArrayList<>();
        if (json != null)
        {
            try
            {
                JSONArray a = null;
                try
                {
                    a = new JSONArray(json);
                    for (int i = 0; i < list.size(); i++)
                    {
                        ListEventObj ss=list.get(i);
                        for(int j=0;j<a.length();j++)
                        {
                            JSONObject jsonObject=a.getJSONObject(j);
                            if(ss.getDealId().equalsIgnoreCase(jsonObject.getString("event_id")))
                            {
                                listEventStatus.set(i,ss.getDealId());
                            }
                            else
                            {
                                if(listEventStatus.get(i).equalsIgnoreCase("0"))
                                {
                                    listEventStatus.set(i,"0");
                                }
                            }

                        }
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return listEventStatus;
    }
}
