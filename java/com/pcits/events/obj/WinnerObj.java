package com.pcits.events.obj;

public class WinnerObj {

	private String uniqueCode, fullName, gender, age, address, avatar, gcmId,
			title, company, status, value;

	public WinnerObj(String uniqueCode, String fullName, String gender,
			String age, String address, String avatar, String gcm,
			String title, String company, String status, String value) {
		this.uniqueCode = uniqueCode;
		this.fullName = fullName;
		this.gender = gender;
		this.age = age;
		this.address = address;
		this.avatar = avatar;
		this.gcmId = gcm;
		this.title = title;
		this.company = company;
		this.status = status;
		this.value = value;
	}

	public String getGcmId() {
		return gcmId;
	}

	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getUniqueCode() {
		return uniqueCode;
	}

	public void setUniqueCode(String uniqueCode) {
		this.uniqueCode = uniqueCode;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
}
