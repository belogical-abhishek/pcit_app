package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.pcits.events.R;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.obj.AdminObj;
import com.pcits.events.widgets.CircleImageWithBorder;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class AdminAdapter extends BaseAdapter {

	private Activity mAct;
	private LayoutInflater mInflater;
	private ArrayList<AdminObj> mArrAdmin;
	private AQuery mAq;

	public AdminAdapter(Activity act, ArrayList<AdminObj> arr) {
		this.mAct = act;
		this.mArrAdmin = arr;
		mInflater = (LayoutInflater) act
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mAq = new AQuery(mAct);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mArrAdmin.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mArrAdmin.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder = null;
		if (convertView == null) {
			holder = new Holder();
			convertView = mInflater.inflate(R.layout.item_admin, null);

			holder.imgActive = (ImageView) convertView
					.findViewById(R.id.img_active);
			holder.imgAvatar = (CircleImageWithBorder) convertView
					.findViewById(R.id.img_avatar);
			holder.lblUserName = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lbl_username);
			holder.lblFullName = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lbl_fullname);
			holder.lblPhone = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lbl_phone);
			holder.lblEmail = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lbl_email);
			holder.lblCountry = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lbl_country);

			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		AdminObj adm = mArrAdmin.get(position);
		if (adm != null) {
			if (adm.isActive()) {
				holder.imgActive.setImageResource(R.drawable.ic_active);
			} else {
				holder.imgActive.setImageResource(R.drawable.ic_inactive);
			}

			if (!adm.getAvatar().equals("")) {
				AQuery aq = mAq.recycle(convertView);
				aq.id(holder.imgAvatar).image(
						UserFunctions.URLAdmin + adm.getAvatar(), false, true);
			} else {
				holder.imgAvatar.setImageResource(R.drawable.avatar2);
			}

			holder.lblUserName.setText(adm.getUserName());
			holder.lblFullName.setText(adm.getFullName());
			holder.lblPhone.setText(adm.getPhone());
			holder.lblEmail.setText(adm.getEmail());
			holder.lblCountry.setText(adm.getCountry());
		}

		return convertView;
	}

	class Holder {
		ImageView imgActive;
		CircleImageWithBorder imgAvatar;
		TextViewRobotoCondensedRegular lblUserName, lblFullName, lblPhone,
				lblEmail, lblCountry;
	}
}
