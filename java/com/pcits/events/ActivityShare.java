/**
 * File        : ActivityShare.java
 * App name    : M1SO Events
 * Version     : 1.1.4
 * Created     : 25/05/14

 * Created by Team M1SO on 21/01/14.
 * Copyright (c) 2014 M1SO. All rights reserved.
 */

package com.pcits.events;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.pcits.common.utils.Logging;
import com.pcits.events.config.ads.Ads;
import com.pcits.events.facebook.AsyncFacebookRunner;
import com.pcits.events.facebook.BaseRequestListener;
import com.pcits.events.facebook.DialogError;
import com.pcits.events.facebook.Facebook;
import com.pcits.events.facebook.Facebook.DialogListener;
import com.pcits.events.facebook.FacebookError;
import com.pcits.events.facebook.SessionStore;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.twitter.TwitterApp;
import com.pcits.events.twitter.TwitterApp.TwDialogListener;
import com.pcits.events.utils.Utils;

public class ActivityShare extends FragmentActivity implements
		OnClickListener {

	// Create an instance of ActionBar
	private android.app.ActionBar actionbar;

	// Declare object of AdView class
	private AdView adView;

	// Declare objects for Facebook and TwitterApp class
	private Facebook mFacebook;
	private TwitterApp mTwitter;

	private boolean postToTwitter = false;
	private ProgressDialog mProgress;
	private Handler mRunOnUi = new Handler();
	private static final String[] PERMISSIONS = new String[] {
			"publish_stream", "read_stream", "offline_access" };

	// Create object of views
	private EditText txtWhatDoYouThink;
	private CheckBox chkFacebook, chkTwitter;
	private Button btnOtherApps, btnShare;

	// Declare variables to store data
	private String mDealId, mTitle, mDesc, mImgDeal;
	private String mAppName;
	private String URLLocationPage;

	private boolean mConnection;
	// Declare object of utils and userFunctions class
	private Utils utils;
	private UserFunctions userFunction;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_share);

		// Declare object of Utils and UserFunctions class
		utils = new Utils(this);
		userFunction = new UserFunctions();

		// Change actionbar title
		int titleId = Resources.getSystem().getIdentifier("action_bar_title",
				"id", "android");
//		if (0 == titleId)
//			titleId = com.actionbarsherlock.R.id.abs__action_bar_title;

		// Change the title color to white
		TextView txtActionbarTitle = (TextView) findViewById(titleId);
		txtActionbarTitle.setTextColor(getResources().getColor(
				R.color.actionbar_title_color));

		// Get ActionBar and set back button on actionbar
		actionbar = getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);

		// Connect view objects and xml ids
		txtWhatDoYouThink = (EditText) findViewById(R.id.txtWhatDoYouThink);
		chkFacebook = (CheckBox) findViewById(R.id.chkFacebook);
		chkTwitter = (CheckBox) findViewById(R.id.chkTwitter);
		btnOtherApps = (Button) findViewById(R.id.btnOtherApps);
		btnShare = (Button) findViewById(R.id.btnShare);
		adView = (AdView) findViewById(R.id.adView);

		// load ads
		Ads.loadAds(adView);

		chkFacebook.setOnClickListener(this);
		chkTwitter.setOnClickListener(this);
		btnOtherApps.setOnClickListener(this);
		btnShare.setOnClickListener(this);

		// Get intent Data from ActivityDetailPlace
		Intent i = getIntent();
		mDealId = i.getStringExtra(utils.EXTRA_DEAL_ID);
		mTitle = i.getStringExtra(utils.EXTRA_DEAL_TITLE);
		mDesc = i.getStringExtra(utils.EXTRA_DEAL_DESC);
		mImgDeal = i.getStringExtra(utils.EXTRA_DEAL_IMG);

		URLLocationPage = UserFunctions.URLAdmin
				+ userFunction.service_view_deal + userFunction.key_deals_id
				+ "=" + mDealId;
		mAppName = getString(R.string.app_name);

		// Set objects of Facebook and TwitterAp class
		mProgress = new ProgressDialog(this);
		mFacebook = new Facebook(getString(R.string.facebook_app_id));
		mTwitter = new TwitterApp(this,
				getString(R.string.twitter_consumer_key),
				getString(R.string.twitter_secret_key));

		// Restore facebook session
		SessionStore.restore(mFacebook, this);

		// Checking facebook session
		if (mFacebook.isSessionValid()) {
			chkFacebook.setChecked(true);
			String name = SessionStore.getName(this);
		}

		// Checking twitter token
		mTwitter.setListener(mTwLoginDialogListener);

		if (mTwitter.hasAccessToken()) {
			chkTwitter.setChecked(true);

		}

	}

	// Check the value of review, if more than 110 show toast message
	private void postReview(String review) {
		Toast.makeText(this, getString(R.string.post_review),
				Toast.LENGTH_SHORT).show();

		if (review.length() > 110) {
			Toast.makeText(this, getString(R.string.post_limit),
					Toast.LENGTH_SHORT).show();
		} else {
			postToTwitter = true;

		}
	}

	// Method to check if twitter token not available, authorize user
	private void onTwitterClick() {
		if (!mTwitter.hasAccessToken()) {
			chkTwitter.setChecked(false);
			mTwitter.authorize();
		}

	}

	// Method to post status to twitter
	private void postToTwitter(final String review) {
		new Thread() {
			@Override
			public void run() {
				int what = 0;

				try {
					mTwitter.updateStatus(review + URLLocationPage);

				} catch (Exception e) {
					what = 1;
				}

				mHandlerTwitter
						.sendMessage(mHandlerTwitter.obtainMessage(what));
			}
		}.start();
	}

	// Event handler for reading twitter posting result
	private Handler mHandlerTwitter = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			String text = (msg.what == 0) ? getString(R.string.post_to_twitter)
					: getString(R.string.post_to_twitter_failed);

			Toast.makeText(ActivityShare.this, text, Toast.LENGTH_SHORT).show();

			if (!(chkFacebook.isChecked() && chkTwitter.isChecked())) {
				finish();
			}
		}
	};

	// Event listener to read twitter username from twitter dialog
	private final TwDialogListener mTwLoginDialogListener = new TwDialogListener() {

		public void onComplete(String value) {
			String username = mTwitter.getUsername();
			username = (username.equals("")) ? getString(R.string.no_name)
					: username;

			chkTwitter.setChecked(true);

			Toast.makeText(ActivityShare.this,
					getString(R.string.connect_twitter) + " " + username,
					Toast.LENGTH_LONG).show();
		}

		public void onError(String value) {
			chkTwitter.setChecked(false);

			Toast.makeText(ActivityShare.this,
					getString(R.string.connect_twitter_failed),
					Toast.LENGTH_LONG).show();
		}
	};

	// Method to post status to facebook
	private void postToFacebook(String review) {
		try {
			mProgress.setMessage("Posting ...");
			mProgress.show();
			mProgress.setIndeterminate(true);
			mProgress.setCancelable(false);
			AsyncFacebookRunner mAsyncFbRunner = new AsyncFacebookRunner(
					mFacebook);

			Bundle params = new Bundle();

			params.putString("message", review);
			params.putString("name", mTitle);
			params.putString("caption", mAppName);
			params.putString("link", URLLocationPage);
			params.putString("description", mDesc);
			params.putString("picture", userFunction.URLAdmin + mImgDeal);

			mAsyncFbRunner.request("me/feed", params, "POST",
					new WallPostListener());
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	// Event listener to read facebook posting result
	private final class WallPostListener extends BaseRequestListener {
		public void onComplete(final String response) {
			mRunOnUi.post(new Runnable() {

				public void run() {
					mProgress.cancel();

					Toast.makeText(ActivityShare.this,
							getString(R.string.post_to_facebook),
							Toast.LENGTH_SHORT).show();
					finish();
				}
			});
		}

		public void onComplete(String response, Object state) {
			// TODO Auto-generated method stub

		}

		public void onIOException(IOException e, Object state) {
			// TODO Auto-generated method stub

		}

		public void onFileNotFoundException(FileNotFoundException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		public void onMalformedURLException(MalformedURLException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		public void onFacebookError(FacebookError e, Object state) {
			// TODO Auto-generated method stub

		}
	}

	// Method to check facebook session, if not valid, authorize user
	private void onFacebookClick() {
		try {
			if (!mFacebook.isSessionValid()) {
				chkFacebook.setChecked(false);
				mFacebook.authorize(this, PERMISSIONS, -1,
						new FbLoginDialogListener());
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	// Event listener to get facebook name from facebook dialog
	private final class FbLoginDialogListener implements DialogListener {
		public void onComplete(Bundle values) {
			try {
				SessionStore.save(mFacebook, ActivityShare.this);

				chkFacebook.setChecked(true);

				getFbName();
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			}
		}

		public void onFacebookError(FacebookError error) {
			Toast.makeText(ActivityShare.this, "Facebook connection failed",
					Toast.LENGTH_SHORT).show();

			chkFacebook.setChecked(false);
		}

		public void onError(DialogError error) {
			Toast.makeText(ActivityShare.this, "Facebook connection failed",
					Toast.LENGTH_SHORT).show();

			chkFacebook.setChecked(false);
		}

		public void onCancel() {
			chkFacebook.setChecked(false);
		}
	}

	// Method to get facebook name
	private void getFbName() {
		mProgress.setMessage("Finalizing ...");
		mProgress.show();

		new Thread() {
			@Override
			public void run() {
				String name = "";
				int what = 1;

				try {
					String me = mFacebook.request("me");

					JSONObject jsonObj = (JSONObject) new JSONTokener(me)
							.nextValue();
					name = jsonObj.getString("name");
					what = 0;
				} catch (Exception ex) {
					throw new RuntimeException(ex);
				}

				mFbHandler.sendMessage(mFbHandler.obtainMessage(what, name));
			}
		}.start();
	}

	// Event handler to read facebook authentication
	private Handler mFbHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			mProgress.dismiss();

			if (msg.what == 0) {
				String username = (String) msg.obj;
				username = (username.equals("")) ? getString(R.string.no_name)
						: username;

				SessionStore.saveName(username, ActivityShare.this);

				Toast.makeText(ActivityShare.this,
						getString(R.string.connect_facebook) + " " + username,
						Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(ActivityShare.this,
						getString(R.string.connect_facebook_2),
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	// Listener for option menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// previous page or exit
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	// Listener on Click
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		try {
			switch (v.getId()) {
			case R.id.btnShare:
				mConnection = utils.isNetworkAvailable();
				if (mConnection) {
					// Get text from edittext and store to string variable
					String review = txtWhatDoYouThink.getText().toString();

					// Check to value of review
					if (review.equals("")) {
						review = mTitle + " at " + getString(R.string.app_name)
								+ " ";
					} else {
						review += " - " + mTitle + " at "
								+ getString(R.string.app_name) + " ";
					}

					// Check the checkbox and post to both facebook and twitter
					// or
					// one of them
					if (chkFacebook.isChecked() && chkTwitter.isChecked()) {
						postReview(review);
						if (postToTwitter) {
							postToFacebook(review);
							postToTwitter(review);
						}
					} else if (chkFacebook.isChecked()
							&& !chkTwitter.isChecked()) {
						postToFacebook(review);
					} else if (!chkFacebook.isChecked()
							&& chkTwitter.isChecked()) {
						postReview(review);
						if (postToTwitter)
							postToTwitter(review);
					} else {
						Toast.makeText(ActivityShare.this,
								getString(R.string.select_share),
								Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(getApplicationContext(),
							getString(R.string.no_connection),
							Toast.LENGTH_SHORT).show();
				}
				break;

			case R.id.btnOtherApps:
				// Share this app to other application
				Intent iShare = new Intent(Intent.ACTION_SEND);
				iShare.setType("text/plain");
				iShare.putExtra(Intent.EXTRA_SUBJECT, mTitle);
				iShare.putExtra(Intent.EXTRA_TEXT, "\nDetail: "
						+ URLLocationPage);
				startActivity(Intent.createChooser(iShare,
						getString(R.string.share_via)));
				break;

			case R.id.chkFacebook:
				// When checkbox facebook click
				onFacebookClick();
				break;

			case R.id.chkTwitter:
				// When checkbox twitter click
				onTwitterClick();
				break;
			default:
				break;
			}
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

}
