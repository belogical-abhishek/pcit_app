package com.pcits.events;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pcits.events.network.EventsAPI;
import com.pcits.events.network.RestClient;
import com.pcits.events.obj.CreateEventObj;
import com.pcits.events.obj.CreateEventResponseObj;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivityAddPrivateEvent extends Activity {

    private EditText medtTitle, medtAddress, medtCountry, medtDescription;
    private Button mbtnAddEvent;
    private float mlongitude, mlatitude;
    private ProgressDialog mProgressDialog;
    private LocationManager locationManager;
    private String TAG =  "ActivityAddPrivateEvent";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_private_event);

        Log.d(TAG, "onCreate: ");
        initUI();
        getLocation();

        mbtnAddEvent.setOnClickListener(onClickListener);


    }

    private void initUI() {
        medtTitle = (EditText) findViewById(R.id.eventTitleedt);
        medtAddress = (EditText) findViewById(R.id.eventAddressedt);
        medtCountry = (EditText) findViewById(R.id.eventCountryedt);
        medtDescription = (EditText) findViewById(R.id.eventDescrriptionedt);
        mbtnAddEvent = (Button) findViewById(R.id.btnAddEventPrivate);
        mProgressDialog=new ProgressDialog(this);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            switch (id) {
                case R.id.btnAddEventPrivate:
                    validateEventData();
                    break;
            }
        }
    };
    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    mlatitude=Double.valueOf(location.getLatitude()).floatValue();
                    mlongitude=Double.valueOf(location.getLongitude()).floatValue();

                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }
    private void validateEventData() {

        if (!validateTitle()) {
            return;
        }
        if (!validateAddress()) {
            return;
        }
        if (!validateCountry()) {
            return;
        }
        if (!validateDescription()) {
            return;
        } else {
            createEvent();
        }

    }

    private boolean validateTitle() {
        if (medtTitle.getText().toString().equals("")) {
            Toast.makeText(this, "Please Enter Title", Toast.LENGTH_SHORT).show();

            return false;
        } else {
            return true;
        }
    }

    private boolean validateAddress() {
        if (medtAddress.getText().toString().equals("")) {
            Toast.makeText(this, "Please Enter Address", Toast.LENGTH_SHORT).show();

            return false;
        } else {
            return true;
        }
    }

    private boolean validateCountry() {
        if (medtCountry.getText().toString().equals("")) {
            Toast.makeText(this, "Please Enter Country", Toast.LENGTH_SHORT).show();

            return false;
        } else {
            return true;
        }
    }

    private boolean validateDescription() {
        if (medtDescription.getText().toString().equals("")) {
            Toast.makeText(this, "Please Enter Desription", Toast.LENGTH_SHORT).show();

            return false;
        } else {
            return true;
        }
    }

    private void createEvent() {
        mProgressDialog.setMessage("Adding Event");
        mProgressDialog.show();

        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsAPI = retrofit.create(EventsAPI.class);

        CreateEventObj mCreateEvent = new CreateEventObj();
        mCreateEvent.setCategoryId("C0001");
        mCreateEvent.setTncId("T0001");
        mCreateEvent.setFeatured(1);
        mCreateEvent.setUsername("admin");
        mCreateEvent.setTitle(medtTitle.getText().toString());
        mCreateEvent.setCompany("Trent Park");
        mCreateEvent.setAddress(medtAddress.getText().toString());
        mCreateEvent.setCity("London");
        mCreateEvent.setCountry(medtCountry.getText().toString());
        mCreateEvent.setPostcode("EN4 0PS");
        mCreateEvent.setCounty("Enfield");
        mCreateEvent.setImage("upload/images/2015-07-11-5843.jpg");
        mCreateEvent.setLatitude(mlatitude);
        mCreateEvent.setLongitude(mlongitude);
        mCreateEvent.setStartTimestamp("2015-04-21 12:00");
        mCreateEvent.setEndTimestamp("2015-04-21 12:00");
        mCreateEvent.setStartTime("2015-04-21");
        mCreateEvent.setEndTime("2015-04-21");
        mCreateEvent.setMaxTicketPurchase(23);
        mCreateEvent.setDealUrl("http://www.cc-g.co.uk/event/ccg-go-ape-london-sun-");
        mCreateEvent.setDescription(medtDescription.getText().toString());
        mCreateEvent.setTncNotes("");
        mCreateEvent.setFeaturedStartDate("000");
        mCreateEvent.setFeaturedEndDate("000");

        Call<CreateEventResponseObj> mCreateEventResponseObjCall = eventsAPI.createEvent(mCreateEvent);
        mCreateEventResponseObjCall.enqueue(new Callback<CreateEventResponseObj>() {
            @Override
            public void onResponse(Call<CreateEventResponseObj> call, Response<CreateEventResponseObj> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()) {
                    try {
                        String eventId = response.body().getDealId();
                        Toast.makeText(ActivityAddPrivateEvent.this, "Event Added", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(ActivityAddPrivateEvent.this,ActivityListEvents.class);
                        startActivity(intent);
                        finish();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<CreateEventResponseObj> call, Throwable throwable) {

            }
        });


    }
}
