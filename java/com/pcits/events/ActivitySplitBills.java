package com.pcits.events;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.common.utils.GsonUtility;
import com.pcits.events.adapters.AdapterSplitBill;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.listeners.OnSplitActionLisener;
import com.pcits.events.modelmanager.ListBillObj;
import com.pcits.events.modelmanager.NewBill;
import com.pcits.events.modelmanager.SaveSplitRequest;
import com.pcits.events.modelmanager.SplitClients;
import com.pcits.events.network.EventsAPI;
import com.pcits.events.network.RestClient;
import com.pcits.events.obj.BillsObj;
import com.pcits.events.obj.ListUsersObj;
import com.pcits.events.obj.SplitShare;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class ActivitySplitBills extends AppCompatActivity implements OnSplitActionLisener{
    private ImageButton mbtnAddBills;
    private String mTitle, mAmount;
    private List<BillsObj> mBillsList;
    private ImageView imgReset;
    private EditText mEdittextAmount;
    private ListView mListViewBills;
    private List<ListUsersObj> listUsers;
    private LinearLayout topLayout;
    private TableRow tableLayout;
    private TextView btnSave,txtPaid,txtOwe;
    private boolean isInitLoad = true;

    private ArrayList<SplitShare> mSplitShareList;
    private Float TotalBill;
    private Toolbar mToolBar;
    private ProgressDialog mProgressDialog;
    private Context mContext;
    private String mDealId;
    private String mBillId="";
    private boolean isForRefresh = false;
    private int mFunctionId;
    String TAG  =ActivitySplitBills.class.getSimpleName();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_split_bills);
        mContext = ActivitySplitBills.this;

        Intent intent = getIntent();
        mFunctionId = intent.getIntExtra("functionid", 0);
        mDealId = intent.getStringExtra("dealid");

        initUI();
        getUsers();
    }

    private SaveSplitRequest getBillRequestData(){
        SaveSplitRequest request = new SaveSplitRequest();
        ArrayList<SplitClients> clientList = new ArrayList<>();
        Log.d(TAG, "getBillRequestData: "+GsonUtility.convertObjectToJSONString(mSplitShareList));
        for(SplitShare s:mSplitShareList){
            SplitClients client = new SplitClients();
            client.setEmail(""+s.getName());
            client.setAmount(""+s.getAmount());
            clientList.add(client);
        }
        Log.d(TAG, "getBillRequestData: "+clientList);

        request.setSplitlist(clientList);
        return request;

    }

    private void addBillToDeal() {
        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsAPI = retrofit.create(EventsAPI.class);
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage("Saving bill ..");
        mProgressDialog.show();

        NewBill newBill = new NewBill();
        newBill.setAmount(Float.valueOf(mEdittextAmount.getText().toString()));
        newBill.setPaidBy(GlobalValue.myClient.getEmail());
        newBill.setClientId(Integer.valueOf(GlobalValue.myClient.getId()));

        Call<Integer> addbill = eventsAPI.AddBill("http://54.173.65.38/events-api/public/bills/create/"+mDealId,newBill);
        addbill.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if(response.isSuccessful()){
                    mProgressDialog.dismiss();
                    saveSplitedBill(response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });

    }


    private void saveSplitedBill(String billId){
        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsAPI = retrofit.create(EventsAPI.class);
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage("Saving share..");
        mProgressDialog.show();
        SaveSplitRequest saveRequest = getBillRequestData();
        Call<Boolean> saveSplit = eventsAPI.saveBillSplit("http://54.173.65.38/events-api/public/bills/split/"+billId,saveRequest);
        saveSplit.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                Log.d(TAG, "onResponse: "+response.toString());
                if(response.body().equals(true)){
                    Toast.makeText(mContext, "Bill Saved", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                    finish();
                }
                else {
                    Toast.makeText(mContext, "Something went wrong, could not save bill", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

                t.printStackTrace();
                Toast.makeText(mContext, "Bill Saved", Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }


    public void getUsers() {
        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsAPI = retrofit.create(EventsAPI.class);
        mProgressDialog.setMessage("Getting added users..");
        mProgressDialog.show();
        Call<List<ListUsersObj>> listCallobj = eventsAPI.getUsers(mDealId);
        listCallobj.enqueue(new Callback<List<ListUsersObj>>() {
            @Override
            public void onResponse(Call<List<ListUsersObj>> call, Response<List<ListUsersObj>> response) {

                if (response.isSuccessful()) {
                    Log.d(TAG, "onResponse: "+GsonUtility.convertObjectToJSONString(response.body()));
                    listUsers = response.body();
                    Log.d(TAG, "onResponse: list user sie "+listUsers.size());
                    Log.d(TAG, "onResponse: list user sie "+response.body().size());
                    List<String> listemails = new ArrayList<String>();


                    mProgressDialog.dismiss();
                    initAdapter();

                    for (int i = 0; i < listUsers.size(); i++) {
                        listemails.add(listUsers.get(i).getEmail());
                    }
                     if (listUsers.isEmpty()) {
                        Toast.makeText(mContext, "No Users Added or waiting for users to accept the invite", Toast.LENGTH_SHORT).show();
                    }

                    mProgressDialog.dismiss();
                }
            }



            @Override
            public void onFailure(Call<List<ListUsersObj>> call, Throwable t) {

                Toast.makeText(mContext,"Something went wrong",Toast.LENGTH_LONG).show();
                mProgressDialog.dismiss();
            }
        });


    }

    private void initAdapter() {


        if(mDealId.equals("D000000120")){
            ///events/bills/D000000120
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage("getting bill details..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            Log.d(TAG, "initAdapter: in deal");

            try {
                Retrofit retrofit = RestClient.retrofitService();
                EventsAPI eventsApiObj = retrofit.create(EventsAPI.class);
                Call<List<ListBillObj> > listBillObjCall = eventsApiObj.getBillForDeal(mDealId);
                listBillObjCall.enqueue(new Callback<List<ListBillObj>>() {
                    @Override
                    public void onResponse(Call<List<ListBillObj>> call, Response<List<ListBillObj>> response) {
                        Log.d(TAG, "onResponse: "+response.isSuccessful());
                        Log.d(TAG, "onResponse: "+ GsonUtility.convertObjectToJSONString(response.body()));
                        if(response.body().size()>0){
                            TotalBill = response.body().get(2).getAmount();
                            mEdittextAmount.setText(""+TotalBill);
                            Log.d(TAG, "onResponse: "+ GsonUtility.convertObjectToJSONString(response.body().get(2)));
                            Log.d(TAG, "onResponse: "+ GsonUtility.convertObjectToJSONString(response.body().get(2).getSplit()));
                            Log.d(TAG, "onResponse: "+response.body().get(2).getAmount());


                            mSplitShareList.clear();


                            SplitShare share = new SplitShare();
                            share.setAmount(response.body().get(2).getSplit().get(2).getAmount());
                            txtOwe.setText("0.0");
                            txtPaid.setText(""+response.body().get(2).getSplit().get(2).getAmount());
                            share.setName("Your");
                            share.setEmail("ravina.buran@envisiodevs.com");//set logged in users email
                            share.setShare(1.00);
                            share.setUpdate(false);

                            mSplitShareList.add(share);
                            int i =0 ;
                            for (ListUsersObj l : listUsers) {

                                share = new SplitShare();
                                share.setAmount(response.body().get(2).getSplit().get(i).getAmount());
                                share.setName(l.getFullname());
                                share.setEmail(l.getEmail());
                                share.setUser(l);
                                share.setShare(1.00);
                                share.setUpdate(false);
                                i++;

                                mSplitShareList.add(share);
                            }
                            setListAdapter();
                            mProgressDialog.dismiss();
                            if(isForRefresh){
                                Toast.makeText(mContext,"Original bill loaded successfully",Toast.LENGTH_LONG).show();
                                isForRefresh = false;
                            }
                        }else
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<List<ListBillObj>> call, Throwable t) {

                        t.printStackTrace();
                        mProgressDialog.dismiss();
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }


        }
else {


            try {
                Retrofit retrofit = RestClient.retrofitService();
                EventsAPI eventsApiObj = retrofit.create(EventsAPI.class);
                Call<List<ListBillObj> > listBillObjCall = eventsApiObj.getBillForDeal(mDealId);
                listBillObjCall.enqueue(new Callback<List<ListBillObj>>() {
                    @Override
                    public void onResponse(Call<List<ListBillObj>> call, Response<List<ListBillObj>> response) {
                        Log.d(TAG, "onResponse: "+response.isSuccessful());
                        Log.d(TAG, "onResponse: "+ GsonUtility.convertObjectToJSONString(response.body()));
                        if(response.body().size()>0){
                            TotalBill = response.body().get(0).getAmount();
                            mBillId = ""+response.body().get(0).getId();
                            mEdittextAmount.setText(""+TotalBill);
                            Log.d(TAG, "onResponse: "+ GsonUtility.convertObjectToJSONString(response.body().get(0)));
                            Log.d(TAG, "onResponse: "+ GsonUtility.convertObjectToJSONString(response.body().get(0).getSplit()));
                            Log.d(TAG, "onResponse: "+response.body().get(0).getAmount());
                            Log.d(TAG, "onResponse: "+""+(response.body().get(0).getAmount()-response.body().get(0).getSplit().get(0).getAmount()));
                            txtOwe.setText(""+(response.body().get(0).getAmount()-response.body().get(0).getSplit().get(0).getAmount()));
                            txtPaid.setText(""+response.body().get(0).getAmount());

                            mSplitShareList.clear();


                            SplitShare share = new SplitShare();
                           /* share.setAmount(response.body().get(2).getSplit().get(2).getAmount());
                            txtOwe.setText("0.0");
                            txtPaid.setText(""+response.body().get(2).getSplit().get(2).getAmount());
                            share.setName("Your");
                            share.setEmail("ravina.buran@envisiodevs.com");//set logged in users email
                            share.setShare(1.00);
                            share.setUpdate(false);

                            mSplitShareList.add(share);*/
                            int i =0 ;
                            for (ListUsersObj l : listUsers) {

                                share = new SplitShare();
                                share.setAmount(response.body().get(0).getSplit().get(i).getAmount());
                                share.setName(l.getEmail());//l.getFullname());
                                share.setEmail(l.getEmail());
                                share.setUser(l);
                                share.setShare(1.00);
                                share.setUpdate(false);
                                i++;

                                mSplitShareList.add(share);
                            }
                            setListAdapter();
                            mProgressDialog.dismiss();
                            if(isForRefresh){
                                Toast.makeText(mContext,"Original bill loaded successfully",Toast.LENGTH_LONG).show();
                                isForRefresh = false;
                            }
                        }else {
                            mProgressDialog.dismiss();
                            freshData();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<ListBillObj>> call, Throwable t) {

                        t.printStackTrace();
                        mProgressDialog.dismiss();
                        freshData();
                    }


                });
            }catch (Exception e){
                e.printStackTrace();
                if(mProgressDialog.isShowing())
                mProgressDialog.dismiss();

            }





        }


    }
    private void freshData() {
        mSplitShareList.clear();

        SplitShare share = new SplitShare();

        Log.d(TAG, "initAdapter: "+listUsers.size());

        for (ListUsersObj l : listUsers) {
            share = new SplitShare();
            share.setAmount(0.0);
            share.setName(l.getEmail());
            share.setUser(l);
            share.setShare(1.00);
            share.setUpdate(false);

            mSplitShareList.add(share);

        }
        Log.d(TAG, "initAdapter: "+mSplitShareList.size());

        setListAdapter();
    }
    private void setListAdapter() {
       // topLayout.setVisibility(View.VISIBLE);
        Log.d(TAG, "setListAdapter: "+mSplitShareList.size());
        AdapterSplitBill adapter = new AdapterSplitBill(mContext,R.layout.adapter_split_bills,mSplitShareList,this,mFunctionId);
        mListViewBills.setAdapter(adapter);

    }

    private void initUI() {

        btnSave = (TextView)findViewById(R.id.btnSave);
        txtPaid = (TextView)findViewById(R.id.txt_amount_get);
        txtOwe = (TextView)findViewById(R.id.txt_amount);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  getBillRequestData();
                if(mBillId.isEmpty())
                addBillToDeal();
                else
                    saveSplitedBill(mBillId);
            }
        });
        topLayout= (LinearLayout)findViewById(R.id.ll_transaction);
        mProgressDialog=new ProgressDialog(mContext);
        mBillsList = new ArrayList<BillsObj>();
        mSplitShareList =new ArrayList<>();
        mListViewBills = (ListView) findViewById(R.id.listBills);
        imgReset = (ImageView)findViewById(R.id.refresh);


        mbtnAddBills = (ImageButton) findViewById(R.id.btn_addBills);

        mbtnAddBills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivitySplitBills.this, ActivityAddBills.class);
                startActivity(intent);
                finish();
            }
        });


        mEdittextAmount = (EditText)findViewById(R.id.editText_bill_amount);
        mEdittextAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!mEdittextAmount.getText().toString().isEmpty()) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            TotalBill = Float.parseFloat( mEdittextAmount.getText().toString());
                            if(isInitLoad) {
                                isInitLoad = false;
                            }
                            else  updateInitialShares();

                        }
                    }, 2000);

                }



            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mToolBar = (Toolbar) findViewById(R.id.toolbar_SplitBills);
        imgReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isForRefresh = true;

                mEdittextAmount.setText("0.0");
                initAdapter();

            }
        });
        mToolBar.setTitle("");
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrow_left);


        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.black));
        }
    }



    private void updateInitialShares() {
        mSplitShareList.clear();

        float sharedAmount  = Float.valueOf(String.format("%.2f",TotalBill/(listUsers.size())));
        Log.d(TAG, "updateInitialShares: shared amount: "+sharedAmount);


        float netshare = sharedAmount*(listUsers.size());


        float adjustment = TotalBill-(netshare);
        Log.d(TAG, "updateInitialShares: multi "+sharedAmount*(listUsers.size()));
        Log.d(TAG, "updateInitialShares: adjustment "+adjustment);

        SplitShare share  = new SplitShare();
       /* share.setAmount(sharedAmount+adjustment);
        share.setName("Your");
        share.setShare(1.00);
        share.setUpdate(false);

        mSplitShareList.add(share);
*/
        for(ListUsersObj l :listUsers){
            share  = new SplitShare();
            share.setAmount(sharedAmount);
            share.setName(l.getEmail());
            share.setUser(l);
            share.setShare(1.00);
            share.setUpdate(false);

            mSplitShareList.add(share);

        }

        setListAdapter();

    }



    @Override
    public void OnSharePlus(int position) {

    }

    @Override
    public void OnShareMinus(int position) {

    }

    @Override
    public void OnEdited(int position,String updatedValue) {

        if((Float.valueOf(updatedValue))<TotalBill) {
            Double adjustment = mSplitShareList.get(position).getAmount() - Double.parseDouble(updatedValue);

            if (getUnupdatedCount() > 1) {
                mSplitShareList.get(position).setUpdate(true);
                mSplitShareList.get(position).setAmount(Double.parseDouble(updatedValue));

                float amountForEach = Float.valueOf(String.format("%.2f", adjustment / (getUnupdatedCount())));
                for (SplitShare s : mSplitShareList) {
                    if (!s.isUpdate())
                        s.setAmount(s.getAmount() + amountForEach);
                }

          /* Double netAmount = Double.parseDouble(updatedValue)+(amountForEach*mSplitShareList.size()-1);
            Log.d(TAG, "OnEdited: net amount: "+netAmount);
            Log.d(TAG, "OnEdited: total bill amount: "+TotalBill);
            Log.d(TAG, "OnEdited: total bill amount adjustment: "+(TotalBill-netAmount));

            Log.d(TAG, "OnEdited: you share before: "+mSplitShareList.get(0).getAmount());
            Log.d(TAG, "OnEdited: you share after : "+(mSplitShareList.get(0).getAmount()+(TotalBill-netAmount)));
            mSplitShareList.get(0).setAmount((mSplitShareList.get(0).getAmount()+(TotalBill-netAmount)));*/

            } else {
                //do not allow editing
            }
            setListAdapter();
        }
    }

    @Override
    public void OnEditorModeOn() {
        Log.d(TAG, "OnEditorModeOn: ");

       // topLayout.setVisibility(View.GONE);
    }

    private int getUnupdatedCount() {
        int count = 0;

        for(SplitShare s:mSplitShareList)
            if(!s.isUpdate())
                count++;
        return count;
    }


    private class BillsListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mBillsList.size();
        }

        @Override
        public Object getItem(int position) {
            return mBillsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater layoutInflater = (LayoutInflater) ActivitySplitBills.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.row_bills, null);

            TextView txtTitle = (TextView) view.findViewById(R.id.txt_billsTitle);
            TextView txtAmount = (TextView) view.findViewById(R.id.txt_billsAmount);
            BillsObj mBillsObj = mBillsList.get(position);
            txtTitle.setText(mBillsObj.getTitle());
            txtAmount.setText(mBillsObj.getAmount());


            return view;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                try {
                    finish();
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }


            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
