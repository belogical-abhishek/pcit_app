package com.pcits.events.obj;

/**
 * Created by SAKSHI on 9/29/2017.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddUsersobj {




    @SerializedName("emails")
    @Expose
    private List<String> emails = null;

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }


}
