package com.pcits.events.adapters;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.pcits.events.R;
import com.pcits.events.fragments.FragmentWinner;
import com.pcits.events.fragments.NotifySurveyFragment;
import com.pcits.events.fragments.UpdateNewEventFragment;

public class TabMessageAdapter extends FragmentPagerAdapter {

	private String[] TITLES;

	public TabMessageAdapter(FragmentManager fm, Activity act) {
		super(fm);

		TITLES = new String[3];
		TITLES[0] = act.getResources().getString(R.string.tab_event);
		TITLES[1] = act.getResources().getString(R.string.tabwinner);
		TITLES[2] = act.getResources().getString(R.string.tabsurvey);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return TITLES[position];
	}

	@Override
	public int getCount() {
		return TITLES.length;
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case 0:
			return new UpdateNewEventFragment();
		case 1:
			return new FragmentWinner();
		case 2:
			return new NotifySurveyFragment();
		default:
			throw new IllegalArgumentException(
					"The item position should be less: " + TITLES.length);
		}
		// return SuperAwesomeCardFragment.newInstance(position);
	}
}
