package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.events.ActivityDetail;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.obj.NotifyBoxObj;
import com.pcits.events.stickylist.StickyListHeadersAdapter;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.MaterialRippleLayout;

public class NotifyUpdateNewEventAdapter extends BaseAdapter implements
		StickyListHeadersAdapter, SectionIndexer {

	private Activity activity;
	private ArrayList<NotifyBoxObj> arrMessage;
	private LayoutInflater mInflate;
	private int[] mSectionIndices;
	private Integer[] mSectionLetters;
	private Utils utils;
	private NotifyUpdateNewEventAdapter self;

	public NotifyUpdateNewEventAdapter(Activity a,
			ArrayList<NotifyBoxObj> listMessage) {
		self = this;
		this.activity = a;
		this.arrMessage = listMessage;
		this.mInflate = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		// mHeaders = a.getResources().getStringArray(R.array.tickets_header);
		mSectionIndices = getSectionIndices();
		mSectionLetters = getSectionLetters();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrMessage.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		// TODO Auto-generated method stub
		final HolderView holder;
		if (convertView == null) {
			holder = new HolderView();
			convertView = mInflate.inflate(R.layout.item_update_new, null);
			holder.lblTitle = (TextView) convertView
					.findViewById(R.id.lblTitleEvent);
			holder.lblDate = (TextView) convertView
					.findViewById(R.id.lblDateBuy);
			holder.itemTicket = (MaterialRippleLayout) convertView
					.findViewById(R.id.itemTicket);

			convertView.setTag(holder);
		} else {
			holder = (HolderView) convertView.getTag();
		}
		final NotifyBoxObj o = arrMessage.get(position);
		if (o != null) {
			utils = new Utils(activity);
			holder.lblTitle.setText(o.getTitle());
			holder.lblDate.setText("Date: "
					+ DateTimeUtility.convertTimeStampToDate(o.getDate(),
							"dd MMM yy")
					+ " - "
					+ DateTimeUtility.convertTimeStampToDate(o.getDate(),
							"HH:mm"));
			holder.itemTicket.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// GlobalValue.notify = arrMessage.get(position);
					// Intent i = new Intent(activity, ActivityDetail.class);
					// i.putExtra(utils.EXTRA_DEAL_ID,
					// GlobalValue.notify.getSummary());
					// parent.getContext().startActivity(i);

					showContextDialog(position);
				}
			});

		}
		return convertView;
	}

	public class HolderView {
		TextView lblTitle, lblDate;
		MaterialRippleLayout itemTicket;
	}

	class HeaderViewHolder {
		TextView lblHeader;
	}

	@Override
	public Object[] getSections() {
		// TODO Auto-generated method stub
		return mSectionLetters;
	}

	@Override
	public int getPositionForSection(int section) {
		// TODO Auto-generated method stub
		if (mSectionIndices.length == 0) {
			return 0;
		}

		if (section >= mSectionIndices.length) {
			section = mSectionIndices.length - 1;
		} else if (section < 0) {
			section = 0;
		}
		return mSectionIndices[section];
	}

	@Override
	public int getSectionForPosition(int position) {
		// TODO Auto-generated method stub
		for (int i = 0; i < mSectionIndices.length; i++) {
			if (position < mSectionIndices[i]) {
				return i - 1;
			}
		}
		return mSectionIndices.length - 1;
	}

	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		HeaderViewHolder holder;

		if (convertView == null) {
			holder = new HeaderViewHolder();
			convertView = mInflate.inflate(R.layout.item_header_ticket, parent,
					false);
			holder.lblHeader = (TextView) convertView
					.findViewById(R.id.lblHeader);
			convertView.setTag(holder);
		} else {
			holder = (HeaderViewHolder) convertView.getTag();
		}

		// set header text as first char in name
		String headerChar = arrMessage.get(position).getType_id() + "";
		if (headerChar.equals("2")) {
			holder.lblHeader.setText(activity.getResources().getString(
					R.string.update_event));
		} else if (headerChar.equals("3")) {
			holder.lblHeader.setText(activity.getResources().getString(
					R.string.new_event));
		}

		return convertView;
	}

	@Override
	public long getHeaderId(int position) {
		// TODO Auto-generated method stub
		return arrMessage.get(position).getType_id();
	}

	private int[] getSectionIndices() {
		ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
		int lastFirstChar = arrMessage.get(0).getType_id();
		sectionIndices.add(0);
		for (int i = 1; i < arrMessage.size(); i++) {
			if (arrMessage.get(i).getType_id() != lastFirstChar) {
				lastFirstChar = arrMessage.get(i).getType_id();
				sectionIndices.add(i);
			}
		}
		int[] sections = new int[sectionIndices.size()];
		for (int i = 0; i < sectionIndices.size(); i++) {
			sections[i] = sectionIndices.get(i);
		}
		return sections;
	}

	private Integer[] getSectionLetters() {
		Integer[] letters = new Integer[mSectionIndices.length];
		for (int i = 0; i < mSectionIndices.length; i++) {
			letters[i] = arrMessage.get(mSectionIndices[i]).getType_id();
		}
		return letters;
	}

	private void showContextDialog(final int pos) {
		final Dialog dialog = new Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_message_event);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		Button btnView = (Button) dialog.findViewById(R.id.btn_view);
		Button btnRemove = (Button) dialog.findViewById(R.id.btn_remove);
		try{
			btnView.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					GlobalValue.notify = arrMessage.get(pos);
					Intent i = new Intent(activity, ActivityDetail.class);
					i.putExtra(utils.EXTRA_DEAL_ID, GlobalValue.notify.getSummary());
					activity.startActivity(i);
	
					dialog.dismiss();
				}
			});
	
			btnRemove.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					AlertDialog.Builder builder = new Builder(activity);
					builder.setTitle(activity.getResources().getString(
							R.string.remove_message));
					builder.setMessage(activity.getResources().getString(
							R.string.do_you_want_to_remove_message));
	
					builder.setNegativeButton(
							activity.getResources().getString(R.string.no),
							new DialogInterface.OnClickListener() {
	
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}
							});
	
					builder.setPositiveButton(
							activity.getResources().getString(R.string.yes),
							new DialogInterface.OnClickListener() {
	
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									DatabaseUtility dbUtility = new DatabaseUtility();
									if (dbUtility.deleteFromNotification(activity,
											GlobalValue.arrUpdateNewEvent.get(pos))) {
										// Refresh list
										GlobalValue.arrUpdateNewEvent.remove(pos);
										self.notifyDataSetChanged();
	
										Toast.makeText(activity,
												"Removed successfully.",
												Toast.LENGTH_SHORT).show();
									} else {
										Toast.makeText(activity,
												"Error: Can not remove this.",
												Toast.LENGTH_SHORT).show();
									}
								}
							});
	
					builder.create().show();
	
					dialog.dismiss();
				}
			});
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
		dialog.show();
	}
}
