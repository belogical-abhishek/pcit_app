package com.pcits.events.obj;

import com.google.gson.annotations.SerializedName;

public class TnCobj {

	@SerializedName("tnc_id")
	private String tnc_id;
	@SerializedName("username")
	private String username;
	@SerializedName("tnc_desc")
	private String tncDesc;
	@SerializedName("notes")
	private String notes;

	public String getTnc_id() {
		return tnc_id;
	}

	public void setTnc_id(String tnc_id) {
		this.tnc_id = tnc_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTncDesc() {
		return tncDesc;
	}

	public void setTncDesc(String tncDesc) {
		this.tncDesc = tncDesc;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
