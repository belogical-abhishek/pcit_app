package com.pcits.events.database;

public final class DBKeyConfig {

	// TABLE :
	// ****************************************************************
	public static String TABLE_API = "Api";
	public static String TABLE_DEALS = "tbl_deals";
	public static String TABLE_PAIDTICKET = "tbl_paidticket";
	public static String TABLE_ADMIN = "tbl_admin";
	public static String TABLE_CLIENT = "tbl_client";
	public static String TABLE_NOTIFICATION = " tbl_notification";

	// *********** KEY FOR TABLE API **************
	public static String KEY_API_ID = "id";
	public static String KEY_API_API = "api";
	public static String KEY_API_RESULT = "result";

	// Key for tbl_notification table
	public static String KEY_NOTIFICATION_NOTIFY_ID = "notify_id";
	public static String KEY_NOTIFICATION_TYPE_ID = "type_id";
	public static String KEY_NOTIFICATION_SUMMARY = "summary";
	public static String KEY_NOTIFICATION_DATE = "date";
	public static String KEY_NOTIFICATION_STATUS_ID = "status_id";
	public static String KEY_NOTIFICATION_TITLE = "title";
}
