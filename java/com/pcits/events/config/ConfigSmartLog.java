package com.pcits.events.config;

public class ConfigSmartLog {
	public static final boolean DEBUG_MODE = true;
	public static final boolean DEBUG_WS = true;
	public static final boolean DEBUG_DB = false;
}
