package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pcits.events.R;
import com.pcits.events.obj.CategoryObj;

public class AdapterCategory extends BaseAdapter {

	private ArrayList<CategoryObj> arrCategory;
	private LayoutInflater mInflate;

	public AdapterCategory(Activity activity,
			ArrayList<CategoryObj> listCategory) {
		this.arrCategory = listCategory;
		this.mInflate = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrCategory.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {
		// TODO Auto-generated method stub
		final HolderView holder;
		if (convertView == null) {
			holder = new HolderView();
			convertView = mInflate.inflate(R.layout.item_list_category, null);
			holder.lblCategory_id = (TextView) convertView
					.findViewById(R.id.lblCategoryId);
			holder.lblCategoryName = (TextView) convertView
					.findViewById(R.id.lblCategoryName);
			convertView.setTag(holder);
		} else {
			holder = (HolderView) convertView.getTag();
		}
		final CategoryObj o = arrCategory.get(position);
		if (o != null) {
			holder.lblCategory_id.setText(o.getCategoryId());
			holder.lblCategoryName.setText(o.getCategoryName());
		}
		return convertView;
	}

	public class HolderView {
		TextView lblCategory_id, lblCategoryName;
	}

}
