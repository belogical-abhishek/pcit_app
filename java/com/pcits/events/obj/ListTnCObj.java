package com.pcits.events.obj;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by EnvisioDevs on 13-12-2017.
 */

public class ListTnCObj {

    @SerializedName("data")
    private ArrayList<TnCobj> list;

    public ArrayList<TnCobj> getList() {
        return list;
    }

    public void setList(ArrayList<TnCobj> list) {
        this.list = list;
    }
}
