package com.pcits.events;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.widgets.KenBurnsView;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class ActivityDetailTickets extends Activity {

	private ImageView qrImg;
	private Button ticket;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;
	private TextView lblTitleHeader;
	private TextViewRobotoCondensedRegular lblUniqueCode;
	private String url = "http://chart.apis.google.com/chart?cht=qr&chs=400x400&chld=M&choe=UTF-8&chl=";

	private AQuery mAq;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_detail_tickets);
		qrImg = (ImageView) findViewById(R.id.qrImg);
		ticket = (Button) findViewById(R.id.ticket);
		lblTitleHeader = (TextView) findViewById(R.id.lblTitleHeader);
		lblUniqueCode = (TextViewRobotoCondensedRegular) findViewById(R.id.lblUniqueCode);

		mAq = new AQuery(this);
		mAq.id(qrImg).image(
				url + " Id: " + GlobalValue.ticket.getId() + "\n Type: "
						+ "tickets" + "\n Event: "
						+ GlobalValue.ticket.getTitle() + "\n By company: "
						+ GlobalValue.ticket.getCompany() + "\n User: "
						+ GlobalValue.ticket.getClientId() + "\n Date buy: "
						+ GlobalValue.ticket.getUsed_date() + "\n Code: "
						+ GlobalValue.ticket.getUniqueCode());

		if (GlobalValue.msg != null) {
			lblTitleHeader.setText(getResources().getString(
					R.string.barcodeWinner));
			lblUniqueCode.setText(" " + GlobalValue.msg.getUniquecode());
			if (GlobalValue.msg.getStatus() == 0) {
				ticket.setBackgroundColor(Color.GREEN);
			} else {
				ticket.setBackgroundColor(Color.GRAY);
				ticket.setText("USED");
			}

			mAq.id(qrImg).image(
					url + " Id: " + GlobalValue.msg.getId() + "\n Type: "
							+ "winner" + "\n Event: "
							+ GlobalValue.msg.getTitle() + "\n By company: "
							+ GlobalValue.msg.getCompany() + "\n User: "
							+ GlobalValue.msg.getClient_id() + "\n Code: "
							+ GlobalValue.msg.getUniquecode());

		}
		llMenu = (LinearLayout) findViewById(R.id.llMenu);
		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		initNotBoringActionBar();

	}

	private void initNotBoringActionBar() {
		mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}
}
