package com.pcits.events.modelmanager;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.network.AsyncHttpGet;
import com.pcits.events.network.AsyncHttpPost;
import com.pcits.events.network.AsyncHttpResponseProcess;
import com.pcits.events.network.ParameterFactory;
import com.pcits.events.obj.APIObj;
import com.pcits.events.obj.AttendedObj;
import com.pcits.events.obj.CategoryObj;
import com.pcits.events.obj.ClientInfo;
import com.pcits.events.obj.DealObj;
import com.pcits.events.obj.GcmObj;
import com.pcits.events.obj.PaidticketsObj;
import com.pcits.events.obj.PurchaseObj;
import com.pcits.events.obj.QuestionInfo;
import com.pcits.events.obj.TnCobj;
import com.pcits.events.obj.UserObj;
import com.pcits.events.viewpage.CustomProgressDialog;

public class ModelManager {
	private static String TAG = "ModelManager";
	private static String mDealId = "";

	public static void getDeals(final Context context, boolean isProgess,
			final ModelManagerListener listener) {
		try{
			final String url = UserFunctions.URL_ALL_DEALS;
	
			final DatabaseUtility databaseUtility = new DatabaseUtility();
			if (!databaseUtility.checkExistsApi(context, url)) {
				APIObj apiInfo = new APIObj();
				Log.d("", "DATA" + " chua co du lieu");
				// add vao database
				apiInfo.setmApi(url);
				if (databaseUtility.insertApi(context, apiInfo)) {
					Log.d("", "INSERT thanh cong");
				} else {
					Log.d("", "INSERT khong  thanh cong");
				}
	
			} else {
				Log.d("", "DATA" + "data da co du lieu");
			}
	
			AsyncHttpGet post = new AsyncHttpGet(context,
					new AsyncHttpResponseProcess() {
						@Override
						public void processIfResponseSuccess(String response) {
							if (response != null) {
								listener.onSuccess(response);
	
								if (databaseUtility.updateResuft_Api(context,
										response, url)) {
									Log.d("UPDATE", "update " + "update thanh cong");
								} else {
									Log.d("UPDATE", "update "
											+ "update khong thanh cong");
								}
							} else
								listener.onError();
						}
					}, new ArrayList<NameValuePair>(), isProgess);
			post.execute(url);
		} catch(Exception ex){
	        Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
	    }
	}

	// list tickets by event
	public static void getListTicketByEvent(final Activity context,
			String dealId, boolean isProgess,
			final ModelManagerListener listener) {
		try{
			final String url = UserFunctions.URL_TICKETS_BY_EVENT;
	
			mDealId = dealId;
			if (!DatabaseUtility.checkExistsApi(context, url + mDealId)) {
				APIObj apiInfo = new APIObj();
				Log.d(TAG, "DATA" + " chua co du lieu");
				// add vao database
				apiInfo.setmApi(url + mDealId);
				if (DatabaseUtility.insertApi(context, apiInfo)) {
					Log.d(TAG, "INSERT thanh cong");
				} else {
					Log.d(TAG, "INSERT khong  thanh cong");
				}
	
			} else {
				Log.d("", "DATA" + "data da co du lieu");
			}
			ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
					.listByEventParams(context, dealId);
			AsyncHttpGet post = new AsyncHttpGet(context,
					new AsyncHttpResponseProcess() {
						@Override
						public void processIfResponseSuccess(String response) {
							if (response != null) {
								listener.onSuccess(response);
								if (DatabaseUtility.updateResuft_Api(context,
										response, url + mDealId)) {
									Log.d("UPDATE", "update " + "update thanh cong");
								} else {
									Log.d("UPDATE", "update "
											+ "update khong thanh cong");
								}
							} else
								listener.onError();
						}
					}, params, isProgess);
			post.execute(url);
		} catch(Exception ex){
	        Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
	    }
	}

	public static void getListDeals(Context context, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_ALL_DEALS;
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, new ArrayList<NameValuePair>(), isProgess);
		post.execute(url);
	}

	//
	public static void attended(Context context, AttendedObj attend,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_ATTENDED;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createAttendedParams(context, attend);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// list check attending
	public static void getCheckAttending(Context context, String device,
			String dealId, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_CHECK_ATTENDED;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createCheckAttending(context, device, dealId);
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// list check lucky
	public static void getCheckLucky(Context context, String dealId,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_CHECK_LUCKY;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.listByEventParams(context, dealId);
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// admin credits
	public static void adminCredits(Context context, int credit,
			String username, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_UPDATE_ADMIN_CREDITS;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.updateAdminCreditaParams(context, credit, username);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// update featured
	public static void updateFeatured(Context context, String deal_id,
			String startDate, String endDate, int featured, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_UPDATE_FEATURED;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.updatefeaturedParams(context, deal_id, startDate, endDate,
						featured);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// buy credits
	public static void buyCredits(Context context, String username,
			int credits, boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_BUY_CREDITS;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.updateAdminCreditaParams(context, credits, username);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// register client
	public static void register(Context context, ClientInfo account,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_REGISTER_CLIENT;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createRegisterParams(context, account);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	public static void registerUser(Context context, UserObj user,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_CREATE_ADMIN;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createUserParams(context, user);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// create deals
	public static void createDeals(Context context, DealObj deal,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_CREATE_DEAL;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createDealsParams(context, deal);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// // create category
	// public static void createCategory(Context context, CategoryObj category,
	// boolean isProgess, final ModelManagerListener listener) {
	// final String url = UserFunctions.URL_CREATE_CATEGORY;
	// ArrayList<NameValuePair> params = (ArrayList<NameValuePair>)
	// ParameterFactory
	// .createCategoryParams(context, category);
	// AsyncHttpPost post = new AsyncHttpPost(context,
	// new AsyncHttpResponseProcess() {
	// @Override
	// public void processIfResponseSuccess(String response) {
	// if (response != null) {
	// listener.onSuccess(response);
	// } else
	// listener.onError();
	// }
	// }, params, isProgess);
	// post.execute(url);
	// }

	// update click count event
	public static void updateClickCount(Context context, String deal_id,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_UPDATE_CLICK_COUNT;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.updateClickCountParams(context, deal_id,"1");
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// Forget password

	public static void forgetpassword(Context context, String email,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_FORGET_PASSWORD;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.forgetpasswordParams(context, email);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// Register Device
	public static void registerDevice(Context context, String ime, String gcm_id) {
		final String url = UserFunctions.URL_REGISTER_DEVICE;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.registerDeviceParams(context, ime, gcm_id);
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						Log.d("Response GCM ID", response);
					}
				}, params, false);
		post.execute(url);
	}

	// update gcm
	public static void updateGcm(Context context, String ime, GcmObj gcm,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_UPDATE_GCM;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.updateGcmParams(context, ime, gcm);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// register tickets
	public static void registerTickets(Context context, PaidticketsObj tickets,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_REGISTER_PAIDTICKETS;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.registerTicketsParams(context, tickets);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// user purchase
	public static void userPurchase(Context context, PurchaseObj purchaseObj,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_USER_PURCHASE;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.purchaseParams(context, purchaseObj);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// create question
	public static void createQuestion(Context context,
			QuestionInfo questionObj, String check, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_CREATE_QUESTION;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.questionsParams(context, questionObj, check);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// create answer
	public static void createAnswer(Context context, String option_group_id,
			String answer, String check, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_CREATE_QUESTION;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.answerParams(context, option_group_id, answer, check);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// list tickets
	public static void getListTickets(Activity context, String type,
			String user, boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_SHOW_PAIDTICKETS;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.listTicketsParams(context, type, user);
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// list Attendee of event
	public static void getListAttendOfEvent(Activity context, String deadId,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_SHOW_ATTENDED;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.listByEventParams(context, deadId);
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// list lucky draw
	public static void getListLuckyDraw(Activity context, String deadId,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_SHOW_LUCKY;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.listByEventParams(context, deadId);
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// list message
	public static void getListMessage(Activity context, String user,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_SHOW_MESSAGE;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.listMessage(context, user);
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// update tickets
	public static void updateTickets(Context context, String id, String status,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_UPDATE_TICKETS;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.updateTicketsParams(context, id, status);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// update notifybox
	public static void updateNotifyBox(Context context, String id,
			String status, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_UPDATE_NOTIFYBOX;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.updateTicketsParams(context, id, status);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// update lucky draw
	public static void updateWinner(Context context, String id, String status,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_UPDATE_WINNER;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.updateTicketsParams(context, id, status);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// create tnc
	public static void createTnC(Context context, TnCobj tnc,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_CREATE_TNC;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createTnCParams(context, tnc);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	public static void editTnc(Context context, String tncId, TnCobj tnc,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_EDIT_TNC;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.editTnCParams(context, tncId, tnc);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// list tnc by user
	public static void getListTncByUser(Context context, String username,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_SHOW_TNC_BY_USER;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createTncByUserParams(context, username);
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	public static void getListTnc(Context context, String username,
			String role, boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_SHOW_TNC_BY_USER;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createTncParams(context, username, role);
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// list show all tnc
	public static void pushWinner(Context context, String deal_id, String note,
			String value, int type, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_WINNER;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.winnerParams(context, deal_id, note, value, type);
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// list show all tnc
	public static void getListAllTnC(Context context, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_SHOW_ALL_TNC;
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, new ArrayList<NameValuePair>(), isProgess);
		post.execute(url);
	}

	// list category
	public static void getListCategory(Context context, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_SHOW_CATEGORY;
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, new ArrayList<NameValuePair>(), isProgess);
		post.execute(url);
	}

	public static void login(Context context, String userName, String passWord,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_LOGIN;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createLoginParams(context, userName, passWord);
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	public static void loginUser(Context context, String userName,
			String passWord, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_LOGIN_USER;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.userLoginParams(context, userName, passWord);
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	public static void loginMedia(Context context, ClientInfo fb,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_LOGIN_MEDIA;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createRegisterFBparam(context, fb);
		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// public static void updateAccount(Context context, String clientname,
	// AccountInfo account, boolean isProgess,
	// final ModelManagerListener listener) {
	// final String url = UserFunctions.URL_UPDATE_CLIENT;
	// ArrayList<NameValuePair> params = (ArrayList<NameValuePair>)
	// ParameterFactory
	// .createUpdateAccountParams(context, clientname, account);
	// AsyncHttpPost post = new AsyncHttpPost(context,
	// new AsyncHttpResponseProcess() {
	// @Override
	// public void processIfResponseSuccess(String response) {
	// if (response != null) {
	// listener.onSuccess(response);
	// } else
	// listener.onError();
	// }
	// }, params, isProgess);
	// post.execute(url);
	// }

	// public static void updateUser(Context context, String clientname,
	// UserObj user, boolean isProgess, final ModelManagerListener listener) {
	// final String url = UserFunctions.URL_UPDATE_ADMIN;
	// ArrayList<NameValuePair> params = (ArrayList<NameValuePair>)
	// ParameterFactory
	// .createUpdateUserParams(context, clientname, user);
	// AsyncHttpPost post = new AsyncHttpPost(context,
	// new AsyncHttpResponseProcess() {
	// @Override
	// public void processIfResponseSuccess(String response) {
	// if (response != null) {
	// listener.onSuccess(response);
	// } else
	// listener.onError();
	// }
	// }, params, isProgess);
	// post.execute(url);
	// }

	public static void changePassword(Context context, String clientname,
			String oldPass, String newPass, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_CHANGE_PASSWORD;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createChangePassParams(context, clientname, oldPass, newPass);
		AsyncHttpPost post = new AsyncHttpPost(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	// add deals
	public static void addDeals(final Activity context, DealObj deal,
			Bitmap photo, boolean isShowDialog,
			final ModelManagerListener listener) {

		new ProcessDealsDoInBackground(context, deal, photo, isShowDialog,
				listener).execute(UserFunctions.URL_CREATE_DEAL);
	}

	// update deals
	public static void updateDeal(final Activity activity, DealObj deal,
			Bitmap photo, boolean isShowDialog,
			final ModelManagerListener listener)
			 {

			     if(deal==null)
			         {
			             deal=GlobalValue.dealsObj;
}
		new ProcessDealsDoInBackground(activity, deal, photo, isShowDialog,
				listener).execute(UserFunctions.URL_UPDATE_DEAL);
	}

	// public static void updateEvent(Context context, String dealId,
	// DealObj deal, boolean isProgess, final ModelManagerListener listener) {
	// final String url = UserFunctions.URL_UPDATE_DEAL;
	// ArrayList<NameValuePair> params = (ArrayList<NameValuePair>)
	// ParameterFactory
	// .createUpdateEventParams(context, dealId, deal);
	// AsyncHttpPost post = new AsyncHttpPost(context,
	// new AsyncHttpResponseProcess() {
	// @Override
	// public void processIfResponseSuccess(String response) {
	// if (response != null) {
	// listener.onSuccess(response);
	// } else
	// listener.onError();
	// }
	// }, params, isProgess);
	// post.execute(url);
	// }

	// add category
	public static void addCategory(final Activity context,
			CategoryObj category, Bitmap photo, boolean isShowDialog,
			final ModelManagerListener listener) {

		new ProcessCategoryDoInBackground(context, category, photo,
				isShowDialog, listener)
				.execute(UserFunctions.URL_CREATE_CATEGORY);
	}

	// update category
	public static void updateCategory(final Activity activity,
			CategoryObj category, Bitmap photo, boolean isShowDialog,
			final ModelManagerListener listener) {

		new ProcessCategoryDoInBackground(activity, category, photo,
				isShowDialog, listener)
				.execute(UserFunctions.URL_UPDATE_CATEGORY);
	}

	// register client
	public static void registerClient(final Activity context,
			ClientInfo client, Bitmap photo, boolean isShowDialog,
			final ModelManagerListener listener) {

		new ProcessClientDoInBackground(context, client, photo, isShowDialog,
				listener).execute(UserFunctions.URL_REGISTER_CLIENT);
	}

	// update client
	public static void updateClient(final Activity context, ClientInfo client,
			Bitmap photo, boolean isShowDialog,
			final ModelManagerListener listener) {

		new ProcessupdateClientDoInBackground(context, client, photo,
				isShowDialog, listener)
				.execute(UserFunctions.URL_UPDATE_CLIENT);
	}

	// register admin
	public static void registerAdmin(final Activity context, UserObj admin,
			Bitmap photo, boolean isShowDialog,
			final ModelManagerListener listener) {

		new ProcessAdminDoInBackground(context, admin, photo, isShowDialog,
				listener).execute(UserFunctions.URL_CREATE_ADMIN);
	}

	// update admin
	public static void updateAdmin(final Activity context, UserObj admin,
			Bitmap photo, boolean isShowDialog,
			final ModelManagerListener listener) {

		new ProcessUpdateAdminDoInBackground(context, admin, photo,
				isShowDialog, listener).execute(UserFunctions.URL_UPDATE_ADMIN);
	}

	// ================= create ==========================

	// register admin
	public static class ProcessAdminDoInBackground extends
			AsyncTask<String, String, String> {

		private CustomProgressDialog dialog;
		private UserObj admin;
		private Bitmap adminfile;
		private Activity act;
		private boolean isShowDialog;
		ModelManagerListener listener;

		public ProcessAdminDoInBackground(Activity act, UserObj admin,
				Bitmap photo, boolean isShowDialog,
				ModelManagerListener listener) {
			this.act = act;
			this.dialog = new CustomProgressDialog(this.act);
			this.dialog.setCancelable(false);
			this.listener = listener;
			this.admin = admin;
			this.adminfile = photo;
			this.isShowDialog = isShowDialog;
		}

		protected void onPreExecute() {
			if (isShowDialog)
				dialog.show();
		}

		protected String doInBackground(String... url) {

			return addAdmin(this.act, admin, adminfile, url[0]);

		}

		protected void onPostExecute(String result) {
			dialog.dismiss();
			listener.onSuccess(result);
		}

	}

	public static String addAdmin(Activity act, UserObj admin, Bitmap photo,
			String url) {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(url);
			byte[] data = null;
			ByteArrayBody bab = null;

			ByteArrayOutputStream bos;
			bos = new ByteArrayOutputStream();
			if (photo != null) {
				String filename = "iconmember"
						+ String.valueOf(Calendar.getInstance()
								.getTimeInMillis()) + ".jpg";
				photo.compress(CompressFormat.JPEG, 100, bos);
				data = bos.toByteArray();
				bab = new ByteArrayBody(data, filename);
			}

			MultipartEntity reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			if (photo == null) {
				System.out.println("no_image: " + "true");
				reqEntity.addPart("image", new StringBody(""));
			} else {
				System.out.println("no_image: " + "false");
				reqEntity.addPart("image", bab);
			}
			if (admin.getUsername() != null && !admin.getUsername().isEmpty()) {
				reqEntity.addPart("username",
						new StringBody(admin.getUsername()));
				System.out.println("username : " + admin.getUsername());
				Log.d("username", "username: " + admin.getUsername());
			}
			reqEntity.addPart("password", new StringBody(admin.getPassword()));
			reqEntity.addPart("fname", new StringBody(admin.getFname()));
			reqEntity.addPart("lname", new StringBody(admin.getLname()));
			reqEntity.addPart("dob", new StringBody(admin.getDob()));
			reqEntity.addPart("email", new StringBody(admin.getEmail()));
			Log.d("email", "email: " + admin.getEmail());
			reqEntity.addPart("phone", new StringBody(admin.getPhone()));
			reqEntity.addPart("address", new StringBody(admin.getAddress()));
			reqEntity.addPart("city", new StringBody(admin.getCity()));
			reqEntity.addPart("county", new StringBody(admin.getCounty()));
			reqEntity.addPart("postcode", new StringBody(admin.getPostcode()));
			reqEntity.addPart("country", new StringBody(admin.getCountry()));
			reqEntity.addPart("role", new StringBody(admin.getRole()));
			reqEntity.addPart("status", new StringBody(admin.getStatus()));

			postRequest.setEntity(reqEntity);
			HttpResponse response2 = httpClient.execute(postRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response2.getEntity().getContent(), "UTF-8"));
			String sResponse;
			StringBuilder s = new StringBuilder();
			while ((sResponse = reader.readLine()) != null) {
				s = s.append(sResponse);
			}
			String str = s.toString();
			Log.d("aaaaaa", "response: " + str);
			return str;

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	// register admin
	public static class ProcessUpdateAdminDoInBackground extends
			AsyncTask<String, String, String> {

		private CustomProgressDialog dialog;
		private UserObj admin;
		private Bitmap adminfile;
		private Activity act;
		private boolean isShowDialog;
		ModelManagerListener listener;

		public ProcessUpdateAdminDoInBackground(Activity act, UserObj admin,
				Bitmap photo, boolean isShowDialog,
				ModelManagerListener listener) {
			this.act = act;
			this.dialog = new CustomProgressDialog(this.act);
			this.dialog.setCancelable(false);
			this.listener = listener;
			this.admin = admin;
			this.adminfile = photo;
			this.isShowDialog = isShowDialog;
		}

		protected void onPreExecute() {
			if (isShowDialog)
				dialog.show();
		}

		protected String doInBackground(String... url) {

			return updatedAdmin(this.act, admin, adminfile, url[0]);

		}

		protected void onPostExecute(String result) {
			dialog.dismiss();
			listener.onSuccess(result);
		}

	}

	public static String updatedAdmin(Activity act, UserObj admin,
			Bitmap photo, String url) {
		try {

			Log.e("PCIT_ABHISHEK", url);
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(url);
			byte[] data = null;
			ByteArrayBody bab = null;

			ByteArrayOutputStream bos;
			bos = new ByteArrayOutputStream();
			if (photo != null) {
				String filename = "iconmember"
						+ String.valueOf(Calendar.getInstance()
								.getTimeInMillis()) + ".jpg";
				photo.compress(CompressFormat.JPEG, 100, bos);
				data = bos.toByteArray();
				bab = new ByteArrayBody(data, filename);
			}

			MultipartEntity reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			if (photo == null) {
				System.out.println("no_image: " + "true");
				reqEntity.addPart("image", new StringBody(""));
			} else {
				System.out.println("no_image: " + "false");
				reqEntity.addPart("image", bab);
			}
			if (admin.getUsername() != null && !admin.getUsername().isEmpty()) {
				reqEntity.addPart("username",
						new StringBody(admin.getUsername()));
				System.out.println("username : " + admin.getUsername());
				Log.d("username", "username: " + admin.getUsername());
			}
			reqEntity.addPart("fname", new StringBody(admin.getFname()));
			reqEntity.addPart("lname", new StringBody(admin.getLname()));
			reqEntity.addPart("dob", new StringBody(admin.getDob()));
			reqEntity.addPart("email", new StringBody(admin.getEmail()));
			Log.d("email", "email: " + admin.getEmail());
			reqEntity.addPart("phone", new StringBody(admin.getPhone()));
			reqEntity.addPart("address", new StringBody(admin.getAddress()));
			reqEntity.addPart("city", new StringBody(admin.getCity()));
			reqEntity.addPart("county", new StringBody(admin.getCounty()));
			reqEntity.addPart("postcode", new StringBody(admin.getPostcode()));
			reqEntity.addPart("country", new StringBody(admin.getCountry()));
			reqEntity.addPart("pay_info", new StringBody(admin.getPayInfo()));

			postRequest.setEntity(reqEntity);
			HttpResponse response2 = httpClient.execute(postRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response2.getEntity().getContent(), "UTF-8"));
			String sResponse;
			StringBuilder s = new StringBuilder();
			while ((sResponse = reader.readLine()) != null) {
				s = s.append(sResponse);
			}
			String str = s.toString();
			Log.d("aaaaaa", "response: " + str);
			return str;

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	// register client
	public static class ProcessClientDoInBackground extends
			AsyncTask<String, String, String> {

		private CustomProgressDialog dialog;
		private ClientInfo client;
		private Bitmap clientfile;
		private Activity act;
		private boolean isShowDialog;
		ModelManagerListener listener;

		public ProcessClientDoInBackground(Activity act, ClientInfo client,
				Bitmap photo, boolean isShowDialog,
				ModelManagerListener listener) {
			this.act = act;
			this.dialog = new CustomProgressDialog(this.act);
			this.dialog.setCancelable(false);
			this.listener = listener;
			this.client = client;
			this.clientfile = photo;
			this.isShowDialog = isShowDialog;
		}

		protected void onPreExecute() {
			if (isShowDialog)
				dialog.show();
		}

		protected String doInBackground(String... url) {

			return addClient(this.act, client, clientfile, url[0]);

		}

		protected void onPostExecute(String result) {
			dialog.dismiss();
			listener.onSuccess(result);
		}

	}

	public static String addClient(Activity act, ClientInfo client,
			Bitmap photo, String url) {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(url);
			byte[] data = null;
			ByteArrayBody bab = null;

			ByteArrayOutputStream bos;
			bos = new ByteArrayOutputStream();
			if (photo != null) {
				String filename = "iconmember"
						+ String.valueOf(Calendar.getInstance()
								.getTimeInMillis()) + ".jpg";
				photo.compress(CompressFormat.JPEG, 100, bos);
				data = bos.toByteArray();
				bab = new ByteArrayBody(data, filename);
			}

			MultipartEntity reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			if (photo == null) {
				System.out.println("no_image: " + "true");
				reqEntity.addPart("image", new StringBody(""));
			} else {
				System.out.println("no_image: " + "false");
				reqEntity.addPart("image", bab);
			}
			if (client.getUsername() != null && !client.getUsername().isEmpty()) {
				reqEntity.addPart("clientname",
						new StringBody(client.getUsername()));
				System.out.println("clientname id : " + client.getUsername());
				Log.d("clientname", "clientname: " + client.getUsername());
			}
			reqEntity.addPart("password", new StringBody(client.getPassword()));
			reqEntity.addPart("fname", new StringBody(client.getFname()));
			reqEntity.addPart("lname", new StringBody(client.getLname()));
			reqEntity.addPart("dob", new StringBody(client.getDob()));
			reqEntity.addPart("email", new StringBody(client.getEmail()));
			Log.d("email", "email: " + client.getEmail());
			reqEntity.addPart("gender", new StringBody(client.getGender()));
			reqEntity.addPart("phone", new StringBody(client.getPhone()));
			reqEntity.addPart("address", new StringBody(client.getAddress()));
			reqEntity.addPart("city", new StringBody(client.getCity()));
			reqEntity.addPart("county", new StringBody(client.getCounty()));
			reqEntity.addPart("postcode", new StringBody(client.getPostcode()));
			reqEntity.addPart("country", new StringBody(client.getCountry()));
			reqEntity
					.addPart("device_id", new StringBody(client.getDeviceid()));

			postRequest.setEntity(reqEntity);
			HttpResponse response2 = httpClient.execute(postRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response2.getEntity().getContent(), "UTF-8"));
			String sResponse;
			StringBuilder s = new StringBuilder();
			while ((sResponse = reader.readLine()) != null) {
				s = s.append(sResponse);
			}
			String str = s.toString();
			Log.d("aaaaaa", "response: " + str);
			return str;

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	// update client
	public static class ProcessupdateClientDoInBackground extends
			AsyncTask<String, String, String> {

		private CustomProgressDialog dialog;
		private ClientInfo client;
		private Bitmap clientfile;
		private Activity act;
		private boolean isShowDialog;
		ModelManagerListener listener;

		public ProcessupdateClientDoInBackground(Activity act,
				ClientInfo client, Bitmap photo, boolean isShowDialog,
				ModelManagerListener listener) {
			this.act = act;
			this.dialog = new CustomProgressDialog(this.act);
			this.dialog.setCancelable(false);
			this.listener = listener;
			this.client = client;
			this.clientfile = photo;
			this.isShowDialog = isShowDialog;
		}

		protected void onPreExecute() {
			if (isShowDialog)
				dialog.show();
		}

		protected String doInBackground(String... url) {

			return updateClient(this.act, client, clientfile, url[0]);

		}

		protected void onPostExecute(String result) {
			dialog.dismiss();

			String json = (String) result;
			boolean answer = ParserUtility.updateAccount(json);
			// GlobalValue.myClient = new ClientInfo();
			// GlobalValue.myClient = ParserUtility
			// .parserAccount(json);
			if (answer) {
				GlobalValue.myClient.setFname(client.getFname());
				GlobalValue.myClient.setLname(client.getLname());
				GlobalValue.myClient.setAddress(client.getAddress());
				GlobalValue.myClient.setDob(client.getDob());
				GlobalValue.myClient.setCity(client.getCity());
				GlobalValue.myClient.setEmail(client.getEmail());
				GlobalValue.myClient.setGender(client.getGender());
				GlobalValue.myClient.setCountry(client.getCountry());
				GlobalValue.myClient.setCounty(client.getCounty());
				GlobalValue.myClient.setPhone(client.getPhone());
				GlobalValue.myClient.setPostcode(client.getPostcode());
			}

			listener.onSuccess(result);
			Toast.makeText(act, "Updated.", Toast.LENGTH_LONG).show();
		}

	}

	public static String updateClient(Activity act, ClientInfo client,
			Bitmap photo, String url) {
		try {
			Log.e("PCIT_ABHISHEK", url);


			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(url);
			byte[] data = null;
			ByteArrayBody bab = null;

			ByteArrayOutputStream bos;
			bos = new ByteArrayOutputStream();
			if (photo != null) {
				String filename = "iconmember"
						+ String.valueOf(Calendar.getInstance()
								.getTimeInMillis()) + ".jpg";
				photo.compress(CompressFormat.JPEG, 100, bos);
				data = bos.toByteArray();
				bab = new ByteArrayBody(data, filename);
			}

			MultipartEntity reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			if (photo == null) {
				System.out.println("no_image: " + "true");
				reqEntity.addPart("image", new StringBody(""));
			} else {
				System.out.println("no_image: " + "false");
				reqEntity.addPart("image", bab);
			}
			if (client.getUsername() != null && !client.getUsername().isEmpty()) {
				reqEntity.addPart("clientname",
						new StringBody(client.getUsername()));
				System.out.println("clientname id : " + client.getUsername());
				Log.d("clientname", "clientname: " + client.getUsername());
			}
			reqEntity.addPart("fname", new StringBody(client.getFname()));
			reqEntity.addPart("lname", new StringBody(client.getLname()));
			reqEntity.addPart("dob", new StringBody(client.getDob()));
			reqEntity.addPart("email", new StringBody(client.getEmail()));
			Log.d("email", "email: " + client.getEmail());
			reqEntity.addPart("gender", new StringBody(client.getGender()));
			reqEntity.addPart("phone", new StringBody(client.getPhone()));
			reqEntity.addPart("address", new StringBody(client.getAddress()));
			reqEntity.addPart("city", new StringBody(client.getCity()));
			reqEntity.addPart("county", new StringBody(client.getCounty()));
			reqEntity.addPart("postcode", new StringBody(client.getPostcode()));
			reqEntity.addPart("country", new StringBody(client.getCountry()));

			postRequest.setEntity(reqEntity);
			HttpResponse response2 = httpClient.execute(postRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response2.getEntity().getContent(), "UTF-8"));
			String sResponse;
			StringBuilder s = new StringBuilder();
			while ((sResponse = reader.readLine()) != null) {
				s = s.append(sResponse);
			}
			String str = s.toString();

			if(str.length() == 0) {
				str = "{\"status\":\"success\"}";
			}

			return str;

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	// create category
	public static class ProcessCategoryDoInBackground extends
			AsyncTask<String, String, String> {

		private CustomProgressDialog dialog;
		private CategoryObj account;
		private Bitmap userfile;
		private Activity act;
		private boolean isShowDialog;
		ModelManagerListener listener;

		public ProcessCategoryDoInBackground(Activity act, CategoryObj acc,
				Bitmap photo, boolean isShowDialog,
				ModelManagerListener listener) {
			this.act = act;
			this.dialog = new CustomProgressDialog(this.act);
			this.dialog.setCancelable(false);
			this.listener = listener;
			this.account = acc;
			this.userfile = photo;
			this.isShowDialog = isShowDialog;
		}

		protected void onPreExecute() {
			if (isShowDialog)
				dialog.show();
		}

		protected String doInBackground(String... url) {

			return createCategory(this.act, account, userfile, url[0]);

		}

		protected void onPostExecute(String result) {
			dialog.dismiss();
			listener.onSuccess(result);
		}

	}

	public static String createCategory(Activity act, CategoryObj acc,
			Bitmap photo, String url) {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(url);
			byte[] data = null;
			ByteArrayBody bab = null;

			ByteArrayOutputStream bos;
			bos = new ByteArrayOutputStream();
			if (photo != null) {
				String filename = "iconmember"
						+ String.valueOf(Calendar.getInstance()
								.getTimeInMillis()) + ".png";
				photo.compress(CompressFormat.PNG, 100, bos);
				data = bos.toByteArray();
				bab = new ByteArrayBody(data, filename);
				long size = data.length;
			}

			MultipartEntity reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			if (photo == null) {
				System.out.println("no_image: " + "true");
				reqEntity.addPart("image", new StringBody(""));
			} else {
				System.out.println("no_image: " + "false");
				reqEntity.addPart("image", bab);
			}
			if (acc.getCategoryId() != null && !acc.getCategoryId().isEmpty()) {
				reqEntity.addPart("category_id",
						new StringBody(acc.getCategoryId()));
				System.out.println("category_id : " + acc.getCategoryId());
				Log.d("category_id", "category_id" + acc.getCategoryId());
			}
			reqEntity.addPart("category_name",
					new StringBody(acc.getCategoryName()));

			postRequest.setEntity(reqEntity);
			HttpResponse response2 = httpClient.execute(postRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response2.getEntity().getContent(), "UTF-8"));
			String sResponse;
			StringBuilder s = new StringBuilder();
			while ((sResponse = reader.readLine()) != null) {
				s = s.append(sResponse);
			}
			String str = s.toString();
			return str;

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	// create deals
	public static class ProcessDealsDoInBackground extends
			AsyncTask<String, String, String> {

		private CustomProgressDialog dialog;
		private DealObj deal;
		private Bitmap userfile;
		private Activity act;
		private boolean isShowDialog;
		ModelManagerListener listener;

		public ProcessDealsDoInBackground(Activity act, DealObj deal,
				Bitmap photo, boolean isShowDialog,
				ModelManagerListener listener) {
			this.act = act;
			this.dialog = new CustomProgressDialog(this.act);
			this.dialog.setCancelable(false);
			this.listener = listener;
			this.deal = deal;
			this.userfile = photo;
			this.isShowDialog = isShowDialog;
		}

		protected void onPreExecute() {
			if (isShowDialog)
				dialog.show();
		}

		protected String doInBackground(String... url) {

			return createDeals(this.act, deal, userfile, url[0]);

		}

		protected void onPostExecute(String result) {
			dialog.dismiss();
			listener.onSuccess(result);
		}

	}

	public static String createDeals(Activity act, DealObj deal, Bitmap photo,
			String url) {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(url);
			byte[] data = null;
			ByteArrayBody bab = null;

			ByteArrayOutputStream bos;
			bos = new ByteArrayOutputStream();
			if (photo != null) {
				String filename = "iconmember"
						+ String.valueOf(Calendar.getInstance()
								.getTimeInMillis()) + ".jpg";
				photo.compress(CompressFormat.JPEG, 100, bos);
				data = bos.toByteArray();
				bab = new ByteArrayBody(data, filename);
			}

			MultipartEntity reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			if (photo == null) {
				System.out.println("no_image: " + "true");
				reqEntity.addPart("image", new StringBody(""));
			} else {
				System.out.println("no_image: " + "false");
				reqEntity.addPart("image", bab);
			}
			if (deal.getDeal_id() != null && !deal.getDeal_id().isEmpty()) {
				reqEntity.addPart("deal_id", new StringBody(deal.getDeal_id()));
				System.out.println("user id : " + deal.getDeal_id());
				Log.d("userid", "userid" + deal.getDeal_id());
			}
			reqEntity.addPart("title", new StringBody(deal.getTitle()));
			reqEntity.addPart("company", new StringBody(deal.getCompany()));
			reqEntity.addPart("featured",
					new StringBody(Integer.toString(deal.getFeatured())));
			reqEntity
					.addPart("category", new StringBody(deal.getCategory_id()));
			reqEntity.addPart("latitude",
					new StringBody(Double.toString(deal.getLatitude())));
			reqEntity.addPart("longitude",
					new StringBody(Double.toString(deal.getLongitude())));
			reqEntity
					.addPart("startDate", new StringBody(deal.getStart_date()));
			reqEntity.addPart("endDate", new StringBody(deal.getEnd_date()));
			reqEntity
					.addPart("startTime", new StringBody(deal.getStart_time()));
			reqEntity.addPart("endTime", new StringBody(deal.getEnd_time()));
			reqEntity.addPart("address", new StringBody(deal.getAddress()));
			reqEntity.addPart("city", new StringBody(deal.getCity()));
			reqEntity.addPart("county", new StringBody(deal.getCounty()));
			reqEntity.addPart("postcode", new StringBody(deal.getPostcode()));
			reqEntity.addPart("country", new StringBody(deal.getCountry()));
			reqEntity.addPart("startValue",
					new StringBody(Double.toString(deal.getStart_value())));
			reqEntity.addPart("discount",
					new StringBody(Integer.toString(deal.getDiscount())));
			reqEntity.addPart(
					"after_discount_value",
					new StringBody(Double.toString(deal
							.getAfter_discount_value())));
			reqEntity.addPart("saveValue",
					new StringBody(Double.toString(deal.getSave_value())));
			reqEntity.addPart("dealUrl", new StringBody(deal.getDeal_url()));
			reqEntity.addPart("description",
					new StringBody(deal.getDescription()));
			reqEntity.addPart("username", new StringBody(deal.getUsername()));
			//reqEntity.addPart("tnc", new StringBody(deal.getTnc_id()));
			reqEntity.addPart("tnc", new StringBody(deal.getTnc_id()));
			reqEntity.addPart("notes", new StringBody(deal.getTnc_notes()));
            reqEntity.addPart("timezone", new StringBody(getTimeZone()));

			postRequest.setEntity(reqEntity);
			HttpResponse response2 = httpClient.execute(postRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response2.getEntity().getContent(), "UTF-8"));
			String sResponse;
			StringBuilder s = new StringBuilder();
			while ((sResponse = reader.readLine()) != null) {
				s = s.append(sResponse);
			}
			String str = s.toString();
			Log.d(TAG, "createDeals: "+str);
			return str;

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	private static String getTimeZone() {
        Calendar cal = Calendar.getInstance();

        long milliDiff = cal.get(Calendar.ZONE_OFFSET);
// Got local offset, now loop through available timezone id(s).

        String [] ids = TimeZone.getAvailableIDs();
        String name = null;

        for (String id : ids) {
            TimeZone tz = TimeZone.getTimeZone(id);
            if (tz.getRawOffset() == milliDiff) {
                // Found a match.
                name = id;
                break;
            }
        }

        return name;
    }

	public static void getListOfTicketPaid(final Context context,
			String dealId, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_GET_LIST_OF_TICKET;

		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createDealIdParams(context, dealId);

		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);

						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	//

	public static void getListNotifyBox(final Context context,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_GET_LIST_NOTIFYBOX;

		final DatabaseUtility databaseUtility = new DatabaseUtility();
		if (!databaseUtility.checkExistsApi(context, url)) {
			APIObj apiInfo = new APIObj();
			Log.d(TAG, "chua co du lieu");
			// add vao database
			apiInfo.setmApi(url);
			if (databaseUtility.insertApi(context, apiInfo)) {
				Log.d(TAG, "INSERT thanh cong");
			} else {
				Log.d(TAG, "INSERT khong  thanh cong");
			}

		} else {
			Log.d(TAG, "data da co du lieu");
		}

		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);

							if (databaseUtility.updateResuft_Api(context,
									response, url)) {
								Log.d(TAG, "update thanh cong");
							} else {
								Log.d(TAG, "update khong thanh cong");
							}

						} else
							listener.onError();
					}
				}, new ArrayList<NameValuePair>(), isProgess);
		post.execute(url);
	}

	public static void getSuperAdmin(final Context context, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_GET_SUPER_DASHBOARD;

		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, new ArrayList<NameValuePair>(), isProgess);
		post.execute(url);
	}

	public static void getNormalAdminDashboard(final Context context,
			String userName, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_GET_NORMAL_ADMIN_DASHBOARD;
		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createNormalAdminParams(context, userName);

		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	public static void getListOfAdmin(final Context context, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_GET_LIST_OF_ADMIN;

		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, new ArrayList<NameValuePair>(), isProgess);
		post.execute(url);
	}

	public static void activeAdmin(final Context context, String userName,
			String active, boolean isProgess,
			final ModelManagerListener listener) {
		final String url = UserFunctions.URL_ACTIVE_ADMIN;

		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createActiveAdminParams(context, userName, active);

		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	public static void getEventDashboard(final Context context, String dealId,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_EVENT_DASHBOARD;

		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createDealIdParams(context, dealId);

		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);
						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	public static void getListOfWinner(final Context context, String dealId,
			boolean isProgess, final ModelManagerListener listener) {
		final String url = UserFunctions.URL_SHOW_LUCKY;

		ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
				.createDealIdParams(context, dealId);

		AsyncHttpGet post = new AsyncHttpGet(context,
				new AsyncHttpResponseProcess() {
					@Override
					public void processIfResponseSuccess(String response) {
						if (response != null) {
							listener.onSuccess(response);

						} else
							listener.onError();
					}
				}, params, isProgess);
		post.execute(url);
	}

	public static void getUpcomingDeals(final Context context, String distance,
			String lat, String lng, boolean isProgess,
			final ModelManagerListener listener) {
		try{
			final String url = UserFunctions.URL_GET_UPCOMING_DEAL;
	
			ArrayList<NameValuePair> params = (ArrayList<NameValuePair>) ParameterFactory
					.createUpcomingDealParams(context, distance, lat, lng);
	
			final DatabaseUtility databaseUtility = new DatabaseUtility();
			if (!databaseUtility.checkExistsApi(context, url)) {
				APIObj apiInfo = new APIObj();
				Log.d(TAG, "Chua co du lieu");
				// add vao database
				apiInfo.setmApi(url);
				if (databaseUtility.insertApi(context, apiInfo)) {
					Log.d(TAG, "INSERT thanh cong");
				} else {
					Log.d(TAG, "INSERT khong  thanh cong");
				}
	
			} else {
				Log.d(TAG, "DATA" + "data da co du lieu");
			}
	
			AsyncHttpGet post = new AsyncHttpGet(context,
					new AsyncHttpResponseProcess() {
						@Override
						public void processIfResponseSuccess(String response) {
							if (response != null) {
								listener.onSuccess(response);
	
								if (databaseUtility.updateResuft_Api(context,
										response, url)) {
									Log.d("UPDATE", "update " + "update thanh cong");
								} else {
									Log.d("UPDATE", "update "
											+ "update khong thanh cong");
								}
	
							} else
								listener.onError();
						}
					}, params, isProgess);
			post.execute(url);
		} catch(Exception ex){
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
	}
}