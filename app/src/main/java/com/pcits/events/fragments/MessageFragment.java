package com.pcits.events.fragments;

import java.lang.reflect.Field;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityHome;
import com.pcits.events.R;
import com.pcits.events.adapters.TabMessageAdapter;
import com.pcits.events.widgets.KenBurnsView;
import com.pcits.events.widgets.PagerSlidingTabStrip;

public class MessageFragment extends Fragment {

	// tab view
	private PagerSlidingTabStrip tabs;
	private ViewPager viewPager;
	// adapter
	private TabMessageAdapter adapter;
	private View v;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		v = inflater.inflate(R.layout.fragment_message, container, false);

		initUI();
		initControl();
		initNotBoringActionBar();
		return v;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		try {
			Field f = Fragment.class.getDeclaredField("mChildFragmentManager");
			f.setAccessible(true);
			f.set(this, null);
		} catch(Exception ex){
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
	}

	private void initNotBoringActionBar() {
		mHeaderPicture = (KenBurnsView) v.findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}

	private void initUI() {
		llMenu = (LinearLayout) v.findViewById(R.id.llMenu);
		viewPager = (ViewPager) v.findViewById(R.id.pager);
		tabs = (PagerSlidingTabStrip) v.findViewById(R.id.tabs);
		adapter = new TabMessageAdapter(getFragmentManager(),
				getActivity());
		adapter.notifyDataSetChanged();
		viewPager.setAdapter(adapter);
		viewPager.getAdapter().notifyDataSetChanged();
		viewPager.setOffscreenPageLimit(3);
		tabs.setViewPager(viewPager);
		tabs.setTextSize(25);
		tabs.setTextColorResource(R.color.white);
		tabs.notifyDataSetChanged();
	}

	private void initControl() {

		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// ActivityHome.resideMenu.openMenu();
				//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
				Intent intent=new Intent(getActivity(),ActivityHome.class);
				getActivity().startActivity(intent);
				getActivity().finish();
			}
		});
	}
}
