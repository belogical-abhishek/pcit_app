package com.pcits.events.adapters;

import java.text.ParseException;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.obj.DealObj;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.utils.Utils;

public class HomeAdapter extends BaseAdapter {

	private static final String TAG = "HomeAdapter";

	private Activity activity;
	private ArrayList<DealObj> arrDeal;
	private LayoutInflater inflater = null;

	private AQuery listAq;

	// Get width/height
	private Display display;
	private int mImgHeight = 0;

	public HomeAdapter(Activity a, ArrayList<DealObj> deals) {
		activity = a;
		arrDeal = deals;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		listAq = new AQuery(a);

		display = a.getWindowManager().getDefaultDisplay();
		mImgHeight = display.getWidth() * 9 / 16;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return arrDeal.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("SimpleDateFormat")
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_home, null);
			holder = new ViewHolder();

			// Connect views object and views id on xml
			holder.lblTitle = (TextView) convertView
					.findViewById(R.id.lblTitle);
			holder.lblCompany = (TextView) convertView
					.findViewById(R.id.lblCompany);
			holder.lblCompany.setSelected(true);
			holder.lblStartValue = (TextView) convertView
					.findViewById(R.id.lblStartValue);
			holder.lblAfterDiscValue = (TextView) convertView
					.findViewById(R.id.lblAfterDiscountValue);
			holder.lblDate = (TextView) convertView.findViewById(R.id.lblDate);
			holder.lblTime = (TextView) convertView.findViewById(R.id.lblTime);
			holder.lblAttend = (TextView) convertView
					.findViewById(R.id.lblAttending);

			holder.imgThumbnail = (ImageView) convertView
					.findViewById(R.id.imgThumbnail);

			holder.rlQuantity = (RelativeLayout) convertView
					.findViewById(R.id.rlQuantity);
			holder.lblQuantity = (TextView) convertView
					.findViewById(R.id.lblQuantity);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final DealObj deal = arrDeal.get(position);

		if (deal != null) {
			// Set data to textview and imageview
			holder.imgThumbnail.getLayoutParams().width = display.getWidth();
			holder.imgThumbnail.getLayoutParams().height = mImgHeight;
			AQuery aq = listAq.recycle(convertView);
			aq.id(holder.imgThumbnail).image(
					UserFunctions.URLAdmin + deal.getImage(), false, true, 300,
					0, new BitmapAjaxCallback() {
						@Override
						protected void callback(String url, ImageView iv,
								Bitmap bm, AjaxStatus status) {
							if (bm != null) {
								// holder.imgThumbnail.setImageBitmap(bm);

								BitmapDrawable bmd = new BitmapDrawable(bm);
								holder.imgThumbnail.setBackgroundDrawable(bmd);
							}
						}
					});

			holder.lblTitle.setText(deal.getTitle());
			holder.lblTitle.setSelected(true);
			holder.lblCompany.setText("By: " + deal.getCompany());
			holder.lblStartValue.setText(Utils.mCurrency + " "
					+ deal.getStart_value());
			holder.lblAfterDiscValue.setText(deal.getAfter_discount_value()
					+ "");
			String price = holder.lblAfterDiscValue.getText().toString();
			if (price.equals("0")) {
				holder.lblAfterDiscValue.setText("Free");
			} else {
				holder.lblAfterDiscValue.setText(Utils.mCurrency + " " + price);
			}


				holder.lblDate.setText( deal.getStart_date()
                        + " - " +
                        deal.getEnd_date());

			holder.lblAttend.setText(deal.getAttend() + "");
			String start[]  =deal.getStart_time().split(":");
			String end[]  =deal.getEnd_time().split(":");
			holder.lblTime.setText(start[0]+":"+start[1]+" - "+end[0]+":"+end[1]);
		/*	holder.lblTime.setText(DateTimeUtility.convertTimeStampToDate(
					deal.getStartTimeStamp(), "HH:mm")
					+ " - "
					+ DateTimeUtility.convertTimeStampToDate(
							deal.getEndTimeStamp(), "HH:mm"));*/
			holder.lblStartValue.setPaintFlags(holder.lblStartValue
					.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


				if (deal.getIsHot()==0) {
					holder.rlQuantity.setVisibility(View.GONE);
				} else {
					holder.rlQuantity.setVisibility(View.VISIBLE);
					holder.lblQuantity.setText(""+deal.getViewCount());
				}


		}

		return convertView;
	}

	// Method to create instance of views
	static class ViewHolder {
		ImageView imgThumbnail;
		TextView lblTitle, lblCompany, lblDate, lblAfterDiscValue,
				lblStartValue, lblTime, lblAttend, lblQuantity;
		int position;
		RelativeLayout rlQuantity;
	}
}