package com.pcits.events;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.pcits.common.utils.Logging;
import com.pcits.events.config.ConstantKeyAds;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.obj.NotifyBoxObj;

public class GCMIntentService extends IntentService {
	private final String TAG = "GCMIntentService";

	private final int NOTIFICATION_ID = 1;

	public GCMIntentService() {
		super(ConstantKeyAds.SENDER_ID);
	}
//
//	@Override
//	protected void onError(Context arg0, String arg1) {
//		Log.e(TAG, "onError: " + arg1);
//	}
//
//	@Override
//	protected void onMessage(Context context, Intent data) {
//		Log.e(TAG, "onMessage: " + data);
//		try {
//			sendNotification(context, data);
//		} catch (Exception ex) {
//			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
//		}
//		// Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
//		// // Vibrate for 1 seconds
//		// v.vibrate(1000);
//
//		try {
//			Uri notification = RingtoneManager
//					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//			Ringtone r = RingtoneManager.getRingtone(getApplicationContext(),
//					notification);
//			r.play();
//		} catch (Exception ex) {
//			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
//		}
//	}
//
//	@Override
//	protected void onRegistered(Context context, String registrationId) {
//		Log.e(TAG, "onRegistered: " + registrationId);
//		// new GcmManager(context).setRegistrationId(context, registrationId);
//		// if (!StringUtility.isEmpty(registrationId)) {
//		// ModelManager.registerDevice(this, "", registrationId);
//		// }
//	}
//
//	@Override
//	protected void onUnregistered(Context arg0, String arg1) {
//		Log.e(TAG, "onUnregistered:  " + arg1);
//	}
//
//	// Put the GCM message into a notification and post it.
//	private void sendNotification(final Context context, Intent data) {
//		String message = "", code = "", type = "";
//		try {
//			message = data.getStringExtra("message");
//			code = data.getStringExtra("code");
//			type = data.getStringExtra("type");
//
//			// Push for create or change event.
//			NotificationManager mNotificationManager = (NotificationManager) context
//					.getSystemService(Context.NOTIFICATION_SERVICE);
//			Intent intent = new Intent(context, ActivityHome.class);
//
//			// intent.putExtra("pushNotifycation", true);
//			ActivityHome.hasPush = true;
//
//			Log.e(TAG, "Type: " + type);
//			if (type.equalsIgnoreCase("event")) {
//				// intent.putExtra("code", code);
//				ActivityHome.DEAL_ID_TO_PUSH = code;
//				Log.e(TAG, "Code: " + code);
//
//				// Insert notification into db.
//				try {
//					JSONObject json = new JSONObject(
//							data.getStringExtra("eventObj"));
//					new InsertNotificationIntoDB(context).execute(json);
//					Log.e(TAG, "Event inserted into db: " + json.toString());
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			} else if (type.equalsIgnoreCase("winner")) {
//				Log.e(TAG, "Winner");
//				// intent.putExtra("code", "winner");
//				ActivityHome.DEAL_ID_TO_PUSH = "winner";
//			}
//
//			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//					| Intent.FLAG_ACTIVITY_SINGLE_TOP);
//
//			PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
//					intent, PendingIntent.FLAG_CANCEL_CURRENT);
//			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
//					context)
//					.setSmallIcon(R.drawable.eventslogo)
//					.setContentTitle(context.getString(R.string.app_name))
//					.setStyle(
//							new NotificationCompat.BigTextStyle()
//									.bigText(message)).setContentText(message)
//					.setAutoCancel(true);
//			mBuilder.setContentIntent(contentIntent);
//			mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
//		} catch (Exception ex) {
//			throw new RuntimeException(ex);
//		}
//	}

	@Override
	protected void onHandleIntent(@Nullable Intent intent) {

	}

	class InsertNotificationIntoDB extends AsyncTask<JSONObject, Void, Void> {

		Context ctx;

		public InsertNotificationIntoDB(Context ctx) {
			this.ctx = ctx;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(JSONObject... params) {
			// TODO Auto-generated method stub
			JSONObject obj = params[0];
			NotifyBoxObj notifyObj = new NotifyBoxObj();
			try {
				notifyObj.setType_id(obj.getInt("type_id"));
				notifyObj.setSummary(obj.getString("summary"));
				notifyObj.setDate(obj.getString("date"));
				notifyObj.setStatus_id(obj.getString("status_id"));
				notifyObj.setTitle(obj.getString("title"));
			} catch (JSONException ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			}

			if (DatabaseUtility.insertNotification(ctx, notifyObj)) {
				Log.e(TAG, "Insert into notification successfully.");
				ActivityHome.hasNewMessage = true;
			} else {
				Log.e(TAG, "Insert into notification fail.");
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}
	}
}
