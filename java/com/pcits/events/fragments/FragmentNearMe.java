package com.pcits.events.fragments;

import java.util.ArrayList;

import org.json.JSONObject;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.pcits.common.utils.Logging;
import com.pcits.common.utils.RuntimePermissions;
import com.pcits.events.ActivityDetail;
import com.pcits.events.ActivityHome;
import com.pcits.events.AddEventActivity;
import com.pcits.events.R;
import com.pcits.events.adapters.HomeAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.libraries.JSONParser;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.DealObj;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class FragmentNearMe extends Fragment implements LocationListener, OnMapReadyCallback {

    private static final String TAG = "FragmentNearMe";

    private ArrayList<DealObj> mArrDeals;

    // Declare object of userFunctions and Utils class
    private UserFunctions userFunction;
    private Utils utils;

    // Create instance of list and ListAdapter
    private ListView list;
    private HomeAdapter mla;
    // flag for current page
    private JSONObject json;

    private View v;
    private SeekBar seeBar;
    private TextView txtKM, txtEventNotFound;
    private double lat;
    private double lnt;
    private LocationManager locationManager;
    private GoogleMap map;
    private MapView mapView;
    private String distance;
    private LatLng mLatLng;
    private Activity self;

    private DealObj mDealObj;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        super.onCreateView(inflater, container, savedInstanceState);

        lat = GlobalValue.latitude;
        lnt = GlobalValue.longitude;

        self = getActivity();
        v = inflater.inflate(R.layout.fragment_near_me, container, false);
        mapView = v.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        if (mArrDeals == null) {
            mArrDeals = new ArrayList<>();
        }

        userFunction = new UserFunctions();
        utils = new Utils(getActivity());

        initUI();
        initControl();

        return v;

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        mDealObj = null;
    }

    private void initUI() {

        list = (ListView) v.findViewById(R.id.lsvNearMe);
        txtKM = (TextView) v.findViewById(R.id.txtKM);
        txtEventNotFound = (TextView) v.findViewById(R.id.event_not_found);
        seeBar = (SeekBar) v.findViewById(R.id.seekBar);
        seeBar.setProgress(5);
        txtKM.setText(seeBar.getProgress() + "");
        distance = txtKM.getText().toString().trim();
        new loadFirstListView().execute();

        list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub

                DealObj dealObj = mArrDeals.get(position);

                DatabaseUtility.insertDeals(getActivity(),
                        dealObj);

                GlobalValue.dealsObj = dealObj;

                Log.d("", "DATA " + "Successfully");

                ModelManager.updateClickCount(getActivity(),
                        mArrDeals.get(position).getDeal_id(), true,
                        new ModelManagerListener() {

                            @Override
                            public void onSuccess(Object object) {
                                // TODO Auto-generated method stub
                                String json = (String) object;
                                boolean result = ParserUtility
                                        .updateAccount(json);
                                if (result) {
                                    Log.d(TAG, "Successfully");
                                }
                            }

                            @Override
                            public void onError() {
                                // TODO Auto-generated method stub

                            }
                        });

                Intent i = new Intent(getActivity(), ActivityDetail.class);
//                i.putExtra(utils.EXTRA_DEAL_ID, mArrDeals.get(position)
//                        .getDeal_id());
                startActivity(i);
                getActivity().overridePendingTransition(
                        R.anim.slide_in_left, R.anim.slide_out_left);

                list.setItemChecked(position, true);
            }
        });
        list.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent,
                                           View view, int position, long id) {
                // TODO Auto-generated method stub
                if (GlobalValue.myUser != null) {
                    try {
                        mDealObj = (DealObj) mArrDeals.get(position)
                                .clone();

                        showOperationDialog();
                    } catch (Exception ex) {
                        // TODO Auto-generated catch block
                        Logging.writeExceptionFromStackTrace(ex,
                                ex.getMessage());
                    }
                }
                return true;
            }
        });
    }

    private void initControl() {
        try {
            seeBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub
                    distance = txtKM.getText().toString().trim();
                    mArrDeals = new ArrayList<DealObj>();
                    new loadFirstListView().execute();

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,
                                              boolean fromUser) {
                    // TODO Auto-generated method stub
                    distance = "";
                    txtKM.setText(Integer.toString(progress));
                    distance = txtKM.getText().toString().trim();
                }
            });
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    LatLng CENTER = null;

    private void setUpMap() {
        try {
            MapsInitializer.initialize(getActivity());

            switch (GooglePlayServicesUtil
                    .isGooglePlayServicesAvailable(getActivity())) {
                case ConnectionResult.SUCCESS:
                    // Gets to GoogleMap from the MapView and does initialization
                    // stuff
                    if (mapView != null) {

                        locationManager = ((LocationManager) getActivity()
                                .getSystemService(Context.LOCATION_SERVICE));

                        Boolean localBoolean = Boolean.valueOf(locationManager
                                .isProviderEnabled("network"));

                        if (localBoolean.booleanValue()) {

                            CENTER = new LatLng(lat, lnt);

                        } else {

                        }
                        mapView.getMapAsync(this);


                        Criteria crit = new Criteria();

                        String provider = locationManager.getBestProvider(crit,
                                true);

                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        Location loc = locationManager
                                .getLastKnownLocation(provider);

                        if (loc != null) {
                            onLocationChanged(loc);
                        }
                        locationManager.requestLocationUpdates(provider, 20000, 0,
                                this);

                        // Stop requesting location.
                        if (locationManager != null) {
                            locationManager.removeUpdates(this);
                        }

                    }
                    break;
                case ConnectionResult.SERVICE_MISSING:

                    break;
                case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:

                    break;
                default:

            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady: ");
        map = googleMap;
        map.clear();
        map.setIndoorEnabled(true);


        if (RuntimePermissions.hasLocationPermission(self)) {

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            map.setMyLocationEnabled(true);
            try {
                setUpMap();

            } catch (Exception ex) {
                Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
                ex.printStackTrace();

            }
        }
    }

    // Load first 10 videos
    private class loadFirstListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            ActivityHome.rltProgress.setVisibility(View.VISIBLE);

            // Remove old data
            mArrDeals.clear();
        }

        protected Void doInBackground(Void... unused) {
            try {

                getDataFromServer();
            } catch (Exception ex) {
                Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
                ex.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void unused) {
            // Condition if data length uder 10 button loadMore is remove
            if (isAdded()) {
                if (mArrDeals.size() != 0) {
                    // Check paramter notif
                    list.setVisibility(View.VISIBLE);
                    // Getting adapter
                    mla = new HomeAdapter(getActivity(), mArrDeals);
                    mla.notifyDataSetChanged();
                    list.setAdapter(mla);

                } else {
                    // list.removeFooterView(btnLoadMore);
                    if (json != null) {

                    } else {
                        Toast.makeText(getActivity(),
                                getString(R.string.no_connection),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
            try {
                super.onPostExecute(unused);
                // Closing progress dialog

                // pDialog.dismiss();
                ActivityHome.rltProgress.setVisibility(View.GONE);
            } catch (NullPointerException ex) {
                throw new RuntimeException(ex);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    private double getDistance(double lat1, double lng1, double lat2, double lng2) {

        double earthRadius = 3958.75; // in miles, change to 6371 for kilometer output

        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);

        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);

        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        double dist = earthRadius * c;

        return dist; // output distance, in MILES
    }

    private void getDataFromServer() {
        try {
            json = userFunction.nearMe(Integer.parseInt(distance), lat, lnt);
            Log.d(TAG, "Near me: " + json.toString());

            if (json != null) {
                ArrayList<DealObj> arr = JSONParser.parserDeal(
                        UserFunctions.KEY_JSON_DATA, json.toString());

                Log.d(TAG, "Near me: " + arr.size());
                Log.d(TAG, "Near me: " + arr.isEmpty());

                if(!arr.isEmpty()) {
                    txtEventNotFound.setVisibility(View.INVISIBLE);

                    for(int i=0;i<arr.size();i++) {
                        double dlat = arr.get(i).getLatitude();
                        double dlon = arr.get(i).getLongitude();

                        double dist = getDistance(lat, lnt, dlat, dlon);

                        if(dist > Integer.parseInt(distance)) {
                            arr.remove(i);
                            i--;
                        }
                    }
                } else {
                    txtEventNotFound.setVisibility(View.VISIBLE);
                }

                mArrDeals.addAll(arr);
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        Log.d(TAG, "onLocationChanged: ");
        // TODO Auto-generated method stub
        lat = location.getLatitude();
        lnt = location.getLongitude();

        mLatLng = new LatLng(lat, lnt);

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 16));

        // mMaps.addMarker(new MarkerOptions().position(mLatLng)).setTitle(
        // "Current Location");

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }

    private void showOperationDialog() {
        final Dialog dialog = new Dialog(self);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_operation_event);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        TextViewRobotoCondensedRegular copy = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_copy);
        copy.setVisibility(View.GONE);
        TextViewRobotoCondensedRegular delete = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_delete);
        TextViewRobotoCondensedRegular revoke = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_revoke);
        TextViewRobotoCondensedRegular scan = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_scan);
        TextViewRobotoCondensedRegular edit = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_edit);

        // Set listener
        delete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(self, "Deleted", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        revoke.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(self, "Revoked", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        scan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(self, "Scanned", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        edit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Open edit event page
                // Intent i = new Intent(self, ActivityEditDeal.class);
                AddEventActivity.isDeal = true;
                GlobalValue.dealsObj = mDealObj;
                Intent i = new Intent(getActivity(), AddEventActivity.class);
                startActivity(i);
                // Dismiss dialog
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
