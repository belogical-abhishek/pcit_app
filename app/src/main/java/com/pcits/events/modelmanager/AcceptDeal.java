package com.pcits.events.modelmanager;

import com.google.gson.annotations.SerializedName;

/**
 * Created by EnvisioDevs on 04-01-2018.
 */

public class AcceptDeal {


    @SerializedName("client_id")
    private int clientId;
    @SerializedName("deal_id")
    private String dealId;

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }
}
