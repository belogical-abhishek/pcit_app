package com.pcits.events.modelmanager;

import com.google.gson.annotations.SerializedName;

/**
 * Created by EnvisioDevs on 29-01-2018.
 */

public class ClientToAdd {
    //	"name": "Krupal Solanki",
	//	"email": "krupal@envisiodevs.com

    @SerializedName("name")
    String name;

    @SerializedName("email")
    String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
