package com.pcits.events.adapters;

import android.app.Activity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.pcits.events.fragments.FragmentListPrivateEvents;
import com.pcits.events.R;
import com.pcits.events.fragments.FragmentHome;
import com.pcits.events.fragments.FragmentNearMe;
import com.pcits.events.fragments.FragmentPopular;
import com.pcits.events.fragments.UpcomingFragment;

import java.util.ArrayList;
import java.util.List;

public class TabsAdapter extends FragmentStatePagerAdapter {

	private String[] TITLES;
	private final List<Fragment> mFragmentList = new ArrayList<>();
	private final List<String> mFragmentTitleList = new ArrayList<>();

	public TabsAdapter(FragmentManager fm, Activity act) {
		super(fm);

		TITLES = new String[5];
		// TITLES[0] = act.getResources().getString(R.string.FEATURED);
		TITLES[0] = act.getResources().getString(R.string.AllEVENTS);
		TITLES[1] = act.getResources().getString(R.string.NEARME);
		TITLES[2] = act.getResources().getString(R.string.upcoming_caps);
		TITLES[3] = act.getResources().getString(R.string.POPULAR);
		TITLES[4]=  act.getResources().getString(R.string.PrivateEvents);
		// TITLES[5] = act.getResources().getString(R.string.MYTICKETS);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return TITLES[position];
	}

	public void addFragment(Fragment fragment, String title) {
		mFragmentList.add(fragment);
		mFragmentTitleList.add(title);
	}
	@Override
	public int getCount() {
		return TITLES.length;
	}

	@Override
	public Fragment getItem(int position) {
		return mFragmentList.get(position);
	/*	switch (position) {
		// case 0:
		// return new FragmentFeatured();
		case 0:
			return new FragmentHome();
		case 1:
			return new FragmentNearMe();
		case 2:
			return new UpcomingFragment();
		case 3:
			return new FragmentPopular();
		case 4:
			return new FragmentListPrivateEvents();
			// case 5:
			// return new FragmentMyTickets();
		default:
			throw new IllegalArgumentException(
					"The item position should be less: " + TITLES.length);
		}*/
		// return SuperAwesomeCardFragment.newInstance(position);
	}
}
