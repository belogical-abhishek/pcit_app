package com.pcits.events.database.binder;

import android.database.sqlite.SQLiteStatement;

import com.pcits.events.obj.DealObj;

public class DealsBinder implements ParameterBinder {

	@Override
	public void bind(SQLiteStatement st, Object object) {
		// TODO Auto-generated method stub
		DealObj dealObj = (DealObj) object;
		st.bindString(1, dealObj.getDeal_id());
		st.bindString(2, dealObj.getTitle());
		st.bindString(3, dealObj.getCompany());
		st.bindString(4, dealObj.getStart_date());
		st.bindString(5, dealObj.getEnd_date());
		st.bindString(6, dealObj.getStart_time());
		st.bindString(7, dealObj.getEnd_time());
		st.bindDouble(8, dealObj.getAfter_discount_value());
		st.bindDouble(9, dealObj.getStart_value());
		st.bindLong(10, dealObj.getAttend());
		st.bindString(11, dealObj.getImage());
		st.bindLong(12, dealObj.getQuantity());
		st.bindString(13, dealObj.getStartTimeStamp());
		st.bindString(14, dealObj.getEndTimeStamp());

	}
}
