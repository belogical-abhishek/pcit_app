package com.pcits.events.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityAddTnC;
import com.pcits.events.ActivityEditTnC;
import com.pcits.events.ActivityHome;
import com.pcits.events.R;
import com.pcits.events.adapters.AdapterTnCByUser;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.widgets.floatingactionbutton.AddFloatingActionButton;

public class FragmentTnC extends Fragment {

	private ListView list;
	private AdapterTnCByUser adapterTnCbyUser;
	private AddFloatingActionButton btnAddDeal;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private TextView lblTitleHeader;

	private View v;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		v = inflater.inflate(R.layout.layout_list_deals, container, false);
		try {
			initUI();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void initUI() {
		try {
			lblTitleHeader = (TextView) v.findViewById(R.id.lblTitleHeader);
			lblTitleHeader.setText("Events");
			llMenu = (LinearLayout) v.findViewById(R.id.llMenu);
			list = (ListView) v.findViewById(R.id.lsvDeals);
			btnAddDeal = (AddFloatingActionButton) v
					.findViewById(R.id.btnAddDeal);
			llMenu.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// ActivityHome.resideMenu.openMenu();
					//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
					Intent intent=new Intent(getActivity(),ActivityHome.class);
					startActivity(intent);
					getActivity().finish();
				}
			});
			// btnAddDeal.setTitle("Add New TNC");
			btnAddDeal.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(getActivity(), ActivityAddTnC.class);
					startActivity(i);
				}
			});
			if (Integer.parseInt(GlobalValue.myUser.getRole()) == 0) {
				getListAllTnc();
			} else {
				getListTnc();
			}
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					GlobalValue.tncObj = GlobalValue.arrTnc.get(position);
					Intent i = new Intent(getActivity(), ActivityEditTnC.class);
					startActivity(i);
				}
			});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	// list tnc by user
	private void getListTnc() {
		try {
			ModelManager.getListTncByUser(getActivity(),
					GlobalValue.myUser.getUsername(), true,
					new ModelManagerListener() {

						@Override
						public void onSuccess(Object object) {
							// TODO Auto-generated method stub
							String json = (String) object;
							Log.e("FragmentTnC", "Result: " + json);
							GlobalValue.arrTnc = ParserUtility.getListTnc(json);
							if (GlobalValue.arrTnc.size() > 0) {
								adapterTnCbyUser = new AdapterTnCByUser(
										getActivity(), GlobalValue.arrTnc);
								adapterTnCbyUser.notifyDataSetChanged();
								list.setAdapter(adapterTnCbyUser);
							} else {
								Toast.makeText(getActivity(), "No Data",
										Toast.LENGTH_SHORT).show();
							}
						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}
					});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	// list all tnc
	private void getListAllTnc() {
		try {
			ModelManager.getListAllTnC(getActivity(), true,
					new ModelManagerListener() {

						@Override
						public void onSuccess(Object object) {
							// TODO Auto-generated method stub
							String json = (String) object;
							GlobalValue.arrTnc = ParserUtility.getListTnc(json);
							if (GlobalValue.arrTnc.size() > 0) {
								adapterTnCbyUser = new AdapterTnCByUser(
										getActivity(), GlobalValue.arrTnc);
								adapterTnCbyUser.notifyDataSetChanged();
								list.setAdapter(adapterTnCbyUser);
							} else {
								Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT).show();
							}
						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}
					});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
}
