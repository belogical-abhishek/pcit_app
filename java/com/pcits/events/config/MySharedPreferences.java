/*
 * Name: $RCSfile: MeyClubSharedPreferences.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Oct 31, 2011 2:07:45 PM $
 *
 * Copyright (C) 2011 COMPANY_NAME, Inc. All rights reserved.
 */

package com.pcits.events.config;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * MeyClubSharedPreferences class which saves setting values
 * 
 * @author FruitySolution
 */
@SuppressLint("NewApi")
public class MySharedPreferences {

	private String TAG = getClass().getSimpleName();
	public static final String ALBERTO_PREFERENCES = "ALBERTO_PREFERENCES";
	public static final String APPLICATION_INSTALL_FIRST_TIME = "APPLICATION_INSTALL_FIRST_TIME";
	public static final String SITE_URL = "SITE_URL";
	public static final String PROPERTY_ID = "PROPERTY_ID";
	public static final String COUNTRY_KEY = "COUNTRY";
	public static final String CUISINE_KEY = "CUISINE";
	private static final String USER_PASSWORD = "USER_PASSWORD";
	private static final String SESSION_KEY = "SESSION_KEY";
	public static final String CLIENT_USER_KEY = "CLIENT_USER_KEY";
	public static final String ADMIN_USER_KEY = "ADMIN_USER_KEY";

	// ================================================================

	private static MySharedPreferences instance;

	private Context context;

	private MySharedPreferences() {

	}

	/**
	 * Constructor
	 * 
	 * @param context
	 * @return
	 */
	public static MySharedPreferences getInstance(Context context) {
		if (instance == null) {
			instance = new MySharedPreferences();
			instance.context = context;

		}

		return instance;
	}

	// ======================== UTILITY FUNCTIONS ========================

	// URL Image for getVisuals

	public void setIsFirstInstall() {
		putBooleanValue(APPLICATION_INSTALL_FIRST_TIME, true);
	}

	public boolean getIsFirstInstall() {
		return getBooleanValue(APPLICATION_INSTALL_FIRST_TIME);
	}

	public void setUser(String userInfo) {
		putStringValue(USER_PASSWORD, userInfo);
	}

	public String getUser() {
		return getStringValue(USER_PASSWORD, "");
	}

	public void setCountryKey(String country) {
		putStringValue(COUNTRY_KEY, country);
	}

	public String getCountryKey() {
		return getStringValue(COUNTRY_KEY, "");
	}

	public String getSessionId() {
		return getStringValue(SESSION_KEY, "");
	}

	public void setSessionId(String sessionId) {
		putStringValue(SESSION_KEY, sessionId);
	}

	// ======================== CORE FUNCTIONS ========================

	/**
	 * Save a long integer to SharedPreferences
	 * 
	 * @param key
	 * @param n
	 */
	public void putLongValue(String key, long n) {
		// SmartLog.log(TAG, "Set long integer value");
		SharedPreferences pref = context.getSharedPreferences(
				ALBERTO_PREFERENCES, 0);
		SharedPreferences.Editor editor = pref.edit();
		editor.putLong(key, n);
		editor.commit();
	}

	/**
	 * Read a long integer to SharedPreferences
	 * 
	 * @param key
	 * @return
	 */
	public long getLongValue(String key) {
		// SmartLog.log(TAG, "Get long integer value");
		SharedPreferences pref = context.getSharedPreferences(
				ALBERTO_PREFERENCES, 0);
		return pref.getLong(key, 0);
	}

	/**
	 * Save an integer to SharedPreferences
	 * 
	 * @param key
	 * @param n
	 */
	public void putIntValue(String key, int n) {
		// SmartLog.log(TAG, "Set integer value");
		SharedPreferences pref = context.getSharedPreferences(
				ALBERTO_PREFERENCES, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor =pref.edit();
		editor.putInt(key, n);
		editor.apply();
	}

	/**
	 * Read an integer to SharedPreferences
	 * 
	 * @param key
	 * @return
	 */
	public int getIntValue(String key) {
		// SmartLog.log(TAG, "Get integer value");
		SharedPreferences pref = context.getSharedPreferences(
				ALBERTO_PREFERENCES, Context.MODE_PRIVATE);
		return pref.getInt(key, 0);
	}

	/**
	 * Save an string to SharedPreferences
	 * 
	 * @param key
	 * @param s
	 */
	public void putStringValue(String key, String s) {
		// SmartLog.log(TAG, "Set string value");
		SharedPreferences pref = context.getSharedPreferences(
				ALBERTO_PREFERENCES, 0);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(key, s);
		editor.apply();
	}

	/**
	 * Read an string to SharedPreferences
	 * 
	 * @param key
	 * @return
	 */
	public String getStringValue(String key) {
		// SmartLog.log(TAG, "Get string value");
		SharedPreferences pref = context.getSharedPreferences(
				ALBERTO_PREFERENCES, 0);
		return pref.getString(key, "");
	}

	/**
	 * Read an string to SharedPreferences
	 * 
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public String getStringValue(String key, String defaultValue) {
		// SmartLog.log(TAG, "Get string value");
		SharedPreferences pref = context.getSharedPreferences(
				ALBERTO_PREFERENCES, 0);
		return pref.getString(key, defaultValue);
	}

	/**
	 * Save an boolean to SharedPreferences
	 * 
	 * @param key
	 * @param s
	 */
	public void putBooleanValue(String key, Boolean b) {
		// SmartLog.log(TAG, "Set boolean value");
		SharedPreferences pref = context.getSharedPreferences(
				ALBERTO_PREFERENCES, 0);
		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean(key, b);
		editor.apply();
	}

	/**
	 * Read an boolean to SharedPreferences
	 * 
	 * @param key
	 * @return
	 */
	public boolean getBooleanValue(String key) {
		// SmartLog.log(TAG, "Get boolean value");
		SharedPreferences pref = context.getSharedPreferences(
				ALBERTO_PREFERENCES, 0);
		return pref.getBoolean(key, false);
	}

	/**
	 * Save an float to SharedPreferences
	 * 
	 * @param key
	 * @param s
	 */
	public void putFloatValue(String key, float f) {
		SharedPreferences pref = context.getSharedPreferences(
				ALBERTO_PREFERENCES, 0);
		SharedPreferences.Editor editor = pref.edit();
		editor.putFloat(key, f);
		editor.apply();
	}

	/**
	 * Read an float to SharedPreferences
	 * 
	 * @param key
	 * @return
	 */
	public float getFloatValue(String key) {
		SharedPreferences pref = context.getSharedPreferences(
				ALBERTO_PREFERENCES, 0);
		return pref.getFloat(key, 0.0f);
	}

}
