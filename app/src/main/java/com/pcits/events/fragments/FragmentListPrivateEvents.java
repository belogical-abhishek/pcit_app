package com.pcits.events.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pcits.common.utils.GsonUtility;
import com.pcits.events.ActivityListEvents;
import com.pcits.events.R;
import com.pcits.events.adapters.ListEventsAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.network.EventsAPI;
import com.pcits.events.network.RestClient;
import com.pcits.events.obj.ListEventObj;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.pcits.events.libraries.UserFunctions.URLAdmin;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentListPrivateEvents#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentListPrivateEvents extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static final String TAG = "FrgListPrivateEvents";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView mRecyclerView;
    private ProgressDialog mProgressDialog;
    private OnFragmentInteractionListener mListener;
    SwipeRefreshLayout mSwipeRefreshLayout;
    public FragmentListPrivateEvents() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentListPrivateEvents.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentListPrivateEvents newInstance(String param1, String param2) {
        FragmentListPrivateEvents fragment = new FragmentListPrivateEvents();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
////        if (context instanceof OnFragmentInteractionListener) {
////            mListener = (OnFragmentInteractionListener) context;
////        } else {
////            throw new RuntimeException(context.toString()
////                    + " must implement OnFragmentInteractionListener");
////        }
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView= inflater.inflate(R.layout.fragment_fragment_list_private_events, container, false);
        mRecyclerView = (RecyclerView) mView.findViewById(R.id.rv_events);
        mProgressDialog=new ProgressDialog(getActivity());
        mSwipeRefreshLayout = (SwipeRefreshLayout) mView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        getEvents();
        return mView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        getEvents();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    public void getEvents()
    {
//        Log.d(TAG, "getEvents: "+ GlobalValue.myClient.getUsername());
        mSwipeRefreshLayout.setRefreshing(true);
        Retrofit retrofit = RestClient.retrofitService();
        EventsAPI eventsApiObj = retrofit.create(EventsAPI.class);
        mProgressDialog.setMessage("Getting Events");
        mProgressDialog.show();

        try {
            Log.d(TAG, "getEvents: ");

            if(GlobalValue.myClient!=null){
            Call<List<ListEventObj>> listeventCallobj = eventsApiObj.getEvents("events/"+Integer.valueOf(GlobalValue.myClient.getId()));
            listeventCallobj.enqueue(new Callback<List<ListEventObj>>() {
                @Override
                public void onResponse(Call<List<ListEventObj>> call, Response<List<ListEventObj>> response) {
                    mProgressDialog.dismiss();
                    if (response.isSuccessful())
                    {
                        List<ListEventObj> listevents = response.body();
                        List<ListEventObj> listeventsnew = new ArrayList<>();
                        for(int i=0;i<listevents.size();i++)
                        {
                            ListEventObj leo=listevents.get(i);
                            if(!leo.getStatus().equalsIgnoreCase("-1"))
                            {
                                if(!leo.getImage().contains("http"))
                                {
                                    leo.setImage(URLAdmin+leo.getImage());
                                }
                                listeventsnew.add(leo);
                            }
                        }
                        listevents.clear();
                        listevents=listeventsnew;
                        List<String> viewStatus=getStringArrayPref(getActivity().getBaseContext(),"Event_List_Status",listevents);
                        boolean fromHome=true;
                        ListEventsAdapter eventsadapterobj = new ListEventsAdapter(fromHome,viewStatus,listevents,getActivity().getBaseContext(),getActivity(),((GlobalValue.myClient!=null)?GlobalValue.myClient.getUsername():""));
                        eventsadapterobj.notifyDataSetChanged();
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getBaseContext());
                        mRecyclerView.setLayoutManager(layoutManager);
                        mRecyclerView.setHasFixedSize(true);
                        mRecyclerView.setAdapter(eventsadapterobj);
                        mSwipeRefreshLayout.setRefreshing(false);
                       // setStatusPrivateEvent(listevents);
                        if (listevents==null || listevents.isEmpty()){
                            Toast.makeText(getActivity().getBaseContext(),"No private events found",Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<ListEventObj>> call, Throwable throwable) {
                    mProgressDialog.dismiss();
                    throwable.printStackTrace();
                    mSwipeRefreshLayout.setRefreshing(false);

                }
            });

        } else{
                mProgressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity().getBaseContext(),"Please login to find your private events",Toast.LENGTH_LONG).show();
            }
        }catch (Exception e) {
            e.printStackTrace();
            mProgressDialog.dismiss();
            mSwipeRefreshLayout.setRefreshing(false);

        }
    }

    public List<String> getStringArrayPref(Context context, String key, List<ListEventObj> list)
    {
        List<String> listEventStatus=new ArrayList<>(list.size());
        for (int i = 0; i < list.size(); i++)
        {
            listEventStatus.add("0");
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = prefs.getString(key, null);
        List<ListEventObj> listEvents=new ArrayList<>();
        if (json != null)
        {
            try
            {
                JSONArray a = null;
                try
                {
                    a = new JSONArray(json);
                    for (int i = 0; i < list.size(); i++)
                    {
                        ListEventObj ss=list.get(i);
                        for(int j=0;j<a.length();j++)
                        {
                            JSONObject jsonObject=a.getJSONObject(j);
                            if(ss.getDealId().equalsIgnoreCase(jsonObject.getString("event_id")))
                            {
                                listEventStatus.set(i,ss.getDealId());
                            }
                            else
                            {
                                if(listEventStatus.get(i).equalsIgnoreCase("0"))
                                {
                                    listEventStatus.set(i,"0");
                                }
                            }

                        }
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return listEventStatus;
    }
}
