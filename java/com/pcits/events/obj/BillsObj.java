package com.pcits.events.obj;

/**
 * Created by SAKSHI on 10/24/2017.
 */

public class BillsObj {
    public String title;
    public String Amount;

    public String getAmount() {
        return Amount;
    }

    public String getTitle() {
        return title;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
