package com.pcits.events.obj;

public class AttendedObj {

	private String attendedId, dealId, tncId, statusId, username, clientname,
			deviceId;

	public String getAttendedId() {
		return attendedId;
	}

	public void setAttendedId(String attendedId) {
		this.attendedId = attendedId;
	}

	public String getDealId() {
		return dealId;
	}

	public void setDealId(String dealId) {
		this.dealId = dealId;
	}

	public String getTncId() {
		return tncId;
	}

	public void setTncId(String tncId) {
		this.tncId = tncId;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getClientname() {
		return clientname;
	}

	public void setClientname(String clientname) {
		this.clientname = clientname;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	
}
