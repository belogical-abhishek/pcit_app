package com.pcits.events;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.widgets.KenBurnsView;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedBold;

public class ActivityDealFeatured extends Activity {

	private TextView lblStartDate, lblEndDate;
	private Button btnSubmit;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;
	private TextView lblTitleHeader;
	private TextViewRobotoCondensedBold lblTotalCredit;
	private Calendar mCal;
	private String strStartDate, strEndDate, startDate, endDate;
	private SimpleDateFormat sdf_date, sdf_check;
	private int date;
	private String mDeal_id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_deal_featured);
		Intent i = getIntent();
		mDeal_id = i.getStringExtra("dealId_new");
		mCal = Calendar.getInstance();
		sdf_date = new SimpleDateFormat("yyyy-MM-dd");
		sdf_check = new SimpleDateFormat("yyyyMMdd");
		initUI();
		initControl();
		initNotBoringActionBar();
	}

	private void initNotBoringActionBar() {
		mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}

	private void initUI() {
		lblTitleHeader = (TextView) findViewById(R.id.lblTitleHeader);
		lblTitleHeader
				.setText(getResources().getString(R.string.deal_featured));
		llMenu = (LinearLayout) findViewById(R.id.llMenu);
		lblStartDate = (TextView) findViewById(R.id.lblStartDate);
		lblEndDate = (TextView) findViewById(R.id.lblEndDate);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);
		lblTotalCredit = (TextViewRobotoCondensedBold) findViewById(R.id.lblTotalCredit);
	}

	private void initControl() {

		//
		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		// start date
		lblStartDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				OnDateSetListener callBack = new OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						mCal.set(year, monthOfYear, dayOfMonth);
						Date currentDate = mCal.getTime();
						strStartDate = sdf_date.format(currentDate);
						startDate = sdf_check.format(currentDate);
						lblStartDate.setText(strStartDate);
					}
				};

				DatePickerDialog datePicker = new DatePickerDialog(
						ActivityDealFeatured.this, callBack, mCal
								.get(Calendar.YEAR), mCal.get(Calendar.MONTH),
						mCal.get(Calendar.DATE));
				datePicker.show();
			}
		});
		// end date

		lblEndDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				OnDateSetListener callBack = new OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						mCal.set(year, monthOfYear, dayOfMonth);
						Date currentDate = mCal.getTime();
						strEndDate = sdf_date.format(currentDate);
						endDate = sdf_check.format(currentDate);
						lblEndDate.setText(strEndDate);

						date = Integer.parseInt(endDate)
								- Integer.parseInt(startDate);
						if (date <= 0) {
							Toast.makeText(ActivityDealFeatured.this,
									"'end date' to greater 'start date'",
									Toast.LENGTH_SHORT).show();
						} else {
							lblTotalCredit.setText("To feature " + date
									+ " days will cost 1 x " + date + " = "
									+ date);
						}
					}
				};

				DatePickerDialog datePicker = new DatePickerDialog(
						ActivityDealFeatured.this, callBack, mCal
								.get(Calendar.YEAR), mCal.get(Calendar.MONTH),
						mCal.get(Calendar.DATE));
				datePicker.show();
			}
		});
		// submit
		btnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (date <= 0) {
					Toast.makeText(ActivityDealFeatured.this,
							"'end date' to greater 'start date'",
							Toast.LENGTH_SHORT).show();
				} else {
					ModelManager.adminCredits(ActivityDealFeatured.this, date,
							"admin4", true, new ModelManagerListener() {

								@Override
								public void onSuccess(Object object) {
									// TODO Auto-generated method stub
									String json = (String) object;

									Log.d("aaaaa", "result :" + json);

									Toast.makeText(ActivityDealFeatured.this,
											checkResult(json),
											Toast.LENGTH_SHORT).show();
								}

								@Override
								public void onError() {
									// TODO Auto-generated method stub
								}
							});
				}
			}
		});

	}

	private void updateFeatured() {
		ModelManager.updateFeatured(this, mDeal_id, strStartDate, strEndDate,
				1, true, new ModelManagerListener() {

					@Override
					public void onSuccess(Object object) {
						// TODO Auto-generated method stub
						String json = (String) object;

						Log.d("aaaaa", "result :" + json);
						Toast.makeText(ActivityDealFeatured.this,
								checkResult(json), Toast.LENGTH_SHORT).show();
						finish();
					}

					@Override
					public void onError() {
						// TODO Auto-generated method stub

					}
				});
	}

	protected String checkResult(String strJson) {
		// TODO Auto-generated method stub
		JSONObject json = null;
		String message = "";
		try {
			json = new JSONObject(strJson);
			if (json.getString("status").equals("success")) {
				message = this.getString(R.string.message_success);
				updateFeatured();
			} else {
				message = json.getString("message");
				new AlertDialog.Builder(ActivityDealFeatured.this)
						.setTitle(R.string.app_name)
						.setMessage("You need buy credits..!")
						.setPositiveButton(android.R.string.ok,
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										Intent i = new Intent(ActivityDealFeatured.this, BuyCreditActivity.class);
										startActivity(i);
									}
								})
						.setNegativeButton(android.R.string.cancel, null)
						.create().show();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return message;
	}
}
