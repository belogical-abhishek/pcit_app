package com.pcits.events.fragments;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityDetail;
import com.pcits.events.R;
import com.pcits.events.adapters.HistoryAdapter;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.obj.DealObj;
import com.pcits.events.utils.Utils;

public class FragmentRecent extends Fragment {

	private View v;
	private HistoryAdapter adapter;
	private ArrayList<DealObj> mArrHistory;
	private ListView lsvHistory;
	private Button btnClearHistory;
	private Utils utils;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		v = inflater.inflate(R.layout.fragment_history, container, false);
		try{
			utils = new Utils(getActivity());
			initUI();
			initControl();
			initData();
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		initData();
	}

	private void initUI() {
		lsvHistory = (ListView) v.findViewById(R.id.lsvHistory);
		btnClearHistory = new Button(getActivity());
		btnClearHistory
				.setBackgroundResource(R.drawable.apptheme_btn_default_holo_light);
		btnClearHistory.setText(getString(R.string.btn_clear_history));
		btnClearHistory.setTextColor(getResources().getColor(R.color.text_btn));
		lsvHistory.addFooterView(btnClearHistory);
		btnClearHistory.setVisibility(View.GONE);

	}

	private void initControl() {
		try{
			lsvHistory.setOnItemClickListener(new OnItemClickListener() {
	
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					DealObj deals = mArrHistory.get(position);
					Intent i = new Intent(getActivity(), ActivityDetail.class);
					i.putExtra(utils.EXTRA_DEAL_ID, deals.getDeal_id());
					// i.putExtra(utils.EXTRA_ATTENDING,
					// Integer.toString(deals.getAttend()));
					startActivity(i);
					getActivity().overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_left);
	
					lsvHistory.setItemChecked(position, true);
				}
			});
			// clear history
	
			btnClearHistory.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Clear History");
					builder.setMessage("Are you sure want to clear?");
					builder.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
	
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}
							});
					builder.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									DatabaseUtility.clearHistory(getActivity());
									lsvHistory.setAdapter(null);
									btnClearHistory.setVisibility(View.GONE);
									initData();
									Toast.makeText(getActivity(), "Successfully",
											Toast.LENGTH_SHORT).show();
									dialog.dismiss();
	
								}
							});
					builder.create().show();
				}
			});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void initData() {
		mArrHistory = DatabaseUtility.getListDeals(getActivity());
		adapter = new HistoryAdapter(getActivity(), mArrHistory);
		adapter.notifyDataSetChanged();
		lsvHistory.setAdapter(adapter);
		if (mArrHistory.size() <= 0) {
			btnClearHistory.setVisibility(View.GONE);
		} else {
			btnClearHistory.setVisibility(View.VISIBLE);
		}
	}

}
