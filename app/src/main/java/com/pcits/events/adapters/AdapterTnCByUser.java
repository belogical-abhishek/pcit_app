package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pcits.events.R;
import com.pcits.events.obj.TnCobj;

public class AdapterTnCByUser extends BaseAdapter {

	private ArrayList<TnCobj> arrTnC;
	private LayoutInflater mInflate;

	public AdapterTnCByUser(Activity activity, ArrayList<TnCobj> listTnC) {
		this.arrTnC = listTnC;
		this.mInflate = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrTnC.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {
		// TODO Auto-generated method stub
		final HolderView holder;
		if (convertView == null) {
			holder = new HolderView();
			convertView = mInflate.inflate(R.layout.item_list_tnc, null);
			holder.lblTnC_id = (TextView) convertView
					.findViewById(R.id.lblIdTnc);
			holder.lblDesc = (TextView) convertView.findViewById(R.id.lblDesc);
			holder.lblNotes = (TextView) convertView.findViewById(R.id.lblNote);
			convertView.setTag(holder);
		} else {
			holder = (HolderView) convertView.getTag();
		}
		final TnCobj o = arrTnC.get(position);
		if (o != null) {
			Log.e("AdapterTncByUser", "Desc: " + o.getNotes());
			holder.lblTnC_id.setText(o.getTnc_id());
			holder.lblDesc.setText(o.getTncDesc());
			holder.lblNotes.setText(o.getNotes());
			/*if(Build.VERSION.SDK_INT>=24)
			holder.lblNotes.setText(Html.fromHtml(o.getNotes(),Html.FROM_HTML_MODE_LEGACY));
			else holder.lblNotes.setText(Html.fromHtml(o.getNotes()));*/
		}
		return convertView;
	}

	public class HolderView {
		TextView lblTnC_id, lblDesc, lblNotes;
	}

}
