package com.pcits.events.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.pcits.events.R;
import com.pcits.events.listeners.OnSplitActionLisener;
import com.pcits.events.obj.ListUsersObj;
import com.pcits.events.obj.SplitShare;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EnvisioDevs on 16-11-2017.
 */

public class AdapterSplitBill extends ArrayAdapter<SplitShare> {

    Context mContext;
    List<SplitShare> mUserList;
    OnSplitActionLisener actionLisener;
    String TAG="AdapterSplitBill";
    int mFunctionId;


    public AdapterSplitBill(@NonNull Context context, @LayoutRes int resource, @NonNull List<SplitShare> objects, OnSplitActionLisener listener,int funcId) {
        super(context, resource, objects);
        mContext = context;
        mUserList = objects;
        actionLisener = listener;
        mFunctionId  = funcId;
        Log.d(TAG, "AdapterSplitBill: "+objects.size());
    }


    @Override
    public int getCount() {
        return mUserList.size();
    }

    @Nullable
    @Override
    public SplitShare getItem(int position) {
        return mUserList.get(position);
    }

    @Override
    public int getPosition(@Nullable SplitShare item) {
        return mUserList.indexOf(item);
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            LayoutInflater inflator = LayoutInflater.from(mContext);
            View view  = inflator.inflate(R.layout.adapter_split_bills, null);
            final SplitViewHolder viewHolder = new SplitViewHolder();
            viewHolder.userName      = (TextView) view.findViewById(R.id.user_lable);
            viewHolder.currencyLable = (TextView) view.findViewById(R.id.current_value);
          //  viewHolder.amountShare   = (TextView) view.findViewById(R.id.share_value);
            viewHolder.splitedAmount = (EditText) view.findViewById(R.id.split_amount);
            if(mFunctionId==1)
                viewHolder.splitedAmount.setEnabled(false);
            else
                viewHolder.splitedAmount.setEnabled(true);


        //  viewHolder.plusImage     = (ImageView) view.findViewById(R.id.add);
          //  viewHolder.minusImage    = (ImageView) view.findViewById(R.id.minus);

        //set data


        if(position==0)
           // viewHolder.userName.setText(mUserList.get(position).getName());
           viewHolder.userName.setText("Your");
        else
            viewHolder.userName.setText(mUserList.get(position).getName()+"'s");
        viewHolder.currencyLable.setText("GBP");
  //      viewHolder.amountShare.setText(""+mUserList.get(position).getShare());
        String num = String.format("%.2f",mUserList.get(position).getAmount());
        if(mUserList.get(position).isUpdate()) {
            viewHolder.splitedAmount.setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
            viewHolder.splitedAmount.setEnabled(false);
        }
        else{
            viewHolder.splitedAmount.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            viewHolder.splitedAmount.setEnabled(true);
        }
        viewHolder.splitedAmount.setText(""+num);
  /*      viewHolder.plusImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                actionLisener.OnSharePlus(position);

            }
        });
        viewHolder.minusImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                actionLisener.OnShareMinus(position);

            }
        });*/

        viewHolder.splitedAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.d(TAG, "onClick: ");
                actionLisener.OnEditorModeOn();
            }
        });

        viewHolder.splitedAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if(!viewHolder.splitedAmount.getText().toString().isEmpty() && !mUserList.get(position).isUpdate()) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            actionLisener.OnEdited(position,viewHolder.splitedAmount.getText().toString());

                        }
                    }, 2000);

                }
                else{
                    //do nothing
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        return view;
    }

        static class SplitViewHolder{
           // protected ImageView plusImage,minusImage;
            protected TextView userName,currencyLable;//amountShare;
            EditText splitedAmount;
        }

}
