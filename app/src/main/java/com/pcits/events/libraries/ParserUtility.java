package com.pcits.events.libraries;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.pcits.common.utils.Logging;
import com.pcits.events.obj.AdminObj;
import com.pcits.events.obj.AttendedObj;
import com.pcits.events.obj.CategoryObj;
import com.pcits.events.obj.ClientInfo;
import com.pcits.events.obj.DealObj;
import com.pcits.events.obj.EventDashboardObj;
import com.pcits.events.obj.MessageObj;
import com.pcits.events.obj.NormalAdminObj;
import com.pcits.events.obj.PaidticketsObj;
import com.pcits.events.obj.PurchaseObj;
import com.pcits.events.obj.SuperAdminObj;
import com.pcits.events.obj.TnCobj;
import com.pcits.events.obj.UserObj;
import com.pcits.events.obj.WinnerObj;

public class ParserUtility {

	private static final String TAG = "ParserUtility";

	public static ArrayList<TnCobj> getListTncFromJson(JSONObject object) {
		ArrayList<TnCobj> tncObj = new ArrayList<TnCobj>();
		try {

			TnCobj tnc;
			JSONObject item;
			JSONArray arr = object.getJSONArray("data");
			if (arr.length() > 0) {
				for (int i = 0; i < arr.length(); i++) {
					item = arr.getJSONObject(i);
					tnc = new TnCobj();
					tnc.setTnc_id(item.getString("tnc_id"));
					tnc.setUsername(item.getString("username"));
					tnc.setTncDesc(item.getString("tnc_desc"));
					tnc.setNotes(item.getString("notes"));
					tncObj.add(tnc);
				}
			}
		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return tncObj;

	}


	public static ArrayList<TnCobj> getListTnc(String json) {
		ArrayList<TnCobj> tncObj = new ArrayList<TnCobj>();
		try {
			JSONObject object = new JSONObject(json);
			TnCobj tnc;
			JSONObject item;
			JSONArray arr = object.getJSONArray("data");
			if (arr.length() > 0) {
				for (int i = 0; i < arr.length(); i++) {
					item = arr.getJSONObject(i);
					tnc = new TnCobj();
					tnc.setTnc_id(item.getString("tnc_id"));
					tnc.setUsername(item.getString("username"));
					tnc.setTncDesc(item.getString("tnc_desc"));
					tnc.setNotes(item.getString("notes"));
					tncObj.add(tnc);
				}
			}
		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return tncObj;

	}

	// list message
	public static ArrayList<MessageObj> getListMessage(String json) {
		ArrayList<MessageObj> msgObj = new ArrayList<MessageObj>();
		try {
			JSONObject object = new JSONObject(json);
			if (object.getString(UserFunctions.KEY_JSON_STATUS)
					.equalsIgnoreCase(UserFunctions.JSON_STATUS_SUCCESS)) {
				MessageObj msg;
				JSONObject item;
				JSONArray arr = object.getJSONArray("data");
				if (arr.length() > 0) {
					for (int i = 0; i < arr.length(); i++) {
						item = arr.getJSONObject(i);
						msg = new MessageObj();
						msg.setId(item.getString("id"));
						msg.setDeal_id(item.getString("deal_id"));
						msg.setTitle(item.getString("title"));
						msg.setCompany(item.getString("company"));
						msg.setUser_id(item.getString("user_id"));
						msg.setClient_id(item.getString("client_id"));
						msg.setGcm_id(item.getString("gcm_id"));
						msg.setDate_created(item.getString("date_created"));
						msg.setDate_claimed(item.getString("date_claimed"));
						msg.setUniquecode(item.getString("uniquecode"));
						msg.setNote(item.getString("note"));
						msg.setStatus(item.getInt("status"));
						msg.setValue(item.getInt("value"));
						msgObj.add(msg);

					}
				}
			}
		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return msgObj;

	}

	public static ArrayList<MessageObj> getListLuckyDrawOfEvent(String json) {
		ArrayList<MessageObj> msgObj = new ArrayList<MessageObj>();
		try {
			JSONObject object = new JSONObject(json);
			if (object.getString(UserFunctions.KEY_JSON_STATUS)
					.equalsIgnoreCase(UserFunctions.JSON_STATUS_SUCCESS)) {
				MessageObj msg;
				JSONObject item;
				JSONArray arr = object.getJSONArray("data");
				if (arr.length() > 0) {
					for (int i = 0; i < arr.length(); i++) {
						item = arr.getJSONObject(i);
						msg = new MessageObj();
						msg.setId(item.getString("id"));
						msg.setDeal_id(item.getString("deal_id"));
						msg.setTitle(item.getString("title"));
						msg.setCompany(item.getString("company"));
						msg.setUser_id(item.getString("user_id"));
						msg.setClient_id(item.getString("client_id"));
						msg.setGcm_id(item.getString("gcm_id"));
						msg.setUniquecode(item.getString("uniquecode"));
						msg.setNote(item.getString("note"));
						msg.setStatus(item.getInt("status"));
						msg.setValue(item.getInt("value"));
						msg.setFname(item.getString("fname"));
						msg.setLname(item.getString("lname"));
						msg.setEmail(item.getString("email"));
						msg.setGender(item.getString("gender"));
						msg.setDob(item.getString("dob"));
						msg.setAddress(item.getString("address"));
						msg.setImage(item.getString("image"));
						msg.setDate_created(item.getString("date_created"));
						msg.setDate_claimed(item.getString("date_claimed"));
						msg.setQuantity(item.getInt("quantity"));
						msg.setAmount(item.getInt("amount"));
						msgObj.add(msg);

					}
				}
			}
		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return msgObj;

	}

	// list tickets
	public static ArrayList<PaidticketsObj> getListTickets(String json) {
		ArrayList<PaidticketsObj> ticketObj = new ArrayList<PaidticketsObj>();
		try {
			JSONObject object = new JSONObject(json);
			if (object.getString(UserFunctions.KEY_JSON_STATUS)
					.equalsIgnoreCase(UserFunctions.JSON_STATUS_SUCCESS)) {
				PaidticketsObj ticket;
				JSONObject item;
				JSONArray arr = object.getJSONArray("data");
				if (arr.length() > 0) {
					for (int i = 0; i < arr.length(); i++) {
						item = arr.getJSONObject(i);
						ticket = new PaidticketsObj();
						ticket.setId(item.getString("id"));
						ticket.setTran_id(item.getString("txn_id"));
						ticket.setClientId(item.getString("username"));
						ticket.setUserId(item.getString("admin_id"));
						ticket.setEmail(item.getString("client_email"));
						ticket.setTitle(item.getString("title"));
						ticket.setCompany(item.getString("company"));
						ticket.setStartTimeStamp(item
								.getString("start_timestamp"));
						ticket.setEndTimeStamp(item.getString("end_timestamp"));
						ticket.setfName(item.getString("fname"));
						ticket.setlName(item.getString("lname"));
						ticket.setImgAvatar(item.getString("image"));
						ticket.setGender(item.getString("gender"));
						ticket.setEventId(item.getString("deal_id"));
						ticket.setQuantity(item.getInt("quantity"));
						ticket.setAmount(item.getString("amount"));
						ticket.setPayMethod(item.getString("paymentType"));
						ticket.setUniqueCode(item.getString("unique_code"));
						ticket.setStatus(item.getInt("status"));
						ticket.setUsed_date(item.getString("used_date"));
						ticket.setCreateDate(item.getString("createDate"));
						ticket.setLastDate(item.getString("last_update"));

						ticketObj.add(ticket);
					}
				}
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		return ticketObj;

	}

	public static ArrayList<PurchaseObj> getListTicketsPaid(String json) {
		ArrayList<PurchaseObj> ticketObj = new ArrayList<PurchaseObj>();
		try {
			JSONObject object = new JSONObject(json);
			if (object.getString(UserFunctions.KEY_JSON_STATUS)
					.equalsIgnoreCase(UserFunctions.JSON_STATUS_SUCCESS)) {
				PurchaseObj ticket;
				JSONObject item;
				JSONArray arr = object.getJSONArray("data");
				if (arr.length() > 0) {
					for (int i = 0; i < arr.length(); i++) {
						item = arr.getJSONObject(i);
						ticket = new PurchaseObj();

						ticket.setTitle(item.getString("title"));
						ticket.setCompany(item.getString("company"));
						ticket.setFullName(item.getString("fname") + " "
								+ item.getString("lname"));
						ticket.setGender(item.getString("gender"));

						ticketObj.add(ticket);
					}
				}
			}
		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return ticketObj;

	}

	// list tickets of event
	// public static ArrayList<PaidticketsObj> getListTicketsOfEvent(String
	// json) {
	// ArrayList<PaidticketsObj> ticketObj = new ArrayList<PaidticketsObj>();
	// try {
	// JSONObject object = new JSONObject(json);
	// if (object.getString(UserFunctions.KEY_JSON_STATUS)
	// .equalsIgnoreCase(UserFunctions.JSON_STATUS_SUCCESS)) {
	// PaidticketsObj ticket;
	// JSONObject item;
	// JSONArray arr = object.getJSONArray("data");
	// if (arr.length() > 0) {
	// for (int i = 0; i < arr.length(); i++) {
	// item = arr.getJSONObject(i);
	// ticket = new PaidticketsObj();
	// ticket.setId(item.getString("id"));
	// ticket.setTran_id(item.getString("transaction_id"));
	// ticket.setUserId(item.getString("user_id"));
	// ticket.setClientId(item.getString("client_id"));
	// ticket.setfName(item.getString("fname"));
	// ticket.setlName(item.getString("lname"));
	// ticket.setEventId(item.getString("event_id"));
	// ticket.setTitle(item.getString("title"));
	// ticket.setCompany(item.getString("company"));
	// ticket.setEmail(item.getString("email"));
	// ticket.setGender(item.getString("gender"));
	// ticket.setImgAvatar(item.getString("image"));
	// ticket.setAmount(item.getInt("amount"));
	// ticket.setUsed_date(item.getString("used_date"));
	// ticket.setPayMethod(item.getString("pay_method"));
	// ticket.setUniqueCode(item.getString("uniquecode"));
	// ticket.setStatus(item.getInt("status"));
	// ticket.setStartTimeStamp(item
	// .getString("start_timestamp"));
	// ticket.setEndTimeStamp(item.getString("end_timestamp"));
	// ticketObj.add(ticket);
	// }
	// }
	// }
	// } catch (Exception e) {
	// // TODO: handle exception
	// }
	// return ticketObj;
	//
	// }

	public static ArrayList<AttendedObj> getListAttending(String json) {
		ArrayList<AttendedObj> attendObj = new ArrayList<AttendedObj>();
		try {
			JSONObject object = new JSONObject(json);
			if (object.getString(UserFunctions.KEY_JSON_STATUS)
					.equalsIgnoreCase(UserFunctions.JSON_STATUS_SUCCESS)) {
				AttendedObj atend;
				JSONObject item;
				JSONArray arr = object.getJSONArray("data");
				if (arr.length() > 0) {
					for (int i = 0; i < arr.length(); i++) {
						item = arr.getJSONObject(i);
						atend = new AttendedObj();
						atend.setAttendedId(item.getString("attend_id"));
						atend.setUsername(item.getString("username"));
						atend.setTncId(item.getString("tnc_id"));
						atend.setStatusId(item.getString("status_id"));
						atend.setDeviceId(item.getString("device_id"));
						atend.setDealId(item.getString("deal_id"));
						atend.setClientname(item.getString("clientname"));
						attendObj.add(atend);
					}
				}
			}
		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return attendObj;

	}

	// list category
	public static ArrayList<CategoryObj> getListCategory(String json) {
		ArrayList<CategoryObj> categoryObj = new ArrayList<CategoryObj>();
		try {
			JSONObject object = new JSONObject(json);
			if (object.getString(UserFunctions.KEY_JSON_STATUS)
					.equalsIgnoreCase(UserFunctions.JSON_STATUS_SUCCESS)) {
				CategoryObj category;
				JSONObject item;
				JSONArray arr = object.getJSONArray("data");
				if (arr.length() > 0) {
					for (int i = 0; i < arr.length(); i++) {
						if(!arr.getJSONObject(i).getString("category_name").isEmpty()) {
							item = arr.getJSONObject(i);
							category = new CategoryObj();
							category.setCategoryId(item.getString("category_id"));
							category.setCategoryName(item
									.getString("category_name"));
							category.setCategoryImage(item
									.getString("category_marker"));
							categoryObj.add(category);
						}

					}
				}
			}
		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return categoryObj;

	}

	public static DealObj detailDeals(String keyData, String json) {
		DealObj deal = null;
		try {
			JSONObject object = new JSONObject(json);
				JSONArray item = object.getJSONArray(keyData);
				JSONObject obj = item.getJSONObject(0);
				deal = new DealObj();
				deal.setTnc_id(obj.getString("tnc_id"));
				deal.setTitle(obj.getString("title"));
				deal.setCompany(obj.getString("company"));
				deal.setUsername(obj.getString("username"));
				deal.setAddress(obj.getString("address"));
				deal.setCity(obj.getString("city"));
				deal.setCounty(obj.getString("country"));
				deal.setCategory_id(obj.getString("category_id"));
				deal.setCategory_id(obj.getString("category_marker"));
				deal.setCategory_name(obj.getString("category_name"));
				deal.setTnc_desc(obj.getString("tnc_desc"));
				deal.setViewCount(obj.getInt("viewed"));
				deal.setIsHot(obj.getInt("isHot"));
				deal.setLatitude(obj.getDouble("latitude"));
				deal.setLongitude(obj.getDouble("latitude"));
				deal.setDeal_url(obj.getString("deal_url"));
				deal.setImage(obj.getString("image"));
				deal.setAfter_discount_value(obj
						.getDouble("after_discount_value"));
				deal.setSave_value(obj.getDouble("start_value"));
				deal.setDiscount(obj.getInt("discount"));
				deal.setSave_value(obj.getDouble("save_value"));
				deal.setStartTimeStamp(obj.getString("start_timestamp"));
				deal.setEndTimeStamp(obj.getString("end_timestamp"));
				deal.setStart_date(obj.getString("start_date"));
				deal.setEnd_date(obj.getString("end_date"));
				deal.setStart_time(obj.getString("start_time"));
				deal.setEnd_time(obj.getString("end_time"));
				deal.setDescription(obj.getString("description"));
				deal.setTnc_notes(obj.getString("tnc_notes"));
				deal.setAttend(obj.getInt("attended"));
				deal.setQuantity(obj.getInt("quantity"));
				
		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return deal;
	}

	// public static AccountInfo parseFacebookAccount(String tokenAccess,
	// String facebookJson) {
	// AccountInfo account = new AccountInfo();
	// account.setExternalAccessToken(tokenAccess);
	// account.setExternalAccount(true);
	// // parse facebook json about me
	// JSONObject accountJson = null;
	// try {
	// accountJson = new JSONObject(facebookJson);
	// account.setEmail(getStringValue(accountJson, "email"));
	// account.setFname(getStringValue(accountJson, "name"));
	// account.setPassword(getStringValue(accountJson, "id"));
	//
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// return account;
	// }
	//
	// private static String getStringValue(JSONObject accountJson, String
	// string) {
	// // TODO Auto-generated method stub
	// return null;
	// }

	public static ClientInfo parserAccount(String json)
	{
		ClientInfo account = null;
		try
		{
			JSONObject object = new JSONObject(json);

			if (object.getString("status").equalsIgnoreCase("success")) {
				JSONArray item2 = object.getJSONArray("data");
				JSONObject obj = item2.getJSONObject(0);
				account = new ClientInfo();
				account.setId(obj.getString("id"));
				account.setFbid(obj.getString("fbid"));
				account.setDeviceid(obj.getString("device_id"));
				account.setUsername(obj.getString("clientname"));
				account.setPassword(obj.getString("password"));
				account.setFname(obj.getString("fname"));
				account.setLname(obj.getString("lname"));
				account.setCity(obj.getString("city"));
				account.setAddress(obj.getString("address"));
				account.setEmail(obj.getString("email"));
				account.setDob(obj.getString("dob"));
				account.setGender(obj.getString("gender"));
				account.setCounty(obj.getString("county"));
				account.setCountry(obj.getString("country"));
				account.setPostcode(obj.getString("postcode"));
				account.setPhone(obj.getString("phone"));
				account.setImage(obj.getString("image"));
				account.setTimezone(obj.getString("timezone"));
				account.setVerified(obj.getString("verified"));
				account.setFullname(obj.getString("fullname"));
				account.setLink(obj.getString("link"));
				account.setUpdatedtime(obj.getString("last_updated"));
				account.setDate_register(obj.getString("date_reged"));
				account.setLocale(obj.getString("locale"));
				account.setNotes(obj.getString("notes"));
				account.setType(obj.getString("type"));
				account.setDeaid(obj.getString("deal_id"));
				account.setLastLoggedin(obj.getString("last_login"));
				account.setLastUpdatedPass(obj.getString("last_pass_update"));
			}
		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return account;
	}

	public static UserObj parserUser(String json) {
		UserObj user = null;
		try {
			JSONObject object = new JSONObject(json);

			if (object.getString("status").equalsIgnoreCase("success")) {
				JSONArray item2 = object.getJSONArray("data");
				JSONObject obj = item2.getJSONObject(0);
				user = new UserObj();
				user.setUsername(obj.getString("username"));
				user.setPassword(obj.getString("password"));
				user.setFname(obj.getString("fname"));
				user.setLname(obj.getString("lname"));
				user.setCity(obj.getString("city"));
				user.setAddress(obj.getString("address"));
				user.setEmail(obj.getString("email"));
				user.setDob(obj.getString("dob"));
				user.setCounty(obj.getString("county"));
				user.setCountry(obj.getString("country"));
				user.setPostcode(obj.getString("postcode"));
				user.setPhone(obj.getString("phone"));
				user.setNotes(obj.getString("notes"));
				user.setRole(obj.getString("role"));
				user.setPayInfo(obj.getString("pay_info"));
				user.setStatus(obj.getString("status"));
				user.setImage(obj.getString("image"));
				user.setLastLoggedin(obj.getString("last_login"));
				user.setLastUpdatedPass(obj.getString("last_pass_update"));
			}
		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return user;
	}

	public static AttendedObj parserAttended(String json) {
		AttendedObj attended = null;
		try {
			JSONObject object = new JSONObject(json);

			if (object.getString("status").equalsIgnoreCase("success")) {
				JSONArray item2 = object.getJSONArray("data");
				JSONObject obj = item2.getJSONObject(0);
				attended = new AttendedObj();
				attended.setUsername(obj.getString("username"));
				attended.setClientname(obj.getString("clientname"));
				attended.setAttendedId(obj.getString("attended_id"));
				attended.setDealId(obj.getString("deal_id"));
				attended.setDeviceId(obj.getString("device_id"));
				attended.setStatusId(obj.getString("status_id"));
				attended.setTncId(obj.getString("tnc_id"));

			}
		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return attended;
	}

	public static boolean updateAccount(String json) {
		try {
			JSONObject object = new JSONObject(json);
			if (object.getString("status").equalsIgnoreCase("success")) {
				return true;
			}
		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return false;
	}

	public static String parserLogin(String json) {
		try {
			JSONObject obj = new JSONObject(json);

			if (obj.getString("status").equalsIgnoreCase("success")) {
				return obj.getString("SessionId");
			}

		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return "";
	}

	public static SuperAdminObj parserSuperAdminDashboard(String json)
	{
		SuperAdminObj adm = null;
		int NumberOfEvents,NumberOfEventsAdv,NumberOfFeatured,NumberOfClient,NumberClientMale,NumberClientFemale,
				PriceRange5,PriceRange5To10,PriceRange10To20,PriceRange20To40,PriceRange40,
				NumberEventNotExpired,NumberEventExpired,GenderClientNotDisclosed,TotalPurchaseClientMale,
				TotalPurchaseClientFemale,TotalPurchaseClientNotDisclosed,NumberEventFree,NumberLuckyDrawClaimed,
				NumberLuckyDrawAwarded,NumberLuckyDrawUnclaimed,NumberOfAdmin,NumberOfTicket,NumberAdminNeedActivating;;

		try
		{
			JSONObject obj = new JSONObject(json);

			if (obj.getString("status").equalsIgnoreCase("success"))
			{
				JSONArray arrJson = obj.getJSONArray("data");
				JSONObject objJson = arrJson.getJSONObject(0);
				try
				{
					NumberOfEvents=objJson.getInt("NumberOfEvents");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberOfEvents=0;
				}
				try {
					NumberOfEventsAdv = objJson.getInt("NumberOfEventsAdv");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberOfEventsAdv=0;
				}
				try
				{
					NumberOfFeatured=objJson.getInt("NumberOfFeatured");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberOfFeatured=0;
				}
				try
				{
					NumberOfClient=objJson.getInt("NumberOfClient");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberOfClient=0;
				}
				try
				{
					NumberClientMale=objJson.getInt("NumberClientMale");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberClientMale=0;
				}
				try
				{
					NumberClientFemale=objJson.getInt("NumberClientFemale");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberClientFemale=0;
				}
				try
				{
					PriceRange5=objJson.getInt("PriceRange5");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					PriceRange5=0;
				}
				try
				{
					PriceRange5To10=objJson.getInt("PriceRange5To10");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					PriceRange5To10=0;
				}
				try
				{
					PriceRange10To20=objJson.getInt("PriceRange10To20");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					PriceRange10To20=0;
				}
				try
				{
					PriceRange20To40=objJson.getInt("PriceRange20To40");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					PriceRange20To40=0;
				}
				try
				{
					PriceRange40=objJson.getInt("PriceRange40");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					PriceRange40=0;
				}
				try
				{
					NumberEventNotExpired=objJson.getInt("NumberEventNotExpired");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberEventNotExpired=0;
				}
				try
				{
					NumberEventExpired=objJson.getInt("NumberEventExpired");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberEventExpired=0;
				}
				try
				{
					GenderClientNotDisclosed=objJson.getInt("GenderClientNotDisclosed");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					GenderClientNotDisclosed=0;
				}
				try
				{
					TotalPurchaseClientMale=objJson.getInt("TotalPurchaseClientMale");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					TotalPurchaseClientMale=0;
				}
				try
				{
					TotalPurchaseClientFemale=objJson.getInt("TotalPurchaseClientFemale");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					TotalPurchaseClientFemale=0;
				}
				try
				{
					TotalPurchaseClientNotDisclosed=objJson.getInt("TotalPurchaseClientNotDisclosed");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					TotalPurchaseClientNotDisclosed=0;
				}
				try
				{
					NumberEventFree=objJson.getInt("NumberEventFree");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberEventFree=0;
				}
				try
				{
					NumberLuckyDrawClaimed=objJson.getInt("NumberLuckyDrawClaimed");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberLuckyDrawClaimed=0;
				}
				try
				{
					NumberLuckyDrawAwarded=objJson.getInt("NumberLuckyDrawAwarded");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberLuckyDrawAwarded=0;
				}
				try
				{
					NumberLuckyDrawUnclaimed=objJson.getInt("NumberLuckyDrawUnclaimed");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberLuckyDrawUnclaimed=0;
				}
				try
				{
					NumberOfAdmin=objJson.getInt("NumberOfAdmin");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberOfAdmin=0;
				}
				try
				{
					NumberOfTicket=objJson.getInt("NumberOfTicket");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberOfTicket=0;
				}
				try
				{
					NumberAdminNeedActivating=objJson.getInt("NumberAdminNeedActivating");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberAdminNeedActivating=0;
				}
				adm = new SuperAdminObj(NumberOfEvents,NumberOfAdmin,NumberOfEventsAdv,NumberOfFeatured,
						NumberOfClient,NumberClientMale,NumberClientFemale,NumberOfTicket,PriceRange5,
						PriceRange5To10,PriceRange10To20,PriceRange20To40,PriceRange40,
						NumberEventNotExpired,NumberEventExpired,NumberAdminNeedActivating,GenderClientNotDisclosed,
						TotalPurchaseClientMale,TotalPurchaseClientFemale,TotalPurchaseClientNotDisclosed,
						NumberEventFree,NumberLuckyDrawClaimed,NumberLuckyDrawAwarded,NumberLuckyDrawUnclaimed);
				return adm;
			}

		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return new SuperAdminObj();
	}

	public static NormalAdminObj parserNormalAdminDashboard(String json)
	{
		NormalAdminObj adm = null;
		int NumberOfEvents,NumberOfEventsAdv,NumberOfFeatured,NumberOfClient,NumberClientMale,NumberClientFemale,
				PriceRange5,PriceRange5To10,PriceRange10To20,PriceRange20To40,PriceRange40,
				NumberEventNotExpired,NumberEventExpired,GenderClientNotDisclosed,TotalPurchaseClientMale,
			TotalPurchaseClientFemale,TotalPurchaseClientNotDisclosed,NumberEventFree,NumberLuckyDrawClaimed,
				NumberLuckyDrawAwarded,NumberLuckyDrawUnclaimed;
		try {
			JSONObject obj = new JSONObject(json);

			if (obj.getString("status").equalsIgnoreCase("success")) {
				JSONArray arrJson = obj.getJSONArray("data");
				JSONObject objJson = arrJson.getJSONObject(0);

				try
				{
					NumberOfEvents=objJson.getInt("NumberOfEvents");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberOfEvents=0;
				}
				try {
					NumberOfEventsAdv = objJson.getInt("NumberOfEventsAdv");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberOfEventsAdv=0;
				}
				try
				{
					NumberOfFeatured=objJson.getInt("NumberOfFeatured");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberOfFeatured=0;
				}
				try
				{
					NumberOfClient=objJson.getInt("NumberOfClient");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberOfClient=0;
				}
				try
				{
					NumberClientMale=objJson.getInt("NumberClientMale");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberClientMale=0;
				}
				try
				{
					NumberClientFemale=objJson.getInt("NumberClientFemale");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberClientFemale=0;
				}
				try
				{
					PriceRange5=objJson.getInt("PriceRange5");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					PriceRange5=0;
				}
				try
				{
					PriceRange5To10=objJson.getInt("PriceRange5To10");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					PriceRange5To10=0;
				}
				try
				{
					PriceRange10To20=objJson.getInt("PriceRange10To20");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					PriceRange10To20=0;
				}
				try
				{
					PriceRange20To40=objJson.getInt("PriceRange20To40");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					PriceRange20To40=0;
				}
				try
				{
					PriceRange40=objJson.getInt("PriceRange40");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					PriceRange40=0;
				}
				try
				{
					NumberEventNotExpired=objJson.getInt("NumberEventNotExpired");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberEventNotExpired=0;
				}
				try
				{
					NumberEventExpired=objJson.getInt("NumberEventExpired");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberEventExpired=0;
				}
				try
				{
					GenderClientNotDisclosed=objJson.getInt("GenderClientNotDisclosed");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					GenderClientNotDisclosed=0;
				}
				try
				{
					TotalPurchaseClientMale=objJson.getInt("TotalPurchaseClientMale");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					TotalPurchaseClientMale=0;
				}
				try
				{
					TotalPurchaseClientFemale=objJson.getInt("TotalPurchaseClientFemale");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					TotalPurchaseClientFemale=0;
				}
				try
				{
					TotalPurchaseClientNotDisclosed=objJson.getInt("TotalPurchaseClientNotDisclosed");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					TotalPurchaseClientNotDisclosed=0;
				}
				try
				{
					NumberEventFree=objJson.getInt("NumberEventFree");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberEventFree=0;
				}
				try
				{
					NumberLuckyDrawClaimed=objJson.getInt("NumberLuckyDrawClaimed");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberLuckyDrawClaimed=0;
				}
				try
				{
					NumberLuckyDrawAwarded=objJson.getInt("NumberLuckyDrawAwarded");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberLuckyDrawAwarded=0;
				}
				try
				{
					NumberLuckyDrawUnclaimed=objJson.getInt("NumberLuckyDrawUnclaimed");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					NumberLuckyDrawUnclaimed=0;
				}
				adm = new NormalAdminObj(NumberOfEvents,NumberOfEventsAdv,NumberOfFeatured,
						NumberOfClient,NumberClientMale,NumberClientFemale,PriceRange5,
						PriceRange5To10,PriceRange10To20,PriceRange20To40,PriceRange40,
						NumberEventNotExpired,NumberEventExpired,GenderClientNotDisclosed,
						TotalPurchaseClientMale,TotalPurchaseClientFemale,TotalPurchaseClientNotDisclosed,
						NumberEventFree,NumberLuckyDrawClaimed,NumberLuckyDrawAwarded,NumberLuckyDrawUnclaimed);
				return adm;
			}

		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return new NormalAdminObj();
	}

	public static ArrayList<AdminObj> parserAdminInfo(String json) {
		ArrayList<AdminObj> arr = new ArrayList<AdminObj>();
		try {
			JSONObject obj = new JSONObject(json);

			if (obj.getString("status").equalsIgnoreCase("success")) {
				JSONArray arrJson = obj.getJSONArray("data");

				for (int i = 0; i < arrJson.length(); i++) {
					JSONObject objJson = arrJson.getJSONObject(i);

					boolean isActive = false;
					if (objJson.getString("status").equals("1")) {
						isActive = true;
					}
					AdminObj adm = new AdminObj(objJson.getString("username"),
							objJson.getString("image"),
							objJson.getString("fname") + " "
									+ objJson.getString("lname"),
							objJson.getString("email"),
							objJson.getString("phone"),
							objJson.getString("country"), isActive);

					arr.add(adm);
				}

				return arr;
			}

		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return arr;
	}

	public static ArrayList<WinnerObj> getListWinner(String json) {
		ArrayList<WinnerObj> obj = new ArrayList<WinnerObj>();
		try {
			JSONObject object = new JSONObject(json);
			if (object.getString(UserFunctions.KEY_JSON_STATUS)
					.equalsIgnoreCase(UserFunctions.JSON_STATUS_SUCCESS)) {
				WinnerObj winner;
				JSONObject item;
				JSONArray arr = object.getJSONArray("data");
				if (arr.length() > 0) {
					for (int i = 0; i < arr.length(); i++) {
						item = arr.getJSONObject(i);

						winner = new WinnerObj(item.getString("uniquecode"),
								item.getString("fname") + " "
										+ item.getString("lname"),
								item.getString("gender"),
								item.getString("dob"),
								item.getString("address"),
								item.getString("image"),
								item.getString("gcm_id"),
								item.getString("title"),
								item.getString("company"),
								item.getString("status"),
								item.getString("value"));

						obj.add(winner);
					}
				}
			}
		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return obj;
	}

	public static EventDashboardObj parserEventDashboard(String json) {
		EventDashboardObj event = null;
		try {
			JSONObject obj = new JSONObject(json);

			if (obj.getString("status").equalsIgnoreCase("success")) {
				JSONArray arrJson = obj.getJSONArray("data");
				JSONObject objJson = arrJson.getJSONObject(0);

				event = new EventDashboardObj(objJson.getString("title"),
						objJson.getDouble("TotalSpent"), 0, 0,
						objJson.getDouble("NumberOfClient"),
						objJson.getDouble("NumberClientMale"),
						objJson.getDouble("NumberClientFemale"),
						objJson.getDouble("GenderClientNotDisclosed"),
						objJson.getDouble("QuantityTicket"),
						objJson.getDouble("TotalPurchaseClientMale"),
						objJson.getDouble("TotalPurchaseClientFemale"),
						objJson.getDouble("TotalPurchaseClientNotDisclosed"),
						objJson.getDouble("ClientOfDeal"),
						objJson.getDouble("NumberLuckyDrawAwarded"), 0,
						objJson.getDouble("NumberLuckyDrawClaimed"),
						objJson.getDouble("NumberLuckyDrawUnclaimed"), 0, 0, 0);

				return event;
			}

		} catch (Exception ex) {
			if (Logging.debugFile.exists()){ex.printStackTrace();}
			throw new RuntimeException(ex);
		}
		return new EventDashboardObj();
	}
}
