package com.pcits.events;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.pcits.events.config.ads.Ads;
import com.pcits.events.fragments.FragmentFilter;
import com.pcits.events.utils.Utils;

public class ActivityFilter extends FragmentActivity implements
		FragmentFilter.OnListSelectedListener {

	// Create an instance of ActionBar
	private android.app.ActionBar actionbar;

	// Declare object of AdView class
	private AdView adView;

	// Declare object of Utils class
	private Utils utils;
	protected Fragment mFrag;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		// Declare object of Utils class
		utils = new Utils(this);

		// connect view objects and xml ids
		adView = (AdView) this.findViewById(R.id.adView);

		// Change actionbar title
		int titleId = Resources.getSystem().getIdentifier("action_bar_title",
				"id", "android");
//		if (0 == titleId)
//			titleId = com.actionbarsherlock.R.id.abs__action_bar_title;

		// Change the title color to white
		TextView txtActionbarTitle = (TextView) findViewById(titleId);
		txtActionbarTitle.setTextColor(getResources().getColor(
				R.color.actionbar_title_color));

		// Get ActionBar and set back button on actionbar
		actionbar = getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);

		FragmentTransaction t = this.getSupportFragmentManager()
				.beginTransaction();
		mFrag = new FragmentFilter();
		t.replace(R.id.frame_content, mFrag);
		t.commit();

		// load ads
		Ads.loadAds(adView);
	}

	// Listener for List Selected
	@Override
	public void onListSelected(String mIdSelected) {
		// TODO Auto-generated method stub
		// Call ActivityDetailPlace
		Intent i = new Intent(this, ActivityDetail.class);
		i.putExtra(utils.EXTRA_DEAL_ID, mIdSelected);
		startActivity(i);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:

			// Previous page or exit
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
