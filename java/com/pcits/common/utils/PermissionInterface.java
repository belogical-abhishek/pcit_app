package com.pcits.common.utils;

/**
 * Created by shrik on 11/7/2017.
 */

public interface PermissionInterface {

    public void onPermissionGranted();
}
