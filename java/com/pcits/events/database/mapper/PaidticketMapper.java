package com.pcits.events.database.mapper;

import android.database.Cursor;

import com.pcits.events.obj.PaidticketsObj;

public class PaidticketMapper implements RowMapper<PaidticketsObj> {

	@Override
	public PaidticketsObj mapRow(Cursor row, int rowNum) {
		// TODO Auto-generated method stub
		PaidticketsObj paidticket = new PaidticketsObj();
		paidticket.setId(CursorParseUtility.getString(row, "id"));
		paidticket.setTran_id(CursorParseUtility.getString(row,
				"transaction_id"));
		paidticket.setUserId(CursorParseUtility.getString(row, "user_id"));
		paidticket.setClientId(CursorParseUtility.getString(row, "client_id"));
		paidticket.setEventId(CursorParseUtility.getString(row, "event_id"));
		paidticket.setAmount(CursorParseUtility.getString(row, "amount"));
		paidticket.setUsed_date(CursorParseUtility.getString(row, "used_date"));
		paidticket.setPayMethod(CursorParseUtility.getString(row, "pay_method"));
		paidticket.setUniqueCode(CursorParseUtility
				.getString(row, "uniquecode"));
		paidticket.setStatus(CursorParseUtility.getInt(row, "status"));
		paidticket.setUpdate(CursorParseUtility.getBoolean(row, "update"));
		return paidticket;
	}

}
