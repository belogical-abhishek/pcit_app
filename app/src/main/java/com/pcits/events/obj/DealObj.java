package com.pcits.events.obj;

public class DealObj implements Cloneable {
	private String deal_id, category_id, tnc_id, username, title, company,
			address, city, county, postcode, country, image, start_date,
			end_date, start_time, end_time, deal_url, description, tnc_notes,
			marker, tnc_desc, category_name;
	private Double latitude, longitude, after_discount_value, start_value,
			save_value;
	private int discount, attend, featured, quantity,isHot, viewCount;
	private String startTimeStamp, endTimeStamp;

	public DealObj() {
	}

	public DealObj(String dealId, String tncId, String tncDesc, String title,
			String company, String startTimeStamp, String endTimeStamp,
			String startDate, String endDate, String startTime, String endTime,
			Double afterDiscount, Double startPrice, String categoryId,
			String marker, String cateName, String urlImage, String tncNote,
			int attened, int quantity, String username, String address,
			String city, String county, String postcode, String country,
			Double lat, Double lng, String dealUrl, int discount,
			Double save_Value, String description,int isHot,int viewCount) {
		this.deal_id = dealId;
		this.tnc_id = tncId;
		this.tnc_desc = tncDesc;
		this.title = title;
		this.isHot = isHot;
		this.viewCount = viewCount;
		this.company = company;
		this.start_date = startDate;
		this.start_time = startTime;
		this.end_date = endDate;
		this.end_time = endTime;
		this.after_discount_value = afterDiscount;
		this.start_value = startPrice;
		this.category_id = categoryId;
		this.marker = marker;
		this.category_name = cateName;
		this.image = urlImage;
		this.tnc_notes = tncNote;
		this.attend = attened;
		this.quantity = quantity;
		this.startTimeStamp = startTimeStamp;
		this.endTimeStamp = endTimeStamp;
		this.username = username;
		this.address = address;
		this.city = city;
		this.county = county;
		this.country = country;
		this.postcode = postcode;
		this.latitude = lat;
		this.longitude = lng;
		this.deal_url = dealUrl;
		this.discount = discount;
		this.save_value = save_Value;
		this.description = description;
	}


	public int getIsHot() {
		return isHot;
	}

	public void setIsHot(int isHot) {
		this.isHot = isHot;
	}

	public int getViewCount() {
		return viewCount;
	}

	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}

	public String getMarker() {
		return marker;
	}

	public String getTnc_desc() {
		return tnc_desc;
	}

	public void setTnc_desc(String tnc_desc) {
		this.tnc_desc = tnc_desc;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public void setMarker(String marker) {
		this.marker = marker;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDeal_id() {
		return deal_id;
	}

	public void setDeal_id(String deal_id) {
		this.deal_id = deal_id;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getTnc_id() {
		return tnc_id;
	}

	public void setTnc_id(String tnc_id) {
		this.tnc_id = tnc_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getDeal_url() {
		return deal_url;
	}

	public void setDeal_url(String deal_url) {
		this.deal_url = deal_url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTnc_notes() {
		return tnc_notes;
	}

	public void setTnc_notes(String tnc_notes) {
		this.tnc_notes = tnc_notes;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getStart_value() {
		return start_value;
	}

	public void setStart_value(Double start_value) {
		this.start_value = start_value;
	}

	public Double getAfter_discount_value() {
		return after_discount_value;
	}

	public void setAfter_discount_value(Double after_discount_value) {
		this.after_discount_value = after_discount_value;
	}

	public Double getSave_value() {
		return save_value;
	}

	public void setSave_value(Double save_value) {
		this.save_value = save_value;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public int getAttend() {
		return attend;
	}

	public void setAttend(int attend) {
		this.attend = attend;
	}

	public int getFeatured() {
		return featured;
	}

	public void setFeatured(int featured) {
		this.featured = featured;
	}

	public String getStartTimeStamp() {
		return startTimeStamp;
	}

	public void setStartTimeStamp(String startTimeStamp) {
		this.startTimeStamp = startTimeStamp;
	}

	public String getEndTimeStamp() {
		return endTimeStamp;
	}

	public void setEndTimeStamp(String endTimeStamp) {
		this.endTimeStamp = endTimeStamp;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		DealObj deal = (DealObj) super.clone();
		return deal;
	}
}
