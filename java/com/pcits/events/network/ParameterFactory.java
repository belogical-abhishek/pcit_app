/*
 * Name: $RCSfile: ParameterFactory.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Oct 31, 2011 2:45:36 PM $
 *
 * Copyright (C) 2011 COMPANY_NAME, Inc. All rights reserved.
 */

package com.pcits.events.network;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;

import com.pcits.events.obj.AttendedObj;
import com.pcits.events.obj.CategoryObj;
import com.pcits.events.obj.ClientInfo;
import com.pcits.events.obj.DealObj;
import com.pcits.events.obj.GcmObj;
import com.pcits.events.obj.PaidticketsObj;
import com.pcits.events.obj.PurchaseObj;
import com.pcits.events.obj.QuestionInfo;
import com.pcits.events.obj.TnCobj;
import com.pcits.events.obj.UserObj;

/**
 * ParameterFactory class builds parameters for web service apis
 * 
 */
public final class ParameterFactory {

	private static final String PARAM_KEY_USER_NAME = "username";
	private static final String PARAM_KEY_PASSWORD = "password";

	public static List<NameValuePair> createLoginParams(Context context,
			String username, String password) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair(PARAM_KEY_USER_NAME, username));
		parameters.add(new BasicNameValuePair(PARAM_KEY_PASSWORD, password));

		return parameters;
	}

	public static List<NameValuePair> userLoginParams(Context context,
			String username, String password) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair(PARAM_KEY_USER_NAME, username));
		parameters.add(new BasicNameValuePair(PARAM_KEY_PASSWORD, password));

		return parameters;
	}

	// list tnc by uer
	public static List<NameValuePair> createTncByUserParams(Context context,
			String username) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("username", username));

		return parameters;
	}

	public static List<NameValuePair> createTncParams(Context context,
			String username, String role) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("username", username));
		parameters.add(new BasicNameValuePair("type", role));
		return parameters;
	}

	// list tickets
	public static List<NameValuePair> listTicketsParams(Context context,
			String type, String user) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("type", type));
		parameters.add(new BasicNameValuePair("user", user));

		return parameters;
	}

	// list message
	public static List<NameValuePair> listMessage(Context context, String user) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("client", user));

		return parameters;
	}

	// list ticket by event
	public static List<NameValuePair> listByEventParams(Context context,
			String dealId) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("deal_id", dealId));

		return parameters;
	}

	// params update admin credits
	public static List<NameValuePair> updateAdminCreditaParams(Context context,
			int credit, String username) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("credit", Integer
				.toString(credit)));
		parameters.add(new BasicNameValuePair("username", username));
		return parameters;
	}
	// params update click count event
		public static List<NameValuePair> updateClickCountParams(Context context,
				String deal_id) {
			List<NameValuePair> parameters = new ArrayList<NameValuePair>();
			parameters.add(new BasicNameValuePair("deal_id", deal_id));
			return parameters;
		}

	// params update admin credits
	public static List<NameValuePair> updatefeaturedParams(Context context,
			String deal_id, String startDate, String endDate, int featured) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("deal_id", deal_id));
		parameters.add(new BasicNameValuePair("featured", Integer
				.toString(featured)));
		parameters.add(new BasicNameValuePair("start_date", startDate));
		parameters.add(new BasicNameValuePair("end_date", endDate));
		return parameters;
	}

	// register client
	public static List<NameValuePair> createRegisterParams(Context context,
			ClientInfo account) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		// parameters
		// .add(new BasicNameValuePair("type", account.getType()));
		parameters
				.add(new BasicNameValuePair("username", account.getUsername()));
		parameters
				.add(new BasicNameValuePair("password", account.getPassword()));
		parameters.add(new BasicNameValuePair("fname", account.getFname()));
		parameters.add(new BasicNameValuePair("lname", account.getLname()));
		parameters.add(new BasicNameValuePair("email", account.getEmail()));
		parameters.add(new BasicNameValuePair("address", account.getAddress()));
		parameters.add(new BasicNameValuePair("dob", account.getDob()));
		parameters.add(new BasicNameValuePair("city", account.getCity()));
		parameters.add(new BasicNameValuePair("county", account.getCounty()));
		parameters.add(new BasicNameValuePair("country", account.getCountry()));
		parameters.add(new BasicNameValuePair("phone", account.getPhone()));
		parameters.add(new BasicNameValuePair("gender", account.getGender()));
		parameters
				.add(new BasicNameValuePair("postcode", account.getPostcode()));
		parameters.add(new BasicNameValuePair("device_id", account
				.getDeviceid()));

		return parameters;
	}

	public static List<NameValuePair> createRegisterFBparam(Context context,
			ClientInfo fb) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("username", fb.getUsername()));
		parameters.add(new BasicNameValuePair("fbid", fb.getFbid()));
		parameters.add(new BasicNameValuePair("fname", fb.getFname()));
		parameters.add(new BasicNameValuePair("lname", fb.getLname()));
		parameters.add(new BasicNameValuePair("email", fb.getEmail()));
		parameters.add(new BasicNameValuePair("dob", fb.getDob()));
		parameters.add(new BasicNameValuePair("timezone", fb.getTimezone()));
		parameters.add(new BasicNameValuePair("fullname", fb.getFullname()));
		parameters.add(new BasicNameValuePair("locale", fb.getLocale()));
		parameters.add(new BasicNameValuePair("gender", fb.getGender()));
		parameters.add(new BasicNameValuePair("link", fb.getLink()));
		parameters.add(new BasicNameValuePair("updated_time", fb
				.getUpdatedtime()));
		parameters.add(new BasicNameValuePair("image", fb.getImage()));
		parameters.add(new BasicNameValuePair("device_id", fb.getDeviceid()));
		parameters.add(new BasicNameValuePair("type", fb
				.getType()));

		return parameters;
	}

	public static List<NameValuePair> createUserParams(Context context,
			UserObj user) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("username", user.getUsername()));
		parameters.add(new BasicNameValuePair("password", user.getPassword()));
		parameters.add(new BasicNameValuePair("fname", user.getFname()));
		parameters.add(new BasicNameValuePair("lname", user.getLname()));
		parameters.add(new BasicNameValuePair("email", user.getEmail()));
		parameters.add(new BasicNameValuePair("address", user.getAddress()));
		parameters.add(new BasicNameValuePair("dob", user.getDob()));
		parameters.add(new BasicNameValuePair("city", user.getCity()));
		parameters.add(new BasicNameValuePair("county", user.getCounty()));
		parameters.add(new BasicNameValuePair("country", user.getCountry()));
		parameters.add(new BasicNameValuePair("phone", user.getPhone()));
		parameters.add(new BasicNameValuePair("notes", user.getNotes()));
		parameters.add(new BasicNameValuePair("postcode", user.getPostcode()));
		parameters.add(new BasicNameValuePair("role", user.getRole()));
		parameters.add(new BasicNameValuePair("status", user.getStatus()));

		return parameters;
	}

	// create deals
	public static List<NameValuePair> createDealsParams(Context context,
			DealObj deal) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("title", deal.getTitle()));
		parameters.add(new BasicNameValuePair("company", deal.getCompany()));
		parameters
				.add(new BasicNameValuePair("category", deal.getCategory_id()));
		parameters.add(new BasicNameValuePair("image", deal.getImage()));
		parameters.add(new BasicNameValuePair("latitude", Double.toString(deal
				.getLatitude())));
		parameters.add(new BasicNameValuePair("longitude", Double.toString(deal
				.getLongitude())));
		parameters
				.add(new BasicNameValuePair("startDate", deal.getStart_date()));
		parameters.add(new BasicNameValuePair("endDate", deal.getEnd_date()));
		parameters
				.add(new BasicNameValuePair("startTime", deal.getStart_time()));
		parameters.add(new BasicNameValuePair("endTime", deal.getEnd_time()));
		parameters.add(new BasicNameValuePair("address", deal.getAddress()));
		parameters.add(new BasicNameValuePair("city", deal.getCity()));
		parameters.add(new BasicNameValuePair("county", deal.getCounty()));
		parameters.add(new BasicNameValuePair("postcode", deal.getPostcode()));
		parameters.add(new BasicNameValuePair("country", deal.getCountry()));
		parameters.add(new BasicNameValuePair("startValue", Double
				.toString(deal.getStart_value())));
		parameters.add(new BasicNameValuePair("discount", Integer.toString(deal
				.getDiscount())));
		parameters.add(new BasicNameValuePair("after_discount_value", Double
				.toString(deal.getAfter_discount_value())));
		parameters.add(new BasicNameValuePair("saveValue", Double.toString(deal
				.getSave_value())));
		parameters.add(new BasicNameValuePair("dealUrl", deal.getDeal_url()));
		parameters.add(new BasicNameValuePair("description", deal
				.getDescription()));
		parameters.add(new BasicNameValuePair("username", deal.getUsername()));
		parameters.add(new BasicNameValuePair("tnc", deal.getTnc_id()));
		parameters.add(new BasicNameValuePair("notes", deal.getTnc_notes()));
		return parameters;

	}

	// create category

	public static List<NameValuePair> createCategoryParams(Context context,
			CategoryObj category) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("category_name", category
				.getCategoryName()));
		parameters.add(new BasicNameValuePair("category_image", category
				.getCategoryImage()));
		return parameters;
	}
	// forget password
	public static List<NameValuePair> forgetpasswordParams(Context context,
			String email) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("email", email));
		return parameters;
	}

	// register device
	public static List<NameValuePair> registerDeviceParams(Context context,
			String ime, String gcm_id) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("gcm_id", gcm_id));
		parameters.add(new BasicNameValuePair("ime", ime));
		parameters.add(new BasicNameValuePair("status", "1"));
		parameters.add(new BasicNameValuePair("clientname", ""));
		return parameters;
	}

	// register tickets

	public static List<NameValuePair> registerTicketsParams(Context context,
			PaidticketsObj tickets) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("transactionId", tickets
				.getTran_id()));
		parameters.add(new BasicNameValuePair("user", tickets.getUserId()));
		parameters.add(new BasicNameValuePair("client", tickets.getClientId()));
		parameters
				.add(new BasicNameValuePair("event_id", tickets.getEventId()));
		parameters.add(new BasicNameValuePair("amount", tickets.getAmount()));
		parameters.add(new BasicNameValuePair("used_date", tickets
				.getUsed_date()));
		parameters.add(new BasicNameValuePair("pay_method", tickets
				.getPayMethod()));
		parameters.add(new BasicNameValuePair("status", Integer
				.toString(tickets.getStatus())));
		return parameters;
	}

	// user purchase

	public static List<NameValuePair> purchaseParams(Context context,
			PurchaseObj purchaseObj) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("username", purchaseObj
				.getUsername()));
		parameters.add(new BasicNameValuePair("admin_id", purchaseObj
				.getAdmin_id()));
		parameters.add(new BasicNameValuePair("client_email", purchaseObj
				.getClient_email()));
		parameters.add(new BasicNameValuePair("userType", purchaseObj
				.getUserType()));
		parameters.add(new BasicNameValuePair("deal_id", purchaseObj
				.getDeal_id()));
		parameters.add(new BasicNameValuePair("paymentType", purchaseObj
				.getPaymentType()));
		parameters
				.add(new BasicNameValuePair("amount", purchaseObj.getAmount()));
		parameters.add(new BasicNameValuePair("quantity", Integer
				.toString(purchaseObj.getQuantity())));
		parameters.add(new BasicNameValuePair("status", Integer
				.toString(purchaseObj.getStatus())));
		parameters.add(new BasicNameValuePair("notes", purchaseObj.getNotes()));
		parameters.add(new BasicNameValuePair("currency", purchaseObj
				.getCurrency()));
		parameters.add(new BasicNameValuePair("used_date", purchaseObj
				.getUsed_date()));
		parameters
				.add(new BasicNameValuePair("txn_id", purchaseObj.getTxn_id()));
		parameters.add(new BasicNameValuePair("createDate", purchaseObj
				.getCreateDate()));
		return parameters;
	}

	// create questions

	public static List<NameValuePair> questionsParams(Context context,
			QuestionInfo questionInfo, String check) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("userId", questionInfo
				.getUserId()));
		parameters.add(new BasicNameValuePair("question_name", questionInfo
				.getQuestion_name()));
		parameters.add(new BasicNameValuePair("question_subtext", questionInfo
				.getQuestion_subtext()));
		parameters.add(new BasicNameValuePair("surveyId", questionInfo
				.getSurveyId()));
		parameters.add(new BasicNameValuePair("answer_required", questionInfo
				.getAnswer_required()));
		parameters.add(new BasicNameValuePair("option_group_id", questionInfo
				.getOption_group_id()));
		parameters
				.add(new BasicNameValuePair("type", questionInfo.getTypeId()));
		parameters.add(new BasicNameValuePair("check", check));
		parameters
				.add(new BasicNameValuePair("allow", questionInfo.getAllow()));
		return parameters;
	}

	// create answer
	public static List<NameValuePair> answerParams(Context context,
			String option_group_id, String answer, String check) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("option_group_id",
				option_group_id));
		parameters.add(new BasicNameValuePair("answer", answer));
		parameters.add(new BasicNameValuePair("check", check));
		return parameters;
	}

	// create tnc

	public static List<NameValuePair> createTnCParams(Context context,
			TnCobj tnc) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("username", tnc.getUsername()));
		parameters.add(new BasicNameValuePair("tnc_desc", tnc.getTncDesc()));
		parameters.add(new BasicNameValuePair("notes", tnc.getNotes()));
		return parameters;
	}

	// check attending
	public static List<NameValuePair> createCheckAttending(Context context,
			String device, String dealId) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("clientname", device));
		parameters.add(new BasicNameValuePair("deal_id", dealId));

		return parameters;
	}

	public static List<NameValuePair> createAttendedParams(Context context,
			AttendedObj attend) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("attendedid", attend
				.getAttendedId()));
		parameters.add(new BasicNameValuePair("clientname", attend
				.getClientname()));
		parameters.add(new BasicNameValuePair("dealid", attend.getDealId()));
		parameters
				.add(new BasicNameValuePair("device_id", attend.getDeviceId()));
		parameters
				.add(new BasicNameValuePair("statusid", attend.getStatusId()));
		parameters.add(new BasicNameValuePair("tnc_id", attend.getTncId()));
		parameters
				.add(new BasicNameValuePair("username", attend.getUsername()));

		return parameters;
	}

	public static List<NameValuePair> createUpdateAccountParams(
			Context context, String clientname, ClientInfo account) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("username", clientname));
		parameters.add(new BasicNameValuePair("fname", account.getFname()));
		parameters.add(new BasicNameValuePair("lname", account.getLname()));
		parameters.add(new BasicNameValuePair("email", account.getEmail()));
		parameters.add(new BasicNameValuePair("address", account.getAddress()));
		parameters.add(new BasicNameValuePair("dob", account.getDob()));
		parameters.add(new BasicNameValuePair("city", account.getCity()));
		parameters.add(new BasicNameValuePair("county", account.getCounty()));
		parameters.add(new BasicNameValuePair("country", account.getCountry()));
		parameters.add(new BasicNameValuePair("phone", account.getPhone()));
		parameters.add(new BasicNameValuePair("gender", account.getGender()));
		parameters
				.add(new BasicNameValuePair("postcode", account.getPostcode()));

		return parameters;
	}

	//
	public static List<NameValuePair> createUpdateUserParams(Context context,
			String clientname, UserObj user) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("username", clientname));
		parameters.add(new BasicNameValuePair("fname", user.getFname()));
		parameters.add(new BasicNameValuePair("lname", user.getLname()));
		parameters.add(new BasicNameValuePair("email", user.getEmail()));
		parameters.add(new BasicNameValuePair("address", user.getAddress()));
		parameters.add(new BasicNameValuePair("dob", user.getDob()));
		parameters.add(new BasicNameValuePair("city", user.getCity()));
		parameters.add(new BasicNameValuePair("county", user.getCounty()));
		parameters.add(new BasicNameValuePair("country", user.getCountry()));
		parameters.add(new BasicNameValuePair("phone", user.getPhone()));
		parameters.add(new BasicNameValuePair("pay_info", user.getPayInfo()));
		parameters.add(new BasicNameValuePair("postcode", user.getPostcode()));

		return parameters;
	}

	// update deal
	public static List<NameValuePair> createUpdateEventParams(Context context,
			String dealId, DealObj deal) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("deal_id", dealId));
		parameters
				.add(new BasicNameValuePair("category", deal.getCategory_id()));
		parameters.add(new BasicNameValuePair("title", deal.getTitle()));
		parameters.add(new BasicNameValuePair("company", deal.getCompany()));
		parameters.add(new BasicNameValuePair("latitude", Double.toString(deal
				.getLatitude())));
		parameters.add(new BasicNameValuePair("longitude", Double.toString(deal
				.getLongitude())));
		parameters
				.add(new BasicNameValuePair("startDate", deal.getStart_date()));
		parameters.add(new BasicNameValuePair("endDate", deal.getEnd_date()));
		parameters
				.add(new BasicNameValuePair("startTime", deal.getStart_time()));
		parameters.add(new BasicNameValuePair("endTime", deal.getEnd_time()));
		parameters.add(new BasicNameValuePair("address", deal.getAddress()));
		parameters.add(new BasicNameValuePair("city", deal.getCity()));
		parameters.add(new BasicNameValuePair("county", deal.getCounty()));
		parameters.add(new BasicNameValuePair("postcode", deal.getPostcode()));
		parameters.add(new BasicNameValuePair("country", deal.getCountry()));
		parameters.add(new BasicNameValuePair("startValue", Double
				.toString(deal.getStart_value())));
		parameters.add(new BasicNameValuePair("discount", Integer.toString(deal
				.getDiscount())));
		parameters.add(new BasicNameValuePair("after_discount_value", Double
				.toString(deal.getAfter_discount_value())));
		parameters.add(new BasicNameValuePair("saveValue", Double.toString(deal
				.getSave_value())));
		parameters.add(new BasicNameValuePair("dealUrl", deal.getDeal_url()));
		parameters.add(new BasicNameValuePair("description", deal
				.getDescription()));
		parameters.add(new BasicNameValuePair("username", deal.getUsername()));
		parameters.add(new BasicNameValuePair("tnc", deal.getTnc_id()));
		parameters.add(new BasicNameValuePair("notes", deal.getTnc_notes()));

		return parameters;
	}

	//
	public static List<NameValuePair> editTnCParams(Context context,
			String tncId, TnCobj tnc) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("tnc_id", tncId));
		parameters.add(new BasicNameValuePair("tnc_desc", tnc.getTncDesc()));
		parameters.add(new BasicNameValuePair("notes", tnc.getNotes()));

		return parameters;
	}

	public static List<NameValuePair> updateGcmParams(Context context,
			String ime, GcmObj gcm) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("gcm_id", gcm.getGcm_id()));
		parameters.add(new BasicNameValuePair("ime", ime));
		parameters.add(new BasicNameValuePair("status", Integer.toString(gcm
				.getStatus())));
		parameters
				.add(new BasicNameValuePair("clientname", gcm.getClientname()));

		return parameters;
	}

	// winner
	public static List<NameValuePair> winnerParams(Context context, String id,
			String note, String value, int type) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("deal_id", id));
		parameters.add(new BasicNameValuePair("note", note));
		parameters.add(new BasicNameValuePair("value", value));
		parameters.add(new BasicNameValuePair("type", Integer.toString(type)));
		return parameters;
	}

	// update tickets
	public static List<NameValuePair> updateTicketsParams(Context context,
			String id, String status) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("id", id));
		parameters.add(new BasicNameValuePair("status", status));

		return parameters;
	}

	public static List<NameValuePair> createChangePassParams(Context context,
			String clientname, String oldPass, String newPass) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("clientname", clientname));
		parameters.add(new BasicNameValuePair("oldpassword", oldPass));
		parameters.add(new BasicNameValuePair("newpassword", newPass));
		return parameters;
	}

	// Active admin params
	public static List<NameValuePair> createActiveAdminParams(Context context,
			String userName, String active) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("username", userName));
		parameters.add(new BasicNameValuePair("status", active));
		return parameters;
	}

	public static List<NameValuePair> createNormalAdminParams(Context context,
			String userName) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("username", userName));
		return parameters;
	}

	public static List<NameValuePair> createDealIdParams(Context context,
			String dealId) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("deal_id", dealId));
		return parameters;
	}

	public static List<NameValuePair> createUpcomingDealParams(Context context,
			String distance, String lat, String lng) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("distance", distance));
		parameters.add(new BasicNameValuePair("lat", lat));
		parameters.add(new BasicNameValuePair("lng", lng));
		return parameters;
	}
}