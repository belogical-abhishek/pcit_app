package com.pcits.events.config.ads;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class Ads {
	public static void loadAds(AdView ads) {

		/**
		 * code below is used to test admob on device during development
		 * process.
		 */
try {
	AdRequest adRequest = new AdRequest.Builder().addTestDevice(
			"YOUR_DEVICE_ID").build();
	ads.loadAd(adRequest);

}catch (Exception e){e.printStackTrace();}
		// AdRequest adRequest = new AdRequest.Builder().build();
		/**
		 * code below is used to publish admob when the app launched. remove the
		 * comment tag below and delete block of code that used for testing
		 * admob.
		 */


		/**
		 * the end of admob code
		 */
	}
}
