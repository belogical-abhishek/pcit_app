/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pcits.events.wizard.model;

import java.util.ArrayList;

import android.support.v4.app.Fragment;

import com.pcits.common.utils.Logging;
import com.pcits.events.fragments.AddEventPage2Fragment;

/**
 * A page asking for a name and an email.
 */
public class CustomAddEventPage2 extends Page implements ICustomPage {
	public static final String BEFORE_DISCOUNT_DATA_KEY = "beforeDiscount";
	public static final String AFTER_DISCOUNT_DATA_KEY = "afterDiscount";
	public static final String START_DATE_DATA_KEY = "startDate";
	public static final String START_TIME_DATA_KEY = "startTime";
	public static final String END_DATE_DATA_KEY = "endDate";
	public static final String END_TIME_DATA_KEY = "endTime";

	private AddEventPage2Fragment mFragment;

	public CustomAddEventPage2(ModelCallbacks callbacks, String title) {
		super(callbacks, title);
	}

	@Override
	public Fragment createFragment() {
		mFragment = AddEventPage2Fragment.create(getKey());
		return mFragment;
	}

	@Override
	public void getReviewItems(ArrayList<ReviewItem> dest) {
		// dest.add(new ReviewItem("Before discount", mData
		// .getString(BEFORE_DISCOUNT_DATA_KEY), getKey(), -1));
		// dest.add(new ReviewItem("After discount", mData
		// .getString(AFTER_DISCOUNT_DATA_KEY), getKey(), -1));
		// dest.add(new ReviewItem("Start date", mData
		// .getString(START_DATE_DATA_KEY), getKey(), -1));
		// dest.add(new ReviewItem("Start time", mData
		// .getString(START_TIME_DATA_KEY), getKey(), -1));
		// dest.add(new ReviewItem("End date",
		// mData.getString(END_DATE_DATA_KEY),
		// getKey(), -1));
		// dest.add(new ReviewItem("End time",
		// mData.getString(END_TIME_DATA_KEY),
		// getKey(), -1));
	}

	@Override
	public boolean isCompleted() {
		// boolean completed = false;
		// if (TextUtils.isEmpty(mData.getString(BEFORE_DISCOUNT_DATA_KEY))
		// || TextUtils.isEmpty(mData.getString(AFTER_DISCOUNT_DATA_KEY))
		// || TextUtils.isEmpty(mData.getString(START_DATE_DATA_KEY))
		// || TextUtils.isEmpty(mData.getString(START_TIME_DATA_KEY))
		// || TextUtils.isEmpty(mData.getString(END_DATE_DATA_KEY))
		// || TextUtils.isEmpty(mData.getString(END_TIME_DATA_KEY))) {
		// completed = false;
		// } else {
		// completed = true;
		// }
		// return completed;
		return true;
	}

	@Override
	public void saveState() {
		try {
			mFragment.saveState();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	@Override
	public void restoreState() {
		try {
			mFragment.restoreState();
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}
}
