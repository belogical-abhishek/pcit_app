package com.pcits.events.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.pcits.common.utils.GsonUtility;
import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityDetail;
import com.pcits.events.ActivityHome;
import com.pcits.events.AddEventActivity;
import com.pcits.events.R;
import com.pcits.events.adapters.HomeAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.libraries.JSONParser;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.obj.APIObj;
import com.pcits.events.obj.DealObj;
import com.pcits.events.obj.ListEventObj;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class HistoricalPublicEventFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "HistoricalPublicEventHome";
    private ArrayList<DealObj> mArrDeals;
    private UserFunctions userFunction;
    private Utils utils;
    private ListView list;
    private TextView lblNoResult;
    private HomeAdapter mla;
    private Button btnLoadMore;
    private LinearLayout lytRetry;
    private JSONObject json, jsonCurrency;
    private int mCurrentPage = 0;
    private int mPreviousPage;
    private int intLengthData;
    public static Activity self;
    private PullToRefreshListView mPullRefreshListView;
    private View v;
    public static LinearLayout llMenu;
    private String client;
    private DealObj mDealObj;
    private int mClickedPos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        self = getActivity();
        v = inflater.inflate(R.layout.fragment_historical_public_event, container, false);

        try
        {
            if (mArrDeals == null)
            {
                mArrDeals = new ArrayList<DealObj>();
            }
            mArrDeals.clear();
            lblNoResult = (TextView) v.findViewById(R.id.lblNoResult);
            lytRetry = (LinearLayout) v.findViewById(R.id.llRetry);
            llMenu = (LinearLayout) v.findViewById(R.id.llMenu);
            llMenu.setOnClickListener(this);
            userFunction = new UserFunctions();
            utils = new Utils(getActivity());
            // Create LoadMore button
         /*   LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.WRAP_CONTENT,
        LinearLayout.LayoutParams.WRAP_CONTENT
);
params.setMargins(1, 10, 1, 1);*/

            btnLoadMore = new Button(getActivity());
          //  btnLoadMore.setLayoutParams(params);
            btnLoadMore.setBackgroundResource(R.drawable.apptheme_btn_default_holo_light);
            btnLoadMore.setText(getString(R.string.btn_load_more));
            btnLoadMore.setTextColor(getResources().getColor(R.color.text_btn));
            // Load data
            mPullRefreshListView = (PullToRefreshListView) v.findViewById(R.id.lsvPullToRefresh);
            list = mPullRefreshListView.getRefreshableView();
            new loadFirstListView().execute();

            // Set a listener to be invoked when the list should be refreshed.
            if (NetworkUtility.getInstance(self).isNetworkAvailable())
            {
                mPullRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
                {
                    @Override
                    public void onRefresh(PullToRefreshBase<ListView> refreshView)
                    {

                        String label = DateUtils.formatDateTime(
                                getActivity(),
                                System.currentTimeMillis(),
                                DateUtils.FORMAT_SHOW_TIME
                                        | DateUtils.FORMAT_SHOW_DATE
                                        | DateUtils.FORMAT_ABBREV_ALL);

                        // Update the LastUpdatedLabel
                        refreshView.getLoadingLayoutProxy()
                                .setLastUpdatedLabel(label);
                        new loadFirstListView().execute();

                    }
                });
            }
            else
            {
                Toast.makeText(getActivity(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            }

            // Listener to handle load more buttton when clicked
            btnLoadMore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0)
                {
                    // Starting a new async task
                    if (NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
                        json = null;
                        new loadMoreListView().execute();
                    }
                    else
                    {
                        Toast.makeText(getActivity(),
                                getString(R.string.no_internet_connection),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });

            // Add an end-of-list listener
            // Listener to get selected id when list item clicked
            list.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
                {
                    if((position-1)<mArrDeals.size())
                    {
                        // TODO Auto-generated method stub
                        GlobalValue.dealsObj = mArrDeals.get(position - 1);

                        DatabaseUtility.insertDeals(getActivity(), mArrDeals.get(position - 1));

                        ModelManager.updateClickCount(getActivity(),
                                mArrDeals.get(position - 1).getDeal_id(), true,
                                new ModelManagerListener()
                                {

                                    @SuppressLint("LongLogTag")
                                    @Override
                                    public void onSuccess(Object object)
                                    {
                                        try
                                        {
                                            // TODO Auto-generated method stub
                                            String json = (String) object;
                                            boolean result = ParserUtility.updateAccount(json);

                                            if (result)
                                            {
                                                Log.d(TAG, "Successfully");
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            throw new RuntimeException(ex);
                                        }
                                    }

                                    @Override
                                    public void onError()
                                    {
                                        // TODO Auto-generated method stub

                                    }
                                });

                        String dealId = GlobalValue.dealsObj.getDeal_id();
                        setStatusPrivateEvent(getActivity(),"Event_List_Status",dealId);
                        Intent i = new Intent(getActivity(), ActivityDetail.class);
                        mla.setVisitStatus(dealId, true);
                        startActivity(i);
                        getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        list.setItemChecked(position, true);
                    }
                }
            });

            list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
            {

                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
                {
                    // TODO Auto-generated method stub
                    if (GlobalValue.myClient != null || GlobalValue.myUser != null)
                    {
                        try
                        {
                            mDealObj = (DealObj) mArrDeals.get(position - 1).clone();
                        }
                        catch (Exception ex)
                        {
                            // TODO Auto-generated catch block
                            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
                        }
                        showOperationDialog();
                        mClickedPos = position - 1;
                    }
                    return true;
                }
            });

            // initNotBoringActionBar();

            return v;
        }
        catch (Exception ex)
        {
            if (Logging.debugFile.exists())
            {
                ex.printStackTrace();
            }
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
            return v;
        }
    }

    private void showOperationDialog()
    {
        final Dialog dialog = new Dialog(self);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_operation_event);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        TextViewRobotoCondensedRegular copy = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_copy);
        TextViewRobotoCondensedRegular delete = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_delete);
        TextViewRobotoCondensedRegular revoke = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_revoke);
        TextViewRobotoCondensedRegular scan = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_scan);
        TextViewRobotoCondensedRegular edit = (TextViewRobotoCondensedRegular) dialog
                .findViewById(R.id.lbl_edit);

        // Show/hide operation
        if (GlobalValue.myUser == null)
        {
            if (!GlobalValue.myClient.getUsername().equals(mDealObj.getUsername()))
            {
                delete.setVisibility(View.GONE);
                revoke.setVisibility(View.GONE);
                scan.setVisibility(View.GONE);
                edit.setVisibility(View.GONE);
            }
        }

        // Set listener
        copy.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Open create event page with event copied
                AddEventActivity.isDeal = true;
                mDealObj.setDeal_id(null);
                try {
                    mDealObj.setStart_date(""+ DateTimeUtility.getDateFormat(mDealObj.getStart_date(),"dd MMM yy","yyyy-MM-dd"));
                    mDealObj.setEnd_date(""+DateTimeUtility.getDateFormat(mDealObj.getEnd_date(),"dd MMM yy","yyyy-MM-dd"));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                GlobalValue.dealsObj = mDealObj;
                Intent i = new Intent(getActivity(), AddEventActivity.class);
                startActivity(i);

                // Dismiss the dialog
                dialog.dismiss();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(self, "Deleted", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        revoke.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(self, "Revoked", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        scan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(self, "Scanned", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                // Open edit event page
                // Intent i = new Intent(self, ActivityEditDeal.class);
                AddEventActivity.isDeal = true;
                try {
                    mDealObj.setStart_date(""+DateTimeUtility.getDateFormat(mDealObj.getStart_date(),"dd MMM yy","yyyy-MM-dd"));
                    mDealObj.setEnd_date(""+DateTimeUtility.getDateFormat(mDealObj.getEnd_date(),"dd MMM yy","yyyy-MM-dd"));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                GlobalValue.dealsObj = mDealObj;


                Log.d(TAG, "onClick: "+ GsonUtility.convertObjectToJSONString(GlobalValue.dealsObj));
                Intent i = new Intent(getActivity(), AddEventActivity.class);
                startActivity(i);
                // Dismiss dialog
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        mDealObj = null;

        // Refresh auto when update or create new event.
        if (mla != null) {
            if (AddEventActivity.isUpdated) {
                if (AddEventActivity.dealObj != null) {
                    // Replace event which just updated.
                    mArrDeals.add(mClickedPos, AddEventActivity.dealObj);
                    AddEventActivity.dealObj = null;

                    // Refresh view
                    mla.notifyDataSetChanged();
                }

                AddEventActivity.isUpdated = false;
            } else if (AddEventActivity.isCreated) {
                if (AddEventActivity.dealObj != null) {
                    // Create temp array.
                    ArrayList<DealObj> arrTemp = new ArrayList<DealObj>();
                    arrTemp.addAll(mArrDeals);

                    // Clear old data
                    mArrDeals.clear();

                    // Add new event which just created to first item.
                    mArrDeals.add(AddEventActivity.dealObj);

                    // Append the old data
                    mArrDeals.addAll(arrTemp);

                    // Clear temp data
                    arrTemp.clear();

                    // Refresh view
                    mla.notifyDataSetChanged();

                    // Scroll on top
                    list.smoothScrollToPositionFromTop(0, 0);

                    AddEventActivity.dealObj = null;
                }

                AddEventActivity.isCreated = false;
            }
        }
    }

    // Load first 10 videos
    private class loadFirstListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            // Clear old data
            mArrDeals.clear();
            mCurrentPage = 0;

            ActivityHome.rltProgress.setVisibility(View.VISIBLE);
        }

        protected Void doInBackground(Void... unused) {
            try {
                if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
                    getDataFromServer();
                } else {
                    getDataFromDB();
                }
            } catch (Exception ex) {
                Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
            }
            return null;
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        protected void onPostExecute(Void unused) {
            // Condition if data length uder 10 button loadMore is remove
            list.removeFooterView(btnLoadMore);
            if (intLengthData < UserFunctions.valueItemsPerPage) {
                list.removeFooterView(btnLoadMore);
            } else {
                list.addFooterView(btnLoadMore);
            }
            if (isAdded()) {
                if (mArrDeals.size() != 0) {
                    // Check parameter notify
                    int paramNotif = utils
                            .loadPreferences(utils.UTILS_PARAM_NOTIF);

                    // Condition if app start in the first time notif will run
                    // in background
                    if (paramNotif != 1)
                    {
                        utils.saveString(utils.UTILS_NOTIF, mArrDeals.get(0)
                                .getDeal_id());
                        utils.savePreferences(utils.UTILS_PARAM_NOTIF, 1);
                       // startService();
                    }

                    // Adding load more button to lisview at bottom
                    lytRetry.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);
                    lblNoResult.setVisibility(View.GONE);
                    // Getting adapter
                    ArrayList<DealObj> mArrDeals2=new ArrayList<>();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yy");
                    String currentDateandTime = sdf.format(new Date());

                    for(DealObj l:mArrDeals)
                    {
                        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                        Date strDate = null,strCdate=null;
                        try
                        {
                            strDate = sdf.parse(l.getEnd_date());
                            strCdate=sdf.parse(currentDateandTime);
                            if (strCdate.after(strDate))
                            {
                                mArrDeals2.add(l);
                            }
                        }
                        catch (ParseException e)
                        {
                            e.printStackTrace();
                        }

                    }
                    List<String> status=null;
                    try
                    {
                        mArrDeals=mArrDeals2;
                        list.removeFooterView(btnLoadMore);
                        status=getStringArrayPref(getActivity(),"Event_List_Status",mArrDeals);
                        mla = new HomeAdapter(getActivity(), mArrDeals,status);
                        mla.notifyDataSetChanged();
                        list.setAdapter(mla);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    mPullRefreshListView.onRefreshComplete();
                    if (mla == null)
                    {
                        list.removeFooterView(btnLoadMore);
                        mla = new HomeAdapter(getActivity(), mArrDeals,status);
                        list.setAdapter(mla);
                    }
                    else
                        {
                        mla.notifyDataSetChanged();
                    }

                } else {
                    list.removeFooterView(btnLoadMore);
                    if (json != null) {
                        lblNoResult.setVisibility(View.VISIBLE);
                        lytRetry.setVisibility(View.GONE);

                    } else {
                        lblNoResult.setVisibility(View.GONE);
                        lytRetry.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(),
                                getString(R.string.no_connection),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
            try {
                super.onPostExecute(unused);
                mPullRefreshListView.onRefreshComplete();
                // Closing progress dialog
                ActivityHome.rltProgress.setVisibility(View.GONE);
            } catch (NullPointerException ex) {
                if (Logging.debugFile.exists()) {
                    ex.printStackTrace();
                }
                throw new RuntimeException(ex);
            } catch (Exception ex) {
                if (Logging.debugFile.exists()) {
                    ex.printStackTrace();
                }
                throw new RuntimeException(ex);
            }
        }
    }

    // Load more videos
    private class loadMoreListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            ActivityHome.rltProgress.setVisibility(View.VISIBLE);
        }

        protected Void doInBackground(Void... unused) {
            try {
                // Store previous value of current page
                mPreviousPage = mCurrentPage;
                // Increment current page
                mCurrentPage += UserFunctions.valueItemsPerPage;
                if (NetworkUtility.getInstance(getActivity())
                        .isNetworkAvailable()) {
                    getDataFromServer();
                } else {
                    getDataFromDB();
                }
            } catch (Exception ex) {
                Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
            }
            return (null);
        }

        protected void onPostExecute(Void unused) {
            // Condition if data length under 10 button loadMore is remove
            if ((intLengthData % UserFunctions.valueItemsPerPage) != 0
                    || intLengthData >= GlobalValue.MAX_EVENT) {
                list.removeFooterView(btnLoadMore);
            }
            if (json != null) {
                // Get listview current position - used to maintain scroll
                // position
                int currentPosition = list.getFirstVisiblePosition();
                lytRetry.setVisibility(View.GONE);
                // Appending new data to menuItems ArrayList
                mla.notifyDataSetChanged();
                // Setting new scroll position
                list.setSelectionFromTop(currentPosition + 1, 0);

            } else {
                if (mArrDeals != null) {
                    mCurrentPage = mPreviousPage;
                    lytRetry.setVisibility(View.GONE);
                } else {
                    lytRetry.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(),
                            getString(R.string.no_connection),
                            Toast.LENGTH_SHORT).show();
                }

            }
            ActivityHome.rltProgress.setVisibility(View.GONE);
        }
    }

    // Method to get Data from Server
    public void getDataFromServer() {

        try
        {
            if (GlobalValue.myClient != null)
            {
                client = GlobalValue.myClient.getUsername();
            }
            else
                {
                client = "";
            }
            // jsonCurrency = userFunction.currency(getActivity());
            json = userFunction.latestDeals(client, mCurrentPage, getActivity());

            ArrayList<DealObj> arr = JSONParser.parserDeal("latestDeals",
                    json.toString());
            mArrDeals.addAll(arr);
            intLengthData = mArrDeals.size();

        } catch (NullPointerException ex) {
            if (Logging.debugFile.exists()) {
                ex.printStackTrace();
            }
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            if (Logging.debugFile.exists()) {
                ex.printStackTrace();
            }
            throw new RuntimeException(ex);
        }
    }

    public void getDataFromDB() {
        try {
            String apiDeals = UserFunctions.URL_ALL_DEALS + mCurrentPage;
            APIObj allDealInfos = DatabaseUtility.getResuftFromApi(
                    getActivity(), apiDeals);
            String jsonAllDeal = "";
            if (allDealInfos != null) {
                jsonAllDeal = allDealInfos.getmResult();
            }
            ArrayList<DealObj> arr = JSONParser.parserDeal("latestDeals",
                    jsonAllDeal.toString());
            mArrDeals.addAll(arr);
            intLengthData = mArrDeals.size();

        } catch (NullPointerException ex) {
            if (Logging.debugFile.exists()) {
                ex.printStackTrace();
            }
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            if (Logging.debugFile.exists()) {
                ex.printStackTrace();
            }
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        // list.setAdapter(null);
        super.onDestroy();

    }
    @Override
    public void onClick(View v)
    {
        if (v == llMenu) {
            ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
        }
    }

    private void setStatusPrivateEvent(android.content.Context context, String key,String deal_id)
    {
        //Event_List_Status-key
        try
        {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = prefs.edit();
            JSONArray a;
            try
            {
                String json = prefs.getString(key, null);
                a = new JSONArray(json);
            }
            catch (Exception e)
            {
                a = new JSONArray();
                e.printStackTrace();
            }

            ListEventObj listEventObj = null;
            JSONObject b=new JSONObject();

            try
            {
                b.put("event_id", deal_id);

            }
            catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            a.put(b);

            if (a!=null)
            {
                editor.putString(key, a.toString());
            }
            else
            {
                editor.putString(key, null);
            }
            editor.commit();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public List<String> getStringArrayPref(Context context, String key, List<DealObj> list)
    {
        List<String> listEventStatus=new ArrayList<>(list.size());
        for (int i = 0; i < list.size(); i++)
        {
            listEventStatus.add("0");
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = prefs.getString(key, null);
        List<ListEventObj> listEvents=new ArrayList<>();
        if (json != null)
        {
            try
            {
                JSONArray a = null;
                try
                {
                    a = new JSONArray(json);
                    for (int i = 0; i < list.size(); i++)
                    {
                        DealObj ss=list.get(i);
                        for(int j=0;j<a.length();j++)
                        {
                            JSONObject jsonObject=a.getJSONObject(j);
                            if(ss.getDeal_id().equalsIgnoreCase(jsonObject.getString("event_id")))
                            {
                                //listEventStatus.set(i,ss.getDeal_id());
                                listEventStatus.set(i,"0");
                            }
                            else
                            {
                                if(listEventStatus.get(i).equalsIgnoreCase("0"))
                                {
                                    listEventStatus.set(i,"0");
                                }
                            }

                        }
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return listEventStatus;
    }
}
