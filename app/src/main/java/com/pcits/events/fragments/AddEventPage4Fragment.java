/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pcits.events.fragments;

import java.io.InputStream;
import java.util.ArrayList;


import yuku.ambilwarna.AmbilWarnaDialog;
import yuku.ambilwarna.AmbilWarnaDialog.OnAmbilWarnaListener;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.pcits.common.utils.GsonUtility;
import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityAddTnC;
import com.pcits.events.R;
import com.pcits.events.adapters.TnCAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.obj.TnCobj;
import com.pcits.events.widgets.FloatLabeledEditText;
import com.pcits.events.widgets.floatingactionbutton.AddFloatingActionButton;
import com.pcits.events.widgets.floatingactionbutton.FloatingActionButton;
import com.pcits.events.widgets.richtext.CustomEditText;
import com.pcits.events.widgets.richtext.CustomEditText.EventBack;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;
import com.pcits.events.wizard.model.CustomAddEventPage4;
import com.pcits.events.wizard.ui.PageFragmentCallbacks;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class AddEventPage4Fragment extends Fragment implements
		OnAmbilWarnaListener {
	private final String TAG = "AddEventPage4Fragment";

	private static final String ARG_KEY = "key";

	private PageFragmentCallbacks mCallbacks;
	private String mKey;
	private CustomAddEventPage4 mPage;

	private TextViewRobotoCondensedRegular mLblTnC;
	private EditText mFlUrl;

	// Rich editor declare
	private LinearLayout llEditor;
	private CustomEditText customEditor;
	private AmbilWarnaDialog colorPickerDialog;
	private ImageView imgChangeColor;

	private int selectionStart;
	private int selectionEnd;

	private ArrayList<TnCobj> mArrTnC;
	private TnCAdapter mAdapter;

	private View mRootView;
	private Bundle mBundle;

	private FloatingActionButton mFabEditTnC;
	private AddFloatingActionButton mAfabAddTnC;
	public static TnCobj tncObj = null;

	private EventBack eventBack = new EventBack() {

		@Override
		public void close() {
			llEditor.setVisibility(View.GONE);
		}

		@Override
		public void show() {
			llEditor.setVisibility(View.VISIBLE);
		}
	};
	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (customEditor.isFocused()) {
				llEditor.setVisibility(View.VISIBLE);
			}
		}
	};

	public static AddEventPage4Fragment create(String key) {
		Bundle args = new Bundle();
		args.putString(ARG_KEY, key);

		AddEventPage4Fragment fragment = new AddEventPage4Fragment();
		fragment.setArguments(args);
		return fragment;
	}

	public AddEventPage4Fragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mBundle = savedInstanceState;
		Log.d(TAG, "onCreate: user: "+ GsonUtility.convertObjectToJSONString(GlobalValue.myUser));
		Log.d(TAG, "onCreate: user: "+GsonUtility.convertObjectToJSONString(GlobalValue.myClient));
		Bundle args = getArguments();
		mKey = args.getString(ARG_KEY);
		mPage = (CustomAddEventPage4) mCallbacks.onGetPage(mKey);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragment_add_event_page4,
				container, false);
		try{
		// ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage
		// .getTitle());

		initUI();
		} catch(Exception ex){
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
		return mRootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (!(activity instanceof PageFragmentCallbacks)) {
			throw new ClassCastException(
					"Activity must implement PageFragmentCallbacks");
		}

		mCallbacks = (PageFragmentCallbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);

		// In a future update to the support library, this should override
		// setUserVisibleHint
		// instead of setMenuVisibility.
		if (mLblTnC != null && customEditor != null) {
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			if (!menuVisible) {
				imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
			}
		}
	}

	@Override
	public void onResume() {
		try{
			// TODO Auto-generated method stub
			super.onResume();
	
			// Get Terms and Conditions async.
			if (NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
				if (GlobalValue.myUser != null) {
					new GetTnCAsync().execute();
				}
			} else {
				Toast.makeText(getActivity(), "Network is not available.",
						Toast.LENGTH_SHORT).show();
			}
	
			restoreState();
		} catch(Exception ex){
	        Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
	    }
	}

	private void initRichEditor(View v) {
		colorPickerDialog = new AmbilWarnaDialog(getActivity(), Color.BLACK,
				this);
		ToggleButton boldToggle = (ToggleButton) v.findViewById(R.id.btnBold);
		ToggleButton italicsToggle = (ToggleButton) v
				.findViewById(R.id.btnItalics);
		ToggleButton underlinedToggle = (ToggleButton) v
				.findViewById(R.id.btnUnderline);
		imgChangeColor = (ImageView) v.findViewById(R.id.btnChangeTextColor);
		llEditor = (LinearLayout) v.findViewById(R.id.lnlAction);
		llEditor.setVisibility(View.VISIBLE);

		customEditor = (CustomEditText) v.findViewById(R.id.txtDescription);
		customEditor.setSingleLine(false);
		customEditor.setMinLines(10);
		customEditor.setBoldToggleButton(boldToggle);
		customEditor.setItalicsToggleButton(italicsToggle);
		customEditor.setUnderlineToggleButton(underlinedToggle);
		customEditor.setEventBack(eventBack);
		customEditor.setOnClickListener(clickListener);
		imgChangeColor.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				selectionStart = customEditor.getSelectionStart();
				selectionEnd = customEditor.getSelectionEnd();
				colorPickerDialog.show();
			}
		});
	}

	@Override
	public void onCancel(AmbilWarnaDialog dialog) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onOk(AmbilWarnaDialog dialog, int color) {
		customEditor.setColor(color, selectionStart, selectionEnd);
		imgChangeColor.setBackgroundColor(color);
	}

	public void saveState() {
		try {
			GlobalValue.dealsObj.setDeal_url(mFlUrl.getText().toString().trim());
			GlobalValue.dealsObj.setTnc_desc(mLblTnC.getText().toString());
			GlobalValue.dealsObj.setDescription(customEditor.getTextHTML()
					.trim());
		} catch (Exception ex) {
			ex.printStackTrace();
            throw new RuntimeException(ex);
        }
	}

	public void restoreState() {
		try {
			mFlUrl.setText(GlobalValue.dealsObj.getDeal_url());
			mLblTnC.setText(GlobalValue.dealsObj.getTnc_desc());
			customEditor.setTextHTML(GlobalValue.dealsObj.getDescription());
		} catch (Exception ex) {
			ex.printStackTrace();
            throw new RuntimeException(ex);


        }
	}

	private void initUI() {
		try{
			mFlUrl = (EditText) mRootView.findViewById(R.id.txtDealUrl);
	
			mLblTnC = (TextViewRobotoCondensedRegular) mRootView
					.findViewById(R.id.lblTnC);
	
			mAfabAddTnC = (AddFloatingActionButton) mRootView
					.findViewById(R.id.fab_add);
			mFabEditTnC = (FloatingActionButton) mRootView
					.findViewById(R.id.fab_edit);
	
			try {
				if (GlobalValue.dealsObj.getTnc_desc() == null) {
					mAfabAddTnC.setVisibility(View.VISIBLE);
					mFabEditTnC.setVisibility(View.GONE);
				} else {
					mAfabAddTnC.setVisibility(View.GONE);
					mFabEditTnC.setVisibility(View.VISIBLE);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
	            throw new RuntimeException(ex);
	        }
	
			initRichEditor(mRootView);
	
			// Should call this method in the end of declaring UI.
			initControl();
			restoreState();
		} catch (Exception ex) {
			ex.printStackTrace();
            throw new RuntimeException(ex);
        }
	}

	private void initControl() {
		try{
			mLblTnC.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					if (NetworkUtility.getInstance(getActivity())
							.isNetworkAvailable()) {
						getDataListTnC();
					} else {
						Toast.makeText(getActivity(), "No network connection",
								Toast.LENGTH_SHORT).show();
					}
				}
			});
	
			mAfabAddTnC.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					Intent i = new Intent(getActivity(), ActivityAddTnC.class);
					startActivity(i);
				}
			});
	
			mFabEditTnC.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					// Get tnc object
					try {
						for (int i = 0; i < mArrTnC.size(); i++) {
							if (mArrTnC.get(i).getTncDesc()
									.equals(mLblTnC.getText().toString())) {
								tncObj = mArrTnC.get(i);
								break;
							}
						}
					} catch (NullPointerException ex) {
						Toast.makeText(getActivity(),
								"Please wait several seconds for getting data.",
								Toast.LENGTH_LONG).show();
					} catch (Exception ex) {
						ex.printStackTrace();
		                throw new RuntimeException(ex);
		            }
	
					// Open AddTnC page.
					if (tncObj != null) {
						Intent i = new Intent(getActivity(), ActivityAddTnC.class);
						i.putExtra("key", "edit");
						startActivity(i);
					} else {
						Toast.makeText(getActivity(),
								"Please choose T & C for updating.",
								Toast.LENGTH_SHORT).show();
					}
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
            throw new RuntimeException(ex);
        }
	}

	private void showTnCDialog() {
		final Dialog dialogTnc = new Dialog(getActivity());
		dialogTnc.getWindow();
		dialogTnc.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogTnc.setContentView(R.layout.layout_nice_tnc);
		final ListView lsvTnc = (ListView) dialogTnc
				.findViewById(R.id.lsvFilterCategory);
		mAdapter = new TnCAdapter(getActivity(), mArrTnC);
		lsvTnc.setAdapter(mAdapter);
		lsvTnc.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				mLblTnC.setText(mArrTnC.get(position).getTncDesc());
				dialogTnc.dismiss();

				// Set T & C
				GlobalValue.dealsObj.setTnc_id(mArrTnC.get(position)
						.getTnc_id());
				GlobalValue.dealsObj.setTnc_notes(mArrTnC.get(position)
						.getNotes());
				GlobalValue.dealsObj.setTnc_desc(mArrTnC.get(position)
						.getTncDesc());

				mAfabAddTnC.setVisibility(View.GONE);
				mFabEditTnC.setVisibility(View.VISIBLE);
			}
		});
		dialogTnc.show();
	}

	private void getDataListTnC() {
		try{
			if (mArrTnC == null || mArrTnC.size() <= 0) {
				String user = "";
				if (!GlobalValue.myUser.getRole().equals("0")) {
					user = GlobalValue.myUser.getUsername();
				}
	
				ModelManager.getListTnc(getActivity(), user,
						GlobalValue.myUser.getRole(), true,
						new ModelManagerListener() {
	
							@Override
							public void onSuccess(Object object) {
								String json = (String) object;
	
								mArrTnC = ParserUtility.getListTnc(json);
	
								showTnCDialog();
							}
	
							@Override
							public void onError() {
								// TODO Auto-generated method stub
	
							}
						});
			} else {
				showTnCDialog();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
            throw new RuntimeException(ex);
        }
	}

	private class GetTnCAsync extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			try{
				// TODO Auto-generated method stub
				String user = "";
				if (!GlobalValue.myUser.getRole().equals("0")) {
					user = GlobalValue.myUser.getUsername();
				}
				String uri = UserFunctions.URL_SHOW_TNC_BY_USER + "?username="
						+ user + "&type=" + GlobalValue.myUser.getRole();
				HttpGet httpGet = new HttpGet(uri);
				HttpClient client = new DefaultHttpClient();
				HttpResponse response;
				StringBuilder stringBuilder = new StringBuilder();
	
				try {
					response = client.execute(httpGet);
					HttpEntity entity = response.getEntity();
					InputStream stream = entity.getContent();
					int b;
					while ((b = stream.read()) != -1) {
						stringBuilder.append((char) b);
					}
				} catch (Exception ex) {
					Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
					return null;
	            }
	
				return stringBuilder.toString();
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
				return null;
            }
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			try{
				super.onPostExecute(result);
				mArrTnC = ParserUtility.getListTnc(result);
			} catch (Exception ex) {
				Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
	        }
		}
	}
}
