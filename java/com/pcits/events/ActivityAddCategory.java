package com.pcits.events;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.CategoryObj;
import com.pcits.events.utils.AccessStorage;
import com.pcits.events.widgets.KenBurnsView;

public class ActivityAddCategory extends FragmentActivity implements
		OnClickListener {

	private EditText txtCategoryName;
	private Button btnSelectImg, btnSubmit;
	private CategoryObj category = null;
	private TextView lblMessage;
	private String selectedImagePath;
	private Bitmap avatarBitmap;
	private ImageView imgCategory;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_category);
		initUI();
		initControl();

		// getWindow().setSoftInputMode(
		// WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		initNotBoringActionBar();
	}

	private void initNotBoringActionBar() {
	//	mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
	//	mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}

	private void initUI() {
		txtCategoryName = (EditText) findViewById(R.id.txtCategoryName);
		btnSelectImg = (Button) findViewById(R.id.btnSelectImgCategory);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);
		lblMessage = (TextView) findViewById(R.id.messageText);
		imgCategory = (ImageView) findViewById(R.id.imgCategory);
		llMenu = (LinearLayout) findViewById(R.id.llMenu);

	}

	private void initControl() {
		btnSelectImg.setOnClickListener(this);
		btnSubmit.setOnClickListener(this);
		llMenu.setOnClickListener(this);
	}

	private String validateForm() {
		// TODO Auto-generated method stub
		String message = "success";
		String categoryname = txtCategoryName.getText().toString();
		// String path = lblMessage.getText().toString().trim();
		// String image = encodeTobase64(path);
		if (categoryname.isEmpty()) {
			Toast.makeText(this, "Please, input Category", Toast.LENGTH_SHORT)
					.show();
			return message;
		}
		category = new CategoryObj();
		category.setCategoryName(categoryname.trim());
		return message;
	}

	@Override
	public void onClick(View v) {
		try{
			// TODO Auto-generated method stub
			if (v == llMenu) {
				finish();
			}
			if (v.getId() == R.id.btnSelectImgCategory) {
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(
						Intent.createChooser(intent, "Complete action using"), 1);
			}
			if (v.getId() == R.id.btnSubmit) {
				String message = validateForm();
				if (!message.equals("success")) {
					Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				} else {
					if (category != null) {
						addCategory(category);
					}
				}
			}
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		// if (requestCode == 1 && resultCode == RESULT_OK) {
		// // Bitmap photo = (Bitmap) data.getData().getPath();
		//
		// Uri selectedImageUri = data.getData();
		// imagepath = getPath(selectedImageUri);
		// lblMessage.setText(imagepath);
		//
		// }
		if (resultCode == RESULT_OK) {
			Toast.makeText(this, "choose image", Toast.LENGTH_SHORT).show();

			try {
				Uri uri = data.getData();
				selectedImagePath = AccessStorage.getPath(
						ActivityAddCategory.this, uri);
				avatarBitmap = AccessStorage
						.loadPrescaledBitmap(selectedImagePath);
				// avatarBitmap = CommonMethods.getResizedBitmap(avatarBitmap,
				// 200, 200);
				Drawable d = new BitmapDrawable(getResources(), avatarBitmap);
				imgCategory.setImageDrawable(d);
				lblMessage.setText(selectedImagePath);
			} catch(Exception ex){
	            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
	        }
		}
	}

	// public String getPath(Uri uri) {
	// String[] projection = { MediaStore.Images.Media.DATA };
	// Cursor cursor = managedQuery(uri, projection, null, null, null);
	// int column_index = cursor
	// .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	// cursor.moveToFirst();
	// return cursor.getString(column_index);
	// }

	private void addCategory(CategoryObj category) {
		try{
			// TODO Auto-generated method stub
			CategoryObj data = category;
			Log.d("ACCOUNT INFO", "ACCOUNT INFO : " + data);
			ModelManager.addCategory(this, data, avatarBitmap, true,
					new ModelManagerListener() {
	
						@Override
						public void onSuccess(Object object) {
							// TODO Auto-generated method stub
							String strJson = (String) object;
							Toast.makeText(ActivityAddCategory.this,
									checkResult(strJson), Toast.LENGTH_SHORT)
									.show();
							// Intent i = new Intent(ActivitySignUp.this,
							// ActivityLogin.class);
							// startActivity(i);
						}
	
						@Override
						public void onError() {
							// TODO Auto-generated method stub
	
						}
					});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	protected String checkResult(String strJson) {
		// TODO Auto-generated method stub
		JSONObject json = null;
		String message = "";
		try {
			json = new JSONObject(strJson);
			if (json.getString("status").equals("success")) {
				message = this.getString(R.string.message_success);
				finish();
			} else {
				message = json.getString("message");
			}

		} catch (JSONException e) {
			throw new RuntimeException(e);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}

		return message;
	}

	// private String encodeTobase64(String imgPath) {
	// BitmapFactory.Options options = new BitmapFactory.Options();
	// options.inPreferredConfig = Bitmap.Config.ARGB_8888;
	// Bitmap bitmap = BitmapFactory.decodeFile(imgPath, options);
	//
	// String imageEncoded = "";
	// if (bitmap != null) {
	// ByteArrayOutputStream baos = new ByteArrayOutputStream();
	// bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
	// byte[] b = baos.toByteArray();
	// imageEncoded = Base64.encodeToString(b, Base64.NO_WRAP);
	// }
	//
	// Log.e("Image", "Length: " + imageEncoded.length() + ", Encoded: "
	// + imageEncoded);
	// return imageEncoded;
	// }
}
