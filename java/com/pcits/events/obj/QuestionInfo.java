package com.pcits.events.obj;

public class QuestionInfo {

	private String id, userId, question_name, question_subtext,
			option_group_id, typeId, surveyId, answer_required, answer, allow;

	public String getAllow() {
		return allow;
	}

	public void setAllow(String allow) {
		this.allow = allow;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getSurveyId() {
		return surveyId;
	}

	public String getAnswer_required() {
		return answer_required;
	}

	public void setAnswer_required(String answer_required) {
		this.answer_required = answer_required;
	}

	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestion_name() {
		return question_name;
	}

	public void setQuestion_name(String question_name) {
		this.question_name = question_name;
	}

	public String getQuestion_subtext() {
		return question_subtext;
	}

	public void setQuestion_subtext(String question_subtext) {
		this.question_subtext = question_subtext;
	}

	public String getOption_group_id() {
		return option_group_id;
	}

	public void setOption_group_id(String option_group_id) {
		this.option_group_id = option_group_id;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

}
