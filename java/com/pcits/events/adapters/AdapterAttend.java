package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pcits.events.R;
import com.pcits.events.obj.AttendedObj;

public class AdapterAttend extends BaseAdapter{

	
	private ArrayList<AttendedObj> arrAttend;
	private LayoutInflater mInflate;

	public AdapterAttend(Activity activity, ArrayList<AttendedObj> listAttend) {
		this.arrAttend = listAttend;
		this.mInflate = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrAttend.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {
		// TODO Auto-generated method stub
		final HolderView holder;
		if (convertView == null) {
			holder = new HolderView();
			convertView = mInflate.inflate(R.layout.item_list_attend, null);
			holder.lblAttend_id = (TextView) convertView
					.findViewById(R.id.lblIdAttend);
			holder.lblClient = (TextView) convertView.findViewById(R.id.lblClient);
			holder.lblNotes = (TextView) convertView.findViewById(R.id.lblNote);
			convertView.setTag(holder);
		} else {
			holder = (HolderView) convertView.getTag();
		}
		final AttendedObj o = arrAttend.get(position);
		if (o != null) {
			holder.lblAttend_id.setText(o.getAttendedId());
			holder.lblClient.setText(o.getClientname());
			holder.lblNotes.setText("Available");
		}
		return convertView;
	}

	public class HolderView {
		TextView lblAttend_id, lblClient, lblNotes;
	}
}
