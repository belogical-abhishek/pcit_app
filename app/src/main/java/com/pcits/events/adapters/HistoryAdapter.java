package com.pcits.events.adapters;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.obj.DealObj;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.utils.Utils;

public class HistoryAdapter extends BaseAdapter {

	private ArrayList<DealObj> arrDeals;
	private LayoutInflater mInflate;
	private AQuery aq;
	private DecimalFormat dec = new DecimalFormat("#");

	// Get width/height
	private Display display;
	private int mImgHeight = 0;

	public HistoryAdapter(Activity activity, ArrayList<DealObj> listDeals) {
		this.arrDeals = listDeals;
		this.mInflate = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		aq = new AQuery(activity);

		display = activity.getWindowManager().getDefaultDisplay();
		mImgHeight = display.getWidth() * 9 / 16;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrDeals.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@RequiresApi(api = Build.VERSION_CODES.N)
	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {
		// TODO Auto-generated method stub
		final HolderView holder;
		if (convertView == null) {
			holder = new HolderView();
			convertView = mInflate.inflate(R.layout.item_home, null);
			holder.imgDeals = (ImageView) convertView
					.findViewById(R.id.imgThumbnail);
			holder.imgDeals.getLayoutParams().height = mImgHeight;

			holder.lblCompany = (TextView) convertView
					.findViewById(R.id.lblCompany);
			holder.lblTitle = (TextView) convertView
					.findViewById(R.id.lblTitle);
			holder.lblTitle.setSelected(true);
			holder.lblAfterDisValue = (TextView) convertView
					.findViewById(R.id.lblAfterDiscountValue);
			holder.lblStartValue = (TextView) convertView
					.findViewById(R.id.lblStartValue);
			holder.lblDate = (TextView) convertView.findViewById(R.id.lblDate);
			holder.lblTime = (TextView) convertView.findViewById(R.id.lblTime);
			holder.lblAttending = (TextView) convertView
					.findViewById(R.id.lblAttending);
			holder.rlQuantity = (RelativeLayout) convertView
					.findViewById(R.id.rlQuantity);
			holder.lblQuantity = (TextView) convertView
					.findViewById(R.id.lblQuantity);

			convertView.setTag(holder);
		} else {
			holder = (HolderView) convertView.getTag();
		}
		final DealObj o = arrDeals.get(position);
		if (o != null) {
			AQuery a = aq.recycle(convertView);
			a.id(holder.imgDeals).image(UserFunctions.URLAdmin + o.getImage(),
					false, true, 300, 0, new BitmapAjaxCallback() {
						@Override
						protected void callback(String url, ImageView iv,
								Bitmap bm, AjaxStatus status) {
							if (bm != null) {
								// holder.imgThumbnail.setImageBitmap(bm);
								BitmapDrawable bmd = new BitmapDrawable(bm);
								holder.imgDeals.setBackgroundDrawable(bmd);
							}
						}
					});

			holder.lblTitle.setText(o.getTitle());
			holder.lblCompany.setText("by: " + o.getCompany());
			// holder.lblAfterDisValue.setText("" +
			// o.getAfter_discount_value());
			String price = dec.format(o.getAfter_discount_value());
			if (price.equals("0")) {
				holder.lblAfterDisValue.setText("Free");
			} else {
				holder.lblAfterDisValue.setText(Utils.mCurrency + " " + price);
			}
			holder.lblStartValue.setText(Utils.mCurrency + " "
					+ dec.format(o.getStart_value()));

			holder.lblDate.setText(DateTimeUtility.convertTimeStampToDate(
					o.getStartTimeStamp(), "dd MMM yy")
					+ " - "
					+ DateTimeUtility.convertTimeStampToDate(
							o.getEndTimeStamp(), "dd MMM yy"));
			holder.lblAttending.setText("" + o.getAttend());
			holder.lblTime.setText(DateTimeUtility.convertTimeStampToDate(
					o.getStartTimeStamp(), "HH:mm")
					+ " - "
					+ DateTimeUtility.convertTimeStampToDate(
							o.getEndTimeStamp(), "HH:mm"));

			String dateStartString = o.getStart_date()+" "+o.getStart_time();
			String dateendString = o.getEnd_date()+" "+o.getEnd_time();

			DateFormat formatter= new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
			formatter.setTimeZone(android.icu.util.TimeZone.getDefault());

			dateStartString=formatter.format(new Date(dateStartString));
			dateendString=formatter.format(new Date(dateendString));

			String[] sDateTime;
			String[] eDateTime;
			String sDate;
			String eDate;
			String sTime;
			String eTime;
			sDateTime = dateStartString.split(" ");
			eDateTime=dateendString.split(" ");

			sDate=sDateTime[0];
			sTime=sDateTime[1];

			eDate=eDateTime[0];
			eTime=eDateTime[1];

			holder.lblDate.setText( sDate
					+ " - " +
					eDate);

			holder.lblTime.setText(sTime +"-"+ eTime);

			holder.lblStartValue.setPaintFlags(holder.lblStartValue
					.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

			if (GlobalValue.myClient != null) {
				holder.lblQuantity.setText(o.getQuantity() + "");
				String quantity = holder.lblQuantity.getText().toString();
				if (quantity.equals("0")) {
					holder.rlQuantity.setVisibility(View.GONE);
				} else {
					holder.rlQuantity.setVisibility(View.VISIBLE);
					holder.lblQuantity.setText(quantity);
				}

			} else {
				holder.rlQuantity.setVisibility(View.GONE);
			}
		}
		return convertView;
	}

	public class HolderView {
		ImageView imgDeals;
		TextView lblTitle, lblCompany, lblStartValue, lblAfterDisValue,
				lblDate, lblTime, lblAttending, lblQuantity;

		RelativeLayout rlQuantity;
	}

}
