package com.pcits.events.obj;

/**
 * Created by SAKSHI on 9/28/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListEventObj {

    @SerializedName("deal_id")
    @Expose
    private String dealId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("tnc_id")
    @Expose
    private String tncId;
    @SerializedName("featured")
    @Expose
    private Integer featured;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("county")
    @Expose
    private String county;
    @SerializedName("postcode")
    @Expose
    private String postcode;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("latitude")
    @Expose
    private Float latitude;
    @SerializedName("longitude")
    @Expose
    private Float longitude;
    @SerializedName("start_timestamp")
    @Expose
    private String startTimestamp;
    @SerializedName("end_timestamp")
    @Expose
    private String endTimestamp;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("start_value")
    @Expose
    private Integer startValue;
    @SerializedName("discount")
    @Expose
    private Float discount;
    @SerializedName("after_discount_value")
    @Expose
    private Integer afterDiscountValue;
    @SerializedName("quota")
    @Expose
    private Integer quota;
    @SerializedName("remaining")
    @Expose
    private Integer remaining;
    @SerializedName("max_ticket_purchase")
    @Expose
    private Integer maxTicketPurchase;
    @SerializedName("save_value")
    @Expose
    private Float saveValue;
    @SerializedName("deal_url")
    @Expose
    private String dealUrl;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("tnc_notes")
    @Expose
    private String tncNotes;
    @SerializedName("viewattendee")
    @Expose
    private String viewattendee;
    @SerializedName("featured_start_date")
    @Expose
    private String featuredStartDate;
    @SerializedName("featured_end_date")
    @Expose
    private String featuredEndDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("is_private")
    @Expose
    private Integer isPrivate;

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getTncId() {
        return tncId;
    }

    public void setTncId(String tncId) {
        this.tncId = tncId;
    }

    public Integer getFeatured() {
        return featured;
    }

    public void setFeatured(Integer featured) {
        this.featured = featured;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public String getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(String startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public String getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(String endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getStartValue() {
        return startValue;
    }

    public void setStartValue(Integer startValue) {
        this.startValue = startValue;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public Integer getAfterDiscountValue() {
        return afterDiscountValue;
    }

    public void setAfterDiscountValue(Integer afterDiscountValue) {
        this.afterDiscountValue = afterDiscountValue;
    }

    public Integer getQuota() {
        return quota;
    }

    public void setQuota(Integer quota) {
        this.quota = quota;
    }

    public Integer getRemaining() {
        return remaining;
    }

    public void setRemaining(Integer remaining) {
        this.remaining = remaining;
    }

    public Integer getMaxTicketPurchase() {
        return maxTicketPurchase;
    }

    public void setMaxTicketPurchase(Integer maxTicketPurchase) {
        this.maxTicketPurchase = maxTicketPurchase;
    }

    public Float getSaveValue() {
        return saveValue;
    }

    public void setSaveValue(Float saveValue) {
        this.saveValue = saveValue;
    }

    public String getDealUrl() {
        return dealUrl;
    }

    public void setDealUrl(String dealUrl) {
        this.dealUrl = dealUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTncNotes() {
        return tncNotes;
    }

    public void setTncNotes(String tncNotes) {
        this.tncNotes = tncNotes;
    }

    public String getViewattendee() {
        return viewattendee;
    }

    public void setViewattendee(String viewattendee) {
        this.viewattendee = viewattendee;
    }

    public String getFeaturedStartDate() {
        return featuredStartDate;
    }

    public void setFeaturedStartDate(String featuredStartDate) {
        this.featuredStartDate = featuredStartDate;
    }

    public String getFeaturedEndDate() {
        return featuredEndDate;
    }

    public void setFeaturedEndDate(String featuredEndDate) {
        this.featuredEndDate = featuredEndDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Integer isPrivate) {
        this.isPrivate = isPrivate;
    }

}
