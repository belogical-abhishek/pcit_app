package com.pcits.events.widgets.textview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewRobotoLight extends TextView {

	public TextViewRobotoLight(Context context) {
		super(context);

		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"fonts/Roboto-Light.ttf");
		this.setTypeface(face);
	}

	public TextViewRobotoLight(Context context, AttributeSet attrs) {
		super(context, attrs);
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"fonts/Roboto-Light.ttf");
		this.setTypeface(face);
	}

	public TextViewRobotoLight(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"fonts/Roboto-Light.ttf");
		this.setTypeface(face);
	}

	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

	}
}
