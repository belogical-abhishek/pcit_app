package com.pcits.events.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.DetailClientTicketActivity;
import com.pcits.events.R;
import com.pcits.events.ScanTicketActivity;
import com.pcits.events.adapters.ScanTicketPage2Adapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.widgets.KenBurnsView;

public class ScanTicketsPage2 extends Activity {

	private ListView lsvMyTicket;
	private ScanTicketPage2Adapter adapterTicket;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;
	private TextView lblTitleHeader;
	private Button btnScan;
	private int type;
	private String dealID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try{
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_scantickets_page2);
			Intent i = getIntent();
			dealID = i.getStringExtra("id");
			initUI();
			initControl();
			getData();
			initNotBoringActionBar();
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	private void initNotBoringActionBar() {
		mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}

	private void initUI() {
		lsvMyTicket = (ListView) findViewById(R.id.lsvMyTickets);
		lblTitleHeader = (TextView) findViewById(R.id.lblTitleHeader);
		lblTitleHeader.setText("Tickets");
		btnScan = (Button) findViewById(R.id.btnScan);
		llMenu = (LinearLayout) findViewById(R.id.llMenu);
		lsvMyTicket.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				GlobalValue.ticket = GlobalValue.arrTickets.get(position);
				Intent i = new Intent(ScanTicketsPage2.this,
						DetailClientTicketActivity.class);
				startActivity(i);
			}
		});
	}

	private void initControl() {
		btnScan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(ScanTicketsPage2.this,
						ScanTicketActivity.class);
				startActivity(i);
			}
		});

		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getData();
	}

	private void getData() {
		try{
			if (GlobalValue.myUser != null) {
				type = 2;
	//			String id = "D000000026";
				ModelManager.getListTickets(ScanTicketsPage2.this,
						Integer.toString(type), dealID, true,
						new ModelManagerListener() {
	
							@Override
							public void onSuccess(Object object) {
								// TODO Auto-generated method stub
								String json = (String) object;
								GlobalValue.arrTickets = ParserUtility
										.getListTickets(json);
								if (GlobalValue.arrTickets.size() > 0) {
									adapterTicket = new ScanTicketPage2Adapter(
											ScanTicketsPage2.this,
											GlobalValue.arrTickets);
									adapterTicket.notifyDataSetChanged();
									lsvMyTicket.setAdapter(adapterTicket);
								} else {
									Toast.makeText(ScanTicketsPage2.this,
											"No Data", Toast.LENGTH_SHORT).show();
								}
							}
	
							@Override
							public void onError() {
								// TODO Auto-generated method stub
	
							}
						});
			}
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}
}
