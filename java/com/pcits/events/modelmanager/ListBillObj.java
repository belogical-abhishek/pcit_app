package com.pcits.events.modelmanager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by EnvisioDevs on 23-11-2017.
 */

public class ListBillObj {

    /*"id": 3,
        "deal_id": "D000000120",
        "description": "Lorem Ipsum",
        "image": "image_path",
        "amount": 300,
        "paid_by": 3,
        "created_by": 2,
        "created_at": "2017-11-10 08:10:22",
        "updated_at": "2017-11-10 08:10:22",
        "split"*/

    @SerializedName("id")
    int id;

    @SerializedName("deal_id")
    String deal_id;

    @SerializedName("amount")
    float amount;

    @SerializedName("split")
    ArrayList<Split> split;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeal_id() {
        return deal_id;
    }

    public void setDeal_id(String deal_id) {
        this.deal_id = deal_id;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public ArrayList<Split> getSplit() {
        return split;
    }

    public void setSplit(ArrayList<Split> split) {
        this.split = split;
    }
}
