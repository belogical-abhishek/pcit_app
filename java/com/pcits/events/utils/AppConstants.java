package com.pcits.events.utils;

/**
 * Created by Shrikant on 10-09-2017.
 */

public class AppConstants {

    public  static final int RC_LOCATION_PERM = 124;
    public static final int RC_CAMERA_PERM = 123;
    public static final int RC_SOTRAGE_PERM = 125;
    public static final int RC_CALENDER_PERM = 126;
    public static final int RC_CONTACT_PERM = 127;
    public static final int RC_ON_SPLITBILL_ADDED = 128;
    public   static final String BASEURL="http://54.173.65.38/events-api/public/";

}
