package com.pcits.events;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.pcits.common.utils.Logging;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.config.MySharedPreferences;
import com.pcits.events.config.ads.Ads;
import com.pcits.events.database.DatabaseUtility;
import com.pcits.events.fragments.FragmentLogin;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.obj.ClientInfo;
import com.pcits.events.obj.UserObj;
import com.pcits.events.widgets.FloatLabeledEditText;
import com.pcits.events.widgets.KenBurnsView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static com.androidquery.util.AQUtility.getContext;
import static com.pcits.events.config.MySharedPreferences.ADMIN_USER_KEY;
import static com.pcits.events.config.MySharedPreferences.CLIENT_USER_KEY;

public class ActivityLogin extends FragmentActivity {

	// Declare object of AdView class
	private AdView adView;

	private Fragment mFrag;

	private String extraString = "";
	private String extraFromString = "";
	private String extraDealIdString = "";
	private String extraEmail = "";
	private String extraName = "";


	private Button btnLogin, btnSignUp, btnForgetPass;
	private FloatLabeledEditText txtUserName, txtPassword;
	private View view;
	private String username = "";
	private String password = "";
	public static Activity self;
	private static final String TAG = "FragmentMain";
	private LoginButton mBtnFacebookLogin;

	private SimpleDateFormat sdf_date, sdf_date1;
	private Date da;
	private KenBurnsView mHeaderPicture;

	public static boolean hasPush = false;

	private ImageView mImgMenu;
	private CallbackManager callbackManager;
	private AccessTokenTracker accessTokenTracker;
	private AccessToken accessToken;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_login);

		try {
			// TODO Auto-generated method stub
			self = ActivityLogin.this;


			sdf_date = new SimpleDateFormat("yyyy-MM-dd");
			sdf_date1 = new SimpleDateFormat("MM/dd/yyyy");
			FacebookSdk.sdkInitialize(ActivityLogin.this.getApplicationContext());
			callbackManager = CallbackManager.Factory.create();
			accessTokenTracker = new AccessTokenTracker() {
				@Override
				protected void onCurrentAccessTokenChanged(
						AccessToken oldAccessToken,
						AccessToken currentAccessToken) {
					// Set the access token using
					// currentAccessToken when it's loaded or set.
				}
			};
			// If the access token is available already assign it.
			accessToken = AccessToken.getCurrentAccessToken();
			initUI();
			initControl();
			// initNotBoringActionBar();

		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
			ex.printStackTrace();
		}

		getExtras(savedInstanceState);




	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:

			// Previous page or exit
			if (GlobalValue.check == 1) {
				finish();
			} else {
				Intent i = new Intent(this, ActivityHome.class);
				startActivity(i);
				finish();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	private void getExtras(Bundle savedInstanceState) {
		try {
			if (savedInstanceState == null) {
				Bundle extras = getIntent().getExtras();
				if (extras != null) {
					extraString = extras.getString("obj");
					extraFromString = extras.getString("from");
					extraDealIdString = extras.getString("dealid");
					extraEmail = extras.getString("email");
					extraName = extras.getString("name");
				}
			} else {
				extraString = (String) savedInstanceState.getSerializable("obj");
				extraFromString = (String) savedInstanceState.getSerializable("from");
				extraDealIdString = (String) savedInstanceState.getSerializable("dealid");
				extraEmail = (String) savedInstanceState.getSerializable("email");
				extraName = (String) savedInstanceState.getSerializable("name");
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}


	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

		closeKeyboard();

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		accessTokenTracker.stopTracking();

	}

	private void initUI() {
		try {
			mBtnFacebookLogin = (LoginButton)findViewById(R.id.btn_login_facebooks);
//            mBtnFacebookLogin.setReadPermissions("email");
			mBtnFacebookLogin.setReadPermissions(Arrays.asList(
					"public_profile", "email", "user_birthday", "user_friends"));
			// If using in a fragment
			//mBtnFacebookLogin.setFragment(this);

			mBtnFacebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
				@Override
				public void onSuccess(LoginResult loginResult)
				{
					// App code
					Log.d("fb", loginResult.toString());
					GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
							new GraphRequest.GraphJSONObjectCallback()
							{
								@Override
								public void onCompleted(JSONObject object, GraphResponse response)
								{
									Log.v("LoginActivity", response.toString());

									try
									{
										// Application code
										final ClientInfo systemUser = new ClientInfo();

										Profile profile = Profile.getCurrentProfile();
										String email = "";
										try
										{
											email = object.getString("email").toString();
										}
										catch (NullPointerException ex)
										{
											Toast.makeText(
													self,
													"Your facebook email is private or not activated yet.",
													Toast.LENGTH_SHORT).show();
										}
										if (!email.isEmpty())
										{
											// Get string before @ symbol in his email.
											systemUser.setUsername(email.substring(0, email.indexOf("@")));
											systemUser.setEmail(email);
										}
										systemUser.setFname(profile.getFirstName());
										systemUser.setLname(profile.getLastName());
										systemUser.setFbid(profile.getId());
										systemUser.setFullname(profile.getName());
                                        /*systemUser.setGender(object.getString("gender"));

                                        try {
                                            String date = object.getString("birthday");
                                            String dob = "";
                                            if (date != null) {
                                                da = sdf_date1.parse(date);
                                                dob = sdf_date.format(da);
                                            } else {
                                                dob = "";
                                            }
                                            systemUser.setDob(dob);
                                        } catch (Exception ex) {

                                        }*/

										systemUser.setLink(profile.getLinkUri().toString());
//                                        systemUser.setUpdatedtime((String) user
//                                                .getProperty("updated_time"));
										systemUser.setDeviceid(GlobalValue.android_id);
										systemUser.setType("1");
										// systemUser.setType(AccountInfo.TYPE_FACEBOOK);

										systemUser.setImage(profile.getProfilePictureUri(200, 200).toString());
										if (systemUser != null) {
											loginSocial(systemUser);
										}
									} catch (JSONException ex)
									{
										ex.printStackTrace();
									}
								}
							});
					Bundle parameters = new Bundle();
					parameters.putString("fields", "id,name,email,gender,birthday");
					request.setParameters(parameters);
					request.executeAsync();
				}

				@Override
				public void onCancel() {
					// App code
				}

				@Override
				public void onError(FacebookException exception) {
					// App code
					exception.printStackTrace();
				}
			});

			btnLogin = (Button) findViewById(R.id.btnLogin);
			btnSignUp = (Button) findViewById(R.id.btnRegister);
			btnForgetPass = (Button) findViewById(R.id.btnForgetPass);
			txtUserName = (FloatLabeledEditText)findViewById(R.id.txtUserName);

			mImgMenu = (ImageView) findViewById(R.id.ic_menu);

			try {
				Field fUserName = TextView.class
						.getDeclaredField("mCursorDrawableRes");
				fUserName.setAccessible(true);
				fUserName.set(txtUserName.getEditText(),
						R.drawable.cursor_custom_white);
			} catch (Exception ex) {
				throw new RuntimeException(ex);
			}

			txtUserName.setText("admin");
			txtPassword = (FloatLabeledEditText) findViewById(R.id.txtPassword);

			try {
				Field fPass = TextView.class
						.getDeclaredField("mCursorDrawableRes");
				fPass.setAccessible(true);
				fPass.set(txtPassword.getEditText(),
						R.drawable.cursor_custom_white);
			} catch (Exception ex) {
				throw new RuntimeException(ex);
			}

			txtPassword.setText("Nam123!");

			mBtnFacebookLogin.setReadPermissions(Arrays.asList(
					"public_profile", "email"));
			//mBtnFacebookLogin.setFragment(this);

		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private String validateForm() {
		String message = "success";
		String username = txtUserName.getText().toString();
		String password = txtPassword.getText().toString();
		if (username.isEmpty()) {
			Toast.makeText(ActivityLogin.this, "Please, input username",
					Toast.LENGTH_SHORT).show();
			return message;
		}
		if (password.isEmpty()) {
			Toast.makeText(ActivityLogin.this, "Please, input password",
					Toast.LENGTH_SHORT).show();
			return message;
		}
		return message;
	}


	private void initControl() {
		try {
			btnSignUp.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(ActivityLogin.this, ActivitySignUp.class);
					startActivity(i);
					// ActivityLogin.this.finish();
				}
			});
			// forget password
			btnForgetPass.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(ActivityLogin.this,
							ForgetPasswordActivity.class);
					startActivity(i);
				}
			});
			// login
			btnLogin.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					login();
				}
			});

			// Hide admob when input username and password
			txtUserName.setOnFocusChangedListener(new View.OnFocusChangeListener() {

				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (hasFocus) {
						//     ActivityHome.adView.setVisibility(View.GONE);
					} else {
						if (!txtPassword.hasFocus()) {
							//      ActivityHome.adView.setVisibility(View.VISIBLE);
						}
					}
				}
			});

			txtUserName.setOnEditorActionListener(new TextView.OnEditorActionListener() {

				@Override
				public boolean onEditorAction(TextView v, int actionId,
											  KeyEvent event) {
					// TODO Auto-generated method stub
					if (actionId == EditorInfo.IME_ACTION_DONE) {
						if (txtPassword.getTextString().trim().length() > 0) {
							login();
						} else {
							txtPassword.requestFocus();
						}
						return true;
					}

					return false;
				}
			});

			txtPassword.setOnFocusChangedListener(new View.OnFocusChangeListener() {

				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (hasFocus) {
						//    ActivityHome.adView.setVisibility(View.GONE);
					} else {
						if (!txtUserName.hasFocus()) {
							//       ActivityHome.adView.setVisibility(View.VISIBLE);
						}
					}
				}
			});

			txtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {

				@Override
				public boolean onEditorAction(TextView v, int actionId,
											  KeyEvent event) {
					// TODO Auto-generated method stub
					if (actionId == EditorInfo.IME_ACTION_DONE) {
						if (txtUserName.getTextString().trim().length() > 0) {
							login();
						} else {
							txtUserName.requestFocus();
						}
						return true;
					}

					return false;
				}
			});

			mImgMenu.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// ActivityHome.resideMenu.openMenu();
					//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
					Intent intent=new Intent(ActivityLogin.this,ActivityHome.class);
					ActivityLogin.this.startActivity(intent);
					ActivityLogin.this.finish();
				}
			});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void login() {
		try {
			String message = validateForm();
			if (!message.equals("success")/* && type == 3 */) {
				Toast.makeText(ActivityLogin.this, message, Toast.LENGTH_SHORT)
						.show();
			} else {
				username = txtUserName.getText().toString();
				password = txtPassword.getText().toString();
				MessageDigest md = null;
				if (NetworkUtility.getInstance(ActivityLogin.this)
						.isNetworkAvailable()) {
					ModelManager.login(ActivityLogin.this, username, password, true, new ModelManagerListener() {

						@Override
						public void onSuccess(Object object) {
							// TODO Auto-generated method stub
							JSONObject json = null;
							String strjson = (String) object;

							try {
								json = new JSONObject(strjson);
								if (json.getString("status").equals(
										"success")) {
									if (json.getString("role").equals(
											"client")) {
										GlobalValue.myClient = ParserUtility
												.parserAccount(strjson);
										if (GlobalValue.myClient != null) {
											DatabaseUtility
													.insertClient(
															ActivityLogin.this,
															GlobalValue.myClient);
											Log.d(TAG, "Success");

											Gson gson = new Gson();
											String clientObj = gson.toJson(GlobalValue.myClient,ClientInfo.class);
											Log.d(TAG, "onSuccess: "+clientObj);
											JSONArray clientData1=json.getJSONArray("data");
											JSONObject clientData=clientData1.getJSONObject(0);
											MySharedPreferences.getInstance(getContext()).putStringValue(CLIENT_USER_KEY,clientObj);
											MySharedPreferences.getInstance(getContext()).putStringValue("User_Role_Type",clientData.getString("id"));
											MySharedPreferences.getInstance(getContext()).putStringValue("User_Name",clientData.getString("clientname"));

											if (GlobalValue.check == 1) {
												ActivityLogin.this.finish();
											} else {
												// Check whether is
												// logged
												// in by normal or login
												// from pushing from
												// notification.
												if (hasPush) {
													hasPush = false;
													ActivityHome.hasPush = true;
												}

												Intent i = new Intent(
														ActivityLogin.this,
														ActivityHome.class);
												startActivity(i);
												ActivityLogin.this.finish();
											}
										} else {
											Toast.makeText(
													ActivityLogin.this,
													"Login false! Try again please",
													Toast.LENGTH_LONG)
													.show();

										}
									} else if (json.getString("role")
											.equals("admin")) {
										GlobalValue.myUser = ParserUtility
												.parserUser(strjson);
										if (GlobalValue.myUser != null) {

											DatabaseUtility
													.insertAdmin(
															ActivityLogin.this,
															GlobalValue.myUser);
											Log.d(TAG, "Success");

											Gson gson = new Gson();
											String adminObj = gson.toJson(GlobalValue.myUser,UserObj.class);
											MySharedPreferences.getInstance(getContext()).putStringValue(ADMIN_USER_KEY,adminObj);
											JSONArray clientData1=json.getJSONArray("data");
											JSONObject clientData=clientData1.getJSONObject(0);
											MySharedPreferences.getInstance(getContext()).putStringValue("User_Role_Type",clientData.getString("role"));
											MySharedPreferences.getInstance(getContext()).putStringValue("User_Name",clientData.getString("username"));

											if (GlobalValue.check == 1) {
												ActivityLogin.this.finish();
											} else {
												Intent i = new Intent(
														ActivityLogin.this,
														ActivityHome.class);
												startActivity(i);
												ActivityLogin.this.finish();
											}
										} else {
											Toast.makeText(
													ActivityLogin.this,
													"Login fail. Please try again!",
													Toast.LENGTH_LONG)
													.show();
										}
									}
								} else {
									Toast.makeText(
											ActivityLogin.this,
											"Login false! Try again please",
											Toast.LENGTH_LONG).show();
								}

							} catch (JSONException e) {
								throw new RuntimeException(e);
							} catch (Exception ex) {
								throw new RuntimeException(ex);
							}

						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}
					});
				} else {
					Toast.makeText(ActivityLogin.this, "Not Internet",
							Toast.LENGTH_LONG).show();
				}
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		callbackManager.onActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

	}

	private void loginSocial(ClientInfo acc) {
		try {
			ClientInfo data = acc;
			ModelManager.loginMedia(self, data, true,
					new ModelManagerListener() {

						@Override
						public void onSuccess(Object object) {
							// TODO Auto-generated method stub
							String strJson = (String) object;
							GlobalValue.myClient = ParserUtility
									.parserAccount(strJson);
							if (GlobalValue.myClient != null) {

								Gson gson = new Gson();
								String clientObj = gson.toJson(GlobalValue.myClient,ClientInfo.class);
								MySharedPreferences.getInstance(getContext()).putStringValue(CLIENT_USER_KEY,clientObj);
								if (GlobalValue.check == 1) {
									ActivityLogin.this.finish();
								} else {
									Intent i = new Intent(ActivityLogin.this,
											ActivityHome.class);
									startActivity(i);
									ActivityLogin.this.finish();
								}
							} else {
								Toast.makeText(self, "Login facebook fail!",
										Toast.LENGTH_SHORT).show();
							}
						}

						@Override
						public void onError() {
							Log.v("onErroor"," here in error");
						}
					});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void closeKeyboard() {
		InputMethodManager imm = (InputMethodManager) ActivityLogin.this
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		if (txtUserName.hasFocus()) {
			imm.hideSoftInputFromWindow(txtUserName.getWindowToken(), 0);
		} else {
			imm.hideSoftInputFromWindow(txtPassword.getWindowToken(), 0);
		}
	}
}
