package com.pcits.events.adapters;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.pcits.events.ActivityHome;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.config.MaintainEventStateClass;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.obj.DealObj;
import com.pcits.events.obj.ListEventObj;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HomeAdapter extends BaseAdapter {

    private static final String TAG = "HomeAdapter";

    private Activity activity;
    private ArrayList<DealObj> arrDeal;
    private List<String> mListStatus;
    private ArrayList<Boolean> arrDealVisitStatus;
    private LayoutInflater inflater = null;
    private static final String DATE_FORMAT = "dd-MMM-yyyy hh:mm:ss";
    private AQuery listAq;

    // Get width/height
    private Display display;
    private int mImgHeight = 0;

    public HomeAdapter(Activity a, ArrayList<DealObj> deals,List<String> mListStatus) {
        activity = a;
        arrDeal = deals;
        arrDealVisitStatus = new ArrayList<>();
        this.mListStatus=mListStatus;

        for(int i=0;i<arrDeal.size();i++) {
            arrDealVisitStatus.add(false);
        }

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listAq = new AQuery(a);

        display = a.getWindowManager().getDefaultDisplay();
        mImgHeight = display.getWidth() * 9 / 16;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return arrDeal.size();
    }

    public void setVisitStatus(String dealId, boolean status)
    {
        try
        {
            if(arrDeal.size()>arrDealVisitStatus.size())
            {
                for(int i=arrDealVisitStatus.size();i<arrDeal.size();i++)
                {
                    arrDealVisitStatus.add(false);
                }
            }
            for(int i=0;i<arrDeal.size();i++)
            {
                DealObj dealObj = arrDeal.get(i);
                if(dealObj.getDeal_id().equals(dealId)) {
                    arrDealVisitStatus.set(i, status);
                    break;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getViewTypeCount()
    {
        try
        {
            return getCount()+1;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
       return 1;
    }

/*    @Override
    public int getItemViewType(int position)
    {
        try
        {
            return position;
        }
        catch (Exception e)
        {
            e.printStackTrace();
           // return position-1;
        }
        return 0;
    }*/

    @Override
    public int getItemViewType(int pos) {
        if (pos < arrDeal.size())
            return 0;
        return 1;
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint({"SimpleDateFormat", "ResourceAsColor"})
    public View getView(final int position, View convertView, final ViewGroup parent) {
        // TODO Auto-generated method stub
        final ViewHolder holder;

        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.item_home, null);

            View viewHorizontalDivider = convertView.findViewById(R.id.horizontal_divider);
            View viewVerticalDivider1 = convertView.findViewById(R.id.vertical_divider_1);
            View viewVerticalDivider2 = convertView.findViewById(R.id.vertical_divider_2);

            try
            {
                if(arrDealVisitStatus.get(position))
                {
                    viewHorizontalDivider.setBackgroundResource(R.color.orange);
                    viewVerticalDivider1.setBackgroundResource(R.color.orange);
                    viewVerticalDivider2.setBackgroundResource(R.color.orange);
                }

            }
            catch (IndexOutOfBoundsException e)
            {
                e.printStackTrace();
            }

            holder = new ViewHolder();

            // Connect views object and views id on xml
            holder.lblTitle = (TextView) convertView
                    .findViewById(R.id.lblTitle);
            holder.lblCompany = (TextView) convertView
                    .findViewById(R.id.lblCompany);
            holder.lblCompany.setSelected(true);
            holder.lblStartValue = (TextView) convertView
                    .findViewById(R.id.lblStartValue);
            holder.lblAfterDiscValue = (TextView) convertView
                    .findViewById(R.id.lblAfterDiscountValue);
            holder.lblDate = (TextView) convertView.findViewById(R.id.lblDate);
            holder.lblTime = (TextView) convertView.findViewById(R.id.lblTime);
            holder.lblAttend = (TextView) convertView
                    .findViewById(R.id.lblAttending);

            holder.imgThumbnail = (ImageView) convertView
                    .findViewById(R.id.imgThumbnail);

            holder.lblStatus=(TextView)convertView.findViewById(R.id.lblStatus);

            holder.rlQuantity = (RelativeLayout) convertView
                    .findViewById(R.id.rlQuantity);
            holder.lytImage=(RelativeLayout)convertView.findViewById(R.id.lytImage);
            holder.lblQuantity = (TextView) convertView
                    .findViewById(R.id.lblQuantity);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
            //holder = convertView;
        }
        DealObj deal=null;
        try
        {
            deal = arrDeal.get(position);
        }
        catch (IndexOutOfBoundsException e)
        {
            e.printStackTrace();
        }


        if (deal != null)
        {
            // Set data to textview and imageview
            holder.imgThumbnail.getLayoutParams().width = display.getWidth();
            holder.imgThumbnail.getLayoutParams().height = mImgHeight;
            AQuery aq = listAq.recycle(convertView);
            aq.id(holder.imgThumbnail).image(
                    UserFunctions.URLAdmin + deal.getImage(), false, true, 300,
                    0, new BitmapAjaxCallback() {
                        @Override
                        protected void callback(String url, ImageView iv,
                                                Bitmap bm, AjaxStatus status) {
                            if (bm != null) {
                                // holder.imgThumbnail.setImageBitmap(bm);

                                BitmapDrawable bmd = new BitmapDrawable(bm);
                                holder.imgThumbnail.setBackgroundDrawable(bmd);
                            }
                        }
                    });

            holder.lblTitle.setText(deal.getTitle());
            holder.lblTitle.setSelected(true);
            holder.lblCompany.setText("By: " + deal.getCompany());

            holder.lblStartValue.setText(Utils.mCurrency + " "
                    + deal.getStart_value());
            holder.lblAfterDiscValue.setText(deal.getAfter_discount_value()
                    + "");
            String price = holder.lblAfterDiscValue.getText().toString();
            if (price.equals("0")) {
                holder.lblAfterDiscValue.setText("Free");
            } else {
                holder.lblAfterDiscValue.setText(Utils.mCurrency + " " + price);
            }

            String dateStartString = deal.getStart_date()+" "+deal.getStart_time();
            String dateendString = deal.getEnd_date()+" "+deal.getEnd_time();

            DateFormat formatter= new SimpleDateFormat(DATE_FORMAT);
            formatter.setTimeZone(android.icu.util.TimeZone.getDefault());

            dateStartString=formatter.format(new Date(dateStartString));
            dateendString=formatter.format(new Date(dateendString));

            String[] sDateTime;
            String[] eDateTime;
            String sDate;
            String eDate;
            String sTime;
            String eTime;
            sDateTime = dateStartString.split(" ");
            eDateTime=dateendString.split(" ");

            sDate=sDateTime[0];
            sTime=sDateTime[1];

            eDate=eDateTime[0];
            eTime=eDateTime[1];

            holder.lblDate.setText( sDate
                    + " - " +
                    eDate);

            holder.lblTime.setText(sTime +"-"+ eTime);

            holder.lblAttend.setText(deal.getAttend() + "");
            String start[]  =deal.getStart_time().split(":");
            String end[]  =deal.getEnd_time().split(":");
            //holder.lblTime.setText(start[0]+":"+start[1]+" - "+end[0]+":"+end[1]);

		/*	holder.lblTime.setText(DateTimeUtility.convertTimeStampToDate(
					deal.getStartTimeStamp(), "HH:mm")
					+ " - "
					+ DateTimeUtility.convertTimeStampToDate(
							deal.getEndTimeStamp(), "HH:mm"));*/
            holder.lblStartValue.setPaintFlags(holder.lblStartValue
                    .getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


            if (deal.getIsHot()==0) {
                holder.rlQuantity.setVisibility(View.GONE);
            } else {
                holder.rlQuantity.setVisibility(View.VISIBLE);
                holder.lblQuantity.setText(""+deal.getViewCount());
            }

				/*if(ActivityHome.event_id_open_list.contains(deal.getDeal_id()))
				{
					//holder.imgThumbnail.setBackgroundResource(R.drawable.black_background);
					holder.lblCompany.setBackgroundColor(R.color.red);
                   // Toast.makeText(activity,deal.getDeal_id(),Toast.LENGTH_SHORT).show();
				}*/
				try
                {
                    if(!mListStatus.get(position).equalsIgnoreCase("0"))
                    {
                        //holder.lblCompany.setBackgroundColor(R.color.red);
                        holder.lblTitle.setBackgroundColor(Color.parseColor("#FF0000"));
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
				/*else
				{
					holder.lblCompany.setBackgroundColor(Color.parseColor("#FFFFFF"));
				}*/

        }

        return convertView;
    }

    // Method to create instance of views
    private class ViewHolder
    {
        public ImageView imgThumbnail;
        public TextView lblTitle, lblCompany, lblDate, lblAfterDiscValue,
                lblStartValue, lblTime, lblAttend, lblQuantity,lblStatus;
        int position;
        public RelativeLayout rlQuantity,lytImage;
    }

    private void setStatusPrivateEvent(android.content.Context context, String key,String deal_id)
    {
        //Event_List_Status-key
        try
        {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            JSONArray a;
            try
            {
                String json = prefs.getString(key, null);
                a = new JSONArray(json);
            }
            catch (Exception e)
            {
                a = new JSONArray();
                e.printStackTrace();
            }

            ListEventObj listEventObj = null;
            JSONObject b=new JSONObject();

            try
            {
                b.put("event_id", deal_id);

            }
            catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            a.put(b);

            if (a!=null)
            {
                editor.putString(key, a.toString());
            }
            else
            {
                editor.putString(key, null);
            }
            editor.commit();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}