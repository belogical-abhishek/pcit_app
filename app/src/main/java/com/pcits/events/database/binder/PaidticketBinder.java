package com.pcits.events.database.binder;

import android.database.sqlite.SQLiteStatement;

import com.pcits.events.obj.PaidticketsObj;

public class PaidticketBinder implements ParameterBinder {

	@Override
	public void bind(SQLiteStatement st, Object object) {
		// TODO Auto-generated method stub
		PaidticketsObj paidticket = (PaidticketsObj) object;
		st.bindString(1, paidticket.getId());
		st.bindString(2, paidticket.getTran_id());
		st.bindString(3, paidticket.getUserId());
		st.bindString(4, paidticket.getClientId());
		st.bindString(5, paidticket.getEventId());
		st.bindString(6, paidticket.getAmount());
		st.bindString(7, paidticket.getUsed_date());
		st.bindString(8, paidticket.getPayMethod());
		st.bindString(9, paidticket.getUniqueCode());
		st.bindLong(10, paidticket.getStatus());
	}

}
