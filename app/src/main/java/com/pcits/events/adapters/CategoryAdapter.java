package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.pcits.events.R;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.obj.CategoryObj;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class CategoryAdapter extends BaseAdapter {
	private Activity activity;
	// private ArrayList<HashMap<String, String>> data;
	private LayoutInflater inflater = null;
	private ArrayList<CategoryObj> mArrCate;
	private AQuery mAq;

	public CategoryAdapter(Activity a, ArrayList<CategoryObj> arr) {
		activity = a;
		mArrCate = arr;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mAq = new AQuery(a);
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return mArrCate.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_nice_category, null);
			holder = new ViewHolder();

			holder.lblTitle = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lblName);
			holder.horBar = convertView.findViewById(R.id.vw_hor_bar);
			holder.imgCate = (ImageView) convertView
					.findViewById(R.id.img_cate);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		CategoryObj cate = mArrCate.get(position);
		if (cate != null) {
			holder.lblTitle.setText(cate.getCategoryName());
			if (!cate.getCategoryImage().equals("")) {
				AQuery aq = mAq.recycle(convertView);
				aq.id(holder.imgCate).image(
						UserFunctions.URLAdmin + cate.getCategoryImage());
			} else {
				holder.imgCate.setImageResource(R.drawable.eventslogo);
			}

			// Hide last bar
			if (position == mArrCate.size() - 1) {
				holder.horBar.setVisibility(View.GONE);
			} else {
				holder.horBar.setVisibility(View.VISIBLE);
			}
		}

		return convertView;
	}

	// Method to create instance of views
	static class ViewHolder {
		TextViewRobotoCondensedRegular lblTitle;
		ImageView imgCate;
		View horBar;
	}
}
