package com.pcits.events.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class DateTimeUtility {

	public static String convertTimeStampToDate(String timeStamp, String outputFormat) {
		SimpleDateFormat formater = new SimpleDateFormat(outputFormat,
				Locale.getDefault());
		try {
			Date date = new Date(Long.parseLong(timeStamp) * 1000);
			return formater.format(date);
		} catch (NumberFormatException ex) {
			return formater.format(new Date());
		}
	}

	public static String getDateFormat(String dateString,String inputFormat,String outputFormat) throws ParseException {


		DateFormat inputDateFormat = new SimpleDateFormat(inputFormat);
		DateFormat outputDateFormat = new SimpleDateFormat(outputFormat);

		Date date = inputDateFormat.parse(dateString);
		return outputDateFormat.format(date);


	}

	public static long getDateDiff(Date curDate, Date specDate,
			TimeUnit timeUnit) {
		long diff = curDate.getTime() - specDate.getTime();
		return timeUnit.convert(diff, TimeUnit.MILLISECONDS);
	}

	public static String formatDate(Date date, String outputFormat) {
		SimpleDateFormat formater = new SimpleDateFormat(outputFormat,
				Locale.getDefault());
		return formater.format(date);
	}

	public static String convertStringToDate(String strDate, String inputFormat,
			String outputFormat) {
		SimpleDateFormat dateFormaterInput, dateFormaterOutput;
		dateFormaterInput = new SimpleDateFormat(inputFormat, Locale.US);
		dateFormaterOutput = new SimpleDateFormat(outputFormat, Locale.US);
		Date dob;
		try {
			dob = dateFormaterInput.parse(strDate);
			return dateFormaterOutput.format(dob);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return strDate;
		}
	}
}
