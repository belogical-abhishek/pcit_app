/**
 * File        : ActivitySetting.java
 * App name    : M1SO Events
 * Version     : 1.1.4
 * Created     : 25/05/14

 * Created by Team M1SO on 21/01/14.
 * Copyright (c) 2014 M1SO. All rights reserved.
 */

package com.pcits.events;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.adapters.AdminAdapter;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.AdminObj;
import com.pcits.events.widgets.KenBurnsView;

public class AdminActivity extends Activity {

	private static final String TAG = "AdminActivity";

	private AdminActivity self;
	// Declare header
	private LinearLayout mLlHeaderHome;
	private KenBurnsView mHeaderPicture;

	private GridView mGrvAdmin;
	private AdminAdapter mAdminAdapter;
	private ArrayList<AdminObj> mArrAdmin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try{
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_admin);
			self = this;
	
			initUI();
			initData();
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	private void initUI() {
		mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);
		mLlHeaderHome = (LinearLayout) findViewById(R.id.ll_home_header);

		mGrvAdmin = (GridView) findViewById(R.id.grv_admin);

		// Should call at end of initUI method.
		initcontrol();
	}

	private void initcontrol() {
		// Finish when click on home icon.
		mLlHeaderHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		// Click on a item in admin gridvew
		mGrvAdmin.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (!mArrAdmin.get(position).isActive()) {
					activeAdmin(mArrAdmin.get(position));
				} else {
					showDialogManualAdmin(mArrAdmin.get(position));
				}
			}
		});
	}

	private void initData() {
		try{
			ModelManager.getListOfAdmin(self, true, new ModelManagerListener() {
	
				@Override
				public void onSuccess(Object object) {
					String json = (String) object;
	
					mArrAdmin = ParserUtility.parserAdminInfo(json);
	
					Log.e(TAG, "Admin size: " + mArrAdmin.size());
	
					mAdminAdapter = new AdminAdapter(self, mArrAdmin);
					mGrvAdmin.setAdapter(mAdminAdapter);
				}
	
				@Override
				public void onError() {
					// TODO Auto-generated method stub
	
				}
			});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void activeAdmin(final AdminObj adm) {
		AlertDialog.Builder builder = new Builder(self);
		builder.setTitle(getResources().getString(R.string.active_admin));
		builder.setMessage(getResources().getString(
				R.string.do_you_want_to_active_admin)
				+ " " + adm.getUserName() + "?");

		builder.setNegativeButton(getResources().getString(R.string.no),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});

		builder.setPositiveButton(getResources().getString(R.string.yes),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						ModelManager.activeAdmin(self, adm.getUserName(), "1",
								true, new ModelManagerListener() {

									@Override
									public void onSuccess(Object object) {
										String json = (String) object;

										try {
											JSONObject obj = new JSONObject(
													json);
											if (obj.getString("status")
													.equalsIgnoreCase("success")) {
												adm.setActive(true);
												mAdminAdapter
														.notifyDataSetChanged();

												Toast.makeText(
														self,
														adm.getUserName()
																+ " is activated succsessfully.",
														Toast.LENGTH_SHORT)
														.show();
											}
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}

									@Override
									public void onError() {
										// TODO Auto-generated method stub

									}
								});
					}
				});

		builder.create().show();
	}

	private void suspendAdmin(final AdminObj adm) {
		AlertDialog.Builder builder = new Builder(self);
		builder.setTitle(getResources().getString(R.string.suspend_admin));
		builder.setMessage(getResources().getString(
				R.string.are_you_sure_you_want_to_suspend)
				+ " " + adm.getUserName() + "?");

		builder.setNegativeButton(getResources().getString(R.string.no),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});

		builder.setPositiveButton(getResources().getString(R.string.yes),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						ModelManager.activeAdmin(self, adm.getUserName(), "0",
								true, new ModelManagerListener() {

									@Override
									public void onSuccess(Object object) {
										String json = (String) object;

										try {
											JSONObject obj = new JSONObject(
													json);
											if (obj.getString("status")
													.equalsIgnoreCase("success")) {
												adm.setActive(false);
												mAdminAdapter
														.notifyDataSetChanged();

												Toast.makeText(
														self,
														adm.getUserName()
																+ " is suspended succsessfully.",
														Toast.LENGTH_SHORT)
														.show();
											}
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}

									@Override
									public void onError() {
										// TODO Auto-generated method stub

									}
								});
					}
				});

		builder.create().show();
	}

	private void showDialogManualAdmin(final AdminObj adminObj) {
		final Dialog dialog = new Dialog(self);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_manual_admin);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		Button btnEvent = (Button) dialog.findViewById(R.id.btn_event);
		Button btnSuspend = (Button) dialog.findViewById(R.id.btn_suspend);

		btnEvent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(self, EventAdminActivity.class);
				i.putExtra("role", adminObj.getUserName());
				startActivity(i);

				dialog.dismiss();
			}
		});

		btnSuspend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				suspendAdmin(adminObj);

				dialog.dismiss();
			}
		});

		dialog.show();
	}
}
