package com.pcits.events.fragments;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.pcits.common.utils.Logging;
import com.pcits.events.ActivityDetail;
import com.pcits.events.AddEventActivity;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.utils.Utils;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedBold;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;
import com.pcits.events.wizard.model.ICustomPage;

public class PreviewEventFragment extends Fragment implements ICustomPage {

	private Display display;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_preview_event,
				container, false);
		try {
			display = getActivity().getWindowManager().getDefaultDisplay();

			initPreview(rootView);
		} catch (Exception ex) {
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
		return rootView;
	}

	private void initPreview(View rootView) {
		RelativeLayout rltEvent = (RelativeLayout) rootView
				.findViewById(R.id.lytImage);
		ImageView imgThumbnail = (ImageView) rootView
				.findViewById(R.id.imgThumbnail);
		imgThumbnail.getLayoutParams().width = display.getWidth();
		imgThumbnail.getLayoutParams().height = display.getWidth() * 9 / 16;

		TextViewRobotoCondensedBold lblTitle = (TextViewRobotoCondensedBold) rootView
				.findViewById(R.id.lblTitle);
		TextViewRobotoCondensedRegular lblCompany = (TextViewRobotoCondensedRegular) rootView
				.findViewById(R.id.lblCompany);
		TextViewRobotoCondensedRegular lblStartValue = (TextViewRobotoCondensedRegular) rootView
				.findViewById(R.id.lblStartValue);
		TextViewRobotoCondensedRegular lblAfterDiscount = (TextViewRobotoCondensedRegular) rootView
				.findViewById(R.id.lblAfterDiscountValue);
		TextViewRobotoCondensedRegular lblDate = (TextViewRobotoCondensedRegular) rootView
				.findViewById(R.id.lblDate);
		TextViewRobotoCondensedRegular lblTime = (TextViewRobotoCondensedRegular) rootView
				.findViewById(R.id.lblTime);
		TextViewRobotoCondensedRegular lblAttending = (TextViewRobotoCondensedRegular) rootView
				.findViewById(R.id.lblAttending);

		try {
			if (GlobalValue.dealsObj.getTitle() != null) {
				lblTitle.setText(GlobalValue.dealsObj.getTitle());
				lblTitle.setSelected(true);
			}
			if (GlobalValue.dealsObj.getCompany() != null) {
				lblCompany.setText("By: " + GlobalValue.dealsObj.getCompany());
			}
			/*if (GlobalValue.dealsObj.getStartTimeStamp() != null
					&& GlobalValue.dealsObj.getEndTimeStamp() != null) {
				lblDate.setText(DateTimeUtility.convertTimeStampToDate(
						GlobalValue.dealsObj.getStartTimeStamp(), "dd MMM yy")
						+ " - "
						+ DateTimeUtility.convertTimeStampToDate(
								GlobalValue.dealsObj.getEndTimeStamp(),
								"dd MMM yy"));

				lblTime.setText(DateTimeUtility.convertTimeStampToDate(
						GlobalValue.dealsObj.getStartTimeStamp(), "HH:mm")
						+ " - "
						+ DateTimeUtility.convertTimeStampToDate(
								GlobalValue.dealsObj.getEndTimeStamp(), "HH:mm"));
			}*/ //else  {
				if (GlobalValue.dealsObj.getStart_date() != null
						&& GlobalValue.dealsObj.getEnd_date() != null) {
					lblDate.setText(DateTimeUtility.convertStringToDate(
							GlobalValue.dealsObj.getStart_date(), "yyyy-MM-dd",
							"dd MMM yy")
							+ " - "
							+ DateTimeUtility.convertStringToDate(
									GlobalValue.dealsObj.getEnd_date(),
									"yyyy-MM-dd", "dd MMM yy"));
				}
				if (GlobalValue.dealsObj.getStart_time() != null
						&& GlobalValue.dealsObj.getEnd_time() != null) {
					lblTime.setText(GlobalValue.dealsObj.getStart_time()
							+ " - " + GlobalValue.dealsObj.getEnd_time());
				}
			//}
			if (GlobalValue.dealsObj.getStart_value() != null) {
				String startValue = Utils.mCurrency + " "
						+ GlobalValue.dealsObj.getStart_value();
				lblStartValue.setText(startValue.substring(0,
						startValue.indexOf(".")));
				lblStartValue.setPaintFlags(lblStartValue.getPaintFlags()
						| Paint.STRIKE_THRU_TEXT_FLAG);
			}
			if (GlobalValue.dealsObj.getAfter_discount_value() != null) {
				if (GlobalValue.dealsObj.getAfter_discount_value() == 0) {
					lblAfterDiscount.setText("Free");
				} else {
					String afterValue = Utils.mCurrency + " "
							+ GlobalValue.dealsObj.getAfter_discount_value();
					lblAfterDiscount.setText(afterValue.substring(0,
							afterValue.indexOf(".")));
				}
			}

			lblAttending.setText(GlobalValue.dealsObj.getAttend() + "");

			if (AddEventActivity.eventCroppedImage != null) {
				imgThumbnail.setImageBitmap(AddEventActivity.eventCroppedImage);
			} else if (GlobalValue.dealsObj.getImage() != null) {
				AQuery aq = new AQuery(getActivity());
				aq.id(imgThumbnail).image(
						UserFunctions.URLAdmin
								+ GlobalValue.dealsObj.getImage(), false, true);
			}

			// Go to detail page
			imgThumbnail.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(getActivity(), ActivityDetail.class);
					i.putExtra("isPreview", true);
					getActivity().startActivity(i);
				}
			});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void saveState() {
		// TODO Auto-generated method stub

	}

	@Override
	public void restoreState() {
		// TODO Auto-generated method stub

	}

}
