package com.pcits.events.obj;

public class UserObj implements Cloneable {

	private String username, password, fname, lname, dob, address, phone, city,
			county, postcode, country, email, notes, role, payInfo, status,
			image, lastUpdatedPass, lastLoggedin, regiteredTime;

	public String getRegiteredTime() {
		return regiteredTime;
	}

	public void setRegiteredTime(String regiteredTime) {
		this.regiteredTime = regiteredTime;
	}

	public String getLastUpdatedPass() {
		return lastUpdatedPass;
	}

	public void setLastUpdatedPass(String lastUpdatedPass) {
		this.lastUpdatedPass = lastUpdatedPass;
	}

	public String getLastLoggedin() {
		return lastLoggedin;
	}

	public void setLastLoggedin(String lastLoggedin) {
		this.lastLoggedin = lastLoggedin;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFname() {
		return fname != null ? fname : "";
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname != null ? lname : "";
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getDob() {
		return dob != null ? dob : "";
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAddress() {
		return address != null ? address : "";
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone != null ? phone : "";
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCity() {
		return city != null ? city : "";
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county != null ? county : "";
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getPostcode() {
		return postcode != null ? postcode : "";
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCountry() {
		return country != null ? country : "";
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email != null ? email : "";
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNotes() {
		return notes != null ? notes : "";
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPayInfo() {
		return payInfo != null ? payInfo : "";
	}

	public void setPayInfo(String payInfo) {
		this.payInfo = payInfo;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		UserObj user = (UserObj) super.clone();
		return user;
	}
}
