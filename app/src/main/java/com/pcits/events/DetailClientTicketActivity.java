package com.pcits.events;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.widgets.CircleImageWithBorder;
import com.pcits.events.widgets.KenBurnsView;

public class DetailClientTicketActivity extends Activity {

	private TextView lblName, lblNameUser, lblEmail, lblGender, lblDate,
			lblPrice, lblQuantiy;
	private CircleImageWithBorder imgAvatar;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;
	private TextView lblTitleHeader;

	private AQuery aq;
	private boolean memCache = false;
	private boolean fileCache = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scan_detail_tickets_page3);
		initUI();
		initControl();
		initNotBoringActionBar();
	}

	private void initNotBoringActionBar() {
		mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}

	private void initUI() {
		lblTitleHeader = (TextView) findViewById(R.id.lblTitleHeader);
		lblTitleHeader.setText("Info Client");
		llMenu = (LinearLayout) findViewById(R.id.llMenu);
		lblName = (TextView) findViewById(R.id.lblName);
		lblNameUser = (TextView) findViewById(R.id.lblNameUser);
		lblEmail = (TextView) findViewById(R.id.lblEmail);
		lblGender = (TextView) findViewById(R.id.lblGender);
		lblDate = (TextView) findViewById(R.id.lblDate);
		imgAvatar = (CircleImageWithBorder) findViewById(R.id.imgAvatar);
		lblPrice = (TextView) findViewById(R.id.lblPrice);
		lblQuantiy = (TextView) findViewById(R.id.lblQuantity);
	}

	private void initControl() {
		aq = new AQuery(this);

		aq.id(imgAvatar).image(
				UserFunctions.URLAdmin + GlobalValue.ticket.getImgAvatar(),
				memCache, fileCache);
		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		lblName.setText(GlobalValue.ticket.getClientId());
		lblNameUser.setText(GlobalValue.ticket.getClientId());
		lblEmail.setText(GlobalValue.ticket.getEmail());
		lblGender.setText(GlobalValue.ticket.getGender());
		lblDate.setText(GlobalValue.ticket.getCreateDate());
		lblPrice.setText(GlobalValue.ticket.getAmount() + "$");
		lblQuantiy.setText(GlobalValue.ticket.getQuantity() + "");
	}
}
