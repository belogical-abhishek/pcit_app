package com.pcits.events.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.pcits.events.fragments.HistoricalPrivateEventFragment;
import com.pcits.events.fragments.HistoricalPublicEventFragment;

public class HistoricalEventsPagerAdapter extends FragmentStatePagerAdapter
{
    int mNumOfTabs;

    public HistoricalEventsPagerAdapter(FragmentManager fm, int NumOfTabs)
    {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position)
    {

        switch (position)
        {
            case 0:
                HistoricalPublicEventFragment tab1 = new HistoricalPublicEventFragment();
                return tab1;
            case 1:
                HistoricalPrivateEventFragment tab2 = new HistoricalPrivateEventFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
