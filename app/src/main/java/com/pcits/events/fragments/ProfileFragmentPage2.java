package com.pcits.events.fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.pcits.common.utils.Logging;
import com.pcits.events.AddEventActivity;
import com.pcits.events.ProfileActivity;
import com.pcits.events.R;
import com.pcits.events.adapters.CountryAdapter;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.network.NetworkUtility;
import com.pcits.events.widgets.FloatLabeledEditText;
import com.pcits.events.wizard.model.Page;
import com.pcits.events.wizard.ui.PageFragmentCallbacks;

public class ProfileFragmentPage2 extends Fragment implements
		PageFragmentCallbacks {

	private View mView;

	private static final String TAG = "ProfileFragmentPage2";

	private static final String ARG_KEY = "key";

	// private PageFragmentCallbacks mCallbacks;
	// private String mKey;
	// private CustomProfilePage2 mPage;

	private FloatLabeledEditText mFlAddress, mFlCity, mFlCounty, mFlCountry,
			mFlPostCode;

	private ArrayList<String> mArrCountry;

	public static ProfileFragmentPage2 create(String key) {
		Bundle args = new Bundle();
		args.putString(ARG_KEY, key);

		ProfileFragmentPage2 fragment = new ProfileFragmentPage2();
		fragment.setArguments(args);
		return fragment;
	}

	public ProfileFragmentPage2() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Bundle args = getArguments();
		// mKey = args.getString(ARG_KEY);
		// mPage = (CustomProfilePage2) mCallbacks.onGetPage(mKey);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.fragment_profile_page2, container,
				false);
		// ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage
		// .getTitle());
		try{
			initUI();
		} catch(Exception ex){
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
		return mView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (!(activity instanceof PageFragmentCallbacks)) {
			throw new ClassCastException(
					"Activity must implement PageFragmentCallbacks");
		}

		// mCallbacks = (PageFragmentCallbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		// mCallbacks = null;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);

		// In a future update to the support library, this should override
		// setUserVisibleHint
		// instead of setMenuVisibility.
		if (mFlAddress != null && mFlCity != null && mFlCounty != null
				&& mFlCountry != null && mFlPostCode != null) {
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			if (!menuVisible) {
				imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
			}
		}
	}

	public void saveState() {
		try {
			if (ProfileActivity.tempClient != null) {
				ProfileActivity.tempClient.setAddress(mFlAddress
						.getTextString());
				ProfileActivity.tempClient.setCity(mFlCity.getTextString()
						.trim());
				ProfileActivity.tempClient.setCounty(mFlCounty.getTextString()
						.trim());
				ProfileActivity.tempClient.setCountry(mFlCountry
						.getTextString());
				ProfileActivity.tempClient.setPostcode(mFlPostCode
						.getTextString());

				Log.i("PCIT_ABHISHEK", "Address : " + ProfileActivity.tempClient.getAddress());
				Log.i("PCIT_ABHISHEK", "City : " + ProfileActivity.tempClient.getCity());
				Log.i("PCIT_ABHISHEK", "County : " + ProfileActivity.tempClient.getCounty());
				Log.i("PCIT_ABHISHEK", "Country : " + ProfileActivity.tempClient.getCountry());
				Log.i("PCIT_ABHISHEK", "Postal code : " + ProfileActivity.tempClient.getPostcode());
			} else if (ProfileActivity.tempAdmin != null) {
				ProfileActivity.tempAdmin
						.setAddress(mFlAddress.getTextString());
				ProfileActivity.tempAdmin.setCity(mFlCity.getTextString()
						.trim());
				ProfileActivity.tempAdmin.setCounty(mFlCounty.getTextString()
						.trim());
				ProfileActivity.tempAdmin
						.setCountry(mFlCountry.getTextString());
				ProfileActivity.tempAdmin.setPostcode(mFlPostCode
						.getTextString());
			}
		}
		catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	public void restoreState() {
		try {
			if (ProfileActivity.tempClient != null) {
				mFlAddress.setText(ProfileActivity.tempClient.getAddress());
				mFlCity.setText(ProfileActivity.tempClient.getCity());
				mFlCounty.setText(ProfileActivity.tempClient.getCounty());
				mFlCountry.setText(ProfileActivity.tempClient.getCountry());
				mFlPostCode.setText(ProfileActivity.tempClient.getPostcode());
			} else if (ProfileActivity.tempAdmin != null) {
				mFlAddress.setText(ProfileActivity.tempAdmin.getAddress());
				mFlCity.setText(ProfileActivity.tempAdmin.getCity());
				mFlCounty.setText(ProfileActivity.tempAdmin.getCounty());
				mFlCountry.setText(ProfileActivity.tempAdmin.getCountry());
				mFlPostCode.setText(ProfileActivity.tempAdmin.getPostcode());
			}
		}
		catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	private void initUI() {
		try{
			mFlAddress = (FloatLabeledEditText) mView
					.findViewById(R.id.txt_address);
			mFlCity = (FloatLabeledEditText) mView.findViewById(R.id.txt_city);
			mFlCounty = (FloatLabeledEditText) mView.findViewById(R.id.txt_county);
			mFlCountry = (FloatLabeledEditText) mView
					.findViewById(R.id.txt_country);
			mFlPostCode = (FloatLabeledEditText) mView
					.findViewById(R.id.txt_postcode);
	
			mFlCountry.getEditText().setKeyListener(null);
	
			// Should call this method end of declaring UI.
			initControl();
			restoreState();
		}
		catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	private void initControl() {
		try{
			mFlCountry.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					if (NetworkUtility.getInstance(getActivity())
							.isNetworkAvailable()) {
						showCountryDialog();
					} else {
						Toast.makeText(getActivity(), "No network connection",
								Toast.LENGTH_SHORT).show();
					}
				}
			});
	
			mFlCountry.getEditText().setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					if (NetworkUtility.getInstance(getActivity())
							.isNetworkAvailable()) {
						showCountryDialog();
					} else {
						Toast.makeText(getActivity(), "No network connection",
								Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
		catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}

	@Override
	public Page onGetPage(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	private void showCountryDialog() {
		final UserFunctions userFunction = new UserFunctions();

		// Get countries for first click.
		if (mArrCountry == null || mArrCountry.size() <= 0) {
			mArrCountry = new ArrayList<String>();

			new AsyncTask<Void, Void, Void>() {

				@Override
				protected Void doInBackground(Void... params) {
					// TODO Auto-generated method stub
					try {
						// If mActivity equal activityCategory then use API
						// dealByCategory
						JSONObject json = userFunction.showByCountry();
						if (json != null) {
							JSONArray dataDealsArray = json
									.getJSONArray("data");

							for (int i = 0; i < dataDealsArray.length(); i++) {
								JSONObject obj = dataDealsArray
										.getJSONObject(i);
								mArrCountry
										.add(obj.getString(userFunction.key_showcountry_country));
							}
						}
					}
					catch (Exception ex) {
						Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		            }
					return null;
					
				}

				@Override
				protected void onPostExecute(Void result) {
					// TODO Auto-generated method stub
					super.onPostExecute(result);

					final Dialog dialog = new Dialog(getActivity());
					dialog.getWindow();
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.setContentView(R.layout.layout_nice_country);
					final ListView lsvCategory = (ListView) dialog
							.findViewById(R.id.lsvFilterCategory);
					CountryAdapter adapter = new CountryAdapter(getActivity(),
							mArrCountry);
					lsvCategory.setAdapter(adapter);
					lsvCategory.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> parent,
										View view, int position, long id) {
									// TODO Auto-generated method stub
									String selectedCountry = mArrCountry.get(position).toString();
									mFlCountry.setText(selectedCountry);
									dialog.dismiss();

									// Set country
                                    if(ProfileActivity.tempAdmin != null) {
                                        ProfileActivity.tempAdmin.setCountry(selectedCountry);
                                    }
								}
							});
					dialog.show();
				}
			}.execute();
		} else {
			final Dialog dialog = new Dialog(getActivity());
			dialog.getWindow();
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layout_filter_category);
			final ListView lsvCategory = (ListView) dialog
					.findViewById(R.id.lsvFilterCategory);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					getActivity(), android.R.layout.simple_list_item_1,
					mArrCountry);
			lsvCategory.setAdapter(adapter);
			lsvCategory.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					mFlCountry.setText(mArrCountry.get(position));
					dialog.dismiss();
				}
			});
			dialog.show();
		}
	}
}
