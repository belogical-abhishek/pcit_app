/**
 * File        : ActivityAbout.java
 * App name    : M1SO Events
 * Version     : 1.1.4
 * Created     : 25/05/14

 * Created by Team M1SO on 25/01/14.
 * Copyright (c) 2014 M1SO. All rights reserved.
 */

package com.pcits.events;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;

import com.pcits.common.utils.Logging;
import com.pcits.events.adapters.ClientAdapter;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.PurchaseObj;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedBold;

public class ListOfTicketActivity extends FragmentActivity {

	private static final String TAG = "ListOfTicketActivity";
	private ListOfTicketActivity self;
	// Create an instance of ActionBar
	private android.app.ActionBar actionbar;

	private ListView mLvClient;
	private ArrayList<PurchaseObj> mArrTicket;
	private ClientAdapter mClientAdapter;
	private TextViewRobotoCondensedBold mLblNumberOf;

	private String mDealId = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		try{
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_list_of_ticket);
	
			self = this;
	
			Bundle b = getIntent().getExtras();
			if (b != null) {
				mDealId = b.getString("dealId");
				Log.e(TAG, "Deal id: " + mDealId);
			}
	
			// Connect view objects and xml ids
			// Get ActionBar and set back button on actionbar
			actionbar = getActionBar();
			actionbar.setDisplayHomeAsUpEnabled(true);
	
			initUI();
			getData();
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	// Listener for option menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// Previous page or exit
			finish();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void initUI() {
		mLblNumberOf = (TextViewRobotoCondensedBold) findViewById(R.id.lblNumberOf);
		mLvClient = (ListView) findViewById(R.id.lv_client);
	}

	private void getData() {
		try{
			ModelManager.getListOfTicketPaid(self, mDealId, true,
				new ModelManagerListener() {

					@Override
					public void onSuccess(Object object) {
						String json = (String) object;
						mArrTicket = ParserUtility.getListTicketsPaid(json);
						mClientAdapter = new ClientAdapter(self, mArrTicket);
						mLvClient.setAdapter(mClientAdapter);

						// Set number of client
						mLblNumberOf.setText(" " + mArrTicket.size());
					}

					@Override
					public void onError() {
					}
				});
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}
}