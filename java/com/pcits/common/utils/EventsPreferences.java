package com.pcits.common.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.pcits.events.AddPrivateEventActivity;

/**
 * Created by EnvisioDevs on 16-11-2017.
 */

public class EventsPreferences {


    private static String PREF_NAME = "EVENT_PREF";

    private static String KEY_PRIVATE_EVENT = "private_event";

    private static SharedPreferences shared;
    private static SharedPreferences.Editor editor ;


    public static String getKeyPrivateEvent(Context context) {
        shared  = context.getSharedPreferences(PREF_NAME,0);
        return shared.getString(KEY_PRIVATE_EVENT,"");
    }

    public static void setKeyPrivateEvent(Context context, String keyPrivateEvent) {
        shared  = context.getSharedPreferences(PREF_NAME,0);
        editor = shared.edit();
        editor.putString(KEY_PRIVATE_EVENT,keyPrivateEvent).commit();
    }

    public static void clearEventKey(Context context) {
        shared  = context.getSharedPreferences(PREF_NAME,0);
        editor = shared.edit();
        editor.remove(KEY_PRIVATE_EVENT).apply();

    }
}
