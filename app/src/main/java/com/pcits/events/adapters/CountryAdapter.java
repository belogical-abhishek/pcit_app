package com.pcits.events.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.pcits.events.R;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class CountryAdapter extends BaseAdapter {
	private Activity activity;
	// private ArrayList<HashMap<String, String>> data;
	private LayoutInflater inflater = null;
	private ArrayList<String> mArrCountry;

	public CountryAdapter(Activity a, ArrayList<String> arr) {
		activity = a;
		mArrCountry = arr;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	public int getCount() {
		// TODO Auto-generated method stub
		return mArrCountry.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_nice_country, null);
			holder = new ViewHolder();

			holder.lblTitle = (TextViewRobotoCondensedRegular) convertView
					.findViewById(R.id.lblName);
			holder.horBar = convertView.findViewById(R.id.vw_hor_bar);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		String country = mArrCountry.get(position);
		if (!country.isEmpty()) {
			holder.lblTitle.setText(country);

			// Hide last bar
			if (position == mArrCountry.size() - 1) {
				holder.horBar.setVisibility(View.GONE);
			} else {
				holder.horBar.setVisibility(View.VISIBLE);
			}
		}

		return convertView;
	}

	// Method to create instance of views
	static class ViewHolder {
		TextViewRobotoCondensedRegular lblTitle;
		View horBar;
	}
}
