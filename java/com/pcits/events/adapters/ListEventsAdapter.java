package com.pcits.events.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.vision.text.Line;
import com.google.firebase.storage.FileDownloadTask;
import com.pcits.common.utils.GsonUtility;
import com.pcits.events.ActivityAddBills;
import com.pcits.events.ActivityDetail;
import com.pcits.events.ActivityListPrivateUsers;
import com.pcits.events.ActivityPrivateEventDetails;
import com.pcits.events.ActivitySplitBills;
import com.pcits.events.R;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.UserFunctions;
import com.pcits.events.obj.CreateEventObj;
import com.pcits.events.obj.ListEventObj;
import com.pcits.events.utils.DateTimeUtility;
import com.pcits.events.utils.Utils;

import java.io.File;
import java.util.List;

import static com.pcits.events.utils.AppConstants.RC_ON_SPLITBILL_ADDED;

/**
 * Created by SAKSHI on 9/27/2017.
 */

public class ListEventsAdapter extends RecyclerView.Adapter<ListEventsAdapter.ListEventsViewHolder> {
    private List<ListEventObj> listevents;
    private Context context;
    Activity mActivity;
    private AQuery listAq;
    private Display display;
    private String mUsername;
    private int mImgHeight = 0;

    public ListEventsAdapter(List<ListEventObj> listevents, Context context,Activity activity,String username) {
        this.listevents = listevents;
        this.context = context;
        this.mUsername = username;
        mActivity = activity;

        display = activity.getWindowManager().getDefaultDisplay();
        mImgHeight = display.getWidth() * 9 / 16;
    }


    @Override
    public ListEventsAdapter.ListEventsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_event, parent, false);
        return new ListEventsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ListEventsAdapter.ListEventsViewHolder holder, final int position) {

        try {


           try {
               Log.d("list", "onBindViewHolder: "+listevents.get(position).getUsername());
               Log.d("list", "onBindViewHolder: "+mUsername);
               if(listevents.get(position).getUsername().equals(mUsername) || listevents.get(position).getDealId().equals("D000000120")) {
                   holder.editLayout.setVisibility(View.VISIBLE);
                   holder.text_viewBills.setVisibility(View.GONE);
               }
               else {
                   holder.editLayout.setVisibility(View.GONE);
                   holder.text_viewBills.setVisibility(View.VISIBLE);
               }
           }catch (Exception e){
               e.printStackTrace();
           }
            holder.txttitle.setText(listevents.get(position).getTitle().toString());
            holder.eventImage.getLayoutParams().height = mImgHeight;

            if(listevents.get(position).getImage()!=null)
           try {

               Uri uri = Uri.parse(listevents.get(position).getImage());

               holder.eventImage.setImageURI(uri);

           }catch (Exception e){
               e.printStackTrace();
           }
            holder.txtcompany.setText("By:" + listevents.get(position).getCompany().toString());
            holder.txtStartValue.setText(Utils.mCurrency + " " + listevents.get(position).getStartValue().toString()+".0");
            holder.txtStartValue.setPaintFlags(holder.txtStartValue.getPaintFlags()
                    | Paint.STRIKE_THRU_TEXT_FLAG);
            if ((listevents.get(position).getAfterDiscountValue().toString().equals("0")))
                holder.txtDiscountValue.setText("Free");
            else
            holder.txtDiscountValue.setText(Utils.mCurrency + " " + listevents.get(position).getAfterDiscountValue().toString()+".0");
            holder.txtTime.setText(listevents.get(position).getStartTimestamp()+ " - "+listevents.get(position).getEndTimestamp());
            holder.txtDate.setText(DateTimeUtility.getDateFormat(
                    listevents.get(position).getStartDate(), "yyyy-MM-dd", "dd MMM yy")
                    + " - "
                    + DateTimeUtility.getDateFormat(
                    listevents.get(position).getEndDate(), "yyyy-MM-dd", "dd MMM yy"));


            if(listevents.get(position).getViewattendee()!=null || !listevents.get(position).getViewattendee().isEmpty())
            holder.txtAttending.setText(listevents.get(position).getViewattendee());
            else
                holder.txtAttending.setText("0");


            holder.btnAddUsers.setTag(position);
            holder.btnShowUsers.setTag(position);
            holder.layout_attending.setTag(position);
            holder.btnSplitBills.setTag(position);
            holder.itemView.setTag(position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int i = (Integer) v.getTag();
                    ListEventObj mListEventObj = listevents.get(i);

                    Intent intent = new Intent(context, ActivityPrivateEventDetails.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("obj", GsonUtility.convertObjectToJSONString(mListEventObj));
                    if(mListEventObj.getUsername.equalsIgnoreCase(GlobalValue.myClient.getUsername()))
                    {
                        bundle.putString("from","private");
                    }
                    else
                    {
                        bundle.putString("from","deep_link");
                    }
                    bundle.putString("title", mListEventObj.getTitle());
                    bundle.putString("startdate", mListEventObj.getStartDate().toString());
                    bundle.putString("endate", mListEventObj.getEndDate().toString());
                    bundle.putString("startTime", mListEventObj.getStartTime());
                    bundle.putString("endTime", mListEventObj.getEndTime());
                    bundle.putString("description", mListEventObj.getDescription());
                    intent.putExtras(bundle);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    mActivity.overridePendingTransition(
                            R.anim.slide_in_left, R.anim.slide_out_left);


                }
            });


            holder.btnAddUsers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int i = (Integer) holder.btnAddUsers.getTag();
                    addUsers(listevents.get(i).getDealId());

                }
            });

            holder.btnShowUsers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int i = (Integer) holder.btnShowUsers.getTag();
                    showUsers(listevents.get(i).getDealId());
                }
            });

            holder.layout_attending.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int i = (Integer) holder.layout_attending.getTag();
                    showUsers(listevents.get(i).getDealId());
                }
            });

            holder.text_viewBills.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Integer.valueOf(listevents.get(position).getViewattendee())>1) {
                        int i = (Integer) v.getTag();
                        splitBills(listevents.get(i).getDealId(),1);
                    }
                    else{
                        Toast.makeText(context,"Users not added to the event yet",Toast.LENGTH_LONG).show();

                    }
                }
            });
            holder.btnSplitBills.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {Activity
                    if(Integer.valueOf(listevents.get(position).getViewattendee())>1) {
                        int i = (Integer) v.getTag();
                        splitBills(listevents.get(i).getDealId(),2);
                    }
                    else{
                        Toast.makeText(context,"Users not added to the event yet",Toast.LENGTH_LONG).show();
                        
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listevents.size();
    }

    public class ListEventsViewHolder extends RecyclerView.ViewHolder {
        private TextView txttitle, txtcompany, txtStartValue, txtDiscountValue, txtDate, txtTime, txtAttending,text_viewBills;
        private Button btnAddUsers, btnShowUsers, btnSplitBills;
        LinearLayout editLayout,layout_attending;
        private SimpleDraweeView eventImage;


        public ListEventsViewHolder(View itemView) {
            super(itemView);
            txttitle = (TextView) itemView.findViewById(R.id.lblTitle);
            editLayout = (LinearLayout) itemView.findViewById(R.id.ll_buttons);
            layout_attending = (LinearLayout) itemView.findViewById(R.id.layout_attending);
            text_viewBills = (TextView) itemView.findViewById(R.id.text_viewBills);
            eventImage = (SimpleDraweeView) itemView.findViewById(R.id.imgThumbnail);
            txtcompany = (TextView) itemView.findViewById(R.id.lblCompany);
            txtStartValue = (TextView) itemView.findViewById(R.id.lblStartValue);
            txtDiscountValue = (TextView) itemView.findViewById(R.id.lblAfterDiscountValue);
            txtDate = (TextView) itemView.findViewById(R.id.lblDate);
            txtTime = (TextView) itemView.findViewById(R.id.lblTime);
            txtAttending = (TextView) itemView.findViewById(R.id.lblAttending);
            btnAddUsers = (Button) itemView.findViewById(R.id.btnAddUsers);
            btnShowUsers = (Button) itemView.findViewById(R.id.btnShowUsers);
            btnSplitBills = (Button) itemView.findViewById(R.id.btnSplitBills);
        }

    }

    private void addUsers(String dealid) {
        Log.d("privateList", "addUsers: ");
        Intent intent = new Intent(context, ActivityListPrivateUsers.class);
        intent.putExtra("functionid", 1);
        intent.putExtra("dealid", dealid);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void showUsers(String dealid) {
        Intent intent = new Intent(context, ActivityListPrivateUsers.class);
        intent.putExtra("functionid", 2);
        intent.putExtra("dealid", dealid);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void splitBills(String dealid,int id) {

        Intent intent = new Intent(context, ActivitySplitBills.class);
        intent.putExtra("functionid", id);
        intent.putExtra("dealid", dealid);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
       // mActivity.startActivityForResult(intent,RC_ON_SPLITBILL_ADDED);

    }
}
