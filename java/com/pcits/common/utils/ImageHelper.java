package com.pcits.common.utils;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.ImageView;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class ImageHelper {
    Context mContext;
    Uri selectedImageUri;
    String  selectedPath;
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";

    public byte[]mImageBytes;
    public int resX;
    public int resY;
    public String getTempImagePath;
    public Bitmap cropImage;

    public ImageHelper(Context context){
        super();
        mContext = context;
    }

    public byte[] compressImage(Bitmap img){
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            img.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
            return outputStream.toByteArray();
        } catch (Exception ex) {
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
        return null;
    }

    public Bitmap getProfilePic(byte[] imgByte){
        return BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length);
    }

    public Intent getAndCropImageFromGallery(){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        
        try{
	        photoPickerIntent.setType("image/*");
	        photoPickerIntent.putExtra("crop", "true");
	        photoPickerIntent.putExtra("outputX", resX);
	        photoPickerIntent.putExtra("outputY", resY);
	        photoPickerIntent.putExtra("aspectX", 16);
	        photoPickerIntent.putExtra("aspectY", 9);
	        photoPickerIntent.putExtra("scale", true);
	        photoPickerIntent.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
	        photoPickerIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        } catch(Exception ex){
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
        return photoPickerIntent;
    }

    public ImageView setImage(Uri selectedImageUri, ImageView image){
        try {
            if (selectedImageUri != null) {
                selectedPath = getPath(selectedImageUri);
                Bitmap photo = BitmapFactory.decodeFile(selectedPath);
                cropImage = photo;
                mImageBytes = compressImage(photo);
                image.setImageBitmap(photo);
            } else {
            	File tempFile = getTempFile();
                Bitmap selectedImage = BitmapFactory.decodeFile(getPath(getTempUri()));
                mImageBytes = compressImage(selectedImage);
                image.setImageBitmap(selectedImage);
                if (tempFile.exists()) tempFile.delete();
            }
        }
        catch (Exception ex){
            Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
        }
        return image;
    }
    

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        String path = "";
        try {
            Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            path = cursor.getString(column_index);
        }
        catch(Exception ex){
            try{
                path = uri.getPath();
            }
            catch (Exception ex2){
                Logging.writeExceptionFromStackTrace(ex2, ex2.getMessage());
            }
        }
        return path;
    }

    private Uri getTempUri() {
    	try{
    		return Uri.fromFile(getTempFile());
    	} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
    }

    private File getTempFile() {
    	File file = new File("sdcard/",TEMP_PHOTO_FILE);
        try {
            file.createNewFile();
        } catch (IOException ex) {
        	throw new RuntimeException(ex);
        } catch (Exception ex) {
			throw new RuntimeException(ex);
		}
        return file;
    }
}