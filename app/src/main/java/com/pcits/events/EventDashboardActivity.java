package com.pcits.events;

import java.text.NumberFormat;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.pcits.common.utils.Logging;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.EventDashboardObj;
import com.pcits.events.widgets.KenBurnsView;
import com.pcits.events.widgets.textview.TextViewRobotoCondensedRegular;

public class EventDashboardActivity extends Activity implements
		OnClickListener {

	private static final String TAG = "EventDashboardActivity";

	// Declare variable
	private EventDashboardActivity self;

	private LinearLayout llMenu;

	private TextViewRobotoCondensedRegular mLblTitle, mLblTotalSpent,
			mLblTotalMember, mLblMaleMember, mLblFemaleMember,
			mLblNotDisclosedMember, mLblTotalPurchased, mLblMalePurchased,
			mLblFemalePurchased, mLblNotDisclosedPurchased, mLblLuckyAwarded,
			mLblLuckyClaimed, mLblLuckyUnclaimed, mLblLuckyWon, mLblLuckyValue,
			mLblSurveyReceived, mLblSurveySent;
	private Button mBtnTicket, mBtnWinner, mBtnSurvey;

	// Declare header
	private KenBurnsView mHeaderPicture;

	private String mDealId = "";
	private EventDashboardObj mEventDashboard;

	// Declare OnListSelected interface
	public interface OnDataListSelectedListener {
		public void onListSelected(String idSelected);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		try{
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_event_dashboard);
			self = this;
	
			Bundle b = getIntent().getExtras();
			if (b != null) {
				mDealId = b.getString("dealId");
				Log.e(TAG, "Deal id: " + mDealId);
			}
	
			initUI();
			initData();
		} catch(Exception ex){
			Logging.writeExceptionFromStackTrace(ex, ex.getMessage());
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == llMenu) {
			finish();
		} else if (v == mBtnTicket) {
			clickOnTicketButton();
		} else if (v == mBtnWinner) {
			clickOnWinnerButton();
		} else if (v == mBtnSurvey) {
			clickOnSurveyButton();
		}
	}

	private void initUI() {
		llMenu = (LinearLayout) findViewById(R.id.llMenu);
		mLblTitle = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_title);
		mLblTotalSpent = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_total_spent);
		mLblTotalMember = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_total_member);
		mLblMaleMember = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_total_male_member);
		mLblFemaleMember = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_total_female_member);
		mLblNotDisclosedMember = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_total_not_disclosed_member);
		mLblTotalPurchased = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_total_purchased);
		mLblMalePurchased = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_total_male_purchased);
		mLblFemalePurchased = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_total_female_purchased);
		mLblNotDisclosedPurchased = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_total_not_disclosed_puchased);
		mLblLuckyAwarded = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_lucky_awarded);
		mLblLuckyClaimed = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_lucky_claimed);
		mLblLuckyUnclaimed = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_lucky_unclaimed);
		mLblLuckyWon = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_lucky_won);
		mLblLuckyValue = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_lucky_value);
		mLblSurveyReceived = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_received_survey);
		mLblSurveySent = (TextViewRobotoCondensedRegular) findViewById(R.id.lbl_sent_survey);
		mBtnSurvey = (Button) findViewById(R.id.btn_survey);
		mBtnTicket = (Button) findViewById(R.id.btn_ticket);
		mBtnWinner = (Button) findViewById(R.id.btn_winner);

		// Header
		mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

		initControl();
	}

	private void initControl() {
		llMenu.setOnClickListener(this);
		mBtnSurvey.setOnClickListener(this);
		mBtnTicket.setOnClickListener(this);
		mBtnWinner.setOnClickListener(this);
	}

	private void initData() {
		try{
			ModelManager.getEventDashboard(self, mDealId, true,
				new ModelManagerListener() {

					@Override
					public void onSuccess(Object object) {
						String json = (String) object;

						Log.e(TAG, "Event dashboard respone: " + json);

						mEventDashboard = ParserUtility
								.parserEventDashboard(json);

						// Set data for widgets.
						Locale loc = new Locale("en", "US");
						NumberFormat fmt = NumberFormat.getNumberInstance(loc);

						// Delimiter by dot
						String title = mEventDashboard.getTitle();

						String totalSpent = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard.getTotalSpent())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard.getTotalSpent() + "")
												.indexOf("."))
								.replace(",", ".");

						String totalMember = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard
														.getTotalRegisteredMember())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard
												.getTotalRegisteredMember() + "")
												.indexOf("."))
								.replace(",", ".");

						String maleMember = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard
														.getRegisteredMale())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard.getRegisteredMale() + "")
												.indexOf("."))
								.replace(",", ".");

						String femaleMember = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard
														.getRegisteredFemale())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard.getRegisteredFemale() + "")
												.indexOf("."))
								.replace(",", ".");

						String notDisclosedMember = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard
														.getRegisteredNotDisclosed())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard
												.getRegisteredNotDisclosed() + "")
												.indexOf("."))
								.replace(",", ".");

						String totalTicket = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard
														.getTotalPurchased())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard.getTotalPurchased() + "")
												.indexOf("."))
								.replace(",", ".");

						String maleTicket = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard
														.getPurchasedMale())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard.getPurchasedMale() + "")
												.indexOf("."))
								.replace(",", ".");

						String femaleTicket = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard
														.getPurchasedFemale())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard.getPurchasedFemale() + "")
												.indexOf("."))
								.replace(",", ".");

						String notDisclosedTicket = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard
														.getPurchasedNotDisclosed())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard
												.getPurchasedNotDisclosed() + "")
												.indexOf("."))
								.replace(",", ".");

						String luckyAwarded = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard
														.getLuckyAwarded())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard.getLuckyAwarded() + "")
												.indexOf("."))
								.replace(",", ".");

						String luckyClaimed = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard
														.getLuckyClaimed())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard.getLuckyClaimed() + "")
												.indexOf("."))
								.replace(",", ".");

						String luckyUnclaimed = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard
														.getLuckyUnclaimed())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard.getLuckyUnclaimed() + "")
												.indexOf("."))
								.replace(",", ".");

						String luckyWon = fmt
								.format(Long.parseLong(Double
										.valueOf(mEventDashboard.getLuckyWon())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard.getLuckyWon() + "")
												.indexOf("."))
								.replace(",", ".");

						String luckyValue = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard.getLuckyValue())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard.getLuckyValue() + "")
												.indexOf("."))
								.replace(",", ".");

						String surveyReceived = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard
														.getSurveyReceived())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard.getSurveyReceived() + "")
												.indexOf("."))
								.replace(",", ".");

						String surveySent = fmt
								.format(Long.parseLong(Double
										.valueOf(
												mEventDashboard.getSurveySent())
										.toString().replace(".", "")))
								.substring(
										0,
										(mEventDashboard.getSurveySent() + "")
												.indexOf("."))
								.replace(",", ".");

						// Assign data
						mLblTitle.setText(title);
						mLblTotalSpent.setText(totalSpent);
						mLblTotalMember.setText(totalMember);
						mLblMaleMember.setText(maleMember);
						mLblFemaleMember.setText(femaleMember);
						mLblNotDisclosedMember.setText(notDisclosedMember);
						mLblTotalPurchased.setText(totalTicket);
						mLblMalePurchased.setText(maleTicket);
						mLblFemalePurchased.setText(femaleTicket);
						mLblNotDisclosedPurchased.setText(notDisclosedTicket);
						mLblLuckyAwarded.setText(luckyAwarded);
						mLblLuckyClaimed.setText(luckyClaimed);
						mLblLuckyUnclaimed.setText(luckyUnclaimed);
						mLblLuckyWon.setText(luckyWon);
						mLblLuckyValue.setText(luckyValue);
						mLblSurveyReceived.setText(surveyReceived);
						mLblSurveySent.setText(surveySent);
					}

					@Override
					public void onError() {
						// TODO Auto-generated method stub

					}
				});
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void clickOnTicketButton() {
		Intent i = new Intent(self, ListOfTicketActivity.class);
		i.putExtra("dealId", mDealId);
		startActivity(i);
	}

	private void clickOnWinnerButton() {
		Intent i = new Intent(self, ListOfWinnerActivity.class);
		i.putExtra("dealId", mDealId);
		startActivity(i);
	}

	private void clickOnSurveyButton() {
	}
}
