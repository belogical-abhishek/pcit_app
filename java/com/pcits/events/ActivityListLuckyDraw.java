package com.pcits.events;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pcits.events.adapters.AdapterLuckyDraw;
import com.pcits.events.config.GlobalValue;
import com.pcits.events.libraries.ParserUtility;
import com.pcits.events.modelmanager.ModelManager;
import com.pcits.events.modelmanager.ModelManagerListener;
import com.pcits.events.obj.MessageObj;
import com.pcits.events.widgets.KenBurnsView;

public class ActivityListLuckyDraw extends Activity {

	private ArrayList<MessageObj> arrLucky;
	private ListView lsvLuckyDraw;
	private AdapterLuckyDraw adapterLucky;
	// Declare NotBoringActionBar
	private LinearLayout llMenu;
	private KenBurnsView mHeaderPicture;
	private TextView lblTitleHeader;
	private String mDealId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_winner);
		Intent i = getIntent();
		mDealId = i.getStringExtra("deal_id");
		initUI();
		initControl();
		getDataListLuckyDraw();
		initNotBoringActionBar();
	}

	private void initNotBoringActionBar() {
		mHeaderPicture = (KenBurnsView) findViewById(R.id.header_picture);
		mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);

	}

	private void initUI() {
		lblTitleHeader = (TextView) findViewById(R.id.lblTitleHeader);
		lblTitleHeader.setText(getResources().getString(R.string.winner));
		lsvLuckyDraw = (ListView) findViewById(R.id.lsvLuckyDraw);
		llMenu = (LinearLayout) findViewById(R.id.llMenu);
	}

	private void initControl() {

		llMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		lsvLuckyDraw.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				GlobalValue.luckyDrawObj = arrLucky.get(position);
				Intent i = new Intent(ActivityListLuckyDraw.this,
						ActivityDetailLuckyDraw.class);
				startActivity(i);
				ActivityListLuckyDraw.this.overridePendingTransition(
						R.anim.slide_in_left, R.anim.slide_out_left);
			}
		});
	}

	private void getDataListLuckyDraw() {
		try{
			ModelManager.getListLuckyDraw(this, mDealId, true,
				new ModelManagerListener() {

					@Override
					public void onSuccess(Object object) {
						// TODO Auto-generated method stub
						String json = (String) object;
						Log.e("ListAttendOfEvent", "Result LuckyDraw: " + json);
						arrLucky = ParserUtility.getListLuckyDrawOfEvent(json);
						if (arrLucky.size() > 0) {
							adapterLucky = new AdapterLuckyDraw(
									ActivityListLuckyDraw.this, arrLucky);
							adapterLucky.notifyDataSetChanged();
							lsvLuckyDraw.setAdapter(adapterLucky);
						} else {
							Toast.makeText(ActivityListLuckyDraw.this,
									"No Data", Toast.LENGTH_SHORT).show();
						}
					}

					@Override
					public void onError() {
						// TODO Auto-generated method stub

					}
				});
		} catch (Exception ex) {
            throw new RuntimeException(ex);
        }
	}
}
