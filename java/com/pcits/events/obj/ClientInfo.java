package com.pcits.events.obj;

public class ClientInfo implements Cloneable {
	private String id;
	private String fbid, deaid, deviceid, username, password, password2, fname,
			lname, dob, timezone, locale, fullname, link, updatedtime,
			date_register, verified, gender, address, phone, city, county,
			postcode, country, email, image, notes, lastUpdatedPass,
			lastLoggedin, token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getLastUpdatedPass() {
		return lastUpdatedPass;
	}

	public void setLastUpdatedPass(String lastUpdatedPass) {
		this.lastUpdatedPass = lastUpdatedPass;
	}

	public String getLastLoggedin() {
		return lastLoggedin;
	}

	public void setLastLoggedin(String lastLoggedin) {
		this.lastLoggedin = lastLoggedin;
	}

	private String type;

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getFbid() {
		return fbid;
	}

	public void setFbid(String fbid) {
		this.fbid = fbid;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDeaid() {
		return deaid;
	}

	public void setDeaid(String deaid) {
		this.deaid = deaid;
	}

	public String getDeviceid() {
		return deviceid != null ? deviceid : "";
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPhone() {
		return phone != null ? phone : "";
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public String getFname() {
		return fname != null ? fname : "";
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname != null ? lname : "";
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getDob() {
		return dob != null ? dob : "";
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender() {

		return gender != null ? gender : "";
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address != null ? address : "";
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city != null ? city : "";
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county != null ? county : "";
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getPostcode() {
		return postcode != null ? postcode : "";
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCountry() {
		return country != null ? country : "";
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email != null ? email : "";
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getUpdatedtime() {
		return updatedtime;
	}

	public void setUpdatedtime(String updatedtime) {
		this.updatedtime = updatedtime;
	}

	public String getDate_register() {
		return date_register;
	}

	public void setDate_register(String date_register) {
		this.date_register = date_register;
	}

	public String getVerified() {
		return verified;
	}

	public void setVerified(String verified) {
		this.verified = verified;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		ClientInfo client = (ClientInfo) super.clone();
		return client;
	}
}
