package com.pcits.events.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.pcits.events.ActivityHome;
import com.pcits.events.R;
import com.pcits.events.viewpage.CirclePageIndicator;
import com.pcits.events.viewpage.HelpFragmentPagerAdapter;

public class FragmentHelp extends Fragment implements OnClickListener {

	private View view;
	static final int ITEMS = 10;
	private LinearLayout llMenu;
	// MyAdapter mAdapter;
	// Declare NotBoringActionBar
	private ViewPager pager;
	private CirclePageIndicator indicator;
	private float density;
	private LinearLayout llbt_details;
	private Animation slideRight;
	private FragmentManager fm;
	private HelpFragmentPagerAdapter pagerAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.help, container, false);

		llbt_details = (LinearLayout) view.findViewById(R.id.llbt_details);
		slideRight = AnimationUtils.loadAnimation(getActivity(),
				R.anim.slide_in_right);

		llbt_details.startAnimation(slideRight);

		/** Getting a reference to the ViewPager defined the layout file */
		pager = (ViewPager) view.findViewById(R.id.pager);
		indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
		density = getResources().getDisplayMetrics().density;

		/** Getting fragment manager */
		fm = getFragmentManager();

		/** Instantiating FragmentPagerAdapter */

		pagerAdapter = new HelpFragmentPagerAdapter(fm, getActivity());
		/** Setting the pagerAdapter to the pager object */
		pager.setAdapter(pagerAdapter);

		indicator.setViewPager(pager);

		// indicator.setBackgroundColor(0xFFCCCCCC);
		indicator.setRadius(10 * density);
		indicator.setPageColor(0x880000FF);
		// indicator.setFillColor(0xFF073847);
		indicator.setFillColor(0xFF62c7e5);

		indicator.setStrokeColor(0xFF000000);
		indicator.setStrokeWidth(2 * density);

		// Resources res = getResources();
		// lblHelp1 = (TextView) view.findViewById(R.id.lblIntro1);
		llMenu = (LinearLayout) view.findViewById(R.id.llMenu);
		llMenu.setOnClickListener(this);
		// String text = String.format(res.getString(R.string.help1));
		// lblHelp1.setText(Html.fromHtml(text));

		// TextView lblIntro1 = (TextView) view.findViewById(R.id.lblIntro1);

		// initNotBoringActionBar();
		return view;
	}

	// private void initNotBoringActionBar() {
	// mHeaderPicture = (KenBurnsView) view.findViewById(R.id.header_picture);
	// mHeaderPicture.setResourceIds(R.drawable.ny, R.drawable.picture1);
	//
	// }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == llMenu)
		{
				//ActivityHome.mDrwLayout.openDrawer(Gravity.LEFT);
				Intent intent=new Intent(getActivity(),ActivityHome.class);
				startActivity(intent);
				getActivity().finish();
		}
	}
}
