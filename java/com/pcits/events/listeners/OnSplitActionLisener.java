package com.pcits.events.listeners;

/**
 * Created by EnvisioDevs on 16-11-2017.
 */

public interface OnSplitActionLisener {

    public void OnSharePlus(int position);
    public void OnShareMinus(int position);
    public void OnEdited(int position,String updatedValue);
    public void OnEditorModeOn();

}
